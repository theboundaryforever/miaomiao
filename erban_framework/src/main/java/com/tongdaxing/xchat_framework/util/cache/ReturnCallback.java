package com.tongdaxing.xchat_framework.util.cache;

public interface ReturnCallback {

    public void onReturn(Object data) throws Exception;
}
