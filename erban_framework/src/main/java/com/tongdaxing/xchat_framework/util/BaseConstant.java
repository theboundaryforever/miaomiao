package com.tongdaxing.xchat_framework.util;

import com.tongdaxing.xchat_framework.BuildConfig;

/**
 * Function:
 * Author: Edward on 2019/3/19
 */
public class BaseConstant {
    /**
     * 礼物占位符
     */
    public static final String GIFT_PLACEHOLDER = "gift_placeholder";
    /**
     * 等级占位符
     */
    public static final String LEVEL_PLACEHOLDER = "level_placeholder";
    /**
     * 标签占位符
     */
    public static final String LABEL_PLACEHOLDER = "label_placeholder";

    /**
     * cp尾灯占位符
     */
    public static final String CP_SIGN_PLACEHOLDER = "cp_sign_placeholder";

    /**
     * cp尾灯占位符
     */
    public static final String CUSTOM_LABEL_PLACEHOLDER = "custom_label_placeholder";

    /**
     * 默认置顶key
     */
    public static final String DEFAULT_STICKY_KEY = "default_sticky_key";//线上，线下

    /**
     * IM官方喵喵酱id账号
     */
    public static final String OFFICIAL_ACCOUNT_DEBUG = "100015";//线下

    /**
     * IM官方喵喵酱id账号
     */
    public static final String OFFICIAL_ACCOUNT_RELEASE = "1000002";//线上

    /**
     * 点赞评论
     */
    public static final String VOICE_GROUP_ACCOUNT_DEBUG = "101009";//线下
    /**
     * 点赞评论
     */
    public static final String VOICE_GROUP_ACCOUNT_RELEASE = "1505225";//线上

    /**
     * 通知消息
     */
    public static final String NOTIFICATION_ACCOUNT_DEBUG = "109807";//线下
    /**
     * 通知消息
     */
    public static final String NOTIFICATION_ACCOUNT_RELEASE = "1252477";//线上

    public static String getmiaomiaoOfficialAccount() {
        if (BuildConfig.isDebug) {
            return OFFICIAL_ACCOUNT_DEBUG;
        } else {
            return OFFICIAL_ACCOUNT_RELEASE;
        }
    }

    public static String getVoiceGroupAccount() {
        if (BuildConfig.isDebug) {
            return VOICE_GROUP_ACCOUNT_DEBUG;
        } else {
            return VOICE_GROUP_ACCOUNT_RELEASE;
        }
    }

    public static String getNotificationAccount() {
        if (BuildConfig.isDebug) {
            return NOTIFICATION_ACCOUNT_DEBUG;
        } else {
            return NOTIFICATION_ACCOUNT_RELEASE;
        }
    }
}
