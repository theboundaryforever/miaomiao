package com.tongdaxing.xchat_framework.util.util.field;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * 反射工具类
 */
public class FieldUtils {

    /**
     * 反射修改final变量
     */
    public static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);
        try {
            Field artField = Field.class.getDeclaredField("artField");
            artField.setAccessible(true);
            Object fieldValue = artField.get(field);
            Field accessFlagsFiled = fieldValue.getClass().getDeclaredField("accessFlags");
            accessFlagsFiled.setAccessible(true);
            accessFlagsFiled.setInt(fieldValue, field.getModifiers() & ~Modifier.FINAL);
        } catch (NoSuchFieldException e) {
            //没有artField的直接处理属性（<=4.4非art或者>=8.0优化过的art）
            Field accessFlagsFiled = field.getClass().getDeclaredField("accessFlags");
            accessFlagsFiled.setAccessible(true);
            accessFlagsFiled.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        }
        field.set(null, newValue);
    }
}
