package com.tongdaxing.xchat_framework.util.util;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2018/3/14.
 */

public class RichTextUtil {

    public static final String RICHTEXT_STRING = "string";
    public static final String RICHTEXT_COLOR = "color";
//    public static final String RICHTEXT_SIZE = "size";
//    public static final String RICHTEXT_RSIZE = "relativesize";
//    public static final String RICHTEXT_DELETE = "delete";

    public static final String RICHTEXT_CLICK = "click";

    public static HashMap<String, Object> getRichTextMap(String text, int color) {
        HashMap<String, Object> stringObjectMap = new HashMap<>();
        stringObjectMap.put(RichTextUtil.RICHTEXT_STRING, text);
        stringObjectMap.put(RichTextUtil.RICHTEXT_COLOR, color);
        return stringObjectMap;
    }

    /**
     * 根据传入的hashmaplist组成富文本返回,key在RichTextUtil里
     *
     * @param list 富文本的组成list
     */
    public static SpannableStringBuilder getSpannableStringFromList(List<HashMap<String, Object>> list) {
        SpannableStringBuilder ssb = new SpannableStringBuilder("");
        int position = 0;
        for (int i = 0; i < list.size(); i++) {
            HashMap<String, Object> map = list.get(i);
            try {
                String st = (String) map.get(RICHTEXT_STRING);
                ssb.append(st);
                int len = st.length();
                final int color = map.containsKey(RICHTEXT_COLOR) ? (Integer) map.get(RICHTEXT_COLOR) : 0xFFFFFFFF;//颜色
                if (map.containsKey(RICHTEXT_CLICK)) {
                    final OnClickableTextClickListener onClickListener = (OnClickableTextClickListener) map.get(RICHTEXT_CLICK);
                    ssb.setSpan(new ClickableSpan() {
                        @Override
                        public void updateDrawState(TextPaint ds) {
                            super.updateDrawState(ds);
                            if (color != -1) {
                                ds.setColor(color);//文字颜色
                            }
                            ds.setUnderlineText(false);
                        }

                        @Override
                        public void onClick(View view) {
                            if (onClickListener != null) {
                                onClickListener.onClick();//文字点击
                            }
                        }
                    }, position, position + len, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    ssb.setSpan(new ForegroundColorSpan(color), position, position + len, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                position = position + len;
            } catch (Exception e) {
                return ssb;
            }
        }
        return ssb;
    }

    public static abstract class OnClickableTextClickListener {
        private Object tag;

        public OnClickableTextClickListener(Object tag) {
            this.tag = tag;
        }

        public Object getTag() {
            return tag;
        }

        public void setTag(Object tag) {
            this.tag = tag;
        }

        public abstract void onClick();
    }
}