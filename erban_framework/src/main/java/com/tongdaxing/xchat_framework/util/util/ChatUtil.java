package com.tongdaxing.xchat_framework.util.util;

import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.coremanager.IAppInfoCore;

public class ChatUtil {
    public static boolean IM_PUSH_BANNED_ALL = false;//禁止所有发言
    public static boolean IM_PUSH_BANNED_ROOM = false;//房间聊天
    public static boolean IM_PUSH_BANNED_P2P = false;//私聊
    public static boolean IM_PUSH_BANNED_PUBLIC_ROOM = false;//广播

    /**
     * 禁言
     *
     * @return
     */
    public static boolean checkBanned() {
        Json json = CoreManager.getCore(IAppInfoCore.class).getBannedMap();
        boolean all = json.boo(IAppInfoCore.BANNED_ALL + "");
        boolean room = json.boo(IAppInfoCore.BANNED_PUBLIC_ROOM + "");
        if (all || room) {
            SingleToastUtil.showToast("亲，由于您的发言违反了平台绿色公约，若有异议请联系客服");
            return true;
        }
        return false;
    }

    /**
     * 检查私聊禁言
     *
     * @return
     */
    public static boolean checkP2pChatBanned() {
        return chatBanned(IAppInfoCore.BANNED_P2P, IM_PUSH_BANNED_P2P);
    }

    /**
     * 检查房间公聊禁言
     *
     * @return
     */
    public static boolean checkRoomChatBanned() {
        return chatBanned(IAppInfoCore.BANNED_ROOM, IM_PUSH_BANNED_ROOM);
    }

    /**
     * 检查广播禁言
     *
     * @return
     */
    public static boolean checkPublicRoomChatBanned() {
        return chatBanned(IAppInfoCore.BANNED_PUBLIC_ROOM, IM_PUSH_BANNED_PUBLIC_ROOM);
    }

    /**
     * 检查是否禁止所有发言
     *
     * @return
     */
    private static boolean checkAllChatBanned() {
        Json json = CoreManager.getCore(IAppInfoCore.class).getBannedMap();
        boolean all = json.boo(IAppInfoCore.BANNED_ALL);
        if (IM_PUSH_BANNED_ALL || all) {//IM推送最高优先级
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param bannedStr    通过接口获取的禁言状态，非即时
     * @param imPushBanned IM推送禁言，即时生效
     * @return
     */
    private static boolean chatBanned(String bannedStr, boolean imPushBanned) {
        Json json = CoreManager.getCore(IAppInfoCore.class).getBannedMap();
        boolean bannedStatus = json.boo(bannedStr);
        if (checkAllChatBanned() || imPushBanned || bannedStatus) {//IM推送最高优先级
            SingleToastUtil.showToast("亲，由于您的发言违反了平台绿色公约，若有异议请联系客服");
            return true;
        } else {
            return false;
        }
    }
}