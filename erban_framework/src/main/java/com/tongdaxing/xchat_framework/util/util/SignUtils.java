package com.tongdaxing.xchat_framework.util.util;

import android.text.TextUtils;

import com.tongdaxing.xchat_framework.util.util.codec.MD5Utils;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by polo on 2018/6/27.
 */

public class SignUtils {
    private static final String signKey = "8780647b1240f0cb09f14135f4fed7b2";

    public static String signParams(String url, Map<String, String> params, String key, String t) {
        Map<String, String> paramsMap = url2Map(url);
        if (params != null)
            paramsMap.putAll(params);
        if (t != null) {
            paramsMap.put("t", t);
        }

        StringBuilder signStringBuffer = new StringBuilder();
        if (paramsMap != null && paramsMap.size() > 0) {
            for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
//                if (isURLDecoderParams) {
//                    signStringBuffer.append(URLDecoder.decode((entry.getKey()))).append("=").append(URLDecoder.decode(entry.getValue() + ""));//这里兼容老接口
//                } else {
                if (entry.getValue() != null) {
                    signStringBuffer.append(entry.getKey()).append("=").append(entry.getValue());
                }
//                }

            }
        }

        signStringBuffer.append(key);

        String sign = "";
        try {
            sign = MD5Utils.getMD5String(signStringBuffer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sign.length() > 7) {
            sign = sign.substring(0, 7);
        }
        return sign;
    }

    public static Map<String, String> url2Map(String param) {
        Map<String, String> map = new TreeMap<String, String>(new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }

        });

        if (TextUtils.isEmpty(param)) {
            return map;
        }

        String[] urlparams = param.split("\\?");
        if (urlparams != null && urlparams.length == 2) {
            param = urlparams[1];
        } else {
            return map;
        }

        String[] params = param.split("&");
        for (int i = 0; i < params.length; i++) {
            String[] p = params[i].split("=");
            if (p.length == 2) {
                map.put(p[0], p[1]);
            } else if (p.length == 1) {
                map.put(p[0], "");
            }
        }
        return map;
    }


}
