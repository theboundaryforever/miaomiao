package com.tongdaxing.xchat_framework.im.base;

public interface IMReportRoute {
    //通知
    String ChatRoomMemberBlackRemove = "ChatRoomMemberBlackRemove";
    String ChatRoomMemberBlackAdd = "ChatRoomMemberBlackAdd";
    String chatRoomMemberIn = "chatRoomMemberIn";
    String ChatRoomTip = "ChatRoomTip";
    String kickoff = "kickoff";
    String chatRoomMemberExit = "chatRoomMemberExit";
    String ChatRoomInfoUpdated = "ChatRoomInfoUpdated";
    String ChatRoomMemberKicked = "ChatRoomMemberKicked";
    String QueueMemberUpdateNotice = "QueueMemberUpdateNotice";
    String QueueMicUpdateNotice = "QueueMicUpdateNotice";
    String sendMessageReport = "sendMessageReport";
    String updateQueue = "updateQueue";
    String ChatRoomManagerAdd = "ChatRoomManagerAdd";
    String ChatRoomManagerRemove = "ChatRoomManagerRemove";
    String sendTextReport = "sendTextReport";
    String enterPublicRoom = "enterPublicRoom";
    String sendPublicMsgNotice = "sendPublicMsgNotice";
    String sendPublicMsg = "sendPublicMsg";
    String passmicNotice = "passmicNotice";
    /**
     * 邀请进房
     */
    String requestNoRoomUserNotice = "requestNoRoomUserNotice";
    /**
     * 申请上麦
     */
    String applyWaitQueueNotice = "applyWaitQueueNotice";
    /**
     * 用户自动上麦，给红娘发送通知
     */
    String guestUpMicNotice = "guestUpMicNotice";


    /**
     * 推荐主播接受通知
     */
    String recommendPopupNotice ="userRecommendPopupNotice";
}
