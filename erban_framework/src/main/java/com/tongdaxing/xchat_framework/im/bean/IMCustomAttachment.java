package com.tongdaxing.xchat_framework.im.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_framework.im.IMCustomAttachParser;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.json.JsonParser;


/**
 * 先定义一个自定义消息附件的基类，负责解析你的自定义消息的公用字段，比如类型等等。
 *
 * @author zhouxiangfeng
 * @date 2017/6/8
 */
public class IMCustomAttachment implements MsgAttachment {
    /**
     * 自定义消息附件的类型，根据该字段区分不同的自定义消息
     */
    protected int first;
    protected int second;
    protected long time;
    protected JSONObject data;
    /**
     * 房间提示类消息：first  2
     * second
     * 21 房间分享成功提示消息
     * 22 房间关注房主提示消息
     * 23 房间的提醒关注（暂定房间内停留5分钟）
     */
    public static final int CUSTOM_MSG_FIRST_ROOM_TIP = 2;

    public static final int CUSTOM_MSG_SECOND_ROOM_TIP_SHARE_ROOM = 21;
    public static final int CUSTOM_MSG_SECOND_ROOM_TIP_ATTENTION_ROOM_OWNER = 22;
    public static final int CUSTOM_MSG_SECOND_ROOM_TIP_GO_ATTENTION_ROOM = 23;

    /**
     * 单个礼物消息first
     */
    public static final int CUSTOM_MSG_HEADER_TYPE_GIFT = 3;

    public static final int CUSTOM_MSG_SUB_TYPE_SEND_GIFT = 31;

    /**
     * 全麦礼物礼物消息first
     */
    public static final int CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT = 12;
    /**
     * 全服礼物的通知消息first
     */
    public static final int CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT = 14;

    /**
     * 公聊的红包消息
     * first 15 公聊红包消息
     * second 1 发红包通知  2大厅抢红包通知
     * first   公屏的红包消息
     */
    public static final int CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM = 15;

    public static final int CUSTOM_MSG_SECOND_PUBLIC_ROOM_SEND = 1;
    public static final int CUSTOM_MSG_SECOND_PUBLIC_ROOM_RECEIVE = 2;


    /**
     * （龙珠）速配 消息
     */
    public static final int CUSTOM_MSG_HEADER_TYPE_MATCH = 18;

    public static final int CUSTOM_MSG_HEADER_TYPE_MATCH_SPEED = 23;
    public static final int CUSTOM_MSG_HEADER_TYPE_MATCH_CHOICE = 24;

    /**
     * 爆出礼物的消息
     */
    public static final int CUSTOM_MSG_TYPE_BURST_GIFT = 31;//first和second一致

    /**
     * 房间的红包消息
     * first 32    房间的红包消息
     * second 1 房间发包通知  2房间抢红包通知
     */
    public static final int CUSTOM_MSG_FIRST_ROOM_RED_PACKAGE = 32;

    public static final int CUSTOM_MSG_SECOND_ROOM_SEND_RED_PACKAGE = 1;
    public static final int CUSTOM_MSG_SECOND_ROOM_RECEIVE_RED_PACKAGE = 2;
    public static final int CUSTOM_MSG_SECOND_ROOM_RED_PACKAGE_FULL_SERVICE = 3;

    public static final int CUSTOM_MSG_HEADER_TYPE_ACCOUNT = 5;

    public static final int CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI = 6;

    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE = 8;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE = 81;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK = 82;
    //屏蔽小礼物特效
    public static final int CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN = 155;
    public static final int CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE = 156;
    //公屏消息开关
    public static final int CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN = 153;
    public static final int CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE = 154;

    public static final int CUSTOM_MSG_HEADER_TYPE_FACE = 9;
    public static final int CUSTOM_MSG_SUB_TYPE_FACE_SEND = 91;

    public static final int CUSTOM_MSG_HEADER_TYPE_NOTICE = 10;

    public static final int CUSTOM_MSG_HEADER_TYPE_PACKET = 11;
    public static final int CUSTOM_MSG_SUB_TYPE_PACKET_FIRST = 111;

    public static final int CUSTOM_MSG_LOTTERY_BOX = 16;
    public static final int CUSTOM_MSG_MIC_IN_LIST = 17;

    //房间邀请好友消息
    public static final int CUSTOM_MSG_SHARE_FANS = 20;

    //房间内规则
    public static final int CUSTOM_MSG_TYPE_RULE_FIRST = 4;

    // 申请上麦
    public static final int CUSTOM_APPLY_UPPER_MICRO_FIRST = 36;
    // 直播结束
    public static final int CUSTOM_LIVE_STOP_FIRST = 35;
    // 三人语音房亲密值
    public static final int CUSTOM_HONEY_VALUE_FIRST = 34;
    // 全屏守护通知
    public static final int CUSTOM_LOVES_GUARDIAN_FIRST = 37;

    //锁定情侣的通知
    public static final int CUSTOM_LOCK_LOVES_FIRST = 301;

    //有人送礼first,更新守护榜
    public static final int CUSTOM_MSG_SB_SEND_GIFT = 38;

    public IMCustomAttachment() {

    }

    //财富等级
    protected int experLevel;
    //魅力等级
    protected int charmLevel;

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public IMCustomAttachment(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    // 解析附件内容。
    public void fromJson(JSONObject data) {
        if (data != null) {
            parseData(data);
        }
    }

    // 实现 MsgAttachment 的接口，封装公用字段，然后调用子类的封装函数。
    @Override
    public String toJson(boolean send) {
        return IMCustomAttachParser.packData(first, second, packData());
    }

    @Override
    public Json toJson() {
        return IMCustomAttachParser.packData(first, second, packData2());
    }


    // 子类的解析和封装接口。
    protected void parseData(JSONObject data) {

    }

    protected JSONObject packData() {
        return null;
    }

    protected Json packData2() {
        return null;
    }


    protected JSONArray packArray() {
        return null;
    }

    protected int containsKeyInteger(JSONObject data, String key) {
        return data.containsKey(key) ? data.getInteger(key) : 0;
    }

    protected long containsKeyLong(JSONObject data, String key) {
        return data.containsKey(key) ? data.getLong(key) : 0;
    }

    protected boolean containsKeyBoolean(JSONObject data, String key) {
        return data.containsKey(key) ? data.getBoolean(key) : false;
    }

    protected String containsKeyString(JSONObject data, String key) {
        return data.containsKey(key) ? data.getString(key) : "";
    }

    protected double containsKeyDouble(JSONObject data, String key) {
        return data.containsKey(key) ? data.getDouble(key) : 0.0;
    }

    protected IMChatRoomMember parse(String json) {
        IMChatRoomMember chatRoomMember = null;
        if (StringUtils.isNotEmpty(json)) {
            try {
                chatRoomMember = JsonParser.parseJsonObject(json, IMChatRoomMember.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return chatRoomMember;
    }

}
