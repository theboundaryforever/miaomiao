package com.tongdaxing.xchat_framework.im.base;


import com.tcloud.core.log.L;

abstract public class IMProCallBack extends IMCallBack {
    @Override
    public void onSuccess(String data) {
        IMReportBean imReportBean = new IMReportBean(data);
        L.info("request_info_im_onSuccess", data);
        onSuccessPro(imReportBean);
    }

    public abstract void onSuccessPro(IMReportBean imReportBean);
}
