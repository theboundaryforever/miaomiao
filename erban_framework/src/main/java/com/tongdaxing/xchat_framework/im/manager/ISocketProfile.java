package com.tongdaxing.xchat_framework.im.manager;

/**
 * Created by Chen on 2019/7/8.
 */
public interface ISocketProfile {
    String linkHost();

    boolean isDebug();

    int linkPort();

    int noopInterval();

    int connectTime();
}
