package com.tongdaxing.xchat_framework.im.manager;

import com.tongdaxing.xchat_framework.util.config.BasicConfig;

/**
 * Created by Chen on 2019/7/8.
 */
public class DefaultSocketProfile implements ISocketProfile{

    private static final String DEFAULT_LINK_HOST = "39.107.81.178";
    private static final int DEFAULT_LINK_PORT = 3006;
    private static final int DEFAULT_NOOP_INTERVAL = 30000;
    private static final int DEFAULT_CONNECT_TIME = 3000;

    @Override
    public String linkHost() {
        return DEFAULT_LINK_HOST;
    }

    @Override
    public boolean isDebug() {
        return BasicConfig.isDebug;
    }

    @Override
    public int linkPort() {
        return DEFAULT_LINK_PORT;
    }

    @Override
    public int noopInterval() {
        return DEFAULT_NOOP_INTERVAL;
    }

    @Override
    public int connectTime() {
        return DEFAULT_CONNECT_TIME;
    }
}
