package com.tongdaxing.xchat_framework.im.base;

public interface ICommonListener {
    void onDisconnectCallBack(IMErrorBean error);
    void onNoticeMessage(String message);
}
