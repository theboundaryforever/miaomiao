package com.tongdaxing.xchat_framework.http_image.util;

import android.content.Context;
import android.os.Build;

import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.AppMetaDataUtil;
import com.tongdaxing.xchat_framework.util.util.NetworkUtils;
import com.tongdaxing.xchat_framework.util.util.VersionUtil;

import java.util.HashMap;
import java.util.Map;

public class CommonParamUtil {
    private static HashMap<String, String> mDefaultParam;

    public static HashMap<String, String> getDefaultParam() {
        HashMap<String, String> param = new HashMap<>();
        param.putAll(getParam());
        return param;
    }

    private static HashMap<String, String> getParam() {
        if (mDefaultParam == null) {
            synchronized (CommonParamUtil.class) {
                if (mDefaultParam == null) {
                    mDefaultParam = new HashMap<>();
                    mDefaultParam.put("os", "android");
                    mDefaultParam.put("osVersion", Build.VERSION.RELEASE);
                    mDefaultParam.put("app", "xchat");
                    mDefaultParam.put("ispType", String.valueOf(getIspType()));
                    mDefaultParam.put("netType", String.valueOf(getNetworkType()));
                    mDefaultParam.put("model", getPhoneModel());
                    mDefaultParam.put("appVersion", VersionUtil.getLocalName(BasicConfig.INSTANCE.getAppContext()));
                    mDefaultParam.put("appCode", VersionUtil.getVersionCode(BasicConfig.INSTANCE.getAppContext()) + "");
                    mDefaultParam.put("deviceId", DeviceUuidFactory.getDeviceId(BasicConfig.INSTANCE.getAppContext()));
                    mDefaultParam.put("channel", AppMetaDataUtil.getChannelID(BasicConfig.INSTANCE.getAppContext()));
                }
            }
        }
        return mDefaultParam;
    }

    public static Map<String, String> getDefaultParam(Map<String, String> param) {
        if (param == null) {
            param = new HashMap<>();
        }
        param.putAll(getParam());
        return param;
    }

    public static Map<String, String> getDefaultHeaders(Context context) {
        //        param.put("os", "android");
//        param.put("osVersion", Build.VERSION.RELEASE);
//        param.put("app", "xchat");
//        param.put("ispType", String.valueOf(getIspType()));
//        param.put("netType", String.valueOf(getNetworkType()));
//        param.put("model", getPhoneModel());
//        param.put("appVersion", VersionUtil.getLocalName(BasicConfig.INSTANCE.getAppContext()));
//        param.put("imei", DeviceUuidFactory.getDeviceId(BasicConfig.INSTANCE.getAppContext()));
//        param.put("channel", AppMetaDataUtil.getChannelID(BasicConfig.INSTANCE.getAppContext()));
        return new HashMap<>(10);
    }

    public static Map<String, String> getDefaultHeaders() {
        return new HashMap<>(10);
    }

    /**
     * 获取是否nettype字段
     *
     * @return
     */
    public static int getNetworkType() {
        if (NetworkUtils.getNetworkType(BasicConfig.INSTANCE.getAppContext()) == NetworkUtils.NET_WIFI) {
            return 2;
        } else {
            return 1;
        }
    }

    /**
     * 获取运营商字段
     *
     * @return
     */
    public static int getIspType() {
        String isp = NetworkUtils.getOperator(BasicConfig.INSTANCE.getAppContext());
        int ispType = 4;
        if (isp.equals(NetworkUtils.ChinaOperator.CMCC)) {
            ispType = 1;
        } else if (isp.equals(NetworkUtils.ChinaOperator.UNICOM)) {
            ispType = 2;
        } else if (isp.equals(NetworkUtils.ChinaOperator.CTL)) {
            ispType = 3;
        }

        return ispType;
    }

    /**
     * 获取手机型号
     *
     * @return
     */
    public static String getPhoneModel() {
        return Build.MODEL;
    }
}
