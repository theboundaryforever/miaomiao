package com.tongdaxing.xchat_framework.http_image.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.tcloud.core.log.L;

import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Created by chenran on 2017/10/21.
 */

public class DeviceUuidFactory {
    protected static final String PREFS_FILE = "device_id.xml";
    protected static final String PREFS_DEVICE_ID = "device_id";
    private static final String TAG = "DeviceUuidFactory";

    public static String getDeviceId(Context context) {
        UUID uuid = null;
        synchronized (DeviceUuidFactory.class) {
            final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
            final String id = prefs.getString(PREFS_DEVICE_ID, null);
            if (id != null) {
                // Use the ids previously computed and stored in the prefs file
                uuid = UUID.fromString(id);
            } else {
                // Use the Android ID unless it's broken, in which case fallback on deviceId,
                // unless it's not available, then fallback on a random number which we store
                // to a prefs file
                try {
                    final String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                    if (!"9774d56d682e549c".equals(androidId)) {
                        uuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
                    } else {
                        final String deviceId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
                        uuid = deviceId != null ? UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")) : UUID.randomUUID();
                    }
                } catch (Exception e) {

                }
                // Write the value out to the prefs file
                prefs.edit().putString(PREFS_DEVICE_ID, uuid.toString()).commit();
            }
        }
        if (uuid == null) {
            return "";
        } else {
            return uuid.toString();
        }
    }

    /**
     * IMEI也是一串唯一的数字， 标识了 GSM 和 UMTS网络里的唯一一个手机. 它通常被打印在手机里电池下面的那一面，拨 *#06# 也能看到它.
     *
     * @param context
     * @return
     */
    public static String getPhoneIMEI(Context context) {
        String IMEI = "";
        try {
            TelephonyManager tm = (TelephonyManager)
                    context.getSystemService(Context.TELEPHONY_SERVICE);
            IMEI = tm.getDeviceId();
            L.info(TAG, "DEFAULT getPhoneIMEI() = %s, model = %s", IMEI, CommonParamUtil.getPhoneModel());
            //getDeviceId(int slotIndex) 在API 23及以上才能调用 所以这里用反射去执行
            if (TextUtils.isEmpty(IMEI)) {//如果IMEI为空，拿MEID，IMEI是联通移动手机的标识，MEID是电信手机的标识
                Method method = tm.getClass().getMethod("getDeviceId", int.class);
                IMEI = (String) method.invoke(tm, 2);//CDMA 结果为MEID
                L.info(TAG, "PHONE_TYPE_CDMA getPhoneIMEI() = %s, model = %s", IMEI, CommonParamUtil.getPhoneModel());
            }
            if (TextUtils.isEmpty(IMEI)) {
                Method method = tm.getClass().getMethod("getDeviceId", int.class);
                IMEI = (String) method.invoke(tm, 1);//GSM 结果可能会拿到双卡中另一张卡的IMEI
                L.info(TAG, "PHONE_TYPE_GSM getPhoneIMEI() = %s, model = %s", IMEI, CommonParamUtil.getPhoneModel());
            }
        } catch (Exception e) {
            L.error(TAG, "getPhoneIMEI() ERROR: ", e);
            e.printStackTrace();
        }
        return IMEI;
    }
}
