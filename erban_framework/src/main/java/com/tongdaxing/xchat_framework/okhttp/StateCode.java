package com.tongdaxing.xchat_framework.okhttp;

/**
 * Created by Administrator on 2018/1/10.
 */

public interface StateCode {

    int SUCCESS_CODE=200;
    String ERROR_MSG="网络异常";
    int LOGOUT_CODE=401;
}
