package com.tongdaxing.xchat_framework.thirdsdk.umeng;

/**
 * 友盟埋点事件id
 */
public class UmengEventId {

    //----------------------------------------------登陆模块----------------------------------------------------
    //登陆页面 微信登陆
    public static String getLoginWechat() {
        return "login_wechat";
    }

    //登陆页面 qq登陆
    public static String getLoginQQ() {
        return "login_qq";
    }

    //登陆页面 手机号登陆
    public static String getLoginPhone() {
        return "login_phone";
    }


    //app底部导航eventId
    public static String getAppNavEventId(int position) {
        if (position == 0) {
            return "home";
        } else if (position == 1) {
            return "find";
        } else if (position == 3) {
            return "message";
        } else if (position == 4) {
            return "me";
        }
        return "";
    }

    //----------------------------------------------首页模块----------------------------------------------------
    //首页顶部默认分类（热门）选中
    public static String getHomeTopTab1() {
        return "home_top_tab1";
    }

    //首页搜索按钮点击
    public static String getHomeSearchClick() {
        return "home_search_click";
    }

    //创建房间按钮点击
    public static String getHomeRoomBtnClick() {
        return "home_room_btn_click";
    }

    //首页轮播广告点击
    public static String getHomeBanner() {
        return "home_banner";
    }

//    //首页推荐位点击
//    public static String getHomeRecommend() {
//        return "home_recommend";
//    }

    //首页活动位icon点击
    public static String getHomeActivity() {
        return "home_activity";
    }

    //首页房间分类点击
//    public static String getHomeCategory() {
//        return "home_category";
//    }

    //首页房间分类列表点击
    public static String getHomeCategoryList() {
        return "home_category_list";
    }

    //首页房间推荐弹框
    public static String getHomeRecommendRoom() {
        return "home_recommend_room";
    }

    //首页红包弹窗点击
    public static String getHomeRedPackage() {
        return "home_red_package";
    }

    //首页新手期用户推荐弹窗
    public static String getHomeNewUserRecommendRoom() {
        return "home_new_user_recommend_room";
    }

    //----------------------------------------------发现模块----------------------------------------------------

    //发现-交友-邀请有礼
    public static String getFindInvite() {
        return "find_invite";
    }

    //发现-交友-声音鉴定
    public static String getFindSound() {
        return "find_sound";
    }

    //发现-寻友广播
    public static String getFindBroadcast() {
        return "find_broadcast";
    }

    //发现模块的大厅的选中
    public static String getHallTab() {
        return "hall_tab";
    }

    //发现模块的发现的选中
    public static String getFindTab() {
        return "find_tab";
    }

    //大厅的发送按钮点击0
    public static String getFindHallSendBtn() {
        return "find_hall_send_btn";
    }

    //大厅的发送成功-
    public static String getFindHallSendSuc() {
        return "find_hall_send_success";
    }

    //我的 分享入口
//    public static String getDailyTask() {
//        return "daily_task";
//    }

    //交友tab
    public static String getFindSocials() {
        return "find_socials";
    }

    //声圈tab 额外参数moments；后分类news、likes、commend
    public static String getFindMoments() {
        return "find_moments";
    }

    //声圈tab 额外参数moments；后分类news、likes、commend
    public static String getFindMomentsItem(int index) {
        switch (index) {
            case 0:
                return "news";
            case 1:
                return "likes";
            case 2:
                return "commend";
            default:
                return "news";
        }
    }

    //大厅tab
    public static String getFindSquare() {
        return "find_square";
    }

    //发现-暖心陪伴
    public static String getFindWarmAccompany() {
        return "find_warm_accompany";
    }

    //情侣速配点击人数
    public static String getFindSocialsCouples() {
        return "find_socials_couples";
    }

    //暖心陪伴点击人数 后分类female、male、all；对应list中的三个类
    public static String getFindSocialsCouplesList() {
        return "find_pairs";
    }

    //暖心陪伴点击人数 后分类female、male、all；对应list中的三个类
    public static String getFindSocialsCouplesListItem(int index) {
        switch (index) {
            case 0:
                return "all";
            case 1:
                return "male";
            case 2:
                return "female";
            default:
                return "female";
        }
    }

    //自定义栏目点击人数
    public static String getFindSocialsCustomitem() {
        return "find_socials_customitem";
    }

    //声圈推荐下人气之星点击人数 待优化
    public static String getFindMomentsCommenduser() {
        return "find_moments_commenduser";
    }

    //声圈喜欢下推荐圈友点击人数
    public static String getFindMomentsTopuser() {
        return "find_moments_topuser";
    }

    //----------------------------------------------房间相关----------------------------------------------------
    //进入房间
    public static String getJoinRoom() {
        return "room_join";
    }

    //退出房间
    public static String getQuitRoom() {
        return "room_quit";
    }

    //变声
    static String getVocieChanger() {
        return "room_voicechanger";
    }

    //房间音乐搜索点击
    public static String getMusicSearch() {
        return "song_search";
    }

    //房间轮播广告点击
    public static String getRoomBanner() {
        return "room_banner";
    }

    //房间快捷文案
    public static String getRoomQuickMsg() {
        return "room_quick_msg";
    }

    //-----------------------------------声鉴卡--------------------------------
    //声鉴卡保存图片
    public static String getIdentifyCardSaveImg() {
        return "sound_picture";
    }

    //声鉴卡重新鉴定
    public static String getIdentifyCardClear() {
        return "sound_again";
    }

    //声鉴卡分享按钮
    public static String getIdentifyCardShare() {
        return "sound_share";
    }

    //-----------------------------------声音匹配--------------------------------
    //声音匹配点击
    static String getMatchAction() {
        return "match_action";
    }

    //声音匹配 "星球"tab点击
    public static String getMatch() {
        return "match";
    }

    //喵喵声鉴卡页面H5的“点击匹配超动听的声音”
    public static String getSoundMatch() {
        return "sound_match";
    }

    //声音匹配 喜欢用户 点击“马上开撩”
    public static String getMatchLike() {
        return "match_like";
    }

    //声音匹配 我的声鉴
    public static String getMatchMyVoice() {
        return "match_myvoice";
    }

    //----------------------------------------------用户信息相关----------------------------------------------------
    //用户资料提交统计
    public static String getUserInfoCommit() {
        return "user_info_commit";
    }

    //---------------------------------------------------------------其他-------------------------------------------------------------------

    //第一次打开App
    public static String getFirstOpenApp() {
        return "app_first_open";
    }

    //-----------------------------------声圈--------------------------------
    //声圈-点赞
    public static String getMomentLike() {
        return "find_moments_like";
    }

    //声圈-留言 发送成功算一次
    public static String getMomentComment() {
        return "find_moments_comment";
    }

    //声圈-私信
    public static String getMomentMsg() {
        return "find_moments_msg";
    }

    //声圈-关注
    public static String getMomentFollow() {
        return "find_moments_follow";
    }

    //声圈-详情页
    public static String getMomentDetail() {
        return "find_moments_detail";
    }

    //声圈-点击大图
    public static String getMomentPics() {
        return "find_moments_pics";
    }

    //声圈-发动态 发送成功算一次
    public static String getMomentPost() {
        return "find_moments_post";
    }

    //-----------------------------------推送--------------------------------
    //推送通知的点击
    public static String getPushType() {
        return "push_type";
    }

    //-----------------------------------创建房间--------------------------------
    //创建房间刷新按钮点击
    public static String getCreateRoomRefreshTitle() {
        return "room_renew";
    }

    //创房时有勾选一键通知粉丝
    public static String getCreateRoomCheckNoticeFans() {
        return "room_call";
    }

    //-----------------------------------房间内分享弹窗--------------------------------
    public static String getRoomShare() {
        return "navigation_share";
    }

    //房间关注按钮
    public static String getRoomFollow() {
        return "room_follow";
    }
}
