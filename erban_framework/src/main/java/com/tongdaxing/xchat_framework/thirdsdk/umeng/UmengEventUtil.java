package com.tongdaxing.xchat_framework.thirdsdk.umeng;

import android.content.Context;

import com.tcloud.core.log.L;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

/**
 * 友盟事件统计工具类
 * event id不能使用特殊字符，不建议使用中文，且长度不能超过128个字节；
 * map中的key和value 都不能使用特殊字符，key 不能超过128个字节，value 不能超过256个字节。
 * id，ts，du是保留字段，不能作为event id及key的名称。
 * 每个应用至多添加500个自定义事件，每个event 的 key不能超过10个，每个key的取值不能超过1000个。
 * 如需要统计支付金额、使用时长等数值型的连续变量，请使用计算事件（不允许通过key-value结构来统计类似搜索关键词，网页链接等随机生成的字符串信息）。
 */
public class UmengEventUtil {

    private static volatile UmengEventUtil instance;

    private UmengEventUtil() {
    }

    public static UmengEventUtil getInstance() {
        if (instance == null) {
            synchronized (UmengEventUtil.class) {
                if (instance == null) {
                    instance = new UmengEventUtil();
                }
            }
        }
        return instance;
    }

    /**
     * 通用的计数事件。
     *
     * @param context 当前宿主进程的ApplicationContext上下文。
     * @param eventId 为当前统计的事件ID。
     */
    public void onEvent(Context context, String eventId) {
        MobclickAgent.onEvent(context, eventId);
    }


    /**
     * 转发型通用的计数事件。
     * 例如：统计微博应用中”转发”事件发生的次数，那么在转发的函数里调用
     *
     * @param context 当前宿主进程的ApplicationContext上下文。
     * @param eventId 为当前统计的事件ID。
     * @param label   事件的标签属性。
     */
    public void onEvent(Context context, String eventId, String label) {
        MobclickAgent.onEvent(context, eventId, label);
    }

//  下面的方法是在：点击行为各属性被触发的次数考虑事件在不同属性上的取值：
    /**
     * 例如：统计电商应用中”购买”事件发生的次数，以及购买的商品类型及数量，那么在购买的函数里调用：
     * context	当前宿主进程的ApplicationContext上下文。
     * eventId	为当前统计的事件ID。
     * map	为当前事件的属性和取值（Key-Value键值对）。
     */

//--------------------------------------------------------------首页模块-------------------------------------------------------------------

    /**
     * 首页轮播广告点击事件
     *
     * @param context
     * @param bannerId
     */
    public void onHomeBanner(Context context, String bannerName, int bannerId, String skipUrl, int positionId) {
        HashMap<String, String> map = new HashMap<>();
        map.put("bannerName", bannerName);
        map.put("bannerId", bannerId + "");
        map.put("skipUrl", skipUrl);
        map.put("positionId", positionId + "");
        MobclickAgent.onEvent(context, UmengEventId.getHomeBanner(), map);
    }

//    /**
//     * 首页推荐位点击事件
//     *
//     * @param context
//     * @param position 位置id 更多推荐 -1 我要上推荐 -2  其他按照具体位置1开始累加
//     */
//    public void onHomeRecommend(Context context, String position) {
//        HashMap<String, String> map = new HashMap<>();
//        map.put("position", position);
//        MobclickAgent.onEvent(context, UmengEventId.getHomeRecommend(), map);
//    }

    /**
     * 首页活动位点击事件
     *
     * @param context
     * @param name
     */
    public void onHomeActivity(Context context, String name) {
        HashMap<String, String> map = new HashMap<>();
        map.put("activityName", name);
        MobclickAgent.onEvent(context, UmengEventId.getHomeActivity(), map);
    }

//    /**
//     * 首页分类点击统计
//     *
//     * @param context
//     * @param name
//     */
//    public void onHomeCategory(Context context, String name) {
//        HashMap<String, String> map = new HashMap<>();
//        map.put("category", name);
//        MobclickAgent.onEvent(context, UmengEventId.getHomeCategory(), map);
//    }

    /**
     * 首页单个分类列表数据的点击统计
     *
     * @param context
     * @param name
     */
    public void onHomeCategoryList(Context context, String name) {
        HashMap<String, String> map = new HashMap<>();
        map.put("category", name);
        MobclickAgent.onEvent(context, UmengEventId.getHomeCategoryList(), map);
    }


    /**
     * app底部导航的点击统计
     *
     * @param context
     * @param position
     */
    public void onAppBottomNav(Context context, int position) {
        String eventId = UmengEventId.getAppNavEventId(position);
        if (StringUtils.isEmpty(eventId)) {
            return;
        }
        MobclickAgent.onEvent(context, eventId);
    }

//---------------------------------------------------------------发现页-------------------------------------------------------------------

    /**
     * 声圈tab 额外参数moments；后分类news、likes、commend
     *
     * @param context
     * @param moments
     */
    public void onFindMomentsTab(Context context, String moments) {
        HashMap<String, String> map = new HashMap<>();
        map.put("moments", moments);
        MobclickAgent.onEvent(context, UmengEventId.getFindMoments(), map);
    }

    /**
     * 暖心陪伴点击人数 后分类female、male、all；对应list中的三个类
     *
     * @param context
     * @param type    female、male、all
     */
    public void onFindSocialsCouplesList(Context context, String type) {
        HashMap<String, String> map = new HashMap<>();
        map.put("type", type);
        MobclickAgent.onEvent(context, UmengEventId.getFindSocialsCouplesList(), map);
    }

    /**
     * 自定义栏目点击人数 额外参数customitem，记录自定义栏目的名称
     *
     * @param context
     * @param customitem 自定义栏目的名称
     */
    public void onFindSocialsCustomitem(Context context, String customitem) {
        HashMap<String, String> map = new HashMap<>();
        map.put("customitem", customitem);
        MobclickAgent.onEvent(context, UmengEventId.getFindSocialsCustomitem(), map);
    }

//---------------------------------------------------------------用户信息相关模块-------------------------------------------------------------------

    /**
     * 用于统计用户完善资料
     *
     * @param context 下面三个参数用于区分是不是通过邀请注册的用户
     * @param channel
     * @param uid
     * @param roomUid
     */
    public void onUserInfoCommit(Context context, String channel, String uid, String roomUid) {
        HashMap<String, String> map = new HashMap<>();
        map.put("shareChannel", channel);
        map.put("shareUid", uid);
        map.put("roomUid", roomUid);
        MobclickAgent.onEvent(context, UmengEventId.getUserInfoCommit(), map);
    }

//---------------------------------------------------------------房间-------------------------------------------------------------------

    /**
     * 用于统计使用 变声功能 的数据
     *
     * @param type 变声类型（原声、大叔、正太、女声、萝莉、怪兽）
     */
    public void onVoiceChangerClick(Context context, String type) {
        HashMap<String, String> map = new HashMap<>();
        map.put("room_voicechanger_type", type);
        MobclickAgent.onEvent(context, UmengEventId.getVocieChanger(), map);
    }

    /**
     * 房间轮播广告点击事件
     *
     * @param context
     * @param actName    ActionDialogInfo.actName 房间轮播广告名称
     * @param actId      ActionDialogInfo.actId 房间轮播广告id
     * @param positionId
     */
    public void onRoomBanner(Context context, String actName, String actId, String skipUrl, int positionId) {
        HashMap<String, String> map = new HashMap<>();
        map.put("bannerName", actName);
        map.put("bannerId", actId);
        map.put("skipUrl", skipUrl);
        map.put("positionId", positionId + "");
        MobclickAgent.onEvent(context, UmengEventId.getRoomBanner(), map);
    }

    /**
     * 房间快捷文案
     *
     * @param context
     * @param quickMsg 房间快捷文案
     */
    public void onRoomQuickMsg(Context context, String quickMsg) {
        HashMap<String, String> map = new HashMap<>();
        map.put("quickMsg", quickMsg);
        MobclickAgent.onEvent(context, UmengEventId.getRoomQuickMsg(), map);
    }

//---------------------------------------------------------------声音匹配-------------------------------------------------------------------

    /**
     * 声音匹配事件点击
     *
     * @param type   类型（新用户，老用户）
     * @param action 类型（喜欢TA，忽略TA）
     */
    public void onAudioMatchActionClick(Context context, String type, String action) {
        HashMap<String, String> map = new HashMap<>();
        map.put("type", type);
        map.put("action", action);
        MobclickAgent.onEvent(context, UmengEventId.getMatchAction(), map);
    }

    /**
     * 声音匹配事件点击
     *
     * @param type 类型（新用户，老用户）
     */
    public void onAudioMatchTabClick(Context context, String type) {
        HashMap<String, String> map = new HashMap<>();
        map.put("type", type);
        MobclickAgent.onEvent(context, UmengEventId.getMatch(), map);
    }

    /**
     * 声音匹配 喜欢用户 点击“马上开撩”
     *
     * @param giftPrice 礼物单价
     */
    public void onMatchLikeClick(Context context, String giftPrice) {
        HashMap<String, String> map = new HashMap<>();
        map.put("giftPrice", giftPrice);
        MobclickAgent.onEvent(context, UmengEventId.getMatchLike(), map);
    }

//---------------------------------------------------------------其他-------------------------------------------------------------------

    /**
     * 创建房间入口点击事件
     *
     * @param context
     * @param type
     */
    public void onCreateRoom(Context context, String type) {
        HashMap<String, String> map = new HashMap<>();
        map.put("type", type);
        MobclickAgent.onEvent(context, UmengEventId.getHomeRoomBtnClick(), map);
    }

    /**
     * 传递type参数的事件点击
     *
     * @param type 类型（新用户，老用户）
     */
    public void onCommonTypeEvent(Context context, String eventId, String type) {
        HashMap<String, String> map = new HashMap<>();
        map.put("type", type);
        MobclickAgent.onEvent(context, eventId, map);
    }

    /**
     * 寻友广播页面是否开始统计了
     */
    private boolean isFindBroadcastStartStat = false;

    public void startStatFindBroadCast(Class aClass) {
        if (!isFindBroadcastStartStat) {
            L.debug("umeng", "开始统计%s", aClass.getName());
            isFindBroadcastStartStat = true;
            MobclickAgent.onPageStart("寻友广播页面");
        }
    }

    public void endStatFindBroadCast(Class aClass) {
        if (isFindBroadcastStartStat) {
            L.debug("umeng", "结束统计%s", aClass.getName());
            isFindBroadcastStartStat = false;
            MobclickAgent.onPageEnd("寻友广播页面");
        }
    }

    /**
     * 传递type参数的事件点击
     *
     * @param type 类型（新用户，老用户）
     */
    public void onPushTypeClickEvent(Context context, String type) {
        HashMap<String, String> map = new HashMap<>();
        map.put("type", type);
        MobclickAgent.onEvent(context, UmengEventId.getPushType(), map);
    }
}
