package com.tongdaxing.erban.common.ui.rankinglist;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.NumberFormatUtils;
import com.tongdaxing.erban.common.view.LevelView;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.bean.RankingInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.List;

import static com.tongdaxing.erban.common.ui.rankinglist.RankingListFragment.RANKING_TYPE_CHARM;
import static com.tongdaxing.erban.common.ui.rankinglist.RankingListFragment.RANKING_TYPE_TYCOON;

/**
 * <p> 排行榜adapter </p>
 */
public class RankingListAdapter extends BaseQuickAdapter<RankingInfo.RankVo, BaseViewHolder> {
    private final static int HEADER_VIEW_DATA_COUNT = 3;//头部数据个数
    private Context context;
    private View headerView;
    private int rankingType;

//    private View.OnClickListener onHeaderChildViewClickListener;

    public RankingListAdapter(int layoutRes, Context context, int rankingType) {
        super(layoutRes);
        this.context = context;
        this.rankingType = rankingType;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, RankingInfo.RankVo info) {
        if (info == null) {
            return;
        }
        if (baseViewHolder.getAdapterPosition() == getData().size()) {//第一个item，这里注意有headerView
            baseViewHolder.getView(R.id.line).setVisibility(View.GONE);//不显示顶部的灰色线
        } else {
            baseViewHolder.getView(R.id.line).setVisibility(View.VISIBLE);
        }
        //排名
        ((TextView) baseViewHolder.getView(R.id.ranking_number)).setText(String.valueOf(HEADER_VIEW_DATA_COUNT
                + (baseViewHolder.getAdapterPosition() - getHeaderLayoutCount() + 1)));
        //头像
        ImageView imgHead = baseViewHolder.getView(R.id.avatar);
        imgHead.setOnClickListener(new UserHeadImgClickImpl(info.getUid()));
        ImageLoadUtils.loadAvatar(context, info.getAvatar(), imgHead);
        //昵称
        ((TextView) baseViewHolder.getView(R.id.nickname)).setText(info.getNick());
        //性别
        ((ImageView) baseViewHolder.getView(R.id.ranking_list_item_gender)).setImageResource(
                info.getGender() == UserInfo.GENDER_BOY ? R.drawable.user_info_boy : R.drawable.user_info_girl);
        //等级
        baseViewHolder.getView(R.id.level_view).setVisibility(View.VISIBLE);
        LevelView levelView = (LevelView) baseViewHolder.getView(R.id.level_view);
        levelView.hideSeatView();
        if (rankingType == RANKING_TYPE_CHARM) {
            levelView.setCharmLevel(info.getCharmLevel());
        } else {
            levelView.setExperLevel(info.getExperLevel());
        }
        //距离前一名相差多少
        TextView numberText = (TextView) baseViewHolder.getView(R.id.number);
        if (info.getDistance() == 0) {//0时显示1
            numberText.setText("0");
        } else if (info.getDistance() == -1) {//-1代表不显示
            numberText.setText("");
        } else if (info.getDistance() >= 10000) {
            numberText.setText(NumberFormatUtils.formatDoubleDecimalPointWithMax2Digit(info.getDistance() / 10000) + "万");
        } else {
            numberText.setText(String.valueOf((long) info.getDistance()));
        }
    }

    @Override
    public int setHeaderView(View headerView) {
        this.headerView = headerView;
        return super.setHeaderView(headerView);
    }

    @Override
    public void setNewData(@Nullable List<RankingInfo.RankVo> dataList) {
        //这里拆分头部和adapter部分显示
        List<RankingInfo.RankVo> headerDataList = new ArrayList<>();
        List<RankingInfo.RankVo> adapterDataList = new ArrayList<>();
        if (dataList != null) {
            if (dataList.size() > HEADER_VIEW_DATA_COUNT) {//头部显示不完
                headerDataList.addAll(dataList.subList(0, HEADER_VIEW_DATA_COUNT));
                adapterDataList.addAll(dataList.subList(HEADER_VIEW_DATA_COUNT, dataList.size()));
            } else {
                headerDataList.addAll(dataList);
            }
        }
        setupHeaderView(headerDataList);
        super.setNewData(adapterDataList);
    }

    /**
     * 显示头部
     * （数据空的时候各项显示为空，并不是都隐藏）
     */
    private void setupHeaderView(List<RankingInfo.RankVo> headerDataList) {
        ImageView rankingListBg = headerView.findViewById(R.id.ranking_list_bg);
        switch (rankingType) {
            case RANKING_TYPE_CHARM:
                rankingListBg.setImageResource(R.drawable.ranking_list_charm_bg_bottom);
                break;
            case RANKING_TYPE_TYCOON:
                rankingListBg.setImageResource(R.drawable.ranking_list_tycoon_bg_bottom);
                break;
        }

        if (!ListUtils.isListEmpty(headerDataList) && headerDataList.get(0) != null) {
            headerDataList.get(0).setDistance(-1);//第一个不显示距离上一名
        }
        setupHeaderChildView(headerDataList != null && headerDataList.size() > 0 ? headerDataList.get(0) : null,
                R.id.avatar_first, R.id.nickname_first, R.id.level_view_first, R.id.number_first);
        setupHeaderChildView(headerDataList != null && headerDataList.size() > 1 ? headerDataList.get(1) : null,
                R.id.avatar_second, R.id.nickname_second, R.id.level_view_second, R.id.number_second);
        setupHeaderChildView(headerDataList != null && headerDataList.size() > 2 ? headerDataList.get(2) : null,
                R.id.avatar_third, R.id.nickname_third, R.id.level_view_third, R.id.number_third);
    }

    /**
     * 显示头部每一个子view
     */
    private void setupHeaderChildView(RankingInfo.RankVo info, int avatarId, int nicknameId, int levelViewId, int numberId) {
        TextView numberText = (TextView) headerView.findViewById(numberId);
        TextView nickName = (TextView) headerView.findViewById(nicknameId);
        if (info != null) {
            //头像
            ImageView userImgHead = headerView.findViewById(avatarId);
            userImgHead.setOnClickListener(new UserHeadImgClickImpl(info.getUid()));
            ImageLoadUtils.loadAvatar(context, info.getAvatar(), userImgHead);
            //昵称
            nickName.setText(info.getNick());
            //性别
            Drawable drawable;
            if (info.getGender() == UserInfo.GENDER_BOY) {
                drawable = mContext.getResources().getDrawable(R.drawable.user_info_boy);
            } else {
                drawable = mContext.getResources().getDrawable(R.drawable.user_info_girl);
            }
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            nickName.setCompoundDrawables(drawable, null, null, null);
            //等级
            headerView.findViewById(levelViewId).setVisibility(View.VISIBLE);
            if (rankingType == RANKING_TYPE_CHARM) {
                ((LevelView) headerView.findViewById(levelViewId)).setCharmLevel(info.getCharmLevel());
            } else {
                ((LevelView) headerView.findViewById(levelViewId)).setExperLevel(info.getExperLevel());
            }
            //距离前一名相差多少
            if (info.getDistance() == 0) {//0时显示1
                numberText.setText("0");
            } else if (info.getDistance() == -1) {//-1代表不显示
                numberText.setText("");
            } else if (info.getDistance() >= 10000) {
                numberText.setText(NumberFormatUtils.formatDoubleDecimalPointWithMax2Digit(info.getDistance() / 10000) + "万");
            } else {
                numberText.setText(String.valueOf((long) info.getDistance()));
            }
        } else {//数据空的时候各项显示为空
            ImageView userImgHead = headerView.findViewById(avatarId);
            userImgHead.setOnClickListener(null);
            ImageLoadUtils.loadImage(context, R.drawable.default_cover, userImgHead);
            headerView.findViewById(avatarId).setOnClickListener(null);
            nickName.setText("");
            nickName.setCompoundDrawables(null, null, null, null);
            headerView.findViewById(levelViewId).setVisibility(View.INVISIBLE);
            numberText.setText("");
        }
    }

    private class UserHeadImgClickImpl implements View.OnClickListener {
        private long userId;

        public UserHeadImgClickImpl(long userId) {
            this.userId = userId;
        }

        @Override
        public void onClick(View v) {
            NewUserInfoActivity.start(mContext, userId);
        }
    }

}
