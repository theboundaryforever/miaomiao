package com.tongdaxing.erban.common.ui.find.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.find.bean.PictureListInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

/**
 * Function:
 * Author: Edward on 2019/3/13
 */
public class VoiceGroupPictureAdapter extends BaseQuickAdapter<PictureListInfo, BaseViewHolder> {
    public VoiceGroupPictureAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, PictureListInfo item) {
        ImageView imageView = helper.getView(R.id.iv_img);
        if (item.getPicCount() == 1) {
            if (!TextUtils.isEmpty(item.getLayoutType())) {
                if (item.getLayoutType().equals("1")) {
                    //竖屏
                    setViewAutoWidthLp(imageView);
                } else {
                    //横屏
                    setViewAutoHeightLp(imageView);
                }
            } else {
                //竖屏
                setViewAutoWidthLp(imageView);
            }
        } else if (item.getPicCount() == 2) {
            setViewLpTwoItems(imageView);
        } else if (item.getPicCount() >= 3) {
            setViewLpThreeItems(imageView);
        }
        ImageLoadUtils.loadRectangleImage(mContext, item.getUrl(), imageView, R.drawable.default_cover);
//        ImageLoadUtils.loadRectangleImageWithCallback(mContext, item.getUrl(), imageView, R.drawable.ic_no_avatar, new RequestListener<Drawable>() {
//            @Override
//            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                return true;
//            }
//        });
    }

    private void setViewAutoWidthLp(ImageView imageView) {//
        imageView.setAdjustViewBounds(true);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageView.getLayoutParams();
        layoutParams.width = DisplayUtils.dip2px(mContext, 156);
        layoutParams.height = DisplayUtils.dip2px(mContext, 200);//定死高度
        imageView.setLayoutParams(layoutParams);
    }

    private void setViewAutoHeightLp(ImageView imageView) {//
        imageView.setAdjustViewBounds(true);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageView.getLayoutParams();
        layoutParams.height = DisplayUtils.dip2px(mContext, 200);//定死高度
        imageView.setLayoutParams(layoutParams);
    }

    private void setViewLpTwoItems(View view) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        layoutParams.height = (DisplayUtils.getScreenWidth(mContext) - mContext.getResources().getDimensionPixelSize(R.dimen.popular_star_margin) - mContext.getResources().getDimensionPixelSize(R.dimen.wonderful_room_list_margin) * 2) / 2;
        view.setLayoutParams(layoutParams);
    }

    private void setViewLpThreeItems(View view) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        layoutParams.height = (DisplayUtils.getScreenWidth(mContext) - mContext.getResources().getDimensionPixelSize(R.dimen.popular_star_margin) * 2 - mContext.getResources().getDimensionPixelSize(R.dimen.wonderful_room_list_margin) * 2) / 3;
        view.setLayoutParams(layoutParams);
    }
}















