package com.tongdaxing.erban.common.ui.me.wallet.presenter;

import com.tongdaxing.erban.common.ui.me.wallet.view.IIncomeView;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.WalletInfoResult;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public class IncomePresenter extends PayPresenter<IIncomeView> {
    private String TAG = "IncomePresenter";

    public IncomePresenter() {
        super();
    }

    public void loadWalletInfo() {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        String uid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        params.put("uid", uid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("Cache-Control", "no-cache");
        L.info(TAG, "loadWalletInfo uid: %s", uid);
        ResponseListener listener = new ResponseListener<WalletInfoResult>() {
            @Override
            public void onResponse(WalletInfoResult response) {
                L.info(TAG, "loadWalletInfo response: %s", response);
                if (response == null) {
                    return;
                }
                L.info(TAG, "loadWalletInfo data: %s, success: %b", response.getData(), response.isSuccess());
                if (response.isSuccess()) {
                    if (getMvpView() != null && response.getData() != null)
                        getMvpView().setupUserWalletBalance(response.getData());
                } else {
                    if (getMvpView() != null)
                        getMvpView().getUserWalletInfoFail(response.getErrorMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                L.info(TAG, "loadWalletInfo error:", error);
                if (getMvpView() != null && error != null)
                    getMvpView().getUserWalletInfoFail(error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getWalletInfos(),
                        CommonParamUtil.getDefaultHeaders(),
                        params, listener, errorListener,
                        WalletInfoResult.class, Request.Method.GET);
    }

    public void handleClick(int id) {
        getMvpView().handleClick(id);
    }

    public void hasBindPhone() {
        String uid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        L.info(TAG, "loadWalletInfo uid: %s", uid);
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid);
        ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
            @Override
            public void onResponse(ServiceResult<String> response) {
                L.info(TAG, "hasBindPhone response: %s", response);
                if (response == null) {
                    return;
                }
                L.info(TAG, "hasBindPhone data: %s, success: %b", response.getData(), response.isSuccess());
                if (response.isSuccess()) {
                    if (getMvpView() != null)
                        getMvpView().hasBindPhone();
                } else {
                    if (getMvpView() != null)
                        getMvpView().hasBindPhoneFail(response.getErrorMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                L.info(TAG, "hasBindPhone error:", error);
                if (getMvpView() != null && error != null)
                    getMvpView().hasBindPhoneFail(error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.isPhones(),
                        CommonParamUtil.getDefaultHeaders(),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);
    }
}
