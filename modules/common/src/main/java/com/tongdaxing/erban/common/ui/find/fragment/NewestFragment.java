package com.tongdaxing.erban.common.ui.find.fragment;

import com.tongdaxing.erban.common.presenter.find.RecommendPresenter;
import com.tongdaxing.erban.common.ui.find.activity.VoiceGroupDetailActivity;
import com.tongdaxing.erban.common.ui.find.view.IRecommendView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupInfo;

/**
 * Function:动态圈---最新
 * Author: Edward on 2019/3/12
 */
@CreatePresenter(RecommendPresenter.class)
public class NewestFragment extends RecommendFragment implements IRecommendView {

    @Override
    protected void startVoiceGroupDetailPage(VoiceGroupInfo voiceGroupInfo, int index) {
        VoiceGroupDetailActivity.start(getContext(), voiceGroupInfo, index);
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == VOICE_GROUP_DETAIL_REFRESH && resultCode == Activity.RESULT_OK) {
//            setRefreshData();
//        }
//    }

    @Override
    protected void setRefreshData() {
        getMvpPresenter().refreshData("1", "");
    }

    @Override
    protected void setLoadMoreData() {
        getMvpPresenter().loadMoreData("1", "");
    }
}
