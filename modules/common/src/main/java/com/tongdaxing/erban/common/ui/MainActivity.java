package com.tongdaxing.erban.common.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.juxiao.safetychecker.SafetyChecker;
import com.juxiao.safetychecker.bean.SafetyCheckResultBean;
import com.microquation.linkedme.android.LinkedME;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.contact.ContactEventListener;
import com.netease.nim.uikit.custom.ICpNotifyCoreClient;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.SessionEventListener;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.netease.nim.uikit.session.fragment.MessageFragment;
import com.netease.nim.uikit.session.module.IShareFansCoreClient;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderInvitationFans;
import com.netease.nimlib.sdk.NimIntent;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.constant.Extras;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.im.actions.GiftAction;
import com.tongdaxing.erban.common.im.holder.MsgViewHolderContent;
import com.tongdaxing.erban.common.im.holder.MsgViewHolderCpGift;
import com.tongdaxing.erban.common.im.holder.MsgViewHolderGift;
import com.tongdaxing.erban.common.im.holder.MsgViewHolderLottery;
import com.tongdaxing.erban.common.im.holder.MsgViewHolderOnline;
import com.tongdaxing.erban.common.im.holder.MsgViewHolderRedPacket;
import com.tongdaxing.erban.common.im.holder.MsgViewHolderTip;
import com.tongdaxing.erban.common.im.holder.MsgViewHolderWeekCp;
import com.tongdaxing.erban.common.jpush.JPushHelper;
import com.tongdaxing.erban.common.jpush.PushExtras;
import com.tongdaxing.erban.common.model.safetychecker.SafetyCheckerModel;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.room.gift.GiftDialog;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.ui.common.permission.PermissionActivity;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.cpexclusive.CpExclusivePageActivity;
import com.tongdaxing.erban.common.ui.find.activity.NoticeIMActivity;
import com.tongdaxing.erban.common.ui.find.activity.VoiceGroupIMActivity;
import com.tongdaxing.erban.common.ui.find.fragment.FindFragment;
import com.tongdaxing.erban.common.ui.find.fragment.FindVoiceGroupFragment;
import com.tongdaxing.erban.common.ui.home.fragment.HomeFragment;
import com.tongdaxing.erban.common.ui.launch.activity.MiddleActivity;
import com.tongdaxing.erban.common.ui.login.activity.NewLoginActivity;
import com.tongdaxing.erban.common.ui.me.MeFragment;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.BinderPhoneActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.message.fragment.MsgFragment;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.ui.widget.MainTabLayout;
import com.tongdaxing.erban.common.utils.HttpUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SessionHelper;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.home.presenter.MainPresenter;
import com.tongdaxing.xchat_core.home.view.IMainView;
import com.tongdaxing.xchat_core.im.action.ImageAction;
import com.tongdaxing.xchat_core.im.custom.bean.CpGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoticeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.OpenRoomNotiAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.ShareFansAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.TipAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.WeekCpMsgAttachment;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.linked.ILinkedCoreClient;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.music.IMusicUploadCoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.praise.PraiseEvent;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomStatus;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.share.IShareCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.VersionsCoreClient;
import com.tongdaxing.xchat_core.user.bean.CheckUpdataBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.utils.ThreadUtil;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.coremanager.IAppInfoCore;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.log.MLog;
import com.xiaomi.mipush.sdk.MiPushClient;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import cn.jpush.android.api.JPluginPlatformInterface;
import cn.jpush.android.api.JPushInterface;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * @author Administrator
 */
@Route(path = CommonRoutePath.MAIN_ACTIVITY_ROUTE_PATH)
@CreatePresenter(MainPresenter.class)
public class MainActivity extends BaseMvpActivity<IMainView, MainPresenter>
        implements MainTabLayout.OnTabClickListener, IMainView, View.OnTouchListener, ChargeListener, GiftDialog.OnGiftDialogBtnClickListener {


    public static final String TAG = "MainActivityTAG";
    private static final String EXTRA_APP_QUIT = "APP_QUIT";
    @Autowired(name = "link")
    String mLink;
    @Autowired(name = "type")
    int mLinkType;
    @Autowired(name = Constants.PUSH_SKIP_EXTRA)
    String pushSkipData;
    private RelativeLayout avatarLayout;
    private CircleImageView avatarImage;
    private ImageView ivFloatSate;
    private MainTabLayout mMainTabLayout;
    private int mCurrentMainPosition;
    private ViewGroup mViewGroup;
    private int mWidthPixels;
    private int mHeightPixels;
    private long mDownTimeMillis;
    private int mMainTabHeight;

    private JPluginPlatformInterface pHuaweiPushInterface;//兼容华为HMSSDK接口

    private MessageFragment messageFragment;//用来更新私聊页面的消息列表
    private ContactEventListener listener1 = new ContactEventListener() {
        @Override
        public void onItemClick(Context context, String account) {
            NimUIKit.startP2PSession(context, account);
        }

        @Override
        public void onItemLongClick(Context context, String account) {

        }

        @Override
        public void onAvatarClick(Context context, String account) {
            NewUserInfoActivity.start(MainActivity.this, JavaUtil.str2long(account));
        }
    };
    private SessionEventListener listener = new SessionEventListener() {
        @Override
        public void onAvatarClicked(Context context, IMMessage message) {
            if ("90000000".equals(message.getFromAccount())) {
                return;
            }
            NewUserInfoActivity.start(MainActivity.this, JavaUtil.str2long(message.getFromAccount()));
        }

        @Override
        public void onAvatarLongClicked(Context context, IMMessage message) {

        }

        @Override
        public void onEnterRoom(Context context, long uid, long userId, String userTitleName) {
            AVRoomActivity.start(context, uid, new HerUserInfo(userId, userTitleName));
        }

        @Override
        public void loadRoomStatus(long queryUid, LoadRoomStatusCallback loadRoomStatusCallback) {
            Map<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            params.put("queryUid", String.valueOf(queryUid));
            params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            OkHttpManager.getInstance().doGetRequest(UriProvider.getTheRoomStatus(), params, new OkHttpManager.MyCallBack<ServiceResult<RoomStatus>>() {
                @Override
                public void onError(Exception e) {
                    if (e != null) {
                        SingleToastUtil.showToast(e.getMessage());
                    }
                }

                @Override
                public void onResponse(ServiceResult<RoomStatus> response) {
                    if (response == null) {
                        return;
                    }
                    if (response.isSuccess() && response.getData() != null && response.getData().getUid() > 0) {
                        if (loadRoomStatusCallback != null) {
                            loadRoomStatusCallback.callback(response.getData().getUid(), response.getData().getAvatar());//uid是房间roomUid
                        }
                    }
                }
            });
        }

        @Override
        public void getCpState(long targetUid, GetCpStateCallback callback) {
            getMvpPresenter().cpCheck(targetUid, callback);
        }

        @Override
        public void openGiftDialog(Context context, long uid) {
            if (context != null) {
                if (MainActivity.this.isFinishing() || MainActivity.this.isDestroyed()) {
                    return;
                }
                GiftDialog giftDialog = new GiftDialog(context, uid);
                messageFragment = ((P2PMessageActivity) context).getMessageFragment();
                giftDialog.setGiftDialogBtnClickListener(MainActivity.this);
                giftDialog.show();
            }
        }

        @Override
        public void openCpGiftDialog(Context context, long uid) {
            if (context != null) {
                if (MainActivity.this.isFinishing() || MainActivity.this.isDestroyed()) {
                    return;
                }
                GiftDialog giftDialog = new GiftDialog(context, uid, GiftDialog.POSITION_CP);
                messageFragment = ((P2PMessageActivity) context).getMessageFragment();
                giftDialog.setGiftDialogBtnClickListener(MainActivity.this);
                giftDialog.show();
            }
        }

        @Override
        public void openCpExclusive(Context context, long uid) {
            CpExclusivePageActivity.start(MainActivity.this, uid, true);
        }
    };
    private long startTime;
    private boolean shown;
    private boolean alreadyOpenMainActivity;
    private int xDelta;
    private int yDelta;

    public static void startPage(Context context, int page) {
        Intent home = new Intent(context, MainActivity.class);
        home.putExtra(Extras.EXTRA_CHANGE_INDEX, page);
        context.startActivity(home);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.KEY_MAIN_POSITION, mCurrentMainPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentMainPosition = savedInstanceState.getInt(Constants.KEY_MAIN_POSITION);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate: start");
        CoreUtils.register(this);
        if (savedInstanceState != null) {
            mCurrentMainPosition = savedInstanceState.getInt(Constants.KEY_MAIN_POSITION);
        }
        setContentView(R.layout.activity_main);
        setSwipeBackEnable(false);
        //自动登录
        CoreManager.getCore(IAuthCore.class).autoLogin();
        initTitleBar(getString(R.string.app_name));
        initView();
        permission();
        onParseIntent();
        updateDatas();
        updateRoomState();
        initP2PSessionCustomization();

        Disposable subscribe = IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) return;
                        int event = roomEvent.getEvent();
                        if (event == RoomEvent.ENTER_ROOM) {
                            onEnter(AvRoomDataManager.get().mCurrentRoomInfo);
                        } else if (event == RoomEvent.KICK_OUT_ROOM) {

                            ChatRoomKickOutEvent reason = roomEvent.getReason();
                            if (reason == null) return;
                            //加入黑名单，踢出房间回调
                            ChatRoomKickOutEvent.ChatRoomKickOutReason reasonReason = reason.getReason();
                            if (reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.BE_BLACKLISTED
                                    || reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.KICK_OUT_BY_MANAGER
                                    || reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.KICK_OUT_BY_CONFLICT_LOGIN
                                    || reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.CHAT_ROOM_INVALID) {
                                exitRoom();
                            }
                        } else if (event == RoomEvent.ROOM_EXIT) {
                            exitRoom();
                        } else if (event == RoomEvent.ROOM_CHAT_RECONNECTION) {
                            updateRoomState();
                        } else if (event == RoomEvent.INVITE_UP_MIC) {
                            //邀请上麦
                            if (AvRoomDataManager.get().isMinimize()) {
                                getMvpPresenter().onInviteUpMic(roomEvent.getMicPosition());
                            }
                        } else if (event == RoomEvent.KICK_DOWN_MIC) {
                            //被踢下麦
                            SingleToastUtil.showToast(getString(R.string.kick_mic));
                        }
                    }
                });
        mCompositeDisposable.add(subscribe);

        checkUpdata();

        Log.i(TAG, "onCreate: end");
        ARouter.getInstance().inject(this);
        registerPhoneStateListener();

        pHuaweiPushInterface = new JPluginPlatformInterface(this.getApplicationContext());
        //小米推送集成结果 成功会打印正常值
        L.info(TAG, "MiPushClient RegId = %s", MiPushClient.getRegId(getApplicationContext()));
        //极光RegistrationID
        L.info(TAG, "JPushInterface.getRegistrationID() = %s", JPushInterface.getRegistrationID(getApplicationContext()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        pHuaweiPushInterface.onStart(this);

    }

    /**
     * 被抱上麦 进行提示
     */
    @Override
    public void showInviteUpMicSuccess() {
        SingleToastUtil.showToast(getString(R.string.embrace_on_mic));
    }

    private void checkUpdata() {
        VersionsCore core = CoreManager.getCore(VersionsCore.class);
        core.getConfig();
        core.checkVersion();
        core.requestSensitiveWord();
        CoreManager.getCore(IAppInfoCore.class).checkBanned();
    }

    @CoreEvent(coreClientClass = VersionsCoreClient.class)
    public void onVersionUpdataDialog(CheckUpdataBean checkUpdataBean) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        LogUtil.d("onVersionUpdataDialog", checkUpdataBean + "");
        if (checkUpdataBean == null)
            return;
        String updateVersion = checkUpdataBean.getUpdateVersion();
        if (TextUtils.isEmpty(updateVersion))
            return;
        boolean force = false;
        switch (checkUpdataBean.getStatus()) {
            case 1:
                return;
            case 3:
                force = true;
                break;
            case 4:
                force = false;
                break;
            default:

        }

        boolean finalForce = force;

        long l = (Long) SpUtils.get(MainActivity.this, "updataDialogDissTime", 0L);
        if (System.currentTimeMillis() - l < (86400 * 1000) && !finalForce) {
            return;
        }
        AlertDialog alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)

                .setTitle("检测到有最新的版本是否更新")
                .setMessage(checkUpdataBean.getUpdateVersionDesc())
                .setPositiveButton("更新", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String download_url = checkUpdataBean.getDownloadUrl();
                        if (TextUtils.isEmpty(download_url)) {
                            download_url = "https://www.baidu.com/";
                        }
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(download_url));
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (finalForce) {
                            finish();
                        } else {
                            SpUtils.put(MainActivity.this, "updataDialogDissTime", System.currentTimeMillis());
                        }
                    }
                }).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(!force);
        alertDialog.show();
    }

    private void closeOpenRoomAnimation() {
        avatarImage.clearAnimation();
        avatarLayout.setVisibility(View.GONE);
    }

    @CoreEvent(coreClientClass = ICpNotifyCoreClient.class)
    public void onP2PCpInviteAgree() {
        CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid());//更新本地登录用户信息
    }

    @CoreEvent(coreClientClass = ICpNotifyCoreClient.class)
    public void onP2PCpRemove() {
        CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid());//更新本地登录用户信息
    }

    private void initP2PSessionCustomization() {
        SessionCustomization sessionCustomization = new SessionCustomization();
        ArrayList<BaseAction> actions = new ArrayList<>();
        actions.add(new ImageAction());
        actions.add(new GiftAction());
//        actions.add(new CpGiftAction());//cp功能
        sessionCustomization.actions = actions;
        sessionCustomization.withSticker = true;
        NimUIKit.setCommonP2PSessionCustomization(sessionCustomization);

        NimUIKit.registerMsgItemViewHolder(OpenRoomNotiAttachment.class, MsgViewHolderOnline.class);
        NimUIKit.registerMsgItemViewHolder(GiftAttachment.class, MsgViewHolderGift.class);
        NimUIKit.registerMsgItemViewHolder(NoticeAttachment.class, MsgViewHolderContent.class);
        NimUIKit.registerMsgItemViewHolder(RedPacketAttachment.class, MsgViewHolderRedPacket.class);
        NimUIKit.registerMsgItemViewHolder(LotteryAttachment.class, MsgViewHolderLottery.class);
        NimUIKit.registerMsgItemViewHolder(ShareFansAttachment.class, MsgViewHolderInvitationFans.class);
        NimUIKit.registerMsgItemViewHolder(CpGiftAttachment.class, MsgViewHolderCpGift.class);//添加cp消息对应的viewholder
        NimUIKit.registerMsgItemViewHolder(TipAttachment.class, MsgViewHolderTip.class);//添加cp消息对应的viewholder
        NimUIKit.registerMsgItemViewHolder(WeekCpMsgAttachment.class, MsgViewHolderWeekCp.class);//添加cp消息对应的viewholder

        NimUIKit.setSessionListener(listener);
        NimUIKit.setContactEventListener(listener1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LinkedME.getInstance().setImmediate(true);
//        showAudioCardDialog();隐藏获取声鉴卡入口
    }

    @SuppressWarnings("ClickableViewAccessibility")
    private void initView() {
        mHeightPixels = getResources().getDisplayMetrics().heightPixels;
        mWidthPixels = getResources().getDisplayMetrics().widthPixels;
        mMainTabLayout = (MainTabLayout) findViewById(R.id.main_tab_layout);
        avatarLayout = (RelativeLayout) findViewById(R.id.avatar_image_layout);
        avatarImage = (CircleImageView) findViewById(R.id.avatar_image);
        ivFloatSate = (ImageView) findViewById(R.id.iv_main_floating_state);
        mViewGroup = (ViewGroup) findViewById(R.id.root);
        avatarLayout.setVisibility(View.GONE);
        mMainTabLayout.setOnTabClickListener(this);
        initAvatarLayout();
    }

    /**
     * 房间入口按钮拖动逻辑
     */
    private void initAvatarLayout() {
        avatarLayout.setOnTouchListener(this);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) avatarLayout.getLayoutParams();
        int width1 = layoutParams.width;
        int height1 = layoutParams.height;
        int distance = DisplayUtils.dip2px(this, 20);
        ViewGroup.LayoutParams mMainTabLayoutLayoutParams = mMainTabLayout.getLayoutParams();
        int width2 = mMainTabLayoutLayoutParams.width;
        mMainTabHeight = mMainTabLayoutLayoutParams.height;
        int mh = mHeightPixels - height1 - mMainTabHeight - 3 * distance;
        int mw = mWidthPixels - width1 - distance;

        layoutParams.leftMargin = mw;
        layoutParams.topMargin = mh;
        avatarLayout.setLayoutParams(layoutParams);
        mViewGroup.invalidate();
    }

    private void updateDatas() {
        mMainTabLayout.select(mCurrentMainPosition);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        onParseIntent();
    }

    private void onParseIntent() {
        Intent intent = getIntent();
        if (intent.hasExtra(NimIntent.EXTRA_NOTIFY_CONTENT)) {
            IMMessage message = (IMMessage) getIntent().getSerializableExtra(NimIntent.EXTRA_NOTIFY_CONTENT);
            switch (message.getSessionType()) {
                case P2P:
                    SessionHelper.startP2PSession(this, message.getSessionId());
                    break;
                case Team:
                    SessionHelper.startTeamSession(this, message.getSessionId());
                    break;
                default:
                    break;
            }
        } else if (intent.hasExtra(EXTRA_APP_QUIT)) {
            onLogout();
        } else if (intent.hasExtra(Extras.EXTRA_JUMP_P2P)) {
            Intent data = intent.getParcelableExtra(Extras.EXTRA_DATA);
            String account = data.getStringExtra(Extras.EXTRA_ACCOUNT);
            if (!TextUtils.isEmpty(account)) {
                SessionHelper.startP2PSession(this, account);
            }
        }
//        else if (!TextUtils.isEmpty(mLink)) {
//            startTime = System.currentTimeMillis();
//        }
        else if (intent.hasExtra(Extras.EXTRA_CHANGE_INDEX)) {
            int index = intent.getIntExtra(Extras.EXTRA_CHANGE_INDEX, 0);//切换主界面的显示页面
            if (index != mCurrentMainPosition) {
                mMainTabLayout.select(index);
            }
        }
    }

    private void permission() {
        checkPermission(new PermissionActivity.CheckPermListener() {
                            @Override
                            public void superPermission() {
                            }
                        }, R.string.ask_again,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        pHuaweiPushInterface.onStop(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //JPush中调用HMS SDK解决错误的接口传入的requestCode为10001,开发者调用是请注意不要同样使用10001
        if (requestCode == JPluginPlatformInterface.JPLUGIN_REQUEST_CODE) {
            pHuaweiPushInterface.onActivityResult(this, requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CoreUtils.unregister(this);
        listener = null;
        listener1 = null;
        NimUIKit.setSessionListener(null);
        NimUIKit.setContactEventListener(null);
        mPhoneStateListener = null;
        MLog.debug(TAG, "MainActivity : destroyed");
    }

    /**
     * 双击返回键退出
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void moveTaskToBack() {
        //                //按一次提示退出信息
        Intent home = new Intent(Intent.ACTION_MAIN);
//                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addCategory(Intent.CATEGORY_HOME);
        startActivity(home);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack();
    }

    private void checkoutLinkedMe() {
        Intent intent = new Intent(this, MiddleActivity.class);
        startActivity(intent);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (uid > 0) {//这个接口是覆盖逻辑，而不是增量逻辑。即新的调用会覆盖之前的设置 -- 所以不用考虑登陆需要删除之前的alias
            L.info(TAG, "JPush alias = %d", uid);
            //uid作为别名
            JPushHelper.getInstance().onAliasAction(this, uid + "", JPushHelper.ACTION_SET_ALIAS);

            //覆盖tag: [uid] [debug]/[product]
            HashSet<String> tagSet = new HashSet<>();
            tagSet.add(uid + "");
            tagSet.add(BasicConfig.isDebug ? JPushHelper.TAG_DEBUG : JPushHelper.TAG_PRODUCT);
            BasicConfig.INSTANCE.setJpushTags(tagSet);
            JPushHelper.getInstance().onTagsAction(this, tagSet, JPushHelper.ACTION_SET_TAG);
        }
        userDefaultPage();
        checkoutLinkedMe();
    }

    private void userDefaultPage() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            if (mMainTabLayout != null) {
                //1.4.3版本取消进入发现页面
//                if (CoreManager.getCore(IUserCore.class).isNewUser(userInfo) && userInfo.getChargeNum() <= 0) {//用户处于新手期，且没有充值过，默认发现页面
//                    com.tongdaxing.xchat_framework.util.util.LogUtil.d("MainTabLayout select 1 用户处于新手期，且没有充值过，默认发现页面");
//                    mMainTabLayout.select(1);
//                } else {
                //用户处于新手期且充值过或者超过新手期，默认房间页面
                com.tongdaxing.xchat_framework.util.util.LogUtil.d("MainTabLayout select 0 用户处于新手期且充值过或者超过新手期，默认房间页面");
                mMainTabLayout.select(0);
//                }
            }
        } else {
            //请求用户信息 判断是否处于新手期，且没有充值过
            getMvpPresenter().requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onRequestTicketFail(String error) {
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {
        //退出登录时 删除别名
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (uid > 0) {
            JPushHelper.getInstance().onAliasAction(this, uid + "", JPushHelper.ACTION_DELETE_ALIAS);
            HashSet<String> tagSet = new HashSet<>();
            tagSet.add(uid + "");
            JPushHelper.getInstance().onTagsAction(this, tagSet, JPushHelper.ACTION_DELETE_TAG);
        }
        NimUIKit.clearCache();
        getMvpPresenter().exitRoom();
        NewLoginActivity.start(MainActivity.this);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onNeedLogin() {
        NewLoginActivity.start(MainActivity.this);
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onImLoginSuccess(LoginInfo loginInfo) {
        L.debug(TAG, "onImLoginSuccess mLink = %s, mLinkType = %d, System.currentTimeMillis() = %d, startTime = %d", mLink, mLinkType, System.currentTimeMillis(), startTime);
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        mMainTabLayout.setMsgNum(unreadCount);
        // 如果需要跳转,在这里实现
        if (Build.BRAND.equalsIgnoreCase("Huawei") || Build.BRAND.equalsIgnoreCase("HONOR")
                || Build.BRAND.equalsIgnoreCase("OPPO")) {
            String pushExtras = BasicConfig.INSTANCE.getPushExtras();//针对华为和OPPO不支持先启动应用 再打开目标页面 只能在内存中保存要跳转的页面的数据
            L.debug(TAG, "BasicConfig.INSTANCE.getPushExtras: %s", pushExtras);
            if (!TextUtils.isEmpty(pushExtras)) {
                dealWithPushData(pushExtras);
                return;
            }
        }
        if (!TextUtils.isEmpty(pushSkipData)) {
            dealWithPushData(pushSkipData);
            return;
        }
        if (!shown && !TextUtils.isEmpty(mLink)) {
            shown = true;
            int type = mLinkType;
            String url = mLink;
            if (type == 3) {
                L.debug(TAG, "startActivity CommonWebViewActivity");
                Intent intent = new Intent(this, CommonWebViewActivity.class);
                intent.putExtra("url", url);
                startActivity(intent);
            } else if (type == 2) {
                L.debug(TAG, "startActivity AVRoomActivity");
                AVRoomActivity.start(this, JavaUtil.str2long(url));
            }
        }
    }

    /**
     * 处理推送通知点击跳转
     *
     * @param pushExtrasJson
     */
    private void dealWithPushData(String pushExtrasJson) {
        L.debug(TAG, "dealWithPushData");
        Json json = Json.parse(pushExtrasJson);
        String skipType = json.str("skipType");
        String skipUri = json.str("skipUri");
        String msgType = json.str("msgType");
        L.info(TAG, "dealWithPushData skipUri = %s, skipType = %s, msgType = %s", skipType, skipUri, msgType);
        if (skipType == null) {
            return;
        }
        if (!TextUtils.isEmpty(msgType)) {
            UmengEventUtil.getInstance().onCommonTypeEvent(MainActivity.this, UmengEventId.getPushType(), msgType);
        }
        switch (skipType) {
            case PushExtras.P2P_CHAT:
                try {
                    if (TextUtils.isEmpty(skipUri)) {
                        return;
                    }
                    long userId = Long.parseLong(skipUri);
                    HttpUtil.checkUserIsDisturb(MainActivity.this, userId);
                } catch (NumberFormatException e) {
                    CoreUtils.crashIfDebug(e, "push userId NumberFormatException");
                }
                break;
            case PushExtras.ROOM:
                try {
                    if (TextUtils.isEmpty(skipUri)) {
                        return;
                    }
                    long userId = Long.parseLong(skipUri);
                    AVRoomActivity.start(MainActivity.this, userId);
                } catch (NumberFormatException e) {
                    CoreUtils.crashIfDebug(e, "push userId NumberFormatException");
                }
                break;
            case PushExtras.H5:
                if (TextUtils.isEmpty(skipUri)) {
                    return;
                }
                CommonWebViewActivity.start(MainActivity.this, skipUri);
                break;
            case PushExtras.VOICE_GROUP_IM:
                VoiceGroupIMActivity.start(MainActivity.this, BaseConstant.getVoiceGroupAccount());
                break;
            case PushExtras.NOTIFICATION_IM:
                NoticeIMActivity.start(MainActivity.this, BaseConstant.getNotificationAccount());
                break;
        }
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onImLoginFaith(String error) {
        toast(error);
        NewLoginActivity.start(MainActivity.this);
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        toast("您已被踢下线");
        CoreManager.getCore(IAuthCore.class).logout();
    }

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceiveRecentContactChanged() {
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        mMainTabLayout.setMsgNum(unreadCount);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onNeedCompleteInfo() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        getDialogManager().dismissDialog();
        UIHelper.showAddInfoAct(this);
    }

    /**
     * 用户首次注册成功，要跳转到指定的页面
     *
     * @param userInfo
     */
    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoComplete(UserInfo userInfo) {
        userDefaultPage();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdateFail(String msg) {
        onLogout();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        if (userInfo != null) {
            if (userInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
                L.debug(TAG, "onCurrentUserInfoUpdate experLevel = %d, headwearUrl = %s, put them to sp", userInfo.getExperLevel(), userInfo.getHeadwearUrl());
                SpUtils.put(getBaseContext(), AvRoomModel.EXPER_LEVEL, userInfo.getExperLevel());
                SpUtils.put(getBaseContext(), AvRoomModel.EXPER_CHARM, userInfo.getCharmLevel());
                SpUtils.put(getBaseContext(), SpEvent.headwearUrl, userInfo.getHeadwearUrl());
            }
            CoreManager.getCore(IAuthCore.class).setThirdUserInfo(null);
            int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
            mMainTabLayout.setMsgNum(unreadCount);

            safetyCheck();
        }
    }

    /**
     * 安全检测
     */
    private void safetyCheck() {
        ThreadUtil.getThreadPool().execute(() -> {
            SafetyCheckResultBean checkResult = SafetyChecker.getInstance().check(this.getApplicationContext());
            if (checkResult.getCheckStatus() != 0) {//不安全的，上报
                ThreadUtil.runOnUiThread(() -> {
                    SafetyCheckerModel safetyCheckerModel = new SafetyCheckerModel();
                    safetyCheckerModel.reportSafetyCheckResult(checkResult);
                });
            }
        });
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetRoomInfo(RoomInfo roomInfo, int pageType) {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        getDialogManager().dismissDialog();
        if (roomInfo != null) {
            if (roomInfo.isValid()) {
                AVRoomActivity.start(this, roomInfo.getUid());
            } else {
                openRoom();
            }

        } else {
            openRoom();
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetRoomInfoFail(int code, String error, int pageType) {
        L.error(TAG, "onGetRoomInfoFail" + error);
        getDialogManager().dismissDialog();
        if (code == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
            //MainActivity回调弹出弹框
            CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_REAL_NAME_AUTH, this,
                    getResources().getString(R.string.real_name_auth_tips,
                            getResources().getString(R.string.real_name_auth_tips_publish_content)));
        } else {
            toast(error);
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onCloseRoomInfo() {
        openRoom();
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onCloseRoomInfoFail(String error) {
        LogUtil.e(TAG, "onCloseRoomInfoFail" + error);
        toast(error);
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onOpenRoom(RoomInfo roomInfo) {
        AVRoomActivity.start(this, roomInfo.getUid());
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onOpenRoomFail(String error) {
        LogUtil.e(TAG, "onOpenRoomFail" + error);
        toast(error);
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onAlreadyOpenedRoom() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        getDialogManager().showProgressDialog(this, "请稍后...");
        CoreManager.getCore(IRoomCore.class).closeRoomInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onOpenRoomAuthFail() {
        CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_REAL_NAME_AUTH, this,
                getResources().getString(R.string.real_name_auth_tips,
                        getResources().getString(R.string.real_name_auth_tips_publish_content)));
    }

    private void openRoom() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        CoreManager.getCore(IRoomCore.class).openRoom(userInfo.getUid(), RoomInfo.ROOMTYPE_HOME_PARTY,
                "", "", "", "");
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoCompleteFaith(String msg) {
        getDialogManager().dismissDialog();
        toast("用户信息加载失败，请检查网络后重新登录");
        CoreManager.getCore(IAuthCore.class).logout();
    }

    private void updateRoomState() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(roomInfo.getUid());
            if (userInfo != null) {
                avatarLayout.clearAnimation();
                avatarLayout.setVisibility(View.VISIBLE);
                Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
                LinearInterpolator lin = new LinearInterpolator();
                operatingAnim.setInterpolator(lin);
                avatarImage.startAnimation(operatingAnim);
                //名称
//                tvName.setText(userInfo.getNick());
//                tvId.setText("ID:" + userInfo.getErbanNo());
                ImageLoadUtils.loadImage(this, R.drawable.ic_main_floating, ivFloatSate);
                ImageLoadUtils.loadAvatar(MainActivity.this, userInfo.getAvatar(), avatarImage);
            } else {
                NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(roomInfo.getUid() + "");
                if (nimUserInfo == null) {
                    NimUserInfoCache.getInstance().getUserInfoFromRemote(roomInfo.getUid() + "", new RequestCallbackWrapper<NimUserInfo>() {
                        @Override
                        public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                            if (nimUserInfo != null) {
                                avatarLayout.clearAnimation();
                                avatarLayout.setVisibility(View.VISIBLE);
                                Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
                                LinearInterpolator lin = new LinearInterpolator();
                                operatingAnim.setInterpolator(lin);
                                avatarImage.startAnimation(operatingAnim);
//                                tvName.setText(nimUserInfo.getName());
                                ImageLoadUtils.loadImage(MainActivity.this, R.drawable.ic_main_floating, ivFloatSate);
                                ImageLoadUtils.loadAvatar(MainActivity.this, nimUserInfo.getAvatar(), avatarImage);
                            }
                        }
                    });
                } else {
                    avatarLayout.clearAnimation();
                    avatarLayout.setVisibility(View.VISIBLE);
                    Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
                    LinearInterpolator lin = new LinearInterpolator();
                    operatingAnim.setInterpolator(lin);
                    avatarImage.startAnimation(operatingAnim);
//                    tvName.setText(nimUserInfo.getName());
                    ImageLoadUtils.loadImage(this, R.drawable.ic_main_floating, ivFloatSate);
                    ImageLoadUtils.loadAvatar(MainActivity.this, nimUserInfo.getAvatar(), avatarImage);
                }
            }
        }
    }

    public void onEnter(RoomInfo roomInfo) {
        updateRoomState();
//        DaemonService.start(this, roomInfo);
    }

    @CoreEvent(coreClientClass = ILinkedCoreClient.class)
    public void onLinkedInfoUpdateNotLogin() {
        onLogout();
    }

    @CoreEvent(coreClientClass = ILinkedCoreClient.class)
    public void onLinkedInfoUpdate(LinkedInfo linkedInfo) {
        if (!StringUtil.isEmpty(linkedInfo.getRoomUid())) {
            AVRoomActivity.start(this, JavaUtil.str2long(linkedInfo.getRoomUid()));
        }
    }

    @CoreEvent(coreClientClass = IShareFansCoreClient.class)
    public void onShareFansJoin(long uid) {
        AVRoomActivity.start(this, uid);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onTabClick(int position) {
        if (position == 2) {
            //声圈单独统计
            UmengEventUtil.getInstance().onFindMomentsTab(this, UmengEventId.getFindMomentsItem(0));
        } else {
            UmengEventUtil.getInstance().onAppBottomNav(this, position);
        }
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        LogUtil.i("liao", "position=" + position + ",-------" + fragments);
        switchFragment(position);
    }

    private void switchFragment(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment mainFragment = fragmentManager.findFragmentByTag(HomeFragment.TAG);
        Fragment findFragment = fragmentManager.findFragmentByTag(FindFragment.TAG);
        Fragment msgFragment = fragmentManager.findFragmentByTag(MsgFragment.TAG);
        Fragment meFragment = fragmentManager.findFragmentByTag(MeFragment.TAG);
        Fragment voiceFragment = fragmentManager.findFragmentByTag(FindVoiceGroupFragment.TAG);

        //先隐藏所有的
        if (mainFragment != null && mainFragment.isVisible()) {
            fragmentTransaction.hide(mainFragment);
        }
        if (findFragment != null && findFragment.isVisible()) {
            fragmentTransaction.hide(findFragment);
        }
        if (msgFragment != null && msgFragment.isVisible()) {
            fragmentTransaction.hide(msgFragment);
        }
        if (meFragment != null && meFragment.isVisible()) {
            fragmentTransaction.hide(meFragment);
        }
        if (voiceFragment != null && voiceFragment.isVisible()) {
            fragmentTransaction.hide(voiceFragment);
        }

        if (position == 0) {
            if (mainFragment == null) {
                mainFragment = new HomeFragment();
                fragmentTransaction.add(R.id.main_fragment, mainFragment, HomeFragment.TAG);
            }
            fragmentTransaction.show(mainFragment);
        } else if (position == 1) {
            if (findFragment == null) {
                findFragment = new FindFragment();
                fragmentTransaction.add(R.id.main_fragment, findFragment, FindFragment.TAG);
            }
//            else {
//                //需要踩人等东西，所以进入这个界面需要强制刷新
//                if (attentionFragment.isAdded()) {
//                    AttentionFragment temp = (AttentionFragment) attentionFragment;
//                    temp.firstLoadData();
//                }
//            }
            fragmentTransaction.show(findFragment);

        } else if (position == 2) {
            if (voiceFragment == null) {
                voiceFragment = new FindVoiceGroupFragment();
                fragmentTransaction.add(R.id.main_fragment, voiceFragment, FindVoiceGroupFragment.TAG);
            }
            fragmentTransaction.show(voiceFragment);
        } else if (position == 3) {
            if (msgFragment == null) {
                msgFragment = new MsgFragment();
                fragmentTransaction.add(R.id.main_fragment, msgFragment, MsgFragment.TAG);
            }
            fragmentTransaction.show(msgFragment);
        } else if (position == 4) {
            if (meFragment == null) {
                meFragment = new MeFragment();
                fragmentTransaction.add(R.id.main_fragment, meFragment, MeFragment.TAG);
            }
            fragmentTransaction.show(meFragment);
        }
        //commit() 可能会出现java.lang.IllegalStateException  Can not perform this action after onSaveInstanceState
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
        mCurrentMainPosition = position;
    }

    @Override
    public void exitRoom() {
        closeOpenRoomAnimation();
//        DaemonService.stop(MainActivity.this);
    }

    @Override
    public void controlTab(UserInfo userInfo) {
        if (userInfo != null) {
            if (mMainTabLayout != null) {
                //1.4.3版本取消进入发现页面
//                if (CoreManager.getCore(IUserCore.class).isNewUser(userInfo) && userInfo.getChargeNum() <= 0) {//用户处于新手期，且没有充值过，默认发现页面
//                    com.tongdaxing.xchat_framework.util.util.LogUtil.d("controlTab MainTabLayout select 1 用户处于新手期，且没有充值过，默认发现页面");
//                    mMainTabLayout.select(1);
//                } else {
                //用户处于新手期且充值过或者超过新手期，默认房间页面
                mMainTabLayout.select(0);
//                }
            }
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int x = (int) event.getRawX();
        final int y = (int) event.getRawY();
//        Log.d(TAG, "onTouch: x= " + x + "y=" + y);
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:

                mDownTimeMillis = System.currentTimeMillis();
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();


                xDelta = x - params.leftMargin;
                yDelta = y - params.topMargin;
//                Log.d(TAG, "ACTION_DOWN: xDelta= " + xDelta + "yDelta=" + yDelta);
                break;
            case MotionEvent.ACTION_MOVE:

                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();
                int width = layoutParams.width;
                int height = layoutParams.height;


                int xDistance = x - xDelta;
                int yDistance = y - yDelta;


                int outX = (mWidthPixels - width) - 10;
                if (xDistance > outX) {
                    xDistance = outX;
                }

                int outY = mHeightPixels - height - mMainTabHeight;
                if (yDistance > outY) {
                    yDistance = outY;
                }

                if (yDistance < 100) {
                    yDistance = 100;
                }
                if (xDistance < 10) {
                    xDistance = 10;
                }


                layoutParams.leftMargin = xDistance;
                layoutParams.topMargin = yDistance;
                view.setLayoutParams(layoutParams);
                break;
            case MotionEvent.ACTION_UP:
                if (System.currentTimeMillis() - mDownTimeMillis < 150) {
                    RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                    if (roomInfo != null) {
                        AVRoomActivity.start(MainActivity.this, roomInfo.getUid());
                    }
                }
                break;
        }
        mViewGroup.invalidate();
        return true;
    }

    //============================实名认证===============================
    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onNeedRealNameAuthChecked(String msg) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        DialogManager mDialogManager = new DialogManager(MainActivity.this);
        mDialogManager.setCanceledOnClickOutside(false);
        mDialogManager.showOkCancelDialog(msg,
                getResources().getString(R.string.real_name_auth_accept),
                getResources().getString(R.string.cancel),
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        CommonWebViewActivity.start(MainActivity.this, UriProvider.getRealNameAuthUrl());
                    }
                });
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onNeedRealNameAuthChecked(Context context, String msg) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        DialogManager mDialogManager = new DialogManager(context);
        mDialogManager.setCanceledOnClickOutside(false);
        mDialogManager.showOkCancelDialog(msg,
                getResources().getString(R.string.real_name_auth_accept),
                getResources().getString(R.string.cancel),
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        if (context != null) {
                            CommonWebViewActivity.start(context, UriProvider.getRealNameAuthUrl());
                        }
                    }
                });
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onNeedBindPhoneAuthChecked(Context context, String msg) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        DialogManager dialogManager = new DialogManager(context);
        dialogManager.setCanceledOnClickOutside(false);
        dialogManager.showOkCancelDialog(msg,
                getResources().getString(R.string.real_name_bind),
                getResources().getString(R.string.cancel),
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        if (context != null) {
                            context.startActivity(new Intent(context, BinderPhoneActivity.class));
                        }
                    }
                });
    }

    /**
     * 下载文件回调
     *
     * @param hotMusicInfo
     */
    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadCompleted(HotMusicInfo hotMusicInfo) {
        getMvpPresenter().addSongToMyLibrary(hotMusicInfo);
    }

    /**
     * 上传文件成功回调
     *
     * @param musicLocalInfo
     */
    @CoreEvent(coreClientClass = IMusicUploadCoreClient.class)
    public void onHotMusicUploadCompleted(MusicLocalInfo musicLocalInfo) {
        getMvpPresenter().uploadSong(musicLocalInfo, musicLocalInfo.getRemoteUri());
    }

    @Override
    public void onRechargeBtnClick() {
        MyWalletNewActivity.start(getApplicationContext());
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number, int sendGiftType) {
        if (giftInfo != null) {
            List<Long> uids = new ArrayList<>();
            uids.add(uid);
            CoreManager.getCore(IGiftCore.class).doSendPersonalGiftToNIMNew(giftInfo.getGiftId(), uids,
                    number, new WeakReference<>(messageFragment), this, sendGiftType);
        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number, int sendGiftType) {

    }

    @Override
    public void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid) {
        if (giftInfo != null) {
            CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftInfo.getGiftId(), uid,
                    1, -1, this);
        }
    }

    @Override
    public void onNeedCharge() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    MyWalletNewActivity.start(getApplicationContext());
                    fragment.dismiss();
                }
            }
        }).show(messageFragment.getActivity().getSupportFragmentManager(), "charge");
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareH5(String showUrl) {
        toast("分享成功");
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareH5Fail() {
        toast("分享失败，请重试");
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareH5Cancel() {
        toast("取消分享");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionSuccess(PraiseEvent.OnAttentionSuccess success) {
        long uid = success.getUid();
        L.debug(TAG, "onAttentionSuccess uid: %d", uid);
        if (!success.isCheckAttention()) {  // 主动关注就提示，如果是检测是否存在关注关系的，就不提示
            if (success.isAttention()) {
                toast(getString(R.string.fan_success));
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionCancel(PraiseEvent.OnAttentionCancel cancel) {
        toast("已取消关注");
    }


    private PhoneStateListener mPhoneStateListener;

    /**
     * 电话的监听，处理即构来电的时候提示推流和音乐播放
     */
    private void registerPhoneStateListener() {
        mPhoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber);
                switch (state) {
                    case TelephonyManager.CALL_STATE_IDLE:

                        break;
                    case TelephonyManager.CALL_STATE_RINGING:
                        stopZegoPlay();
                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        stopZegoPlay();
                        break;
                }
            }
        };
        TelephonyManager tm = (TelephonyManager) getSystemService(Service.TELEPHONY_SERVICE);
        tm.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    private void stopZegoPlay() {
        if (AvRoomDataManager.get().mCurrentRoomInfo != null && RtcEngineManager.get().getAudioOrganization() == RtcEngineManager.ZEGO) {
            IPlayerCore playerCore = CoreManager.getCore(IPlayerCore.class);
            if (playerCore != null) {
                playerCore.pause();
            }
            RtcEngineManager.get().setMute(true);
        }
    }

}
