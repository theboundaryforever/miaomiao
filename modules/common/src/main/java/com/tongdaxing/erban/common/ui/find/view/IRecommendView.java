package com.tongdaxing.erban.common.ui.find.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.find.bean.RecommendModuleInfo;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public interface IRecommendView extends IMvpBaseView {

    void setupSuccessView(List<VoiceGroupInfo> voiceGroupInfoList, boolean isRefresh);

    void insertRecommendModuleItemsData(List<RecommendModuleInfo> moduleInfoList);

    void insertPopularityStarItemsData(List<UserInfo> userInfoList);

    void setupFailView(boolean isRefresh);

    void refreshAdapterLikeStatus(int commentId);
}
