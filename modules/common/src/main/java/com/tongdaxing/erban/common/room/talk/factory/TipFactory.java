package com.tongdaxing.erban.common.room.talk.factory;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.juxiao.library_ui.widget.EmojiTextView;
import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.room.talk.message.TipMessage;


/**
 * Created by Administrator on 5/31 0031.
 */

public class TipFactory implements AbsBaseViewHolder.ViewHolderFactory {
    @NonNull
    @Override
    public TipViewHolder create(@NonNull ViewGroup parent) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_talk_tip_item, parent, false);
        return new TipViewHolder(v);
    }

    public static class TipViewHolder extends AbsBaseViewHolder<TipMessage> {
        private EmojiTextView mTvContent;

        private TipViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvContent = itemView.findViewById(R.id.talk_iv_tip_content);
        }

        @Override
        public void bind(TipMessage message) {
            super.bind(message);
            if (message != null) {
                String content = message.getContent();
                L.info("TipFactory", "content: %s, type: %d", content, message.getRoomTalkType());
                if (!TextUtils.isEmpty(content)) {
                    mTvContent.setTextColor(mTvContent.getContext().getResources().getColor(R.color.room_tip_color));
                    mTvContent.setText(message.getContent());
                }
            }
        }
    }
}

