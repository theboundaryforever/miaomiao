package com.tongdaxing.erban.common.ui.audiomatch;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.bean.LikeUserGiftInfo;

/**
 * <p> 喜欢页面的礼物adapter </p>
 */
public class LikeGiftAdapter extends BaseQuickAdapter<LikeUserGiftInfo, BaseViewHolder> {
    private Context context;
    private int currGiftSelectedPosition = 0;//当前选中的礼物position

    public LikeGiftAdapter(Context context) {
        super(R.layout.item_like_user_gift);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, LikeUserGiftInfo giftInfo) {
        baseViewHolder.itemView.setSelected(currGiftSelectedPosition == baseViewHolder.getAdapterPosition());
        if (giftInfo.getGiftId() != -1) {
            baseViewHolder.getView(R.id.no_data).setVisibility(View.GONE);
            baseViewHolder.getView(R.id.content).setVisibility(View.VISIBLE);
            ImageView giftIv = baseViewHolder.getView(R.id.gift);
            TextView coinTv = baseViewHolder.getView(R.id.tv_coin);
            ImageLoadUtils.loadImage(context, giftInfo.getPicUrl(), giftIv);
            coinTv.setText(context.getString(R.string.number_of_coin, giftInfo.getGoldPrice()));
        } else {//最后一个"无"
            baseViewHolder.getView(R.id.content).setVisibility(View.GONE);
            baseViewHolder.getView(R.id.no_data).setVisibility(View.VISIBLE);
        }
    }

    public void setGift(int position) {
        currGiftSelectedPosition = position;
        notifyDataSetChanged();
    }

    public LikeUserGiftInfo getSelectedGift() {
        return getItem(currGiftSelectedPosition);
    }
}
