package com.tongdaxing.erban.common.presenter.makefriedns;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.List;

public interface IPopularStarView extends IMvpBaseView {

    void setupFailView(boolean isRefresh);

    void setupSuccessView(List<UserInfo> popularStarList, boolean isRefresh);

    void setupFindHerFail(String msg);

    void toHerRoom(long uid);

    void toUserInfoPage(long uid);
}
