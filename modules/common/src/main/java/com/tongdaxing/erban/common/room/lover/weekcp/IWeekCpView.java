package com.tongdaxing.erban.common.room.lover.weekcp;

/**
 * Created by Chen on 2019/4/30.
 */
public interface IWeekCpView {
    void updateUI(int value);

    void showView(boolean isOnMic);

    void resetUI();

    void showTacitTestView();

    void showNotCpOnMic();

    void showTip();

    void dismissTacitView();

    void showMeHasCp(String nick);

    void showWeekCpDes();

    void showReceiveView(String cpId);

    void showResultTestView(String questionId);
}
