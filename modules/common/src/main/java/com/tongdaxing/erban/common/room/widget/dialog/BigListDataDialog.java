package com.tongdaxing.erban.common.room.widget.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.common.room.avroom.adapter.RoomConsumeListAdapter;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.erban.common.ui.widget.ButtonPayList;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.tongdaxing.xchat_core.UriProvider.JAVA_WEB_URL;

/**
 * Created by MadisonRong on 13/01/2018.
 */

public class BigListDataDialog extends BaseDialogFragment implements View.OnClickListener, BaseQuickAdapter.OnItemClickListener {
    public static final String TYPE_ONLINE_USER = "ONLINE_USER";
    public static final String TYPE_CONTRIBUTION = "ROOM_CONTRIBUTION";
    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_TYPE = "KEY_TYPE";
    @BindView(R2.id.bu_pay_tab)
    TextView buPayTab;
    @BindView(R2.id.bu_imcome_tab)
    TextView buImcomeTab;
    @BindView(R2.id.iv_close_dialog)
    ImageView ivCloseDialog;
    @BindView(R2.id.bu_tab_day)
    ButtonPayList buTabDay;
    @BindView(R2.id.bu_tab_week)
    ButtonPayList buTabWeek;
    @BindView(R2.id.bu_tab_all)
    ButtonPayList buTabAll;
    @BindView(R2.id.rv_pay_income_list)
    RecyclerView rvPayIncomeList;
    Unbinder unbinder;
    private String TAG = "BigListDataDialog";
    private int type = 1;
    private int dataType = 1;
    private long roomId;
    private RoomConsumeListAdapter roomConsumeListAdapter;
    private View noDataView;
    private RoomInfo roomInfo;
    private SelectOptionAction selectOptionAction;

    public BigListDataDialog() {
    }

    public static BigListDataDialog newContributionListInstance(Context context) {
        return newInstance(context.getString(R.string.contribution_list_text), TYPE_CONTRIBUTION);
    }

    public static BigListDataDialog newInstance(String title, String type) {
        BigListDataDialog listDataDialog = new BigListDataDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE, title);
        bundle.putString(KEY_TYPE, type);
        listDataDialog.setArguments(bundle);
        return listDataDialog;
    }

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, null);
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_big_list_data, window.findViewById(android.R.id.content), false);
        unbinder = ButterKnife.bind(this, view);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);

        roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null)
            roomId = roomInfo.getUid();
        view.findViewById(R.id.iv_close_dialog).setOnClickListener(this);
        noDataView = view.findViewById(R.id.tv_no_data);
        unbinder = ButterKnife.bind(this, view);
        initView();
        initRv();
        return view;
    }

    private void initRv() {
        rvPayIncomeList.setLayoutManager(new LinearLayoutManager(getContext()));
        roomConsumeListAdapter = new RoomConsumeListAdapter(getContext());
        roomConsumeListAdapter.setOnItemClickListener(this);
        rvPayIncomeList.setAdapter(roomConsumeListAdapter);
        getData();
    }

    private void initView() {

        buTabDay.getTabName().setText("日榜");
        buTabWeek.getTabName().setText("周榜");
        buTabAll.getTabName().setText("总榜");

        buTabDay.getLine().setVisibility(View.VISIBLE);
        buTabDay.getTabName().setTextColor(getResources().getColor(R.color.room_rank_tab_selected));
        buTabWeek.getTabName().setTextColor(getResources().getColor(R.color.room_rank_tab_un_selected));
        buTabAll.getTabName().setTextColor(getResources().getColor(R.color.room_rank_tab_un_selected));

        buPayTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeChange(1);
            }
        });
        buImcomeTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeChange(2);
            }
        });
        buTabDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataTypeChange(1);
            }
        });
        buTabWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataTypeChange(2);
            }
        });
        buTabAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataTypeChange(3);
            }
        });
    }

    private void dataTypeChange(int i) {
        if (i == dataType) {
            return;
        }
        dataType = i;
        buTabDay.getLine().setVisibility(i == 1 ? View.VISIBLE : View.INVISIBLE);
        buTabWeek.getLine().setVisibility(i == 2 ? View.VISIBLE : View.INVISIBLE);
        buTabAll.getLine().setVisibility(i == 3 ? View.VISIBLE : View.INVISIBLE);
        int selectedColor = getResources().getColor(R.color.room_rank_tab_selected);
        int unSelectedColor = getResources().getColor(R.color.room_rank_tab_un_selected);
        buTabDay.getTabName().setTextColor(i == 1 ? selectedColor : unSelectedColor);
        buTabWeek.getTabName().setTextColor(i == 2 ? selectedColor : unSelectedColor);
        buTabAll.getTabName().setTextColor(i == 3 ? selectedColor : unSelectedColor);
        if (selectOptionAction != null) {
            selectOptionAction.optionClick();
        }
        getData();
    }

    private void getData() {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(roomId));
        requestParam.put("dataType", String.valueOf(dataType));
        requestParam.put("type", String.valueOf(type));
        String url = JAVA_WEB_URL + "/roomctrb/queryByType";
        L.info(TAG, "getData, type: %d, roomId: %d, url: %s, dataType: %d", type, roomId, url, dataType);
        OkHttpManager.getInstance().doGetRequest(url, requestParam, new OkHttpManager.MyCallBack<ServiceResult<List<RoomConsumeInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (selectOptionAction != null)
                    selectOptionAction.onDataResponse();
            }

            @Override
            public void onResponse(ServiceResult<List<RoomConsumeInfo>> response) {
                L.info(TAG, "getData, onResponse: response: %s", response);
                if (selectOptionAction != null)
                    selectOptionAction.onDataResponse();
                if (response.isSuccess()) {

                    if (response.getData() != null) {
                        if (noDataView != null) {
                            noDataView.setVisibility(response.getData().size() > 0 ? View.GONE : View.VISIBLE);
                        }
                        if (roomConsumeListAdapter != null) {
                            L.info(TAG, "getData, onResponse: data: %s", response.getData());
                            roomConsumeListAdapter.setNewData(response.getData());
                        }
                    }
                }
            }
        });
    }

    private void typeChange(int i) {
        if (i == type) {
            return;
        }

        if (roomConsumeListAdapter != null) {
            if (i == 1) {
                roomConsumeListAdapter.rankType = 0;
            } else {
                roomConsumeListAdapter.rankType = 1;
            }
        }


        dataType = 1;
        type = i;

        buTabDay.getLine().setVisibility(dataType == 1 ? View.VISIBLE : View.INVISIBLE);
        buTabWeek.getLine().setVisibility(dataType == 2 ? View.VISIBLE : View.INVISIBLE);
        buTabAll.getLine().setVisibility(dataType == 3 ? View.VISIBLE : View.INVISIBLE);

        buPayTab.setTextColor(i == 1 ? getResources().getColor(R.color.white) : getResources().getColor(R.color.room_rank_un_selected));
        buImcomeTab.setTextColor(i == 2 ? getResources().getColor(R.color.white) : getResources().getColor(R.color.room_rank_un_selected));

        buPayTab.setTextSize(i == 1 ? 20.0f : 18.0f);
        buImcomeTab.setTextSize(i == 2 ? 20.0f : 18.0f);
        getData();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_close_dialog) {
            dismiss();

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        List<RoomConsumeInfo> list = roomConsumeListAdapter.getData();
        if (ListUtils.isListEmpty(list)) {
            return;
        }
        RoomConsumeInfo roomConsumeInfo = list.get(i);
        UserInfoDialogManager.showDialogFragment(getContext(), roomConsumeInfo.getCtrbUid());
    }

    public void setSelectOptionAction(SelectOptionAction selectOptionAction) {
        this.selectOptionAction = selectOptionAction;
    }

    public interface SelectOptionAction {
        void optionClick();

        void onDataResponse();
    }

}
