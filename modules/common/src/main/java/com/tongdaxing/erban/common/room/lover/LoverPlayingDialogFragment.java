package com.tongdaxing.erban.common.room.lover;

import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.erban.ui.baseview.BaseDialogFragment;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.uuzuche.lib_zxing.DisplayUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chen on 2019/5/5.
 */
@Route(path = RoomLoverRoute.ROOM_LOVER_PLAYING_PATH)
public class LoverPlayingDialogFragment extends BaseDialogFragment {

    @BindView(R2.id.room_lover_iv_close)
    ImageView mIvClose;

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null && getContext() != null) {
            // 这里用透明颜色替换掉系统自带背景
            int color = ContextCompat.getColor(getContext(), android.R.color.transparent);
            window.setBackgroundDrawable(new ColorDrawable(color));
            window.setLayout(DisplayUtil.dip2px(getContext(), 270), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_lover_dialog_fragment_playing;
    }

    @Override
    public void initBefore() {

    }

    @Override
    public void findView() {
        ButterKnife.bind(this, mContentView);
    }

    @Override
    public void setView() {

    }

    @Override
    public void setListener() {

    }

    @OnClick(R2.id.room_lover_iv_close)
    public void onViewClicked() {
        dismissAllowingStateLoss();
    }
}
