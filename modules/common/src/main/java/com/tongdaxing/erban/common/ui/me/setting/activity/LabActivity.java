package com.tongdaxing.erban.common.ui.me.setting.activity;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.pref.CommonPref;

/**
 * Created by chenran on 2017/10/16.
 */
public class LabActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab);

        int environment = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("environment", 1);
        //根据ID找到RadioGroup实例
        RadioGroup group = (RadioGroup) this.findViewById(R.id.radioGroup);
        RadioButton button0 = (RadioButton) findViewById(R.id.product);
        RadioButton button1 = (RadioButton) findViewById(R.id.test);
        RadioButton button2 = (RadioButton) findViewById(R.id.product18);
        if (environment == 0) {
            button0.setChecked(true);
        } else if (environment == 2) {
            button2.setChecked(true);
        } else {
            button1.setChecked(true);//默认勾选测试环境
        }
        //绑定一个匿名监听器
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
                //  Auto-generated method stub
                //获取变更后的选中项的ID
//                if (arg1 == R.id.product) {
//                    CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("environment", 0);
//                } else if (arg1 == R.id.product18) {
//                    CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("environment", 2);
//                } else {
//                    CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("environment", 1);
//                }
//                Env.instance().init();
//                XChatApplication.initRxNet(BasicConfig.INSTANCE.getAppContext(), UriProvider.JAVA_WEB_URL);
//                CoreManager.getCore(IAuthCore.class).logout();
//                finish();
            }
        });
    }
}
