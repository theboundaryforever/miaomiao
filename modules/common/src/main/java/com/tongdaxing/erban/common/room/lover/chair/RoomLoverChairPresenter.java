package com.tongdaxing.erban.common.room.lover.chair;

import android.util.SparseArray;

import com.erban.ui.mvp.BasePresenter;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.agora.LiveEvent;
import com.tongdaxing.xchat_core.room.RoomAction;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.RoomFaceEvent;
import com.tongdaxing.xchat_core.room.model.HomePartyModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Chen on 2019/4/26.
 */
public class RoomLoverChairPresenter extends BasePresenter<IRoomLoverChairView> {

    public final int mCurrentOwnerPositionInQueue = -1;      // 云信回来的座位列表，房主在第一个，但是在我们定义上下麦的座位位置在-1，也就是map的key为-1
    public final int mLoverPositionInQueue = 0;              // 云信回来的座位列表，情侣位置在第二个，但是在我们定义上下麦的座位位置在0，也就是map的key为0
    private String TAG = "RoomLoverChairPresenter";
    private HomePartyModel mHomePartyModel;
    /**
     * 判断所坑服务端是否响应回来了
     */
    private boolean mIsLockMicPosResultSuccess = true;
    private boolean mIsUnLockMicPosResultSuccess = true;


    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        if (getView() == null) {
            return;
        }
        mHomePartyModel = new HomePartyModel();
        updateChairView();
    }

    public SparseArray<RoomQueueInfo> getRoomQueueInfo() {
        return AvRoomDataManager.get().mMicQueueMemberMap;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateChairView(RoomAction.OnUpdateChairView updateChairView) {
        L.debug(TAG, "onUpdateChairView");
        updateChairView();
    }

    private void updateChairView() {
        if (getView() == null) {
            return;
        }
        SparseArray<RoomQueueInfo> queueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        getView().updateChairView(queueMemberMap);
    }

    public void microPhonePositionClick(final int micPosition) {
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null || getView() == null) {
            return;
        }
        final String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null || roomQueueInfo.mRoomMicInfo == null) {
            L.error(TAG, "microPhonePositionClick roomQueueInfo or roomQueueInfo.mRoomMicInfo is null.");
            return;
        }

        boolean isRoomOwner = AvRoomDataManager.get().isRoomOwner(currentUid);
        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;

        // 点击的是房主位置
        if (micPosition == mCurrentOwnerPositionInQueue) {
            if (chatRoomMember == null) {
                chatRoomMember = new ChatRoomMember();
                RoomInfo info = AvRoomDataManager.get().mCurrentRoomInfo;
                chatRoomMember.setNick("房主");
                chatRoomMember.setAvatar("");
                chatRoomMember.setAccount(info.getUid() + "");
            }

            // 自己是房主
            if (isRoomOwner) {
                getView().showOwnerSelfInfo(chatRoomMember);
            } else {
                getView().showGiftDialog(chatRoomMember);
            }
        } else {
            if (isRoomOwner) {
                if (chatRoomMember != null) {
                    avatarClick(micPosition);
                } else {
                    onOwnerUpMicroClick(micPosition, currentRoom.getUid());
                }
            } else {
                if (chatRoomMember != null) {
                    boolean isMySelf = Objects.equals(currentUid, chatRoomMember.getAccount());
                    if (isMySelf) {
                        List<ButtonItem> buttonItems = new ArrayList<>();
                        SparseArray<ButtonItem> avatarButtonItemMap = getView().getAvatarButtonItemList(micPosition,
                                chatRoomMember, currentRoom);
                        buttonItems.add(avatarButtonItemMap.get(4));
                        buttonItems.add(avatarButtonItemMap.get(5));
                        getView().showMicAvatarClickDialog(buttonItems);
                    } else {
                        getView().showGiftDialog(chatRoomMember);
                    }
                } else {
                    upMicroPhone(micPosition, currentUid, false);
                }
            }
        }
    }

    public void avatarClick(int micPosition) {
        if (getView() == null) {
            return;
        }
//        LogUtils.d("avatarClick", "micPosition:" + micPosition);
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) return;
        // 判断在不在麦上
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null) return;
        // 麦上的人员信息,麦上的坑位信息

        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
        RoomMicInfo roomMicInfo = roomQueueInfo.mRoomMicInfo;


        if (chatRoomMember == null && micPosition == -1) {
            chatRoomMember = new ChatRoomMember();
            RoomInfo info = AvRoomDataManager.get().mCurrentRoomInfo;
            chatRoomMember.setNick("房主");
            chatRoomMember.setAvatar("");
            chatRoomMember.setAccount(info.getUid() + "");


        }
        if (chatRoomMember == null || roomMicInfo == null) return;

        String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        boolean isMySelf = Objects.equals(currentUid, chatRoomMember.getAccount());
        boolean isTargetRoomAdmin = AvRoomDataManager.get().isRoomAdmin(chatRoomMember.getAccount());
        boolean isTargetRoomOwner = AvRoomDataManager.get().isRoomOwner(chatRoomMember.getAccount());


        List<ButtonItem> buttonItems = new ArrayList<>();
        SparseArray<ButtonItem> avatarButtonItemMap = getView().getAvatarButtonItemList(micPosition,
                chatRoomMember, currentRoom);
        if (AvRoomDataManager.get().isRoomOwner()) {
            //房主操作
            if (!isMySelf) {
                //点击不是自己
                buttonItems.add(avatarButtonItemMap.get(0));
                buttonItems.add(avatarButtonItemMap.get(4));
                buttonItems.add(avatarButtonItemMap.get(2));
                if (roomMicInfo.isMicMute()) {
                    buttonItems.add(avatarButtonItemMap.get(6));
                } else {
                    buttonItems.add(avatarButtonItemMap.get(1));
                }
                buttonItems.add(avatarButtonItemMap.get(3));

                buttonItems.add(avatarButtonItemMap.get(7));
                getView().showMicAvatarClickDialog(buttonItems);
            } else {
                getView().showOwnerSelfInfo(chatRoomMember);
            }

        } else if (AvRoomDataManager.get().isRoomAdmin()) {
            //管理员操作
            if (isMySelf) {
                //点击自己
                //查看资料
                //下麦旁听
                // 禁麦此座位/解禁此座位
                buttonItems.add(avatarButtonItemMap.get(4));
                buttonItems.add(avatarButtonItemMap.get(5));
                if (roomMicInfo.isMicMute()) {
                    buttonItems.add(avatarButtonItemMap.get(6));
                } else {
                    buttonItems.add(avatarButtonItemMap.get(1));
                }
            } else {


                //送礼物
                buttonItems.add(avatarButtonItemMap.get(0));
                //查看资料
                buttonItems.add(avatarButtonItemMap.get(4));


                if (!isTargetRoomAdmin && !isTargetRoomOwner) {
                    //非房主或管理员
                    //抱他下麦
                    buttonItems.add(avatarButtonItemMap.get(2));
                    //禁麦/解麦操作
                    buttonItems.add(roomMicInfo.isMicMute() ?
                            avatarButtonItemMap.get(6) :
                            avatarButtonItemMap.get(1));
                    //踢出房间
                    buttonItems.add(avatarButtonItemMap.get(3));
                    //加入黑名单
                    buttonItems.add(avatarButtonItemMap.get(7));
                } else {


                    if (!roomMicInfo.isMicMute() && !isTargetRoomOwner) {
                        buttonItems.add(avatarButtonItemMap.get(1));
                    }


                    if (!isTargetRoomOwner)
                        buttonItems.add(avatarButtonItemMap.get(2));
                    // 对于管理员和房主,只有解麦功能
                    if (roomMicInfo.isMicMute())
                        buttonItems.add(avatarButtonItemMap.get(6));
                }
            }
            getView().showMicAvatarClickDialog(buttonItems);
        } else {
            //游客操作
            if (isMySelf) {
                //查看资料
                //下麦旁听
                buttonItems.add(avatarButtonItemMap.get(4));
                buttonItems.add(avatarButtonItemMap.get(5));
                getView().showMicAvatarClickDialog(buttonItems);
            } else {
                buttonItems.add(avatarButtonItemMap.get(0));
                buttonItems.add(avatarButtonItemMap.get(4));

                if (micPosition == -1 && roomQueueInfo.mChatRoomMember == null) {
                    getView().showGiftDialog(chatRoomMember);
                } else {
                    getView().showGiftDialog(chatRoomMember);
                }

            }
        }
    }

    private void onOwnerUpMicroClick(final int micPosition, final long currentUid) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null || getView() == null) {
            return;
        }

        getView().showOwnerClickDialog(roomQueueInfo.mRoomMicInfo, micPosition, currentUid);
    }

    public void upMicroPhone(final int micPosition, final String uId, boolean isInviteUpMic) {
        upMicroPhone(micPosition, uId, isInviteUpMic, false);
    }

    /**
     * 上麦
     *
     * @param micPosition
     * @param uId
     * @param isInviteUpMic 是否是主动的，true：主动
     */
    public void upMicroPhone(final int micPosition, final String uId, boolean isInviteUpMic, boolean needCloseMic) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null || mHomePartyModel == null) {
            return;
        }
        mHomePartyModel.upMicroPhone(micPosition, uId, String.valueOf(roomInfo.getRoomId()),
                isInviteUpMic, new CallBack<String>() {
                    @Override
                    public void onSuccess(String data) {
                        if (micPosition == -1) {
                            if (getView() != null) {
                                getView().updateChairView(AvRoomDataManager.get().mMicQueueMemberMap);
                            }
                        }
                        L.info(TAG, "upMicroPhone onSuccess uid: %s, data: %s", uId, data);
                    }

                    @Override
                    public void onFail(int code, String error) {
                        L.error(TAG, "upMicroPhone onFail uid: %s, data: %s, code: %d", uId, error, code);
                    }
                }, needCloseMic);
    }

    /**
     * 开麦
     *
     * @param micPosition
     */
    public void openMicroPhone(int micPosition) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }

        ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
            @Override
            public void onResponse(ServiceResult<String> data) {
                if (data == null) {
                    return;
                }
                L.info(TAG, "openMicroPhone onResponse uid: %d, data: %s", roomInfo.getUid(), data);
            }

        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {//解麦失败
                L.error(TAG, "openMicroPhone onErrorResponse ", error);
            }
        };
        mHomePartyModel.openMicroPhone(micPosition, roomInfo.getUid(), listener, errorListener);
    }

    /**
     * 闭麦
     *
     * @param micPosition
     */
    public void closeMicroPhone(int micPosition) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
            @Override
            public void onResponse(ServiceResult<String> data) {
                if (data == null) {
                    return;
                }
                L.info(TAG, "openMicroPhone onResponse uid: %d, data: %s", roomInfo.getUid(), data);
            }

        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {//解麦失败
                L.error(TAG, "openMicroPhone onErrorResponse ", error);
            }
        };
        mHomePartyModel.closeMicroPhone(micPosition, roomInfo.getUid(), listener, errorListener);
    }

    /**
     * 坑位释放锁
     */
    public void unLockMicroPhone(int micPosition) {
        if (!mIsUnLockMicPosResultSuccess) {
            return;
        }
        mIsUnLockMicPosResultSuccess = false;
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        final String currentUid = String.valueOf(currentRoom.getUid());
        if (AvRoomDataManager.get().isRoomAdmin() || AvRoomDataManager.get().isRoomOwner(currentUid)) {
            ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
                @Override
                public void onResponse(ServiceResult<String> data) {
                    if (null != data && data.isSuccess()) {//解麦成功
                        mIsUnLockMicPosResultSuccess = true;
                    } else {//解麦失败
                        mIsUnLockMicPosResultSuccess = true;
                    }
                }

            };
            ResponseErrorListener errorListener = new ResponseErrorListener() {
                @Override
                public void onErrorResponse(RequestError error) {//解麦失败
                    mIsUnLockMicPosResultSuccess = true;
                }
            };
            mHomePartyModel.unLockMicroPhone(micPosition, currentUid, CoreManager.getCore(IAuthCore.class).getTicket(), listener, errorListener);
        }
    }

    public void lockMicroPhone(int micPosition, final long currentUid) {
        if (!mIsLockMicPosResultSuccess) {
            return;
        }
        mIsLockMicPosResultSuccess = false;
        ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
            @Override
            public void onResponse(ServiceResult<String> data) {
                mIsLockMicPosResultSuccess = true;
                if (data == null) {
                    return;
                }
                L.info(TAG, "openMicroPhone onResponse uid: %d, data: %s", currentUid, data);
            }

        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {//解麦失败
                L.error(TAG, "openMicroPhone onErrorResponse ", error);
                mIsLockMicPosResultSuccess = true;
            }
        };
        mHomePartyModel.lockMicroPhone(micPosition, String.valueOf(currentUid),
                CoreManager.getCore(IAuthCore.class).getTicket(), listener, errorListener);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlayerSpeak(LiveEvent.OnSpeakerVolume speakerVolume) {
        if (getView() == null) {
            return;
        }
        getView().updateSpeakState(speakerVolume.getPos(), speakerVolume.hasSound());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveFace(RoomFaceEvent.OnReceiveFace face) {
        L.debug(TAG, "function:sendFace RoomLoverChairPresenter onReceiveFace faceReceiveInfos: %s", face.getReceiveInfos());
        if (getView() == null) {
            return;
        }
        getView().onReceiveFace(face.getReceiveInfos());
    }
}
