package com.tongdaxing.erban.common.room.audio.presenter;

import com.tongdaxing.erban.common.room.audio.view.ISearchMusicView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public class SearchMusicPresenter extends AbstractMvpPresenter<ISearchMusicView> {
    private int currPage = Constants.PAGE_START;

    /**
     * 刷新数据
     */
    public void refreshData(String searchContent) {
        getSearchResult(Constants.PAGE_START, searchContent);
    }

    public void loadMoreData(String searchContent) {
        getSearchResult(currPage + 1, searchContent);
    }

    public void getSearchResult(int page, String searchStr) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageSize", String.valueOf(Constants.PAGE_SIZE));
        params.put("pageNum", String.valueOf(page));
        params.put("key", searchStr);//主要用来搜索
        OkHttpManager.getInstance().doGetRequest(UriProvider.getHotSongList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<MyMusicInfo>>>() {
            @Override
            public void onError(Exception e) {
                getMvpView().loadDataFailure(page == Constants.PAGE_START, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<MyMusicInfo>> response) {
                currPage = page;
                if (response != null && response.isSuccess() && response.getData() != null) {
                    if (currPage == Constants.PAGE_START) {
                        getMvpView().refreshMyMusicList(true, response.getData());
                    } else {
                        getMvpView().refreshMyMusicList(false, response.getData());
                    }
                }
            }
        });
    }
}
