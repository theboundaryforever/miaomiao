package com.tongdaxing.erban.common.ui.find.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * Function:
 * Author: Edward on 2019/3/18
 */
public interface INotCustomLabelView extends IMvpBaseView {
    void delLabelSucceed(int tagId);

    int getLabelId();

    void setDefaultSelected(int tagId);
}
