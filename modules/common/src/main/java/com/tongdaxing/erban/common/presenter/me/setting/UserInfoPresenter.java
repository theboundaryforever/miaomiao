package com.tongdaxing.erban.common.presenter.me.setting;

import com.tongdaxing.erban.common.ui.me.user.view.IUserInfoView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * Function:
 * Author: Edward on 2019/3/26
 */
public class UserInfoPresenter extends AbstractMvpPresenter<IUserInfoView> {
    public boolean isMySelf(long userId) {
        if (userId == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
            return true;
        } else {
            return false;
        }
    }
}
