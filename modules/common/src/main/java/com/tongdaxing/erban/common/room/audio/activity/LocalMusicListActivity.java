package com.tongdaxing.erban.common.room.audio.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.room.audio.adapter.LocalMusicListAdapter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by chenran on 2017/10/28.
 */
@Deprecated
public class LocalMusicListActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R2.id.image_bg)
    ImageView imageBg;
    @BindView(R2.id.transparent_view)
    View transparentView;
    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.refresh_btn)
    ImageView refreshBtn;
    @BindView(R2.id.title_layout)
    RelativeLayout titleLayout;
    @BindView(R2.id.empty_layout_music_add)
    ImageView emptyLayoutMusicAdd;
    @BindView(R2.id.empty_bg)
    LinearLayout emptyBg;
    @BindView(R2.id.recycler_view)
    RecyclerView recyclerView;
    private String imgBgUrl;
    private LocalMusicListAdapter adapter;

    public static void start(Context context, String imgBgUrl) {
        Intent intent = new Intent(context, LocalMusicListActivity.class);
        intent.putExtra("imgBgUrl", imgBgUrl);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_music_list);
        ButterKnife.bind(this);
        imgBgUrl = getIntent().getStringExtra("imgBgUrl");
        initView();
        initData();

//        if (!StringUtil.isEmpty(imgBgUrl)) {
//            ImageLoadUtils.loadImageWithBlurTransformation(this, imgBgUrl, imageBg);
//        }

        if (CoreManager.getCore(IPlayerCore.class).isRefresh()) {
            playFlagRotateAnim();
        }
//        View content = findViewById(android.R.id.content);
//        ViewGroup.LayoutParams params = content.getLayoutParams();
//        params.height = getResources().getDisplayMetrics().heightPixels;
    }

    @Override
    public boolean blackStatusBar() {
        return false;
    }

    private void initView() {
    }

    private void initData() {
        List<MusicLocalInfo> localMusicInfoList = CoreManager.getCore(IPlayerCore.class).requestLocalMusicInfos();
        adapter = new LocalMusicListAdapter(this);
        adapter.setLocalMusicInfos(localMusicInfoList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (localMusicInfoList == null || localMusicInfoList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyBg.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyBg.setVisibility(View.GONE);
        }
    }

    private void playFlagRotateAnim() {
        Animation operatingAnim = AnimationUtils.loadAnimation(this, R.anim.rotate_fast_anim);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
        refreshBtn.startAnimation(operatingAnim);
    }

    private void stopFlagRotateAnim() {
        refreshBtn.clearAnimation();
    }

    @Override
    protected boolean needSteepStateBar() {
        return false;
    }

    @OnClick({R2.id.refresh_btn, R2.id.back_btn, R2.id.empty_layout_music_add})
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.refresh_btn) {
            if (!CoreManager.getCore(IPlayerCore.class).isRefresh()) {
                playFlagRotateAnim();
                toast("开始扫描...");
                CoreManager.getCore(IPlayerCore.class).refreshLocalMusic(null);
            } else {
                toast("正在扫描，请稍后...");
            }
        } else if (i == R.id.back_btn) {
            finish();
        } else if (i == R.id.empty_layout_music_add) {
            if (!CoreManager.getCore(IPlayerCore.class).isRefresh()) {
                playFlagRotateAnim();
                toast("开始扫描...");
                CoreManager.getCore(IPlayerCore.class).refreshLocalMusic(null);
            } else {
                toast("正在扫描，请稍后...");
            }
        } else {
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onRefreshLocalMusic(List<MusicLocalInfo> localMusicInfoList) {
        stopFlagRotateAnim();
        toast(ListUtils.isListEmpty(localMusicInfoList) ? "扫描完成，暂未发现歌曲" : "扫描完成");
        if (localMusicInfoList == null || localMusicInfoList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyBg.setVisibility(View.VISIBLE);
            adapter.setLocalMusicInfos(null);
            adapter.notifyDataSetChanged();
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyBg.setVisibility(View.GONE);
            adapter.setLocalMusicInfos(localMusicInfoList);
            adapter.notifyDataSetChanged();
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onBeKickOut(ChatRoomKickOutEvent.ChatRoomKickOutReason reason) {
        finish();
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }
}
