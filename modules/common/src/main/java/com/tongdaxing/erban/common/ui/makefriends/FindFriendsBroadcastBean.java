package com.tongdaxing.erban.common.ui.makefriends;

/**
 * 寻友广播实体类
 * <p>
 * Created by zhangjian on 2019/6/20.
 */
public class FindFriendsBroadcastBean {
    /**
     * uid : 109975
     * avatar : https://pic.miaomiaofm.com/Fmrt5mggKtynNhwxmqE4kR75y6Uf?imageslim
     * tagId : 101
     * roomTag : 一周CP
     * tagPict :
     * title : 偏偏
     * content : qasdadsadsadasdsadassda
     * onlineNum : 10
     * roomUid : 109975
     * roomId : 106906909
     */

    private int uid;
    private String avatar;//房主头像
    private String tagId;
    private String roomTag;
    private String tagPict;
    private String userNick;//房主昵称
    private String content;//发布内容
    private int onlineNum;
    private int roomUid;//房主ID
    private int roomId;//房间ID

    private boolean isBlueBg = true;//默认蓝色

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public int getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(int roomUid) {
        this.roomUid = roomUid;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public boolean isBlueBg() {
        return isBlueBg;
    }

    public void setBlueBg(boolean blueBg) {
        isBlueBg = blueBg;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }
}
