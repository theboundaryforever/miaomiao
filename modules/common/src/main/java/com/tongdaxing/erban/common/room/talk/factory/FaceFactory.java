package com.tongdaxing.erban.common.room.talk.factory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.common.ui.widget.marqueeview.Utils;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.view.UrlImageSpan;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.widget.CpSignImageSpan;
import com.tongdaxing.xchat_core.im.custom.bean.FaceAttachment;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.talk.message.FaceMessage;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.uuzuche.lib_zxing.DisplayUtil;

import java.util.List;


/**
 * Created by Administrator on 5/31 0031.
 */

public class FaceFactory implements AbsBaseViewHolder.ViewHolderFactory {
    @NonNull
    @Override
    public FaceViewHolder create(@NonNull ViewGroup parent) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_talk_face_item, parent, false);
        return new FaceViewHolder(v);
    }

    public static class FaceViewHolder extends AbsBaseViewHolder<FaceMessage> {
        private LinearLayout mLlFaceContainer;

        private FaceViewHolder(@NonNull View itemView) {
            super(itemView);
            mLlFaceContainer = itemView.findViewById(R.id.talk_ll_face_container);
            mLlFaceContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatRoomMessage chatRoomMessage = (ChatRoomMessage) v.getTag();
                    CoreUtils.send(new RoomTalkEvent.OnRoomMsgItemClicked(chatRoomMessage));
                }
            });
        }

        @Override
        public void bind(FaceMessage message) {
            super.bind(message);
            setMsgFace(message.getChatRoomMessage());
        }

        private void setMsgFace(ChatRoomMessage chatRoomMessage) {
            Context context = mLlFaceContainer.getContext();
            TextView iconView = getIconView(chatRoomMessage, context);

            FaceAttachment faceAttachment = (FaceAttachment) chatRoomMessage.getAttachment();

            mLlFaceContainer.removeAllViews();
            mLlFaceContainer.setTag(chatRoomMessage);

            List<FaceReceiveInfo> faceReceiveInfos = faceAttachment.getFaceReceiveInfos();
            for (int i = 0; i < faceReceiveInfos.size(); i++) {
                FaceReceiveInfo faceReceiveInfo = faceReceiveInfos.get(i);
                FaceInfo faceInfo = CoreManager.getCore(IFaceCore.class).findFaceInfoById(faceReceiveInfo.getFaceId());
                if (faceReceiveInfo.getResultIndexes().size() <= 0 ||
                        faceInfo == null) {
                    continue;
                }

                LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.CENTER_VERTICAL;


                LinearLayout.LayoutParams textViewLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                textViewLayoutParams.gravity = Gravity.CENTER_VERTICAL;

                mLlFaceContainer.addView(linearLayout);
                linearLayout.setLayoutParams(layoutParams);
                TextView textView = new TextView(context);
                textView.setTextColor(context.getResources().getColor(R.color.white));
                String nick = faceReceiveInfo.getNick();
                if (StringUtil.isEmpty(nick)) {
                    nick = "";
                }
                int margin = Utils.dip2px(context, 5);
                SpannableStringBuilder builder = new SpannableStringBuilder(nick);
                textView.setTextSize(13.0f);
                textView.setText(builder);

                textView.setLayoutParams(textViewLayoutParams);
                linearLayout.addView(textView);

                if (iconView != null) {
                    linearLayout.addView(iconView);
                }

                List<Integer> resultIndexes = faceReceiveInfo.getResultIndexes();
                LinearLayout llmLlFaceContainer = new LinearLayout(context);
                llmLlFaceContainer.setOrientation(LinearLayout.HORIZONTAL);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER_VERTICAL;
                int llMargin = Utils.dip2px(context, 2);
                params.setMargins(0, llMargin, 0, 0);
                llmLlFaceContainer.setLayoutParams(layoutParams);
                llmLlFaceContainer.setBackground(context.getResources().getDrawable(R.drawable.room_talk_content_bg_shape));
                for (Integer index : resultIndexes) {
                    ImageView imageView = new ImageView(context);
                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    LinearLayout.LayoutParams imgLayoutParams = new LinearLayout.LayoutParams(UIUtil.dip2px(context, 30), UIUtil.dip2px(context, 30));
                    imgLayoutParams.gravity = Gravity.CENTER_VERTICAL;
                    if (faceInfo.getId() == 17 && resultIndexes.size() > 1) {
                        // 骰子
                        imgLayoutParams = new LinearLayout.LayoutParams(UIUtil.dip2px(context, 22), UIUtil.dip2px(context, 22));
                        imgLayoutParams.setMargins(0, margin, margin, margin);
                        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    } else if (faceInfo.getId() == 23) {
                        // 纸牌
                        imageView.setScaleType(ImageView.ScaleType.FIT_START);
                        imgLayoutParams.setMargins(0, margin, 0, margin);
                    }
                    imageView.setLayoutParams(imgLayoutParams);
                    ImageLoadUtils.loadImage(context, faceInfo.getFacePath(index), imageView);
                    llmLlFaceContainer.addView(imageView);
                }
                linearLayout.addView(llmLlFaceContainer);
            }
        }

        private TextView getIconView(ChatRoomMessage chatRoomMessage, Context context) {
            int experLevel = -1;
            int experCharm = -1;
            String newUserContent = "";
            String cpSign = null;
            String customLabelUrl = null;
            int cpGiftLevel = 0;

            if (chatRoomMessage.getChatRoomMessageExtension() == null) {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (userInfo != null) {
                    newUserContent = userInfo.getUserLabel();
                    experLevel = userInfo.getExperLevel();
                    experCharm = userInfo.getCharmLevel();
                    if (experLevel == 0) {
                        experLevel = (int) SpUtils.get(context, AvRoomModel.EXPER_LEVEL, 0);
                    }
                    if (experCharm == 0) {
                        experCharm = (int) SpUtils.get(context, AvRoomModel.EXPER_CHARM, 0);
                    }

                    cpSign = (userInfo.getCpUser() == null || userInfo.getSign() == null) ? "" : userInfo.getSign();
                    cpGiftLevel = userInfo.getCpUser() == null ? 0 : userInfo.getCpUser().getCpGiftLevel();
                    customLabelUrl = userInfo.getCustomSign();
                }
            } else {
                try {
                    experLevel = (Integer) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.EXPER_LEVEL);
                    experCharm = (Integer) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.EXPER_CHARM);
                    newUserContent = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.IS_NEW_USER);
                    cpSign = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.CP_SIGN);
                    Object cpLevel = chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.CP_LEVEL);
                    if (cpLevel != null) {
                        cpGiftLevel = (int) cpLevel;
                    }
                    customLabelUrl = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.CUSTOM_LABEL_URL);
                } catch (Exception e) {
                }
            }

            if (!TextUtils.isEmpty(newUserContent) || experLevel > 0 || experCharm > 0 || !TextUtils.isEmpty(cpSign) || !TextUtils.isEmpty(customLabelUrl)) {
                TextView textView = new TextView(context);
                String content = "";
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                setNewLabel(builder, newUserContent, textView);
                setUserLevel(builder, experLevel);
                setUserCharm(builder, experCharm);
                setCpSign(builder, cpSign, cpGiftLevel);
                setCustomLabel(builder, customLabelUrl, textView);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER_VERTICAL;
                textView.setLayoutParams(params);
                textView.setText(builder);
                return textView;
            }
            return null;
        }

        private void setNewLabel(SpannableStringBuilder builder, String userLabelUrl, TextView textView) {
            if (!TextUtils.isEmpty(userLabelUrl) && userLabelUrl.equals("new")) {//兼容旧版标签
                builder.append(BaseConstant.LABEL_PLACEHOLDER);
                int start = builder.toString().length() - BaseConstant.LABEL_PLACEHOLDER.length();
                int end = builder.toString().length();
                UrlImageSpan imageSpan = new UrlImageSpan(textView.getContext(), R.drawable.new_user_msg_icon);
                builder.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            } else if (!TextUtils.isEmpty(userLabelUrl)) {
                builder.append(BaseConstant.LABEL_PLACEHOLDER);
                int start = builder.toString().length() - BaseConstant.LABEL_PLACEHOLDER.length();
                int end = builder.toString().length();
                UrlImageSpan urlImageSpan = new UrlImageSpan(textView.getContext(), userLabelUrl, textView);
                builder.setSpan(urlImageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }

        private void setUserLevel(SpannableStringBuilder builder, int experLevel) {
            if (experLevel > 0) {
                int start = builder.length();
                builder.append(BaseConstant.LEVEL_PLACEHOLDER);
                int end = builder.length();
                int levelRes = BaseApp.mBaseContext.getResources().getIdentifier("lv" + experLevel, "drawable", BaseApp.mBaseContext.getPackageName());
                UrlImageSpan urlImageSpan = new UrlImageSpan(BaseApp.mBaseContext, levelRes);
                builder.setSpan(urlImageSpan, start,
                        end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }

        private void setUserCharm(SpannableStringBuilder builder, int experCharm) {
            if (experCharm > 0) {
                int start = builder.length();
                builder.append(BaseConstant.LEVEL_PLACEHOLDER);
                int end = builder.length();
                int levelRes = BaseApp.mBaseContext.getResources().getIdentifier("charm" + experCharm, "drawable", BaseApp.mBaseContext.getPackageName());
                UrlImageSpan urlImageSpan = new UrlImageSpan(BaseApp.mBaseContext, levelRes);
                builder.setSpan(urlImageSpan, start,
                        end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }


        /**
         * 展示cp尾灯
         *
         * @param builder
         * @param sign
         */
        private void setCpSign(SpannableStringBuilder builder, String sign, int cpGiftLevel) {
            com.juxiao.library_utils.log.LogUtil.d("setCpSign", "sign = " + sign);
            if (!TextUtils.isEmpty(sign)) {
                builder.append(BaseConstant.CP_SIGN_PLACEHOLDER);
                CpSignImageSpan urlImageSpan = new CpSignImageSpan(BaseApp.mBaseContext, getCpLevelSignBg(cpGiftLevel), sign);
//                urlImageSpan.setImgWidth(DisplayUtil.dip2px(mContext,53));
//                urlImageSpan.setImgHeight(DisplayUtil.dip2px(mContext,14));
                builder.setSpan(urlImageSpan, builder.toString().length() - BaseConstant.CP_SIGN_PLACEHOLDER.length(),
                        builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }

        /**
         * 设置自定义尾灯
         *
         * @param builder
         * @param customLabelUrl
         * @param textView
         */
        private void setCustomLabel(SpannableStringBuilder builder, String customLabelUrl, TextView textView) {
            if (!TextUtils.isEmpty(customLabelUrl)) {
                builder.append(BaseConstant.CUSTOM_LABEL_PLACEHOLDER);
                int start = builder.toString().length() - BaseConstant.CUSTOM_LABEL_PLACEHOLDER.length();
                int end = builder.toString().length();
                UrlImageSpan urlImageSpan = new UrlImageSpan(BaseApp.mBaseContext, customLabelUrl, textView);
                urlImageSpan.setImgWidth(DisplayUtil.dip2px(BaseApp.mBaseContext, 36));
                urlImageSpan.setImgHeight(DisplayUtil.dip2px(BaseApp.mBaseContext, 15));
                builder.setSpan(urlImageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }

        private int getCpLevelSignBg(int cpLevel) {
            switch (cpLevel) {
                case 1:
                    return R.drawable.cp_chenghaopai_one;
                case 2:
                    return R.drawable.cp_chenghaopai_two;
                case 3:
                    return R.drawable.cp_chenghaopai_three;
                default:
                    return R.drawable.cp_chenghaopai_one;
            }
        }
    }
}

