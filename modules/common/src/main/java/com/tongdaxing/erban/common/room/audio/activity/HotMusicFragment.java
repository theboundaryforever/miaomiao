package com.tongdaxing.erban.common.room.audio.activity;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.room.audio.adapter.MyMusicAdapter;
import com.tongdaxing.erban.common.room.audio.presenter.HotMusicPresenter;
import com.tongdaxing.erban.common.room.audio.view.IHotMusicView;
import com.tongdaxing.erban.common.room.audio.view.IMusicView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.music.IMusicCore;
import com.tongdaxing.xchat_core.music.IMusicCoreClient;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

import butterknife.BindView;

/**
 * Function:热门歌曲
 * Author: Edward on 2019/2/13
 */
@CreatePresenter(HotMusicPresenter.class)
public class HotMusicFragment extends BaseMvpFragment<IHotMusicView, HotMusicPresenter> implements IHotMusicView {
    @BindView(R2.id.et_search)
    EditText etSearch;
    @BindView(R2.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R2.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    private MyMusicAdapter myMusicAdapter;
    private IMusicView iMusicView;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_hot_music;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IMusicView) {
            iMusicView = (IMusicView) context;
        }
    }

    @Override
    public void onFindViews() {
        etSearch.setOnClickListener(v -> {
            SearchMusicActivity.Companion.start(getActivity());
        });
    }

    @Override
    public void onReloadData() {
        showLoading();
        getMvpPresenter().refreshData();
    }

    @Override
    public void onSetListener() {
        myMusicAdapter = new MyMusicAdapter(R.layout.adapter_my_music);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.mm_theme));
        swipeRefreshLayout.setOnRefreshListener(() -> {
            getMvpPresenter().refreshData();
        });
        myMusicAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (iMusicView != null) {
                List<MyMusicInfo> list = myMusicAdapter.getData();
                iMusicView.onItemClick(list.get(position), view, position);
            }
        });
        myMusicAdapter.setOnLoadMoreListener(() -> {
            if (NetworkUtil.isNetAvailable(getContext())) {
                getMvpPresenter().loadMoreData();
            } else {
                myMusicAdapter.loadMoreEnd(true);
            }
        }, recyclerView);
        recyclerView.setAdapter(myMusicAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void loadDataFailure(boolean isRefresh, String msg) {
        if (isRefresh) {
            if (ListUtils.isListEmpty(myMusicAdapter.getData())) {
                showNetworkErr();
            } else {
                toast(msg);
            }
            swipeRefreshLayout.setRefreshing(false);
        } else {
            myMusicAdapter.loadMoreFail();
        }
    }

    @Override
    public void showRefresh(boolean flag) {
        showLoading();
    }

    @Override
    public void initiate() {
    }

    /**
     * 开始下载
     */
    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicStartDownload(HotMusicInfo hotMusicInfo) {
        notifyDataStatus(hotMusicInfo.getId(), MyMusicAdapter.FILE_STATUS_DOWNLOADING);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(MusicLocalInfo localMusicInfo) {
        notifyDataStatus(Long.parseLong(localMusicInfo.getSongId()), MyMusicAdapter.FILE_STATUS_STOP);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(MusicLocalInfo localMusicInfo) {
        notifyDataStatus(Long.parseLong(localMusicInfo.getSongId()), MyMusicAdapter.FILE_STATUS_PLAY);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop(MusicLocalInfo localMusicInfo) {
        notifyDataStatus(Long.parseLong(localMusicInfo.getSongId()), MyMusicAdapter.FILE_STATUS_PLAY);
    }

    private void notifyDataStatus(long id, int status) {
        if (myMusicAdapter != null) {
            List<MyMusicInfo> list = myMusicAdapter.getData();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getId() == id) {
                    MyMusicInfo myMusicInfo = list.get(i);
                    myMusicInfo.setFileStatus(status);
                    myMusicAdapter.setData(i, myMusicInfo);
                    myMusicAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicCoreClient.class)
    public void delOperationRefresh(long id, boolean isHidePlayer) {
        if (myMusicAdapter != null) {
            List<MyMusicInfo> list = myMusicAdapter.getData();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getId() == id) {
                    myMusicAdapter.remove(i);
                    myMusicAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadError(String msg, MusicLocalInfo localMusicInfo, HotMusicInfo hotMusicInfo) {
        notifyDataStatus(Long.parseLong(localMusicInfo.getSongId()), 0);
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void addSongSucceed(HotMusicInfo hotMusicInfo) {
        notifyDataStatus(hotMusicInfo.getId(), MyMusicAdapter.FILE_STATUS_PLAY);
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void addSongFailure(HotMusicInfo hotMusicInfo) {
        notifyDataStatus(hotMusicInfo.getId(), MyMusicAdapter.FILE_STATUS_DOWNLOAD);
    }

    @Override
    public void refreshMyMusicList(boolean isRefresh, List<MyMusicInfo> myMusicInfoList) {
        CoreManager.getCore(IMusicCore.class).setMusicStatus(myMusicInfoList);
        myMusicAdapter.setNewData(myMusicInfoList);
        if (isRefresh) {
            hideStatus();
            swipeRefreshLayout.setRefreshing(false);
        } else {
            if (!ListUtils.isListEmpty(myMusicInfoList)) {
                myMusicAdapter.loadMoreComplete();
            } else {
                myMusicAdapter.loadMoreEnd(true);
            }
        }
    }
}
