package com.tongdaxing.erban.common.room.talk.factory;

import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;
import com.tongdaxing.erban.common.view.UrlImageSpan;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.AuctionAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.BurstGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.talk.message.GiftMessage;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.tongdaxing.xchat_framework.util.util.RichTextUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Administrator on 5/31 0031.
 */

public class GiftFactory implements AbsBaseViewHolder.ViewHolderFactory {

    @NonNull
    @Override
    public GiftViewHolder create(@NonNull ViewGroup parent) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_talk_gift_item, parent, false);
        return new GiftViewHolder(v);
    }

    public static class GiftViewHolder extends AbsBaseViewHolder<GiftMessage> {
        private String TAG = "GiftViewHolder";
        private TextView mTvContent;
        private String mAccount;

        private GiftViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvContent = itemView.findViewById(R.id.talk_iv_gift_content);
            mTvContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatRoomMessage chatRoomMessage = (ChatRoomMessage) v.getTag();
                    CoreUtils.send(new RoomTalkEvent.OnRoomMsgItemClicked(chatRoomMessage));
                }
            });
        }

        @Override
        public void bind(GiftMessage message) {
            super.bind(message);
            ChatRoomMessage chatRoomMessage = message.getChatRoomMessage();
            mTvContent.setTag(chatRoomMessage);
            CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
            L.debug(TAG, "bind attachment: %s", attachment);
            if (attachment != null) {
                int first = attachment.getFirst();
                L.debug(TAG, "bind first: %d", first);
                if (first == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_AUCTION) {
                    setMsgAuction(message.getChatRoomMessage(), attachment);
                } else if (first == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                    setMsgHeaderGift(attachment);
                } else if (first == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                    setMsgMultiGift(attachment);
                } else if (first == CustomAttachment.CUSTOM_MSG_TYPE_BURST_GIFT) {
                    setBurstGift((BurstGiftAttachment) attachment);
                }
            }
        }

        private void setMsgAuction(ChatRoomMessage chatRoomMessage, CustomAttachment attachment) {
            String senderNick = chatRoomMessage.getChatRoomMessageExtension().getSenderNick();
            AuctionAttachment auctionAttachment = (AuctionAttachment) attachment;
            if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_AUCTION_START) {
                senderNick = "房主";
                String content = senderNick + " 开启了竞拍";
                mTvContent.setTextColor(mTvContent.getContext().getResources().getColor(R.color.roomTextNick));
                mTvContent.setText(content);
            } else if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_AUCTION_FINISH) {
                if (auctionAttachment.getAuctionInfo().getCurMaxUid() > 0) {
                    UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(auctionAttachment.getAuctionInfo().getCurMaxUid());
                    if (userInfo != null) {
                        senderNick = userInfo.getNick();
                    } else {
                        senderNick = "";
                    }
                    String coin = " 以" + auctionAttachment.getAuctionInfo().getRivals().get(0).getAuctMoney() + "金币拍下 ";
                    String voiceActorNick = "";
                    UserInfo voiceActor = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(auctionAttachment.getAuctionInfo().getAuctUid());
                    if (voiceActor != null) {
                        voiceActorNick = voiceActor.getNick();
                    }
                    String content = senderNick + coin + voiceActorNick;
                    SpannableStringBuilder builder = new SpannableStringBuilder(content);
                    ForegroundColorSpan redSpan = new ForegroundColorSpan(mTvContent.getContext().getResources().getColor(R.color.roomTextNick));
                    ForegroundColorSpan yellowSpan = new ForegroundColorSpan(mTvContent.getContext().getResources().getColor(R.color.white));
                    builder.setSpan(redSpan, 0, senderNick.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    builder.setSpan(yellowSpan, senderNick.length(), senderNick.length() + coin.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mTvContent.setTextColor(mTvContent.getContext().getResources().getColor(R.color.roomTextNick));
                    mTvContent.setText(builder);
                } else {
                    senderNick = "房主";
                    String content = senderNick + " 结束了竞拍，当前暂无人出价";
                    mTvContent.setTextColor(mTvContent.getContext().getResources().getColor(R.color.roomTextNick));
                    mTvContent.setText(content);
                }
            } else {
                if (StringUtil.isEmpty(senderNick)) {
                    senderNick = "";
                }
                String content = senderNick + " 出价" + auctionAttachment.getAuctionInfo().getRivals().get(0).getAuctMoney() + "金币";
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                ForegroundColorSpan redSpan = new ForegroundColorSpan(mTvContent.getContext().getResources().getColor(R.color.roomTextNick));
                if (!TextUtils.isEmpty(senderNick))
                    builder.setSpan(redSpan, 0, senderNick.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                mTvContent.setText(builder);
            }
        }

        private void setMsgHeaderGift(CustomAttachment attachment) {
            GiftAttachment giftAttachment = (GiftAttachment) attachment;
            GiftReceiveInfo giftRecieveInfo = giftAttachment.getGiftRecieveInfo();
            if (giftRecieveInfo == null) {
                return;
            }
            GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftRecieveInfo.getGiftId());
            L.debug(TAG, "setMsgHeaderGift giftInfo: %s", giftInfo);
            if (giftInfo != null) {
                SpannableStringBuilder builder1 = new SpannableStringBuilder("");

                String nick = giftAttachment.getGiftRecieveInfo().getNick();
                String targetNick = giftAttachment.getGiftRecieveInfo().getTargetNick();

                List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                HashMap<String, Object> map;
                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, nick);
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xffffffff);
                list.add(map);
                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, " 送给 ");
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xffffd800);
                list.add(map);
                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, targetNick + " ");
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xffffffff);
                list.add(map);
                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, giftInfo.getGiftName() + " ");
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xffffd800);
                list.add(map);
                SpannableStringBuilder builder = RichTextUtil.getSpannableStringFromList(list);
                builder.append(BaseConstant.GIFT_PLACEHOLDER);//插入小图标占位符
                UrlImageSpan imageSpan = new UrlImageSpan(mTvContent.getContext(), giftInfo.getGiftUrl(), mTvContent);
                imageSpan.setImgWidth(ScreenUtil.dip2px(22));
                imageSpan.setImgHeight(ScreenUtil.dip2px(22));
                builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(), builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder1.append(builder);
                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, " X" + giftAttachment.getGiftRecieveInfo().getGiftNum());
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xffffd800);
                list.clear();
                list.add(map);
                SpannableStringBuilder numberBuilder = RichTextUtil.getSpannableStringFromList(list);
                builder1.append(numberBuilder);
                mTvContent.setText(builder1);
            }
        }

        private void setMsgMultiGift(CustomAttachment attachment) {
            MultiGiftAttachment giftAttachment = (MultiGiftAttachment) attachment;
            MultiGiftReceiveInfo multiGiftRecieveInfo = giftAttachment.getMultiGiftRecieveInfo();
            if (multiGiftRecieveInfo == null) {
                return;
            }
            GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(multiGiftRecieveInfo.getGiftId());
            L.debug(TAG, "setMsgMultiGift giftInfo: %s", giftInfo);
            if (giftInfo != null) {
                String newContent = "";
                SpannableStringBuilder builder1 = new SpannableStringBuilder(newContent);

                int normalColor = 0xffffffff;
                int focusColor = 0xffffd800;
                List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                //赠送者
                list.add(RichTextUtil.getRichTextMap(giftAttachment.getMultiGiftRecieveInfo().getNick(), normalColor));

                //计算到底是全麦礼物还是多人非全麦礼物 多人非全麦礼物显示最后一个被赠送者的昵称
                SparseArray<RoomQueueInfo> sparseArray = AvRoomDataManager.get().mMicQueueMemberMap;//size为9
                int micUserNum = 0;
                for (int i = 0; i < sparseArray.size(); i++) {
                    RoomQueueInfo roomQueueInfo = sparseArray.valueAt(i);
                    if (roomQueueInfo != null) {
                        ChatRoomMember c = roomQueueInfo.mChatRoomMember;
                        if (c != null && !AvRoomDataManager.get().isRoomOwner(c.getAccount())) {//先把房主排除
                            micUserNum++;
                        }
                    }
                }
                micUserNum++;//再把房主算进去

                long senderUid = giftAttachment.getMultiGiftRecieveInfo().getUid();
                if (AvRoomDataManager.get().isOnMic(senderUid)) {
                    //赠送者在麦上
                    micUserNum--;
                }

                if (multiGiftRecieveInfo.getTargetUids().size() < micUserNum && multiGiftRecieveInfo.getNickList() != null && multiGiftRecieveInfo.getNickList().size() > 0) {
                    //多人 只显示最后一个人
                    //"赠送者 送给 被赠送者 "
                    list.add(RichTextUtil.getRichTextMap(" 送给 ", focusColor));
                    list.add(RichTextUtil.getRichTextMap(multiGiftRecieveInfo.getNickList().get(multiGiftRecieveInfo.getNickList().size() - 1) + " ", normalColor));
                } else {
                    //全麦
                    //"赠送者 全麦送出 "
                    list.add(RichTextUtil.getRichTextMap(" 全麦送出 ", focusColor));
                }
                //计算

                //礼物名
                list.add(RichTextUtil.getRichTextMap(giftInfo.getGiftName(), focusColor));
                SpannableStringBuilder builder = RichTextUtil.getSpannableStringFromList(list);
                builder.append(" ");
                builder.append(BaseConstant.GIFT_PLACEHOLDER);//插入小图标占位符
                UrlImageSpan imageSpan = new UrlImageSpan(mTvContent.getContext(), giftInfo.getGiftUrl(), mTvContent);
                imageSpan.setImgWidth(ScreenUtil.dip2px(22));
                imageSpan.setImgHeight(ScreenUtil.dip2px(22));
                builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(), builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder1.append(builder);

                HashMap<String, Object> map;
                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, " X" + giftAttachment.getMultiGiftRecieveInfo().getGiftNum());
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xffffd800);
                list.clear();
                list.add(map);
                SpannableStringBuilder numberBuilder = RichTextUtil.getSpannableStringFromList(list);
                builder1.append(numberBuilder);

                mTvContent.setText(builder1);
            }
        }

        private void setBurstGift(BurstGiftAttachment attachment) {
            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            list.add(RichTextUtil.getRichTextMap("神秘爆出~", 0xFFFFFFFF));
            list.add(RichTextUtil.getRichTextMap(attachment.getNick(), 0xFFFFD800));
            list.add(RichTextUtil.getRichTextMap(" 通过 ", 0xFFFFFFFF));
            list.add(RichTextUtil.getRichTextMap(attachment.getSendNick(), 0xFFFFD800));
            list.add(RichTextUtil.getRichTextMap(" 送礼意外获得 ", 0xFFFFFFFF));
            list.add(RichTextUtil.getRichTextMap("【" + attachment.getGiftName() + "】", 0xFFFFD800));
            list.add(RichTextUtil.getRichTextMap("X" + attachment.getGiftNum(), 0xFFFFFFFF));
            mTvContent.setText(RichTextUtil.getSpannableStringFromList(list));
        }
    }
}
