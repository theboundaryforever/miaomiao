package com.tongdaxing.erban.common.ui.login.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;

import com.netease.nim.uikit.cache.MsgIgnoreCache;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.ClientType;
import com.netease.nis.captcha.Captcha;
import com.netease.nis.captcha.CaptchaListener;
import com.opensource.svgaplayer.SVGAImageView;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.ui.common.permission.PermissionActivity;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.setting.utils.NotificationPermissionUtil;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.ui.widget.dialog.CommonDialog;
import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.initial.InitModel;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.coremanager.IAppInfoCore;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.constant.AppKey;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 新版登录页面
 * Created by zhangjian on 2019/4/23.
 */
public class NewLoginActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "NewLoginActivity";
    private static final String KICK_OUT = "KICK_OUT";

    /**
     * 基本权限管理
     */
    private final String[] BASIC_PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO,
    };
    @BindView(R2.id.login_svga_bg)
    SVGAImageView loginSvgaBg;

    private Captcha mCaptcha;
    private int nextStepId = -1;
    //避免重复点击多次执行
    private boolean captchaIsShowing = false;
    /**
     * 网易图形验证码操作回调
     */
    CaptchaListener myCaptchaListener = new CaptchaListener() {

        @Override
        public void onValidate(String result, String validate, String message) {
            captchaIsShowing = false;
            //验证结果，valiadte，可以根据返回的三个值进行用户自定义二次验证
            if (validate.length() > 0) {//验证成功
                nextStep();
            } else {
                SingleToastUtil.showToast(NewLoginActivity.this, "验证失败：" + message);
            }
        }

        @Override
        public void closeWindow() {
            //请求关闭页面
            captchaIsShowing = false;
        }

        @Override
        public void onError(String errormsg) {
            //出错
            captchaIsShowing = false;
        }

        @Override
        public void onCancel() {
            captchaIsShowing = false;
        }

        @Override
        public void onReady(boolean ret) {
            //该为调试接口，ret为true表示加载Sdk完成
        }

    };

    public static void start(Context context) {
        start(context, false);
    }

    public static void start(Context context, boolean kickOut) {
        Intent intent = new Intent(context, NewLoginActivity.class);
        intent.putExtra(KICK_OUT, kickOut);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_login);
        ButterKnife.bind(this);
        SvgaUtils.getInstance(loginSvgaBg.getContext()).cyclePlayAssetsAnim(loginSvgaBg, "login_svga");
        initImgCheck();
        onParseIntent();
        permission();
        setSwipeBackEnable(false);

        //如果没有通知权限
        if (!NotificationPermissionUtil.areNotificationsEnabled(getApplicationContext())) {
            showNoPermissionDialog();
        }
    }

    private void onParseIntent() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        if (getIntent().getBooleanExtra(KICK_OUT, false)) {
            int type = NIMClient.getService(AuthService.class).getKickedClientType();
            String client;
            switch (type) {
                case ClientType.Web:
                    client = "网页端";
                    break;
                case ClientType.Windows:
                    client = "电脑端";
                    break;
                case ClientType.REST:
                    client = "服务端";
                    break;
                default:
                    client = "移动端";
                    break;
            }
            getDialogManager().showOkAndLabelDialog("你的帐号在" + client + "登录，被踢出下线，请确定帐号信息安全", "下线通知", true, true, false, false, new DialogManager.OkDialogListener() {
                @Override
                public void onOk() {
                }
            });
        }
    }

    private void permission() {
        checkPermission(new PermissionActivity.CheckPermListener() {
            @Override
            public void superPermission() {
//                toast("授权成功");
                checkKsActive();
            }
        }, R.string.ask_again, BASIC_PERMISSIONS);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        super.onPermissionsGranted(requestCode, perms);
        //用户把权限全部处理完才会调用 这里边的权限是用户授予的权限
        L.debug(TAG, "requestCode = %d, permissions = %s", requestCode, Arrays.toString(perms.toArray()));
        if (perms.contains(BASIC_PERMISSIONS[3])) {
            checkKsActive();
        } else {
            perms.add(BASIC_PERMISSIONS[3]);
            onPermissionsDenied(0, perms);
        }
    }

    private void showNoPermissionDialog() {
        CommonDialog commonDialog = new CommonDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", getString(R.string.notification_no_permission_dialog_title));
        commonDialog.setArguments(bundle);
        commonDialog.show(getSupportFragmentManager(), "");
        commonDialog.setmOnClickListener(new CommonDialog.OnClickListener() {
            @Override
            public void onOKClick() {
                NotificationPermissionUtil.requestNotify(getApplicationContext());
            }

            @Override
            public void onCancelClick() {

            }
        });
    }


    /**
     * 快手imei激活
     */
    private void checkKsActive() {
        Boolean aBoolean = (Boolean) SpUtils.get(NewLoginActivity.this, SpEvent.SP_KS_ACTIVE, false);
        L.info(TAG, "checkKsActive() SP_KS_ACTIVE %b", aBoolean);
        if (!aBoolean) {
            InitModel.get().ksActive(new OkHttpManager.MyCallBack<ServiceResult>() {
                @Override
                public void onError(Exception e) {

                }

                @Override
                public void onResponse(ServiceResult response) {
                    if (response != null && response.isSuccess()) {
                        SpUtils.put(NewLoginActivity.this, SpEvent.SP_KS_ACTIVE, true);
                    }
                }
            });
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void openWebView(String url) {
        if (TextUtils.isEmpty(url)) return;
        Intent intent = new Intent(this, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLoginFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        LogUtil.d("LoginActivity onLogin");
        CoreManager.getCore(IAppInfoCore.class).checkBanned(true);
        MsgIgnoreCache.getInstance().clearAllMsgIgnoreNumber(this);//登录成功清除一下上次登录用户忽略的 未读消息
        getDialogManager().dismissDialog();
        finish();
    }

    /**
     * 10分钟内操作是否是要验证的
     *
     * @return
     */
    private boolean cleckLoginTimeNeedCaptcha() {
        long l = (Long) SpUtils.get(this, SpEvent.cleckLoginTime, 0L);
        long currentTimeMillis = System.currentTimeMillis();
        int i = 600000;
        if (BasicConfig.isDebug) {
            i = 60000;
        }
        if (currentTimeMillis - l > i) {
            SpUtils.put(this, SpEvent.cleckLoginTime, currentTimeMillis);
            return false;
        }
        return true;
    }

    /**
     * 网易图形验证码初始化
     */
    private void initImgCheck() {
        //初始化验证码SDK相关参数，设置CaptchaId、Listener最后调用start初始化。
        if (mCaptcha == null) {
            mCaptcha = new Captcha(this);
        }
        mCaptcha.setCaptchaId(AppKey.getCaptchaId());
        mCaptcha.setCaListener(myCaptchaListener);
        //可选：开启debug
        mCaptcha.setDebug(false);
        //可选：设置超时时间
        mCaptcha.setTimeout(10000);
    }

    /**
     * 图形验证码验证成功后操作
     */
    private void nextStep() {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        if (nextStepId == R.id.user_ll_login_wechat) {
            getDialogManager().showProgressDialog(this, "请稍后");
            CoreManager.getCore(IAuthCore.class).doWxLogin();
            UmengEventUtil.getInstance().onEvent(this, UmengEventId.getLoginWechat());

        } else if (nextStepId == R.id.user_ll_login_qq) {
            getDialogManager().showProgressDialog(this, "请稍后");
            CoreManager.getCore(IAuthCore.class).doQqLogin();
            UmengEventUtil.getInstance().onEvent(this, UmengEventId.getLoginQQ());

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SvgaUtils.closeSvga(loginSvgaBg);
    }

    @OnClick(R2.id.user_tv_register)
    public void onRegisterClicked() {
        UIHelper.showRegisterAct(NewLoginActivity.this);
    }

    @OnClick(R2.id.user_tv_login)
    public void onLoginByPhone() {
        PhoneLoginActivity.start(NewLoginActivity.this);
    }

    @OnClick(R2.id.user_ll_login_wechat)
    public void onLoginByWeChat() {
        login(R.id.user_ll_login_wechat);
    }

    @OnClick(R2.id.user_ll_login_qq)
    public void onLoginByQQ() {
        login(R.id.user_ll_login_qq);
    }

    private void login(int id) {
        if (captchaIsShowing)
            return;
        nextStepId = id;
        if (cleckLoginTimeNeedCaptcha()) {
            captchaIsShowing = true;
            mCaptcha.start();
            //简单测试是否填写captcha 是否初始化，包括captchaId与设置监听对象
            if (mCaptcha.checkParams()) {
                if (this.isDestroyed() || this.isFinishing()) {
                    return;
                }
                mCaptcha.Validate();
            } else {
                nextStep();
            }
        } else {
            nextStep();
        }
    }
}
