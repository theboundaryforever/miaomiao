package com.tongdaxing.erban.common.ui.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.widget.SquareImageView;
import com.tongdaxing.xchat_core.home.HomeRoom;

/**
 * <p> 首页item view  </p>
 *
 * @author Administrator
 * @date 2017/11/29
 */
public class HomeItemView extends RelativeLayout implements View.OnClickListener {
    private SquareImageView mIvCover;
    private ImageView mIvTabCategory;
    private ImageView mIvLockBg;
    private TextView mTvTitle;
    private ImageView mIvTabLabel;
    private TextView mTvOnlineNumber;
    private Context mContext;

    private int mRound;
    private int mTagHeight, mBadgeHeight;
    private HomeRoom mHomeRoom;


    public HomeItemView(Context context) {
        this(context, null);
    }

    public HomeItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        inflate(context, R.layout.list_item_home_layout, this);

        mIvCover = (SquareImageView) findViewById(R.id.iv_cover);
        mIvTabCategory = (ImageView) findViewById(R.id.iv_tab_category);
        mIvLockBg = (ImageView) findViewById(R.id.lock_bg);
        mTvTitle = (TextView) findViewById(R.id.title);
        mTvOnlineNumber = (TextView) findViewById(R.id.tv_online_number);

        mRound = context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size);
        mTagHeight = (int) context.getResources().getDimension(R.dimen.tag_height);
        mBadgeHeight = (int) context.getResources().getDimension(R.dimen.badge_tab_category_height);

        setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mHomeRoom != null) {
            AVRoomActivity.start(mContext, mHomeRoom.getUid());
        }
    }
}
