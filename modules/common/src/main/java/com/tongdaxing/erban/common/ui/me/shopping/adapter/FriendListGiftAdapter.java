package com.tongdaxing.erban.common.ui.me.shopping.adapter;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;


/**
 * Created by chenran on 2017/10/3.
 */

public class FriendListGiftAdapter extends BaseQuickAdapter<NimUserInfo, BaseViewHolder> {

    public IGiveAction iGiveAction;

    public FriendListGiftAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, NimUserInfo item) {

        Button buInvite = helper.getView(R.id.bu_invite);
        TextView tvItemName = helper.getView(R.id.tv_item_name);
        ImageView imageView = helper.getView(R.id.bu_invite);

        ImageLoadUtils.loadImage(mContext, item.getAvatar(), imageView);
        String name = item.getName();
        String account = item.getAccount();
        buInvite.setText("赠送");
        tvItemName.setText(name);
        buInvite.setOnClickListener(view -> {
            if (iGiveAction != null)
                iGiveAction.onGiveEvent(account, name);
        });
    }

    public interface IGiveAction {
        void onGiveEvent(String uid, String userName);
    }
}
