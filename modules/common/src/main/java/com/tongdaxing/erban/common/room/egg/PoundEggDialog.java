package com.tongdaxing.erban.common.room.egg;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.erban.ui.adapter.BaseAdapterHelper;
import com.erban.ui.adapter.ListAdapter;
import com.tcloud.core.log.L;
import com.tcloud.core.util.LimitClickUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.room.egg.dialog.PoundEggRankListDialog;
import com.tongdaxing.erban.common.room.widget.dialog.ListDataDialog;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.lotterybox.ILotteryBoxCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.EggStatus;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 砸金蛋dialog
 * Created by zwk on 2018/6/6.
 */
public class PoundEggDialog extends BaseDialogFragment implements View.OnClickListener {
    private final int EVERY_TIME_COUNT = 20;
    //砸一次按钮
    @BindView(R2.id.bu_lottery_box_one)
    Button buLotteryBoxOne;
    //砸十次按钮
    @BindView(R2.id.bu_lottery_box_ten)
    Button buLotteryBoxTen;
    //自动砸按钮
    @BindView(R2.id.bu_lottery_box_fifty)
    Button buLotteryBoxAuto;
    //锤子图片
    @BindView(R2.id.iv_hammer)
    ImageView ivHammer;
    //砸金蛋关闭页面
    @BindView(R2.id.iv_pound_egg_closs)
    ImageView ivCloss;
    @BindView(R2.id.iv_lottery_box_box)
    ImageView ivLotteryBoxBox;
    @BindView(R2.id.tv_lottery_box_title)
    TextView tvLotteryBoxTitle;
    @BindView(R2.id.ll_lottery_button)
    LinearLayout llLotteryButton;
    @BindView(R2.id.tv_gold_count)
    TextView tvGoldCount;
    @BindView(R2.id.tv_lottery_box_recharge)
    TextView tvLotteryBoxRecharge;
    Unbinder unbinder;
    @BindView(R2.id.tv_lottery_dialog_title)
    TextView tvLotteryDialogTitle;
    @BindView(R2.id.bu_lottery_dialog_ok)
    Button buLotteryDialogOk;
    @BindView(R2.id.rl_lottery_ten_time_dialog)
    RelativeLayout rlLotteryTenTimeDialog;
    @BindView(R2.id.iv_lottery_rule)
    ImageView ivLotteryRule;
    @BindView(R2.id.iv_lottery_rank)
    ImageView ivLotteryRank;
    @BindView(R2.id.tv_lottery_rule)
    TextView tvLotteryRule;
    @BindView(R2.id.tv_my_egg_record)
    TextView tv_egg_record;
    @BindView(R2.id.egg_tv_auto_50)
    TextView mEggTvAuto50;
    @BindView(R2.id.cb_close_msg)
    CheckBox cbCloseMsg;
    @BindView(R2.id.egg_iv_award_close)
    ImageView mEggIvAwardClose;
    @BindView(R2.id.egg_gv_award)
    GridView mEggGvAward;
    @BindView(R2.id.egg_tv_award_btn)
    TextView mEggTvAwardBtn;
    @BindView(R2.id.egg_cl_award_layout)
    LinearLayout mEggClAwardLayout;
    @BindView(R2.id.egg_ll_button_layout)
    LinearLayout mEggButtonLayout;
    @BindView(R2.id.egg_ll_root_view)
    LinearLayout mEggRootLayout;
    private String TAG = "PoundEggDialog";
    private List<EggGiftInfo> mAwardList = new ArrayList<>();
    private ListAdapter<EggGiftInfo> mAwardAdapter;
    private ILotteryBoxCore iLotteryBoxCore;
    private boolean mIsAutoContinue;
    private int mEggFirstType = 1;  // 砸蛋1次
    private int mEggTenType = 2;       // 砸蛋10次，
    private int mEggFiftyType = 3;      // 砸蛋50次
    private boolean mIsAuto;
    private double goldNum;
    private Handler animHandler = new LotteryHandler();
    private LimitClickUtils mLimitClickUtils = new LimitClickUtils();

    private int lotteryType = 1;//砸蛋次数类别 1为1次、2为10次、3为50次
    OkHttpManager.MyCallBack<ServiceResult<List<EggGiftInfo>>> lotteryCallBack = new OkHttpManager.MyCallBack<ServiceResult<List<EggGiftInfo>>>() {
        @Override
        public void onError(Exception e) {
            onLotteryError("网络异常");
        }

        @Override
        public void onResponse(ServiceResult<List<EggGiftInfo>> response) {
            if (getContext() == null) {
                return;
            }
            if (response != null) {
                if (response.isSuccess()) {
                    onLotterySuccess(response.getData());
                } else if (response.getCode() == 2103) {// 余额不足
                    if (mIsAuto) {
                        SingleToastUtil.showShortToast(response.getMessage());
                    } else {
                        if (getContext() == null) {
                            return;
                        }
                        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
                            @Override
                            public void onClick(View view, ChargeDialogFragment fragment) {
                                if (view.getId() == R.id.btn_cancel) {
                                    fragment.dismissAllowingStateLoss();
                                } else if (view.getId() == R.id.btn_ok) {
                                    MyWalletNewActivity.start(getContext());
                                    fragment.dismissAllowingStateLoss();
                                }
                            }
                        }).show(((FragmentActivity) getContext()).getSupportFragmentManager(), "charge");
                    }
                    resetState();
                    closeView();
                } else {
                    resetState();
                    closeView();
                    onLotteryError(response.getErrorMessage());
                }
            } else {
                resetState();
                closeView();
                onLotteryError("数据异常");
            }
        }
    };

    public static PoundEggDialog newOnlineUserListInstance() {
        return newInstance();
    }

    private static PoundEggDialog newInstance() {
        PoundEggDialog lotteryBoxDialog = new PoundEggDialog();
        return lotteryBoxDialog;
    }

    /**
     * 根据：
     * http://blog.sina.com.cn/s/blog_5da93c8f0102x5zl.html
     * https://blog.csdn.net/freelander_j/article/details/52925745
     * 修复IllegalStateException: Can not perform this action after onSaveInstanceState with DialogFragment
     *
     * @param transaction
     * @param tag
     * @return
     */
    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        CoreManager.removeClient(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getStatus();
    }

    private void getStatus() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("queryUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getPoundEggHideStatus(), null, params, new OkHttpManager.MyCallBack<ServiceResult<EggStatus>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<EggStatus> response) {
                if (response != null && response.isSuccess()) {
                    boolean result = response.getData().isDrawMsgHide();
                    if (cbCloseMsg != null) {
                        Constants.IS_SHOW_WINNING_MSG = result;
                        cbCloseMsg.setChecked(result);//true 是隐藏（勾），false是显示（不勾）

                    }
                }
            }
        });
    }

    /**
     * 设置选中状态
     */
    private void setStatus(boolean flag) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("hide", String.valueOf(flag));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.hidePoundEgg(), null, params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    if (cbCloseMsg != null) {
                        Constants.IS_SHOW_WINNING_MSG = flag;
                        cbCloseMsg.setChecked(flag);
                    }
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        iLotteryBoxCore = CoreManager.getCore(ILotteryBoxCore.class);
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_pound_egg, window.findViewById(android.R.id.content), false);
        unbinder = ButterKnife.bind(this, view);
        setCancelable(true);

        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        initEvent();
        rlLotteryTenTimeDialog.setVisibility(View.GONE);
        CoreManager.getCore(IPayCore.class).getWalletInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (walletInfo != null) {
            goldNum = walletInfo.getGoldNum();
            tvGoldCount.setText(getContext().getString(R.string.gold_num_text, goldNum));
        }

        //初始化头奖名称
        Json configData = CoreManager.getCore(VersionsCore.class).getConfigData();
        String lotteryBoxBigGift = configData.str("lotteryBoxBigGift");
        if (!TextUtils.isEmpty(lotteryBoxBigGift)) {
            tvLotteryBoxTitle.setText("1个锤子1次，最高可得“" + lotteryBoxBigGift + "”");
            tvLotteryRule.setText("1、参与砸金蛋活动，“砸蛋1次”消耗1个锤子，“砸蛋10次”消耗10个锤子，1锤子=20金币，100%中奖 \n\n2.最高可以砸中“" + lotteryBoxBigGift + "” \n\n3.砸中的礼物可免费赠送给主播 \n\n4.连续砸蛋中奖概率更高哦~");
        }
        return view;
    }

    private void showDialogStyle(boolean isRule) {
        tvLotteryDialogTitle.setText("活动规则");
        tvLotteryRule.setVisibility(View.VISIBLE);
        buLotteryDialogOk.setText("关闭");
    }

    private void initEvent() {
        buLotteryBoxOne.setOnClickListener(this);
        buLotteryBoxAuto.setOnClickListener(this);
        buLotteryBoxTen.setOnClickListener(this);
        buLotteryDialogOk.setOnClickListener(this);
        tvLotteryBoxRecharge.setOnClickListener(this);
        ivCloss.setOnClickListener(this);
        ivLotteryRule.setOnClickListener(this);
        ivLotteryRank.setOnClickListener(this);
        tv_egg_record.setOnClickListener(this);
        mEggTvAuto50.setOnClickListener(this);
        cbCloseMsg.setOnClickListener(v -> {
            if (((CheckBox) v).isChecked()) {//选中
                setStatus(true);
            } else {//未选中
                setStatus(false);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        animHandler.removeCallbacksAndMessages(null);
        animHandler = null;
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        L.debug(TAG, "onClick viewId: %d", viewId);
        if (mLimitClickUtils.checkForTime(viewId, 500)) {
            L.debug(TAG, "onClick limit");
            return;
        }
        if (viewId == R.id.bu_lottery_box_one) {
            startLottery(mEggFirstType);

        } else if (viewId == R.id.bu_lottery_box_ten) {
            startLottery(mEggTenType);

        } else if (viewId == R.id.bu_lottery_box_fifty) {
            startLottery(mEggFiftyType);

        } else if (viewId == R.id.bu_lottery_dialog_ok) {
            resetAnim();
            rlLotteryTenTimeDialog.setVisibility(View.GONE);

        } else if (viewId == R.id.tv_lottery_box_recharge) {
            MyWalletNewActivity.start(getActivity());
            finishLottery();

        } else if (viewId == R.id.iv_pound_egg_closs) {
            try {
                dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (viewId == R.id.iv_lottery_rule) {
            showDialogStyle(true);
            rlLotteryTenTimeDialog.setVisibility(View.VISIBLE);

        } else if (viewId == R.id.iv_lottery_rank) {
            PoundEggRankListDialog bigListDataDialog = PoundEggRankListDialog.newContributionListInstance(getActivity());
            bigListDataDialog.show(getChildFragmentManager());

        } else if (viewId == R.id.tv_my_egg_record) {
            ListDataDialog record = ListDataDialog.newPoundEggRecordInstance(getContext());
            record.show(getChildFragmentManager());

        } else if (viewId == R.id.egg_tv_auto_50) {
            if (mIsAuto) {
                return;
            }
            mIsAuto = true;
            startLottery(mEggFiftyType);

        } else {
        }
    }

    /**
     * 开始砸蛋
     *
     * @param type 次数类别 1为1次、2为10次、3为自动砸（循环1次1次的砸）
     */
    private void startLottery(int type) {
        lotteryType = type;
        resetView();
        if (lotteryType == mEggFiftyType) {//自动砸蛋模式下，开始砸蛋的时候要改变按钮形状
            buLotteryBoxAuto.setSelected(true);
        }
        doLottery();
    }

    /**
     * 砸蛋
     */
    private void doLottery() {
        if (iLotteryBoxCore != null) {
            iLotteryBoxCore.lotteryRequest(lotteryType, lotteryCallBack);
        }
    }

    /**
     * 结束砸蛋
     */
    private void finishLottery() {
        if (buLotteryBoxAuto == null) {//dialog销毁之后不执行方法
            return;
        }
        if (lotteryType == mEggFiftyType) {//自动砸蛋模式下，结束砸蛋的时候要恢复按钮形状
            buLotteryBoxAuto.setSelected(false);
        }
    }

    /**
     * 重置动画
     */
    private void resetAnim() {
        if (ivLotteryBoxBox != null)
            ivLotteryBoxBox.setImageResource(R.drawable.ic_pound_egg_0);
        if (ivHammer != null)
            ivHammer.setVisibility(View.VISIBLE);
        animHandler.removeCallbacksAndMessages(null);
    }

    public void onLotterySuccess(List<EggGiftInfo> gifts) {
        if (ListUtils.isListEmpty(gifts)) {
            finishLottery();
            return;
        }
        if (gifts.size() == 1 && gifts.get(0).getGiftNum() == 1) {//礼物数等于1
            goldNum -= EVERY_TIME_COUNT;
            CoreManager.getCore(IPayCore.class).minusGold(EVERY_TIME_COUNT);
            if (tvGoldCount != null) {
                tvGoldCount.setText(getContext().getString(R.string.gold_num_text, goldNum));
                showFreeGiftAnim(gifts);
                updateGiftData(gifts);
            }
        } else if (gifts.size() > 1 || gifts.get(0).getGiftNum() > 1) {//礼物数大于1 或者单个礼物数量大于1
            int gold;
            if (lotteryType == mEggTenType) {
                gold = EVERY_TIME_COUNT * 10;
            } else {
                gold = EVERY_TIME_COUNT * 50;
            }
            goldNum -= gold;
            CoreManager.getCore(IPayCore.class).minusGold(gold);
            if (tvGoldCount != null) {
                tvGoldCount.setText(getContext().getString(R.string.gold_num_text, goldNum));
                showFreeGiftDialog();
                updateGiftData(gifts);
            }
        }
        setLotteryGiftInfo(gifts);
    }

    /***
     * 更新礼物信息 普通礼物和神秘礼物
     * @param gifts
     */
    private void setLotteryGiftInfo(List<EggGiftInfo> gifts) {
        if (ListUtils.isListEmpty(gifts))
            return;
        List<GiftInfo> giftInfos = CoreManager.getCore(IGiftCore.class).getBackpackGiftInfosByType2And3And8();
        if (ListUtils.isListEmpty(giftInfos)) {
            return;
        }
        //把gift的索引标记起来
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < giftInfos.size(); i++) {
            GiftInfo giftInfo = giftInfos.get(i);
            if (giftInfo == null)
                continue;
            map.put(giftInfo.getGiftId(), i);
        }

        GiftInfo markGift = new GiftInfo();
        int markCount = 0;
        //把获得的礼物加进集合
        boolean isRequest = false;
        for (int i = 0; i < gifts.size(); i++) {
            EggGiftInfo gift = gifts.get(i);
            Integer integer = map.get(gift.getGiftId());
            if (gift.getGiftType() == 3) {
                isRequest = true;
            }
            if (integer == null)
                continue;
            int index = integer;
            GiftInfo giftInfo = giftInfos.get(index);
            if (giftInfo.getGoldPrice() >= Constants.FULL_PUSH_MAX && giftInfo.getGoldPrice() > markGift.getGoldPrice()) {
                markGift = giftInfo;
                markCount = gift.getGiftNum();
            }
            giftInfo.setUserGiftPurseNum(giftInfo.getUserGiftPurseNum() + gift.getGiftNum());
        }

        if (markGift.getGoldPrice() >= Constants.FULL_PUSH_MAX && !Constants.IS_SHOW_WINNING_MSG && markCount > 0) {
            CoreManager.getCore(IGiftCore.class).sendLotteryMeg(markGift, markCount);
        }

//        for (int i = 0; i < gifts.size(); i++) {
//            EggGiftInfo gift = gifts.get(i);
//            if (gift.getGoldPrice() >= Constants.FULL_PUSH_MAX && !Constants.IS_SHOW_WINNING_MSG) {//礼物只要大于指定值就发出
//                CoreManager.getCore(IGiftCore.class).sendLotteryMegNew(gift, gift.getGiftNum());
//            }
//        }

//        if (isRequest) {
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
//        }
    }

    private void updateGiftData(List<EggGiftInfo> gifts) {
        mEggClAwardLayout.setVisibility(View.VISIBLE);
        setEnabled(mEggButtonLayout, false);
        setEnabled(mEggRootLayout, false);
        mAwardList.clear();
        mAwardList.addAll(gifts);
        setAdapter();
    }

    private void autoPoundEgg() {
        L.debug(TAG, "autoPoundEgg");
        if (mIsAuto && lotteryType == mEggFiftyType) {
            startLottery(lotteryType);
        }
    }

    private void resetView() {
        if (mIsAuto) {
            mEggTvAwardBtn.setText("停止");
        } else {
            if (lotteryType == mEggFirstType) {
                mEggTvAwardBtn.setText("再砸1次");
            } else if (lotteryType == mEggTenType) {
                mEggTvAwardBtn.setText("再砸10次");
            } else if (lotteryType == mEggFiftyType) {
                mEggTvAwardBtn.setText("再砸50次");
            }
        }
    }

    @OnClick(R2.id.egg_iv_award_close)
    public void onCloseClicked(View view) {
        L.debug(TAG, "onCloseClicked");
        resetState();
        closeView();
    }

    @OnClick(R2.id.egg_tv_award_btn)
    public void onAwardClicked(View view) {
        if (mLimitClickUtils.checkForTime(view.getId(), 500)) {
            return;
        }
        if (mIsAutoContinue) {
            mIsAuto = true;
            mIsAutoContinue = false;
            mEggTvAwardBtn.setText("停止");
            startLottery(lotteryType);
        } else {
            if (mIsAuto) {
                mIsAutoContinue = true;
                mEggTvAwardBtn.setText("继续");
                resetAnim();
                resetState();
            } else {
                startLottery(lotteryType);
            }
        }
    }

    private void resetState() {
        mIsAuto = false;
        setEnabled(mEggButtonLayout, true);
        setEnabled(mEggRootLayout, true);
        finishLottery();
    }

    private void closeView() {
        L.debug(TAG, "closeView");
        mIsAutoContinue = false;
        mEggClAwardLayout.setVisibility(View.GONE);
    }

    private void setEnabled(View view, boolean enabled) {
        if (null == view) {
            return;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            LinkedList<ViewGroup> queue = new LinkedList<ViewGroup>();
            queue.add(viewGroup);
            // 遍历viewGroup
            while (!queue.isEmpty()) {
                ViewGroup current = queue.removeFirst();
                current.setEnabled(enabled);
                for (int i = 0; i < current.getChildCount(); i++) {
                    if (current.getChildAt(i) instanceof ViewGroup) {
                        queue.addLast((ViewGroup) current.getChildAt(i));
                    } else {
                        current.getChildAt(i).setEnabled(enabled);
                    }
                }
            }
        } else {
            view.setEnabled(enabled);
        }
    }

    public void onLotteryError(String msg) {
        finishLottery();
        if (getContext() != null)
            SingleToastUtil.showShortToast(msg);
    }

    /**
     * 更新金额信息
     */
    private void updateWalletInfo(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldNum = walletInfo.getGoldNum();
            tvGoldCount.setText(getContext().getString(R.string.gold_num_text, goldNum));
        }
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onGetWalletInfo(WalletInfo walletInfo) {
        updateWalletInfo(walletInfo);
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        updateWalletInfo(walletInfo);
    }

    /**
     * 礼物数大于1时的操作
     */
    private void showFreeGiftDialog() {
        if (ivHammer == null) {
            return;
        }
        AnimationSet set = new AnimationSet(false);
        TranslateAnimation tanslate = new TranslateAnimation(0, -20, 0, 0);
        tanslate.setDuration(300);
        set.addAnimation(tanslate);
        RotateAnimation rotate = new RotateAnimation(0, -30, 100, 100);
        rotate.setDuration(300);
        set.addAnimation(rotate);
        set.setFillAfter(true);
        ivHammer.startAnimation(set);
        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ivLotteryBoxBox.setImageResource(R.drawable.ic_pound_egg_1);
                //打开宝箱
                animHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ivLotteryBoxBox.setImageResource(R.drawable.ic_pound_egg_2);
                    }
                }, 300);
                //礼物特效
                animHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ivHammer.clearAnimation();
                        ivHammer.setVisibility(View.GONE);
                        ivLotteryBoxBox.setImageResource(R.drawable.ic_pound_egg_3);
                        showDialogStyle(false);
                    }
                }, 600);
                //动画结束
                animHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        resetAnim();
                        finishLottery();
                        autoPoundEgg();
                    }
                }, 1500);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 执行礼物数为1时的操作动画
     *
     * @param gifts
     */
    private void showFreeGiftAnim(List<EggGiftInfo> gifts) {
        EggGiftInfo gift = gifts.get(0);
        if (gift.getGiftId() == 0) {
            finishLottery();
            return;
        }
        AnimationSet set = new AnimationSet(false);
        TranslateAnimation tanslate = new TranslateAnimation(0, -20, 0, 0);
        tanslate.setDuration(200);
        set.addAnimation(tanslate);
        RotateAnimation rotate = new RotateAnimation(0, -30, 100, 100);
        rotate.setDuration(200);
        set.addAnimation(rotate);
        set.setFillAfter(true);//动画执行完后是否停留在执行完的状态
        ivHammer.startAnimation(set);
        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ivLotteryBoxBox.setImageResource(R.drawable.ic_pound_egg_1);
                //打开宝箱
                animHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ivLotteryBoxBox.setImageResource(R.drawable.ic_pound_egg_2);
                    }
                }, 200);
                //礼物特效
                animHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ivHammer.clearAnimation();
                        ivHammer.setVisibility(View.GONE);
                        ivLotteryBoxBox.setImageResource(R.drawable.ic_pound_egg_3);
//                        updateGiftData(gifts);
                    }
                }, 300);
                //动画结束
                animHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        resetAnim();
                        finishLottery();
                        autoPoundEgg();
                    }
                }, 1500);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void setAdapter() {
        if (mAwardAdapter == null) {
            mAwardAdapter = new ListAdapter<EggGiftInfo>(getContext(), R.layout.egg_award_item_layout, mAwardList) {
                @Override
                public void onUpdate(BaseAdapterHelper helper, EggGiftInfo item, int position) {
                    ImageView giftView = helper.getView(R.id.egg_iv_award);
                    TextView numView = helper.getView(R.id.egg_iv_award_num);

                    int giftNum = item.getGiftNum();
                    numView.setText("x" + giftNum);
                    numView.setVisibility(View.VISIBLE);
                    numView.setTextColor(getResources().getColor(R.color.white));

                    String picUrl = item.getPicUrl();
                    if (!TextUtils.isEmpty(picUrl)) {
                        ImageLoadUtils.loadImage(getContext(), picUrl, giftView);
                    } else {
                        ImageLoadUtils.loadImage(getContext(), R.drawable.default_cover, giftView);
                    }

                    View view = helper.getView(R.id.egg_cl_award_item_root);
                    int price = item.getGoldPrice();
                    if (price >= 33440) {
                        view.setBackground(getResources().getDrawable(R.drawable.egg_pound_big_gift_bg));
                    } else if (price >= 520) {
                        view.setBackground(getResources().getDrawable(R.drawable.egg_pound_small_gift_bg));
                    } else {
                        view.setBackgroundColor(getResources().getColor(R.color.transparent));
//                        view.setBackground(null);
                    }
                }
            };
            mEggGvAward.setAdapter(mAwardAdapter);
        } else {
            mAwardAdapter.replaceAll(mAwardList);
            ObjectAnimator alpha = ObjectAnimator.ofFloat(mEggGvAward, "alpha", 0f, 1f);
            ObjectAnimator scaleX = ObjectAnimator.ofFloat(mEggGvAward, "scaleX", 0, 1f);
            ObjectAnimator scaleY = ObjectAnimator.ofFloat(mEggGvAward, "scaleY", 0, 1f);
            AnimatorSet animatorSet = new AnimatorSet();  //组合动画
            animatorSet.playTogether(alpha, scaleX, scaleY); //设置动画
            animatorSet.setDuration(600);  //设置动画时间
            animatorSet.start(); //启动
        }
    }

    static class LotteryHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {

        }
    }

}
