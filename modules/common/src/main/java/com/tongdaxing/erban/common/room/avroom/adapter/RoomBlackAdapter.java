package com.tongdaxing.erban.common.room.avroom.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.bindadapter.BindingViewHolder;
import com.tongdaxing.erban.common.ui.widget.SquareImageView;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;

/**
 * Created by huangmeng1 on 2018/1/18.
 */

public class RoomBlackAdapter extends BaseQuickAdapter<ChatRoomMember, BindingViewHolder> {
    private RoomBlackDelete roomBlackDelete;

    public RoomBlackAdapter(int layoutResId) {
        super(layoutResId);
    }

    public void setRoomBlackDelete(RoomBlackDelete roomBlackDelete) {
        this.roomBlackDelete = roomBlackDelete;
    }

    @Override
    protected void convert(BindingViewHolder helper, ChatRoomMember item) {
        LinearLayout container = helper.getView(R.id.container);
        TextView tvName = helper.getView(R.id.tv_userName);
        TextView tvDelete = helper.getView(R.id.tv_delete);
        SquareImageView squareImageView = helper.getView(R.id.imageView);

        ImageLoadUtils.loadImage(container.getContext(), item.getAvatar(), squareImageView);

        tvName.setText(item.getNick());
        ViewGroup.LayoutParams layoutParams = container.getLayoutParams();
        layoutParams.width = UIUtil.getScreenWidth(mContext);
        container.setLayoutParams(layoutParams);
        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (roomBlackDelete != null) {
                    roomBlackDelete.onDeleteClick(item);
                }
            }
        });
    }

    public interface RoomBlackDelete {
        void onDeleteClick(ChatRoomMember chatRoomMember);
    }
}
