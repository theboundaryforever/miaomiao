package com.tongdaxing.erban.common.ui.me.shopping.adapter;


import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.view.LevelView;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;


public class GiveGoodsAdapter extends BaseQuickAdapter<Json, GiveGoodsAdapter.ViewHolder> {


    public ItemAction itemAction;

    public GiveGoodsAdapter(@Nullable List<Json> data) {
        super(R.layout.list_item_share_fans, data);
    }

    @Override
    protected void convert(ViewHolder viewHolder, Json json) {

        ImageLoadUtils.loadImage(mContext, json.str("avatar"), viewHolder.imageView);
        String nick = json.str("nick");
        viewHolder.tvItemName.setText(nick);

        viewHolder.levelViewNewUserList.setExperLevel(json.num("experLevel"));


        String uid = json.str("uid");
        viewHolder.buInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAction != null)
                    itemAction.itemClickAction(uid, nick);
            }
        });


    }

    public interface ItemAction {
        void itemClickAction(String uid, String userName);

    }

    public class ViewHolder extends BaseViewHolder {

        //        @BindView(R2.id.imageView)
        ImageView imageView;
        //        @BindView(R2.id.tv_item_name)
        TextView tvItemName;
        //        @BindView(R2.id.level_view_new_user_list)
        LevelView levelViewNewUserList;
        //        @BindView(R2.id.iv_new_user_item_sex)
//        ImageView ivNewUserItemSex;
//        @BindView(R2.id.tv_new_user_item_age)
//        TextView tvNewUserItemAge;
//        @BindView(R2.id.top_line)
//        View topLine;
//        @BindView(R2.id.bu_invite)
        Button buInvite;
        //        @BindView(R2.id.iv_share_fans_option)
        ImageView ivShareFansOption;
//        @BindView(R2.id.container)
//        RelativeLayout container;

        public ViewHolder(View view) {
            super(view);
//            ButterKnife.bind(this, view);
            imageView = view.findViewById(R.id.imageView);
            tvItemName = view.findViewById(R.id.tv_item_name);
            buInvite = view.findViewById(R.id.bu_invite);
            buInvite.setText("赠送");
            ivShareFansOption = view.findViewById(R.id.iv_share_fans_option);
            levelViewNewUserList = view.findViewById(R.id.level_view_new_user_list);
        }
    }


}
