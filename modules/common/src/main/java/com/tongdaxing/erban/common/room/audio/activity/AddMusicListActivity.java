package com.tongdaxing.erban.common.room.audio.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.room.audio.adapter.AddMusicListAdapter;
import com.tongdaxing.erban.common.room.audio.widget.VoiceSeekDialog;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenran
 * @date 2017/10/28
 */
@Deprecated
public class AddMusicListActivity extends BaseActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    @BindView(R2.id.image_bg)
    ImageView imageBg;
    @BindView(R2.id.transparent_view)
    View transparentView;
    @BindView(R2.id.back_btn)
    ImageView backBtn;
    @BindView(R2.id.add_music_btn)
    ImageView addMusicBtn;
    @BindView(R2.id.title_layout)
    RelativeLayout titleLayout;
    @BindView(R2.id.music_play_pause_btn)
    ImageView musicPlayPauseBtn;
    @BindView(R2.id.song_name)
    TextView songName;
    @BindView(R2.id.artist_name)
    TextView artistName;
    @BindView(R2.id.music_adjust_voice)
    ImageView musicAdjustVoice;
    @BindView(R2.id.music_box_layout)
    LinearLayout musicBoxLayout;
    @BindView(R2.id.empty_layout_music_add)
    ImageView emptyLayoutMusicAdd;
    @BindView(R2.id.empty_bg)
    LinearLayout emptyBg;
    @BindView(R2.id.recycler_view)
    RecyclerView recyclerView;
    private AddMusicListAdapter adapter;
    private String imgBgUrl;
    private MusicLocalInfo current;

    public static void start(Context context, String imgBgUrl) {
        Intent intent = new Intent(context, AddMusicListActivity.class);
        intent.putExtra("imgBgUrl", imgBgUrl);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_music_list);
        ButterKnife.bind(this);
        imgBgUrl = getIntent().getStringExtra("imgBgUrl");
        initView();
        initData();

//        if (!StringUtil.isEmpty(imgBgUrl)) {
//            ImageLoadUtils.loadImageWithBlurTransformation(this, imgBgUrl, imageBg);
//        }

//        View content = findViewById(android.R.id.content);
//        ViewGroup.LayoutParams params = content.getLayoutParams();
//        params.height = getResources().getDisplayMetrics().heightPixels;
    }

    @Override
    public boolean blackStatusBar() {
        return false;
    }

    private void initView() {
//        voiceSeek = (SeekBar) findViewById(R.id.voice_seek);
//        voiceSeek.setOnSeekBarChangeListener(this);
//        voiceSeek.setMax(100);
//        voiceSeek.setProgress(CoreManager.getCore(IPlayerCore.class).getCurrentVolume());
    }

    private void initData() {
        List<MusicLocalInfo> localMusicInfoList = CoreManager.getCore(IPlayerCore.class).requestPlayerListLocalMusicInfos();
        adapter = new AddMusicListAdapter(this);
        adapter.setLocalMusicInfos(localMusicInfoList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if (localMusicInfoList == null || localMusicInfoList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyBg.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyBg.setVisibility(View.GONE);
        }

        current = CoreManager.getCore(IPlayerCore.class).getCurrent();
        updateView();
    }

    @Override
    protected boolean needSteepStateBar() {
        return false;
    }

    @OnClick({R2.id.add_music_btn, R2.id.back_btn, R2.id.music_play_pause_btn, R2.id.empty_layout_music_add, R2.id.music_adjust_voice})
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.add_music_btn) {
            LocalMusicListActivity.start(this, imgBgUrl);

        } else if (i == R.id.back_btn) {
            finish();

        } else if (i == R.id.music_play_pause_btn) {
            int state = CoreManager.getCore(IPlayerCore.class).getState();
            if (state == IPlayerCore.STATE_PLAY) {
                CoreManager.getCore(IPlayerCore.class).pause();
            } else if (state == IPlayerCore.STATE_PAUSE) {
                int result = CoreManager.getCore(IPlayerCore.class).play((MusicLocalInfo) null);
                if (result < 0) {
                    toast("播放失败，文件异常");
                }
            } else {
//                    int result = CoreManager.getCore(IPlayerCore.class).playNext();
//                    if (result < 0) {
//                        if (result == -3) {
//                            toast("播放列表中还没有歌曲哦！");
//                        } else {
//                            toast("播放失败，文件异常");
//                        }
//                    }
            }

        } else if (i == R.id.empty_layout_music_add) {
            LocalMusicListActivity.start(this, imgBgUrl);

        } else if (i == R.id.music_adjust_voice) {
            VoiceSeekDialog voiceSeekDialog = new VoiceSeekDialog(this);
            voiceSeekDialog.show();

        } else {
        }
    }

    private void updateView() {
        if (current != null) {
            songName.setText(current.getSongName());

            if (current.getArtistNames() != null && current.getArtistNames().size() > 0) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < current.getArtistNames().size(); i++) {
                    String artistName = current.getArtistNames().get(i);
                    sb.append(artistName);
                    sb.append(",");
                }
                sb.deleteCharAt(sb.length() - 1);
                artistName.setText(sb.toString());
            } else {
                artistName.setText("");
            }

            int state = CoreManager.getCore(IPlayerCore.class).getState();
            if (state == IPlayerCore.STATE_PLAY) {
                musicPlayPauseBtn.setImageResource(R.drawable.icon_music_play_big);
            } else {
                musicPlayPauseBtn.setImageResource(R.drawable.icon_music_pause);
            }
        } else {
            songName.setText("暂无歌曲播放");
            artistName.setText("");
            musicPlayPauseBtn.setImageResource(R.drawable.icon_music_pause);
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(MusicLocalInfo localMusicInfo) {
        this.current = localMusicInfo;
        updateView();
        adapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(MusicLocalInfo localMusicInfo) {
        this.current = localMusicInfo;
        updateView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop(MusicLocalInfo localMusicInfo) {
        this.current = null;
        updateView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onCurrentMusicUpdate(MusicLocalInfo localMusicInfo) {
        this.current = localMusicInfo;
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onRefreshPlayerList(List<MusicLocalInfo> playerListMusicInfoList) {
        if (playerListMusicInfoList == null || playerListMusicInfoList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyBg.setVisibility(View.VISIBLE);
            adapter.setLocalMusicInfos(null);
            adapter.notifyDataSetChanged();
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyBg.setVisibility(View.GONE);
            adapter.setLocalMusicInfos(playerListMusicInfoList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        CoreManager.getCore(IPlayerCore.class).seekVolume(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onBeKickOut(ChatRoomKickOutEvent.ChatRoomKickOutReason reason) {
        finish();
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }
}
