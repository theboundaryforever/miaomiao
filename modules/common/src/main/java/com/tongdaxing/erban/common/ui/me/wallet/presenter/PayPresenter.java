package com.tongdaxing.erban.common.ui.me.wallet.presenter;

import com.tongdaxing.erban.common.ui.me.wallet.model.PayModel;
import com.tongdaxing.erban.common.ui.me.wallet.view.IPayView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.result.WalletInfoResult;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public class PayPresenter<T extends IPayView> extends AbstractMvpPresenter<T> {

    protected PayModel payModel;
    protected WalletInfo walletInfo;

    public PayPresenter() {
        this.payModel = new PayModel();
    }

    /**
     * 刷新钱包信息
     */
    public void refreshWalletInfo(boolean force) {
//        String cacheStrategy = force ? "no-cache" : "max-stale";
//        execute(payModel.getPayService().getMyWallet(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()),
//                CoreManager.getCore(IAuthCore.class).getTicket(), cacheStrategy),
//                new CallBack<WalletInfo>() {
//                    @Override
//                    public void onSuccess(WalletInfo data) {
//                        // cache walletInfo for calling in the future
//                        walletInfo = data;
//                        getMvpView().setupUserWalletBalance(data);
//                    }
//
//                    @Override
//                    public void onFail(int code, String error) {
//                        getMvpView().getUserWalletInfoFail(error);
//                    }
//                });
        String cacheStrategy = force ? "no-cache" : "max-stale";
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("Cache-Control", cacheStrategy);
        ResponseListener listener = new ResponseListener<WalletInfoResult>() {
            @Override
            public void onResponse(WalletInfoResult response) {
                if (null != response && response.isSuccess() && response.getData() != null) {
                    walletInfo = response.getData();
                    if (getMvpView() != null)
                        getMvpView().setupUserWalletBalance(response.getData());
                } else {
                    if (getMvpView() != null && response != null)
                        getMvpView().getUserWalletInfoFail(response.getErrorMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if (getMvpView() != null && error != null)
                    getMvpView().getUserWalletInfoFail(error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getWalletInfos(),
                        CommonParamUtil.getDefaultHeaders(),
                        params, listener, errorListener,
                        WalletInfoResult.class, Request.Method.GET);
    }
}
