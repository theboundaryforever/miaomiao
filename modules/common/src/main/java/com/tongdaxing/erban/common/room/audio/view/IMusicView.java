package com.tongdaxing.erban.common.room.audio.view;

import android.view.View;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public interface IMusicView extends IMvpBaseView {
    void onItemClick(MyMusicInfo musicInfo, View view, int position);
}
