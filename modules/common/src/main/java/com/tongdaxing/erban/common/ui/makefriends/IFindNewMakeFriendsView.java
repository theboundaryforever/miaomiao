package com.tongdaxing.erban.common.ui.makefriends;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.im.custom.bean.FriendsBroadcastAttachment;

import java.util.List;

/**
 * 发现-交友页
 * <p>
 * Created by zhangjian on 2019/6/13.
 */
public interface IFindNewMakeFriendsView extends IMvpBaseView {

    void setupView(List<FriendsBroadcastAttachment> broadcastBeans);

    void addAttachment(FriendsBroadcastAttachment attachment);
}
