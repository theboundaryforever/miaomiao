package com.tongdaxing.erban.common.ui.me.setting.notificationSetting;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * <p>
 * Created by zhangjian on 2019/7/18.
 */
public class NotificationSettingModel {
    /**
     * 所有通知开关
     */
    public static final String all = "all";
    /**
     * 通知权限开关
     */
    public static final String sysNoticeOption = "sysNoticeOption";
    /**
     * 关注通知开关
     */
    public static final String focusNoticeOption = "focusNoticeOption";
    /**
     * 关注的房间的通知开关
     */
    public static final String roomNoticeOption = "roomNoticeOption";
    /**
     * 声控星球的开关
     */
    public static final String soundNoticeOption = "soundNoticeOption";
    /**
     * 声圈动态的开关
     */
    public static final String monentNoticeOption = "monentNoticeOption";

    /**
     * 获取免打扰状态
     */
    public void getMsgDisturbState(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        String uid = CoreManager.getCore(IAuthCore.class).getCurrentUid() + "";
        params.put("queryUid", uid);
        params.put("uid", uid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getFocusMsgSwitch(), params, myCallBack);
    }

    /**
     * 修改免打扰状态
     *
     * @param state
     */
    public void changeMsgDisturbState(int state, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("chatPermission", state + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.saveFocusMsgSwitch(), params, myCallBack);
    }

    /**
     * 修改免打扰状态
     *
     * @param configId
     * @param configValue 0关1开
     */
    public void getSetNoticeOption(String configId, int configValue, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("configId", configId);
        params.put("configValue", configValue + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getSetNoticeOption(), params, myCallBack);
    }
}
