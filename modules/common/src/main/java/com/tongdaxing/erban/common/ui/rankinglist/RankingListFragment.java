package com.tongdaxing.erban.common.ui.rankinglist;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.presenter.rankinglist.IRankingListView;
import com.tongdaxing.erban.common.presenter.rankinglist.RankingListPresenter;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.find.activity.CreateRoomActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.NumberFormatUtils;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.bean.RankingInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 排行榜 fragment
 */
@CreatePresenter(RankingListPresenter.class)
public class RankingListFragment extends BaseMvpFragment<IRankingListView, RankingListPresenter> implements IRankingListView {

    //魅力榜、土豪榜、CP榜
    public static final int RANKING_TYPE_CHARM = 0, RANKING_TYPE_TYCOON = 1, RANKING_TYPE_CP = 2;
    private static final String RANKING_TYPE = "rankingType";
    @BindView(R2.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R2.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R2.id.ranking_list_my_ranking)
    TextView rankingListMyRanking;
    @BindView(R2.id.ranking_list_my_avatar)
    CircleImageView rankingListMyAvatar;
    @BindView(R2.id.ranking_list_her_avatar)
    CircleImageView rankingListHerAvatar;
    @BindView(R2.id.ranking_list_my_ranking_is_on)
    TextView rankingListMyRankingIsOn;
    @BindView(R2.id.ranking_list_my_ranking_receive_number)
    TextView rankingListMyRankingReceiveNumber;
    @BindView(R2.id.ranking_list_my_ranking_distance)
    TextView rankingListMyRankingDistance;
    @BindView(R2.id.ranking_list_my_ranking_distance_text)
    TextView rankingListMyRankingDistanceText;
    @BindView(R2.id.ranking_list_my_group_cp)
    TextView rankingListMyGroupCp;
    @BindView(R2.id.ranking_list_myself)
    ConstraintLayout rankingListMyself;

    private View headerView;

    private BaseQuickAdapter adapter;
    //    private CircleImageView avatarFirst, avatarSecond, avatarThird;
    private int rankingType = RANKING_TYPE_CHARM;
    private int dateType;// 日、周、总

    /**
     * @param rankingType 排行榜类型 RankingListFragment.RANKING_TYPE_CHARM or RankingListFragment.RANKING_TYPE_TYCOON
     */
    public static RankingListFragment newInstance(int rankingType) {
        Bundle args = new Bundle();
        args.putInt(RANKING_TYPE, rankingType);
        RankingListFragment fragment = new RankingListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_ranking_list;
    }

    @Override
    public void onFindViews() {
        CoreUtils.register(this);
        if (getArguments() != null) {
            rankingType = getArguments().getInt(RANKING_TYPE, RANKING_TYPE_CHARM);
        }
        //1、2、3名在headerView
        refreshLayout = mView.findViewById(R.id.refresh_layout);
        recyclerView = mView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        if (rankingType == RANKING_TYPE_CP) {
            headerView = LayoutInflater.from(mContext).inflate(R.layout.layout_ranking_list_cp_header, null);
        } else {
            headerView = LayoutInflater.from(mContext).inflate(R.layout.layout_ranking_list_header, null);
        }
    }

    @Override
    public void onSetListener() {
        refreshLayout.setColorSchemeColors(ContextCompat.getColor(mContext, R.color.common_theme_pink), ContextCompat.getColor(mContext, R.color.common_theme_blue));
        refreshLayout.setOnRefreshListener(() -> getMvpPresenter().refreshData(rankingType, dateType));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTabClick(TabRadioGroupEvent.OnTabClick onTabClick) {
        if (onTabClick.getRankingType() == rankingType) {
            dateType = onTabClick.getPosition();
            getMvpPresenter().refreshData(rankingType, dateType);
        }
    }

    @Override
    public void initiate() {
        if (rankingType == RANKING_TYPE_CP) {
            adapter = new CpRankingListAdapter(R.layout.item_cp_ranking_list, mContext, rankingType);
        } else {
            adapter = new RankingListAdapter(R.layout.item_ranking_list, mContext, rankingType);
        }
        adapter.setHeaderView(headerView);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setupFailView(String message) {
        toast(message);
        refreshLayout.setRefreshing(false);
        adapter.setNewData(null);
    }

    @Override
    public void setupSuccessView(RankingInfo rankingList) {
        refreshLayout.setRefreshing(false);
        recyclerView.post(() -> adapter.setNewData(rankingList.getRankVoList()));

        RankingInfo.Me me = rankingList.getMe();
        if (me == null) {
            me = new RankingInfo.Me();
        }
        //头像
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo != null) {
            if (rankingType == RANKING_TYPE_CP) {
                if (cacheLoginUserInfo.getCpUser() != null) {
                    rankingListHerAvatar.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadAvatar(mContext, cacheLoginUserInfo.getCpUser().getAvatar(), rankingListHerAvatar);
                }
            }
            ImageLoadUtils.loadAvatar(mContext, cacheLoginUserInfo.getAvatar(), rankingListMyAvatar);
        }
        //时间
        String time = "";
        switch (rankingType == RANKING_TYPE_CP ? dateType + 1 : dateType) {
            case 0:
                time = getString(R.string.ranking_list_day);
                break;
            case 1:
                time = getString(R.string.ranking_list_week);
                break;
            case 2:
                time = getString(R.string.ranking_list_all);
        }
        //排名
        if (me.getSeqNo() <= 0) {
            //未上榜
            if (rankingType == RANKING_TYPE_CP) {
                if (cacheLoginUserInfo != null && cacheLoginUserInfo.getCpUser() == null) {
                    //没有CP 显示去组CP
                    rankingListMyRankingIsOn.setText(R.string.ranking_list_no_cp);
                    rankingListMyGroupCp.setVisibility(View.VISIBLE);
                    rankingListMyRankingDistance.setVisibility(View.GONE);
                    rankingListMyRankingDistanceText.setVisibility(View.GONE);
                } else {
                    //有CP 但未上榜
                    if (dateType == 0) {
                        //这周未上榜
                        rankingListMyRankingIsOn.setText(time.concat(getString(R.string.ranking_list_no_cp_seq)));
                    } else {
                        //未上榜
                        rankingListMyRankingIsOn.setText(R.string.ranking_list_no_cp_seq);
                    }
                }
            } else {
                //我未上榜
                rankingListMyRankingIsOn.setText(R.string.ranking_list_no_seq);
                rankingListMyRanking.setVisibility(View.GONE);
                rankingListMyRankingDistance.setVisibility(View.GONE);
                rankingListMyRankingDistanceText.setVisibility(View.GONE);
            }
        } else {
            rankingListMyRanking.setVisibility(View.VISIBLE);
            rankingListMyRankingDistance.setVisibility(View.VISIBLE);
            rankingListMyRankingDistanceText.setVisibility(View.VISIBLE);
            rankingListMyRanking.setText(String.valueOf(me.getSeqNo()));
            if (dateType == 2) {
                //总榜已上榜
                rankingListMyRankingIsOn.setText("总榜".concat(getString(R.string.ranking_list_have_seq)));
            } else {
                if (rankingType == RANKING_TYPE_CP) {
                    if (dateType == 0) {
                        //这周已上榜
                        rankingListMyRankingIsOn.setText(getString(R.string.ranking_list_week).concat(getString(R.string.ranking_list_have_seq)));
                    } else {
                        //已上榜
                        rankingListMyRankingIsOn.setText(R.string.ranking_list_have_seq);
                    }
                } else {
                    //今日/这周已上榜
                    rankingListMyRankingIsOn.setText(time.concat(getString(R.string.ranking_list_have_seq)));
                }
            }
        }
        //收到喵币/亲密值
        if (rankingType == RANKING_TYPE_CP) {
            if (me.getTotalNum() == 0) {
                //暂没亲密值...
                rankingListMyRankingReceiveNumber.setText(getString(R.string.ranking_list_no_cp_love));
            } else {
                //这周/总共收到xxx亲密值
                rankingListMyRankingReceiveNumber.setText(time.concat(String.format(getString(R.string.ranking_list_have_cp_love_desc), me.getTotalNum())));
            }
        } else if (rankingType == RANKING_TYPE_CHARM) {
            if (me.getTotalNum() == 0) {
                //暂没收到喵币...
                rankingListMyRankingReceiveNumber.setText(getString(R.string.ranking_list_no_miao_gold));
            } else {
                //今日/这周/总共收到xxx喵币
                rankingListMyRankingReceiveNumber.setText(time.concat(String.format(getString(R.string.ranking_list_have_seq_desc), me.getTotalNum())));
            }
        } else {
            if (me.getTotalNum() == 0) {
                //暂没送出喵币...
                rankingListMyRankingReceiveNumber.setText(getString(R.string.ranking_list_no_give_miao_gold));
            } else {
                //今日/这周/总共送出xxx喵币
                rankingListMyRankingReceiveNumber.setText(time.concat(String.format(getString(R.string.ranking_list_tycoon_seq_desc), me.getTotalNum())));
            }
        }
        if (me.getSeqNo() == 1) {
            rankingListMyRankingDistance.setVisibility(View.GONE);
            rankingListMyRankingDistanceText.setVisibility(View.GONE);
        } else {
            //距离前一名相差多少
            if (me.getDistance() == 0) {//0时显示1
                rankingListMyRankingDistance.setText("0");
            } else if (me.getDistance() == -1) {//-1代表不显示
                rankingListMyRankingDistance.setText("");
            } else if (me.getDistance() >= 10000) {
                rankingListMyRankingDistance.setText(NumberFormatUtils.formatDoubleDecimalPointWithMax2Digit(me.getDistance() / 10000) + "万");
            } else {
                rankingListMyRankingDistance.setText(String.valueOf((long) me.getDistance()));
            }
        }
        Drawable drawable;
        if (rankingType == RANKING_TYPE_CP) {
            drawable = mContext.getResources().getDrawable(R.drawable.ranking_list_love);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            rankingListMyRankingDistance.setCompoundDrawables(drawable, null, null, null);
        }
    }

    @OnClick(R2.id.ranking_list_my_group_cp)
    public void onGroupCpClicked() {
        //打开创建房间 选中情侣陪伴
        UmengEventUtil.getInstance().onCreateRoom(mContext, "rankingList");
        CreateRoomActivity.start(mContext, 2);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        CoreUtils.unregister(this);
    }
}
