package com.tongdaxing.erban.common.ui.web;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.TResult;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.constant.BaseUrl;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.audiocard.AudioCard2Activity;
import com.tongdaxing.erban.common.ui.widget.dialog.shareDialog.NewShareDialog;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.common.ICommonClient;
import com.tongdaxing.xchat_core.count.IChargeClient;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCoreClient;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.file.JXFileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.moments.WechatMoments;


public class CommonWebViewActivity extends TakePhotoActivity implements NewShareDialog.OnShareDialogItemClick {
    private static final String CAMERA_PREFIX = "picture_";
    private static final String POSITION = "position";
    private String TAG = "CommonWebViewActivity";
    private TitleBar mTitleBar;
    private WebView webView;
    private CommonWebViewActivity mActivity;
    private WebChromeClient wvcc;
    private ImageView imgShare;
    private TextView tvOption;//右上角提供文字选项，与分享功能不同时显示
    private WebViewInfo webViewInfo = null;
    private String url;
    private String option;//文字选项
    private JSInterface jsInterface;
    private int mPosition;

    private int mProgress;

    private Handler mHandler = new Handler(Looper.getMainLooper());
    private boolean isShareToMoments = false;

    public void showShareToMomentsProgress() {
        isShareToMoments = true;
        getDialogManager().showProgressDialog(this, "请稍后...", true);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        getDialogManager().dismissDialog();
        if (isShareToMoments) {
            WebViewInfo webViewInfo = new WebViewInfo();
            webViewInfo.setTitle("喵喵，语音交友神器！");
            webViewInfo.setImgUrl(url);
            webViewInfo.setDesc("喵喵交友速配处CP，你想要的我们都有。");
            webViewInfo.setShowUrl(BaseUrl.SHARE_DOWNLOAD);
            CoreManager.getCore(IShareCore.class).sharePage(webViewInfo, ShareSDK.getPlatform(WechatMoments.NAME));
        } else {
            jsInterface.onImageChooserResult(url);
        }
        isShareToMoments = false;
    }

    public static void startForResult(Context context, String url, int requestCode) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        if (context instanceof Activity) {
            ((Activity) context).startActivityForResult(intent, requestCode);
        }
    }

    /**
     * 右上角文字选项
     *
     * @param context
     * @param url
     */
    public static void startForResult(Context context, String url, int requestCode, String option) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("option", option);
        if (context instanceof Activity) {
            ((Activity) context).startActivityForResult(intent, requestCode);
        }
    }

    public static void start(Context context, String url) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        context.startActivity(intent);
    }

    /**
     * 右上角文字选项
     *
     * @param context
     * @param url
     */
    public static void start(Context context, String url, String option) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("option", option);
        context.startActivity(intent);
    }

    /**
     * 排行榜专用
     */
    public static void start(Context context, String url, int position) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra(POSITION, position);
        context.startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    webView.evaluateJavascript("onStart()", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {

                        }
                    });
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_web_view);
        initTitleBar("");
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        option = intent.getStringExtra("option");
        LogUtil.e("mylog", url);
        mPosition = intent.getIntExtra(POSITION, 0) + 1;
        mActivity = this;
        initView();
        setListener();
        ShowWebView(url);
    }

    private void setListener() {
        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewShareDialog shareDialog = new NewShareDialog(mActivity);
                shareDialog.setOnShareDialogItemClick(CommonWebViewActivity.this);
                shareDialog.show();
            }
        });
    }


    private void initView() {
        webView = (WebView) findViewById(R.id.webview);
        mTitleBar = (TitleBar) findViewById(R.id.title);
        imgShare = (ImageView) findViewById(R.id.img_share);
        tvOption = (TextView) findViewById(R.id.tv_option);

        if (!TextUtils.isEmpty(option)) {
            tvOption.setVisibility(View.VISIBLE);
            imgShare.setVisibility(View.GONE);
            tvOption.setText(option);

            tvOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getString(R.string.audio_card_clear).equals(option)) {
                        // 声鉴卡清空声音功能
                        clearAudioCard();
                    }
                }
            });
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initData() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        jsInterface = new JSInterface(webView, this);
        jsInterface.setPosition(mPosition);
        webView.addJavascriptInterface(jsInterface, "androidJsObj");
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//                super.onReceivedSslError(view, handler, error); //一定要去掉
                // handler.cancel();// Android默认的处理方式
                handler.proceed();// 接受所有网站的证书
                // handleMessage(Message msg);// 进行其他处理
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                try {
                    webView.evaluateJavascript("shareInfo()", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            com.tongdaxing.xchat_framework.util.util.LogUtil.d("onReceiveValue11", webViewInfo + "");
                            if (!StringUtil.isEmpty(value) || !value.equals("null")) {
                                String jsonData = value.replace("\\", "");
                                L.debug(TAG, "onPageFinished evaluateJavascript() onReceiveValue() jsonData = %s", jsonData);
                                if (jsonData.indexOf("\"") == 0) {
                                    //去掉第一个 "
                                    jsonData = jsonData.substring(1, jsonData.length());
                                }
                                if (jsonData.lastIndexOf("\"") == (jsonData.length() - 1)) {
                                    jsonData = jsonData.substring(0, jsonData.length() - 1);
                                }
                                L.debug(TAG, "onPageFinished evaluateJavascript() onReceiveValue() after deal with: jsonData =  %s", jsonData);
                                try {
                                    Gson gson = new Gson();
                                    webViewInfo = gson.fromJson(jsonData, WebViewInfo.class);
                                    com.tongdaxing.xchat_framework.util.util.LogUtil.d("onReceiveValue", webViewInfo + "");
                                } catch (JsonSyntaxException e) {
                                    L.error(TAG, "onPageFinished evaluateJavascript() onReceiveValue() error: ", e);
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    L.error(TAG, "onPageFinished evaluateJavascript() error: ", e);
                    e.printStackTrace();
                    com.tongdaxing.xchat_framework.util.util.LogUtil.d("onReceiveValue11Exception", webViewInfo + "   " + e.toString());
                }
                super.onPageFinished(view, url);
            }
        });
        //获取webviewtitle作为titlebar的title
        wvcc = new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                mTitleBar.setTitle(title);

            }
        };
        // 设置setWebChromeClient对象
        webView.setWebChromeClient(wvcc);
        if (mTitleBar != null) {
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.back_font));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        finish();
                    }
                }
            });
        }
        // 设置Webview的user-agent
        webView.getSettings().setUserAgentString(webView.getSettings().getUserAgentString() + " miaomiaoAppAndroid");//todo 这个换成miaomiao有没有问题

        // 允许http 与 https 混合页面访问
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    public void ShowWebView(String url) {
        webView.loadUrl(url);
        initData();
    }

    @Override
    public void onSharePlatformClick(Platform platform) {
        L.debug(TAG, "onSharePlatformClick: %s, webViewInfo == null: %b", platform.getName(), webViewInfo == null);
        if (webViewInfo != null) {
            CoreManager.getCore(IShareCore.class).shareH5(webViewInfo, platform);
//            LogUtil.d("onSharePlatformClick",webViewInfo+"");
        }
    }

    @Override
    public void onSendBroadcastMsg() {

    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onShareWebView() {
        toast("分享成功");
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onShareWebViewError() {
        toast("分享失败，请重试");
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onShareWebViewCanle() {
        toast("取消分享");
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onShareViewRedError(String error) {
        toast(error);
    }

    @CoreEvent(coreClientClass = ICommonClient.class)
    public void onRecieveNeedRefreshWebView() {
        if (!StringUtil.isEmpty(url)) {
            ShowWebView(url);
        }
    }

    @CoreEvent(coreClientClass = IChargeClient.class)
    public void chargeAction(String action) {
//        webView.loadUrl(url);
        LogUtil.d("chargeAction", "sdad");
        try {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    webView.evaluateJavascript("getStatus()", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {

                        }
                    });
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showImageChooser() {
        //图片压缩配置 暂时写死 长或宽不超过当前屏幕的长或宽、图片大小不超过2M
        CompressConfig compressConfig = new CompressConfig.Builder().setMaxPixel(DisplayUtils.getScreenHeight(this)).setMaxSize(2048 * 1024).create();
        getTakePhoto().onEnableCompress(compressConfig, true);

        ButtonItem buttonItem = new ButtonItem("拍照上传", () ->
                //授权检查
                checkPermission(this::takePhoto, R.string.ask_camera, android.Manifest.permission.CAMERA));
        ButtonItem buttonItem1 = new ButtonItem("本地相册", () -> {
            getTakePhoto().onPickFromGallery();

        });
        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(buttonItem);
        buttonItems.add(buttonItem1);
        getDialogManager().showCommonPopupDialog(buttonItems, "取消", false);
    }

    public ImageView getImgShare() {
        return imgShare;
    }

    public String getOption() {
        return option;
    }

    private void takePhoto() {
        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
        File cameraOutFile = JXFileUtils.getTempFile(CommonWebViewActivity.this, mCameraCapturingName);
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        getTakePhoto().onPickFromCapture(uri);
    }

    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(CommonWebViewActivity.this, "请稍后");
        CoreManager.getCore(IFileCore.class).upload(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
        jsInterface.onImageChooserResult("");
    }


    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        toast("上传失败");
        getDialogManager().dismissDialog();
        jsInterface.onImageChooserResult("");
    }

    /**
     * 清空声卡
     */
    private void clearAudioCard() {
        getDialogManager().showOkCancelDialog(getResources().getString(R.string.audio_card_clear_tip), true, new DialogManager.OkCancelDialogListener() {
            @Override
            public void onCancel() {

            }

            @Override
            public void onOk() {
                UserInfo user = new UserInfo();
                user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                user.setUserVoice("");
                user.setVoiceDura(0);
                getDialogManager().showProgressDialog(CommonWebViewActivity.this, getString(R.string.please_wait));
                CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user, false, "userVoice", "voiceDura");
            }
        });
    }

    /**
     * 声鉴卡清空声音的回调
     *
     * @param userInfo
     */
    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        toast(R.string.audio_card_clear_success);
        getDialogManager().dismissDialog();
        Intent intent = new Intent();
        intent.putExtra(AudioCard2Activity.AUDIO_FILE_URL, "");
        intent.putExtra(AudioCard2Activity.AUDIO_LENGTH, 0);
        setResult(Activity.RESULT_OK, intent);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        toast(error);
        getDialogManager().dismissDialog();
    }

    //调用系统按键返回上一层
    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        webView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        webView.onPause();
    }

    @Override
    protected void onDestroy() {
        long userId = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        //刷新礼物数据
        CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        CoreManager.getCore(IUserCore.class).requestUserInfo(userId);//WEB充值大礼，领取奖励之后需要刷新UserInfo。由于web领取之后没有回调方法，因此迫不得已只能暂时在这里做刷新操作
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        try {
            ((ViewGroup) webView.getParent()).removeView(webView);
            webView.removeAllViews();
            webView.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onBinderPhone() {
        onRecieveNeedRefreshWebView();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onMoidfyOnBiner() {
        onRecieveNeedRefreshWebView();
    }
}
