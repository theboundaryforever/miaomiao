package com.tongdaxing.erban.common.ui.find.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.find.bean.PublishVoiceGroupInfo;

public interface IPublishVoiceGroupView extends IMvpBaseView {
    void repeatSendVoiceGroup(String msgHint);

    void publishSucceed(PublishVoiceGroupInfo publishVoiceGroupInfo);

    void publishFailure(String errorStr);

    /**
     * 发布失败，内容含有敏感词
     */
    void publishFailureSensitiveWord();

    /**
     * 发布失败，图片违规
     *
     * @param publishVoiceGroupInfo
     */
    void publishFailureImgViolations(PublishVoiceGroupInfo publishVoiceGroupInfo);
}
