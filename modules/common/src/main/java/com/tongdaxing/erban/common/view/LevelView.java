package com.tongdaxing.erban.common.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tongdaxing.erban.common.R;


/**
 * Created by Administrator on 2018/3/10.
 */

public class LevelView extends LinearLayout {

    //解决聊天室公屏占位过多
    public View seatView;
    ImageView mExperLevel;
    ImageView mCharmLevel;
    private Context mContext;
    private View mInflate;

    public LevelView(Context context) {
        this(context, null);
    }

    public LevelView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mInflate = LayoutInflater.from(context).inflate(R.layout.level_view, this);
        mCharmLevel = (ImageView) mInflate.findViewById(R.id.charm_level);
        mExperLevel = (ImageView) mInflate.findViewById(R.id.exper_level);
        seatView = mInflate.findViewById(R.id.seat);

    }

    public ImageView getExperLevel() {

        return mExperLevel;
    }

    public void setExperLevel(int experLevel) {
        if (experLevel < 1) {
            mExperLevel.setVisibility(GONE);
            seatView.setVisibility(GONE);
            return;
        }
        if (experLevel > 50) {
            experLevel = 50;
        }
        mExperLevel.setVisibility(VISIBLE);

        int drawableId = getResources().getIdentifier("lv" + experLevel, "drawable", mContext.getPackageName());
        mExperLevel.setImageResource(drawableId);
    }

    public ImageView getCharmLevel() {
        return mCharmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        if (charmLevel < 1) {
            mCharmLevel.setVisibility(GONE);
            return;
        }

        if (charmLevel >= 50) {
            charmLevel = 50;
        }
        mCharmLevel.setVisibility(VISIBLE);
        int drawableId = getResources().getIdentifier("charm" + charmLevel, "drawable", mContext.getPackageName());


        mCharmLevel.setImageResource(drawableId);


//        Log.d("setCharmLevel","drawableId" + drawableId + "   " + R.drawable.ml20);

//        mCharmLevel.setImageResource(R.drawable.ml3);
    }

    public void hideSeatView() {
        if (seatView != null) {
            seatView.setVisibility(GONE);
        }
    }
}
