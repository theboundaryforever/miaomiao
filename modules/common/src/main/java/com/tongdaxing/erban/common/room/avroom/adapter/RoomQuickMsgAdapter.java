package com.tongdaxing.erban.common.room.avroom.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;

import java.util.ArrayList;

/**
 * 房间快捷文案adapter
 * <p>
 * Created by zhangjian on 2019/5/8.
 */
public class RoomQuickMsgAdapter extends RecyclerView.Adapter<RoomQuickMsgAdapter.RoomQuickMsgViewHolder> {
    private OnItemClickListener mOnItemClickListener;
    private ArrayList<String> mQuickMsgList;
    private LayoutInflater mInflater;
    private Context mContext;

    public RoomQuickMsgAdapter(Context context, ArrayList<String> quickMsgList, RoomQuickMsgAdapter.OnItemClickListener onItemClickListener) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mQuickMsgList = quickMsgList;
        mOnItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RoomQuickMsgViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RoomQuickMsgViewHolder(mInflater.inflate(R.layout.item_room_quick_msg, null));
    }

    @Override
    public void onBindViewHolder(@NonNull RoomQuickMsgViewHolder holder, int position) {
        holder.tv.setText(mQuickMsgList.get(position));
        holder.tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mQuickMsgList.size();
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }

    public class RoomQuickMsgViewHolder extends RecyclerView.ViewHolder {
        private TextView tv;

        public RoomQuickMsgViewHolder(View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.room_quick_msg_tv);
        }
    }
}
