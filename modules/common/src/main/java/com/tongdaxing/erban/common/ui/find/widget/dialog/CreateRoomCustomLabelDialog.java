package com.tongdaxing.erban.common.ui.find.widget.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.juxiao.library_ui.widget.ViewHolder;
import com.juxiao.library_ui.widget.dialog.BaseDialog;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.find.model.CreateRoomModel;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.find.ICreateRoomClient;
import com.tongdaxing.xchat_core.find.bean.CreateRoomLabelInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.List;

/**
 * Function: 创建房间自定义标签
 * Author: Edward on 2019/3/18
 */
public class CreateRoomCustomLabelDialog extends BaseDialog {
    private CreateRoomModel createRoomModel;
    private int type;
    private DialogManager dialogManager;

    @Override
    public void convertView(ViewHolder viewHolder) {
        createRoomModel = new CreateRoomModel();

        Bundle bundle = getArguments();
        if (bundle == null) {
            SingleToastUtil.showToast("数据错误!");
            return;
        }

        type = bundle.getInt("type");
        EditText editText = viewHolder.getView(R.id.et_label);

        //主动弹出输入法
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputManager = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(editText, 0);
            }
        }, 100);

        Button btnSaveLabel = viewHolder.getView(R.id.btn_save_label);

        btnSaveLabel.setOnClickListener(v -> {
            if (TextUtils.isEmpty(editText.getText().toString())) {
                SingleToastUtil.showToast("标签名称不能为空!");
                return;
            }
            dialogManager = new DialogManager(getActivity());
            dialogManager.showProgressDialog(getActivity(), "请稍后...");
            addCustomLabel(editText.getText().toString());
        });
    }

    private int convertItemIndex(int roomType) {
        if (roomType == 4) {
            return 1;
        } else {
            return 2;
        }
    }

    /**
     * 加载标签数据
     */
    public void loadLabelData() {
        createRoomModel.loadLabel(new OkHttpManager.MyCallBack<ServiceResult<List<CreateRoomLabelInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (dialogManager != null) {
                    dialogManager.dismissDialog();
                }
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<CreateRoomLabelInfo>> response) {
                if (response != null && response.getData() != null && response.getData().size() >= 3) {
                    CreateRoomLabelInfo createRoomLabelInfo = response.getData().get(convertItemIndex(type));
                    if (createRoomLabelInfo != null) {
                        if (dialogManager != null) {
                            dialogManager.dismissDialog();
                        }
                        SingleToastUtil.showToast("添加标签成功!");
                        CoreManager.notifyClients(ICreateRoomClient.class, ICreateRoomClient.LOAD_LABEL_DATA, type, createRoomLabelInfo.getTagList());
                        dismiss();
                    } else {
                        onError(new Exception());
                    }
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    private void addCustomLabel(String name) {
        createRoomModel.addCustomLabel(name, type, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (dialogManager != null) {
                    dialogManager.dismissDialog();
                }
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    loadLabelData();
                } else if (response != null && response.has("message")) {
                    onError(new Exception(response.str("message")));
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_create_room_custom_label;
    }
}
