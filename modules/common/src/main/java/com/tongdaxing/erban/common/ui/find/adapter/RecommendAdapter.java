package com.tongdaxing.erban.common.ui.find.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.base.popupwindow.RelativePopupWindow;
import com.tongdaxing.erban.common.model.makefriends.PopularStarModel;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.find.activity.VoiceGroupDetailActivity;
import com.tongdaxing.erban.common.ui.find.widget.VoiceMorePopupWindow;
import com.tongdaxing.erban.common.ui.makefriends.PopularStarActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.ShowPhotoActivity;
import com.tongdaxing.erban.common.utils.HttpUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.NewAndOldUserEventUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.find.bean.PictureListInfo;
import com.tongdaxing.xchat_core.find.bean.RecommendModuleInfo;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupInfo;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/13
 */
public class RecommendAdapter extends BaseMultiItemQuickAdapter<VoiceGroupInfo, BaseViewHolder> {
    /**
     * 推荐圈友item
     */
    public static final int RECOMMEND_MODULE_ITEM = 2;
    /**
     * 人气之星item
     */
    public static final int POPULARITY_STAR_ITEM = 1;
    /**
     * 动态item
     */
    public static final int VOICE_GROUP_ITEM = 0;
    private final int RC_PHOTO_PREVIEW = 2;
    private PopularStarModel popularStarModel;
    private long curClickUserId;
    private VoiceGroupLikeCallback voiceGroupLikeCallback;

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public RecommendAdapter(List<VoiceGroupInfo> data) {
        super(data);
        popularStarModel = new PopularStarModel();
        addItemType(VOICE_GROUP_ITEM, R.layout.adapter_voice_group_item);
        addItemType(POPULARITY_STAR_ITEM, R.layout.adapter_popularity_star);
        addItemType(RECOMMEND_MODULE_ITEM, R.layout.adapter_recommend_module);
    }

    @Override
    protected void convert(BaseViewHolder helper, VoiceGroupInfo item) {
        switch (item.getItemType()) {
            case VOICE_GROUP_ITEM:
                setVoiceGroupItemData(helper, item);
                break;
            case POPULARITY_STAR_ITEM://人气之星
                setPopularityStarItemData(helper, item);
                break;
            case RECOMMEND_MODULE_ITEM://推荐圈友
                setRecommendModuleItemData(helper, item);
                break;
            default:
                setVoiceGroupItemData(helper, item);
                break;
        }
    }

    public void setVoiceGroupLikeCallback(VoiceGroupLikeCallback voiceGroupLikeCallback) {
        this.voiceGroupLikeCallback = voiceGroupLikeCallback;
    }

    public long getCurClickUserId() {
        return curClickUserId;
    }

    private void setVoiceGroupItemData(BaseViewHolder helper, VoiceGroupInfo itemData) {
        ImageView ivUserHead = helper.getView(R.id.iv_user_head);
        ImageLoadUtils.loadCircleImage(mContext, itemData.getAvatar(), ivUserHead, R.drawable.ic_no_avatar);
        ivUserHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转至对应用户页
                NewUserInfoActivity.start(mContext, itemData.getUid());
            }
        });

        ImageView voiceChat = helper.getView(R.id.voice_iv_chat);
        ImageView voiceMore = helper.getView(R.id.voice_iv_more);
        if (AvRoomDataManager.get().isSelf(itemData.getUid())) {
            voiceChat.setVisibility(View.GONE);
            voiceMore.setVisibility(View.GONE);
        } else {
            voiceChat.setVisibility(View.VISIBLE);
            voiceMore.setVisibility(View.VISIBLE);
            voiceChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NewAndOldUserEventUtils.onCommonTypeEvent(mContext, UmengEventId.getMomentMsg());
                    int uid = itemData.getUid();
                    HttpUtil.checkUserIsDisturb(voiceChat.getContext(), uid);
                }
            });
            voiceMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = voiceMore.getContext();
                    VoiceMorePopupWindow voiceMorePopupWindow = new VoiceMorePopupWindow(context, itemData.getUid());
                    voiceMorePopupWindow.setIsAttention(itemData.isFans());
                    voiceMorePopupWindow.showOnAnchor(voiceMore, RelativePopupWindow.VerticalPosition.CENTER,
                            RelativePopupWindow.HorizontalPosition.LEFT);
                }
            });
        }

        helper.setText(R.id.tv_name, itemData.getNick());
        ImageView ivGender = helper.getView(R.id.iv_gender);
        if (itemData.getGender() == 1) {
            ivGender.setImageResource(R.drawable.room_user_man);
        } else {
            ivGender.setImageResource(R.drawable.room_user_girl);
        }
        helper.setText(R.id.tv_time, TimeUtils.getPostTimeString(mContext, itemData.getCreateDate(), true, false));
        helper.setText(R.id.tv_content, itemData.getContext());
        RadioButton radioButton = helper.getView(R.id.rb_like);
        if (itemData.getLikeNum() > 999) {
            radioButton.setText("999+");
        } else {
            radioButton.setText(String.valueOf(itemData.getLikeNum()));
        }
        radioButton.setOnClickListener(new ClickImpl(itemData.getId()));
        radioButton.setChecked(itemData.isIsLike());
        TextView tvComment = helper.getView(R.id.tv_comment);
        if (itemData.getPlayerNum() > 999) {
            tvComment.setText("999+");
        } else {
            tvComment.setText(String.valueOf(itemData.getPlayerNum()));
        }
        tvComment.setOnClickListener(v -> {
            VoiceGroupDetailActivity.startOpenKeyboard(mContext, itemData, VoiceGroupDetailActivity.OPERATION_TYPE_OPEN_KEYBOARD_VALUE, helper.getAdapterPosition());
        });

        setPictureLayout(helper, itemData);
    }

    /**
     * 设置图片网格布局
     *
     * @param helper
     * @param info
     */
    private void setPictureLayout(BaseViewHolder helper, VoiceGroupInfo info) {
        RecyclerView recyclerView = helper.getView(R.id.recycler_view);
        List<PictureListInfo> picList = info.getPicList();
        if (picList != null && picList.size() > 0) {
            ArrayList<UserPhoto> remoteUrlList = new ArrayList<>();//查看大图集合列表
            for (int i = 0; i < picList.size(); i++) {
                picList.get(i).setLayoutType(info.getLayoutType());
                picList.get(i).setPicCount(picList.size());
                UserPhoto userPhoto = new UserPhoto();
                userPhoto.setPhotoUrl(picList.get(i).getUrl());
                userPhoto.setPid(i);
                remoteUrlList.add(userPhoto);
            }
            recyclerView.setVisibility(View.VISIBLE);
            VoiceGroupPictureAdapter adapter = new VoiceGroupPictureAdapter(R.layout.adapter_voice_group_picture_item);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 6);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);
            recyclerView.setNestedScrollingEnabled(false);
            if (picList.size() > 9) {
                adapter.setNewData(picList.subList(0, 9));
            } else {
                adapter.setNewData(picList);
            }
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (picList.size() == 1 && position == 0) {
                        return 6;
                    } else if (picList.size() == 2 && position == 0) {
                        return 3;
                    } else if (picList.size() == 2 && position == 1) {
                        return 3;
                    } else {
                        return 2;
                    }
                }
            });
            adapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter1, View view, int position) {
                    NewAndOldUserEventUtils.onCommonTypeEvent(mContext, UmengEventId.getMomentPics());
                    ShowPhotoActivity.start(mContext, position, remoteUrlList);
                }
            });
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * 人气之星
     *
     * @param helper
     * @param itemData
     */
    private void setPopularityStarItemData(BaseViewHolder helper, VoiceGroupInfo itemData) {
        TextView tvMore = helper.getView(R.id.tv_more);
        tvMore.setOnClickListener(v -> mContext.startActivity(new Intent(mContext, PopularStarActivity.class)));
        LinearLayout llContainer = helper.getView(R.id.ll_container);
        llContainer.removeAllViews();
        List<UserInfo> list = itemData.getUserInfoList();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                View itemView = LayoutInflater.from(mContext).inflate(R.layout.adapter_popularity_star_items, null);
                llContainer.addView(itemView);
                TextView tvName = itemView.findViewById(R.id.tv_name);
                tvName.setText(list.get(i).getNick());
                TextView tvLookForIt = itemView.findViewById(R.id.tv_look_for_it);
                int finalI = i;
                tvLookForIt.setOnClickListener(v -> lookForIt(list.get(finalI)));
                ImageView imageView = itemView.findViewById(R.id.iv_user_head);
                ImageLoadUtils.loadCircleImage(mContext, list.get(i).getAvatar(), imageView, R.drawable.ic_no_avatar);
            }
        }
    }

    private void lookForIt(UserInfo userInfo) {
        UmengEventUtil.getInstance().onEvent(mContext, UmengEventId.getFindMomentsCommenduser());
        DialogManager dialogManager = new DialogManager(mContext);
        dialogManager.showProgressDialog(mContext, mContext.getResources().getString(R.string.waiting_text), true);
        popularStarModel.getUserRoomInfo(userInfo.getUid(), new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                SingleToastUtil.showToast(mContext.getResources().getString(R.string.state_load_error));
            }

            @Override
            public void onResponse(ServiceResult<RoomInfo> response) {
                if (response != null && response.getData() != null) {
                    dialogManager.dismissDialog();
                    if (response.getData().getUid() > 0) {
                        AVRoomActivity.start(mContext, response.getData().getUid(), new HerUserInfo(userInfo.getUid(), userInfo.getNick()));
                    } else {
                        onError(new Exception());
                    }
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    private void setRecommendModuleItemData(BaseViewHolder helper, VoiceGroupInfo itemData) {
        LinearLayout llContainer = helper.getView(R.id.ll_container);
        llContainer.removeAllViews();
        List<RecommendModuleInfo> list = itemData.getRecommendModuleInfoList();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                View itemView = LayoutInflater.from(mContext).inflate(R.layout.adapter_popularity_star_items, null);
                itemView.setTag(i);
                llContainer.addView(itemView);
                ImageView imageView = itemView.findViewById(R.id.iv_user_head);
                ImageLoadUtils.loadCircleImage(mContext, list.get(i).getAvatar(), imageView, R.drawable.ic_no_avatar);
                TextView tvName = itemView.findViewById(R.id.tv_name);
                tvName.setText(list.get(i).getNick());
                TextView goIt = itemView.findViewById(R.id.tv_look_for_it);
                int finalI = i;
                goIt.setOnClickListener(v -> {
                    UmengEventUtil.getInstance().onEvent(mContext, UmengEventId.getFindMomentsTopuser());
                    CoreManager.getCore(IPraiseCore.class).recommendFriends(list.get(finalI).getUid());
                });
            }
        }
    }

    public interface VoiceGroupLikeCallback {
        void likeClick(int id);
    }

    private class ClickImpl implements View.OnClickListener {
        private int id;

        public ClickImpl(int id) {
            this.id = id;
        }

        @Override
        public void onClick(View v) {
            if (voiceGroupLikeCallback != null) {
                voiceGroupLikeCallback.likeClick(id);
            }
        }
    }
}













