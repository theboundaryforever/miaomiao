package com.tongdaxing.erban.common.room.audio.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public interface IHotMusicView extends IMvpBaseView {
    void refreshMyMusicList(boolean isRefresh, List<MyMusicInfo> myMusicInfoList);

    void loadDataFailure(boolean isRefresh, String msg);

    void showRefresh(boolean flag);
}
