package com.tongdaxing.erban.common.ui.message.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.SystemMessageService;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.ui.home.adpater.CommonImageIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

import cn.jpush.android.api.JPushInterface;

/**
 * <p> 主页消息界面  </p>
 *
 * @author Administrator
 * @date 2017/11/14
 */
public class MsgFragment extends BaseFragment implements CommonMagicIndicatorAdapter.OnItemSelectListener {
    public static final String TAG = "MsgFragment";

    //private TextView mTvCenterTitle;
    private MagicIndicator indicator;
    private ViewPager viewPager;
    private CommonImageIndicatorAdapter mMsgIndicatorAdapter;

    private View ignoreUnreadBtn;

    private RecentListFragment recentListFragment;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_msg;
    }

    @Override
    public void onFindViews() {
        // mTvCenterTitle = (TextView) mView.findViewById(R.id.tv_center_title);
        indicator = (MagicIndicator) mView.findViewById(R.id.indicator);
        viewPager = (ViewPager) mView.findViewById(R.id.viewpager);
        ignoreUnreadBtn = mView.findViewById(R.id.ignore_unread_msg_btn);
    }

    @Override
    public void onSetListener() {
        //忽略未读消息（非全部已读）
        ignoreUnreadBtn.setOnClickListener(view -> {
            if (recentListFragment != null) {
                recentListFragment.ignoreUnreadMsg();
            }
            JPushInterface.clearAllNotifications(mContext);
            NIMClient.getService(SystemMessageService.class).clearSystemMessages();
            CoreManager.notifyClients(IIMMessageCoreClient.class, IIMMessageCoreClient.METHOD_ON_RECEIVE_CONTACT_CHANGED);
        });
    }

    @Override
    public void initiate() {
        List<Fragment> mTabs = new ArrayList<>(2);
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(RecentListFragment.TAG);
        if (fragment != null) {
            recentListFragment = (RecentListFragment) fragment;
        } else {
            recentListFragment = new RecentListFragment();
        }
        fragment = fragmentManager.findFragmentByTag(FriendListFragment.TAG);
        FriendListFragment friendListFragment;
        if (fragment != null) {
            friendListFragment = (FriendListFragment) fragment;
        } else {
            friendListFragment = new FriendListFragment();
        }
        mTabs.add(recentListFragment);
        mTabs.add(friendListFragment);
        initViewPager();
        viewPager.setAdapter(new ImFragmentPagerAdapter(getChildFragmentManager(), mTabs));
        viewPager.setOffscreenPageLimit(2);
        ViewPagerHelper.bind(indicator, viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ignoreUnreadBtn.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemSelect(int position) {
        viewPager.setCurrentItem(position);
    }

    private void initViewPager() {
        initMagicIndicator();
    }

    private void initMagicIndicator() {
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(1, getString(R.string.message)));
        tabInfoList.add(new TabInfo(2, getString(R.string.friend)));
//        tabInfoList.add(new TabInfo(3, "关注"));
        mMsgIndicatorAdapter = new CommonImageIndicatorAdapter(mContext, tabInfoList);
        mMsgIndicatorAdapter.setSize(18);
        mMsgIndicatorAdapter.setNormalColorId(R.color.color_626269);
        mMsgIndicatorAdapter.setSelectColorId(R.color.color_000000);
        mMsgIndicatorAdapter.setImageIndicatorWidth(34.0f);
        mMsgIndicatorAdapter.setImageIndicatorHeight(12.0f);
        mMsgIndicatorAdapter.setImageIndicatorMarginTop(3.0f);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        indicator.setNavigator(commonNavigator);
        // must after setNavigator
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
    }

    private static class ImFragmentPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> mTabs;

        ImFragmentPagerAdapter(FragmentManager fm, List<Fragment> tabs) {
            super(fm);
            mTabs = tabs;
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        @Override
        public Fragment getItem(int position) {
            return mTabs.get(position);
        }
    }
}