package com.tongdaxing.erban.common.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.room.audio.activity.AudioRecordActivity;
import com.tongdaxing.erban.common.ui.login.activity.AddUserInfoActivity;
import com.tongdaxing.erban.common.ui.login.activity.ForgetPswActivity;
import com.tongdaxing.erban.common.ui.login.activity.RegisterActivity;
import com.tongdaxing.erban.common.ui.me.setting.activity.SettingActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.ModifyInfoActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.UserInfoModifyActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.UserModifyPhotosActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.xchat_core.UriProvider;

import java.util.List;


/**
 * *************************************************************************
 *
 * @Version 1.0
 * @ClassName: UIHelper
 * @Description: 应用程序UI工具包：封装UI相关的一些操作
 * @Author zhouxiangfeng
 * @date 2013-8-6 下午1:39:11
 * **************************************************************************
 */
public class UIHelper {
    private static final String TAG = "UIHelper";

    public static void showRegisterAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, RegisterActivity.class));
    }

    public static void showSettingAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, SettingActivity.class));
    }

    public static void showForgetPswAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, ForgetPswActivity.class));
    }

    public static void showAddInfoAct(Context mContext) {
        Intent intent = new Intent(mContext, AddUserInfoActivity.class);
        mContext.startActivity(intent);

    }

//    public static void showUserAvatarUpdateAct(Context mContext) {
//        mContext.startActivity(new Intent(mContext, UserAvatarUpdateActivity.class));
//    }


    //修改用户资料
    public static void showUserInfoModifyAct(Context mContext, long userId) {
        Intent intent = new Intent(mContext, UserInfoModifyActivity.class);
        intent.putExtra("userId", userId);
        mContext.startActivity(intent);
    }

    //我的钱包
    public static void showWalletAct(Context mContext) {
        Intent intent = new Intent(mContext, MyWalletNewActivity.class);
        //intent.putExtra("userInfo",userInfo);
        mContext.startActivity(intent);
    }

    //侧边栏===>帮助
    public static void showUsinghelp(Context mContext) {
//        CommonWebViewActivity.start(mContext, UriProvider.JAVA_WEB_URL + "/ttyy/question/question.html");
        CommonWebViewActivity.start(mContext, "https://mp.weixin.qq.com/s/6YqQ9Ns3euMP7Aq0hJZppQ");

    }


    public static void showUserInfoAct(Context mContext, long userId) {
        Intent intent = new Intent(mContext, NewUserInfoActivity.class);
        intent.putExtra("userId", userId);
        mContext.startActivity(intent);
    }

    public static void showAudioRecordAct(Context mContext) {
        Intent intent = new Intent(mContext, AudioRecordActivity.class);
        mContext.startActivity(intent);
    }


    public static void showModifyInfoAct(Activity mActivity, int requestCode, String title) {
        Intent intent = new Intent(mActivity, ModifyInfoActivity.class);
        intent.putExtra("title", title);
        mActivity.startActivityForResult(intent, requestCode);
    }

    public static void showModifyPhotosAct(Activity mActivity, long userId) {
        Intent intent = new Intent(mActivity, UserModifyPhotosActivity.class);
        intent.putExtra("userId", userId);
        mActivity.startActivity(intent);
    }

    public static void showModifyPhotosAndPhotos(Activity mActivity, long userId, boolean isSelf) {
        Intent intent = new Intent(mActivity, UserModifyPhotosActivity.class);
        intent.putExtra("isSelf", isSelf);
        intent.putExtra("userId", userId);
        mActivity.startActivity(intent);
    }


    /**
     * 启动应用的设置
     *
     * @param context
     */
    public static void startAppSettings(Context context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        context.startActivity(intent);
    }

    public static void openContactUs(Context context) {
        CommonWebViewActivity.start(context, UriProvider.IM_SERVER_URL + "/mm/links/links.html");
    }

    /**
     * app是否运行在前台
     *
     * @param context
     * @return
     */
    public static boolean isAppRunningForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager != null) {
            List runningTasks = activityManager.getRunningTasks(1);
            return context.getPackageName().equalsIgnoreCase(((ActivityManager.RunningTaskInfo) runningTasks.get(0)).baseActivity.getPackageName());
        } else {
            return false;
        }
    }

    /**
     * 判断程序是否在后台运行
     *
     * @param context
     * @return true 主程序并没有运行（虽然主进程存活，但是应用并没有打开） false 主程序在运行
     */
    public static boolean isRunBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager == null) {
            return false;
        }
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            L.debug(TAG, "context.getPackageName() = %s, appProcess.processName = %s, appProcess.importance = %d",
                    context.getPackageName(), appProcess.processName, appProcess.importance);
            if (appProcess.processName.equals(context.getPackageName())) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
                    // 表明程序在后台运行
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * 判断程序是否在前台运行（当前运行的程序）
     *
     * @param context
     * @return true 当前应用位于前台可见 false 当前应用不可见，此时应用可能是按了home键位于后台，也可能是应用不存在但主进程存在
     */
    public static boolean isRunForeground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        boolean isOnForground = false;
        List<ActivityManager.RunningAppProcessInfo> runnings = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo running : runnings) {
            if (running.processName.equals(context.getPackageName())) {
                if (running.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
                        || running.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE) {
                    //前台显示...
                    isOnForground = true;
                } else {
                    //后台显示...
                    isOnForground = false;
                }
                break;
            }
        }
        String currentPackageName = "";
        if (am.getRunningTasks(1).size() > 0) {
            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
            currentPackageName = cn.getPackageName();
        }
//        Log.e("my", "isAppOnForeground  :" + currentPackageName + "   getPackageName:" + getPackageName());
        return isOnForground && !TextUtils.isEmpty(currentPackageName) && currentPackageName.equals(context.getPackageName());
    }
}
