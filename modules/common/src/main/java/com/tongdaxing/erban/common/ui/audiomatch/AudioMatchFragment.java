package com.tongdaxing.erban.common.ui.audiomatch;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.media.AudioManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.audiocard.AudioCard2Activity;
import com.tongdaxing.erban.common.ui.me.task.view.IMeView;
import com.tongdaxing.erban.common.ui.me.user.activity.UserInfoModifyActivity;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.audio.AudioCardMarkManager;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.AudioMatchConfigInfo;
import com.tongdaxing.xchat_core.bean.AudioMatchRandomInfo;
import com.tongdaxing.xchat_core.bean.FocusMsgSwitchInfo;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCoreClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.media.AudioPlayer;
import com.tongdaxing.xchat_framework.media.OnPlayListener;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AudioMatchFragment extends BaseFragment implements IMeView, OpenAudioCardCallback {
    public static final String TAG = "AudioMatchFragment";
    private final static int CODE_TUTORIAL = 1002, CODE_MAX_LIMIT = 90001;
    private ImageView ivDislike, ivLikeUser, ivMyAudio, ivAudioSetting, ivStartPlay, ivPlanet, ivGender, ivAvatar;
    private TextView tvReport, tvBlackList, tvName, tvOnline;
    private TextView mTvBack;
    private SVGAImageView ivAudioPlayingAnim, ivDataLoadingAnim;
    private SVGADrawable loadAnimDrawable, playAnimDrawable;
    private RelativeLayout rlExceptionContent, rlNormalContent, rlContent;
    private Button btnRestartMatch;

    private AudioMatchRandomInfo currRandomInfo;
    private boolean isShowNoAudioDataDialog = true;//只显示一次
    private AudioMatchSettingDialog audioMatchSettingDialog;
    private NoAudioDataHintDialog noAudioDataHintDialog;
    private LikeUserMaxDialog likeUserMaxDialog;
    private AudioPlayer player;
    private OnPlayListener onPlayListener = new OnPlayListener() {
        @Override
        public void onPrepared() {
        }

        @Override
        public void onCompletion() {
            setupStopAudioView();
        }

        @Override
        public void onInterrupt() {
            setupStopAudioView();
        }

        @Override
        public void onError(String error) {
            setupStopAudioView();
            toast("播放失败");
        }

        @Override
        public void onPlaying(long curPosition) {
        }
    };
    private List<AudioMatchRandomInfo> randomInfoList;
    private int curPosition = 0;
    private boolean isLoading = false;
    //like时发的文案
    private String[] likeMsgList = {"【来自声音匹配】我喜欢你的声音", "【来自声音匹配】给你点亮小心心~", "【来自声音匹配】哈喽~哈喽~", "【来自声音匹配】你在吗？",
            "【来自声音匹配】声音好听，比心~", "【来自声音匹配】糟糕，这是心动的感觉", "【来自声音匹配】喂喂喂~你在吗？",
            "【来自声音匹配】这么好听的声音，必须点赞啊~", "【来自声音匹配】你的声音，我想再听一遍", "【来自声音匹配】我想认识你"};

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_audio_match;
    }

    @Override
    public void onFindViews() {
        rlExceptionContent = mView.findViewById(R.id.rl_exception_content);
        rlNormalContent = mView.findViewById(R.id.rl_normal_content);
        rlContent = mView.findViewById(R.id.rl_content);
        btnRestartMatch = mView.findViewById(R.id.btn_restart_match);
        ivDataLoadingAnim = mView.findViewById(R.id.svga_loading);
        ivPlanet = mView.findViewById(R.id.iv_planet);
        ivAudioPlayingAnim = mView.findViewById(R.id.svga_imageview);
        tvName = mView.findViewById(R.id.tv_name);
        tvOnline = mView.findViewById(R.id.online);
        ivAvatar = mView.findViewById(R.id.iv_avatar);
//        ivGender = mView.findViewById(R.id.iv_gender);
        ivStartPlay = mView.findViewById(R.id.iv_start_play);
        ivMyAudio = mView.findViewById(R.id.iv_my_audio);
        ivAudioSetting = mView.findViewById(R.id.iv_audio_setting);
        ivDislike = mView.findViewById(R.id.iv_dislike);
        ivLikeUser = mView.findViewById(R.id.iv_like_user);
        tvReport = mView.findViewById(R.id.tv_report);
        tvBlackList = mView.findViewById(R.id.tv_blacklist);
        mTvBack = mView.findViewById(R.id.voice_main_back);

        ivDataLoadingAnim.setClearsAfterStop(true);
        ivDataLoadingAnim.setLoops(Integer.MAX_VALUE);

        ivAudioPlayingAnim.setClearsAfterStop(true);
        ivAudioPlayingAnim.setLoops(Integer.MAX_VALUE);
    }

    @Override
    public void initiate() {
        player = AudioPlayAndRecordManager.getInstance().createAudioPlayer(BasicConfig.INSTANCE.getAppContext(), onPlayListener);
        audioMatchSettingDialog = new AudioMatchSettingDialog();
        audioMatchSettingDialog.setOnSettingCompleteListener(this::saveSetting);
        noAudioDataHintDialog = new NoAudioDataHintDialog();
        likeUserMaxDialog = new LikeUserMaxDialog();

        randomInfoList = new ArrayList<>();

        boolean audioMatchTutorialShowed = (boolean) SpUtils.get(getContext(), SpEvent.audioMatchTutorialShowed, false);//是否已显示新手教程
        if (!audioMatchTutorialShowed) {
            startActivityForResult(new Intent(getContext(), AudioMatchTutorialActivity.class), CODE_TUTORIAL);
            SpUtils.put(getContext(), SpEvent.audioMatchTutorialShowed, true);
        } else {
            resetLoadData();
            loadConfig();
        }
    }

    @Override
    public void onSetListener() {
        btnRestartMatch.setOnClickListener(v -> resetLoadData());
        ivMyAudio.setOnClickListener(v -> {
            UmengEventUtil.getInstance().onEvent(mContext, UmengEventId.getMatchMyVoice());
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo == null) {
                SingleToastUtil.showToast("数据错误!");
                return;
            }
            if (userInfo.getVoiceCard() == null) {
                openAudioCard();
            } else {
                CommonWebViewActivity.start(getActivity(), UriProvider.getAudioIdentifyCard());
            }
        });
        ivAudioSetting.setOnClickListener(v -> audioMatchSettingDialog.show(getFragmentManager(), ""));
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo == null) {
            return;
        }
        String userType;
        if (CoreManager.getCore(IUserCore.class).isNewUser(userInfo) && userInfo.getChargeNum() <= 0) {
            //用户处于新手期，且没有充值过
            userType = "新用户";
        } else { //充值过或者超过新手期
            userType = "老用户";
        }
        ivDislike.setOnClickListener(v -> {
            UmengEventUtil.getInstance().onAudioMatchActionClick(getContext(), userType, "忽略TA");//加友盟统计
            doUnLike();
            if (randomInfoList.size() - curPosition < 5) {//剩余少于5个就开始加载新的数据
                loadData();
            }
            if (player.isPlaying()) {
                player.stop();
            }
            next();
        });
        ivLikeUser.setOnClickListener(v -> {
            UmengEventUtil.getInstance().onAudioMatchActionClick(getContext(), userType, "喜欢TA");//加友盟统计
            doLike();
        });
        tvReport.setOnClickListener(v -> {
            if (currRandomInfo != null) {
                createUserMoreOperate();
            }
        });
        tvBlackList.setOnClickListener(v -> {
            if (currRandomInfo != null) {
                createUserMoreOperate();
            }
        });
        ivStartPlay.setOnClickListener(v -> {
            setupPlayAudioView();
            player.start(AudioManager.STREAM_MUSIC, true);
        });
        ivAudioPlayingAnim.setOnClickListener(v -> {
            if (player.isPlaying()) {
                player.stop();
            }
        });
        mTvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }
        });
    }

    private void resetLoadData() {
        curPosition = 0;
        randomInfoList.clear();
        if (loadAnimDrawable == null) {//未加载的话，先加载一次
            SVGAParser parser = new SVGAParser(getActivity());
            parser.parse("anim_audio_match_launch", new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity videoItem) {
                    loadAnimDrawable = new SVGADrawable(videoItem);
                    ivDataLoadingAnim.setImageDrawable(loadAnimDrawable);
                    rlNormalContent.setVisibility(View.GONE);
                    rlExceptionContent.setVisibility(View.GONE);
                    ivDataLoadingAnim.setVisibility(View.VISIBLE);
                    ivDataLoadingAnim.startAnimation();
                    loadData();
                }

                @Override
                public void onError() {
                    loadData();
                }
            });
        } else {
            rlNormalContent.setVisibility(View.GONE);
            rlExceptionContent.setVisibility(View.GONE);
            ivDataLoadingAnim.setVisibility(View.VISIBLE);
            ivDataLoadingAnim.startAnimation();
            loadData();
        }
    }

    private void finishLoadAnim(boolean isException) {
        if (ivDataLoadingAnim.isAnimating()) {
            ivDataLoadingAnim.postDelayed(() -> {
                ivDataLoadingAnim.stopAnimation();
                ivDataLoadingAnim.setVisibility(View.GONE);
                if (isException) {
                    showExceptionContent();
                } else {
                    showNormalContent();
                }
            }, 1000);
        } else {
            ivDataLoadingAnim.setVisibility(View.GONE);
            if (isException) {
                showExceptionContent();
            } else {
                showNormalContent();
            }
        }
    }

    /**
     * 下一个
     */
    private void next() {
        if (curPosition < randomInfoList.size()) {
            ++curPosition;
            UserInfo currUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                int cicular_R = rlNormalContent.getHeight() / 2;
//                Animator animator = ViewAnimationUtils.createCircularReveal(rlNormalContent, UIUtil.getScreenWidth(getContext()) / 2, cicular_R, 0, cicular_R);
//                // Animator animator = ViewAnimationUtils.createCircularReveal(mainContainer, UIUtil.dip2px(this,40),  UIUtil.dip2px(this,70) , 0, cicular_R);
//                animator.setDuration(800);
//                animator.setInterpolator(new AccelerateInterpolator());
//                animator.start();
//            }
            ScaleAnimation animation = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            animation.setDuration(500);
            rlContent.startAnimation(animation);
            setupUserInfo(curPosition);
            if (curPosition == 3 && currUserInfo != null && TextUtils.isEmpty(currUserInfo.getUserVoice()) && isShowNoAudioDataDialog) {//用户点击三次之后弹出提示，之后不再弹出
                isShowNoAudioDataDialog = false;
                Context context = getContext();
                if (context instanceof Activity) {
                    Activity activity = (Activity) context;
                    if (activity.isFinishing() || activity.isDestroyed()) {
                        noAudioDataHintDialog.show(getFragmentManager(), "");
                    }
                }
            }
            setupPlayAudioView();
            player.start(AudioManager.STREAM_MUSIC, true);
        }
    }

    private void setupPlayAudioView() {
        if (playAnimDrawable == null) {//未加载的话，先加载一次
            SVGAParser parser = new SVGAParser(getActivity());
            parser.parse("anim_audio_match_start", new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity videoItem) {
                    playAnimDrawable = new SVGADrawable(videoItem);
                    ivAudioPlayingAnim.setImageDrawable(playAnimDrawable);
                    ivPlanet.setVisibility(View.INVISIBLE);
                    ivStartPlay.setVisibility(View.INVISIBLE);
                    ivAudioPlayingAnim.setVisibility(View.VISIBLE);
                    ivAudioPlayingAnim.startAnimation();
                }

                @Override
                public void onError() {
                }
            });
        } else {
            ivPlanet.setVisibility(View.INVISIBLE);
            ivStartPlay.setVisibility(View.INVISIBLE);
            ivAudioPlayingAnim.setVisibility(View.VISIBLE);
            ivAudioPlayingAnim.startAnimation();
        }
    }

    private void setupUserInfo(int position) {
        if (getContext() == null) {
            return;
        }
        if (position < randomInfoList.size()) {
            AudioMatchRandomInfo randomInfo = randomInfoList.get(position);
            tvName.setText(randomInfo.getNick());
            tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(getContext(), randomInfo.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_female), null);
//            ivGender.setImageResource(randomInfo.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_women);
            tvOnline.setVisibility(randomInfo.getOperatorStatus() == 2 ? View.VISIBLE : View.GONE);
            if (!TextUtils.isEmpty(randomInfo.getAvatar())) {
                ImageLoadUtils.loadAvatar(getActivity(), randomInfo.getAvatar(), ivAvatar, true);
            }
            player.setDataSource(randomInfo.getUserVoice());
            currRandomInfo = randomInfo;
        }
    }

    private void setupStopAudioView() {
        ivPlanet.setVisibility(View.VISIBLE);
        ivStartPlay.setVisibility(View.VISIBLE);
        if (ivAudioPlayingAnim.isAnimating()) {
            ivAudioPlayingAnim.stopAnimation();
        }
    }

    /**
     * 生成用户功能按钮
     */
    private void createUserMoreOperate() {
        List<ButtonItem> buttonItems = new ArrayList<>();
        ButtonItem msgBlackListItem = ButtonItemFactory.createMsgBlackListItem(!CoreManager.getCore(IIMFriendCore.class).isUserInBlackList(currRandomInfo.getUid() + "") ? "拉黑" : "取消拉黑", this::showDeFriendDialog);
        buttonItems.add(ButtonItemFactory.createReportItem(getActivity(), "举报", 1));
        buttonItems.add(msgBlackListItem);
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    private void loadData() {
        if (isLoading) {
            return;
        }
        isLoading = true;
        if (player.isPlaying()) {
            player.stop();
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getRandomUser(), params, new OkHttpManager.MyCallBack<ServiceResult<List<AudioMatchRandomInfo>>>() {
            @Override
            public void onError(Exception e) {
                isLoading = false;
                finishLoadAnim(true);
            }

            @Override
            public void onResponse(ServiceResult<List<AudioMatchRandomInfo>> response) {
                L.info(TAG, "response = %s", response == null ? "null" : (response.getData() == null ? "response.getData() = null" : response.getData()));
                if (null != response && response.isSuccess() && response.getData() != null) {
                    isLoading = false;
                    finishLoadAnim(false);
                    for (AudioMatchRandomInfo audioMatchRandomInfo : response.getData()) {
                        L.debug(TAG, "audioMatchRandomInfo = %s", audioMatchRandomInfo);
                    }
                    randomInfoList.addAll(response.getData());
                    if (curPosition == 0) {//第一次要选中一下
                        setupUserInfo(curPosition);
                    }
                    showNormalContent();
                } else {
                    if (response != null) {
                        toast(response.getMessage());
                    }
                    onError(new Exception());
                }
            }
        });
    }

    private void showNormalContent() {
        rlNormalContent.setVisibility(View.VISIBLE);
        rlExceptionContent.setVisibility(View.GONE);
    }

    private void showExceptionContent() {
        rlNormalContent.setVisibility(View.GONE);
        rlExceptionContent.setVisibility(View.VISIBLE);
    }

    private void showDeFriendDialog() {
        if (CoreManager.getCore(IIMFriendCore.class).isUserInBlackList(currRandomInfo.getUid() + "")) {
            getDialogManager().showOkCancelDialog("是否取消拉黑", true, new DialogManager.AbsOkDialogListener() {
                @Override
                public void onOk() {
                    CoreManager.getCore(IIMFriendCore.class).removeFromBlackList("" + currRandomInfo.getUid());
                }
            });
        } else {
            getDialogManager().showOkCancelDialog("加入黑名单后，将不再收到对方信息", true, new DialogManager.AbsOkDialogListener() {
                @Override
                public void onOk() {
                    CoreManager.getCore(IIMFriendCore.class).addToBlackList("" + currRandomInfo.getUid());
                }
            });
        }
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void removeBlackListSuccess() {
        toast("已取消拉黑");
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void addBlackListSuccess() {
        toast("已拉黑");
    }

    @Override
    public void updateUserInfoUI(UserInfo userInfo) {

    }

    @Override
    public void openAudioCard() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        checkPermission(() -> {
                    Intent intent = new Intent(getActivity(), AudioCard2Activity.class);
                    intent.putExtra(AudioCard2Activity.AUDIO_FILE_URL, userInfo != null ? userInfo.getUserVoice() : null);
                    intent.putExtra(AudioCard2Activity.AUDIO_LENGTH, userInfo != null ? userInfo.getVoiceDura() : 0);
                    startActivityForResult(intent, UserInfoModifyActivity.Method.AUDIO);
                    AudioCardMarkManager.getInstance(getActivity()).clearMark();
                }, R.string.ask_again,
                Manifest.permission.RECORD_AUDIO);
    }

    private void loadConfig() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.audioMatchGetConfig(), params, new OkHttpManager.MyCallBack<ServiceResult<AudioMatchConfigInfo>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<AudioMatchConfigInfo> response) {
                if (response.isSuccess() && response.getData() != null) {
                    AudioMatchConfigInfo configInfo = response.getData();
                    int gender = 0;
                    switch (configInfo.getFilterGender()) {
                        case 0:
                            gender = 0;
                            break;
                        case 1:
                            gender = 2;//接口返回的是过滤性别
                            break;
                        case 2:
                            gender = 1;//接口返回的是过滤性别
                            break;
                    }
                    audioMatchSettingDialog.setupConfig(response.getData().isVoiceHide(), gender);
                }
            }
        });
    }

    private void saveSetting(boolean isHideOpen, int gender) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("voiceHide", String.valueOf(isHideOpen));
        int filterGender = 0;
        switch (gender) {
            case 0:
                filterGender = 0;
                break;
            case 1:
                filterGender = 2;//接口传的参数是过滤性别
                break;
            case 2:
                filterGender = 1;//接口传的参数是过滤性别
                break;
        }
        params.put("filterGender", String.valueOf(filterGender));
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.audioMatchSaveConfig(), null, params, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                toast("保存失败");
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                if (null != response && response.isSuccess()) {
                    if (audioMatchSettingDialog != null) {
                        audioMatchSettingDialog.dismiss();
                    }
                    resetLoadData();
                } else {
                    toast(response != null ? response.getMessage() : "保存失败");
                }
            }
        });
    }

    /**
     * 喜欢
     */
    private void doLike() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        if (currRandomInfo == null || currRandomInfo.getUid() <= 0) {
            SingleToastUtil.showToast("数据错误!");
            return;
        }
        params.put("likeUid", String.valueOf(currRandomInfo.getUid()));
        OkHttpManager.getInstance().doPostRequest(UriProvider.likeUser(), null, params, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                toast("网络不稳定，请稍后再试");
            }

            @Override
            public void onResponse(ServiceResult<String> response) {
                if (!response.isSuccess()) {
                    if (response.getCode() == CODE_MAX_LIMIT) {
                        likeUserMaxDialog.show(getFragmentManager(), "");
                    } else {
                        toast(response.getMessage());
                    }
                } else {
                    checkFocusMsgSwitch(new OkHttpManager.MyCallBack<Integer>() {
                        @Override
                        public void onError(Exception e) {
                            toast("网络不稳定，请稍后再试");
                        }

                        @Override
                        public void onResponse(Integer status) {
                            if (status == 1) {
                                toast("对方已开启免打扰模式");
                            } else {
                                L.debug(TAG, "doLike() response = %s", response.getData());
//                                sendMsg(response.getData()); //去掉发消息给对方
                                if (player.isPlaying()) {
                                    player.stop();
                                }
                                if (currRandomInfo != null) {
                                    int[] location = new int[2];
                                    ivLikeUser.getLocationOnScreen(location);
                                    LikeUserDialog likeUserDialog = LikeUserDialog.newInstance(currRandomInfo, new Point(location[0] + ivLikeUser.getWidth() / 2, location[1] + ivLikeUser.getHeight() / 2));
                                    likeUserDialog.show(getFragmentManager(), "LikeUserDialog");
                                    likeUserDialog.setOnDismissListener(dialog -> {
                                        next();
                                    });
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    /**
     * 不喜欢
     */
    private void doUnLike() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        if (currRandomInfo == null || currRandomInfo.getUid() <= 0) {
            SingleToastUtil.showToast("数据错误!");
            return;
        }
        params.put("unlikeUid", String.valueOf(currRandomInfo.getUid()));
        OkHttpManager.getInstance().doPostRequest(UriProvider.unLikeUser(), params, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(ServiceResult<Object> response) {

            }
        });
    }

    public void checkFocusMsgSwitch(OkHttpManager.MyCallBack<Integer> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", currRandomInfo.getUid() + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getFocusMsgSwitch(), null, params, new OkHttpManager.MyCallBack<ServiceResult<FocusMsgSwitchInfo>>() {
            @Override
            public void onError(Exception e) {
                callBack.onError(new Exception("网络错误，请稍后再试"));
            }

            @Override
            public void onResponse(ServiceResult<FocusMsgSwitchInfo> response) {
                int isFocusMsgSwitchCheckStatus = 0;//1免打扰
                if (response.isSuccess() && response.getData() != null) {
                    int chatPermission = response.getData().getChatPermission();
                    if (chatPermission == 1) {
                        isFocusMsgSwitchCheckStatus = 1;//免打扰
                    } else if (chatPermission == 2) {
                        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                        if (userInfo == null || userInfo.getExperLevel() < 10) {
                            isFocusMsgSwitchCheckStatus = 1;//对小于10级的用户免打扰
                        }
                    }
                    callBack.onResponse(isFocusMsgSwitchCheckStatus);
                } else {
                    callBack.onError(new Exception(response.getMessage()));
                }
            }
        });
    }

    private void sendMsg(String response) {
        if (TextUtils.isEmpty(response)) {
            Random random = new Random();//随机一个文案
            response = likeMsgList[random.nextInt(likeMsgList.length)];
        }
        IMMessage message = MessageBuilder.createTextMessage(String.valueOf(currRandomInfo.getUid()), SessionTypeEnum.P2P, response);
        NIMClient.getService(MsgService.class).sendMessage(message, false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_TUTORIAL) {
            resetLoadData();
            loadConfig();
        }
    }

    @Override
    public void onDestroy() {
        if (ivAudioPlayingAnim.isAnimating()) {
            ivAudioPlayingAnim.stopAnimation();
        }
        if (player.isPlaying()) {
            player.stop();
        }
        AudioPlayAndRecordManager.getInstance().releaseAudioPlayer(player);
        super.onDestroy();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            player.stop();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        player.stop();
    }
}
