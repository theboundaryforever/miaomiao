package com.tongdaxing.erban.common.utils;

import java.text.DecimalFormat;

public class NumberFormatUtils {
    /**
     * 格式化double，如果没小数不显示小数点，有小数正常显示小数
     */
    public static String formatDoubleDecimalPoint(double doubleNumber) {
        int intNumber = (int) doubleNumber;
        if (intNumber == doubleNumber) {
            return String.valueOf(intNumber);
        } else {
            return String.valueOf(doubleNumber);
        }
    }

    /**
     * 格式化double，如果没小数不显示小数点，有小数正常显示小数，最多2位小数
     */
    public static String formatDoubleDecimalPointWithMax2Digit(double doubleNumber) {
        int intNumber = (int) doubleNumber;
        if (intNumber == doubleNumber) {
            return String.valueOf(intNumber);
        } else {
            return new DecimalFormat("#.##").format(doubleNumber);
        }
    }
}
