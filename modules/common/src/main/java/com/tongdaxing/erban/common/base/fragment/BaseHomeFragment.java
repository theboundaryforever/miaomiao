package com.tongdaxing.erban.common.base.fragment;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.umeng.analytics.MobclickAgent;

/**
 * 该基类适合于采用Hide和show分别隐藏和显示Fragment时的友盟统计
 * <p>
 * Created by zhangjian on 2019/7/3.
 */
public class BaseHomeFragment<V extends IMvpBaseView, P extends AbstractMvpPresenter<V>> extends BaseMvpFragment {
    private static final String TAG = "BaseHomeFragment";

    /**
     * 首次初始化
     */
    private boolean mFirstInit = true;

    /**
     * 是否可见
     */
    private boolean mVisiable;

    public boolean isVisiable() {
        return mVisiable;
    }

    public void setVisiable(boolean mVisiable) {
        this.mVisiable = mVisiable;
    }

    public boolean ismFirstInit() {
        return mFirstInit;
    }

    public void setmFirstInit(boolean mFirstInit) {
        this.mFirstInit = mFirstInit;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        L.debug(TAG, "onHiddenChanged----" + this.toString());
        if (!hidden) {
            mVisiable = true;
            L.debug(TAG, "开始统计%s", getClass().getName());
            MobclickAgent.onPageStart(getClass().getName());
        } else {
            mVisiable = false;
            L.debug(TAG, "结束统计%s", getClass().getName());
            MobclickAgent.onPageEnd(getClass().getName());
        }
    }

    @Override
    public void onResume() {
        L.debug(TAG, "onResume----" + this.toString());
        super.onResume();
        //若首次初始化,默认可见并开启友盟统计
        if (mFirstInit) {
            mVisiable = true;
            mFirstInit = false;
            L.debug(TAG, "开始统计%s", getClass().getName());
            MobclickAgent.onPageStart(getClass().getName());
            return;
        }

        //若当前界面可见,调用友盟开启跳转统计
        if (mVisiable) {
            L.debug(TAG, "开始统计%s", getClass().getName());
            MobclickAgent.onPageStart(getClass().getName());
        }
    }

    @Override
    public void onPause() {
        L.debug(TAG, "onPause----" + this.toString());
        super.onPause();
        //若当前界面可见,调用友盟结束跳转统计
        if (mVisiable) {
            L.debug(TAG, "结束统计%s", getClass().getName());
            MobclickAgent.onPageStart(getClass().getName());
        }
    }

    @Override
    public int getRootLayoutId() {
        return 0;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {

    }
}
