package com.tongdaxing.erban.common.ui.me.setting.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.presenter.me.setting.QrCodeScanResultPresenter;
import com.tongdaxing.erban.common.ui.me.setting.vew.QrCodeScanResultView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_framework.util.util.LogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@CreatePresenter(QrCodeScanResultPresenter.class)
public class QrCodeScanResultActivity extends BaseMvpActivity<QrCodeScanResultView, QrCodeScanResultPresenter>
        implements QrCodeScanResultView, View.OnClickListener {

    private final String TAG = QrCodeScanResultActivity.class.getSimpleName();
    @BindView(R2.id.title_bar)
    TitleBar titleBar;
    @BindView(R2.id.iv_loginPc)
    ImageView ivLoginPc;
    @BindView(R2.id.tv_cancel)
    TextView tvCancel;
    @BindView(R2.id.tv_login)
    TextView tvLogin;

    private String qrcodeResult = null;

    public static void start(Context context, String qrcodeResult) {
        Intent intent = new Intent(context, QrCodeScanResultActivity.class);
        intent.putExtra("qrcodeResult", qrcodeResult);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_scan_login);
        ButterKnife.bind(this);
        qrcodeResult = getIntent().getStringExtra("qrcodeResult");
        initTitleBar(getResources().getString(R.string.qrcode_scan_result_login_title));
    }

    @OnClick({R2.id.tv_login, R2.id.tv_cancel})
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tv_login) {
            getMvpPresenter().loginForPcByQrCode(qrcodeResult);

        } else if (i == R.id.tv_cancel) {
            QrCodeScanerActivity.start(this);
            finish();

        }
    }

    @Override
    protected void onLeftClickListener() {
        QrCodeScanerActivity.start(this);
        super.onLeftClickListener();
    }

    @Override
    public void onQrCodeLogin(boolean isSuccess, String msg) {
        LogUtils.d(TAG, "onQrCodeLogin-isSuccess:" + isSuccess + " msg:" + msg);
        if (!TextUtils.isEmpty(msg)) {
            toast(msg);
        }
        if (isSuccess) {
            finish();
        }
    }
}
