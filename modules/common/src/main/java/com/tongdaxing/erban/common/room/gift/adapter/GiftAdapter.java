package com.tongdaxing.erban.common.room.gift.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaoyu
 * @date 2017/12/11
 */

public class GiftAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<GiftInfo> giftInfoList;
    private Context context;
    private int index;
    private int resourceId = R.layout.list_item_gift;
    private OnItemClickListener mOnItemClickListener;

    public GiftAdapter(Context context) {
        if (this.giftInfoList == null)
            this.giftInfoList = new ArrayList<>();
        this.context = context;
    }

    public GiftAdapter(Context context, int resourceId) {
        if (this.giftInfoList == null)
            this.giftInfoList = new ArrayList<>();
        this.context = context;
        this.resourceId = resourceId;
    }

    public List<GiftInfo> getGiftInfoList() {
        return giftInfoList;
    }

    public void setGiftInfoList(List<GiftInfo> giftInfoList) {
        this.giftInfoList = giftInfoList;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public GiftInfo getIndexGift() {
        if (giftInfoList != null && giftInfoList.size() > index) {
            return giftInfoList.get(index);
        }
        return null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(resourceId, parent, false);
        return new GiftViewHolder(root);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
//        long t1 = System.currentTimeMillis();
        GiftViewHolder holder = (GiftViewHolder) viewHolder;
        GiftInfo giftInfo = giftInfoList.get(position);

        holder.giftName.setText(giftInfo.getGiftName());
        holder.giftGold.setText(giftInfo.getGoldPrice() + "");
        if (giftInfo.getUserGiftPurseNum() > 0 && giftInfo.isBackpackGift()) {//只有背包礼物才需要显示数量
            holder.freeGiftCount.setVisibility(View.VISIBLE);
            holder.freeGiftCount.setText("X" + giftInfo.getUserGiftPurseNum());
        } else {
            holder.freeGiftCount.setVisibility(View.GONE);
        }

        holder.bind(position);
        ImageLoadUtils.loadImage(BasicConfig.INSTANCE.getAppContext(), giftInfo.getGiftUrl(), holder.giftImage);
        if (position == index) {
//            holder.giftSelect.setVisibility(View.GONE);
            holder.giftImageSelected.setVisibility(View.VISIBLE);
        } else {
//            holder.giftSelect.setVisibility(View.GONE);
            holder.giftImageSelected.setVisibility(View.GONE);
        }

        if (giftInfo.isBackpackGift()) {//背包礼物不显示任何标签
            holder.ivNewLabel.setVisibility(View.GONE);
            holder.giftEffects.setVisibility(View.GONE);
            holder.giftLimitTime.setVisibility(View.GONE);
            holder.giftNew.setVisibility(View.GONE);
            return;
        }

        if (TextUtils.isEmpty(giftInfo.getGiftTag())) {
            holder.ivNewLabel.setVisibility(View.GONE);

            if (giftInfo.isHasEffect() && !giftInfo.isBackpackGift()) {//只有普通礼物才会显示标签
                holder.giftEffects.setVisibility(View.VISIBLE);
            } else {
                holder.giftEffects.setVisibility(View.GONE);
            }
            if (giftInfo.isHasTimeLimit() && !giftInfo.isBackpackGift()) {//只有普通礼物才会显示标签
                if (holder.giftEffects.getVisibility() == View.GONE) {
                    holder.giftLimitTime.setVisibility(View.GONE);
                } else {
                    holder.giftLimitTime.setVisibility(View.VISIBLE);
                }
            } else {
                holder.giftLimitTime.setVisibility(View.GONE);
            }
            if (giftInfo.isHasLatest() && !giftInfo.isBackpackGift()) {//只有普通礼物才会显示标签
                if (holder.giftEffects.getVisibility() == View.GONE &&
                        holder.giftLimitTime.getVisibility() == View.GONE) {
                    holder.giftNew.setVisibility(View.GONE);
                } else {
                    holder.giftNew.setVisibility(View.VISIBLE);
                }
            } else {
                holder.giftNew.setVisibility(View.GONE);
            }
        } else {
            holder.ivNewLabel.setVisibility(View.VISIBLE);
            holder.giftEffects.setVisibility(View.GONE);
            holder.giftLimitTime.setVisibility(View.GONE);
            holder.giftNew.setVisibility(View.GONE);
            ImageLoadUtils.loadImage(BasicConfig.INSTANCE.getAppContext(), giftInfo.getGiftTag(), holder.ivNewLabel);
        }
//        long t2 = System.currentTimeMillis();
//        Log.e("输出", ((long)(t2 - t1) )+ "");
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return giftInfoList == null ? 0 : giftInfoList.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    class GiftViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        int position;
        private ImageView giftImage;
        private ImageView giftImageSelected;
        private TextView giftName;
        private TextView giftGold;
        private ImageView giftSelect;
        private ImageView giftEffects;
        private ImageView giftNew;
        private ImageView giftLimitTime;
        private TextView freeGiftCount;
        private ImageView ivNewLabel;

        GiftViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ivNewLabel = (ImageView) itemView.findViewById(R.id.iv_new_label);
            giftImage = (ImageView) itemView.findViewById(R.id.gift_image);
            giftImageSelected = itemView.findViewById(R.id.gift_image_selected);
            giftGold = (TextView) itemView.findViewById(R.id.gift_gold);
            giftName = (TextView) itemView.findViewById(R.id.gift_name);
            giftEffects = (ImageView) itemView.findViewById(R.id.icon_gift_effect);
            giftNew = (ImageView) itemView.findViewById(R.id.icon_gift_new);
            giftLimitTime = (ImageView) itemView.findViewById(R.id.icon_gift_limit_time);
            giftSelect = (ImageView) itemView.findViewById(R.id.icon_room_gift_select);
            freeGiftCount = (TextView) itemView.findViewById(R.id.tv_free_gift_count);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, position);
            }
        }

        public void bind(int position) {
            this.position = position;
        }
    }
}
