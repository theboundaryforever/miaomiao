package com.tongdaxing.erban.common.room.lover.weekcp.topic.item;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

/**
 * Created by Chen on 2019/5/8.
 */
public class SoulTopicItemPresenter extends AbstractMvpPresenter<ISoulTopicItemView> {

    public List<String> getData(int index) {
        return CoreManager.getCore(IRoomCore.class).getWeekManager().getSoulTopicInfo().getSoulTabQuestions(index);
    }

    public void requestRoomPlay(String question) {
        CoreManager.getCore(IRoomCore.class).getWeekManager().getSoulTopicCtrl().doRoomPlay(question);
    }
}
