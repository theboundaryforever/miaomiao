package com.tongdaxing.erban.common.presenter.home;

import android.os.Handler;
import android.os.Looper;

import com.tongdaxing.erban.common.model.room.RoomModel;
import com.tongdaxing.erban.common.ui.home.adpater.RoomListAdapter;
import com.tongdaxing.erban.common.ui.home.fragment.HotTagFragment;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.AdvertInfo;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.home.HomeInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

import java.util.ArrayList;
import java.util.List;

public class HotTagPresenter extends AbstractMvpPresenter<IHotTagView> {
    private RoomModel dataSource;
    private int currPage = Constants.PAGE_START;

    private List<HomeRoom> hotRoomList;

    private int bannerPosition = RoomListAdapter.POSITION_NOT_BANNER;

    private Handler mHandler = new Handler(Looper.getMainLooper());
    private boolean isFirst = true;

    public HotTagPresenter() {
        dataSource = new RoomModel();
        hotRoomList = new ArrayList<>();
    }

    /**
     * 刷新数据
     */
    public void refreshData() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                getData(Constants.PAGE_START);
            }
        });
    }

    public void loadMoreData() {
        getData(currPage + 1);
    }

    private void getData(int page) {
        LogUtil.d(HotTagFragment.TAG, "getData()");
        dataSource.getHotTabData(page, new HttpRequestCallBack<HomeInfo>() {
            @Override
            public void onFinish() {
            }

            @Override
            public void onSuccess(String message, HomeInfo response) {
                if (getMvpView() == null) {
                    return;
                }
                currPage = page;
                hotRoomList = response == null ? new ArrayList<>() : response.listRoom;
                if (page == Constants.PAGE_START) {
                    LogUtil.d(HotTagFragment.TAG, "getData() onSuccess PAGE_START");
                    getMvpView().setupBanner(response == null ? null : response.banners);
//                    getMvpView().setupPopularList(hotRoomList.subList(0, 4));
                    getMvpView().setupPopularList(response == null ? null : response.hotRooms);
                    getMvpView().showNewUserRoomGuideDialog(response == null ? null : response.newbieRecom);
                    getAdvert();
                } else {
                    LogUtil.d(HotTagFragment.TAG, "getData() onSuccess setupListView");
                    getMvpView().setupListView(false, new ArrayList<>(hotRoomList), bannerPosition);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() == null) {
                    return;
                }
                LogUtil.d(HotTagFragment.TAG, "getData() onFailure: code=" + code + " msg=" + msg + " page=" + page);
                getMvpView().setupFailView(page == Constants.PAGE_START, msg);
            }
        });
    }

    /**
     * 获取节目预告和活位推荐
     */
    private void getAdvert() {
        LogUtil.d(HotTagFragment.TAG, "getAdvert()");
        dataSource.getAdvert(new HttpRequestCallBack<AdvertInfo>() {
            @Override
            public void onFinish() {
            }

            @Override
            public void onSuccess(String message, AdvertInfo response) {
                /**
                 * 此处展示逻辑
                 * 1、有banner，有活位，则展示banner和活位
                 * 2、有banner，无活位，则展示banner
                 * 3、无banner，有活位，都不展示
                 * 总结：活位能否展示，取决于banner有无数据
                 */
                if (response != null && !ListUtils.isListEmpty(response.getListBanner())) {//banner不空，插入活位
                    List<HomeRoom> roomList = new ArrayList<>();
                    int size = hotRoomList.size();
                    if (size > 6) {//如果有6个，那么活位插在第6位之后
                        LogUtil.d(HotTagFragment.TAG, "getAdvert() onSuccess size > 6");
                        roomList.addAll(hotRoomList.subList(0, 6));

                        //先增加banner
                        if (!ListUtils.isListEmpty(response.getListBanner())) {
                            bannerPosition = insertBanner(roomList, getBannerRoomInfo(response.getListBanner()));
                        } else {
                            bannerPosition = RoomListAdapter.POSITION_NOT_BANNER;
                        }

                        if (!ListUtils.isListEmpty(response.getListAdvert())) {//如果活位有数据，则插入数据
                            roomList.addAll(response.getListAdvert());
                        }

                        roomList.addAll(hotRoomList.subList(6, size));
                    } else {//如果没有6个，那么就直接插在最后
                        LogUtil.d(HotTagFragment.TAG, "getAdvert() onSuccess size <= 6");
                        roomList.addAll(hotRoomList);

                        //增加banner
                        if (!ListUtils.isListEmpty(response.getListBanner())) {
                            bannerPosition = insertBanner(roomList, getBannerRoomInfo(response.getListBanner()));
                        } else {
                            bannerPosition = RoomListAdapter.POSITION_NOT_BANNER;
                        }

                        if (!ListUtils.isListEmpty(response.getListAdvert())) {//如果活位有数据，则插入数据
                            roomList.addAll(response.getListAdvert());
                        }
                    }
                    getMvpView().setupListView(true, roomList, bannerPosition);
                } else {
                    LogUtil.d(HotTagFragment.TAG, "getAdvert() onSuccess new ArrayList<>(hotRoomList)");
                    bannerPosition = RoomListAdapter.POSITION_NOT_BANNER;
                    getMvpView().setupListView(true, new ArrayList<>(hotRoomList), bannerPosition);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtil.d(HotTagFragment.TAG, "getAdvert() onFailure: code=" + code + " msg=" + msg);
                bannerPosition = RoomListAdapter.POSITION_NOT_BANNER;
                getMvpView().setupListView(true, new ArrayList<>(hotRoomList), bannerPosition);
            }
        });
    }

    private HomeRoom getBannerRoomInfo(List<BannerInfo> bannerInfoList) {
        HomeRoom bannerInfo = new HomeRoom(-1);
        bannerInfo.setItemType(1);
        bannerInfo.setBannerInfos(bannerInfoList);
        return bannerInfo;
    }

    /**
     * 加进去的时候考虑不满一行的情况
     */
    private int insertBanner(List<HomeRoom> roomList, HomeRoom bannerRoomInfo) {
        int roomSize = roomList.size();
        int bannerPosition;
        if (roomSize % HotTagFragment.ITEM_COUNT_BY_ROW == 0) {
            bannerPosition = roomSize;
        } else {//如果不满一行，放到上一行
            bannerPosition = roomSize - roomSize % HotTagFragment.ITEM_COUNT_BY_ROW;
        }
        roomList.add(bannerPosition, bannerRoomInfo);
        return bannerPosition;
    }

    public boolean isFirstLoad() {
        return isFirst;
    }

    public void setIsFirstLoad(boolean isFirst) {
        this.isFirst = isFirst;
    }

    @Override
    public void onResumePresenter() {
        super.onResumePresenter();
        LogUtil.d(HotTagFragment.TAG, "onResumePresenter()");
        if (isFirstLoad() && CoreManager.getCore(IAuthCore.class).isLogin()) {
            LogUtil.d(HotTagFragment.TAG, "onResumePresenter() isFirstLoad");
            setIsFirstLoad(false);
            getMvpView().setViewLoading();
            refreshData();
        }
    }
}
