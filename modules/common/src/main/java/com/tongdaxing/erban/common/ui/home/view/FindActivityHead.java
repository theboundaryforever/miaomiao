package com.tongdaxing.erban.common.ui.home.view;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

/**
 * @zwk
 */
public class FindActivityHead extends LinearLayout {

    public RelativeLayout rlRank, rlShare;

    public FindActivityHead(Context context) {
        super(context);
        initView(context);
    }

    private void initView(Context context) {
        View view = inflate(context, R.layout.layout_find_activity_head, this);
        int width = ScreenUtil.getDisplayWidth() - DisplayUtils.dip2px(context, 20);
        //图片根据屏幕自适应宽高
        rlShare = view.findViewById(R.id.rl_activity_share);
        rlRank = view.findViewById(R.id.rl_chat_room);
        LayoutParams llShare = (LayoutParams) rlShare.getLayoutParams();
        LayoutParams llRank = (LayoutParams) rlRank.getLayoutParams();
        llShare.height = 90 * width / 355;
        llRank.height = llShare.height;
        rlShare.setLayoutParams(llShare);
        rlRank.setLayoutParams(llRank);
    }

    public void setOnShareClickListener(OnClickListener onShareClickListener) {
        rlShare.setOnClickListener(onShareClickListener);
    }

    public void setOnRankClickListener(OnClickListener onRankClickListener) {
        rlRank.setOnClickListener(onRankClickListener);
    }

    public void setChatRoomViewVisibility(int visibility) {
        if (rlRank != null) {
            rlRank.setVisibility(visibility);
        }
    }
}
