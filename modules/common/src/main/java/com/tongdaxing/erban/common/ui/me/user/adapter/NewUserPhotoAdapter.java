package com.tongdaxing.erban.common.ui.me.user.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenran on 2017/7/24.
 */

public class NewUserPhotoAdapter extends RecyclerView.Adapter<NewUserPhotoAdapter.UserPhtotViewHolder> {
    // private RealmList<UserPhoto> photoUrls;
    private List<UserPhoto> photoUrls;
    private boolean isSelf;
    private int type;
    private int itemWidth;
    private ImageClickListener mImageClickListener;

    public NewUserPhotoAdapter(int type, int itemWidth) {
        if (this.photoUrls == null)
            this.photoUrls = new ArrayList<>();
        this.photoUrls = photoUrls;
        this.type = type;
        this.itemWidth = itemWidth;
    }

    public void setImageClickListener(NewUserPhotoAdapter.ImageClickListener imageClickListener) {
        mImageClickListener = imageClickListener;
    }

    @Override
    public UserPhtotViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = null;
        if (type == 0) {
            item = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.list_item_user_photo_new, parent, false);
        } else {
            item = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.list_item_user_photo_edit_new, parent, false);
        }
        UserPhtotViewHolder holder = new UserPhtotViewHolder(item);
        return holder;
    }

    @Override
    public void onBindViewHolder(UserPhtotViewHolder holder, final int position) {
        LogUtil.d("UserPhotoAdapter", position + " " + position);
        LinearLayout.LayoutParams ll = (LinearLayout.LayoutParams) holder.mPhotoImage.getLayoutParams();
        ll.width = itemWidth;
        ll.height = itemWidth;
        holder.mPhotoImage.setLayoutParams(ll);
        if (isSelf && position == 0) {
            holder.mPhotoImage.setOnClickListener(v -> {
                if (mImageClickListener != null) {
                    mImageClickListener.addClick();
                }
            });

//            holder.mPhotoImage.setImageResource(R.drawable.icon_add_photo);
            ImageLoadUtils.loadRectangleImage(holder.mPhotoImage.getContext(),
                    R.drawable.user_info_ic_add_photo, holder.mPhotoImage,
                    R.drawable.default_cover, R.dimen.dp_20);
        } else {
            UserPhoto photo;
            if (isSelf) {
                photo = photoUrls.get(position - 1);
            } else {
                photo = photoUrls.get(position);
            }

            ImageLoadUtils.loadRectangleImage(holder.mPhotoImage.getContext(),
                    photo.getPhotoUrl(), holder.mPhotoImage,
                    R.drawable.default_cover, R.dimen.dp_20);
//            GlideApp.with(holder.mPhotoImage.getContext()).load(photo.getPhotoUrl()).transforms(new CenterCrop()).into(holder.mPhotoImage);
            holder.mPhotoImage.setOnClickListener(v -> {
                if (mImageClickListener != null) {
                    if (isSelf) {
                        mImageClickListener.click(position - 1, photo);
                    } else {
                        mImageClickListener.click(position, photo);
                    }
                }
            });
        }


    }

    public void setSelf(boolean self) {
        isSelf = self;
    }

    @Override
    public int getItemCount() {
        if (isSelf) {
            if (ListUtils.isListEmpty(photoUrls)) {
                return 1;
            } else {
                return (photoUrls.size() + 1) >= 9 ? 9 : (photoUrls.size() + 1);
            }
        } else {
            if (ListUtils.isListEmpty(photoUrls)) {
                return 0;
            } else {
                return photoUrls.size() > 9 ? 9 : photoUrls.size();
            }
        }
    }

    public void setPhotoUrls(List<UserPhoto> photoUrls) {
        if (!ListUtils.isListEmpty(photoUrls)) {
            this.photoUrls = photoUrls;
            notifyDataSetChanged();
        }
    }

    //    public interface ImageClickListener {
//        void click(int position, ArrayList<UserPhoto> photoUrl);
//    }
    public interface ImageClickListener {
        void click(int position, UserPhoto userPhoto);

        void addClick();
    }

    // ViewHolder
    public class UserPhtotViewHolder extends RecyclerView.ViewHolder {

        private ImageView mPhotoImage;

        public UserPhtotViewHolder(View itemView) {
            super(itemView);

            mPhotoImage = (ImageView) itemView.findViewById(R.id.iv_user_photo);
        }

    }
}
