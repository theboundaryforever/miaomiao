package com.tongdaxing.erban.common.ui.me.shopping.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseMvpListFragment;
import com.tongdaxing.erban.common.presenter.shopping.DressUpFragmentPresenter;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.shopping.activity.GiveGoodsActivity;
import com.tongdaxing.erban.common.ui.me.shopping.adapter.DressUpListAdapter;
import com.tongdaxing.erban.common.ui.me.shopping.listener.OnHeadWearCallback;
import com.tongdaxing.erban.common.ui.me.shopping.view.IDressUpFragmentView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.DressUpBean;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.List;

import static com.tongdaxing.erban.common.ui.me.shopping.activity.DressUpMallActivity.DRESS_CAR;
import static com.tongdaxing.erban.common.ui.me.shopping.activity.DressUpMallActivity.DRESS_HEADWEAR;

/**
 * 装扮商城分类列表模块
 *
 * @author zwk 2018/10/16
 */
@CreatePresenter(DressUpFragmentPresenter.class)
public class DressUpFragment extends BaseMvpListFragment<DressUpListAdapter, IDressUpFragmentView, DressUpFragmentPresenter> implements IDressUpFragmentView, DressUpListAdapter.OnDressUpClickListener, View.OnClickListener {
    private int type;//0 头饰 1 座驾
    private boolean isMySelf = false;
    private OnHeadWearCallback onHeadWearCallback;
    private LinearLayout llAction;//装扮操作布局
    private TextView tvLeftAct;//左侧功能按钮
    private TextView tvRightAct;//右侧功能按钮

    public static DressUpFragment newInstance(int type, boolean isMySelf) {
        DressUpFragment dressFragment = new DressUpFragment();
        Bundle dressBundle = new Bundle();
        dressBundle.putInt("type", type);
        dressBundle.putBoolean("isMySelf", isMySelf);
        dressFragment.setArguments(dressBundle);
        return dressFragment;
    }

    public void setOnHeadWearCallback(OnHeadWearCallback onHeadWearCallback) {
        this.onHeadWearCallback = onHeadWearCallback;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_dress_up_list;
    }

    @Override
    protected void getMyArguments() {
        if (getArguments() != null) {
            type = getArguments().getInt("type", DRESS_HEADWEAR);
            isMySelf = getArguments().getBoolean("isMySelf", false);
        }
    }

    @Override
    protected void initMyView() {
        llAction = mView.findViewById(R.id.ll_dress_up_action);
        tvLeftAct = mView.findViewById(R.id.tv_dress_up_left_action);
        tvRightAct = mView.findViewById(R.id.tv_dress_up_right_action);
    }

    @Override
    public void addItemDecoration() {
        if (rvList != null)
            rvList.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    outRect.top = 2;
                    if (parent.getChildAdapterPosition(view) % 2 == 0) {
                        outRect.right = 2;
                    }
                }
            });
    }

    @Override
    protected RecyclerView.LayoutManager initManager() {
        return new GridLayoutManager(mContext, 2);
    }

    @Override
    protected DressUpListAdapter initAdpater() {
        return new DressUpListAdapter(type, isMySelf);
    }

    @Override
    protected void initClickListener() {
        tvLeftAct.setOnClickListener(this);
        tvRightAct.setOnClickListener(this);
        if (mAdapter != null) {
            mAdapter.setOnDressUpClickListener(this);
        }
    }

    @Override
    public void initData() {
        getMvpPresenter().getDressUpData(isMySelf, type, mPage, pageSize);
    }

    @Override
    public void getDressUpListSuccess(ServiceResult<List<DressUpBean>> result) {
        String emtpy = "";
        if (isMySelf) {
            emtpy = getString(type == DRESS_HEADWEAR ? R.string.txt_my_headwear_emtpy : R.string.txt_my_car_emtpy);
        } else {
            emtpy = getString(type == DRESS_HEADWEAR ? R.string.txt_headwear_emtpy : R.string.txt_car_emtpy);
        }
        if (mPage == Constants.PAGE_START) {//刷新
            if (result == null || !result.isSuccess() || ListUtils.isListEmpty(result.getData())) {
                if (llAction.getVisibility() == View.VISIBLE) {
                    llAction.setVisibility(View.GONE);
                }
            }
        }
        dealSuccess(result, emtpy);
    }

    @Override
    public void getDressUpListFail(Exception e) {
        if (llAction.getVisibility() == View.VISIBLE)
            llAction.setVisibility(View.GONE);
        dealFail(e);
    }

    @Override
    public void onClick(View v) {
        if (mAdapter == null)
            return;
        DressUpBean data = mAdapter.getCurrentSelectData();
        if (data == null)
            return;
        int i = v.getId();//购买或者续费
        if (i == R.id.tv_dress_up_left_action) {
            if (isMySelf) {//自己
                if (data.getIsPurse() == 1 || data.getIsPurse() == 2) {
                    getDialogManager().showProgressDialog(mContext, getString(R.string.txt_dress_up_use_change));
                    int dressUpId = (type == DRESS_HEADWEAR ? data.getHeadwearId() : data.getCarId());
                    getMvpPresenter().onChangeDressUpState(type, data.getIsPurse() == 1 ? dressUpId : -1);
                }
                return;
            } else {//送好友
                Intent intent = new Intent(mContext, GiveGoodsActivity.class);
                intent.putExtra("carName", type == DRESS_HEADWEAR ? data.getHeadwearName() : data.getCarName());
                intent.putExtra("goodsId", (type == DRESS_HEADWEAR ? data.getHeadwearId() : data.getCarId()) + "");
                intent.putExtra("type", type == DRESS_HEADWEAR ? DRESS_HEADWEAR : DRESS_CAR);
                mContext.startActivity(intent);
            }

        } else if (i == R.id.tv_dress_up_right_action) {
            String msg = "确认" + (isMySelf ? "续费" : "购买") + "“" + (type == DRESS_HEADWEAR ? (data.getHeadwearName() + "”头饰") : (data.getCarName() + "”座驾"));
            getDialogManager().showOkCancelDialog(msg, true,
                    new DialogManager.AbsOkDialogListener() {
                        @Override
                        public void onOk() {
                            getMvpPresenter().onPurseDressUp(type, isMySelf ? 2 : 1, type == DRESS_HEADWEAR ? data.getHeadwearId() : data.getCarId());
                        }
                    });

        }

    }

    @Override
    public void onPurseDressUpSuccess(int purseType) {
        if (purseType == 1) {
            SingleToastUtil.showToast(getString(R.string.txt_purchase_success));
        } else if (purseType == 2) {
            SingleToastUtil.showToast(getString(R.string.txt_successful_renewal));
        }
        if (isMySelf) {
            onReloadData();
            CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        } else {
            if (purseType == 1) {
                CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            }
        }
    }

    @Override
    public void onPurseDressUpFail(String error) {
        SingleToastUtil.showToast(error);
    }


    @Override
    public void onChangeDressUpStateSuccess(int dressUpId) {
        getDialogManager().dismissDialog();
        if (dressUpId == -1) {
            SingleToastUtil.showToast(getString(R.string.txt_cancel_use_success));
            if (tvLeftAct != null) {
                tvLeftAct.setText(R.string.txt_use);
            }
            if (mAdapter != null && mAdapter.getCurrentSelectData() != null) {
                mAdapter.getCurrentSelectData().setIsPurse(1);
                mAdapter.notifyDataSetChanged();
            }
        } else {
            SingleToastUtil.showToast(getString(R.string.txt_use_success));
            if (tvLeftAct != null) {
                tvLeftAct.setText(R.string.txt_cancel_use);
            }
            if (mAdapter != null && mAdapter.getCurrentSelectData() != null) {
                mAdapter.resetUseState(dressUpId);
            }
        }
        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @Override
    public void onChangeDressUpStateFail(String error) {
        getDialogManager().dismissDialog();
        SingleToastUtil.showToast(error);
    }

    @Override
    public void onDressUpItemClickListener(DressUpBean item) {
        if (item == null)
            return;
        dealWithSelectStae(item);
        if (onHeadWearCallback != null) {
            if (type == DRESS_HEADWEAR) {
                onHeadWearCallback.onHeadWearChangeListener(item.getPicUrl());
            }
        }
    }

    /**
     * 根据选中的装扮的信息修改底部功能按钮状态
     *
     * @param item
     */
    private void dealWithSelectStae(DressUpBean item) {
        //默认不显示
        if (llAction.getVisibility() == View.GONE)
            llAction.setVisibility(View.VISIBLE);
        if (isMySelf) {//我的装扮
            if (item.getIsPurse() == 1) {//已购买
                tvLeftAct.setText(R.string.txt_use);
            } else if (item.getIsPurse() == 2) {//使用中
                tvLeftAct.setText(R.string.txt_cancel_use);
            }
            if (!item.isAllowPurse()) {//不能购买
                if (tvRightAct.getVisibility() == View.VISIBLE) {
                    tvRightAct.setVisibility(View.GONE);
                }
            } else {//续费
                if (tvRightAct.getVisibility() == View.GONE) {
                    tvRightAct.setVisibility(View.VISIBLE);
                }
                tvRightAct.setText(R.string.txt_renewal_fee);
            }
        } else {//装扮商城
            tvLeftAct.setText(R.string.txt_dress_up_send_firend);
            tvRightAct.setText(R.string.txt_buy);
        }
    }

    @Override
    public void onCarTryClickListener(String vggUrl) {
        if (onHeadWearCallback != null) {
            onHeadWearCallback.onCarTryListener(vggUrl);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHeadWearCallback) {
            setOnHeadWearCallback((OnHeadWearCallback) context);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onHeadWearCallback = null;
    }

}
