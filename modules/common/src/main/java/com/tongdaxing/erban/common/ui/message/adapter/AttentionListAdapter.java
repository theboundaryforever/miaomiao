package com.tongdaxing.erban.common.ui.message.adapter;

import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;

import java.util.List;

/**
 * <p> 关注列表 </p>
 * Created by Administrator on 2017/11/17.
 */
public class AttentionListAdapter extends BaseQuickAdapter<AttentionInfo, BaseViewHolder> {
    private boolean fromInViteCp;
    private onClickListener rylListener;

    public AttentionListAdapter(List<AttentionInfo> attentionInfoList) {
        super(R.layout.attention_item, attentionInfoList);
    }

    public AttentionListAdapter(List<AttentionInfo> attentionInfoList, boolean fromInViteCp) {
        super(R.layout.attention_item, attentionInfoList);
        this.fromInViteCp = fromInViteCp;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final AttentionInfo attentionInfo) {
        if (attentionInfo == null) return;
        baseViewHolder.setText(R.id.tv_userName, attentionInfo.getNick())
                .setText(R.id.tv_fans_count, "粉丝:" + String.valueOf(attentionInfo.getFansNum()))
                .setVisible(R.id.view_line, baseViewHolder.getLayoutPosition() != getItemCount() - 1)
                .setVisible(R.id.online, attentionInfo.getOperatorStatus() != 2)
                .setVisible(R.id.tv_invite_cp, fromInViteCp)
                .setVisible(R.id.find_him_layout, !(fromInViteCp || attentionInfo.getUserInRoom() == null))
                .setOnClickListener(R.id.find_him_layout, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (rylListener != null)
                            rylListener.findHimListeners(attentionInfo);
                    }
                })
                .setOnClickListener(R.id.rly, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (rylListener != null)
                            rylListener.rylListeners(attentionInfo);
                    }
                })
                .setOnClickListener(R.id.tv_invite_cp, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (rylListener != null)
                            rylListener.openCpGiftDialog(attentionInfo);
                    }
                });
//
//        if (attentionInfo.getUserInRoom() != null) {
//            LogUtil.d("attentionInfo.getUserInRoom() != null", attentionInfo.getUserInRoom() + "");
//        }
        ImageView imageView = baseViewHolder.getView(R.id.imageView);
        ImageLoadUtils.loadCircleImage(mContext, attentionInfo.getAvatar(), imageView, R.drawable.ic_no_avatar);

    }

    public void setRylListener(onClickListener onClickListener) {
        rylListener = onClickListener;
    }

    public interface onClickListener {
        void rylListeners(AttentionInfo attentionInfo);

        void findHimListeners(AttentionInfo attentionInfo);

        void openCpGiftDialog(AttentionInfo attentionInfo);
    }
}
