package com.tongdaxing.erban.common.ui.widget.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.me.withdraw.WxBindingActivity;

/**
 * Function:通用标题对话框
 * Author: Edward on 2019/1/9
 */
@Deprecated
public class CommonTitleDialog extends DialogFragment {
    public static final int REQUEST_CODE = 1001;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getContentView(), container, false);
        initView(view);
        initData();
        initDialog();
        return view;
    }

    private void initDialog() {
        Dialog dialog = getDialog();
        if (null != dialog) {
            Window window = dialog.getWindow();
            if (null != window) {
                window.setBackgroundDrawable(new ColorDrawable(0));
            }
        }
    }

    public void initView(View view) {
        TextView tvCancel = view.findViewById(R.id.tv_cancel);
        tvCancel.setOnClickListener(v -> dismiss());
        TextView tvSure = view.findViewById(R.id.tv_sure);
        tvSure.setOnClickListener(v -> {
//            CommonWebViewActivity.start(getActivity(), UriProvider.getWithdrawal());
            WxBindingActivity.go(getActivity(), REQUEST_CODE);  // todo 163不发版的话 打开注释
            dismiss();
        });
    }

    public void initData() {
    }

    public int getContentView() {
        return R.layout.dialog_common_title;
    }
}
