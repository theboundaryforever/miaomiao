package com.tongdaxing.erban.common.ui.widget.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.juxiao.library_ui.widget.ViewHolder;
import com.juxiao.library_ui.widget.dialog.BaseDialog;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.uuzuche.lib_zxing.DisplayUtil;

/**
 * Function: 通用对话框 确定、取消 or 确定
 * Author: zhangjian on 2019/7/14
 */
public class CommonDialog extends BaseDialog {
    private OnClickListener mOnClickListener;
    private TextView tvCancel;
    private View one;
    private boolean singleOkMode;

    public void setSingleOkMode() {
        singleOkMode = true;
    }

    public void setmOnClickListener(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null && getContext() != null) {
                int color = ContextCompat.getColor(getContext(), android.R.color.transparent);
                window.setBackgroundDrawable(new ColorDrawable(color));
                window.setLayout(DisplayUtil.dip2px(getContext(), 280.0f), WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }
    }

    @Override
    public void convertView(ViewHolder viewHolder) {
        Bundle bundle = getArguments();
        if (bundle == null) {
            SingleToastUtil.showToast("数据错误!");
            dismiss();
            return;
        }

        TextView tvTitle = viewHolder.getView(R.id.tv_title);
        String title = bundle.getString("title");
        tvTitle.setText(title);

        tvCancel = viewHolder.getView(R.id.tv_cancel);
        one = viewHolder.getView(R.id.one);
        if (singleOkMode) {
            tvCancel.setVisibility(View.GONE);
            one.setVisibility(View.GONE);
        }
        tvCancel.setOnClickListener(v -> {
            this.dismiss();
            if (mOnClickListener != null) {
                mOnClickListener.onCancelClick();
            }
        });

        TextView tvOk = viewHolder.getView(R.id.tv_ok);
        tvOk.setOnClickListener(v -> {
            if (mOnClickListener != null) {
                this.dismiss();
                mOnClickListener.onOKClick();
            }
        });
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_common;
    }

    public interface OnClickListener {
        void onOKClick();

        void onCancelClick();
    }
}

