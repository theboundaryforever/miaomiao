package com.tongdaxing.erban.common.room.talk.adapter;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by Chen on 2019/4/18.
 */

public interface IRecyclerView<M> {

    List<M> getData();

    void add(@NonNull M elem);

    void addAll(@NonNull List<M> elem);

    void add(@NonNull int index, @NonNull List<M> elem);

    void set(@NonNull M oldElem, @NonNull M newElem);

    void set(@NonNull int index, @NonNull M elem);

    void remove(@NonNull M elem);

    void remove(@NonNull int index);

    void replaceAll(@NonNull List<M> elem);

    boolean contains(@NonNull M elem);

    void clear();

    void notifyDataChange(int index);

}

