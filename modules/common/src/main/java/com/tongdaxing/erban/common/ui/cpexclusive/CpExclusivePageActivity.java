package com.tongdaxing.erban.common.ui.cpexclusive;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.baseview.BaseDialogFragment;
import com.juxiao.library_ui.widget.AppToolBar;
import com.opensource.svgaplayer.SVGAImageView;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.presenter.find.CpExclusivePagePresenter;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.cpexclusive.editSign.CpExclusivePageARouterPath;
import com.tongdaxing.erban.common.ui.find.view.ICpExclusivePageView;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.utils.HttpUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.erban.libcommon.widget.CpSignImageSpan;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.find.bean.CpUserPageVo;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Function: CP专属页
 * Author: Edward on 2019/3/28
 */
@CreatePresenter(CpExclusivePagePresenter.class)
public class CpExclusivePageActivity extends BaseMvpActivity<ICpExclusivePageView, CpExclusivePagePresenter> implements ICpExclusivePageView {
    @BindView(R2.id.iv_user_head_one)
    CircleImageView ivUserHeadOne;
    @BindView(R2.id.iv_user_head_two)
    CircleImageView ivUserHeadTwo;
    @BindView(R2.id.tv_name_one)
    TextView tvNameOne;
    @BindView(R2.id.tv_name_two)
    TextView tvNameTwo;
    @BindView(R2.id.tv_ranking_list)
    ImageView tvRankingList;
    @BindView(R2.id.app_tool_bar)
    AppToolBar appToolBar;
    @BindView(R2.id.tv_intimacy)
    TextView tvIntimacy;
    @BindView(R2.id.tv_together_day)
    TextView tvTogetherDay;
    @BindView(R2.id.iv_rule)
    ImageView ivRule;
    @BindView(R2.id.iv_cp_gift)
    SVGAImageView mIvCpGift;
    @BindView(R2.id.rl_bg)
    RelativeLayout rlBg;
    @BindView(R2.id.cp_exclusive_page_iv_edit_cp_sign)
    ImageView cpExclusivePageIvEditCpSign;
    @BindView(R2.id.cp_exclusive_page_tv_sign_two)
    TextView cpExclusivePageTvSignTwo;
    @BindView(R2.id.cp_exclusive_page_tv_sign_one)
    TextView cpExclusivePageTvSignOne;
    @BindView(R2.id.cp_exclusive_page_iv_formal_cp)
    ImageView cpExclusivePageIvFormalCp;
    private int[] cpBg = {R.drawable.cp_exclusive_bg_1, R.drawable.cp_exclusive_bg_2, R.drawable.cp_exclusive_bg_3};
    private int[] cpLoveBg = {R.drawable.cp_exclusive_love_1, R.drawable.cp_exclusive_love_2, R.drawable.cp_exclusive_love_3};
    private long userId;
    private boolean showRelieveCp;//是否展示解除cp 编辑称号牌 结成正式cp等按钮
    private CpUserPageVo cpUserPageVo;
    private boolean currentLoginUserIsInviter = true;//当前登录用户是cp专属页中的邀请者还是接受者
    private long loginUid;
    private List<ButtonItem> mButtonItemList;

    public static void start(Context context, long userId) {
        start(context, userId, false);
    }

    public static void start(Context context, long userId, boolean showRelieveCp) {
        Intent intent = new Intent(context, CpExclusivePageActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("showRelieveCp", showRelieveCp);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cp_exclusive_page);
        ButterKnife.bind(this);

        loginUid = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid();
        userId = getIntent().getLongExtra("userId", 0);
        showRelieveCp = getIntent().getBooleanExtra("showRelieveCp", false);
        if (userId <= 0) {
            return;
        }
        setItems();
        setAppToolBar();
        getMvpPresenter().loadData(userId);
        setCallback();
    }

    private void setAppToolBar() {
        appToolBar.setOnLeftImgBtnClickListener(this::finish);
        if (showRelieveCp) {
            appToolBar.setRightImageBtnVisibility(View.VISIBLE);//默认是不出现的
            appToolBar.setOnRightImgBtnClickListener(this::showItems);
            cpExclusivePageIvEditCpSign.setVisibility(View.VISIBLE);
        } else {
            appToolBar.setRightImageBtnVisibility(View.GONE);
            ivRule.setTranslationX(DisplayUtils.dip2px(getBaseContext(), 50));
        }
    }

    private void setItems() {
        ButtonItem relieveCp = new ButtonItem("解除CP", () -> {
            String tip = "确认解除CP关系？";
            getDialogManager().showOkCancelDialog(tip, true, new DialogManager.OkCancelDialogListener() {
                @Override
                public void onCancel() {
                    getDialogManager().dismissDialog();
                }

                @Override
                public void onOk() {
                    getDialogManager().showProgressDialog(CpExclusivePageActivity.this, "");
                    getMvpPresenter().relieveCp();
                }
            });
        });
        mButtonItemList = new ArrayList<>();
        mButtonItemList.add(relieveCp);
    }

    private void showItems() {
        getDialogManager().showCommonPopupDialog(mButtonItemList, "取消", false);
    }

    @Override
    public void relieveCpSucceed() {
        getDialogManager().dismissDialog();
        toast("解除成功");
        finish();
    }

    @Override
    public void relieveCpFailure() {
        getDialogManager().dismissDialog();
        toast("解除失败!");
    }

    @Override
    public void setCpSignSucceed() {
        getDialogManager().dismissDialog();
        toast("设置成功，已佩戴");

    }

    @Override
    public void setCpSignFailure() {
        getDialogManager().dismissDialog();
        toast("设置失败!");
    }

    private void setCallback() {
        ivRule.setOnClickListener(v -> {
            CommonWebViewActivity.start(this, UriProvider.getCpRule());
        });
        tvRankingList.setOnClickListener(v -> {
            CommonWebViewActivity.start(this, UriProvider.getCpRankingList());
        });
    }

    @Override
    public void loadFailure(String errorStr) {
        toast(errorStr);
    }

    private String convertNumber(int i) {
        if (i >= 100000) {
            int temp = i / 10000;
            return String.valueOf(temp) + "w";
        } else {
            return String.valueOf(i);
        }
    }

    @Override
    public void loadSucceed(CpUserPageVo cpUserPageVo) {
        if (cpUserPageVo == null || cpUserPageVo.getInviteUser() == null || cpUserPageVo.getRecUser() == null) {
            showNoData();
            return;
        }
        this.cpUserPageVo = cpUserPageVo;
        tvIntimacy.setText(convertNumber(cpUserPageVo.getCloseLevel()));
        if (cpUserPageVo.getCloseLevel() >= 0 && cpUserPageVo.getCloseLevel() < 120000) {
            rlBg.setBackgroundResource(cpBg[0]);
            tvTogetherDay.setBackgroundResource(cpLoveBg[0]);
        } else if (cpUserPageVo.getCloseLevel() >= 120000 && cpUserPageVo.getCloseLevel() < 1800000) {
            rlBg.setBackgroundResource(cpBg[1]);
            tvTogetherDay.setBackgroundResource(cpLoveBg[1]);
        } else if (cpUserPageVo.getCloseLevel() >= 1800000) {
            rlBg.setBackgroundResource(cpBg[2]);
            tvTogetherDay.setBackgroundResource(cpLoveBg[2]);
        }
        if (cpUserPageVo.getCpType() == CpUserPageVo.CP_TYPE_FORMAL) {
            mIvCpGift.setVisibility(View.VISIBLE);
            tvTogetherDay.setText("我们在一起\n" + cpUserPageVo.getCreate_date() + "天");
            if (!TextUtils.isEmpty(cpUserPageVo.getVggUrl())) {
                SvgaUtils.cyclePlayWebAnim(this, mIvCpGift, cpUserPageVo.getVggUrl());
            }
        } else {
            if (showRelieveCp) {
                cpExclusivePageIvFormalCp.setVisibility(View.VISIBLE);
            }
            tvTogetherDay.setText("一周CP\n剩余" + cpUserPageVo.getCreate_date() + "天");
        }
        if (cpUserPageVo.getCpGiftLevel() == 0) {
            cpUserPageVo.setCpGiftLevel(1);
        }

        if (cpUserPageVo.getInviteUser().getUid() == userId) {
            tvNameOne.setText(cpUserPageVo.getInviteUser().getNick());
            ImageLoadUtils.loadCircleImage(this, cpUserPageVo.getInviteUser().getAvatar(), ivUserHeadOne, R.drawable.default_cover);
            setCpSign(cpExclusivePageTvSignOne, cpUserPageVo.getInviteSign());
            tvNameTwo.setText(cpUserPageVo.getRecUser().getNick());
            ImageLoadUtils.loadCircleImage(this, cpUserPageVo.getRecUser().getAvatar(), ivUserHeadTwo, R.drawable.default_cover);
            setCpSign(cpExclusivePageTvSignTwo, cpUserPageVo.getRecSign());
        } else {
            tvNameTwo.setText(cpUserPageVo.getInviteUser().getNick());
            ImageLoadUtils.loadCircleImage(this, cpUserPageVo.getInviteUser().getAvatar(), ivUserHeadTwo, R.drawable.default_cover);
            setCpSign(cpExclusivePageTvSignTwo, cpUserPageVo.getInviteSign());
            tvNameOne.setText(cpUserPageVo.getRecUser().getNick());
            ImageLoadUtils.loadCircleImage(this, cpUserPageVo.getRecUser().getAvatar(), ivUserHeadOne, R.drawable.default_cover);
            setCpSign(cpExclusivePageTvSignOne, cpUserPageVo.getRecSign());
        }

        long inviteUserUid = cpUserPageVo.getInviteUser().getUid();
        currentLoginUserIsInviter = inviteUserUid == loginUid;
        if (showRelieveCp) {
            if ((currentLoginUserIsInviter && TextUtils.isEmpty(cpUserPageVo.getInviteSign())) ||
                    (!currentLoginUserIsInviter && TextUtils.isEmpty(cpUserPageVo.getRecSign()))) {
                onCpExclusivePageIvEditCpSignClicked();
            }
        }
    }

    private void setCpSign(TextView tv, String s) {
        if (TextUtils.isEmpty(s)) {
            s = "";
        }
        SpannableStringBuilder builder = new SpannableStringBuilder("");
        builder.append(BaseConstant.CP_SIGN_PLACEHOLDER);
        CpSignImageSpan urlImageSpan = new CpSignImageSpan(getBaseContext(), getCpLevelSignBg(cpUserPageVo.getCpGiftLevel()), s);
        builder.setSpan(urlImageSpan, builder.toString().length() - BaseConstant.CP_SIGN_PLACEHOLDER.length(),
                builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setText(builder);
    }

    private int getCpLevelSignBg(int cpLevel) {
        switch (cpLevel) {
            case 1:
                return R.drawable.cp_chenghaopai_one;
            case 2:
                return R.drawable.cp_chenghaopai_two;
            case 3:
                return R.drawable.cp_chenghaopai_three;
            default:
                return R.drawable.cp_chenghaopai_one;
        }
    }

    @Override
    public void onFreshSignFromEdit(String sign) {
        if (userId == loginUid) {
            setCpSign(cpExclusivePageTvSignOne, sign);
        } else {
            setCpSign(cpExclusivePageTvSignTwo, sign);
        }
    }

    /**
     * 余额不足，是否充值
     */
    @Override
    public void onNeedCharge() {
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    MyWalletNewActivity.start(CpExclusivePageActivity.this);
                    fragment.dismiss();
                }
            }
        }).show(this.getSupportFragmentManager(), "charge");
    }

    @OnClick(R2.id.iv_user_head_one)
    public void onCPByMeHeadClick() {
        if (cpUserPageVo != null && cpUserPageVo.getInviteUser() != null && cpUserPageVo.getRecUser() != null) {
            if (cpUserPageVo.getInviteUser().getUid() == userId) {
                NewUserInfoActivity.start(this, cpUserPageVo.getInviteUser().getUid());
            } else {
                NewUserInfoActivity.start(this, cpUserPageVo.getRecUser().getUid());
            }
        }
    }

    @OnClick(R2.id.iv_user_head_two)
    public void onCPByHeHeadClick() {
        if (cpUserPageVo != null && cpUserPageVo.getInviteUser() != null && cpUserPageVo.getRecUser() != null) {
            if (cpUserPageVo.getInviteUser().getUid() == userId) {
                NewUserInfoActivity.start(this, cpUserPageVo.getRecUser().getUid());
            } else {
                NewUserInfoActivity.start(this, cpUserPageVo.getInviteUser().getUid());
            }
        }
    }

    /**
     * 打开编辑称号牌
     */
    @OnClick(R2.id.cp_exclusive_page_iv_edit_cp_sign)
    public void onCpExclusivePageIvEditCpSignClicked() {
        BaseDialogFragment dialogFragment = (BaseDialogFragment) (ARouter.getInstance()
                .build(CpExclusivePageARouterPath.CP_EXCLUSIVE_PAGE_EDIT_SIGN_PATH)
                .navigation(this));
        if (dialogFragment.isAdded()) {
            if (dialogFragment.isStateSaved()) {
                dialogFragment.dismissAllowingStateLoss();
            }
        } else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            dialogFragment.show(fragmentManager, "CpEditSignDialogFragment");
        }
    }

    /**
     * 申请正式CP 弹出私聊 并弹出CP申请戒指栏
     */
    @OnClick(R2.id.cp_exclusive_page_iv_formal_cp)
    public void onCpExclusivePageIvFormalCpClicked() {
        if (this.isFinishing() || this.isDestroyed()) {
            return;
        }
        long targetUid;
        if (currentLoginUserIsInviter) {
            targetUid = cpUserPageVo.getRecUser().getUid();
        } else {
            targetUid = cpUserPageVo.getInviteUser().getUid();
        }
        boolean isFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(targetUid + "");
        HttpUtil.checkUserIsDisturb(this, isFriend, targetUid, true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SvgaUtils.closeSvga(mIvCpGift);
    }
}
