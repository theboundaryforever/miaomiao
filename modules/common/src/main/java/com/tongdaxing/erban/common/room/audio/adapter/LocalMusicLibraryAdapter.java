package com.tongdaxing.erban.common.room.audio.adapter;

import android.text.TextUtils;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.room.audio.view.ILocalMusicLibraryView;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.util.FileUtil;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public class LocalMusicLibraryAdapter extends BaseQuickAdapter<MusicLocalInfo, BaseViewHolder> {
    public static final int FILE_STATUS_UPLOAD = 0;
    public static final int FILE_STATUS_PLAY = 1;
    public static final int FILE_STATUS_STOP = 2;
    public static final int FILE_STATUS_UPLOADING = 3;
    public static final int FILE_STATUS_NO = 4;//表示无状态

    private ILocalMusicLibraryView localMusicLibraryView;

    public LocalMusicLibraryAdapter(ILocalMusicLibraryView localMusicLibraryView) {
        super(R.layout.adapter_local_music);
        this.localMusicLibraryView = localMusicLibraryView;
    }

    private String resolveUnknownName(String name) {
        if (TextUtils.isEmpty(name)) {
            return "";
        }

        if (name.contains("<unknown>") || name.contains("unknown")) {
            return "未知艺术家";
        } else {
            return name;
        }
    }

    @Override
    protected void convert(BaseViewHolder helper, MusicLocalInfo item) {
        helper.setText(R.id.tv_music_name, item.getSongName());
        helper.setText(R.id.tv_singer_name, resolveUnknownName(item.getSingerName()) + "-" + FileUtil.getFileSizeMB(item.getFileSize()) + "m");
        ImageView ivUploadMusic = helper.getView(R.id.iv_upload_music);

        if (item.getFileStatus() == FILE_STATUS_UPLOAD) {
            ivUploadMusic.setImageResource(R.drawable.ic_upload_music);
        } else if (item.getFileStatus() == FILE_STATUS_PLAY) {
            ivUploadMusic.setImageResource(R.drawable.ic_player_stop);
        } else if (item.getFileStatus() == FILE_STATUS_STOP) {
            ivUploadMusic.setImageResource(R.drawable.ic_player_play);
        } else if (item.getFileStatus() == FILE_STATUS_UPLOADING) {
            ImageLoadUtils.loadImage(mContext, R.drawable.ic_music_upload_loading, ivUploadMusic);
        } else {//最后一种无状态
            ivUploadMusic.setImageResource(FILE_STATUS_UPLOAD);
        }
    }


}
