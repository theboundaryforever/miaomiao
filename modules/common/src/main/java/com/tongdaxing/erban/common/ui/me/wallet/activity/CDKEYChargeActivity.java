package com.tongdaxing.erban.common.ui.me.wallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.ui.me.wallet.view.ChargeDialog;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * Created by chenran on 2017/11/15.
 */
public class CDKEYChargeActivity extends BaseActivity implements View.OnClickListener {
    private EditText editText;
    private Button charge;

    public static void start(Context context) {
        if (context == null) return;
        Intent intent = new Intent(context, CDKEYChargeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cd_key_charge);
        initTitleBar("兑换码充值");

        editText = (EditText) findViewById(R.id.cd_key_edit);
        charge = (Button) findViewById(R.id.btn_charge);
        charge.setOnClickListener(this);
    }

    @Override
    protected int setBgColor() {
        return R.color.white;
    }

    @Override
    public void onClick(View v) {
        String text = editText.getText().toString();
        if (StringUtil.isEmpty(text)) {
            toast("兑换码为空");
            return;
        }

        getDialogManager().showProgressDialog(this, "请稍后");
        CoreManager.getCore(IPayCore.class).requestCDKeyCharge(editText.getText().toString());
        editText.setText("");
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onCDKeyCharge(int gold) {
        getDialogManager().dismissDialog();
        ChargeDialog dialog = new ChargeDialog(this, gold);
        dialog.show();
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onCDKeyChargeFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }
}
