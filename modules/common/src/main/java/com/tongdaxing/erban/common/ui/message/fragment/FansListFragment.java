package com.tongdaxing.erban.common.ui.message.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.message.adapter.FansViewAdapter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.user.AttentionCore;
import com.tongdaxing.xchat_core.user.AttentionCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.FansInfo;
import com.tongdaxing.xchat_core.user.bean.FansListInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 粉丝列表
 * Created by chenran on 2017/10/2.
 */
public class FansListFragment extends BaseFragment {
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FansViewAdapter adapter;
    private int mCurrentCounter = Constants.PAGE_START;
    private List<FansInfo> mFansInfoList = new ArrayList<>();
    private Context mContext;
    private int mPageType;

    public static FansListFragment newInstance(int pageType) {
        FansListFragment fragment = new FansListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.KEY_PAGE_TYPE, pageType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (bundle != null) {
            mPageType = bundle.getInt(Constants.KEY_PAGE_TYPE);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }

    @Override
    public void onFindViews() {
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipe_refresh);
    }

    @Override
    public void onSetListener() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new FansViewAdapter(mFansInfoList);
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentCounter++;
                onRefreshing();
            }
        }, mRecyclerView);
        mRecyclerView.setAdapter(adapter);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentCounter = Constants.PAGE_START;
                onRefreshing();
            }
        });
        adapter.setRylListener(new FansViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(FansInfo fansInfo) {
                if (90000000 == fansInfo.getUid()) {
                    return;
                }

                NewUserInfoActivity.start(mContext, fansInfo.getUid());
            }

            @Override
            public void onAttentionBtnClick(FansInfo fansInfo) {
                getDialogManager().showProgressDialog(mContext, getString(R.string.waiting_text));
                CoreManager.getCore(IPraiseCore.class).praise(fansInfo.getUid());
            }
        });
    }


    private void onRefreshing() {
        CoreManager.getCore(AttentionCore.class).getFansList(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                mCurrentCounter, Constants.PAGE_SIZE, mPageType);
    }

    @Override
    public void initiate() {
        showLoading();
        onRefreshing();
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_fans_list;
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long uid) {
        getDialogManager().dismissDialog();
        List<FansInfo> data = adapter.getData();
        if (!ListUtils.isListEmpty(data)) {
            for (FansInfo fansInfo : data) {
                if (!fansInfo.isValid()) {
                    fansInfo.setValid(fansInfo.getUid() == uid);
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid) {
        List<FansInfo> data = adapter.getData();
        if (!ListUtils.isListEmpty(data)) {
            for (FansInfo fansInfo : data) {
                if (fansInfo.isValid()) {
                    fansInfo.setValid(fansInfo.getUid() == likedUid);
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFail(String error) {
        toast(error);
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = AttentionCoreClient.class)
    public void onGetMyFansList(FansListInfo fansListInfo, int pageType, int page) {
        mCurrentCounter = page;
        if (pageType == mPageType) {
            mSwipeRefreshLayout.setRefreshing(false);
            if (fansListInfo == null || ListUtils.isListEmpty(fansListInfo.getFansList())) {
                //第一页
                if (mCurrentCounter == Constants.PAGE_START) {
                    showNoData(getString(R.string.no_fan_text));
                } else {
                    adapter.loadMoreEnd(true);
                }
            } else {
                hideStatus();
                if (mCurrentCounter == Constants.PAGE_START) {
                    mFansInfoList.clear();
                    List<FansInfo> fansList = fansListInfo.getFansList();
                    mFansInfoList.addAll(fansList);
                    adapter.setNewData(mFansInfoList);
                    if (fansList.size() < Constants.PAGE_SIZE) {
                        adapter.setEnableLoadMore(false);
                    }
                    return;
                }
                adapter.loadMoreComplete();
                adapter.addData(fansListInfo.getFansList());
            }
        }
    }

    @CoreEvent(coreClientClass = AttentionCoreClient.class)
    public void onGetMyFansListFail(String error, int pageType, int page) {
        mCurrentCounter = page;
        if (pageType == mPageType) {
            if (mCurrentCounter == Constants.PAGE_START) {
                mSwipeRefreshLayout.setRefreshing(false);
                showNetworkErr();
            } else {
                adapter.loadMoreFail();
                toast(error);
            }
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        mCurrentCounter = Constants.PAGE_START;
        onRefreshing();
    }


    @Override
    public void onReloadData() {
        super.onReloadData();
        mCurrentCounter = Constants.PAGE_START;
        showLoading();
        onRefreshing();
    }
}
