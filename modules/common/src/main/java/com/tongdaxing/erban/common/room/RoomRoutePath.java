package com.tongdaxing.erban.common.room;

/**
 * Created by Chen on 2019/6/12.
 */
public class RoomRoutePath {
    public final static String ROOM_USER_INFO_PATH = "/common/UserInfoDialogFragment";
    public final static String ROOM_USER_INFO_USER_ID = "user_id";

}
