package com.tongdaxing.erban.common.room.talk.factory;

import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.juxiao.library_ui.widget.EmojiTextView;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomNotificationAttachment;
import com.netease.nimlib.sdk.msg.constant.NotificationType;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.talk.message.NotificationMessage;
import com.tongdaxing.xchat_framework.util.util.RichTextUtil;
import com.tongdaxing.xchat_framework.util.util.json.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Administrator on 5/31 0031.
 */

public class NotificationFactory implements AbsBaseViewHolder.ViewHolderFactory {
    @NonNull
    @Override
    public NotificationViewHolder create(@NonNull ViewGroup parent) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_talk_notification_item, parent, false);
        return new NotificationViewHolder(v);
    }

    public static class NotificationViewHolder extends AbsBaseViewHolder<NotificationMessage> {
        private EmojiTextView mTvContent;

        private NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvContent = itemView.findViewById(R.id.talk_iv_notification_content);
            mTvContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatRoomMessage chatRoomMessage = (ChatRoomMessage) v.getTag();
                    CoreUtils.send(new RoomTalkEvent.OnRoomMsgItemClicked(chatRoomMessage));
                }
            });
        }

        @Override
        public void bind(NotificationMessage message) {
            super.bind(message);
            ChatRoomMessage chatRoomMessage = message.getChatRoomMessage();
            mTvContent.setTag(chatRoomMessage);
            setMsgNotification(chatRoomMessage);
        }

        /**
         * 显示通知消息（房间进来消息、座驾消息等）
         */
        private void setMsgNotification(ChatRoomMessage chatRoomMessage) {
            ChatRoomNotificationAttachment attachment = (ChatRoomNotificationAttachment) chatRoomMessage.getAttachment();
            String carName = "";
            HerUserInfo herUserInfo = null;
            long uid = 0;
            try {
                Map<String, Object> roomExtension = chatRoomMessage.getChatRoomMessageExtension().getSenderExtension();
                if (roomExtension != null) {
                    carName = (String) roomExtension.get(AvRoomModel.USER_CAR_NAME);
                    String herUserInfoStr = (String) roomExtension.get(AvRoomModel.HER_USER_INFO);
                    if (!TextUtils.isEmpty(herUserInfoStr)) {
                        herUserInfo = JsonParser.parseJsonObject(herUserInfoStr, HerUserInfo.class);
                    }
                } else if (attachment.getExtension() != null) {//小程序存在这里面
                    carName = (String) attachment.getExtension().get(AvRoomModel.USER_CAR_NAME);
                }
                List<String> ids = attachment.getTargets();
                if (ids != null && ids.size() > 0) {
                    uid = Long.valueOf(ids.get(0));
                }
            } catch (Exception e) {
                CoreUtils.crashIfDebug(e, "setMsgNotification");
                e.printStackTrace();
            }
            String senderNick = "";
            List<String> nicks = attachment.getTargetNicks();
            if (nicks != null && nicks.size() > 0) {
                senderNick = nicks.get(0);
            }
            if (attachment.getType() != NotificationType.ChatRoomMemberIn) {
                return;
            }
            if (herUserInfo != null) {
                String content = "";
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                builder.append(senderNick);
                builder.setSpan(new ClickableSpanImpl(uid), builder.toString().length() - senderNick.length(), builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" 追寻").append(" ").append(herUserInfo.getNickname());
                builder.setSpan(new ClickableSpanImpl(herUserInfo.getUid()), builder.toString().length() - herUserInfo.getNickname().length(), builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" 的身影来了");
                mTvContent.setOnClickListener(null);
                mTvContent.setText(builder);
                mTvContent.setMovementMethod(LinkMovementMethod.getInstance());
            } else if (!TextUtils.isEmpty(carName)) {
                String content = "";
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                builder.append(senderNick).append(" 驾着").append("“");

                List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                HashMap<String, Object> map;
                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, carName);
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xffffd800);
                list.add(map);
                SpannableStringBuilder builder1 = RichTextUtil.getSpannableStringFromList(list);
                builder.append(builder1);

                builder.append("”").append("来了");
                mTvContent.setText(builder);
            } else {
                String content = "";
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                builder.append(senderNick).append(" ").append("来了");
                mTvContent.setText(builder);
            }
        }

        private class ClickableSpanImpl extends ClickableSpan {
            private long uid;

            public ClickableSpanImpl(long uid) {
                this.uid = uid;
            }

            @Override
            public void onClick(View widget) {
                UserInfoDialogManager.showDialogFragment(mTvContent.getContext(), uid);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }
    }
}

