package com.tongdaxing.erban.common.room.talk;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.erban.ui.mvp.MVPBaseFrameLayout;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.room.talk.adapter.RoomTalkAdapter;
import com.tongdaxing.erban.common.room.talk.adapter.SpacesItemDecoration;
import com.tongdaxing.erban.common.room.talk.manager.TalkManager;
import com.tongdaxing.erban.common.room.talk.manager.WrapContentLinearLayoutManager;
import com.tongdaxing.erban.common.ui.widget.marqueeview.Utils;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Chen on 2019/4/18.
 */
public class RoomTalkView extends MVPBaseFrameLayout<IRoomTalkView, TalkPresenter> implements IRoomTalkView {
    @BindView(R2.id.room_talk_recycler)
    RecyclerView mRoomTalkRecycler;

    private WrapContentLinearLayoutManager mLayoutManager;
    private RoomTalkAdapter mRoomTalkAdapter;
    private int mLastVisibleItemPosition = 0;
    private TextView mTvBottomTip;

    public RoomTalkView(@NonNull Context context) {
        super(context);
    }

    public RoomTalkView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RoomTalkView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_talk_view;
    }

    @NonNull
    @Override
    protected TalkPresenter createPresenter() {
        return new TalkPresenter();
    }

    @Override
    protected void findView() {
        ButterKnife.bind(this);
    }

    @Override
    protected void setView() {
        setRecyclerView();

        setBottomTip();
    }

    private void setRecyclerView() {
        mRoomTalkRecycler.getItemAnimator().setChangeDuration(0);
        mRoomTalkRecycler.addItemDecoration(new SpacesItemDecoration(0, 10));

        mRoomTalkRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (mRoomTalkAdapter == null) {
                    return;
                }
                mLastVisibleItemPosition = getLastVisibleItem();
                if (mLastVisibleItemPosition + 1 >= mRoomTalkAdapter.getItemCount()) {
                    mTvBottomTip.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setBottomTip() {
        // 底部有新消息
        mTvBottomTip = new TextView(getContext());
        LayoutParams params1 = new LayoutParams(
                Utils.dip2px(getContext(), 100F), Utils.dip2px(getContext(), 30));
        params1.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        params1.bottomMargin = Utils.dip2px(getContext(), 0);
        mTvBottomTip.setBackgroundResource(R.drawable.bg_messge_view_bottom_tip);
        mTvBottomTip.setGravity(Gravity.CENTER);
        mTvBottomTip.setText(getContext().getString(R.string.message_view_bottom_tip));
        mTvBottomTip.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.white));
        mTvBottomTip.setLayoutParams(params1);
        mTvBottomTip.setVisibility(GONE);
        mTvBottomTip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRoomTalkAdapter != null) {
                    mTvBottomTip.setVisibility(GONE);
                    mRoomTalkRecycler.scrollToPosition(mRoomTalkAdapter.getItemCount() - 1);
                }
            }
        });
        addView(mTvBottomTip);
    }

    @Override
    protected void setListener() {
        mRoomTalkAdapter = new RoomTalkAdapter();
        mRoomTalkRecycler.setAdapter(mRoomTalkAdapter);  //设置聊天内容的适配器
        mRoomTalkRecycler.getItemAnimator().setChangeDuration(0);

        mLayoutManager = new WrapContentLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        mRoomTalkRecycler.setLayoutManager(mLayoutManager);
        ((SimpleItemAnimator) mRoomTalkRecycler.getItemAnimator()).setSupportsChangeAnimations(false);
        mRoomTalkRecycler.getItemAnimator().setChangeDuration(0);
    }

    @Override
    public void showTalkMessage(IRoomTalkMessage message) {
        if (mRoomTalkAdapter == null) {
            return;
        }
        int len = mRoomTalkAdapter.getItemCount();
        if (len > TalkManager.TALK_MAX_MESSAGE_COUNT) {
            for (int i = 0; i < 20; i++) {
                mRoomTalkAdapter.remove(0);
            }
        }

//        if (ifSelf()) {
//            mRoomTalkRecycler.smoothScrollToPosition(mRoomTalkAdapter.getItemCount());
//            mTvBottomTip.setVisibility(View.GONE);
//        } else {
            if (mLastVisibleItemPosition + 5 >= len) {
                mRoomTalkRecycler.smoothScrollToPosition(len);
                mTvBottomTip.setVisibility(View.GONE);
            } else {
                mTvBottomTip.setVisibility(View.VISIBLE);
            }
//        }
        mRoomTalkAdapter.add(message);
    }

    private boolean ifSelf() {
        return false;
    }

    @Override
    public void showAllTalkMessage(List<IRoomTalkMessage> messageList) {
        if (mRoomTalkAdapter == null || messageList == null) {
            return;
        }
        L.debug(TAG, "showAllTalkMessage messageList.size: %d", messageList.size());
        mRoomTalkAdapter.addAll(messageList);
        RecyclerView.LayoutManager layoutManager = mRoomTalkRecycler.getLayoutManager();
        int count = mRoomTalkAdapter.getItemCount() - 1;
        if (count > 0) {
            layoutManager.scrollToPosition(count);
        }
    }

    @Override
    public void showSendFailMessage(int errorCode, String errorMessage) {

    }

    @Override
    public void showLimitTip() {

    }

    @Override
    public void updateTalkView() {
        if (mRoomTalkAdapter != null) {
            mRoomTalkAdapter.notifyDataSetChanged();
        }
    }

    private int getLastVisibleItem() {
        if (mLayoutManager == null) {
            return 0;
        }
        mLastVisibleItemPosition = mLayoutManager.findLastVisibleItemPosition();
        return mLastVisibleItemPosition;
    }
}
