package com.tongdaxing.erban.common.room.avroom.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.room.avroom.adapter.OnlineUserAdapter;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.orhanobut.logger.Logger;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.HomePartyUserListPresenter;
import com.tongdaxing.xchat_core.room.view.IHomePartyUserListView;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>  在线用户列表 </p>
 *
 * @author Administrator
 * @date 2017/12/4
 */
@CreatePresenter(HomePartyUserListPresenter.class)
public class OnlineUserFragment extends BaseMvpFragment<IHomePartyUserListView, HomePartyUserListPresenter>
        implements BaseQuickAdapter.OnItemClickListener, IHomePartyUserListView, OnlineUserAdapter.OnRoomOnlineNumberChangeListener {
    private RecyclerView mRecyclerView;
    private SmartRefreshLayout mRefreshLayout;

    private OnlineUserAdapter mOnlineUserAdapter;
    private int mPage = Constants.PAGE_START;
    private OnLineUserCallback onLineUserCallback;

    @Override
    public void onDestroy() {
        if (mOnlineUserAdapter != null)
            mOnlineUserAdapter.release();
        super.onDestroy();
    }

    @Override
    public void onFindViews() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mRefreshLayout = mView.findViewById(R.id.refresh_layout);
    }

    @Override
    public void onSetListener() {
        mRefreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(mContext)) {
                    mRefreshLayout.finishLoadmore();
                    return;
                }
                List<OnlineChatMember> data = mOnlineUserAdapter.getData();
                if (ListUtils.isListEmpty(data)) {
                    mRefreshLayout.finishLoadmore();
                    return;
                }
                loadData(data.get(data.size() - 1).chatRoomMember.getEnterTime());
            }

            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(mContext)) {
                    mRefreshLayout.finishRefresh();
                    return;
                }
                firstLoad();
            }
        });
    }

//    private List<OnlineChatMember> fitUser(List<OnlineChatMember> user) {
//        if (!isMic) {
//            return user;
//        }
//        if (ListUtils.isListEmpty(user)) {
//            return user;
//        }
//        List<OnlineChatMember> data = new ArrayList<>();
//        for (int i = 0; i < user.size(); i++) {
//            if (user.get(i).isOnMic) {
//                data.add(user.get(i));
//            }
//        }
//        return data;
//    }

    @Override
    public void initiate() {
        mRefreshLayout.setEnableHeaderTranslationContent(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mOnlineUserAdapter = new OnlineUserAdapter(true);
        mRecyclerView.setAdapter(mOnlineUserAdapter);
        mOnlineUserAdapter.setOnItemClickListener(this);
        mOnlineUserAdapter.setListener(this);
    }


    @Override
    public int getRootLayoutId() {
        return R.layout.common_refresh_recycler_view;
    }


    public void firstLoad() {
        mPage = Constants.PAGE_START;
        loadData(0);
    }

    private void loadData(long time) {
        getMvpPresenter().requestChatMemberByPage(mPage, time,
                mOnlineUserAdapter == null ? null : mOnlineUserAdapter.getData());
    }


    @Override
    public void onRequestChatMemberByPageSuccess(List<OnlineChatMember> chatRoomMemberList, int page) {
        mPage = page;
        if (!ListUtils.isListEmpty(chatRoomMemberList)) {
            mOnlineUserAdapter.setNewData(chatRoomMemberList);
            if (mPage == Constants.PAGE_START) {
                mRefreshLayout.finishRefresh();
            } else {
                mRefreshLayout.finishLoadmore(0);
            }
            mPage++;
        } else {
            if (mPage == Constants.PAGE_START) {
                mRefreshLayout.finishRefresh();
            } else {
                mRefreshLayout.finishLoadmore(0);
            }
        }
    }

    @Override
    public void onRequestChatMemberByPageFail(String errorStr, int page) {
        Logger.i("获取到数据失败,page=" + page);
        if ("416".equals(errorStr)) {
            toast(R.string.operating_too_often);
        }
        mPage = page;
        if (mPage == Constants.PAGE_START) {
            mRefreshLayout.finishRefresh();
        } else {
            mRefreshLayout.finishLoadmore(0);
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int position) {
        if (onLineUserCallback != null)
            onLineUserCallback.onDismiss();
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom != null) {
            List<OnlineChatMember> chatRoomMembers = mOnlineUserAdapter.getData();
            if (ListUtils.isListEmpty(chatRoomMembers)) return;
            //Index: 4, Size: 4
            //com.tongdaxing.erban.common.room.avroom.b.m.onItemClick(OnlineUserFragment.java:177)
            if (chatRoomMembers.size() <= position)
                return;
            ChatRoomMember chatRoomMember = chatRoomMembers.get(position).chatRoomMember;
            if (TextUtils.isEmpty(chatRoomMember.getAccount())) return;
            final List<ButtonItem> buttonItems = new ArrayList<>();
            List<ButtonItem> items = ButtonItemFactory.createAllRoomPublicScreenButtonItems(mContext, chatRoomMember.getAccount());
            if (items == null) return;
            buttonItems.addAll(items);
            ((BaseMvpActivity) mContext).getDialogManager().showCommonPopupDialog(buttonItems, "取消");
        }
    }

    @Override
    public void onMemberIn(String account, List<OnlineChatMember> dataList) {
        getMvpPresenter().onMemberInRefreshData(account, dataList, mPage);
    }

    @Override
    public void onMemberDownUpMic(String account, boolean isUpMic, List<OnlineChatMember> dataList) {
        getMvpPresenter().onMemberDownUpMic(account, isUpMic, dataList, mPage);
    }

    @Override
    public void onUpdateMemberManager(String account, boolean isRemoveManager, List<OnlineChatMember> dataList) {
        getMvpPresenter().onUpdateMemberManager(account, dataList, isRemoveManager, mPage);
    }

    @Override
    public void addMemberBlack() {
    }

    public void setOnLineUserCallback(OnLineUserCallback onLineUserCallback) {
        this.onLineUserCallback = onLineUserCallback;
    }

    public interface OnLineUserCallback {
        void onDismiss();
    }
}
