package com.tongdaxing.erban.common.ui.home.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 首页-引导新手期用户进入推荐房间的弹窗
 * <p>
 * Created by zhangjian on 2019/4/23.
 */
public class NewUserRoomGuideDialogFragment extends BaseDialogFragment {
    @BindView(R2.id.iv_user_head)
    CircleImageView ivUserHead;
    @BindView(R2.id.view_close)
    View viewClose;
    @BindView(R2.id.tv_user_name)
    TextView tvUserName;
    @BindView(R2.id.tv_open_room)
    TextView tvOpenRoom;
    Unbinder unbinder;

    public static NewUserRoomGuideDialogFragment newInstance(String avatarUrl, String nick, long roomUid) {
        Bundle bundle = new Bundle();
        bundle.putString("avatarUrl", avatarUrl);
        bundle.putString("nick", nick);
        bundle.putLong("roomUid", roomUid);
        NewUserRoomGuideDialogFragment fragment = new NewUserRoomGuideDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View view = inflater.inflate(R.layout.layout_new_user_room_guide_dialog,
                ((ViewGroup) window.findViewById(android.R.id.content)), false);//需要用android.R.id.content这个view
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            String avatarUrl = getArguments().getString("avatarUrl");
            ImageLoadUtils.loadCircleImage(getContext(), avatarUrl, ivUserHead, R.drawable.default_cover);
            String nick = getArguments().getString("nick");
            tvUserName.setText(nick);
            long roomUid = getArguments().getLong("roomUid");
            viewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            tvOpenRoom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UmengEventUtil.getInstance().onEvent(getContext(), UmengEventId.getHomeNewUserRecommendRoom());
                    AVRoomActivity.start(getContext(), roomUid);
                    dismiss();
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
