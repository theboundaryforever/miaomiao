package com.tongdaxing.erban.common.ui.home.adpater;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.blur.BlurTransformation;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.home.HomeRoom;

import java.util.List;

/**
 * <p> 首页非热门adapter </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class HotRecommendAdapter extends BaseQuickAdapter<HomeRoom, BaseViewHolder> {


    private Context context;

    public HotRecommendAdapter(Context context, @Nullable List<HomeRoom> data) {

        super(R.layout.hot_list_recommend_item, data);
        this.context = context;

    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, HomeRoom homeRoom) {
        if (homeRoom == null) {
            return;
        }
        baseViewHolder.setText(R.id.tv_recommend_title, homeRoom.title);
        baseViewHolder.setText(R.id.tv_recommend_online_count, homeRoom.onlineNum + "人");
        ImageView ivBg = baseViewHolder.getView(R.id.iv_recommend_bg);
        ImageView ivIcon = baseViewHolder.getView(R.id.iv_recommend_icon);
        GlideApp.with(context)
                .load(homeRoom.getAvatar())
                .transform(new BlurTransformation(context, 23, 4))
                .into(ivBg);
        ImageLoadUtils.loadImage(context, homeRoom.getAvatar(), ivIcon);


    }
}
