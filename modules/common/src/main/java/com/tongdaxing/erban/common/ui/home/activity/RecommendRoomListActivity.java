package com.tongdaxing.erban.common.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.ui.home.adpater.RecommendListAdpater;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RecommendRoomListActivity extends BaseActivity implements OnRefreshLoadmoreListener {

    protected RecyclerView mRecyclerView;
    protected SmartRefreshLayout mRefreshLayout;
    protected RecommendListAdpater mAdapter;
    protected List<RoomInfo> datas;
    private int mPage = Constants.PAGE_START;


    public static void start(Context context) {
        Intent intent = new Intent(context, RecommendRoomListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend_room_list);
        initTitleBar("更多推荐");
        initView();
        initListener();
        showLoading();
        initData();
    }

    private void initView() {
        mRefreshLayout = findView(R.id.refresh_layout);
        mRecyclerView = findView(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        int offset = DisplayUtils.dip2px(this, 15);
        int bottom = offset * 2 / 3;
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                if (parent.getChildAdapterPosition(view) == 0) {
                    outRect.top = bottom;
                }
                outRect.bottom = bottom;
                outRect.left = offset;
                outRect.right = offset;
            }
        });
        datas = new ArrayList<>();
        mAdapter = new RecommendListAdpater(this);
        mRecyclerView.setAdapter(mAdapter);
    }


    private void initListener() {
        mRefreshLayout.setOnRefreshLoadmoreListener(this);
    }

    private void initData() {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        requestParam.put("pageNum", mPage + "");
        requestParam.put("pageSize", Constants.PAGE_HOME_HOT_SIZE + "");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getHomePopularRoom(), requestParam, new OkHttpManager.MyCallBack<ServiceResult<List<HomeRoom>>>() {
            @Override
            public void onError(Exception e) {
                if (mPage == Constants.PAGE_START) {
                    mRefreshLayout.finishRefresh(0);
                    showNetworkErr();
                } else {
                    mPage--;
                    mRefreshLayout.finishLoadmore(0);
                    hideStatus();
                }
            }

            @Override
            public void onResponse(ServiceResult<List<HomeRoom>> response) {
                if (mPage == Constants.PAGE_START) {
                    mRefreshLayout.finishRefresh();
                    if (response == null || ListUtils.isListEmpty(response.getData())) {
                        showNoData();
                    } else {
                        hideStatus();
                        mAdapter.setNewData(filterOther(response.getData()));
                    }
                } else {
                    mRefreshLayout.finishLoadmore();
                    if (response != null && !ListUtils.isListEmpty(response.getData())) {
                        mAdapter.addData(response.getData());
                    }
                }
            }
        });
    }

    private List<HomeRoom> filterOther(List<HomeRoom> data) {
        List<HomeRoom> recommend = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            if (i >= 4) {
                recommend.add(data.get(i));
            }
        }
        return recommend;
    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        mPage++;
        initData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        mPage = Constants.PAGE_START;
        initData();
    }
}
