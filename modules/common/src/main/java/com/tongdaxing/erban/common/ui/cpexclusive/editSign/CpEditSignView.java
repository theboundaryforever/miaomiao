package com.tongdaxing.erban.common.ui.cpexclusive.editSign;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

import com.erban.ui.mvp.MVPBaseLinearLayout;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.libcommon.widget.CpSignImageSpan;
import com.tongdaxing.xchat_core.cp.CpEditSignEvent;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by zhangjian on 2019/4/30.
 */
public class CpEditSignView extends MVPBaseLinearLayout<ICpEditSignView, CpSignPresenter> implements ICpEditSignView {
    public static final String TAG = "CpEditSignView";
    @BindView(R2.id.room_cp_et_sign)
    EditText roomCpEtSign;
    @BindView(R2.id.room_cp_tv_cp_des_content)
    TextView roomCpTvCpDesContent;
    private String sign;
    private int cpLevelSignBg;

    public CpEditSignView(@NonNull Context context) {
        super(context);
    }

    public CpEditSignView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CpEditSignView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @NonNull
    @Override
    protected CpSignPresenter createPresenter() {
        return new CpSignPresenter();
    }

    @Override
    protected void findView() {
        ButterKnife.bind(this);
    }

    @Override
    protected void setView() {
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo != null) {
            sign = cacheLoginUserInfo.getSign();
            int cpGiftLevel = 1;//结成一周cp后，有时候cacheLoginUserInfo还未及时更新，getCpUser()为null，所以这里有个默认值
            if (cacheLoginUserInfo.getCpUser() != null) {
                cpGiftLevel = cacheLoginUserInfo.getCpUser().getCpGiftLevel();
            }
            cpLevelSignBg = getCpLevelSignBg(cpGiftLevel);
            setCpSign();
        }
    }

    private int getCpLevelSignBg(int cpLevel) {
        switch (cpLevel) {
            case 1:
                return R.drawable.cp_chenghaopai_one_bigger;
            case 2:
                return R.drawable.cp_chenghaopai_two_bigger;
            case 3:
                return R.drawable.cp_chenghaopai_three_bigger;
            default:
                return R.drawable.cp_chenghaopai_one_bigger;
        }
    }

    @Override
    protected void setListener() {
        roomCpEtSign.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                sign = s.toString();
                if (s.length() > 3) {
                    SingleToastUtil.showToast("称号牌名字只能在三个字以内哦");
                    return;
                }
                setCpSign();
            }
        });
    }

    private void setCpSign() {
        if (TextUtils.isEmpty(sign)) {
            sign = "";
        }
        SpannableStringBuilder builder = new SpannableStringBuilder("");
        builder.append(BaseConstant.CP_SIGN_PLACEHOLDER);
        CpSignImageSpan urlImageSpan = new CpSignImageSpan(getContext(), cpLevelSignBg, sign, 13, 27, 0);
        builder.setSpan(urlImageSpan, builder.toString().length() - BaseConstant.CP_SIGN_PLACEHOLDER.length(),
                builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        roomCpTvCpDesContent.setText(builder);
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_view_cp_edit_sign;
    }

    @OnClick(R2.id.room_cp_iv_ok)
    public void onRoomCpIvOkClicked() {
        if (sign.length() > 3) {
            SingleToastUtil.showToast("称号牌名字只能在三个字以内哦");
            return;
        }
        //修改称号牌
        mPresenter.setCpSign(sign);
    }

    @OnClick(R2.id.iv_close)
    public void onIvCloseClicked() {
        L.debug(TAG, "onIvCloseClicked send OnEditClose");
        CoreUtils.send(new CpEditSignEvent.OnEditClose());
    }

    @Override
    public void setCpSignSucceed() {
        SingleToastUtil.showToast("设置成功，已佩戴");
        onIvCloseClicked();
    }

    @Override
    public void setCpSignFailure(String message) {
        SingleToastUtil.showToast(message);
        roomCpEtSign.setText(null);
    }

    @Override
    public void editFailureSensitiveWord() {
        SingleToastUtil.showToast("内容含有敏感词，修改失败!");
        roomCpEtSign.setText(null);
    }
}
