package com.tongdaxing.erban.common.ui.find.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.presenter.find.PublicVoiceGroupPresenter;
import com.tongdaxing.erban.common.ui.common.permission.EasyPermissions;
import com.tongdaxing.erban.common.ui.find.view.IPublishVoiceGroupView;
import com.tongdaxing.erban.common.utils.NewAndOldUserEventUtils;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.find.IVoiceGroupClient;
import com.tongdaxing.xchat_core.find.bean.PublishVoiceGroupInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_core.utils.file_manager.FileBatchUploadManager;
import com.tongdaxing.xchat_core.utils.file_manager.FileManagerCallback;
import com.tongdaxing.xchat_core.utils.file_manager.UploadFailureInfo;
import com.tongdaxing.xchat_core.utils.file_manager.UploadSucceedInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerActivity;
import cn.bingoogolapple.photopicker.activity.BGAPhotoPickerPreviewActivity;
import cn.bingoogolapple.photopicker.util.BGAPhotoHelper;
import cn.bingoogolapple.photopicker.widget.BGASortableNinePhotoLayout;

/**
 * 发布动态
 */
@CreatePresenter(PublicVoiceGroupPresenter.class)
public class PublishVoiceGroupActivity extends BaseMvpActivity<IPublishVoiceGroupView, PublicVoiceGroupPresenter>
        implements IPublishVoiceGroupView, BGASortableNinePhotoLayout.Delegate, FileManagerCallback {
    private static final int RC_CHOOSE_PHOTO = 1;
    private static final int RC_PHOTO_PREVIEW = 2;
    /**
     * 拍照的请求码
     */
    private static final int REQUEST_CODE_TAKE_PHOTO = 3;
    /**
     * 预览照片的请求码
     */
    private static final int RC_PREVIEW = 4;
    private static final int REQUEST_CODE_PERMISSION_TAKE_PHOTO = 10;
    private List<UploadSucceedInfo> storageUrlList = new CopyOnWriteArrayList<>();
    private AtomicInteger atomicInteger = new AtomicInteger(0);
    private BGASortableNinePhotoLayout bnp;
    private EditText etContent;
    private boolean isPublishing = false;//状态正在发布中
    private BGAPhotoHelper mPhotoHelper;
    private ArrayList<UserPhoto> userPhotoArrayList;
    private TextView mTextCount;

    private final int MAX_TEXT_LENGTH = 280;

    public static void start(Fragment fragment) {
        start(fragment.getContext());
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, PublishVoiceGroupActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, String text) {
        Intent intent = new Intent(context, PublishVoiceGroupActivity.class);
        intent.putExtra("text", text);
        context.startActivity(intent);
    }

    private void initTitleBarNew() {
        AppToolBar appToolBar = (AppToolBar) findViewById(R.id.app_tool_bar);
        appToolBar.setOnLeftImgBtnClickListener(this::finish);
        appToolBar.getTvTitle().setText("动态发布");
        TextPaint tp = appToolBar.getTvTitle().getPaint();
        tp.setFakeBoldText(true);
        appToolBar.setOnRightImgBtnClickListener(() -> {
            if (isPublishing) {
                toast("正在发布中，请稍后再试");
                return;
            }
            isPublishing = true;
            String content = etContent.getText().toString();
            if (TextUtils.isEmpty(content)) {
                SingleToastUtil.showToast("内容不能为空!");
                return;
            }
            getDialogManager().setCanceledOnClickOutside(false);
            getDialogManager().showProgressDialog(this, getString(R.string.waiting_text), true);
            getMvpPresenter().publicVoiceGroupContent(content, getMvpPresenter().getJointUrlStr(storageUrlList), getMvpPresenter().getImgLayoutType(storageUrlList));
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FileBatchUploadManager.newInstance().unregisterFileManagerCallback(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public_voice_group);

        // 拍照后照片的存放目录，改成你自己拍照后要存放照片的目录。如果不传递该参数的话就没有拍照功能
        File takePhotoDir = new File(Environment.getExternalStorageDirectory(), "miaomiao_camera_photo");
        mPhotoHelper = new BGAPhotoHelper(takePhotoDir);

        etContent = (EditText) findViewById(R.id.et_content);
        etContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null) {
                    String content = s.toString();
                    if (TextUtils.isEmpty(content)) {
                        mTextCount.setText("0/" + MAX_TEXT_LENGTH);
                    } else {
                        int length = content.length();
                        if (length >= MAX_TEXT_LENGTH) {
                            length = MAX_TEXT_LENGTH;
                        }
                        mTextCount.setText(length + "/" + MAX_TEXT_LENGTH);
                    }
                }
            }
        });
        mTextCount = (TextView) findViewById(R.id.hall_tv_voice_text_count);
        bnp = (BGASortableNinePhotoLayout) findViewById(R.id.snpl_moment_add_photos);
        bnp.setDeleteDrawableResId(R.drawable.common_photo_delete);
        bnp.setPlusDrawableResId(R.drawable.common_photo_add);
        bnp.setDelegate(this);
        bnp.setMaxItemCount(9);//最多选9张图片
        //设置文件上传回调
        FileBatchUploadManager.newInstance().registerFileManagerCallback(this, this);
        initTitleBarNew();

        String text = getIntent().getStringExtra("text");
        if (!TextUtils.isEmpty(text)) {
            etContent.setText(text);
        }
    }

    public void takePhoto() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            try {
                startActivityForResult(mPhotoHelper.getTakePhotoIntent(), REQUEST_CODE_TAKE_PHOTO);
            } catch (Exception e) {
                SingleToastUtil.showToast(getResources().getString(R.string.bga_pp_not_support_take_photo));
            }
        } else {
            EasyPermissions.requestPermissions(this, getResources().getString(R.string.tip_permission_camera_storage), REQUEST_CODE_PERMISSION_TAKE_PHOTO, perms);
        }
    }

    private void uploadPicture() {
        ButtonItem upItem = new ButtonItem("拍照上传", this::takePhoto);
        ButtonItem loaclItem = new ButtonItem("本地相册", () -> {
            Intent photoPickerIntent = new BGAPhotoPickerActivity.IntentBuilder(this)
                    .maxChooseCount(bnp.getMaxItemCount() - bnp.getItemCount())//当前可选最多图片
                    .selectedPhotos(null) // 当前已选中的图片路径集合
                    .pauseOnScroll(false) // 滚动列表时是否暂停加载图片
                    .build();
            startActivityForResult(photoPickerIntent, RC_CHOOSE_PHOTO);
        });
        List<ButtonItem> buttonItemList = new ArrayList<>();
        buttonItemList.add(upItem);
        buttonItemList.add(loaclItem);
        getDialogManager().showCommonPopupDialog(buttonItemList, "取消", false);
    }

    @Override
    public void onClickAddNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, ArrayList<UserPhoto> models) {
        uploadPicture();
    }

    @Override
    public void onClickDeleteNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, UserPhoto model, ArrayList<UserPhoto> models) {
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                bnp.removeItem(position);
            }
        });
        if (storageUrlList != null && model != null) {
            for (int i = 0; i < storageUrlList.size(); i++) {
                if (storageUrlList.get(i).localUrl.equals(model.getPhotoUrl())) {
                    storageUrlList.remove(i);
                    return;
                }
            }
        }
    }

    @Override
    public void onClickNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, UserPhoto model, ArrayList<UserPhoto> models) {
        ArrayList<String> temp = getMvpPresenter().convertPhotoListToStrList(models);
        Intent photoPickerPreviewIntent = new BGAPhotoPickerPreviewActivity.IntentBuilder(this)
                .previewPhotos(temp) // 当前预览的图片路径集合
                .selectedPhotos(temp) // 当前已选中的图片路径集合
                .maxChooseCount(bnp.getMaxItemCount()) // 图片选择张数的最大值
                .currentPosition(position) // 当前预览图片的索引
                .isFromTakePhoto(false) // 是否是拍完照后跳转过来
                .build();
        startActivityForResult(photoPickerPreviewIntent, RC_PHOTO_PREVIEW);
    }

    @Override
    public void onNinePhotoItemExchanged(BGASortableNinePhotoLayout sortableNinePhotoLayout, int fromPosition, int toPosition, ArrayList<UserPhoto> models) {

    }

    private void addPhotoItems(Intent data) {
        getDialogManager().setCanceledOnClickOutside(false);
        getDialogManager().showProgressDialog(this, getString(R.string.waiting_text), true);
        ArrayList<String> list = BGAPhotoPickerActivity.getSelectedPhotos(data);
        getMvpPresenter().addUrlList(storageUrlList, list);
        atomicInteger = new AtomicInteger(0);
        userPhotoArrayList = getMvpPresenter().convertStrListToPhotoList(list);
        if (userPhotoArrayList.size() > 0) {
            bnp.addMoreData(userPhotoArrayList);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RC_CHOOSE_PHOTO && data != null) {
            addPhotoItems(data);
        } else if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_TAKE_PHOTO) {
                ArrayList<String> photos = new ArrayList<>(Collections.singletonList(mPhotoHelper.getCameraFilePath()));
                Intent photoPickerPreview = new BGAPhotoPickerPreviewActivity.IntentBuilder(this)
                        .isFromTakePhoto(true)
                        .maxChooseCount(1)
                        .previewPhotos(photos)
                        .selectedPhotos(photos)
                        .currentPosition(0)
                        .build();
                startActivityForResult(photoPickerPreview, RC_PREVIEW);
            } else if (requestCode == RC_PREVIEW && data != null) {
                if (BGAPhotoPickerPreviewActivity.getIsFromTakePhoto(data)) {
                    // 从拍照预览界面返回，刷新图库
                    mPhotoHelper.refreshGallery();
                }
                addPhotoItems(data);
            }
        }
    }

    @Override
    public void repeatSendVoiceGroup(String msgHint) {
        isPublishing = false;
        getDialogManager().dismissDialog();
        toast(msgHint);
    }

    @Override
    public void publishSucceed(PublishVoiceGroupInfo publishVoiceGroupInfo) {
        NewAndOldUserEventUtils.onCommonTypeEvent(PublishVoiceGroupActivity.this, UmengEventId.getMomentPost());
        isPublishing = false;
        getDialogManager().dismissDialog();
        CoreManager.notifyClients(IVoiceGroupClient.class, IVoiceGroupClient.PUBLISH_VOICE_GROUP_REFRESH_LIST);
        finish();
    }

    @Override
    public void publishFailure(String errorStr) {
        isPublishing = false;
        getDialogManager().dismissDialog();
        SingleToastUtil.showToast(errorStr);
    }

    @Override
    public void publishFailureSensitiveWord() {
        isPublishing = false;
        getDialogManager().dismissDialog();
        SingleToastUtil.showToast("内容含有敏感词，发布失败!");
    }

    @Override
    public void publishFailureImgViolations(PublishVoiceGroupInfo publishVoiceGroupInfo) {
        SingleToastUtil.showToast("图片违规，请重新添加");
        isPublishing = false;
        getDialogManager().dismissDialog();
        if (bnp != null && bnp.getData() != null) {//清空所有数据
            bnp.getData().clear();
        }
        List<String> illegalImg = publishVoiceGroupInfo.getIllegalPicList();
        if (illegalImg != null && illegalImg.size() > 0) {//剔除违规图片Url
            for (int i = storageUrlList.size() - 1; i >= 0; i--) {
                for (int j = 0; j < illegalImg.size(); j++) {
                    if (storageUrlList.get(i).remoteUrl.equals(illegalImg.get(j))) {
                        storageUrlList.remove(i);
                    }
                }
            }
        }

        ArrayList<UserPhoto> list = new ArrayList<>();
        for (int i = 0; i < storageUrlList.size(); i++) {//重新添加数据
            UserPhoto userPhoto = new UserPhoto();
            userPhoto.setPhotoUrl(storageUrlList.get(i).localUrl);
            list.add(userPhoto);
        }
        bnp.setData(list, true);
    }

    @Override
    public void success(UploadSucceedInfo uploadSucceedInfo) {
        getMvpPresenter().updateUrlListInfo(storageUrlList, uploadSucceedInfo);
        atomicInteger.incrementAndGet();
        if (userPhotoArrayList != null && atomicInteger.get() >= userPhotoArrayList.size()) {
            getDialogManager().dismissDialog();
        }
    }

    @Override
    public void failure(UploadFailureInfo uploadFailureInfo) {
        SingleToastUtil.showToast("上传图片失败!");//测试小伙伴要求写死
        atomicInteger.incrementAndGet();
        if (bnp != null && bnp.getData() != null) {//清空所有数据
            bnp.getData().clear();
        }
        if (storageUrlList != null) {
            for (int i = storageUrlList.size() - 1; i >= 0; i--) {//剔除添加失败Url
                if (storageUrlList.get(i).localUrl.equals(uploadFailureInfo.localUrl)) {
                    storageUrlList.remove(i);
                }
            }
        }
        ArrayList<UserPhoto> list = new ArrayList<>();
        for (int i = 0; i < storageUrlList.size(); i++) {//重新添加数据
            UserPhoto userPhoto = new UserPhoto();
            userPhoto.setPhotoUrl(storageUrlList.get(i).localUrl);
            list.add(userPhoto);
        }
        bnp.setData(list, true);
        if (userPhotoArrayList != null && atomicInteger.get() >= userPhotoArrayList.size()) {
            getDialogManager().dismissDialog();
        }
    }
}
