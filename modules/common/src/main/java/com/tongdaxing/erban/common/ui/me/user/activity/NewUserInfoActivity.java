package com.tongdaxing.erban.common.ui.me.user.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.custom.ICpNotifyCoreClient;
import com.netease.nim.uikit.glide.GlideApp;
import com.opensource.svgaplayer.SVGAImageView;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.presenter.me.setting.UserInfoPresenter;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.cpexclusive.CpExclusivePageActivity;
import com.tongdaxing.erban.common.ui.find.adapter.ViewPagerAdapter;
import com.tongdaxing.erban.common.ui.find.fragment.MyselfFragment;
import com.tongdaxing.erban.common.ui.home.adpater.CommonImageIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.me.user.fragment.DatumPageFragment;
import com.tongdaxing.erban.common.ui.me.user.fragment.GiftPageFragment;
import com.tongdaxing.erban.common.ui.me.user.view.IUserInfoView;
import com.tongdaxing.erban.common.ui.message.activity.AttentionListActivity;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.common.utils.HttpUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SessionHelper;
import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.common.view.LevelView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.audio.IAudioCardCoreClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.IUserDbCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.TextViewDrawableUtils;
import com.uuzuche.lib_zxing.DisplayUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Function:新用户个人页
 * Author: Edward on 2019/3/26
 */
@CreatePresenter(UserInfoPresenter.class)
public class NewUserInfoActivity extends BaseMvpActivity<IUserInfoView, UserInfoPresenter> implements IUserInfoView {
    private static final String TAG = "NewUserInfoActivity";
    @BindView(R2.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R2.id.head_layout)
    LinearLayout llHeadLayout;
    @BindView(R2.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R2.id.view_pager)
    ViewPager viewPager;
    @BindView(R2.id.tv_user_info_user_name)
    TextView tvUserInfoUserName;
    @BindView(R2.id.iv_gender)
    ImageView ivGender;
    @BindView(R2.id.tv_erban_id)
    TextView tvErbanId;
    @BindView(R2.id.tv_attention_count)
    TextView tvAttentionCount;
    @BindView(R2.id.tv_fans_count)
    TextView tvFansCount;
    @BindView(R2.id.level_view_user_info)
    LevelView levelViewUserInfo;
    @BindView(R2.id.avatar_cover)
    ImageView ivAvatarCover;
    @BindView(R2.id.ll_bottom)
    View llBottom;
    @BindView(R2.id.ll_private_chat_bottom)
    FrameLayout llPrivateChatBottom;
    @BindView(R2.id.ll_attention_bottom)
    FrameLayout llAttentionBottom;
    @BindView(R2.id.iv_attention_bottom_bg)
    ImageView ivAttentionbBottomBg;
    @BindView(R2.id.tv_attention)
    TextView tvAttention;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.iv_defriend)
    ImageView ivDefriend;
    @BindView(R2.id.tv_user_info_edit)
    TextView tvUserInfoEdit;
    @BindView(R2.id.iv_mark)
    ImageView ivMark;
    @BindView(R2.id.fl_menu)
    FrameLayout flMenu;
    @BindView(R2.id.riv_cp_user_icon1)
    CircleImageView rivCpUserIcon1;
    @BindView(R2.id.riv_cp_user_icon2)
    CircleImageView rivCpUserIcon2;
    @BindView(R2.id.fl_have_cp)
    FrameLayout flHaveCp;
    @BindView(R2.id.fl_no_cp)
    FrameLayout flNoCp;
    @BindView(R2.id.iv_cp)
    ImageView ivCp;
    @BindView(R2.id.iv_cp_gift)
    SVGAImageView mIvCpGift;
    @BindView(R2.id.iv_go_back)
    ImageView ivGoBack;
    @BindView(R2.id.tv_user_name)
    TextView tvUserName;
    @BindView(R2.id.user_info_indicator)
    MagicIndicator userInfoIndicator;
    private int cpState = 0;//CP状态 1有cp 0没有
    private long userId = 0;
    private UserInfo userInfo;
    private boolean isSelf;
    /**
     * 是否关注
     */
    private boolean isAttention = false;
    private GiftPageFragment giftPageFragment;
    private DatumPageFragment datumPageFragment;
    private MyselfFragment myselfFragment;
    private boolean isShowRedDot = false;

    public static void start(Context context, long userId) {
        Intent intent = new Intent(context, NewUserInfoActivity.class);
        intent.putExtra("userId", userId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user_info);
        ButterKnife.bind(this);
        setCallback();
        userId = getIntent().getLongExtra("userId", 0);
        if (userId == 0) {
            return;
        }
        isSelf = getMvpPresenter().isMySelf(userId);
        setTab();
        setPageFragment();
        refreshRightMore();
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId, true);
        L.debug(TAG, "userInfo = %s", userInfo);
        if (userInfo == null) {
            return;
        }
        if (userInfo.getCreateTime() <= 0) {
            UIHelper.showAddInfoAct(this);
            return;
        }
        refreshData();
    }

    private void refreshData() {
        refreshUserData();
        refreshPageFragmentData();
    }

    private void refreshPageFragmentData() {
        if (datumPageFragment != null) {
            datumPageFragment.refreshData(userInfo);
        }

        if (giftPageFragment != null) {
            giftPageFragment.refreshData();
        }

        if (myselfFragment != null) {
            myselfFragment.setRefreshData();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == userId) {
            userInfo = info;
            refreshData();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        if (info.getUid() == userId) {
            userInfo = info;
            refreshData();
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(Boolean islike, long uid) {
        isAttention = islike;
        setAttentionStatus(isAttention);
    }

    private void setAttentionStatus(boolean isAttention) {
        if (isAttention) {
            ivAttentionbBottomBg.setImageResource(R.drawable.user_info_bg_message);
            tvAttention.setText("已关注");
            tvAttention.setTextColor(getResources().getColor(R.color.color_999999));
            tvAttention.setCompoundDrawablesRelative(TextViewDrawableUtils.getCompoundDrawables(this, R.drawable.user_info_attention_already), null, null, null);
        } else {
            ivAttentionbBottomBg.setImageResource(R.drawable.user_info_bg_attention);
            tvAttention.setCompoundDrawablesRelative(TextViewDrawableUtils.getCompoundDrawables(this, R.drawable.user_info_activity_attention),
                    null, null, null);
            tvAttention.setText("关注");
            tvAttention.setTextColor(getResources().getColor(R.color.white));
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLikedFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
//        toast("关注成功，相互关注可成为好友哦！");
        getDialogManager().dismissDialog();
        isAttention = true;
        setAttentionStatus(true);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid) {
//        toast("取消关注成功");
        getDialogManager().dismissDialog();
        isAttention = false;
        setAttentionStatus(false);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraiseFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    private void setCallback() {
        ivGoBack.setOnClickListener(v -> finish());
        appBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            if (verticalOffset <= -llHeadLayout.getHeight() / 2) {
                collapsingToolbarLayout.setTitle("");
                if (userInfo != null) {
                    tvUserName.setText(userInfo.getNick());
                }
                ivGoBack.setImageResource(R.drawable.ic_user_info_black_arrow);
                ivDefriend.setImageResource(R.drawable.user_info_ic_more_black);
            } else {
                tvUserName.setText("");
                collapsingToolbarLayout.setTitle("");
                ivGoBack.setImageResource(R.drawable.ic_user_info_white_arrow);
                ivDefriend.setImageResource(R.drawable.user_info_ic_more_white);
            }
        });
        flMenu.setOnClickListener(v -> {
            if (userId <= 0) {
                return;
            }
            if (isSelf) {//自己
                UIHelper.showUserInfoModifyAct(this, userId);
                ivMark.setVisibility(View.GONE);
            } else {//对方
                if (userId == SessionHelper.getSendFocusMsgUid()) {
                    Map<String, String> params = CommonParamUtil.getDefaultParam();
                    params.put("queryUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
                    params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
                    params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
                    OkHttpManager.getInstance().doPostRequest(UriProvider.getFocusMsgSwitch(), params, new OkHttpManager.MyCallBack<Json>() {
                        @Override
                        public void onError(Exception e) {
                            createUserMoreOperate(null);
                        }

                        @Override
                        public void onResponse(Json response) {
                            if (response != null && response.num("code") == 200) {
                                Json json = response.json("data");
                                if (json != null) {
                                    int likedSend = json.num("likedSend");
                                    if (likedSend == 1 || likedSend == 2)
                                        createUserMoreOperate(new ButtonItem(likedSend == 1 ? "关闭消息提醒" : "开启消息提醒", new ButtonItem.OnClickListener() {
                                            @Override
                                            public void onClick() {
                                                HttpUtil.saveFocusMsgState(CoreManager.getCore(IAuthCore.class).getCurrentUid(), likedSend == 1 ? 2 : 1);
                                            }
                                        }));
                                }
                            } else {
                                createUserMoreOperate(null);
                            }
                        }
                    });
                } else {
                    createUserMoreOperate(null);
                }
            }
        });
        llAttentionBottom.setOnClickListener(v -> {
            if (userInfo == null) {//防止空指针
                return;
            }
            if (isDestroyed() || isFinishing()) {
                return;
            }
            if (isAttention) {
                boolean isMyFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(userInfo.getUid() + "");
                String tip;
                if (isMyFriend) {
                    tip = "取消关注将不再是好友关系，确定取消关注？";
                } else {
                    tip = "确定取消关注？";
                }
                getDialogManager().showOkCancelDialog(tip, true, new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {
                        getDialogManager().dismissDialog();
                    }

                    @Override
                    public void onOk() {
                        getDialogManager().dismissDialog();
                        getDialogManager().showProgressDialog(NewUserInfoActivity.this, "请稍后...");
                        CoreManager.getCore(IPraiseCore.class).cancelPraise(userInfo.getUid());
                    }
                });
            } else {
                getDialogManager().showProgressDialog(this, "请稍后...");
                CoreManager.getCore(IPraiseCore.class).praise(userInfo.getUid());
            }
        });

        llPrivateChatBottom.setOnClickListener(v -> {
            if (userInfo == null) {
                return;
            }
            HttpUtil.checkUserIsDisturb(this, userId);
        });
    }

    private void refreshUserData() {
        if (isSelf) {
            llBottom.setVisibility(View.GONE);
        } else {
            llBottom.setVisibility(View.VISIBLE);
            CoreManager.getCore(IPraiseCore.class).isPraised(CoreManager.getCore(IAuthCore.class).getCurrentUid(), userInfo.getUid());
        }
        if (userInfo == null) {
            return;
        }
        ImageLoadUtils.loadImage(this, userInfo.getAvatar(), ivAvatarCover, R.drawable.default_cover);
        tvUserInfoUserName.setText(userInfo.getNick());
        tvErbanId.setText("ID:" + userInfo.getErbanNo());
        if (userInfo.getGender() == 1) {
            ivGender.setImageResource(R.drawable.user_info_boy);
        } else {
            ivGender.setImageResource(R.drawable.user_info_girl);
        }
        tvAttentionCount.setText(String.valueOf(userInfo.getFollowNum()));
        tvFansCount.setText(String.valueOf(userInfo.getFansNum()));
        if (userInfo.getExperLevel() == 0 && userInfo.getCharmLevel() == 0) {
            levelViewUserInfo.setVisibility(View.GONE);
        } else {
            levelViewUserInfo.setExperLevel(userInfo.getExperLevel());
            levelViewUserInfo.setCharmLevel(userInfo.getCharmLevel());
        }
        levelViewUserInfo.seatView.setVisibility(View.GONE);

        //设置cp头像
        cpState = userInfo.getCpUser() == null ? 0 : 1;
        setCpContent();
    }

    /**
     * CP状态 1有cp 0没有
     */
    private void setCpContent() {
        if (isDestroyed() || isFinishing() || userInfo == null) {
            return;
        }
        if (cpState == 1) {
            flHaveCp.setVisibility(View.VISIBLE);
            flNoCp.setVisibility(View.GONE);
            GlideApp.with(this)
                    .load(userInfo.getAvatar())
                    .placeholder(R.drawable.default_cover)
                    .error(R.drawable.default_cover)
                    .into(rivCpUserIcon1);
            if (userInfo.getCpUser() != null) {
                GlideApp.with(this)
                        .load(userInfo.getCpUser().getAvatar())
                        .placeholder(R.drawable.default_cover)
                        .error(R.drawable.default_cover)
                        .into(rivCpUserIcon2);
            }
            LogUtil.d(userInfo.toString());
            if (userInfo.getCpUser() != null && userInfo.getCpUser().getCpGiftLevel() == 0) {
                CoreManager.getCore(IUserDbCore.class).updateCpGiftLevel(userInfo.getUid(), 1);
            }
            if (userInfo.getCpUser() != null && userInfo.getCpUser().getCpGiftLevel() >= 1 && userInfo.getCpUser().getCpGiftLevel() <= 3) {
                mIvCpGift.setVisibility(View.VISIBLE);
                ivCp.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(userInfo.getCpUser().getVggUrl())) {
                    SvgaUtils.cyclePlayWebAnim(this, mIvCpGift, userInfo.getCpUser().getVggUrl());
                }
            }
        } else {
            flHaveCp.setVisibility(View.GONE);
            flNoCp.setVisibility(View.VISIBLE);
            mIvCpGift.setVisibility(View.GONE);
            ivCp.setVisibility(View.VISIBLE);
        }
    }

    @CoreEvent(coreClientClass = ICpNotifyCoreClient.class)
    public void onP2PCpInviteAgree() {
        LogUtil.d("NewUserInfoActivity onP2PCpInviteAgree CP配对成功啦");
        cpState = 1;//CP状态 1有cp 0没有
        setCpContent();
        CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid());//更新本地登录用户信息
    }

    @CoreEvent(coreClientClass = ICpNotifyCoreClient.class)
    public void onP2PCpRemove() {
        LogUtil.d("NewUserInfoActivity onP2PCpRemove 你们的CP关系已解除");
        cpState = 0;//CP状态 1有cp 0没有
        if (userInfo != null) {
            userInfo.setCpUser(null);
        }
        setCpContent();
        CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid());//更新本地登录用户信息
    }

    private void setTab() {
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(1, "资料"));
        tabInfoList.add(new TabInfo(2, "动态"));
        tabInfoList.add(new TabInfo(3, "礼物"));
        CommonImageIndicatorAdapter indicatorAdapter = new CommonImageIndicatorAdapter(this, tabInfoList);
        indicatorAdapter.setSize(17);
        indicatorAdapter.setNormalColorId(R.color.color_626269);
        indicatorAdapter.setSelectColorId(R.color.color_000000);
        indicatorAdapter.setImageIndicatorWidth(DisplayUtil.dip2px(this, 5));
        indicatorAdapter.setImageIndicatorHeight(DisplayUtil.dip2px(this, 1));
        indicatorAdapter.setImageIndicatorMarginTop(DisplayUtil.dip2px(this, 3));
        indicatorAdapter.setDefaultDrawableId(R.drawable.common_theme_btn);
        indicatorAdapter.setOnItemSelectListener(new CommonMagicIndicatorAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                viewPager.setCurrentItem(position);
            }
        });
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdapter(indicatorAdapter);
        userInfoIndicator.setNavigator(commonNavigator);
    }

    private void setPageFragment() {
        datumPageFragment = new DatumPageFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putLong("userId", userId);
        datumPageFragment.setArguments(bundle1);
        myselfFragment = new MyselfFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putLong("userId", userId);
        if (!isSelf) {
            bundle2.putBoolean("isFromUserInfoActivity", true);
        }
        myselfFragment.setArguments(bundle2);
        giftPageFragment = new GiftPageFragment();
        Bundle bundle3 = new Bundle();
        bundle3.putLong("userId", userId);
        giftPageFragment.setArguments(bundle3);
        Fragment[] fragments = {datumPageFragment, myselfFragment, giftPageFragment};
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this, fragments);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(fragments.length);
        ViewPagerHelper.bind(userInfoIndicator, viewPager);
    }

    /**
     * 清除红点事件
     */
    @CoreEvent(coreClientClass = IAudioCardCoreClient.class)
    public void onAudioCardMarkClear() {
        isShowRedDot = false;
        ivMark.setVisibility(View.GONE);
    }

    private void refreshRightMore() {
        if (isSelf) {//自己
            tvUserInfoEdit.setVisibility(View.VISIBLE);
            ivMark.setVisibility(View.VISIBLE);
            ivDefriend.setVisibility(View.INVISIBLE);
            flMenu.setEnabled(false);
            if (isShowRedDot) {
                ivMark.setVisibility(View.VISIBLE);
            } else {
                ivMark.setVisibility(View.GONE);
            }
        } else {//对方
            tvUserInfoEdit.setVisibility(View.GONE);
            ivMark.setVisibility(View.GONE);
            ivDefriend.setVisibility(View.VISIBLE);
            flMenu.setEnabled(true);
        }
    }

    private void showDefriendDialog() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        if (CoreManager.getCore(IIMFriendCore.class).isUserInBlackList(userId + "")) {
            getDialogManager().showOkCancelDialog("是否取消拉黑", true, new DialogManager.AbsOkDialogListener() {
                @Override
                public void onOk() {
                    CoreManager.getCore(IIMFriendCore.class).removeFromBlackList("" + userId);
                }
            });
        } else {
            getDialogManager().showOkCancelDialog("加入黑名单后，将不再收到对方信息", true, new DialogManager.AbsOkDialogListener() {
                @Override
                public void onOk() {
                    CoreManager.getCore(IIMFriendCore.class).addToBlackList("" + userId);
                }
            });
        }
    }

    /**
     * 生成用户功能按钮
     *
     * @param focusItem
     */
    private void createUserMoreOperate(ButtonItem focusItem) {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        List<ButtonItem> buttonItems = new ArrayList<>();
        ButtonItem msgBlackListItem = ButtonItemFactory.createMsgBlackListItem(!CoreManager.getCore(IIMFriendCore.class).isUserInBlackList(userId + "") ? "拉黑" : "取消拉黑", new ButtonItemFactory.OnItemClick() {
            @Override
            public void itemClick() {
                showDefriendDialog();
            }
        });
        buttonItems.add(ButtonItemFactory.createReportItem(this, "举报", 1));
        buttonItems.add(msgBlackListItem);
        if (focusItem != null)
            buttonItems.add(focusItem);
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onSetRedDot(boolean flag) {
        isShowRedDot = flag;
    }

    @OnClick(R2.id.rl_cp)
    public void onLlCpClicked() {
        if (userInfo == null) {
            return;
        }
        if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == userId) {
            if (userInfo.getCpUser() != null) {
                CpExclusivePageActivity.start(this, userId, true);
            } else {
                //打开我的关注页 出现邀请按钮
                AttentionListActivity.start(this, true);
            }
        } else {
            if (userInfo.getCpUser() != null) {
                if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == userInfo.getCpUser().getUid()) {
                    //打开自己和cp的CP专属页 有解除CP功能
                    CpExclusivePageActivity.start(this, userId, true);
                } else {
                    //打开他人的CP专属页 没有解除CP功能
                    CpExclusivePageActivity.start(this, userId);
                }
            } else {
                //弹出私聊 并弹出CP申请戒指栏
                boolean isFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(userInfo.getUid() + "");
                HttpUtil.checkUserIsDisturb(this, isFriend, userId, true);
            }
        }
    }

    @OnClick(R2.id.tv_user_info_edit)
    public void onEditUserInfoClicked() {
        UIHelper.showUserInfoModifyAct(this, userId);
        ivMark.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SvgaUtils.closeSvga(mIvCpGift);
    }
}
