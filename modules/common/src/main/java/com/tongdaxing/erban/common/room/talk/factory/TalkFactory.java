package com.tongdaxing.erban.common.room.talk.factory;

import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.juxiao.library_ui.widget.EmojiTextView;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.view.UrlImageSpan;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.widget.CpSignImageSpan;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.talk.message.TalkMessage;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.uuzuche.lib_zxing.DisplayUtil;


/**
 * Created by Administrator on 5/31 0031.
 */

public class TalkFactory implements AbsBaseViewHolder.ViewHolderFactory {
    @NonNull
    @Override
    public TalkViewHolder create(@NonNull ViewGroup parent) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_talk_text_item, parent, false);
        return new TalkViewHolder(v);
    }

    public static class TalkViewHolder extends AbsBaseViewHolder<TalkMessage> {
        private String TAG = "TalkViewHolder";
        private EmojiTextView mTvName;
        private EmojiTextView mTvContent;
        private ImageView mIvPhoto;
        private View mItemView;

        public TalkViewHolder(@NonNull View itemView) {
            super(itemView);
            mItemView = itemView;
            mTvName = itemView.findViewById(R.id.room_talk_tv_name);
            mTvContent = itemView.findViewById(R.id.room_talk_tv_content);
            mIvPhoto = itemView.findViewById(R.id.room_talk_iv_photo);
            mItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatRoomMessage chatRoomMessage = (ChatRoomMessage)v.getTag();
                    if (chatRoomMessage != null) {
                        String playerId = chatRoomMessage.getFromAccount();
                        if (!TextUtils.isEmpty(playerId)) {
                            try {
                                UserInfoDialogManager.showDialogFragment(v.getContext(), Long.valueOf(playerId));
                            }catch (Exception e) {
                                L.error("TalkViewHolder exception:", e);
                            }
                        }
                    }
                }
            });
        }

        @Override
        public void bind(TalkMessage talkMessage) {
            super.bind(talkMessage);
            if (talkMessage == null) {
                return;
            }
            ChatRoomMessage chatRoomMessage = talkMessage.getChatRoomMessage();
            if (chatRoomMessage == null) {
                return;
            }
            mItemView.setTag(chatRoomMessage);
            String senderNick = "";
            int experLevel = -1;
            int experCharm = -1;
            String newUserContent = "";
            String cpSign = null;
            String customLabelUrl = null;
            int cpGiftLevel = 0;
            String photoUrl = "";

            if (chatRoomMessage.getChatRoomMessageExtension() == null) {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (userInfo != null) {
                    senderNick = userInfo.getNick();
                    newUserContent = userInfo.getUserLabel();
                    experLevel = userInfo.getExperLevel();
                    experCharm = userInfo.getCharmLevel();
                    if (experLevel == 0) {
                        experLevel = (int) SpUtils.get(mTvName.getContext(), AvRoomModel.EXPER_LEVEL, 0);
                        L.info(TAG, "setMsgText() userInfo.getExperLevel() == 0, so get it from SP: %d", experLevel);
                    }
                    if (experCharm == 0) {
                        experCharm = (int) SpUtils.get(mTvName.getContext(), AvRoomModel.EXPER_CHARM, 0);
                        L.info(TAG, "setMsgText() userInfo.getCharmLevel() == 0, so get it from SP: %d", experCharm);
                    }

                    cpSign = (userInfo.getCpUser() == null || userInfo.getSign() == null) ? "" : userInfo.getSign();
                    cpGiftLevel = userInfo.getCpUser() == null ? 0 : userInfo.getCpUser().getCpGiftLevel();
                    customLabelUrl = userInfo.getCustomSign();
                    L.info(TAG, "setMsgText() 我: isNewUser = %s, experLevel = %d, cpSign = %s, cpGiftLevel = %d, customLabelUrl = %s",
                            newUserContent, experLevel, cpSign, cpGiftLevel, customLabelUrl);
                    photoUrl = userInfo.getAvatar();
                }
            } else {
                senderNick = chatRoomMessage.getChatRoomMessageExtension().getSenderNick() + " ";
                try {
                    experLevel = (Integer) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.EXPER_LEVEL);
                    experCharm = (Integer) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.EXPER_CHARM);
                    newUserContent = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.IS_NEW_USER);
                    cpSign = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.CP_SIGN);
                    Object cpLevel = chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.CP_LEVEL);
                    if (cpLevel != null) {
                        cpGiftLevel = (int) cpLevel;
                    }
                    customLabelUrl = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(AvRoomModel.CUSTOM_LABEL_URL);
                    L.info(TAG, "setMsgText() %s: isNewUser = %s, experLevel = %d, cpSign = %s, cpGiftLevel = %d, customLabelUrl = %s, experCharm = %d",
                            senderNick, newUserContent, experLevel, cpSign, cpGiftLevel, customLabelUrl, experCharm);
                    photoUrl = chatRoomMessage.getChatRoomMessageExtension().getSenderAvatar();
                } catch (Exception e) {
                    L.info(TAG, "setMsgText() ", e);
                }
            }

            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (roomInfo != null) {
                if (roomInfo.getTagType() == RoomInfo.ROOMTYPE_PERSONAL) {
                    mIvPhoto.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadCircleImage(mIvPhoto.getContext(), photoUrl, mIvPhoto, R.drawable.ic_no_avatar);
                    mIvPhoto.setTag(chatRoomMessage.getFromAccount());
                }
            }

            String content = "";
            SpannableStringBuilder builder = new SpannableStringBuilder(content);
            builder.append(senderNick);
            if (!TextUtils.isEmpty(newUserContent) || experLevel > 0 || experCharm > 0 || !TextUtils.isEmpty(cpSign) || !TextUtils.isEmpty(customLabelUrl)) {
                builder.append("\n");
            }
            setNewLabel(builder, newUserContent, mTvName);
            setUserLevel(builder, experLevel);
            setUserCharm(builder, experCharm);
            setCpSign(builder, cpSign, cpGiftLevel);
            setCustomLabel(builder, customLabelUrl, mTvName);
            mTvName.setText(builder);
            mTvName.setTextColor(mTvName.getContext().getResources().getColor(R.color.room_talk_player_name));
            mTvContent.setVisibility(View.VISIBLE);
            mTvContent.setText(chatRoomMessage.getContent());
        }

        private void setNewLabel(SpannableStringBuilder builder, String userLabelUrl, TextView textView) {
            if (!TextUtils.isEmpty(userLabelUrl) && userLabelUrl.equals("new")) {//兼容旧版标签
                builder.append(BaseConstant.LABEL_PLACEHOLDER);
                int start = builder.toString().length() - BaseConstant.LABEL_PLACEHOLDER.length();
                int end = builder.toString().length();
                UrlImageSpan imageSpan = new UrlImageSpan(textView.getContext(), R.drawable.new_user_msg_icon);
                builder.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            } else if (!TextUtils.isEmpty(userLabelUrl)) {
                builder.append(BaseConstant.LABEL_PLACEHOLDER);
                int start = builder.toString().length() - BaseConstant.LABEL_PLACEHOLDER.length();
                int end = builder.toString().length();
                UrlImageSpan urlImageSpan = new UrlImageSpan(textView.getContext(), userLabelUrl, textView);
                builder.setSpan(urlImageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }

        private void setUserLevel(SpannableStringBuilder builder, int experLevel) {
            if (experLevel > 0) {
                int start = builder.length();
                builder.append(BaseConstant.LEVEL_PLACEHOLDER);
                int end = builder.length();
                int levelRes = BaseApp.mBaseContext.getResources().getIdentifier("lv" + experLevel, "drawable", BaseApp.mBaseContext.getPackageName());
                UrlImageSpan urlImageSpan = new UrlImageSpan(BaseApp.mBaseContext, levelRes);
                builder.setSpan(urlImageSpan, start,
                        end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }

        private void setUserCharm(SpannableStringBuilder builder, int experCharm) {
            if (experCharm > 0) {
                int start = builder.length();
                builder.append(BaseConstant.LEVEL_PLACEHOLDER);
                int end = builder.length();
                int levelRes = BaseApp.mBaseContext.getResources().getIdentifier("charm" + experCharm, "drawable", BaseApp.mBaseContext.getPackageName());
                UrlImageSpan urlImageSpan = new UrlImageSpan(BaseApp.mBaseContext, levelRes);
                builder.setSpan(urlImageSpan, start,
                        end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }


        /**
         * 展示cp尾灯
         *
         * @param builder
         * @param sign
         */
        private void setCpSign(SpannableStringBuilder builder, String sign, int cpGiftLevel) {
            com.juxiao.library_utils.log.LogUtil.d("setCpSign", "sign = " + sign);
            if (!TextUtils.isEmpty(sign)) {
                builder.append(BaseConstant.CP_SIGN_PLACEHOLDER);
                CpSignImageSpan urlImageSpan = new CpSignImageSpan(BaseApp.mBaseContext, getCpLevelSignBg(cpGiftLevel), sign);
//                urlImageSpan.setImgWidth(DisplayUtil.dip2px(mContext,53));
//                urlImageSpan.setImgHeight(DisplayUtil.dip2px(mContext,14));
                builder.setSpan(urlImageSpan, builder.toString().length() - BaseConstant.CP_SIGN_PLACEHOLDER.length(),
                        builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }

        /**
         * 设置自定义尾灯
         *
         * @param builder
         * @param customLabelUrl
         * @param textView
         */
        private void setCustomLabel(SpannableStringBuilder builder, String customLabelUrl, TextView textView) {
            L.info(TAG, "setCustomLabel() customLabelUrl = %s", customLabelUrl);
            if (!TextUtils.isEmpty(customLabelUrl)) {
                builder.append(BaseConstant.CUSTOM_LABEL_PLACEHOLDER);
                int start = builder.toString().length() - BaseConstant.CUSTOM_LABEL_PLACEHOLDER.length();
                int end = builder.toString().length();
                UrlImageSpan urlImageSpan = new UrlImageSpan(BaseApp.mBaseContext, customLabelUrl, textView);
                urlImageSpan.setImgWidth(DisplayUtil.dip2px(BaseApp.mBaseContext, 36));
                urlImageSpan.setImgHeight(DisplayUtil.dip2px(BaseApp.mBaseContext, 15));
                builder.setSpan(urlImageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.append(" ");//添加空格
            }
        }

        private int getCpLevelSignBg(int cpLevel) {
            switch (cpLevel) {
                case 1:
                    return R.drawable.cp_chenghaopai_one;
                case 2:
                    return R.drawable.cp_chenghaopai_two;
                case 3:
                    return R.drawable.cp_chenghaopai_three;
                default:
                    return R.drawable.cp_chenghaopai_one;
            }
        }
    }
}

