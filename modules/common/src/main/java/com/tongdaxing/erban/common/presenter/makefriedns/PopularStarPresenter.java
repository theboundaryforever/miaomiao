package com.tongdaxing.erban.common.presenter.makefriedns;

import com.tongdaxing.erban.common.model.makefriends.PopularStarModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomManager;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.List;

public class PopularStarPresenter extends AbstractMvpPresenter<IPopularStarView> {

    private PopularStarModel dataSource;
    private int currPage = Constants.PAGE_START;

    public PopularStarPresenter() {
        dataSource = new PopularStarModel();
    }

    public void refreshData() {
        getData(Constants.PAGE_START);
    }

    public void loadMoreData() {
        getData(currPage + 1);
    }

    private void getData(final int pageNum) {
        dataSource.getDressUpData(pageNum, Constants.PAGE_SIZE, new OkHttpManager.MyCallBack<ServiceResult<List<UserInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(pageNum == Constants.PAGE_START);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<UserInfo>> response) {
                if (response != null && response.isSuccess()) {
                    currPage = pageNum;
                    if (getMvpView() != null) {
                        getMvpView().setupSuccessView(response.getData(), pageNum == Constants.PAGE_START);
                    }
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    /**
     * 检查用户是否在房间
     */
    public void checkUserInRoom(final long uid) {
        dataSource.getUserRoomInfo(uid, new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
            @Override
            public void onError(Exception e) {
                IPopularStarView view = getMvpView();
                if (view != null) {
                    view.setupFindHerFail(null);
                }
            }

            @Override
            public void onResponse(ServiceResult<RoomInfo> response) {
                IPopularStarView view = getMvpView();
                if (view != null) {
                    long formalRoomId = BasicConfig.isDebug ? PublicChatRoomManager.devRoomId : PublicChatRoomManager.formalRoomId;
                    if (response != null && response.getData() != null &&
                            response.getData().getRoomId() != formalRoomId && response.getData().getRoomId() > 0) {
                        view.toHerRoom(response.getData().getUid());//在房间，进入对应房间（不一定是TA自己的房间）
                    } else {
                        view.toUserInfoPage(uid);//不确定在房间，去个人资料页
                    }
                }
            }
        });
    }
}
