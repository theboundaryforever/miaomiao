package com.tongdaxing.erban.common.ui.audiomatch;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.tongdaxing.erban.common.R;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class LikeUserMaxDialog extends BaseCommonDialog {

    @Override
    void initView(View view) {
        ImageView ivClose = view.findViewById(R.id.iv_close);
        ivClose.setOnClickListener(v -> dismiss());
        Button btnSure = view.findViewById(R.id.btn_sure);
        btnSure.setOnClickListener(v -> dismiss());
    }

    @Override
    int getContentView() {
        return R.layout.dialog_like_user_max;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            Class c = Class.forName("android.support.v4.app.DialogFragment");
            Constructor con = c.getConstructor();
            Object obj = con.newInstance();
            Field dismissed = c.getDeclaredField("mDismissed");
            dismissed.setAccessible(true);
            dismissed.set(obj, false);
            Field shownByMe = c.getDeclaredField("mShownByMe");
            shownByMe.setAccessible(true);
            shownByMe.set(obj, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }
}
