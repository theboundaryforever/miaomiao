package com.tongdaxing.erban.common.ui.message.fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.recent.RecentContactsCallback;
import com.netease.nim.uikit.recent.RecentContactsFragment;
import com.netease.nimlib.sdk.msg.attachment.AudioAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.ui.find.activity.NoticeIMActivity;
import com.tongdaxing.erban.common.ui.find.activity.VoiceGroupIMActivity;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoticeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfoV2;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.util.BaseConstant;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_CP;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_CP_TIP;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_LOTTERY;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_NOTICE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PACKET;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_LOVER_WEEK_CP_OFFICE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SHARE_FANS;

/**
 * 最近消息聊天好友列表
 *
 * @author chenran
 * @date 2017/9/18
 */
public class RecentListFragment extends BaseFragment {
    public static final String TAG = "RecentListFragment";
    private RecentContactsFragment recentContactsFragment;

    @Override
    public void onFindViews() {
        recentContactsFragment = new RecentContactsFragment();
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.recent_container, recentContactsFragment).commitAllowingStateLoss();
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        recentContactsFragment.setCallback(new RecentContactsCallback() {
            @Override
            public void onRecentContactsLoaded() {

            }

            @Override
            public void onUnreadCountChange(int unreadCount) {

            }

            @Override
            public void onItemClick(RecentContact recent) {
                if (recent.getSessionType() == SessionTypeEnum.Team) {
                    NimUIKit.startTeamSession(getActivity(), recent.getContactId());
                } else if (recent.getSessionType() == SessionTypeEnum.P2P) {
                    if (recent.getContactId().equals(BaseConstant.getVoiceGroupAccount())) {
                        VoiceGroupIMActivity.start(getActivity(), recent.getContactId());//只有点赞评论这个特殊账号才会跳转这个页面
                    } else if (recent.getContactId().equals(BaseConstant.getNotificationAccount())) {
                        NoticeIMActivity.start(getActivity(), recent.getContactId());//只有通知消息这个特殊账号才会跳转这个页面
                    } else {
                        NimUIKit.startP2PSession(getActivity(), recent.getContactId());
                    }
                }
            }

            @Override
            public String getDigestOfAttachment(RecentContact recent, MsgAttachment attachment) {
                if (attachment instanceof CustomAttachment) {
                    CustomAttachment customAttachment = (CustomAttachment) attachment;
                    if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                        return "您关注的TA上线啦，快去围观吧~~~";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_GIFT) {
                        return "[礼物]";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_NOTICE) {
                        NoticeAttachment noticeAttachment = (NoticeAttachment) attachment;
                        return noticeAttachment.getTitle();
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_PACKET) {
                        RedPacketAttachment redPacketAttachment = (RedPacketAttachment) attachment;
                        RedPacketInfoV2 redPacketInfo = redPacketAttachment.getRedPacketInfo();
                        if (redPacketInfo == null) {
                            return "您收到一个红包哦!";
                        }
                        return "您收到一个" + redPacketInfo.getPacketName() + "红包哦!";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_LOTTERY) {
                        return "恭喜您，获得抽奖机会";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_SHARE_FANS) {
                        return "[房间邀请]";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_CP || customAttachment.getFirst() == CUSTOM_MSG_CP_TIP) {
                        return "[CP消息]";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_ROOM_LOVER_WEEK_CP_OFFICE) {
                        return "[一周CP消息]";
                    }
                } else if (attachment instanceof AudioAttachment) {
                    return "[语音]";
                }
                return null;
            }

            @Override
            public String getDigestOfTipMsg(RecentContact recent) {
                return null;
            }
        });
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_recent_list;
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        if (recentContactsFragment != null) {
            recentContactsFragment.requestMessages(true);
        }
    }

    /**
     * 忽略未读消息（非全部已读）
     */
    public void ignoreUnreadMsg() {
        if (recentContactsFragment != null) {
            recentContactsFragment.ignoreUnreadMsg();
        }
    }
}
