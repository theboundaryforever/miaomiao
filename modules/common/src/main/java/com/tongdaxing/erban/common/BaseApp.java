package com.tongdaxing.erban.common;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

/**
 * Created by Chen on 2019/7/11.
 */
public class BaseApp {
    public static Context mBaseContext;

    public static RefWatcher mRefWatcher;

    //首页直播tagId
    private static int MIAOMIAO_HOME_LIVE_TAG_ID = 4;
    private static int MIAOBO_HOME_LIVE_TAG_ID = 3;

    //首页热门tagId
    private static int MIAOMIAO_HOME_HOT_TAG_ID = 2;
    private static int MIAOBO_HOME_HOT_TAG_ID = 1;

    public static void init(Context context) {
        mBaseContext = context;
        setupLeakCanary();
    }

    public static RefWatcher getRefWatcher() {
        return mRefWatcher;
    }

    private static void setupLeakCanary() {
        if (!BasicConfig.INSTANCE.isDebuggable()) {
            return;
        }
        if (LeakCanary.isInAnalyzerProcess(mBaseContext)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        mRefWatcher = LeakCanary.install((Application) mBaseContext);
    }

    /**
     * @return 首页直播tagId
     */
    public static int getHomeLiveTagID() {
        return mBaseContext.getApplicationInfo().packageName.contains("miaomiao") ? MIAOMIAO_HOME_LIVE_TAG_ID : MIAOBO_HOME_LIVE_TAG_ID;
    }

    /**
     * @return 首页热门tagId
     */
    public static int getHomeHotTagID() {
        return mBaseContext.getApplicationInfo().packageName.contains("miaomiao") ? MIAOMIAO_HOME_HOT_TAG_ID : MIAOBO_HOME_HOT_TAG_ID;
    }

    /**
     * 关于我们slogan
     *
     * @return
     */
    public static String getAboutUs() {
        return mBaseContext.getApplicationInfo().packageName.contains("miaomiao") ? "喵喵语音 妙不可言" : "你的声音 我的世界";
    }
}
