package com.tongdaxing.erban.common.room;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewStub;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomNotificationAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.orhanobut.logger.Logger;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.room.avroom.fragment.AbsRoomFragment;
import com.tongdaxing.erban.common.room.avroom.fragment.HomeLoverFragment;
import com.tongdaxing.erban.common.room.avroom.fragment.HomePartyFragment;
import com.tongdaxing.erban.common.room.avroom.fragment.InputPwdDialogFragment;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.room.avroom.widget.dialog.BottomSetAudioDialog;
import com.tongdaxing.erban.common.room.chat.RoomPrivateChatDialog;
import com.tongdaxing.erban.common.room.chat.RoomPrivateChatEvent;
import com.tongdaxing.erban.common.room.personal.HomePersonalFragment;
import com.tongdaxing.erban.common.room.personal.profile.RoomProfileAction;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.utils.HttpUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.bean.RoomMemberComeInfo;
import com.tongdaxing.xchat_core.common.ICommonClient;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.FaceAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.im.room.IIMRoomCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.IBgClient;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.presenter.AvRoomPresenter;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.view.IAvRoomView;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.IUserInfoDialogCoreClient;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.json.JsonParser;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 房间入口
 *
 * @author Administrator
 */
@CreatePresenter(AvRoomPresenter.class)
public class AVRoomActivity extends BaseMvpActivity<IAvRoomView, AvRoomPresenter>
        implements View.OnClickListener, IAvRoomView {

    private final String TAG = "AVRoomActivity";
    public static final int NEW_USER_SKIP_TYPE = 4;//心动礼包的skipType 注意跟一般banner跳转不一样
    //    private ImageView roomBg;
    private RelativeLayout finishLayout;
    private ViewStub mVsRoomOffline;
    private ImageView avatarBg;
    private TextView nick;
    private ImageView avatar;
    private ImageView gender;

    private long roomUid;
    private HerUserInfo herInfo;//去找TA时，TA的资料

    private AbsRoomFragment mCurrentFragment;

    private InputPwdDialogFragment mPwdDialogFragment;
    private Disposable subscribe;

    private RoomInfo mRoomInfo;

    private View mainContainer;
    private ImageView ivBack;
    private int mDefaultBgResId = R.drawable.chat_room_bg_for_non_cp;

    public static void start(Context context, long roomUid) {
        Intent intent = new Intent(context, AVRoomActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        context.startActivity(intent);
    }

    public static void start(Context context, long roomUid, int roomType) {
        Intent intent = new Intent(context, AVRoomActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        intent.putExtra(Constants.ROOM_TYPE, roomType);
        context.startActivity(intent);
    }

    /**
     * 去找TA时使用
     *
     * @param roomUid 房间ID
     * @param herInfo 去找TA时，TA的资料
     */
    public static void start(Context context, long roomUid, HerUserInfo herInfo) {
        Intent intent = new Intent(context, AVRoomActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        intent.putExtra(Constants.ROOM_HER_USER_INFO, JsonParser.toJson(herInfo));
        context.startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // 如果是同一个房间，则只更新房间的信息
        long newRoomUid = intent.getLongExtra(Constants.ROOM_UID, 0);
        String herInfoStr = intent.getStringExtra(Constants.ROOM_HER_USER_INFO);//去找TA时，TA的UID
        if (!TextUtils.isEmpty(herInfoStr)) {
            herInfo = JsonParser.parseJsonObject(herInfoStr, HerUserInfo.class);
        }
        if (newRoomUid != 0 && newRoomUid == roomUid) {
            enterRoom();
            return;
        }
        roomUid = newRoomUid;
        getSupportFragmentManager().beginTransaction()
                .remove(mCurrentFragment).commitAllowingStateLoss();
        mCurrentFragment = null;
        // 相同类型的房间，但是是不同人的房间
        if (AvRoomDataManager.get().isFirstEnterRoomOrChangeOtherRoom(roomUid)) {
            mainContainer.setVisibility(View.GONE);
            enterRoom();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        L.debug(TAG, "onCreate start");
        setContentView(R.layout.activity_chat_room);
        mainContainer = findViewById(R.id.main_container);
        ivBack = (ImageView) findViewById(R.id.iv_room_back);
        //保持房间页面不息屏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        roomUid = getIntent().getLongExtra(Constants.ROOM_UID, 0);
        String herInfoStr = getIntent().getStringExtra(Constants.ROOM_HER_USER_INFO);//去找TA时，TA的UID
        if (!TextUtils.isEmpty(herInfoStr)) {
            herInfo = JsonParser.parseJsonObject(herInfoStr, HerUserInfo.class);
        }
        checkIsKick(roomUid);


        setSwipeBackEnable(false);

//        roomBg = (ImageView) findViewById(R.id.room_bg);
        mVsRoomOffline = (ViewStub) findViewById(R.id.vs_room_offline);
        //修改新的注册监听方式防止内存泄露
        IMNetEaseManager.get().subscribeChatRoomEventObservable(new Consumer<RoomEvent>() {
            @Override
            public void accept(RoomEvent roomEvent) throws Exception {
                onRoomEventReceive(roomEvent);
            }
        }, this);
        //第一次进来
        if (AvRoomDataManager.get().isFirstEnterRoomOrChangeOtherRoom(roomUid)) {
            mainContainer.setVisibility(View.GONE);
            // getDialogManager().showProgressDialog(this, getString(R.string.waiting_text), false);
            enterRoom();
        } else {
            addRoomFragment(true);
        }
        //为了避免标记未改变

        CoreUtils.register(this);
        L.debug(TAG, "onCreate end");
//        CoreManager.getCore(IRoomService.class).getRoomBasicMgr();

        CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    private void checkIsKick(long roomUid) {

        int kickTime = CoreManager.getCore(VersionsCore.class).checkKick();
        if (kickTime < 1)
            return;

        if (kickTime > 600) {
            kickTime = 600;
        }


        String kickInfo = (String) SpUtils.get(this, SpEvent.onKickRoomInfo, "");
        Json json = Json.parse(kickInfo);
        String roomUidCache = json.str(SpEvent.roomUid);
        String time = json.str(SpEvent.time);
        if (roomUidCache.equals(roomUid + "")) {
            int i = kickTime * 1000;
            if (BasicConfig.isDebug && i > 10000) {
                i = 10000;
            }
            if (System.currentTimeMillis() - JavaUtil.str2long(time) < i) {
                toast("您被此房间踢出，请稍后再进");
                finish();
            }
        }
    }

    @Override
    public boolean blackStatusBar() {
        return false;
    }

    private void onRoomEventReceive(RoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) return;

        int event = roomEvent.getEvent();
        switch (event) {
            case RoomEvent.ENTER_ROOM:
                LogUtil.d("zhangjian", "onRoomEventReceive onEnterRoom");
                onEnterRoom();
                break;
            case RoomEvent.KICK_OUT_ROOM:
                onKickMember(roomEvent.getReason());
                break;
            case RoomEvent.ROOM_MANAGER_ADD:
            case RoomEvent.ROOM_MANAGER_REMOVE:
                if (AvRoomDataManager.get().isSelf(roomEvent.getAccount())) {
                    if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_ADD) {
                        toast(R.string.set_room_manager);
                    } else if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_REMOVE) {
                        toast(R.string.remove_room_manager);
                    }
                }
                break;
            case RoomEvent.RTC_ENGINE_NETWORK_BAD:
                toast("当前网络不稳定，请检查网络");
                break;
            case RoomEvent.RTC_ENGINE_NETWORK_CLOSE:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                break;
            case RoomEvent.ROOM_INFO_UPDATE:
                setBackBg(roomEvent.getRoomInfo());
                break;
            default:
        }
    }

    private void onKickMember(ChatRoomKickOutEvent reason) {
        if (reason == null) return;

//        statisticsReason(reason);

        ChatRoomKickOutEvent.ChatRoomKickOutReason reasonReason = reason.getReason();
        getDialogManager().dismissDialog();
        if (reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.BE_BLACKLISTED) {
            toast(getString(R.string.add_black_list));
            finish();
        } else if (reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.CHAT_ROOM_INVALID) {
            showLiveFinishView();
        } else if (reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.KICK_OUT_BY_MANAGER) {

            Json json = new Json();
            json.set(SpEvent.roomUid, roomUid + "");
            json.set(SpEvent.time, System.currentTimeMillis());
            SpUtils.put(this, SpEvent.onKickRoomInfo, json + "");
            toast(getString(R.string.kick_member_by_manager));
            finish();
        } else {
            finish();
        }
    }

    private void onEnterRoom() {
        mRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        Logger.i("进入房间成功：" + mRoomInfo.getRoomId());
        mainContainer.setVisibility(View.VISIBLE);
        dimissDialog();
        if (mRoomInfo != null && mRoomInfo.getTagType() == RoomInfo.ROOMTYPE_LOVERS) {
            mDefaultBgResId = R.drawable.chat_room_bg_for_cp;
        } else {
            mDefaultBgResId = R.drawable.chat_room_bg_for_non_cp;
        }
    }

    /**
     * @param isCreate 避免不在onCreate方法中调用commit报错IllegalStateException:
     *                 Can not perform this action after onSaveInstanceState
     */
    private void addRoomFragment(boolean isCreate) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            //获取房主信息
            IAVRoomCore core = CoreManager.getCore(IAVRoomCore.class);
            core.removeRoomOwnerInfo();
            core.requestRoomOwnerInfo(roomInfo.getUid() + "");

            setBackBg(roomInfo);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            int roomType = roomInfo.getTagType();
            L.info(TAG, "addRoomFragment isCreate: %b, roomType: %d", isCreate, roomType);
            if (roomType == RoomInfo.ROOMTYPE_LOVERS) {
                mCurrentFragment = HomeLoverFragment.newInstance(roomUid);
            } else if (roomType == RoomInfo.ROOMTYPE_PERSONAL) {
                mCurrentFragment = HomePersonalFragment.newInstance(roomUid);
            } else {
                mCurrentFragment = HomePartyFragment.newInstance(roomUid);
            }

            if (mCurrentFragment != null) {
                String TAG = "AVRoomFragment";
                if (isCreate) {
                    fragmentTransaction.replace(R.id.main_container, mCurrentFragment, TAG).commit();
                } else {
                    fragmentTransaction.replace(R.id.main_container, mCurrentFragment, TAG).commitAllowingStateLoss();
                }
            }
            getMvpPresenter().getActionDialog(2);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        L.debug(TAG, "onBackPressed");
        AvRoomDataManager.get().setMinimize(true);
    }

    private void setBackBg(RoomInfo roomInfo) {
        L.info(TAG, "setBackBg roomInfo: %s", roomInfo);
        if (roomInfo != null) {
            if (mRoomInfo != null && mRoomInfo.getTagType() == RoomInfo.ROOMTYPE_LOVERS) {
                mDefaultBgResId = R.drawable.chat_room_bg_for_cp;
            } else {
                mDefaultBgResId = R.drawable.chat_room_bg_for_non_cp;
            }
            String backPicUrl = roomInfo.getBackPicUrl();
            L.info(TAG, "setBackBg backPicUrl: %s", backPicUrl);
            if (StringUtils.isNotEmpty(backPicUrl)) {
                ImageLoadUtils.loadImage(this, roomInfo.getBackPicUrl(), ivBack, mDefaultBgResId, mDefaultBgResId);
            } else {
                ImageLoadUtils.loadImageResNoCache(this, mDefaultBgResId, ivBack, mDefaultBgResId);
            }
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo userInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            setBackBg(roomInfo);
        }
        setUserInfo(userInfo);
    }

    @CoreEvent(coreClientClass = IBgClient.class)
    public void bgModify(String type, String bgUrl) {
        if (StringUtils.isNotEmpty(bgUrl)) {
            ImageLoadUtils.loadImage(this, bgUrl, ivBack);
        } else {
            ImageLoadUtils.loadImageResNoCache(this, mDefaultBgResId, ivBack, mDefaultBgResId);
        }
    }

    private void showLiveFinishView() {
        if (mRoomInfo != null) {
            if (finishLayout == null) {
                finishLayout = (RelativeLayout) mVsRoomOffline.inflate();
                avatar = finishLayout.findViewById(R.id.avatar);
                avatarBg = finishLayout.findViewById(R.id.avatar_bg);
                nick = finishLayout.findViewById(R.id.nick);
                gender = finishLayout.findViewById(R.id.iv_gender);
            }
            finishLayout.setVisibility(View.VISIBLE);
            finishLayout.findViewById(R.id.home_page_btn).setOnClickListener(this);
            finishLayout.findViewById(R.id.back_btn).setOnClickListener(this);

            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(mRoomInfo.getUid());
            setUserInfo(userInfo);
            AvRoomDataManager.get().release();
        } else {
            AvRoomDataManager.get().release();
            finish();
        }
    }

    private void setUserInfo(UserInfo userInfo) {
        if (avatarBg == null || avatar == null || nick == null) return;
        if (userInfo != null) {
            //   ImageLoadUtils.loadImageWithBlurTransformation(this, userInfo.getAvatar(), avatarBg);
            ImageLoadUtils.loadCircleImage(this, userInfo.getAvatar(), avatar, R.drawable.ic_no_avatar);
            nick.setText(userInfo.getNick());
            if (userInfo.getGender() == 1) {
                gender.setImageResource(R.drawable.icon_man);
            } else {
                gender.setImageResource(R.drawable.icon_female);
            }
        } else {
            avatar.setImageResource(R.drawable.ic_no_avatar);
        }
    }

    private void enterRoom() {
        getMvpPresenter().enterRoomFromService(String.valueOf(roomUid));
    }


    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void onUpdateMyRoomRoleFail() {
        toast("操作太频繁，请30秒后再试");
    }

    private void showRoomPwdDialog(final RoomInfo roomInfo) {
        if (mPwdDialogFragment == null) {
            mPwdDialogFragment = InputPwdDialogFragment.newInstance(getString(R.string.input_pwd),
                    getString(R.string.ok),
                    getString(R.string.cancel),
                    roomInfo.getRoomPwd());

            //google的bug
            try {
                mPwdDialogFragment.show(getSupportFragmentManager(), "pwdDialog");
            } catch (Exception e) {
                finish();
            }

            mPwdDialogFragment.setOnDialogBtnClickListener(new InputPwdDialogFragment.OnDialogBtnClickListener() {
                @Override
                public void onBtnConfirm() {
                    mPwdDialogFragment.dismiss();
                    mPwdDialogFragment = null;
                    getMvpPresenter().enterRoom(roomInfo, AVRoomActivity.this, herInfo);
                }

                @Override
                public void onBtnCancel() {
                    mPwdDialogFragment.dismiss();
                    mPwdDialogFragment = null;
                    finish();
                }
            });
        }
    }

    @Override
    public void onGetActionDialog(List<ActionDialogInfo> dialogInfo) {
        if (mCurrentFragment != null && mCurrentFragment.isVisible()) {
            mCurrentFragment.onShowActivity(dialogInfo);
        }
    }

    @Override
    public void onGetActionDialogError(String error) {
    }

    @Override
    public void exitRoom(RoomInfo currentRoomInfo) {
        if (currentRoomInfo != null && currentRoomInfo.getUid() == roomUid) {
            finish();
        }
    }

    @Override
    public void onRoomOnlineNumberSuccess(int onlineNumber) {
        if (mCurrentFragment != null)
            mCurrentFragment.onRoomOnlineNumberSuccess(onlineNumber);
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftPastDue() {
        toast(R.string.gift_expire);
    }

    @CoreEvent(coreClientClass = ICommonClient.class)
    public void onRecieveNeedRecharge() {
        getDialogManager().showOkCancelDialog("余额不足，是否充值", true,
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        MyWalletNewActivity.start(AVRoomActivity.this);
                    }
                });
    }

    public void toBack() {
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomUid == myUid && roomInfo != null && roomInfo.getType() != RoomInfo.ROOMTYPE_HOME_PARTY) {
            getDialogManager().showOkCancelDialog("当前正在开播，是否要关闭直播？", true, new DialogManager.OkCancelDialogListener() {
                @Override
                public void onCancel() {

                }

                @Override
                public void onOk() {
                    exitRoom();
                }
            });
        } else {
            exitRoom();
        }
    }

    public void exitRoom() {
        getMvpPresenter().exitRoom();
        finish();
    }


    @Override
    protected int setBgColor() {
        return R.color.black;
    }

    @Override
    protected boolean needSteepStateBar() {
        return false;
    }

    @Override
    protected void onDestroy() {
        if (subscribe != null) {
            subscribe.dispose();
            subscribe = null;
        }
        CoreUtils.unregister(this);

        super.onDestroy();
        if (mPwdDialogFragment != null) {
            //这里可能会导致空指针，直接捕获，不蹦就行
            try {
                mPwdDialogFragment.dismiss();
            } catch (Exception e) {
                L.error(TAG, "onDestroy exception:", e);
            }
            mPwdDialogFragment = null;
        }


        fixInputMethodManagerLeak(this);
//        if (mCurrentFragment != null) {
//            mCurrentFragment.releaseAllAudioPlayer();
//            mCurrentFragment = null;
//
//        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.home_page_btn) {
            if (mRoomInfo != null) {
                NewUserInfoActivity.start(this, mRoomInfo.getUid());
                finish();
            }

        } else if (i == R.id.back_btn) {
            finish();

        } else {
        }
    }

    @Override
    public void requestRoomInfoSuccessView(RoomInfo roomInfo) {
        if (roomInfo != null) {
            int[] hideFace = roomInfo.getHideFace();
            if (hideFace != null) {
                for (int i = 0; i < hideFace.length; i++) {
                    L.info("RoomInfo", "requestRoomInfoSuccessView hideFace[%d]: %d", i, hideFace[i]);
                }
            }
        }
        mRoomInfo = roomInfo;
        if (TextUtils.isEmpty(roomInfo.getRoomPwd())
                || roomInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
            getMvpPresenter().enterRoom(roomInfo, AVRoomActivity.this, herInfo);
        } else {
            if (isFinishing()) {
                return;
            }
            showRoomPwdDialog(roomInfo);
        }
    }

    @Override
    public void requestRoomInfoFailView(int code, String errorStr) {
        getDialogManager().dismissDialog();
        if (code == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
            //MainActivity回调弹出弹框 马上AVRoomActivity就要被销毁，这里不能传递this来显示弹窗，弹窗一显示也会马上随着AVRoomActivity销毁而销毁
            CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_REAL_NAME_AUTH,
                    getResources().getString(R.string.real_name_auth_tips,
                            getResources().getString(R.string.real_name_auth_tips_publish_content)));
        } else {
            toast(errorStr);
        }
        finish();
    }

    @Override
    public void enterRoomSuccess() {
        L.debug(TAG, "enterRoomSuccess");
        if (mPwdDialogFragment != null) {
            mPwdDialogFragment.dismiss();
        }
        addRoomFragment(false);
        //获取管理员
        getMvpPresenter().getNormalChatMember();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReconnectRoom(NewRoomEvent.OnEnterRoomSuccess success) {
        getMvpPresenter().getNormalChatMember();
    }

    @Override
    public void enterRoomFail(int code, String error) {
        // : 2018/4/13 0013
        dimissDialog();
        {
            AvRoomDataManager.get().release();
            toast(error);
            finish();
        }

    }

    @Override
    public void showFinishRoomView() {
        dimissDialog();
        showLiveFinishView();
    }

    @Override
    public void showBlackEnterRoomView() {

        dimissDialog();
        AvRoomDataManager.get().release();
        toast(getString(R.string.add_black_list));
        finish();
    }

    private void dimissDialog() {
        getDialogManager().dismissDialog();
        if (mPwdDialogFragment != null) {
            mPwdDialogFragment.dismiss();
        }
    }


    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onUserCome(ChatRoomMessage roomMessage) {
        RoomMemberComeInfo memberComeInfo = new RoomMemberComeInfo();
        ChatRoomNotificationAttachment attachment = (ChatRoomNotificationAttachment) roomMessage.getAttachment();
        List<String> nicks = attachment.getTargetNicks();
        if (nicks != null && nicks.size() > 0) {
            memberComeInfo.setNickName(nicks.get(0));
        }
        try {
            Map<String, Object> senderExtension = roomMessage.getChatRoomMessageExtension().getSenderExtension();
            memberComeInfo.setExperLevel((Integer) senderExtension.get(AvRoomModel.EXPER_LEVEL));
            memberComeInfo.setCarName((String) senderExtension.get(AvRoomModel.USER_CAR_NAME));
            memberComeInfo.setCarImgUrl((String) senderExtension.get(AvRoomModel.USER_CAR));
            memberComeInfo.setIsNewUser((String) senderExtension.get(AvRoomModel.IS_NEW_USER));
        } catch (Exception e) {
            e.printStackTrace();
        }
        AvRoomDataManager.get().addMemberComeInfo(memberComeInfo);//加到处理队列（因为有可能fragment还没初始化，还不能show，所以加到队列，由fragment处理后再remove）
        CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.dealUserComeMsg);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {

        AvRoomDataManager.get().release();
        finish();
    }

    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void enterError() {
        enterRoomFail(-1, "网络异常");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        CoreManager.notifyClients(IAppInfoCoreClient.class,IAppInfoCoreClient.METHOD_ON_SEND_PIC,requestCode,resultCode,data);
    }

    /**
     * 个人资料弹窗点击-显示私聊弹窗
     */
    @CoreEvent(coreClientClass = IUserInfoDialogCoreClient.class)
    public void showMsgChatDialog(String userId) {
        if (String.valueOf(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid()).equals(userId)) {
            SingleToastUtil.showToast("不允许跟自己私聊！");
            return;
        }
        HttpUtil.checkUserIsDisturbInRoom(userId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPrivateChatEnable(RoomPrivateChatEvent.OnSuccess onSuccess) {
        String userId = onSuccess.getUserId();
        NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(userId);
        if (nimUserInfo != null) {
            RoomPrivateChatDialog chat = RoomPrivateChatDialog.newInstance(userId);
            chat.show(getSupportFragmentManager(), null);
        } else {
            NimUserInfoCache.getInstance().getUserInfoFromRemote(userId + "", new RequestCallbackWrapper<NimUserInfo>() {
                @Override
                public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                    if (i == 200) {
                        RoomPrivateChatDialog chat = RoomPrivateChatDialog.newInstance(userId);
                        chat.show(getSupportFragmentManager(), null);
                    } else {
                        SingleToastUtil.showToast("进入私聊失败！");
                    }
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMusicPlay(RoomProfileAction.OnMusicClicked musicClicked) {
        BottomSetAudioDialog bottomSetAudioDialog = new BottomSetAudioDialog(this);
        bottomSetAudioDialog.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoomMsgItemClicked(RoomTalkEvent.OnRoomMsgItemClicked clicked) {
        String account = "";
        ChatRoomMessage chatRoomMessage = clicked.getMessage();
        if (chatRoomMessage.getMsgType() != MsgTypeEnum.tip) {
            if (chatRoomMessage.getMsgType() == MsgTypeEnum.text) {
                account = chatRoomMessage.getFromAccount();
            } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.notification) {
                account = chatRoomMessage.getFromAccount();
            } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
                if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                    GiftAttachment giftAttachment = (GiftAttachment) attachment;
                    if (giftAttachment != null) {
                        GiftReceiveInfo giftRecieveInfo = giftAttachment.getGiftRecieveInfo();
                        if (giftRecieveInfo != null) {
                            account = giftRecieveInfo.getUid() + "";
                        }

                    }
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                    MultiGiftAttachment giftAttachment = (MultiGiftAttachment) attachment;
                    account = giftAttachment.getMultiGiftRecieveInfo().getUid() + "";
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP) {
                    account = ((RoomTipAttachment) attachment).getUid() + "";
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE) {
                    account = ((FaceAttachment) attachment).getUid() + "";
                }
            } else {
                account = "";
            }
            if (TextUtils.isEmpty(account)) return;
            final List<ButtonItem> buttonItems = new ArrayList<>();
            List<ButtonItem> items = ButtonItemFactory.createAllRoomPublicScreenButtonItems(this, account);
            if (items == null) {
                return;
            }
            buttonItems.addAll(items);
            getDialogManager().showCommonPopupDialog(buttonItems, "取消");
        }
    }
}