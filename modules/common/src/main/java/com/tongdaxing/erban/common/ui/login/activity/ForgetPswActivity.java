package com.tongdaxing.erban.common.ui.login.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.ui.login.CodeDownTimer;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.utils.Logger;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by zhouxiangfeng on 17/3/5.
 */
public class ForgetPswActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "RegisterActivity";
    @BindView(R2.id.title_bar)
    TitleBar titleBar;
    @BindView(R2.id.et_phone)
    EditText etPhone;
    @BindView(R2.id.et_code)
    EditText etCode;
    @BindView(R2.id.btn_get_code)
    TextView btnGetCode;
    @BindView(R2.id.code_layout)
    LinearLayout codeLayout;
    @BindView(R2.id.et_password)
    EditText etPassword;
    @BindView(R2.id.btn_modify)
    Button btnModify;
    private String errorStr;
    private CodeDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_psw);
        ButterKnife.bind(this);
        initTitleBar("");
//        onFindViews();
    }


    public void onFindViews() {
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() < 6 || charSequence.length() > 16) {
                    SingleToastUtil.showToast("密码长度6-16个字符");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onSmsFail(String error) {
        toast(error);
        Logger.error(TAG, "获取短信失败!");
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onModifyPsw() {
        toast("重置密码成功！");
        finish();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onModifyPswFail(String error) {
        toast(error);
        System.out.println("error===" + error);
    }

    @OnClick({R2.id.btn_modify, R2.id.btn_get_code})
    public void onClick(View v) {
        String phone = etPhone.getText().toString();
        int i = v.getId();
        if (i == R.id.btn_modify) {
            String psw = etPassword.getText().toString();
            String sms_code = etCode.getText().toString();
            if (!StringUtils.isEmpty(sms_code)) {
                if (isOK(phone, psw)) {
                    CoreManager.getCore(IAuthCore.class).requestResetPsw(phone, sms_code, psw);
                } else {
                    toast(errorStr);
                }
            } else {
                toast("验证码不能为空！");
            }

        } else if (i == R.id.btn_get_code) {
            if (phone.length() == 11) {
                timer = new CodeDownTimer(btnGetCode, 60000, 1000);
                timer.start();
                CoreManager.getCore(IAuthCore.class).doRequestSMSCode(phone, 3);
            } else {
                toast("手机号码不正确");
            }

        }
    }

    private boolean isOK(String phone, String psw) {
        if (StringUtils.isEmpty(psw) && psw.length() < 6) {
            errorStr = "请核对密码！";
            return false;
        }
        if (StringUtils.isEmpty(phone)) {
            errorStr = "请填写手机号码！";
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
    }
}
