package com.tongdaxing.erban.common.ui.find.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Function:
 * Author: Edward on 2019/3/27
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 3;
    private String tabTitles[];
    private Fragment data[];

    public ViewPagerAdapter(FragmentManager fm, Context context, Fragment data[]) {
        super(fm);
        tabTitles = new String[]{"资料", "动态", "礼物"};
        this.data = data;

    }

    @Override
    public Fragment getItem(int position) {
        return data[position];
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}