package com.tongdaxing.erban.common;

import com.tcloud.core.thread.ExecutorCenter;
import com.tcloud.core.thread.WorkRunnable;

/**
 * Created by Chen on 2019/7/12.
 */
public abstract class AbsInit {
    public AbsInit(){
        onMainInit();
        ExecutorCenter.getInstance().post(new WorkRunnable() {
            @android.support.annotation.NonNull
            @Override
            public String getTag() {
                return this.toString();
            }

            @Override
            public void run() {
                onDelayInit();
            }
        });
    }

    protected void onMainInit(){}

    protected void onDelayInit(){}


}
