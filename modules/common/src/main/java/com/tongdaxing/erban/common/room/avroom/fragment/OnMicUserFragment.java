package com.tongdaxing.erban.common.room.avroom.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.room.avroom.adapter.OnMicUserAdapter;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;

import java.util.ArrayList;
import java.util.List;

import static com.tongdaxing.xchat_core.manager.AvRoomDataManager.get;

/**
 * 麦上用户列表
 * Created by zeda on 2018/11/2.
 */
public class OnMicUserFragment extends BaseFragment implements BaseQuickAdapter.OnItemClickListener {
    private RecyclerView mRecyclerView;

    private OnMicUserAdapter onMicUserAdapter;
    private OnMicUserItemClick onMicUserItemClick;

    @Override
    public void onFindViews() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        onMicUserAdapter = new OnMicUserAdapter();
        onMicUserAdapter.setOnItemClickListener(this);
        mRecyclerView.setAdapter(onMicUserAdapter);

        registerMicDownUpEvent();
        refreshView();
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.common_recycler_view;
    }

    private void refreshView() {
        ArrayList<ChatRoomMember> chatRoomMembers = new ArrayList<>();
        for (int i = 0; i < get().mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = get().mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                chatRoomMembers.add(roomQueueInfo.mChatRoomMember);
            }
        }
        onMicUserAdapter.setNewData(chatRoomMembers);
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int position) {
        if (get().mCurrentRoomInfo != null && onMicUserItemClick != null) {
            List<ChatRoomMember> chatRoomMembers = onMicUserAdapter.getData();
            if (ListUtils.isListEmpty(chatRoomMembers)) return;
            //Index: 4, Size: 4
            //com.tongdaxing.erban.common.room.avroom.b.m.onItemClick(OnlineUserFragment.java:177)
            if (chatRoomMembers.size() > position) {
                onMicUserItemClick.onItemClick(chatRoomMembers.get(position));
            }
        }
    }

    public void setOnMicUserItemClick(OnMicUserItemClick onMicUserItemClick) {
        this.onMicUserItemClick = onMicUserItemClick;
    }

    private void registerMicDownUpEvent() {
        IMNetEaseManager.get().subscribeChatRoomEventObservable(roomEvent -> {
            if (roomEvent == null) return;
            int event = roomEvent.getEvent();
            if (event == RoomEvent.UP_MIC || event == RoomEvent.DOWN_MIC) {
                refreshView();
            }
        }, this);
    }

    public interface OnMicUserItemClick {
        void onItemClick(ChatRoomMember chatRoomMember);
    }
}
