package com.tongdaxing.erban.common.room.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.tongdaxing.erban.common.base.fragment.BaseDialogFragment;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpEvent;

/**
 * 房间CP邀请弹窗
 * 是否解除当前CP关系与XXX结成一周CP
 * <p>
 * Created by zhangjian on 2019/3/29.
 */
public class RoomRemoveCurrentCpDialog extends BaseDialogFragment {
    private static String KEY_CP_DES = "CpDes";

    public static RoomRemoveCurrentCpDialog instance(String des) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_CP_DES, des);
        RoomRemoveCurrentCpDialog roomCpInviteDialog = new RoomRemoveCurrentCpDialog();
        roomCpInviteDialog.setArguments(bundle);
        return roomCpInviteDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View view = inflater.inflate(R.layout.dialog_room_remove_current_cp, ((ViewGroup) window.findViewById(android.R.id.content)), false);//需要用android.R.id.content这个view
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        String des = getArguments().getString(KEY_CP_DES);
        TextView tv_cp_remove_and_agree = view.findViewById(R.id.tv_cp_remove_and_agree);
        TextView tv_cancel = view.findViewById(R.id.tv_cancel);
        TextView tv_ok = view.findViewById(R.id.tv_ok);

        tv_cp_remove_and_agree.setText(des);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CoreUtils.send(new WeekCpEvent.OnRemoveCurrentCpForNewWeekCp());
                dismiss();
            }
        });
    }
}
