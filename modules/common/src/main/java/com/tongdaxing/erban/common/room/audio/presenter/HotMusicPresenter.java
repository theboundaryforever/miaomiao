package com.tongdaxing.erban.common.room.audio.presenter;

import com.tongdaxing.erban.common.room.audio.view.IHotMusicView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public class HotMusicPresenter extends AbstractMvpPresenter<IHotMusicView> {
    private int currPage = Constants.PAGE_START;

    private List<MyMusicInfo> mMusicList = new CopyOnWriteArrayList<>();

    @Override
    public void onResumePresenter() {
        super.onResumePresenter();
        getMvpView().showRefresh(true);
        refreshData();
    }

    /**
     * 刷新数据
     */
    public void refreshData() {
        getHotSongList(Constants.PAGE_START);
    }

    public void loadMoreData() {
        getHotSongList(currPage + 1);
    }

    public void getHotSongList(int page) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageSize", String.valueOf(Constants.PAGE_SIZE));
        params.put("pageNum", String.valueOf(page));
        params.put("key", "");//主要用来搜索
        OkHttpManager.getInstance().doGetRequest(UriProvider.getHotSongList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<MyMusicInfo>>>() {
            @Override
            public void onError(Exception e) {
                getMvpView().loadDataFailure(page == Constants.PAGE_START, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<MyMusicInfo>> response) {
                currPage = page;
                if (response != null && response.isSuccess() && response.getData() != null) {
                    mMusicList.addAll(response.getData());
                    if (currPage == Constants.PAGE_START) {
                        getMvpView().refreshMyMusicList(true, mMusicList);
                    } else {
                        getMvpView().refreshMyMusicList(false, mMusicList);
                    }
                }
            }
        });
    }
}
