package com.tongdaxing.erban.common.room.user;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tongdaxing.erban.common.room.RoomRoutePath;

import java.lang.ref.WeakReference;

/**
 * Created by Chen on 2019/6/12.
 */
public class UserInfoDialogManager {
    public static void showDialogFragment(Context context, long userId) {
        if (!checkActivityValid(context)) {
            return;
        }
        WeakReference<Context> weakReference = new WeakReference<>(context);
        if (weakReference.get() instanceof FragmentActivity) {
            DialogFragment dialogFragment = (DialogFragment) ARouter.getInstance()
                    .build(RoomRoutePath.ROOM_USER_INFO_PATH)
                    .withLong(RoomRoutePath.ROOM_USER_INFO_USER_ID, userId)
                    .navigation();
            if (dialogFragment.isAdded()) {
                if (dialogFragment.isStateSaved()) {
                    dialogFragment.dismissAllowingStateLoss();
                }
            } else {
                if (!dialogFragment.isStateSaved()) {
                    dialogFragment.show(((FragmentActivity) weakReference.get()).getSupportFragmentManager(), "UserInfoDialogManager");
                }
            }
        }
    }

    private static boolean checkActivityValid(Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity && ((Activity) context).isFinishing()) {
            return false;
        }

        if (context instanceof Activity && ((Activity) context).isDestroyed()) {
            return false;
        }
        return true;
    }
}
