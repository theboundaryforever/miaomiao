package com.tongdaxing.erban.common.room.profile;

import android.util.SparseArray;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.room.RoomBasePresenter;
import com.tongdaxing.erban.common.room.personal.profile.RoomProfileAction;
import com.tongdaxing.erban.common.ui.find.model.CreateRoomModel;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.agora.LiveEvent;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.praise.PraiseEvent;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.RoomAction;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.RoomFaceEvent;
import com.tongdaxing.xchat_core.room.weekcp.RoomLoverEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Chen on 2019/6/5.
 */
public class RoomProfilePresenter extends RoomBasePresenter<IRoomProfileView> {
    private String TAG = "RoomProfilePresenter";
    private CreateRoomModel createRoomModel;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        if (getView() == null) {
            return;
        }
        updateChairView();
        checkIsPraise();
        createRoomModel = new CreateRoomModel();
    }

    private void checkIsPraise() {
        boolean roomOwner = isRoomOwner();
        L.debug(TAG, "checkIsPraise roomOwner: %b", roomOwner);
        if (!roomOwner) {
            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (roomInfo != null) {
                CoreManager.getCore(IPraiseCore.class).isPraised(CoreManager.getCore(IAuthCore.class).getCurrentUid(), roomInfo.getUid());
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChairStateChange(RoomAction.OnChairStateChange change) {
        L.debug(TAG, "onChairStateChange");
        if (getView() == null) {
            return;
        }
        if (AvRoomDataManager.get().isRoomOwner(change.getPlayerId())) {
            updateChairView();
        }
    }

    public SparseArray<RoomQueueInfo> getRoomQueueInfo() {
        return AvRoomDataManager.get().mMicQueueMemberMap;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateChairView(RoomAction.OnUpdateChairView updateChairView) {
        L.debug(TAG, "onUpdateChairView");
        updateChairView();
    }

    private void updateChairView() {
        if (getView() == null) {
            return;
        }
        SparseArray<RoomQueueInfo> queueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (queueMemberMap != null) {
            getView().updateChairView(queueMemberMap);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlayerSpeak(LiveEvent.OnSpeakerVolume speakerVolume) {
        if (getView() == null) {
            return;
        }
        L.debug(TAG, "onPlayerSpeak volume: %f, pos: %d", speakerVolume.getVolume(), speakerVolume.getPos());
        if (speakerVolume.getPos() == -1) {
            getView().updateSpeakState(speakerVolume.hasSound());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveFace(RoomFaceEvent.OnReceiveFace face) {
        L.debug(TAG, "function:sendFace RoomLoverChairPresenter onReceiveFace faceReceiveInfos: %s", face.getReceiveInfos());
        if (getView() == null) {
            return;
        }
        getView().onReceiveFace(face.getReceiveInfos());
    }

    public void microPhonePositionClick() {
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null || getView() == null) {
            return;
        }
        final String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(-1);
        if (roomQueueInfo == null || roomQueueInfo.mRoomMicInfo == null) {
            L.error(TAG, "microPhonePositionClick roomQueueInfo or roomQueueInfo.mRoomMicInfo is null.");
            return;
        }

        boolean isRoomOwner = AvRoomDataManager.get().isRoomOwner(currentUid);
        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;

        if (chatRoomMember == null) {
            chatRoomMember = new ChatRoomMember();
            RoomInfo info = AvRoomDataManager.get().mCurrentRoomInfo;
            chatRoomMember.setNick("房主");
            chatRoomMember.setAvatar("");
            chatRoomMember.setAccount(info.getUid() + "");
        }

        // 自己是房主
        if (isRoomOwner) {
            getView().showOwnerSelfInfo(chatRoomMember);
        } else {
            getView().showGiftDialog(chatRoomMember);
        }
    }

    public void doAttention() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            CoreManager.getCore(IPraiseCore.class).praise(roomInfo.getUid());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionSuccess(PraiseEvent.OnAttentionSuccess success) {
        if (getView() == null) {
            return;
        }
        L.info(TAG, "onAttentionSuccess uid: %d", success.getUid());
        long uid = success.getUid();
        if (AvRoomDataManager.get().isRoomOwner(uid)) {
            getView().attentionSuccess(success.getUid(), success.isAttention());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionFailed(PraiseEvent.OnAttentionFailed failed) {
        if (getView() == null) {
            return;
        }
        getView().attentionFailed(failed.getMessage());
    }

    @Subscribe
    public void onAttentionClicked(RoomProfileAction.OnAttentionClicked failed) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            long uid = roomInfo.getUid();
            IPraiseCore core = CoreManager.getCore(IPraiseCore.class);
            boolean isPraised = core.checkIsPraised(uid);
            if (!isPraised) {
                core.praise(uid);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionCancel(PraiseEvent.OnAttentionCancel cancel) {
        if (getView() == null) {
            return;
        }
        getView().attentionCancel(cancel.getUid());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRequestChatMemberByPageSuccess(RoomLoverEvent.OnRequestSuccess onRequestSuccess) {
        if (getView() == null) {
            return;
        }
        int onlineUserNum = onRequestSuccess.getOnlineChatMembers().size();
        getView().updateOnlineUserNum(onlineUserNum);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoomOnlineNumberChange(NewRoomEvent.OnRoomOnlineNumberChange change) {
        if (getView() == null) {
            return;
        }
        getView().updateOnlineUserNum(change.getCount());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetRoomOwnerInfoSuccess(NewRoomEvent.OnGetRoomOwnerInfoSuccess success) {
        if (getView() == null) {
            return;
        }
        getView().updateProfileView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onExitRoom(NewRoomEvent.OnExitRoom exitRoom) {
        String account = exitRoom.getAccount();
        L.debug(TAG, "onExitRoom account: %s", account);
        if (AvRoomDataManager.get().isRoomOwner(account)) {
            updateChairView();
        }
    }

    // 网络变化
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkChange(NewRoomEvent.OnNetworkChange change) {
        updateChairView();
    }
}
