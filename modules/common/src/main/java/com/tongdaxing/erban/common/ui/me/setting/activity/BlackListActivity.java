package com.tongdaxing.erban.common.ui.me.setting.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.ui.message.fragment.FriendBlackFragment;

/**
 * 黑名单页面
 * <p>
 * Created by zhangjian on 2019/6/21.
 */
@Route(path = CommonRoutePath.BLACK_LIST_ACTIVITY_ROUTE_PATH)
public class BlackListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_black_list);
        initView();
    }

    private void initView() {
        initTitleBar(getString(R.string.setting_black_list));
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FriendBlackFragment friendBlackFragment = (FriendBlackFragment) supportFragmentManager.findFragmentByTag(FriendBlackFragment.TAG);
        if (friendBlackFragment == null) {
            friendBlackFragment = new FriendBlackFragment();
        }
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        if (friendBlackFragment.isAdded()) {
            fragmentTransaction.remove(friendBlackFragment).commitAllowingStateLoss();
        }
        fragmentTransaction.add(R.id.audio_match_container, friendBlackFragment, FriendBlackFragment.TAG);
        fragmentTransaction.show(friendBlackFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }
}
