package com.tongdaxing.erban.common.ui.me.shopping.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.me.shopping.adapter.CarAdapter;
import com.tongdaxing.erban.common.ui.me.shopping.fragment.ShopFragment;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/3/30.
 */

public class ShopActivity extends BaseActivity {
    @BindView(R2.id.title_bar)
    TitleBar titleBar;
    @BindView(R2.id.vp_shop)
    ViewPager vpShop;
    @BindView(R2.id.shop_indicator)
    MagicIndicator shopIndicator;

    private CarAdapter carAdapter;
    private boolean showVgg = false;
    private String typeUrl = "";
    private List<Fragment> fragments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_car);
        initTitleBar("装扮商城");
        ButterKnife.bind(this);

        fragments = new ArrayList<>();


        ShopFragment headwearFragment = new ShopFragment();
        headwearFragment.setPrizeName("头饰");
        headwearFragment.setShortUrl("/headwear");
        headwearFragment.setPrizeParamsName("headwear");
        headwearFragment.setShowVgg(false);
        fragments.add(headwearFragment);

        ShopFragment carFragment = new ShopFragment();
        carFragment.setPrizeName("座驾");
        carFragment.setShortUrl("/giftCar");
        carFragment.setPrizeParamsName("car");
        carFragment.setShowVgg(true);
        fragments.add(carFragment);

        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }
        };


        List<TabInfo> mTabInfoList = new ArrayList<>();
        mTabInfoList.add(new TabInfo(1, "头饰"));
        mTabInfoList.add(new TabInfo(1, "座驾"));
        CommonNavigator commonNavigator = new CommonNavigator(this);
        CommonMagicIndicatorAdapter magicIndicatorAdapter = new CommonMagicIndicatorAdapter(this,
                mTabInfoList, UIUtil.dip2px(this, 4));
        magicIndicatorAdapter.setOnItemSelectListener(new CommonMagicIndicatorAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                vpShop.setCurrentItem(position);
            }
        });
        commonNavigator.setAdapter(magicIndicatorAdapter);
        commonNavigator.setAdjustMode(true);

        shopIndicator.setNavigator(commonNavigator);
        vpShop.setAdapter(fragmentPagerAdapter);
        ViewPagerHelper.bind(shopIndicator, vpShop);

    }


}
