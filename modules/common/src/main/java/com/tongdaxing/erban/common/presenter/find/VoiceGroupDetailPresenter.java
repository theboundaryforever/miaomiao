package com.tongdaxing.erban.common.presenter.find;

import com.tongdaxing.erban.common.ui.find.model.RecommendModel;
import com.tongdaxing.erban.common.ui.find.model.VoiceGroupDetailModel;
import com.tongdaxing.erban.common.ui.find.view.IVoiceGroupDetailView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupDetailInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public class VoiceGroupDetailPresenter extends AbstractMvpPresenter<IVoiceGroupDetailView> {
    private VoiceGroupDetailModel voiceGroupDetailModel;
    private RecommendModel recommendModel;
    private int currPage = Constants.PAGE_START;

    public VoiceGroupDetailPresenter() {
        voiceGroupDetailModel = new VoiceGroupDetailModel();
        recommendModel = new RecommendModel();
    }

    public void refreshData(String commentId) {
        loadData(Constants.PAGE_START, commentId);
    }

    public void loadMoreData(String commentId) {
        loadData(currPage + 1, commentId);
    }

    public void loadData(int page, String commentId) {
        voiceGroupDetailModel.getVoiceGroupDetailData(page, commentId, new OkHttpManager.MyCallBack<ServiceResult<VoiceGroupDetailInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(page == Constants.PAGE_START);
                }
            }

            @Override
            public void onResponse(ServiceResult<VoiceGroupDetailInfo> response) {
                currPage = page;
                if (response == null || getMvpView() == null) {
                    SingleToastUtil.showToast("数据错误!");
                    return;
                }

                if (response.isSuccess()) {
                    getMvpView().insertHead(page, response.getData());
                    getMvpView().setupSuccessView(response.getData().getPlayerList(), page == Constants.PAGE_START);
                } else {
                    onError(new Exception());
                }

            }
        });
    }

    public void sendComment(String content, String momentId, String playerUid) {
        voiceGroupDetailModel.publishComment(content, momentId, playerUid, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().publishCommentFailure(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response == null || getMvpView() == null) {
                    getMvpView().publishCommentFailure("数据错误!");
                    return;
                }

                if (response.num("code") == 200) {
                    getMvpView().publishCommentSucceed();
                } else if (response.num("code") == 2501 && response.has("message")) {//短期内不能重复发送
                    getMvpView().publishCommentFailure(response.str("message"));
                } else {
                    getMvpView().publishCommentFailure("数据错误!");
                }
            }
        });
    }

    public void reportItems(String momentId, String playerId, String reasonStr) {
        voiceGroupDetailModel.reportVoiceGroupItems(momentId, playerId, reasonStr, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    SingleToastUtil.showToast("举报成功!");
                } else {
                    SingleToastUtil.showToast("数据错误!");
                }
            }
        });
    }

    public void voiceGroupLike(int commentId) {
        recommendModel.voiceGroupLike(String.valueOf(commentId), new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    getMvpView().refreshHeadAdapterLikeStatus(commentId);
                } else {
                    SingleToastUtil.showToast("数据错误!");
                }
            }
        });
    }

    public void commentLike(int playerId) {
        voiceGroupDetailModel.commentLike(String.valueOf(playerId), new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    getMvpView().refreshAdapterLikeStatus(playerId);
                } else {
                    onError(new Exception("数据错误!"));
                }
            }
        });
    }

    public void deleteVoiceGroupItem(String commentId) {
        voiceGroupDetailModel.deleteVoiceGroupItem(commentId, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    getMvpView().deleteVoiceGroupSuccess();
                } else {
                    onError(new Exception("数据错误!"));
                }
            }
        });
    }

}
