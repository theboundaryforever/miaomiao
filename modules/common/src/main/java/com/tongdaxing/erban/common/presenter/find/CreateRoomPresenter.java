package com.tongdaxing.erban.common.presenter.find;

import com.tongdaxing.erban.common.ui.find.model.CreateRoomModel;
import com.tongdaxing.erban.common.ui.find.model.WarmAccompanyModel;
import com.tongdaxing.erban.common.ui.find.view.ICreateRoomView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.find.ICreateRoomClient;
import com.tongdaxing.xchat_core.find.bean.CreateRoomLabelInfo;
import com.tongdaxing.xchat_core.find.bean.ReopenRoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.List;

import static com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH;

/**
 * Function:
 * Author: Edward on 2019/3/18
 */
public class CreateRoomPresenter extends AbstractMvpPresenter<ICreateRoomView> {
    private CreateRoomModel createRoomModel;
    private WarmAccompanyModel warmAccompanyModel;
    private AvRoomModel mAvRoomModel;

    public CreateRoomPresenter() {
        mAvRoomModel = new AvRoomModel();
        createRoomModel = new CreateRoomModel();
        warmAccompanyModel = new WarmAccompanyModel();
    }

    public int getIndexByRoomType(int roomType) {
        switch (roomType) {
            case 3:
                return 0;// (娱乐)默认
            case 4:
                return 1;//个人直播
            case 5:
                return 2;//情侣
            case 6:
                return 3;//交友闲聊
            default:
                return -1;
        }
    }

    public int getRoomTypeByIndex(int curChoose) {
        switch (curChoose) {
            case 0:
                return 3;// (娱乐)默认
            case 1:
                return 4;//个人直播
            case 2:
                return 5;//情侣
            case 3:
                return 6;//交友闲聊
            default:
                return 3;
        }
    }

    /**
     * 获取当前用户的房间信息
     *
     * @param tagId
     * @param type
     */
    public void getCurrentUserRoomInfo(int tagId, int type, boolean isCheckNoticeFans, String title) {
        mAvRoomModel.getRoomInfoNoRealNameAuth(new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null && e != null) {
                    getMvpView().loadDataFailure(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                if (data.isSuccess()) {
                    if (data.getData() != null && data.getData().getTagType() != type) {//和已有的房间类型相同，进行二次提示
                        getMvpView().showCreateRoomConfirmDialog(tagId, type);
                    } else {
                        loadReopenRoom(tagId, type, isCheckNoticeFans, title);//更新房间类型
                    }
                } else if (getMvpView() != null) {
                    getMvpView().loadDataFailure("数据错误!");
                }
            }
        });
    }

    private void loadReopenRoom(int tagId, int type, boolean isCheckNoticeFans, String title) {
        warmAccompanyModel.getReopenRoom(tagId, type, isCheckNoticeFans, title,
                new OkHttpManager.MyCallBack<ServiceResult<ReopenRoomInfo>>() {
                    @Override
                    public void onError(Exception e) {
                        if (getMvpView() != null) {
                            getMvpView().loadDataFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void onResponse(ServiceResult<ReopenRoomInfo> response) {
                        if (response == null || getMvpView() == null) {
                            onError(new Exception());
                            return;
                        }
                        if (response.isSuccess()) {
                            CoreManager.notifyClients(ICreateRoomClient.class, ICreateRoomClient.CREATE_ROOM_SUCCEED, tagId, type);
                        } else if (response.getCode() == RESULT_FAILED_NEED_REAL_NAME_AUTH) {//要求实名认证
                            getMvpView().requestRoomInfoFailView(response.getErrorMessage());
                        } else {
                            onError(new Exception());
                        }
                    }
                });
    }

    public void loadData() {
        createRoomModel.loadLabel(new OkHttpManager.MyCallBack<ServiceResult<List<CreateRoomLabelInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null && e != null) {
                    getMvpView().setupFailView();
                }
            }

            @Override
            public void onResponse(ServiceResult<List<CreateRoomLabelInfo>> response) {
                if (response != null && response.getData() != null && response.getData().size() >= 3) {
                    if (getMvpView() != null) {
                        getMvpView().setupSuccessView(response.getData());
                    }
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    public void enterRoomFromService() {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (uid == 0) {
            return;
        }
        ResponseListener listener = new ResponseListener<ServiceResult<RoomInfo>>() {
            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                if (getMvpView() == null) {
                    return;
                }
                if (null != data) {
                    if (data.isSuccess() && data.getData() != null && data.getData().getRoomId() > 0) {
                        getMvpView().requestOwnRoomInfoSuccessView(data.getData());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
            }
        };
        mAvRoomModel.requestRoomInfoFromService(uid + "", listener, errorListener);
    }

    public void getRoomNoticeFansCd() {
        createRoomModel.getRoomNoticeFansCd(CoreManager.getCore(IAuthCore.class).getCurrentUid(), new OkHttpManager.MyCallBack<ServiceResult<Long>>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast("请求失败");
            }

            @Override
            public void onResponse(ServiceResult<Long> response) {
                if (response != null && response.getData() != null) {
                    if (getMvpView() != null) {
                        getMvpView().requestNoticeFansCdSuccess(response.getData());
                    }
                } else {
                    onError(new Exception());
                }
            }
        });
    }
}
