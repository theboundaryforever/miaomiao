package com.tongdaxing.erban.common.ui.makefriends;

import android.Manifest;
import android.content.Intent;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.alibaba.android.arouter.launcher.ARouter;
import com.juxiao.library_utils.DisplayUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.presenter.find.FindNewMakeFriendsPresenter;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.ui.home.fragment.HomeFragment;
import com.tongdaxing.erban.common.ui.me.audiocard.AudioCard2Activity;
import com.tongdaxing.erban.common.ui.me.user.activity.UserInfoModifyActivity;
import com.tongdaxing.erban.common.utils.NewAndOldUserEventUtils;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.audio.AudioCardMarkManager;
import com.tongdaxing.xchat_core.im.custom.bean.FriendsBroadcastAttachment;
import com.tongdaxing.xchat_core.makefriedns.FindBroadCastChatRoomController;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 发现-交友页
 * <p>
 * Created by zhangjian on 2019/6/13.
 */
@CreatePresenter(FindNewMakeFriendsPresenter.class)
public class FindNewMakeFriendsFragment extends BaseMvpFragment<IFindNewMakeFriendsView, FindNewMakeFriendsPresenter> implements IFindNewMakeFriendsView {
    public static final String TAG = "FindNewMakeFriendsFragment";
    FrameLayout findNewMakeFriendsInvite;
    FrameLayout findNewMakeFriendsChatHall;
    FrameLayout findNewMakeFriendsAudioMatch;
    FrameLayout findNewMakeFriendsAudioCard;
    @BindView(R2.id.find_new_make_friends_broadcast_list)
    RecyclerView findNewMakeFriendsBroadcastList;
    private HomeFragment.OnScrollChangedListener onScrollChangedListener;
    private FindFriendsBroadcastAdapter adapter;
    private List<FriendsBroadcastAttachment> broadcastBeans;
    private View listHeadView;

    private FindBroadCastChatRoomController mFindBroadCastChatRoomController;
    private LinearLayoutManager linearLayoutManager;
    private View emptyView;
    private Handler mHandler = new Handler();

    @Override
    public int getRootLayoutId() {
        return R.layout.find_new_make_friends_fragment;
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        broadcastBeans = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(mContext);
        findNewMakeFriendsBroadcastList.setLayoutManager(linearLayoutManager);
        adapter = new FindFriendsBroadcastAdapter(R.layout.find_friends_broadcast_item, broadcastBeans);
        findNewMakeFriendsBroadcastList.setAdapter(adapter);
        findNewMakeFriendsBroadcastList.setFocusable(false);
        findNewMakeFriendsBroadcastList.setHasFixedSize(true);
        findNewMakeFriendsBroadcastList.setNestedScrollingEnabled(false);
        findNewMakeFriendsBroadcastList.getViewTreeObserver().addOnScrollChangedListener(() -> {
            if (onScrollChangedListener != null) {
                onScrollChangedListener.onScrollChanged(findNewMakeFriendsBroadcastList);
            }
        });
        adapter.addHeaderView(listHeadView);
        adapter.setHeaderAndEmpty(true);

        mFindBroadCastChatRoomController = new FindBroadCastChatRoomController();
//        getMvpPresenter().requestMsgList(Constants.PAGE_START);

        ScaleAndFadeItemAnimator animator = new ScaleAndFadeItemAnimator();
        animator.setAddDuration(120);
//        DefaultItemAnimator animator = new DefaultItemAnimator();
        findNewMakeFriendsBroadcastList.setItemAnimator(animator);

        emptyView = getEmptyView(findNewMakeFriendsBroadcastList, getString(R.string.no_list_data));
        emptyView.setOnClickListener(getLoadListener());
    }

    public void setOnScrollChangedListener(HomeFragment.OnScrollChangedListener onScrollChangedListener) {
        this.onScrollChangedListener = onScrollChangedListener;
    }

    @Override
    protected void onLazyLoadData() {
        boolean isEnterRoom = FindBroadCastChatRoomController.mIsEnterRoom;
        if (!isEnterRoom) {
            enterNeteaseRoom();
        }
    }

    @Override
    public void setupView(List<FriendsBroadcastAttachment> broadcastBeans) {
        this.broadcastBeans.addAll(broadcastBeans);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addAttachment(FriendsBroadcastAttachment attachment) {
        L.debug(TAG, "addAttachment = %s", attachment);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (broadcastBeans.size() > 0) {
                    attachment.setBlueBg(!broadcastBeans.get(0).isBlueBg());//新加进来的item永远跟第一个item的颜色不同
                }
                adapter.addData(0, attachment);
                if (broadcastBeans.size() > FindBroadCastChatRoomController.MAX_MSG_SIZE) {
                    adapter.remove(broadcastBeans.size() - 1);//移除队尾，保证只有50条数据
                }
            }
        }, 1000);
    }

    private void enterNeteaseRoom() {
        mFindBroadCastChatRoomController.enterRoom(new FindBroadCastChatRoomController.ActionCallBack() {
            @Override
            public void success() {
                L.info(TAG, "enterNeteaseRoom() success()");
            }

            @Override
            public void error(int code) {
                L.error(TAG, "enterNeteaseRoom() error() code = %d", code);
                adapter.setEmptyView(emptyView);
            }

            @Override
            public void error(Throwable exception) {
                L.error(TAG, "enterNeteaseRoom() error() exception: ", exception);
                adapter.setEmptyView(emptyView);
            }
        });
    }

    /**
     * 重新加载页面数据：通常应用于无网络切换到有网络情况，重新刷新页面
     */
    @Override
    public void onReloadData() {
        super.onReloadData();
        onLazyLoadData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFindBroadCastChatRoomController.leaveRoom();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onFindViews() {
        listHeadView = View.inflate(mContext, R.layout.find_new_make_friends_headview, null);
        findNewMakeFriendsInvite = listHeadView.findViewById(R.id.find_new_make_friends_invite);
        findNewMakeFriendsChatHall = listHeadView.findViewById(R.id.find_new_make_friends_chat_hall);
        findNewMakeFriendsAudioMatch = listHeadView.findViewById(R.id.find_new_make_friends_audio_match);
        findNewMakeFriendsAudioCard = listHeadView.findViewById(R.id.find_new_make_friends_audio_card);
        findNewMakeFriendsInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewAndOldUserEventUtils.onCommonTypeEvent(mContext, UmengEventId.getFindInvite());
                ARouter.getInstance().build(CommonRoutePath.INVITE_AWARD_ACTIVITY_ROUTE_PATH).navigation();
            }
        });
        findNewMakeFriendsChatHall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewAndOldUserEventUtils.onCommonTypeEvent(mContext, UmengEventId.getFindSquare());
                ARouter.getInstance().build(CommonRoutePath.CHAT_HALL_ACTIVITY_ROUTE_PATH).navigation();
                //手动发送寻友广播消息 测试用
//                FriendsBroadcastAttachment attachment = new FriendsBroadcastAttachment(56, 56);
//                attachment.setBlueBg(true);
//                attachment.setAvatar("https://pic.miaomiaofm.com/FtYuLhIt5J8xiqL3nw4ZALLcVclg?imageslim");
//                attachment.setUserNick("宇宙无敌");
//                attachment.setRoomTag("听歌");
//                attachment.setContent("宇宙无敌的房间");
//                attachment.setRoomUid(100665);
//                ChatRoomMessage chatRoomMessage = ChatRoomMessageBuilder.createChatRoomCustomMessage("55761209", attachment);
//                CoreUtils.send(new BroadcastMsgEvent.OnReceiveNewMsg(chatRoomMessage));
            }
        });
        findNewMakeFriendsAudioMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewAndOldUserEventUtils.onCommonTypeEvent(mContext, UmengEventId.getMatch());
                ARouter.getInstance().build(CommonRoutePath.AUDIO_MATCH_ACTIVITY_ROUTE_PATH).navigation();
            }
        });
        findNewMakeFriendsAudioCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                NewAndOldUserEventUtils.onCommonTypeEvent(mContext, UmengEventId.getFindSound());
                checkPermission(() -> {
                            Intent intent = new Intent(getActivity(), AudioCard2Activity.class);
                            intent.putExtra(AudioCard2Activity.AUDIO_FILE_URL, userInfo != null ? userInfo.getUserVoice() : null);
                            intent.putExtra(AudioCard2Activity.AUDIO_LENGTH, userInfo != null ? userInfo.getVoiceDura() : 0);
                            startActivityForResult(intent, UserInfoModifyActivity.Method.AUDIO);
                            AudioCardMarkManager.getInstance(getActivity()).clearMark();
                        }, R.string.ask_again,
                        Manifest.permission.RECORD_AUDIO);
            }
        });

        int screenWidth = DisplayUtils.getScreenWidth(mContext);
        int imgWidthDp = 375;
        float imgHeightDp = 226f;
        float topImgHeight = screenWidth * imgHeightDp / imgWidthDp;
        ConstraintLayout.LayoutParams inviteLayoutParams = (ConstraintLayout.LayoutParams) findNewMakeFriendsInvite.getLayoutParams();
        inviteLayoutParams.setMargins(((int) (screenWidth * 30f / imgWidthDp)),
                ((int) (topImgHeight * 50 / imgHeightDp)), 0, 0);
        findNewMakeFriendsInvite.setLayoutParams(inviteLayoutParams);
        ConstraintLayout.LayoutParams chatHallLayoutParams = (ConstraintLayout.LayoutParams) findNewMakeFriendsChatHall.getLayoutParams();
        chatHallLayoutParams.setMargins(((int) (screenWidth * 119f / imgWidthDp)),
                ((int) (topImgHeight * 25 / imgHeightDp)), 0, 0);
        findNewMakeFriendsChatHall.setLayoutParams(chatHallLayoutParams);
        ConstraintLayout.LayoutParams audioMatchLayoutParams = (ConstraintLayout.LayoutParams) findNewMakeFriendsAudioMatch.getLayoutParams();
        audioMatchLayoutParams.setMargins(((int) (screenWidth * 205f / imgWidthDp)),
                ((int) (topImgHeight * 54 / imgHeightDp)), 0, 0);
        findNewMakeFriendsAudioMatch.setLayoutParams(audioMatchLayoutParams);
        ConstraintLayout.LayoutParams cardLayoutParams = (ConstraintLayout.LayoutParams) findNewMakeFriendsAudioCard.getLayoutParams();
        cardLayoutParams.setMargins(((int) (screenWidth * 285f / imgWidthDp)),
                ((int) (topImgHeight * 28 / imgHeightDp)), 0, 0);
        findNewMakeFriendsAudioCard.setLayoutParams(cardLayoutParams);
    }

    //--------------------------寻友广播时长统计埋点操作--------------------------

    /**
     * 是否已经开始统计
     */
    private boolean hasStarted;
    /**
     * 是否可见
     */
    private boolean mVisiable;


    @Override
    public void onResume() {
        L.debug(TAG, this.toString() + "----onResume");
        super.onResume();
        //若当前界面可见,调用友盟开启跳转统计
        if (mVisiable) {
            UmengEventUtil.getInstance().startStatFindBroadCast(FindNewMakeFriendsFragment.class);
        }
    }

    @Override
    public void onPause() {
        L.debug(TAG, this.toString() + "----onPause");
        super.onPause();
        //若当前界面可见,调用友盟结束跳转统计
        if (mVisiable) {
            hasStarted = false;
            UmengEventUtil.getInstance().endStatFindBroadCast(FindNewMakeFriendsFragment.class);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        L.debug(TAG, this.toString() + "----setUserVisibleHint----" + isVisibleToUser);
        mVisiable = isVisibleToUser;
        if (isVisibleToUser) {
            hasStarted = true;
            UmengEventUtil.getInstance().startStatFindBroadCast(FindNewMakeFriendsFragment.class);
        } else {
            if (hasStarted) {
                hasStarted = false;
                UmengEventUtil.getInstance().endStatFindBroadCast(FindNewMakeFriendsFragment.class);
            }
        }
    }
    //--------------------------寻友广播时长统计埋点操作--------------------------
}
