package com.tongdaxing.erban.common.ui.find.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.juxiao.library_ui.widget.AppToolBar;
import com.juxiao.library_ui.widget.NestedScrollSwipeRefreshLayout;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.base.popupwindow.RelativePopupWindow;
import com.tongdaxing.erban.common.presenter.find.VoiceGroupDetailPresenter;
import com.tongdaxing.erban.common.ui.find.adapter.VoiceGroupDetailAdapter;
import com.tongdaxing.erban.common.ui.find.adapter.VoiceGroupPictureAdapter;
import com.tongdaxing.erban.common.ui.find.event.VoiceGroupDeletedEvent;
import com.tongdaxing.erban.common.ui.find.view.IVoiceGroupDetailView;
import com.tongdaxing.erban.common.ui.find.widget.VoiceMorePopupWindow;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.ShowPhotoActivity;
import com.tongdaxing.erban.common.utils.HttpUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.NewAndOldUserEventUtils;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.find.IVoiceGroupClient;
import com.tongdaxing.xchat_core.find.bean.PictureListInfo;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupDetailInfo;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.PraiseEvent;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Function:动态列表详情页
 * Author: Edward on 2019/3/12
 */
@CreatePresenter(VoiceGroupDetailPresenter.class)
public class VoiceGroupDetailActivity extends BaseMvpActivity<IVoiceGroupDetailView, VoiceGroupDetailPresenter> implements IVoiceGroupDetailView {
    public static final String OPERATION_TYPE_OPEN_KEYBOARD_KEY = "operation_type_open_keyboard";
    public static final int OPERATION_TYPE_OPEN_KEYBOARD_VALUE = 1;
    public static final String VOICE_GROUP_LIST_INDEX = "voice_group_list_index";
    private static final String COMMENTID = "commentId";
    private final int RC_PHOTO_PREVIEW = 2;
    @BindView(R2.id.app_tool_bar)
    AppToolBar appToolBar;
    @BindView(R2.id.btn_publish_comment)
    Button btnPublishComment;
    @BindView(R2.id.et_content)
    EditText etContent;
    @BindView(R2.id.swipe_refresh)
    NestedScrollSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R2.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R2.id.fl)
    FrameLayout frameLayout;
    private VoiceGroupDetailAdapter groupDetailAdapter;
    private int index;//在声圈动态列表中的索引
    private int operationType = 0;//0表示不进行任何操作
    private VoiceGroupDetailInfo info;
    private long curClickUserId;
    private VoiceGroupInfo voiceGroupInfo;
    private long replyUid = 0;

    /**
     * 从动态列表中点击评论的入口
     *
     * @param context
     * @param voiceGroupInfo
     * @param operationType  1表示打开键盘
     */
    public static void startOpenKeyboard(Context context, VoiceGroupInfo voiceGroupInfo, int operationType, int index) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(COMMENTID, voiceGroupInfo);
        bundle.putInt(OPERATION_TYPE_OPEN_KEYBOARD_KEY, operationType);
        bundle.putInt(VOICE_GROUP_LIST_INDEX, index);
        Intent intent = new Intent(context, VoiceGroupDetailActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public static void start(Context context, VoiceGroupInfo voiceGroupInfo, int index) {
        NewAndOldUserEventUtils.onCommonTypeEvent(context, UmengEventId.getMomentDetail());
        startOpenKeyboard(context, voiceGroupInfo, 0, index);
    }

    public static void start(Activity activity, VoiceGroupInfo voiceGroupInfo) {
        start(activity, voiceGroupInfo, -1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_group_detail);
        ButterKnife.bind(this);
        initTitleBarNew();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            voiceGroupInfo = bundle.getParcelable(COMMENTID);
            operationType = bundle.getInt(OPERATION_TYPE_OPEN_KEYBOARD_KEY);//1表示弹出键盘
            index = bundle.getInt(VOICE_GROUP_LIST_INDEX, -1);
            if (voiceGroupInfo != null) {
                groupDetailAdapter = new VoiceGroupDetailAdapter(R.layout.adapter_voice_group_comment_item);
                groupDetailAdapter.setCommentLikeCallback(id -> {
                    getMvpPresenter().commentLike(id);
                });
                recyclerView.setFocusable(false);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(groupDetailAdapter);

                setCallback();
                onReloadData();
            }
        }
        CoreUtils.register(this);
    }

    @Override
    public void setupFailView(boolean isRefresh) {
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            showNetworkErr();
        } else {
            groupDetailAdapter.loadMoreFail();
        }
    }

    @Override
    public void deleteVoiceGroupSuccess() {
        SingleToastUtil.showToast("删除成功!");
        //发出删除通知
        if (voiceGroupInfo.getId() != 0 && index != -1) {
            EventBus.getDefault().post(new VoiceGroupDeletedEvent(voiceGroupInfo.getId(), index));
        }
        finish();
    }

    @Override
    public void insertHead(int page, VoiceGroupDetailInfo voiceGroupDetailInfo) {
        if (voiceGroupDetailInfo == null) {
            showNoData();
            return;
        }
        this.info = voiceGroupDetailInfo;
        if (page == Constants.PAGE_START) {
            LinearLayout linearLayout = groupDetailAdapter.getHeaderLayout();
            if (linearLayout == null) {
                View view = LayoutInflater.from(this).inflate(R.layout.adapter_voice_group_item, null);
                initHeadData(view, voiceGroupDetailInfo);
                groupDetailAdapter.addHeaderView(view, 0);
            } else {
                View view = linearLayout.getChildAt(0);
                initHeadData(view, voiceGroupDetailInfo);
            }
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        if (likedUid == curClickUserId && info != null) {
            info.setFans(true);
            insertHead(1, info);
            sendRefreshInform();
        }
    }

    /**
     * 发送刷新通知
     */
    private void sendRefreshInform() {
        CoreManager.notifyClients(IVoiceGroupClient.class, IVoiceGroupClient.PUBLISH_VOICE_GROUP_REFRESH_LIST);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFail(String error) {
        toast(error);
    }

    private void initHeadData(View view, VoiceGroupDetailInfo info) {
        view.setOnClickListener(new View.OnClickListener() {//从回复他人变为回复楼主
            @Override
            public void onClick(View v) {
                if (replyUid != 0) {
                    commentToLz();
                }
            }
        });
        ImageView ivUserHead = view.findViewById(R.id.iv_user_head);
        ImageLoadUtils.loadCircleImage(this, info.getAvatar(), ivUserHead, R.drawable.ic_no_avatar);
        ivUserHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewUserInfoActivity.start(VoiceGroupDetailActivity.this, info.getUid());
            }
        });

        ImageView voiceChat = view.findViewById(R.id.voice_iv_chat);
        ImageView voiceMore = view.findViewById(R.id.voice_iv_more);
        if (AvRoomDataManager.get().isSelf(info.getUid())) {
            voiceChat.setVisibility(View.GONE);
            voiceMore.setVisibility(View.GONE);
        } else {
            voiceChat.setVisibility(View.VISIBLE);
            voiceMore.setVisibility(View.VISIBLE);
            voiceChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NewAndOldUserEventUtils.onCommonTypeEvent(VoiceGroupDetailActivity.this, UmengEventId.getMomentMsg());
                    int uid = info.getUid();
                    HttpUtil.checkUserIsDisturb(VoiceGroupDetailActivity.this, uid);
                }
            });
            voiceMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = voiceMore.getContext();
                    VoiceMorePopupWindow voiceMorePopupWindow = new VoiceMorePopupWindow(context, info.getUid());
                    voiceMorePopupWindow.setIsAttention(info.isFans());
                    voiceMorePopupWindow.showOnAnchor(voiceMore, RelativePopupWindow.VerticalPosition.CENTER,
                            RelativePopupWindow.HorizontalPosition.LEFT);
                }
            });
        }

        ((TextView) view.findViewById(R.id.tv_name)).setText(info.getNick());
        ImageView ivGender = view.findViewById(R.id.iv_gender);
        if (info.getGender() == 1) {
            ivGender.setImageResource(R.drawable.icon_man);
        } else {
            ivGender.setImageResource(R.drawable.icon_female);
        }
        ((TextView) view.findViewById(R.id.tv_time)).setText(TimeUtils.getPostTimeString(this,
                info.getCreateDate(), true, false));
        TextView tvContent = view.findViewById(R.id.tv_content);
        tvContent.setText(info.getContext());
        tvContent.setMaxLines(Integer.MAX_VALUE);//动态详情没有行数限制
        RadioButton radioButton = view.findViewById(R.id.rb_like);
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewAndOldUserEventUtils.onCommonTypeEvent(VoiceGroupDetailActivity.this, UmengEventId.getMomentLike());
                VoiceGroupDetailActivity.this.getMvpPresenter().voiceGroupLike(info.getId());
            }
        });
        radioButton.setChecked(info.isIsLike());
        if (info.getLikeNum() > 999) {
            radioButton.setText("999+");
        } else {
            radioButton.setText(String.valueOf(info.getLikeNum()));
        }
        TextView tvComment = view.findViewById(R.id.tv_comment);
        if (info.getPlayerNum() > 999) {
            tvComment.setText("999+");
        } else {
            tvComment.setText(String.valueOf(info.getPlayerNum()));
        }
        tvComment.setOnClickListener(v -> {
            showSoftInputFromWindow(etContent);
        });
        setPictureLayout(view, convertBean(info));
    }

    private VoiceGroupInfo convertBean(VoiceGroupDetailInfo info) {
        VoiceGroupInfo voiceGroupInfo = new VoiceGroupInfo();
        voiceGroupInfo.setPicList(info.getPicList());
        voiceGroupInfo.setLayoutType(info.getLayoutType());
        return voiceGroupInfo;
    }

    /**
     * 设置图片网格布局
     *
     * @param info
     */
    private void setPictureLayout(View viewLayout, VoiceGroupInfo info) {
        RecyclerView recyclerView = viewLayout.findViewById(R.id.recycler_view);
        List<PictureListInfo> picList = info.getPicList();
        if (picList != null && picList.size() > 0) {
            ArrayList<UserPhoto> remoteUrlList = new ArrayList<>();//查看大图集合列表
            for (int i = 0; i < picList.size(); i++) {
                picList.get(i).setLayoutType(info.getLayoutType());
                picList.get(i).setPicCount(picList.size());
                UserPhoto userPhoto = new UserPhoto();
                userPhoto.setPhotoUrl(picList.get(i).getUrl());
                userPhoto.setPid(i);
                remoteUrlList.add(userPhoto);
            }
            recyclerView.setVisibility(View.VISIBLE);
            VoiceGroupPictureAdapter adapter = new VoiceGroupPictureAdapter(R.layout.adapter_voice_group_picture_item);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 6);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);
            if (picList.size() > 9) {
                adapter.setNewData(picList.subList(0, 9));
            } else {
                adapter.setNewData(picList);
            }
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (picList.size() == 1 && position == 0) {
                        return 6;
                    } else if (picList.size() == 2 && position == 0) {
                        return 3;
                    } else if (picList.size() == 2 && position == 1) {
                        return 3;
                    } else {
                        return 2;
                    }
                }
            });

            adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter1, View view, int position) {
                    NewAndOldUserEventUtils.onCommonTypeEvent(VoiceGroupDetailActivity.this, UmengEventId.getMomentPics());
                    ShowPhotoActivity.start(VoiceGroupDetailActivity.this, position, remoteUrlList);
                }
            });

        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void refreshHeadAdapterLikeStatus(int commentId) {
        if (info != null) {
            info.setIsLike(!info.isIsLike());
            if (info.isIsLike()) {
                info.setLikeNum(info.getLikeNum() + 1);
            } else {
                if (info.getLikeNum() < 0) {
                    info.setLikeNum(0);
                } else {
                    info.setLikeNum(info.getLikeNum() - 1);
                }
            }
            insertHead(1, info);
            sendRefreshInform();
        }
    }

    @Override
    public void refreshAdapterLikeStatus(int id) {
        if (groupDetailAdapter != null) {
            List<VoiceGroupDetailInfo.PlayerListBean> list = groupDetailAdapter.getData();
            for (int i = 0; i < list.size(); i++) {
                VoiceGroupDetailInfo.PlayerListBean bean = list.get(i);
                if (bean.getId() == id) {
                    bean.setIsLike(!bean.isIsLike());
                    if (bean.isIsLike()) {
                        bean.setLikeNum(bean.getLikeNum() + 1);
                    } else {
                        if (bean.getLikeNum() < 0) {
                            bean.setLikeNum(0);
                        } else {
                            bean.setLikeNum(bean.getLikeNum() - 1);
                        }
                    }
                    list.set(i, bean);
                    groupDetailAdapter.notifyDataSetChanged();
                    sendRefreshInform();
                    return;
                }
            }
        }
    }

    @Override
    public void setupSuccessView(List<VoiceGroupDetailInfo.PlayerListBean> voiceGroupInfoList, boolean isRefresh) {
        hideStatus();
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            if (ListUtils.isListEmpty(voiceGroupInfoList)) {
                voiceGroupInfoList = new ArrayList<>();
            }
            groupDetailAdapter.setNewData(voiceGroupInfoList);
        } else {
            groupDetailAdapter.loadMoreComplete();
            if (!ListUtils.isListEmpty(voiceGroupInfoList)) {
                groupDetailAdapter.addData(voiceGroupInfoList);
            }
        }
        //不够10个的话，不显示加载更多
        if (voiceGroupInfoList.size() < Constants.PAGE_SIZE) {
            groupDetailAdapter.setEnableLoadMore(false);
        }

        if (operationType == 1) {
            showSoftInputFromWindow(etContent);//默认弹出键盘
            operationType = 0;//关闭默认操作
        }
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        showLoading();
        getMvpPresenter().refreshData(String.valueOf(voiceGroupInfo.getId()));
    }

    private void showDetailReportOptionItems() {
        ButtonItem buttonItem;
        if (info != null && CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid() == info.getUid()) {
            buttonItem = new ButtonItem("删除", () -> {
                getMvpPresenter().deleteVoiceGroupItem(String.valueOf(info.getId()));
            });
        } else {
            buttonItem = new ButtonItem("举报", () -> {
                if (info != null) {
                    showReportItems(String.valueOf(info.getId()), "");
                } else {
                    SingleToastUtil.showToast("数据错误!");
                }
            });
        }
        List<ButtonItem> buttonItemList = new ArrayList<>();
        buttonItemList.add(buttonItem);
        getDialogManager().showCommonPopupDialog(buttonItemList, "取消", false);
    }

    private void initTitleBarNew() {
        appToolBar.setOnLeftImgBtnClickListener(this::finish);
        appToolBar.getTvTitle().setText("动态详情");
        appToolBar.setRightBtnImage(R.drawable.user_info_menu_black);
        appToolBar.setOnRightImgBtnClickListener(this::showDetailReportOptionItems);
        TextPaint tp = appToolBar.getTvTitle().getPaint();
        tp.setFakeBoldText(true);
    }

    private void showReportItems(String momentId, String playerId) {
        ButtonItem buttonItem1 = new ButtonItem("色情", () -> {
            getMvpPresenter().reportItems(momentId, playerId, "色情");
        });
        ButtonItem buttonItem2 = new ButtonItem("不良信息", () -> {
            getMvpPresenter().reportItems(momentId, playerId, "不良信息");
        });
        ButtonItem buttonItem3 = new ButtonItem("广告", () -> {
            getMvpPresenter().reportItems(momentId, playerId, "广告");
        });
        List<ButtonItem> buttonItemList = new ArrayList<>();
        buttonItemList.add(buttonItem1);
        buttonItemList.add(buttonItem2);
        buttonItemList.add(buttonItem3);
        if (getDialogManager() != null) {
            getDialogManager().showCommonPopupDialog(buttonItemList, "取消");
        }
    }

    public void showSoftInputFromWindow(EditText editText) {
        if (editText != null) {
            editText.setFocusable(true);
            editText.setFocusableInTouchMode(true);
            editText.requestFocus();
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        InputMethodManager inputManager = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (inputManager != null) {
                            inputManager.showSoftInput(editText, 0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }, 300);
        }
    }

    private void showItems(VoiceGroupDetailInfo.PlayerListBean item) {
        ButtonItem reportItem = new ButtonItem("举报", () -> showReportItems(String.valueOf(item.getMomentId()), String.valueOf(item.getId())));
        ButtonItem replyItem = new ButtonItem("回复 " + item.getNick(), () -> {
            etContent.setHint("回复 " + item.getNick());
            replyUid = item.getUid();
            showSoftInputFromWindow(etContent);
        });
        List<ButtonItem> buttonItemList = new ArrayList<>();
        buttonItemList.add(reportItem);
        buttonItemList.add(replyItem);
        getDialogManager().showCommonPopupDialog(buttonItemList, "取消", false);
    }

    private void setCallback() {
        groupDetailAdapter.setOnItemClickListener((adapter, view, position) -> {
            VoiceGroupDetailInfo.PlayerListBean item = ((VoiceGroupDetailAdapter) adapter).getItem(position);
            if (item != null && item.getUid() != CoreManager.getCore(IAuthCore.class).getCurrentUid()) {//不能对自己进行操作
                showItems(item);
            }
        });
        btnPublishComment.setOnClickListener(v -> {
            if (voiceGroupInfo == null) {
                return;
            }
            String content = etContent.getText().toString();
            if (TextUtils.isEmpty(content)) {
                toast("评论内容不能为空!");
                return;
            }
            if (voiceGroupInfo.getId() <= 0) {
                toast(getString(R.string.data_exception));
                return;
            }
            String replyUidStr = "";
            if (replyUid > 0) {
                replyUidStr = String.valueOf(replyUid);
            }
            getDialogManager().showProgressDialog(this, getString(R.string.waiting_text), true);
            getMvpPresenter().sendComment(content, String.valueOf(voiceGroupInfo.getId()), replyUidStr);
        });

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getMvpPresenter().refreshData(String.valueOf(voiceGroupInfo.getId()));
        });

        groupDetailAdapter.setOnLoadMoreListener(() -> {
            getMvpPresenter().loadMoreData(String.valueOf(voiceGroupInfo.getId()));
        }, recyclerView);
    }

    /**
     * 回复楼主
     */
    public void commentToLz() {
        replyUid = 0;//设置为直接发表评论
        etContent.setHint("请输入你的评论");
        etContent.setText("");
    }

    @Override
    public void publishCommentSucceed() {
        NewAndOldUserEventUtils.onCommonTypeEvent(VoiceGroupDetailActivity.this, UmengEventId.getMomentComment());
        commentToLz();
        getDialogManager().dismissDialog();
        getMvpPresenter().refreshData(String.valueOf(voiceGroupInfo.getId()));
        sendRefreshInform();
    }

    @Override
    public void publishCommentFailure(String errorStr) {
        getDialogManager().dismissDialog();
        toast(errorStr);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionSuccess(PraiseEvent.OnAttentionSuccess success) {
        long uid = success.getUid();
        refreshAdapterAttentionStatus(uid, success.isAttention());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionCancel(PraiseEvent.OnAttentionCancel cancel) {
        refreshAdapterAttentionStatus(cancel.getUid(), false);
    }

    public void refreshAdapterAttentionStatus(long commentId, boolean isAttention) {
        if (info != null) {
            if (info.getUid() == commentId) {
                info.setFans(isAttention);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CoreUtils.unregister(this);
    }
}
