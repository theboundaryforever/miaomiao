package com.tongdaxing.erban.common.ui.me.shopping.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.fragment.BaseListFragment;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.me.shopping.adapter.FriendListGiftAdapter;
import com.tongdaxing.erban.common.ui.me.shopping.adapter.GiveGoodsAdapter;
import com.tongdaxing.erban.common.ui.me.shopping.fragment.FriendListGiftFragment;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GiveGoodsActivity extends BaseActivity {

    @BindView(R2.id.title_bar)
    TitleBar titleBar;
    @BindView(R2.id.give_goods_indicator)
    MagicIndicator giveGoodsIndicator;
    @BindView(R2.id.vp_give_goods)
    ViewPager vpGiveGoods;
    private ArrayList<Fragment> fragments;
    private String carName;
    private String goodsId;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_give_goods);
        ButterKnife.bind(this);
        carName = getIntent().getStringExtra("carName");
        goodsId = getIntent().getStringExtra("goodsId");
        type = getIntent().getIntExtra("type", 0);


        initTitleBar("赠送");
        ButterKnife.bind(this);

        fragments = new ArrayList<>();

        BaseListFragment baseListFragment = new BaseListFragment();
        GiveGoodsAdapter shareFansAdapter = new GiveGoodsAdapter(new ArrayList<>());
        shareFansAdapter.itemAction = new GiveGoodsAdapter.ItemAction() {
            @Override
            public void itemClickAction(String uid, String userName) {
                showEnsureDialog(uid, userName);
            }
        };
        baseListFragment.setEmptyStr("没有关注的用户");
        baseListFragment.pageNoParmasName = "pageNo";
        baseListFragment.setShortUrl(UriProvider.getAllFans());
        baseListFragment.setOtherParams(Json.parse("uid:" + CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        baseListFragment.setAdapter(shareFansAdapter);
        baseListFragment.setDataFilter(new BaseListFragment.IDataFilter() {
            @Override
            public List<Json> dataFilter(Json json) {
                return json.jlist("data");
            }
        });


        FriendListGiftFragment friendListGiftFragment = new FriendListGiftFragment();
        friendListGiftFragment.iGiveAction = new FriendListGiftAdapter.IGiveAction() {
            @Override
            public void onGiveEvent(String uid, String userName) {
                showEnsureDialog(uid, userName);
            }
        };
        fragments.add(friendListGiftFragment);
        fragments.add(baseListFragment);
        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }
        };


        List<TabInfo> mTabInfoList = new ArrayList<>();
        mTabInfoList.add(new TabInfo(1, "好友"));
        mTabInfoList.add(new TabInfo(1, "关注"));
        CommonNavigator commonNavigator = new CommonNavigator(this);
        CommonMagicIndicatorAdapter magicIndicatorAdapter = new CommonMagicIndicatorAdapter(this,
                mTabInfoList, UIUtil.dip2px(this, 4));
        magicIndicatorAdapter.setOnItemSelectListener(new CommonMagicIndicatorAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                vpGiveGoods.setCurrentItem(position);
            }
        });
        commonNavigator.setAdapter(magicIndicatorAdapter);
        commonNavigator.setAdjustMode(true);

        giveGoodsIndicator.setNavigator(commonNavigator);
        vpGiveGoods.setAdapter(fragmentPagerAdapter);
        ViewPagerHelper.bind(giveGoodsIndicator, vpGiveGoods);
    }

    private void showEnsureDialog(String uid, String userName) {
        getDialogManager().showOkCancelDialog(
                "确认购买“" + carName + "”并赠送给" + userName + "？", true,
                new DialogManager.AbsOkDialogListener() {
                    @Override
                    public void onOk() {
                        requestGift(uid);
                    }
                });
    }

    private void requestGift(String uid) {

        if (TextUtils.isEmpty(uid) || TextUtils.isEmpty(goodsId)) {
            toast("参数异常");
            return;
        }


        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("targetUid", uid);

        String shortUrl;
        if (type == DressUpMallActivity.DRESS_CAR) {
            shortUrl = UriProvider.requestGiftCar();
            param.put("carId", goodsId);
        } else {
            shortUrl = UriProvider.requestHeadWear();
            param.put("headwearId", goodsId);
        }
        OkHttpManager.getInstance().doPostRequest(shortUrl, null, param, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                toast("网络异常");
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                if (response.isSuccess()) {
                    toast("赠送成功");
                    finish();
                } else {
                    toast(response.getMessage() == null ? "网络异常" : response.getMessage());
                }
            }
        });
    }

}
