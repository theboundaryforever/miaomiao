package com.tongdaxing.erban.common.ui.me.user.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.ui.me.user.activity.ShowPhotoActivity;
import com.tongdaxing.erban.common.ui.widget.UserInfoAudioIdentifyView;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.audio.AudioCardMarkManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.find.bean.DatumPageInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomStatus;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_core.utils.StarUtils;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.uuzuche.lib_zxing.DisplayUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Function:
 * Author: Edward on 2019/3/30
 */
public class DatumPageAdapter extends BaseMultiItemQuickAdapter<DatumPageInfo, BaseViewHolder> {
    public static final int DATUM_PAGE_ENTER_ROOM = 0;//房间
    public static final int DATUM_PAGE_IDENTIFY_CARD = 1;//声鉴卡
    public static final int DATUM_PAGE_ENTER_ROOM_AND_IDENTIFY_CARD = 10;//房间和声鉴卡
    public static final int DATUM_PAGE_PERSONAL_INFO = 2;//个人信息
    public static final int DATUM_PAGE_ALBUM = 3;//相册

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public DatumPageAdapter(List<DatumPageInfo> data) {
        super(data);
        addItemType(DATUM_PAGE_ENTER_ROOM, R.layout.adapter_datum_page_room_and_identify_card_item);
        addItemType(DATUM_PAGE_IDENTIFY_CARD, R.layout.adapter_datum_page_room_and_identify_card_item);
        addItemType(DATUM_PAGE_ENTER_ROOM_AND_IDENTIFY_CARD, R.layout.adapter_datum_page_room_and_identify_card_item);
        addItemType(DATUM_PAGE_PERSONAL_INFO, R.layout.adapter_datum_page_personal_info_item);
        addItemType(DATUM_PAGE_ALBUM, R.layout.adapter_datum_page_album_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, DatumPageInfo item) {
        switch (item.getItemType()) {
            case DATUM_PAGE_ENTER_ROOM:
                setEnterRoomView(helper, item.getUserInfo(), item.getRoomStatus());
                break;
            case DATUM_PAGE_IDENTIFY_CARD:
                setIdentifyCardView(helper, item.getUserInfo());
                break;
            case DATUM_PAGE_ENTER_ROOM_AND_IDENTIFY_CARD:
                setEnterRoomDataAndIdentifyCardData(helper, item.getUserInfo(), item.getRoomStatus());
                break;
            case DATUM_PAGE_PERSONAL_INFO:
                setPersonalInfoData(helper, item.getUserInfo());
                break;
            case DATUM_PAGE_ALBUM:
                setAlbumData(helper, item.getUserInfo());
                break;
            default:
//                setEnterRoomView(helper, item.getRoomStatus());
                break;
        }
    }

    private void setEnterRoomView(BaseViewHolder helper, UserInfo userInfo, RoomStatus roomStatus) {
        helper.getView(R.id.user_info_datum_room_content).setVisibility(View.VISIBLE);
        helper.getView(R.id.user_info_audio_identify_view).setVisibility(View.GONE);
        View blankView = helper.getView(R.id.user_info_datum_blank_content);
        blankView.setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) blankView.getLayoutParams();
        layoutParams.leftMargin = 0;
        blankView.setLayoutParams(layoutParams);

        setEnterRoomData(helper, userInfo, roomStatus);
    }

    private void setEnterRoomData(BaseViewHolder helper, UserInfo userInfo, RoomStatus roomStatus) {
        TextView tvCustomLabel = helper.getView(R.id.user_info_datum_tv_custom_label);
        String roomTag = roomStatus.getRoomTag();
        if (TextUtils.isEmpty(roomTag)) {
            roomTag = "";
            tvCustomLabel.setVisibility(View.GONE);
        }
        tvCustomLabel.setText(roomTag);
        tvCustomLabel.setVisibility(View.VISIBLE);

        ImageView ivPlay = helper.getView(R.id.iv_play);
        GlideApp.with(mContext)
                .load(R.drawable.icon_music_play_white)
                .into(ivPlay);
        helper.setText(R.id.user_info_datum_tv_online_num, String.valueOf(roomStatus.getOnlineNum()));

        helper.setText(R.id.user_info_datum_tv_title, roomStatus.getTitle());
        ImageView ivUserHead = helper.getView(R.id.user_info_datum_iv_cover);
        ImageLoadUtils.loadImage(mContext, roomStatus.getAvatar(), ivUserHead, R.drawable.default_cover);
        View enterRoomView = helper.getView(R.id.user_info_datum_room_content);
        enterRoomView.setOnClickListener(v -> {
            if (userInfo != null) {
                CoreManager.notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GO_TO_USER_ROOM, userInfo.getUid());
            }
        });
    }

    private void setEnterRoomDataAndIdentifyCardData(BaseViewHolder helper, UserInfo userInfo, RoomStatus roomStatus) {
        helper.getView(R.id.user_info_datum_room_content).setVisibility(View.VISIBLE);
        helper.getView(R.id.user_info_audio_identify_view).setVisibility(View.VISIBLE);
        helper.getView(R.id.user_info_datum_blank_content).setVisibility(View.GONE);

        setEnterRoomData(helper, userInfo, roomStatus);
        setIdentifyCardData(helper, userInfo);
    }

    private void setIdentifyCardView(BaseViewHolder helper, UserInfo userInfo) {
        helper.getView(R.id.user_info_datum_room_content).setVisibility(View.GONE);
        helper.getView(R.id.user_info_audio_identify_view).setVisibility(View.VISIBLE);
        helper.getView(R.id.user_info_datum_blank_content).setVisibility(View.VISIBLE);
        View blankView = helper.getView(R.id.user_info_datum_blank_content);
        blankView.setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) blankView.getLayoutParams();
        layoutParams.leftMargin = DisplayUtil.dip2px(mContext, mContext.getResources().getDimension(R.dimen.dp_15));
        blankView.setLayoutParams(layoutParams);

        setIdentifyCardData(helper, userInfo);
    }

    private void setIdentifyCardData(BaseViewHolder helper, UserInfo userInfo) {
        UserInfoAudioIdentifyView view = helper.getView(R.id.user_info_audio_identify_view);
        view.setAudioDataSource(userInfo.getVoiceDura(), userInfo.getUserVoice(), userInfo.getVoiceCard().getMasterTone());
    }

    private void setPersonalInfoData(BaseViewHolder helper, UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }
        if (!TextUtils.isEmpty(userInfo.getUserDesc())) {
            helper.setText(R.id.tv_user_desc, userInfo.getUserDesc());
        } else {
            helper.setText(R.id.tv_user_desc, mContext.getResources().getString(R.string.user_info_desc_empty));
        }
        if (userInfo.getCreateTime() > 0) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM月dd日", Locale.CHINA);
            String str = simpleDateFormat.format(new Date(userInfo.getBirth()));
            helper.setText(R.id.tv_user_bir, "生日:  " + str);
        } else {
            helper.setText(R.id.tv_user_bir, "生日:  ");
        }
        String star = StarUtils.getConstellation(new Date(userInfo.getBirth()));
        if (!TextUtils.isEmpty(star)) {
            helper.setText(R.id.tv_user_constellation, "星座: " + star);
        }
    }

    private void setAlbumData(BaseViewHolder helper, UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }

        RecyclerView photoRecyclerView = helper.getView(R.id.photo_recyclerView);
        photoRecyclerView.setNestedScrollingEnabled(false);
        photoRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        NewUserPhotoAdapter photoAdapter = new NewUserPhotoAdapter(0, (DisplayUtils.getScreenWidth(mContext) -
                DisplayUtils.dip2px(mContext, 60)) / 3);
        photoRecyclerView.setAdapter(photoAdapter);
        photoRecyclerView.setFocusableInTouchMode(false);

        ArrayList<UserPhoto> photos = new ArrayList<>();
        if (null != userInfo.getPrivatePhoto()) {
            photos.addAll(userInfo.getPrivatePhoto());
        }

        if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == userInfo.getUid()) {
            CoreManager.notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_SET_RED_DOT, AudioCardMarkManager.getInstance(mContext).isMark());
            photoAdapter.setSelf(true);
        } else {
            if (photos.size() == 0) {
                if (helper.getView(R.id.tv_tip).getVisibility() == View.GONE) {
                    helper.getView(R.id.tv_tip).setVisibility(View.VISIBLE);
                }
                if (photoRecyclerView.getVisibility() == View.VISIBLE) {
                    photoRecyclerView.setVisibility(View.GONE);
                }
            } else {
                if (helper.getView(R.id.tv_tip).getVisibility() == View.VISIBLE) {
                    helper.getView(R.id.tv_tip).setVisibility(View.GONE);
                }
                if (photoRecyclerView.getVisibility() == View.GONE) {
                    photoRecyclerView.setVisibility(View.VISIBLE);
                }
            }
            CoreManager.notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_SET_RED_DOT, false);
            photoAdapter.setSelf(false);
        }
        photoAdapter.setPhotoUrls(photos);
        if (!ListUtils.isListEmpty(photos) && photos.size() >= 9) {
            helper.getView(R.id.tv_user_album_more).setVisibility(View.VISIBLE);
            helper.getView(R.id.tv_user_album_more).setOnClickListener(v -> {
                UIHelper.showModifyPhotosAndPhotos((Activity) mContext, userInfo.getUid(), AvRoomDataManager.get().isSelf(userInfo.getUid()));
            });
        }
        photoAdapter.setImageClickListener(new NewUserPhotoAdapter.ImageClickListener() {
            @Override
            public void click(int position, UserPhoto userPhoto) {
                Json photoDataJson = getPhotoDataJson(photos);
                if (photoDataJson == null)
                    return;
                Intent intent = new Intent(mContext, ShowPhotoActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("photoJsonData", photoDataJson.toString());
                mContext.startActivity(intent);

            }

            @Override
            public void addClick() {
                UIHelper.showModifyPhotosAct((Activity) mContext, userInfo.getUid());
            }
        });
    }

    private Json getPhotoDataJson(ArrayList<UserPhoto> photos) {
        if (photos == null)
            return null;
        Json json = new Json();
        for (int i = 0; i < photos.size(); i++) {
            UserPhoto userPhoto = photos.get(i);
            Json j = new Json();
            j.set("pid", userPhoto.getPid());
            j.set("photoUrl", userPhoto.getPhotoUrl());
            json.set(i + "", j.toString());
        }
        return json;
    }
}
