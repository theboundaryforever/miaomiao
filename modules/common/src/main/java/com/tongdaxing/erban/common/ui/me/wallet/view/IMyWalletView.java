package com.tongdaxing.erban.common.ui.me.wallet.view;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public interface IMyWalletView extends IPayView {

    public void handleClickByViewId(int id);
}
