package com.tongdaxing.erban.common.ui.widget.itemdecotion;

import android.view.View;


public interface PowerGroupListener {

    String getGroupName(int position);

    View getGroupView(int position);
}
