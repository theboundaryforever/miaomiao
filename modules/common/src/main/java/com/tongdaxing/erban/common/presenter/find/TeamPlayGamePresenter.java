package com.tongdaxing.erban.common.presenter.find;

import com.tongdaxing.erban.common.ui.find.model.TeamPlayGameModel;
import com.tongdaxing.erban.common.ui.find.view.ITeamPlayGameView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.find.bean.ConvertPlayGameInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class TeamPlayGamePresenter extends AbstractMvpPresenter<ITeamPlayGameView> {
    private int currPage = Constants.PAGE_START;
    private TeamPlayGameModel teamPlayGameModel;

    public TeamPlayGamePresenter() {
        teamPlayGameModel = new TeamPlayGameModel();
    }

    public void refreshData(int tagId) {
        loadData(Constants.PAGE_START, tagId);
    }

    public void loadMoreData(int tagId) {
        loadData(currPage + 1, tagId);
    }

    public void loadData(int page, int tagId) {
        teamPlayGameModel.loadPlayGameData(page, tagId, new OkHttpManager.MyCallBack<ServiceResult<ConvertPlayGameInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(page == Constants.PAGE_START);
                }
            }

            @Override
            public void onResponse(ServiceResult<ConvertPlayGameInfo> response) {
                currPage = page;
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().setupSuccessView(response.getData().getRoomList(), page == Constants.PAGE_START);//直接展示数据
                    } else {
                        onError(new Exception());
                    }
                } else {
                    onError(new Exception());
                }
            }
        });
    }
}
