package com.tongdaxing.erban.common.ui.home.view;


import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.home.HomeRoom;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> 首页hot 推荐控件 </p>
 *
 * @author Administrator
 * @date 2017/11/23
 */
public class HotRecommendView extends LinearLayout {

    private HomeRecommendItemView mItem1, mItem2, mItem3, mItem4, mItem5;
    private List<HomeRecommendItemView> mItemList;
    private Context mContext;

    public HotRecommendView(Context context) {
        this(context, null);
    }

    public HotRecommendView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HotRecommendView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public HotRecommendView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context) {
        mContext = context;
        inflate(context, R.layout.home_hot_recommend_layout, this);
        setOrientation(VERTICAL);
        mItem1 = (HomeRecommendItemView) findViewById(R.id.item_1);
        mItem2 = (HomeRecommendItemView) findViewById(R.id.item_2);
        mItem3 = (HomeRecommendItemView) findViewById(R.id.item_3);
        mItem4 = (HomeRecommendItemView) findViewById(R.id.item_4);
        mItem5 = (HomeRecommendItemView) findViewById(R.id.item_5);

        mItemList = new ArrayList<>(5);
        mItemList.add(mItem1);
        mItemList.add(mItem2);
        mItemList.add(mItem3);
        mItemList.add(mItem4);
        mItemList.add(mItem5);
    }

    public void setData(final List<HomeRoom> homeRoomList) {
        if (ListUtils.isListEmpty(homeRoomList)) {
            return;
        }
        int size = homeRoomList.size();
        setItemVisible(size);
        int minSize = Math.min(size, 5);
        for (int i = 0; i < minSize; i++) {
            HomeRecommendItemView homeRecommendItemView = mItemList.get(i);
            homeRecommendItemView.setData(homeRoomList.get(i), i);
            final int finalI = i;
            homeRecommendItemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeRoom homeRoom = homeRoomList.get(finalI);
                    if (homeRoom != null) {
                        AVRoomActivity.start(mContext, homeRoom.getUid());
                    }
                }
            });
        }
    }

    private void setItemVisible(int size) {
        mItem1.setVisibility(size >= 1 ? VISIBLE : GONE);
        mItem2.setVisibility(size >= 2 ? VISIBLE : GONE);
        mItem3.setVisibility(size >= 3 ? VISIBLE : GONE);
        mItem4.setVisibility(size >= 4 ? VISIBLE : GONE);
        mItem5.setVisibility(size >= 5 ? VISIBLE : GONE);
    }


}
