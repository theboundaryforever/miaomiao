package com.tongdaxing.erban.common.ui.find.model;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public class RecommendModel extends BaseMvpModel {
    private String TAG = "RecommendModel";

    public void loadData(int page, String dataType, String queryUid, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("uid", String.valueOf(currentUid));
        params.put("pageNum", String.valueOf(page));
        params.put("pageSize", String.valueOf(Constants.PAGE_HOME_HOT_SIZE));
        params.put("queryUid", queryUid);
        params.put("type", dataType);
        params.put("ticket", ticket);
        String url = UriProvider.getPublicVoiceGroupList();
        L.info(TAG, "params: %s", params);
        L.info(TAG, "uid: %d, pageNum: %d, pageSize: %d, queryUid: %s, type: %s, url: %s, ticket: %s", currentUid, Constants.PAGE_HOME_HOT_SIZE, page, queryUid, dataType, url, ticket);
        OkHttpManager.getInstance().doGetRequest(url, params, myCallBack);
    }

    public void voiceGroupLike(String commentId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("momentId", commentId);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getPublicVoiceGroupLike(), params, myCallBack);
    }

    public void loadRecommendModule(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getRecommendModule(), params, myCallBack);
    }
}
