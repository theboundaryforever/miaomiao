package com.tongdaxing.erban.common.ui.praise;

import android.graphics.Bitmap;

import com.tongdaxing.erban.common.ui.praise.base.IDrawable;
import com.tongdaxing.erban.common.ui.praise.base.IPraise;

import java.util.Random;

public class HiPraise implements IPraise {
    public float scale = 1.0f;
    public float alpha = 1.0f;
    public long duration;
    public long startDelay;
    public long delayAplhaTime;
    protected Bitmap bitmap;

    public HiPraise(Bitmap bitmap) {
        this.bitmap = bitmap;
        final int maxDuration = 2500;
        final int minDuration = 2000;
        int minDelayAlphaTime = minDuration / 4;
        duration = Utils.rondomRange(maxDuration, minDuration);
        delayAplhaTime = new Random().nextInt((int) duration)
                % (duration - minDelayAlphaTime + 1) + minDelayAlphaTime;
    }

    @Override
    public IDrawable toDrawable() {
        return new PraiseDrawable(bitmap, scale,
                alpha, duration, startDelay, delayAplhaTime, 0.45f);
    }

}
