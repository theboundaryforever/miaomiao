package com.tongdaxing.erban.common.room.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.common.base.fragment.BaseDialogFragment;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.UriProvider;

/**
 * cp戒指说明Dialog
 * <p>
 * Created by zhangjian on 2019/3/27.
 */
public class CpRingDescriptionDialog extends BaseDialogFragment {
    public static final String TAG = "CpRingDescriptionDialog";
    private static String KEY_URL = "url";
    private static String KEY_TITLE = "title";
    private String mUrl;
    private String mTitle;

    public static CpRingDescriptionDialog instance() {
        return new CpRingDescriptionDialog();
    }

    public static CpRingDescriptionDialog instance(String title, String url) {
        Bundle bundle = new Bundle();
        CpRingDescriptionDialog cpRingDescriptionDialog = new CpRingDescriptionDialog();
        bundle.putString(KEY_URL, url);
        bundle.putString(KEY_TITLE, title);
        cpRingDescriptionDialog.setArguments(bundle);
        return cpRingDescriptionDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View view = inflater.inflate(R.layout.layout_cp_ring_des_dialog, ((ViewGroup) window.findViewById(android.R.id.content)), false);//需要用android.R.id.content这个view
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        TextView tvTitle = view.findViewById(R.id.tv_title);
        WebView webview = view.findViewById(R.id.webview);
        ImageView ivClose = view.findViewById(R.id.iv_close);

        // 允许http 与 https 混合页面访问
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        //------设置圆角------
        webview.setBackgroundResource(R.drawable.dialog_bg_cp_ring_des);
        webview.setBackgroundColor(Color.TRANSPARENT);
        //------设置圆角------
        webview.getSettings().setTextZoom(300);//字体放大3倍
        webview.setVerticalScrollBarEnabled(false);//垂直不显示滚动条
        if (getArguments() != null) {
            mUrl = getArguments().getString(KEY_URL);
            mTitle = getArguments().getString(KEY_TITLE);
        }
        if (!TextUtils.isEmpty(mTitle)) {
            tvTitle.setText(mTitle);
        }
        if (!TextUtils.isEmpty(mUrl)) {
            L.debug(TAG, mUrl);
            webview.loadUrl(mUrl);
        } else {
            webview.loadUrl(UriProvider.getCpRingDesH5());
        }

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
