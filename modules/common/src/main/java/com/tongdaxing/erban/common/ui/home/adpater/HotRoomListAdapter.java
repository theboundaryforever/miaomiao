package com.tongdaxing.erban.common.ui.home.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.home.HomeRoom;

/**
 * <p> 首页热门adapter </p>
 */
public class HotRoomListAdapter extends BaseQuickAdapter<HomeRoom, BaseViewHolder> {
    private Context context;
    private int viewType;
    private int coverHeight;

    public HotRoomListAdapter(int layoutRes, Context context, int viewType) {
        super(layoutRes);
        this.viewType = viewType;
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, HomeRoom homeRoom) {
        if (homeRoom == null) {
            return;
        }
        ImageView ivCover = baseViewHolder.getView(R.id.iv_cover);
        if (viewType == 1) {
            //简单版布局头像大图显示
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) ivCover.getLayoutParams();
            lp.width = coverHeight;
            lp.height = coverHeight;//这里不能用SquareImageView，底部会丢失圆角，后续再分析原理，先自己计算
            ivCover.setLayoutParams(lp);
            GlideApp.with(context)
                    .load(homeRoom.getAvatar())
                    .placeholder(R.drawable.default_cover)
                    .error(R.drawable.default_cover)
//                    .transforms(new CenterCrop(),//这里不能用transforms，会丢失圆角，后续再分析原理，先用RoundedImageView
//                            new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size)))
                    .into(ivCover);
        } else {
            ImageLoadUtils.loadSmallRoundBackground(context, homeRoom.getAvatar(), ivCover);
        }
        baseViewHolder.setText(R.id.tv_title, homeRoom.getTitle());
        TextView tvCustomLabel = baseViewHolder.getView(R.id.tv_custom_label);
        String roomTag = homeRoom.getRoomTag();
        if (TextUtils.isEmpty(roomTag)) {
            roomTag = "";
        }
        tvCustomLabel.setText(roomTag);
        tvCustomLabel.setVisibility(View.VISIBLE);
        baseViewHolder.setText(R.id.tv_online_num, String.valueOf(homeRoom.getOnlineNum()));

        if (viewType == 1) {
            //简单版的布局特有的以下view
            ImageView ivPlay = baseViewHolder.getView(R.id.iv_play);
            GlideApp.with(context)
                    .load(R.drawable.icon_music_play_white)
                    .into(ivPlay);
//            SVGAImageView svgaPlay = baseViewHolder.getView(R.id.svga_play);
//
//            SvgaUtils.getInstance(context).cyclePlayAssetsAnim(svgaPlay, SvgaUtils.HALL_ROOM_ITEM_PLAY_SVGA_ASSETS);
        } else {
            //简单版的布局没有以下view
            baseViewHolder.setGone(R.id.lock_bg, !TextUtils.isEmpty(homeRoom.getRoomPwd()));
            baseViewHolder.setGone(R.id.lock_ic, !TextUtils.isEmpty(homeRoom.getRoomPwd()));
            baseViewHolder.setGone(R.id.iv_tab_category, !TextUtils.isEmpty(homeRoom.badge));
            baseViewHolder.setText(R.id.erban_no, context.getString(R.string.room_id) + homeRoom.getErbanNo());
            baseViewHolder.setGone(R.id.line, homeRoom.showLine != -1);
        }
    }

    public void setCoverHeightByGrid(int height) {
        this.coverHeight = height;
    }
}
