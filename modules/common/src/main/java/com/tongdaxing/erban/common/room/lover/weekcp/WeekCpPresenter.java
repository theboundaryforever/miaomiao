package com.tongdaxing.erban.common.room.lover.weekcp;

import android.text.TextUtils;
import android.util.LongSparseArray;
import android.util.SparseArray;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.lover.BaseWeekPresenter;
import com.tongdaxing.erban.common.room.lover.weekcp.tacit.TacitTestAction;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.custom.bean.WeekCpAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.RoomAction;
import com.tongdaxing.xchat_core.room.bean.RoomCpTicket;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.weekcp.tacit.ITacitTestCtrl;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTestEvent;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTextInfo;
import com.tongdaxing.xchat_core.room.weekcp.week.IWeekCpCtrl;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpEvent;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

import cn.sharesdk.framework.Platform;

/**
 * Created by Chen on 2019/4/30.
 */
public class WeekCpPresenter extends BaseWeekPresenter<IWeekCpView> {
    private String TAG = "WeekCpPresenter";

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChairChange(RoomAction.OnChairStateChange stateChange) {
        if (getView() == null) {
            return;
        }
        boolean sitDown = stateChange.isSitDown();
        L.debug(TAG, "onChairChange sitDown: %b", sitDown);
        if (!sitDown) {
            String cpId = getCpPlayerId();
            if (TextUtils.isEmpty(cpId)) {
                getView().resetUI();
            }
        }
    }

    public WeekCpInfo getWeekCpInfo() {
        return CoreManager.getCore(IRoomCore.class).getWeekManager().getWeekCpInfo();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCpValueResponse(WeekCpEvent.OnChangeHeartValue heartResponse) {
        if (getView() == null) {
            return;
        }
        int value = heartResponse.getHeartValue();
        L.debug(TAG, "onCpValueResponse value: %d", value);
        getView().updateUI(value);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRemoveCurrentCpForNewWeekCp(WeekCpEvent.OnRemoveCurrentCpForNewWeekCp removeCurrentCpForNewWeekCp) {
        String cpId = getCpPlayerId();
        L.debug(TAG, "onRemoveCurrentCpForNewWeekCp cpId: %s", cpId);
        if (TextUtils.isEmpty(cpId)) {
            return;
        }
        RoomCpTicket roomCpTicket = new RoomCpTicket();
        roomCpTicket.setUserId(getUserId());
        roomCpTicket.setRoomId(getRoomUid());
        roomCpTicket.setCpId(cpId);
        CoreManager.getCore(IRoomCore.class).getWeekManager().getWeekCpCtrl().requestWeekCp(roomCpTicket);
    }

    public void checkHasCp() {
        if (getView() == null) {
            return;
        }
        if (isSelfOnMic()) {
            int heartValue = getWeekCpInfo().getHeartValue();
            L.info(TAG, "requestCpHeart heartValue: %d", heartValue);
            if (heartValue >= IWeekCpCtrl.ROOM_CP_MAX_VALUE) {
                SparseArray<RoomQueueInfo> queueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
                if (queueMemberMap != null && queueMemberMap.size() > 2) {
                    RoomQueueInfo cpInfo;
                    RoomQueueInfo myInfo;
                    if (AvRoomDataManager.get().isRoomOwner()) {
                        myInfo = queueMemberMap.valueAt(0);
                        cpInfo = queueMemberMap.valueAt(1);
                    } else {
                        cpInfo = queueMemberMap.valueAt(0);
                        myInfo = queueMemberMap.valueAt(1);
                    }

                    if (myInfo != null && cpInfo != null) {
                        if (cpInfo.mChatRoomMember != null) {
                            String cpId = getWeekCpInfo().getCpId();
                            L.debug(TAG, "cpPlayerId: %d, cpUid: %s, cpId: %s", myInfo.cpUserId, cpInfo.mChatRoomMember.getAccount(), cpId);
                            if (!TextUtils.isEmpty(cpId)) {     // 已经拥有CP
                                if (Objects.equals(cpId, cpInfo.mChatRoomMember.getAccount())) {
                                    RoomCpTicket roomCpTicket = new RoomCpTicket();
                                    roomCpTicket.setUserId(getUserId());
                                    roomCpTicket.setRoomId(getRoomUid());
                                    roomCpTicket.setCpId(cpId);
                                    CoreManager.getCore(IRoomCore.class).getWeekManager().getWeekCpCtrl().requestAddCpHeart(roomCpTicket);
                                } else {
                                    String nick = cpInfo.mChatRoomMember.getNick();
                                    if (nick.length() > 8) {
                                        nick = nick.substring(0, 8);
                                    }
                                    getView().showMeHasCp(nick);
                                }
                            } else {
                                String account = cpInfo.mChatRoomMember.getAccount();
                                RoomCpTicket roomCpTicket = new RoomCpTicket();
                                roomCpTicket.setUserId(getUserId());
                                roomCpTicket.setRoomId(getRoomUid());
                                roomCpTicket.setCpId(account);
                                CoreManager.getCore(IRoomCore.class).getWeekManager().getWeekCpCtrl().requestWeekCp(roomCpTicket);
                            }
                        }
                    }
                }
            } else {
                //未满60点时 点击展示一周cp玩法
                getView().showWeekCpDes();
            }
        } else {
            getView().showWeekCpDes();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onExitRoom(NewRoomEvent.OnExitRoom exitRoom) {
        L.debug(TAG, "onExitRoom");
        updateView(getWeekCpInfo().getHeartValue());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChairStateChange(RoomAction.OnChairStateChange change) {
        L.debug(TAG, "onChairStateChange");
        if (getView() == null) {
            return;
        }
        getView().showView(isSelfOnMic());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTacitTestAskExit(TacitTestEvent.OnTacitTestAskExit askExit) {
        L.debug(TAG, "onTacitTestAskExit");
        if (getView() == null) {
            return;
        }
        if (!askExit.isInitiative()) {
            SingleToastUtil.showToast(BaseApp.mBaseContext.getString(R.string.room_lover_await_tacit_exit));
        }
        getView().dismissTacitView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTacitTestAskFinish(TacitTestEvent.OnTacitTestAskFinishShowView showView) {
        L.debug(TAG, "onTacitTestAskFinish questionId: %s", showView.getQuestionId());
        if (getView() == null) {
            return;
        }
        getView().dismissTacitView();
        getView().showResultTestView(showView.getQuestionId());
    }

    private void updateView(int value) {
        if (getView() == null) {
            return;
        }
        String cpId = getCpPlayerId();
        L.debug(TAG, "updateView cpId: %s, value: %d", cpId, value);
        if (TextUtils.isEmpty(cpId)) {
            getView().resetUI();
        } else {
            getView().updateUI(value);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWeekCpMerry(WeekCpEvent.OnFromCpChange fromCpChange) {
        int second = fromCpChange.getWeekCpAttachment().getSecond();
        if (second == WeekCpAttachment.ROOM_WEEK_CP_MERRY || second == WeekCpAttachment.ROOM_WEEK_CP_UPDATE) {
            updateView(0);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTacitTestAsk(TacitTestEvent.OnTacitTestAskClick testAsk) {
        if (getView() == null) {
            return;
        }
        String currentTime = testAsk.getCurrentTime();
        L.info(TAG, "currentTime: %s", currentTime);
        if (!TextUtils.isEmpty(currentTime)) {
            TacitTextInfo textInfo = null;
            LongSparseArray<TacitTextInfo> roomTalkTacitMsgs = CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTestCtrl().getRoomTalkTacitMsgs();
            if (roomTalkTacitMsgs != null) {
                textInfo = roomTalkTacitMsgs.get(Long.valueOf(currentTime));
            }
            if (textInfo == null) {
                textInfo = CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo();
            }
            int currentState = textInfo.getCurrentState();
            L.info(TAG, "currentState: %d", currentState);
            if (currentState == ITacitTestCtrl.TACIT_TEST_UN_ASK) {
                checkAskUid(testAsk.getUid());
            } else if (currentState == ITacitTestCtrl.TACIT_TEST_PAST_DUE) {
                SingleToastUtil.showToast(BaseApp.mBaseContext, "考验已过期");
            } else if (currentState == ITacitTestCtrl.TACIT_TEST_FINISHED) {
                getView().showResultTestView(currentTime);
            } else if (currentState == ITacitTestCtrl.TACIT_TEST_CANCEL) {
                SingleToastUtil.showToast(BaseApp.mBaseContext, "考验已取消");
            } else {
                getView().showTacitTestView();
            }
        }
    }

    private void checkAskUid(String uid) {
        String userId = getUserId();
        L.info(TAG, "checkAskUid uid: %s, userId: %s", uid, userId);
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        if (userId.equals(uid)) {
            getView().showTip();
        } else {
            getView().showReceiveView(getCpPlayerId());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTacitTestAsk(TacitTestEvent.OnTacitTestAsk tacitTestAsk) {
        if (getView() == null) {
            return;
        }
        getView().showTacitTestView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWeekCpFailed(WeekCpEvent.OnWeekCpFailed tacitTestFailed) {
        if (getView() == null) {
            return;
        }
        SingleToastUtil.showToast(BaseApp.mBaseContext, tacitTestFailed.getMessage());
    }

    public boolean isCpPlayer() {
        boolean isCp = false;
        SparseArray<RoomQueueInfo> queueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (queueMemberMap != null && queueMemberMap.size() > 2) {
            RoomQueueInfo cpInfo;
            RoomQueueInfo myInfo;
            if (AvRoomDataManager.get().isRoomOwner()) {
                myInfo = queueMemberMap.valueAt(0);
                cpInfo = queueMemberMap.valueAt(1);
            } else {
                cpInfo = queueMemberMap.valueAt(0);
                myInfo = queueMemberMap.valueAt(1);
            }
            if (myInfo != null && cpInfo != null) {
                if (myInfo.mChatRoomMember != null && cpInfo.mChatRoomMember != null) {
                    String account = cpInfo.mChatRoomMember.getAccount();
                    String cpId = getWeekCpInfo().getCpId();
                    L.debug(TAG, "cpId: %s, account: %s", cpId, account);
                    isCp = Objects.equals(account, cpId);
                }
            }
        }
        L.debug(TAG, "isCp: %b", isCp);
        return isCp;
    }

    public void share(Platform platform) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo != null) {
            CoreManager.getCore(IShareCore.class).shareRoom(platform, currentRoomInfo.getUid(), currentRoomInfo.getTitle());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDismissTacitView(TacitTestAction.OnDismissTacitTestView tacitTestView) {
        if (getView() == null) {
            return;
        }
        L.debug(TAG, "onDismissTacitView ");
        getView().dismissTacitView();
    }

    public boolean hasCp() {
        return !TextUtils.isEmpty(getWeekCpInfo().getCpId());
    }
}
