package com.tongdaxing.erban.common.ui.me.user.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.tongdaxing.erban.common.ui.common.permission.PermissionActivity;
import com.tongdaxing.erban.common.ui.me.audiocard.AudioCard2Activity;
import com.tongdaxing.erban.common.ui.me.user.adapter.UserPhotoAdapter;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.audio.AudioCardMarkManager;
import com.tongdaxing.xchat_core.audio.IAudioCardCoreClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.AuxiliaryToneBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_core.user.bean.VoiceCardBean;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;
import com.tongdaxing.xchat_framework.util.util.file.JXFileUtils;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 编辑页面
 *
 * @author zhouxiangfeng
 * @date 2017/5/23
 */
public class UserInfoModifyActivity extends TakePhotoActivity
        implements View.OnClickListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, UserPhotoAdapter.ImageClickListener {

    private static final String CAMERA_PREFIX = "picture_";
    private static final String BMP_TEMP_NAME = "bmp_temp_name";
    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
        @Override
        public void superPermission() {
            takePhoto();
        }
    };
    private String avatar;
    private ImageView civAvatar;
    private DatePickerDialog datePickerDialog;
    private TextView tvBirth;
    private TextView tvNick;
    private TextView tvSex;
    private TextView tvDesc;
    private UserInfo mUserInfo;
    private long userId;
    //    private AudioPlayView audioPlayView;
    private TextView tvAudioDesc;
    private RecyclerView photosRecyclerView;
    private UserInfoModifyActivity mActivity;
    private LinearLayout llyPhoto;

    //    private ImageView ivAudioCardMark;
    private String birth;
    private TextView tvThreeColumnOneRow, tvThreeColumnTwoRow, tvThreeColumnThreeRow, tvThreeColumnFourRow;
    private TextView tvTwoColumnOneRow, tvTwoColumnTwoRow, tvTwoColumnThreeRow, tvTwoColumnFourRow;
    private RelativeLayout rlVoice;
    private ImageView ivIconVoice;
    private boolean isAvatar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_modify);
        setStatusBar();
        mActivity = this;
        initTitleBar("编辑");
        findViews();
        init();

        userId = getIntent().getLongExtra("userId", 0);
        mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId);
        if (mUserInfo != null) {
            initData(mUserInfo);
        }
//        ivAudioCardMark.setVisibility(AudioCardMarkManager.getInstance(this).isMark() ? View.VISIBLE : View.GONE);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == userId) {
            mUserInfo = info;
            initData(mUserInfo);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        if (info.getUid() == userId) {
            mUserInfo = info;
            initData(mUserInfo);
            getDialogManager().dismissDialog();
        }
    }

    @Override
    public void click(int position, UserPhoto userPhoto) {
        if (userPhoto != null) {
            UIHelper.showModifyPhotosAct(UserInfoModifyActivity.this, userId);
        }
    }

    @Override
    public void addClick() {

    }

    private void setTextViewLeftDrawable(TextView tvVoice, int drawId) {
        Drawable drawablePlay = getResources().getDrawable(drawId);
        drawablePlay.setBounds(0, 0, drawablePlay.getMinimumWidth(), drawablePlay.getMinimumHeight());
        tvVoice.setCompoundDrawables(drawablePlay, null, null, null);
    }

    private void initData(UserInfo userInfo) {
        if (null != userInfo) {
            VoiceCardBean voiceCardBean = userInfo.getVoiceCard();
            if (voiceCardBean == null) {
                tvAudioDesc.setVisibility(View.VISIBLE);
                ivIconVoice.setVisibility(View.VISIBLE);
                rlVoice.setVisibility(View.GONE);
            } else {
                ivIconVoice.setVisibility(View.GONE);
                tvAudioDesc.setVisibility(View.GONE);
                rlVoice.setVisibility(View.VISIBLE);

                tvTwoColumnOneRow.setText(voiceCardBean.getMasterTone());
                tvThreeColumnOneRow.setText(voiceCardBean.getMasterTonePro() + "%");

                List<AuxiliaryToneBean> auxiliaryToneBeanList = voiceCardBean.getAuxiliaryToneList();
                if (!ListUtils.isListEmpty(auxiliaryToneBeanList) && auxiliaryToneBeanList.size() >= 3) {
                    tvTwoColumnTwoRow.setText(auxiliaryToneBeanList.get(0).getToneName());
                    tvThreeColumnTwoRow.setText(auxiliaryToneBeanList.get(0).getTonePro() + "%");

                    tvTwoColumnThreeRow.setText(auxiliaryToneBeanList.get(1).getToneName());
                    tvThreeColumnThreeRow.setText(auxiliaryToneBeanList.get(1).getTonePro() + "%");

                    tvTwoColumnFourRow.setText(auxiliaryToneBeanList.get(2).getToneName());
                    tvThreeColumnFourRow.setText(auxiliaryToneBeanList.get(2).getTonePro() + "%");
                }
            }

            ImageLoadUtils.loadCircleImage(this, userInfo.getAvatar(), civAvatar, R.drawable.ic_no_avatar);
            birth = TimeUtil.getDateTimeString(Long.valueOf(userInfo.getBirth()), "yyyy-MM-dd");
            tvBirth.setText(birth);
            tvNick.setText(userInfo.getNick());
            tvDesc.setText(userInfo.getUserDesc());
            if (userInfo.getGender() == 1) {
                tvSex.setText("男");
            } else {
                tvSex.setText("女");
            }
            UserPhotoAdapter adapter = new UserPhotoAdapter(userInfo.getPrivatePhoto(), 1);
            adapter.setImageClickListener(this);
            photosRecyclerView.setAdapter(adapter);
            if (userInfo.getPrivatePhoto() != null && userInfo.getPrivatePhoto().size() > 0) {
                photosRecyclerView.setVisibility(View.VISIBLE);
            } else {
                photosRecyclerView.setVisibility(View.GONE);
            }
        }
    }

    private void findViews() {
        ivIconVoice = (ImageView) findViewById(R.id.iv_icon_voice);
        rlVoice = (RelativeLayout) findViewById(R.id.rl_voice);
        tvThreeColumnOneRow = (TextView) findViewById(R.id.tv_three_column_one_row);
        tvThreeColumnTwoRow = (TextView) findViewById(R.id.tv_three_column_two_row);
        tvThreeColumnThreeRow = (TextView) findViewById(R.id.tv_three_column_three_row);
        tvThreeColumnFourRow = (TextView) findViewById(R.id.tv_three_column_four_row);
        tvTwoColumnOneRow = (TextView) findViewById(R.id.tv_two_column_one_row);
        tvTwoColumnTwoRow = (TextView) findViewById(R.id.tv_two_column_two_row);
        tvTwoColumnThreeRow = (TextView) findViewById(R.id.tv_two_column_three_row);
        tvTwoColumnFourRow = (TextView) findViewById(R.id.tv_two_column_four_row);

        civAvatar = (ImageView) findViewById(R.id.civ_avatar);
        tvBirth = (TextView) findViewById(R.id.tv_birth);
        tvNick = (TextView) findViewById(R.id.tv_nick);
        tvSex = (TextView) findViewById(R.id.tv_sex);
        tvDesc = (TextView) findViewById(R.id.tv_desc);
        tvAudioDesc = (TextView) findViewById(R.id.tv_audio_desc);
        photosRecyclerView = (RecyclerView) findViewById(R.id.rv_photos);
        findViewById(R.id.layout_avatar).setOnClickListener(this);
        findViewById(R.id.layout_birth).setOnClickListener(this);
        findViewById(R.id.layout_nick).setOnClickListener(this);
        findViewById(R.id.layout_desc).setOnClickListener(this);
        findViewById(R.id.layout_audiocard).setOnClickListener(this);
        findViewById(R.id.layout_photos).setOnClickListener(this);
        llyPhoto = (LinearLayout) findViewById(R.id.lly_photo);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        photosRecyclerView.setLayoutManager(mLayoutManager);
    }

    private void init() {
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        String monthstr;
        if ((month + 1) < 10) {
            monthstr = "0" + (month + 1);
        } else {
            monthstr = String.valueOf(month + 1);
        }
        String daystr;
        if ((day) < 10) {
            daystr = "0" + (day);
        } else {
            daystr = String.valueOf(day);
        }
        Date mSelectDate = new Date(year - 1900, month, day);
        Date date = new Date();
        date.setYear(date.getYear() - 18);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        if (mSelectDate.getTime() - date.getTime() > 0) {
            SingleToastUtil.showToast("仅对年满18岁及以上用户开放！");
            return;
        }

        String birth = String.valueOf(year) + "-" + monthstr + "-" + daystr;
        UserInfo user = new UserInfo();
        user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        user.setBirthStr(birth);
        requestUpdateUserInfo(user);
    }

    private void requestUpdateUserInfo(UserInfo user) {
        CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user, false);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {

    }

    private void takePhoto() {
        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
        File cameraOutFile = JXFileUtils.getTempFile(UserInfoModifyActivity.this, mCameraCapturingName);
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        CompressConfig compressConfig = new CompressConfig.Builder().create();
        getTakePhoto().onEnableCompress(compressConfig, false);
        CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
        getTakePhoto().onPickFromCaptureWithCrop(uri, options);
    }

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, Manifest.permission.CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            MLog.info(this, "return is not ok,resultCode=%d", resultCode);
            return;
        }

        if (requestCode == Method.NICK) {
            String stringExtra = data.getStringExtra(ModifyInfoActivity.CONTENTNICK);
            UserInfo user = new UserInfo();
            user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            user.setNick(stringExtra);
            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user, false);

        }

        if (requestCode == Method.DESC) {
            String stringExtra = data.getStringExtra(ModifyInfoActivity.CONTENT);
            UserInfo user = new UserInfo();
            user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            user.setUserDesc(stringExtra);
            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user, false);
        }

        if (requestCode == Method.AUDIO) {
            mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId);
            if (mUserInfo != null) {
                initData(mUserInfo);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.layout_avatar) {
            ButtonItem buttonItem = new ButtonItem("拍照上传", new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    checkPermissionAndStartCamera();
                }
            });
            ButtonItem buttonItem1 = new ButtonItem("本地相册", new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
                    File cameraOutFile = JXFileUtils.getTempFile(UserInfoModifyActivity.this, mCameraCapturingName);
                    if (!cameraOutFile.getParentFile().exists()) {
                        cameraOutFile.getParentFile().mkdirs();
                    }
                    Uri uri = Uri.fromFile(cameraOutFile);
                    CompressConfig compressConfig = new CompressConfig.Builder().create();
                    getTakePhoto().onEnableCompress(compressConfig, true);
                    CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
                    getTakePhoto().onPickFromGalleryWithCrop(uri, options);

                }
            });
            List<ButtonItem> buttonItems = new ArrayList<>();
            buttonItems.add(buttonItem);
            buttonItems.add(buttonItem1);
            getDialogManager().showCommonPopupDialog(buttonItems, "取消", false);

            isAvatar = true;

        } else if (i == R.id.layout_birth) {
            if (mUserInfo != null) {
                int year = TimeUtils.getYear(mUserInfo.getBirth());
                int month = TimeUtils.getMonth(mUserInfo.getBirth());
                int day = TimeUtils.getDayOfMonth(mUserInfo.getBirth());
                datePickerDialog = DatePickerDialog.newInstance(this, year, (month - 1), day, true);
            }
            datePickerDialog.setVibrate(true);
            datePickerDialog.setYearRange(1919, 2017);
            datePickerDialog.show(getSupportFragmentManager(), "DATEPICKER_TAG_1");

        } else if (i == R.id.layout_nick) {
            UIHelper.showModifyInfoAct(UserInfoModifyActivity.this, Method.NICK, "昵称");

        } else if (i == R.id.layout_desc) {
            UIHelper.showModifyInfoAct(UserInfoModifyActivity.this, Method.DESC, "个性签名");

        } else if (i == R.id.layout_audiocard) {
            mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId);
            if (mUserInfo != null && mUserInfo.getVoiceCard() != null) {
                //通过startForResult打开CommonWebViewActivity页面，在清空声音之后需要用户页面更新一下用户信息
                CommonWebViewActivity.startForResult(this, UriProvider.getAudioIdentifyCard(), Method.AUDIO, getString(R.string.audio_card_clear));
            } else {
                checkPermission(() -> {
                            Intent intent = new Intent(UserInfoModifyActivity.this, AudioCard2Activity.class);
                            intent.putExtra(AudioCard2Activity.AUDIO_FILE_URL, mUserInfo != null ? mUserInfo.getUserVoice() : null);
                            intent.putExtra(AudioCard2Activity.AUDIO_LENGTH, mUserInfo != null ? mUserInfo.getVoiceDura() : 0);
                            UserInfoModifyActivity.this.startActivityForResult(intent, Method.AUDIO);
                            isAvatar = false;
                            clearMark();
                        }, R.string.ask_again,
                        Manifest.permission.RECORD_AUDIO);
            }

        } else if (i == R.id.layout_photos) {
            UIHelper.showModifyPhotosAct(UserInfoModifyActivity.this, userId);

        } else {
        }
    }

    /**
     * 清除 声卡签名 红点
     */
    private void clearMark() {
        AudioCardMarkManager.getInstance(UserInfoModifyActivity.this).clearMark();
    }

    @CoreEvent(coreClientClass = IAudioCardCoreClient.class)
    public void onAudioCardMarkClear() {
//        ivAudioCardMark.setVisibility(View.GONE);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        if (isAvatar) {
            UserInfo user = new UserInfo();
            avatar = url;
            user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            user.setAvatar(avatar);
            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user, true);
        }
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        toast("上传失败");
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId, true);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onUserAvatarChanged() {
        toast("头像上传成功，图片审核中");
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(UserInfoModifyActivity.this, "请稍后");
        CoreManager.getCore(IFileCore.class).upload(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
    }

    @Override
    protected void onDestroy() {
//        audioPlayView.release();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        audioPlayView.stop();
    }

    public interface Method {
        /**
         * 录音
         */
        int AUDIO = 2;
        /**
         * 昵称
         */
        int NICK = 3;
        /**
         * 个人介绍
         */
        int DESC = 4;
    }
}
