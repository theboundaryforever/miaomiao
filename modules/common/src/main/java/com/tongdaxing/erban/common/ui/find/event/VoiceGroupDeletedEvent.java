package com.tongdaxing.erban.common.ui.find.event;

/**
 * 动态删除事件
 * <p>
 * Created by zhangjian on 2019/4/19.
 */
public class VoiceGroupDeletedEvent {
    public int id;
    public int index;

    public VoiceGroupDeletedEvent(int id, int index) {
        this.id = id;
        this.index = index;
    }

    @Override
    public String toString() {
        return "VoiceGroupDeletedEvent{" +
                "id=" + id +
                ", index=" + index +
                '}';
    }
}
