package com.tongdaxing.erban.common.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.tongdaxing.erban.common.R;

/**
 * <p> main tab 控件 </p>
 * Created by Administrator on 2017/11/14.
 */
public class MainTab extends android.support.v7.widget.AppCompatImageView {
    private int mTabIcon, mTabIconSelect;
    private Context mContext;

    public MainTab(Context context) {
        this(context, null);
    }

    public MainTab(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainTab(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        mContext = context;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MainTab);
        mTabIcon = typedArray.getResourceId(R.styleable.MainTab_tab_icon, R.drawable.ic_main_tab_home_new_year);
        mTabIconSelect = typedArray.getResourceId(R.styleable.MainTab_tab_icon_select, R.drawable.ic_main_tab_home_pressed_new_year);
        typedArray.recycle();

        select(false);
    }

    public void setIcon(int iconResId) {
//        Drawable mainTabHome = ContextCompat.getDrawable(mContext, iconResId);
//        //必须加上这句，否则不显示
//        if (mainTabHome != null) {
////            mainTabHome.setBounds(0, 0, mainTabHome.getMinimumWidth(), mainTabHome.getIntrinsicHeight());
//            setImageDrawable(mainTabHome);
//        }
        setImageResource(iconResId);
    }

    public void select(boolean select) {
        setIcon(select ? mTabIconSelect : mTabIcon);
    }
}
