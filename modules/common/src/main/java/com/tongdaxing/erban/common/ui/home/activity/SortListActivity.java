package com.tongdaxing.erban.common.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.ui.home.fragment.SortHotFragment;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 * Created by Administrator on 2018/4/20.
 */

public class SortListActivity extends BaseActivity {


    public static void start(Context context, int type) {
        Intent intent = new Intent(context, SortListActivity.class);
        intent.putExtra("index", type);
        context.startActivity(intent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sort_list_activity);
        int index = getIntent().getIntExtra("index", -1);
        if (index < 0)
            finish();

        String s = (String) SpUtils.get(this, SpEvent.not_hot_menu, "");
        if (TextUtils.isEmpty(s))
            finish();
        SortHotFragment sortHotFragment = null;


        try {
            Json parse = Json.parse(s);
            Json json = parse.jlist("data").get(index);
            initTitleBar(json.str("name"));
            Gson gson = new Gson();
            TabInfo tabInfo = gson.fromJson(json + "", TabInfo.class);
            sortHotFragment = SortHotFragment.newInstance(tabInfo);

        } catch (Exception e) {
            finish();
        }
        if (sortHotFragment == null) {
            finish();
        } else {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fl_sort, sortHotFragment);
            fragmentTransaction.commitNowAllowingStateLoss();
        }


    }
}
