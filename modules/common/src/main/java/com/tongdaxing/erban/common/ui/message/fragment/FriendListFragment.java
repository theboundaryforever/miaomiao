package com.tongdaxing.erban.common.ui.message.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.message.adapter.FriendListAdapter;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCoreClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * 好友列表界面
 *
 * @author chenran
 * @date 2017/9/18
 */
public class FriendListFragment extends BaseFragment {
    public static final String TAG = "FriendListFragment";
    private RecyclerView recyclerView;
    private FriendListAdapter adapter;
    private List<NimUserInfo> infoList;


    @Override
    public void onFindViews() {
        recyclerView = (SwipeMenuRecyclerView) mView.findViewById(R.id.recycler_view);
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
//        View headView = LayoutInflater.from(getActivity()).inflate(R.layout.head_fragment_friend_list, null);
//        binding = DataBindingUtil.bind(headView);
//        binding.setClick(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new FriendListAdapter(R.layout.list_item_friend);
//        adapter.setHeaderAndEmpty(true);
//        adapter.addHeaderView(headView);
        adapter.setEmptyView(getEmptyView(recyclerView, getString(R.string.no_frenids_text)));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (infoList == null || position >= infoList.size()) {
                    toast("数据异常请稍后重试");
                    return;
                }

                NimUIKit.startP2PSession(getActivity(), infoList.get(position).getAccount());
            }
        });

        adapter.setOnItemLongClickListener(new BaseQuickAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
                NimUserInfo nimUserInfo = infoList.get(position);
                getDialogManager().showOkCancelDialog(
                        "是否与" + nimUserInfo.getName() + "解除好友关系？",
                        true,
                        new DialogManager.AbsOkDialogListener() {
                            @Override
                            public void onOk() {
                                //取消关注 刷新列表 会触发onFriendListUpdate()
                                CoreManager.getCore(IPraiseCore.class).cancelPraise(Long.parseLong(nimUserInfo.getAccount()));
                            }
                        });
                return false;
            }
        });


        List<NimUserInfo> userInfos = CoreManager.getCore(IIMFriendCore.class).getMyFriends();
        setData(userInfos);
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_friend_list;
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void onFriendListUpdate(List<NimUserInfo> userInfos) {
        L.debug(TAG, "onFriendListUpdate: userInfos.size() = %d", userInfos == null ? -1 : userInfos.size());
        setData(userInfos);
    }

    private void setData(List<NimUserInfo> userInfos) {
        infoList = userInfos;
        if (userInfos != null && userInfos.size() > 0) {
            hideStatus();
            adapter.setNewData(userInfos);
        } else {
            infoList = new ArrayList<>();
            adapter.setNewData(infoList);
        }
        adapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        L.debug(TAG, "onCurrentUserInfoUpdate");
        recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                List<NimUserInfo> userInfos = CoreManager.getCore(IIMFriendCore.class).getMyFriends();
                setData(userInfos);
            }
        }, 250);

    }



 /*   @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onImLoginSuccess(LoginInfo loginInfo) {
        List<NimUserInfo> userInfos = CoreManager.getCore(IIMFriendCore.class).getMyFriends();
        setData(userInfos);
    }*/

}
