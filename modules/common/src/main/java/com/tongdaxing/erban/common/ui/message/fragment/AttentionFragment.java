package com.tongdaxing.erban.common.ui.message.fragment;

import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.message.adapter.AttentionListAdapter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.user.AttentionCore;
import com.tongdaxing.xchat_core.user.AttentionCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

import static com.tongdaxing.erban.common.R.id.swipe_refresh;

/**
 * <p> 主页关注界面 </p>
 * Created by Administrator on 2017/11/14.
 */
public class AttentionFragment extends BaseFragment {
    public static final String TAG = "AttentionFragment";
    private RecyclerView mRecylcerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AttentionListAdapter adapter;
    private List<AttentionInfo> mAttentionInfoList = new ArrayList<>();
    private int mPage = Constants.PAGE_START;
    SwipeRefreshLayout.OnRefreshListener onRefreshLisetener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            firstLoadData();
        }
    };

    @Override
    public void onFindViews() {
        mRecylcerView = (RecyclerView) mView.findViewById(R.id.user_live_recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(swipe_refresh);
        mRecylcerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onSetListener() {
        swipeRefreshLayout.setOnRefreshListener(onRefreshLisetener);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.mm_theme));
        adapter = new AttentionListAdapter(mAttentionInfoList);
        adapter.setRylListener(new AttentionListAdapter.onClickListener() {
            @Override
            public void rylListeners(AttentionInfo attentionInfo) {
                NewUserInfoActivity.start(getContext(), attentionInfo.getUid());
            }

            @Override
            public void findHimListeners(AttentionInfo attentionInfo) {
                if (attentionInfo.getUserInRoom() != null)
                    AVRoomActivity.start(getContext(), attentionInfo.getUserInRoom().getUid(), new HerUserInfo(attentionInfo.uid, attentionInfo.nick));
            }

            @Override
            public void openCpGiftDialog(AttentionInfo attentionInfo) {

            }
        });
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mPage++;
                onRefreshing();
            }
        }, mRecylcerView);
    }

    @Override
    public void initiate() {
        mRecylcerView.setAdapter(adapter);
        showLoading();
        onRefreshing();
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_attention;
    }

    public void firstLoadData() {
        mPage = Constants.PAGE_START;
        onRefreshing();
    }

    private void onRefreshing() {
        CoreManager.getCore(AttentionCore.class)
                .getAttentionList(CoreManager.getCore(IAuthCore.class).getCurrentUid(), mPage, Constants.PAGE_SIZE);
    }

    @CoreEvent(coreClientClass = AttentionCoreClient.class)
    public void onGetAttentionList(List<AttentionInfo> attentionInfoList, int page) {
        mPage = page;
        if (!ListUtils.isListEmpty(attentionInfoList)) {
            if (mPage == Constants.PAGE_START) {
                hideStatus();
                swipeRefreshLayout.setRefreshing(false);
                mAttentionInfoList.clear();
                adapter.setNewData(attentionInfoList);
                if (attentionInfoList.size() < Constants.PAGE_SIZE) {
                    adapter.setEnableLoadMore(false);
                }
            } else {
                adapter.loadMoreComplete();
                adapter.addData(attentionInfoList);
            }
        } else {
            if (mPage == Constants.PAGE_START) {
                swipeRefreshLayout.setRefreshing(false);
                showNoData(getString(R.string.no_attention_text));
            } else {
                adapter.loadMoreEnd(true);
            }

        }
    }

    @CoreEvent(coreClientClass = AttentionCoreClient.class)
    public void onGetAttentionListFail(String error, int page) {
        mPage = page;
        if (mPage == Constants.PAGE_START) {
            swipeRefreshLayout.setRefreshing(false);
            showNetworkErr();
        } else {
            adapter.loadMoreFail();
            toast(error);
        }
    }

    // ------------------关注动作回调 begin-------------------
    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        onRefreshing();
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long uid) {
        onRefreshing();
    }
    // ------------------关注动作回调 end-------------------


    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        onRefreshing();
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        mPage = Constants.PAGE_START;
        showLoading();
        onRefreshing();
    }
}
