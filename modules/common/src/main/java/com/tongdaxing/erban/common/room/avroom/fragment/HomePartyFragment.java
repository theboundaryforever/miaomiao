package com.tongdaxing.erban.common.room.avroom.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.room.avroom.widget.GiftV2View;
import com.tongdaxing.erban.common.room.avroom.widget.HomePartyPKView;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DESUtils;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.tongdaxing.xchat_framework.util.util.DESUtils.giftCarSecret;

/**
 * 轰趴房
 * Created by 2016/9/22.
 *
 * @author Administrator
 */
public class HomePartyFragment extends AbsRoomFragment implements View.OnClickListener, HomePartyRoomFragment.UserComeAction {

    @BindView(R2.id.fm_content)
    FrameLayout fmContent;
    @BindView(R2.id.view_pk)
    HomePartyPKView viewPk;
    @BindView(R2.id.gift_view)
    GiftV2View giftView;
    private HomePartyRoomFragment roomFragment;

    public static HomePartyFragment newInstance(long roomUid) {
        Log.e("newInstance", "HomePartyFragment - newInstance");
        HomePartyFragment homePartyFragment = new HomePartyFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.ROOM_UID, roomUid);
        homePartyFragment.setArguments(bundle);
        return homePartyFragment;
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // clear views
        Log.e("onNewIntent", "HomePartyFragment - onNewIntent");
        if (roomFragment != null)
            roomFragment.onNewIntent(intent);
//        if (listFragment != null)
//            listFragment.onNewIntent(intent);


    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_chatroom_game_main;
    }

    @Override
    public void onFindViews() {
        ButterKnife.bind(this, mView);
        viewPk = mView.findViewById(R.id.view_pk);
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {
        roomFragment = new HomePartyRoomFragment();
        roomFragment.setUserComeAction(this);
        getChildFragmentManager().beginTransaction().replace(R.id.fm_content, roomFragment).commitAllowingStateLoss();
        updateGiftInfo();
    }

    /**
     * 更新礼物信息
     */
    private void updateGiftInfo() {
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
    }

    @Override
    public void release() {
        super.release();
        if (roomFragment != null) {
            roomFragment = null;
        }
    }

    @Override
    public void onShowActivity(List<ActionDialogInfo> dialogInfos) {
//        if (roomFragment != null && roomFragment.isAdded()) {
//            roomFragment.showActivity(dialogInfos);
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        roomFragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRoomOnlineNumberSuccess(int onlineNumber) {
        super.onRoomOnlineNumberSuccess(onlineNumber);
    }

    @Override
    public void showCar(String carImageUrl) {
        try {
            final String carUrl = DESUtils.DESAndBase64Decrypt(carImageUrl, giftCarSecret);
            LogUtil.d("setUserCarAciton", carUrl);
            if (!TextUtils.isEmpty(carUrl)) {
                giftView.giftEffectView.drawSvgaEffect(carUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}