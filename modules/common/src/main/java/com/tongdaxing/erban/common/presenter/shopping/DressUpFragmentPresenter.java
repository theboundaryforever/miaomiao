package com.tongdaxing.erban.common.presenter.shopping;

import com.tongdaxing.erban.common.model.shopping.DressUpModel;
import com.tongdaxing.erban.common.ui.me.shopping.view.IDressUpFragmentView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.user.bean.DressUpBean;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;

/**
 * 装扮商城列表的Presenter
 */
public class DressUpFragmentPresenter extends AbstractMvpPresenter<IDressUpFragmentView> {
    private DressUpModel dressUpModel;


    public DressUpFragmentPresenter() {
        if (this.dressUpModel == null)
            this.dressUpModel = new DressUpModel();
    }


    /**
     * 获取装扮商城列表
     *
     * @param type
     * @param pageNum
     * @param pageSize
     */
    public void getDressUpData(boolean isMyself, int type, int pageNum, int pageSize) {
        if (dressUpModel != null)
            dressUpModel.getDressUpData(isMyself, type, pageNum, pageSize, new OkHttpManager.MyCallBack<ServiceResult<List<DressUpBean>>>() {
                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        getMvpView().getDressUpListFail(e);
                    }
                }

                @Override
                public void onResponse(ServiceResult<List<DressUpBean>> response) {
                    if (getMvpView() != null) {
                        getMvpView().getDressUpListSuccess(response);
                    }
                }
            });
    }

    /**
     * 购买座驾
     *
     * @param type  type 0 头饰 1 座驾
     * @param carId
     */
    public void onPurseDressUp(int type, int purseType, int carId) {
        if (dressUpModel != null)
            dressUpModel.onPurseDressUp(type, purseType, carId, new OkHttpManager.MyCallBack<Json>() {
                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        getMvpView().onPurseDressUpFail(e.getMessage());
                    }
                }

                @Override
                public void onResponse(Json response) {
                    if (response != null) {
                        if (response.num("code") == 200) {
                            if (getMvpView() != null) {
                                getMvpView().onPurseDressUpSuccess(purseType);
                            }
                        } else {
                            if (getMvpView() != null) {
                                getMvpView().onPurseDressUpFail(response.str("message"));
                            }
                        }
                    }
                }
            });
    }

    /**
     * 修改装扮的使用状态
     *
     * @param type    type 0 头饰 1 座驾
     * @param dressId 装扮id
     */
    public void onChangeDressUpState(int type, int dressId) {
        if (dressUpModel != null) {
            dressUpModel.onChangeDressUpState(type, dressId, new OkHttpManager.MyCallBack<Json>() {
                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        getMvpView().onChangeDressUpStateFail(e.getMessage());
                    }
                }

                @Override
                public void onResponse(Json response) {
                    if (response != null) {
                        if (response.num("code") == 200) {
                            if (getMvpView() != null) {
                                getMvpView().onChangeDressUpStateSuccess(dressId);
                            }
                        } else {
                            if (getMvpView() != null) {
                                getMvpView().onChangeDressUpStateFail(response.str("message"));
                            }
                        }
                    }
                }
            });
        }
    }

}
