package com.tongdaxing.erban.common.ui.message.activity;

import android.os.Bundle;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.ui.message.fragment.FansListFragment;
import com.tongdaxing.xchat_core.Constants;

/**
 * 粉丝列表
 */
public class FansListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fans);
        initTitleBar(getString(R.string.my_fan));

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, FansListFragment.newInstance(Constants.FAN_NO_MAIN_PAGE_TYPE), FansListFragment.class.getSimpleName())
                .commitAllowingStateLoss();


    }
}
