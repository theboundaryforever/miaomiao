package com.tongdaxing.erban.common.ui.message.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.common.R;

/**
 * Created by huangmeng1 on 2018/1/18.
 */

public class FriendBlackAdapter extends BaseQuickAdapter<NimUserInfo, BaseViewHolder> {
    private RoomBlackDelete roomBlackDelete;

    public FriendBlackAdapter(int layoutResId) {
        super(layoutResId);
    }

    public void setRoomBlackDelete(RoomBlackDelete roomBlackDelete) {
        this.roomBlackDelete = roomBlackDelete;
    }

    @Override
    protected void convert(BaseViewHolder helper, NimUserInfo item) {
        helper.setText(R.id.tv_userName, item.getName());
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), helper.getView(R.id.imageView), R.drawable.ic_no_avatar);
        View llContainer = helper.getView(R.id.container);
        ViewGroup.LayoutParams layoutParams = llContainer.getLayoutParams();
        layoutParams.width = UIUtil.getScreenWidth(mContext);
        llContainer.setLayoutParams(layoutParams);
        llContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (roomBlackDelete != null) {
                    roomBlackDelete.onDeleteClick(item);
                }
                return true;
            }
        });
    }

    public interface RoomBlackDelete {
        void onDeleteClick(NimUserInfo item);
    }
}
