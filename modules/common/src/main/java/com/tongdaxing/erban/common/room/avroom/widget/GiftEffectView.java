package com.tongdaxing.erban.common.room.avroom.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.tongdaxing.erban.common.view.UrlImageSpan;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.find.SquareClient;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.ResolutionUtils;
import com.tongdaxing.xchat_framework.util.util.RichTextUtil;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by chenran on 2017/10/8.
 */

public class GiftEffectView extends RelativeLayout implements SVGACallback {
    public View svgaBg;
    CircleImageView ivCpUser1;
    CircleImageView ivCpUser2;
    TextView tvCpContent;
    View cpLayout;
    private RelativeLayout container;
    private SVGAImageView svgaImageView;
    private ImageView giftLightBg;
    private ImageView giftImg;
    private ImageView imgBg;
    private CircleImageView benefactorAvatar;
    private CircleImageView receiverAvatar;
    private TextView benefactorNick;
    private TextView receiverNick;
    private TextView giftNumber;
    private TextView giftName;
    private GiftEffectListener giftEffectListener;
    private EffectHandler effectHandler;
    private boolean isAnim;
    private LinearLayout benfactor;
    private TextView giveText;
    private ImageView ivSend;
    private LinearLayout receiverContainer;
    private ImageView whiteLightBg;
    private TextView tvSupergiftInfo;
    private RelativeLayout rl_cp;
    private SVGAImageView svga_cp;
    private CircleImageView avatarLeft;
    private CircleImageView avatarRight;
    private SVGAImageView iv_cp_gift;

    public GiftEffectView(Context context) {
        super(context);
        init();
    }

    public GiftEffectView(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    public GiftEffectView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init();
    }

    public boolean isAnim() {
        return isAnim;
    }

    public void setGiftEffectListener(GiftEffectListener giftEffectListener) {
        this.giftEffectListener = giftEffectListener;
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_gift_effect, this, true);
        effectHandler = new EffectHandler(this);
        container = (RelativeLayout) findViewById(R.id.container);
        imgBg = (ImageView) findViewById(R.id.img_bg);
        giftLightBg = (ImageView) findViewById(R.id.gift_light_bg);
        giftImg = (ImageView) findViewById(R.id.gift_img);
        benefactorAvatar = (CircleImageView) findViewById(R.id.benefactor_avatar);
        receiverAvatar = (CircleImageView) findViewById(R.id.receiver_avatar);
        benefactorNick = (TextView) findViewById(R.id.benefactor_nick);
        receiverNick = (TextView) findViewById(R.id.receiver_nick);
        giftNumber = (TextView) findViewById(R.id.gift_number);
        giftName = (TextView) findViewById(R.id.gift_name);
        svgaImageView = (SVGAImageView) findViewById(R.id.svga_imageview);


        benfactor = (LinearLayout) findViewById(R.id.benefactor_container);
        giveText = (TextView) findViewById(R.id.give_text);
        ivSend = (ImageView) findViewById(R.id.iv_send);
        receiverContainer = (LinearLayout) findViewById(R.id.receiver_container);
        whiteLightBg = (ImageView) findViewById(R.id.white_light_bg);
        tvSupergiftInfo = findViewById(R.id.tv_super_gift_info);

        rl_cp = findViewById(R.id.rl_cp);
        svga_cp = findViewById(R.id.svga_cp);
        avatarLeft = findViewById(R.id.avatar1);
        avatarRight = findViewById(R.id.avatar2);
        iv_cp_gift = findViewById(R.id.iv_cp_gift);

        ivCpUser1 = findViewById(R.id.iv_cp_user1);
        ivCpUser2 = findViewById(R.id.iv_cp_user2);
        tvCpContent = findViewById(R.id.tv_cp_content);
        cpLayout = findViewById(R.id.cp_layout);


        svgaImageView.setCallback(this);
        svgaImageView.setClearsAfterStop(true);
        svgaImageView.setLoops(1);
        svgaBg = findViewById(R.id.svga_imageview_bg);
    }

    // : 2018/3/10  横幅的ui
    public void startGiftEffect(GiftReceiveInfo giftRecieveInfo) {
        LogUtil.d("startGiftEffect", "" + giftRecieveInfo);
        this.isAnim = true;
        if (giftRecieveInfo.isCpGift()) {//CP全屏动效 + CP顶部Notify 表白了，大家快来围观鸭
            startCpGiftEffect(giftRecieveInfo);
            return;
        }
        if (giftRecieveInfo.isCpTopNotify()) {//CP顶部Notify 表白了，大家快来围观鸭
            startCpTopNotifyEffect(giftRecieveInfo);
            return;
        }
        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftRecieveInfo.getGiftId());
        LogUtil.d("startGiftEffect", "" + giftInfo);
        if (giftInfo != null) {

            //如果有roomId是大礼物，全服特效
            //这个roomId其实是产生送礼物的房间uid
            //userNo是送礼物的房间  显示的ID号
            final String roomId = giftRecieveInfo.getRoomId();
            if (TextUtils.isEmpty(roomId)) {
                container.setOnClickListener(v -> {

                });
                benfactor.setVisibility(VISIBLE);
//                giveText.setVisibility(VISIBLE);
                ivSend.setVisibility(VISIBLE);
                receiverContainer.setVisibility(VISIBLE);
                whiteLightBg.setVisibility(VISIBLE);
                tvSupergiftInfo.setVisibility(GONE);
            } else {
                container.setOnClickListener(v -> {
//                        LogUtils.d("startGiftEffect", roomId);
                    Intent intent = new Intent(getContext(), AVRoomActivity.class);
                    intent.putExtra(Constants.ROOM_UID, JavaUtil.str2long(roomId));
                    intent.putExtra(Constants.ROOM_TYPE, 3);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);

                });
                benfactor.setVisibility(INVISIBLE);
                giveText.setVisibility(INVISIBLE);
                ivSend.setVisibility(INVISIBLE);
                receiverContainer.setVisibility(INVISIBLE);
                whiteLightBg.setVisibility(INVISIBLE);
                tvSupergiftInfo.setVisibility(VISIBLE);

                String nick = giftRecieveInfo.getNick() + "";
                if (nick.length() > 10) {
                    nick = nick.substring(0, 10) + "…";
                }
                String targetNick = giftRecieveInfo.getTargetNick() + "";
                if (targetNick.length() > 10) {
                    targetNick = targetNick.substring(0, 10) + "…";
                }
                List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                HashMap<String, Object> map;
                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, nick);
                map.put(RichTextUtil.RICHTEXT_COLOR, Color.BLACK);
                list.add(map);

                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, "在");
                map.put(RichTextUtil.RICHTEXT_COLOR, Color.WHITE);
                list.add(map);

                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, "ID" + giftRecieveInfo.getUserNo());
                map.put(RichTextUtil.RICHTEXT_COLOR, Color.BLACK);
                list.add(map);

                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, "房间送给");
                map.put(RichTextUtil.RICHTEXT_COLOR, Color.WHITE);
                list.add(map);

                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, targetNick);
                map.put(RichTextUtil.RICHTEXT_COLOR, Color.BLACK);
                list.add(map);

                tvSupergiftInfo.setText(RichTextUtil.getSpannableStringFromList(list));
            }
            ImageLoadUtils.loadAvatar(benefactorAvatar.getContext(), giftRecieveInfo.getAvatar(), benefactorAvatar);
            ImageLoadUtils.loadImage(giftImg.getContext(), giftInfo.getGiftUrl(), giftImg);
            benefactorNick.setText(giftRecieveInfo.getNick());
            giftNumber.setText("x" + giftRecieveInfo.getGiftNum());
            if (TextUtils.isEmpty(roomId)) {
                giftNumber.setTextColor(Color.parseColor("#Fee800"));
            } else {
                giftNumber.setTextColor(Color.parseColor("#491E1A"));
            }
            giftName.setText(giftInfo.getGiftName());
            container.setVisibility(VISIBLE);

            if (!StringUtil.isEmpty(giftRecieveInfo.getTargetAvatar()) && !StringUtil.isEmpty(giftRecieveInfo.getTargetNick())) {
                ImageLoadUtils.loadAvatar(receiverAvatar.getContext(), giftRecieveInfo.getTargetAvatar(), receiverAvatar);
                receiverNick.setText(giftRecieveInfo.getTargetNick());
            } else {
                receiverAvatar.setImageResource(R.drawable.ic_mm_logo);
                receiverNick.setText("全麦");
            }

            Animation operatingAnim = AnimationUtils.loadAnimation(getContext(), R.anim.light_bg_rotate_anim);
            LinearInterpolator lin = new LinearInterpolator();
            operatingAnim.setInterpolator(lin);
            giftLightBg.setAnimation(operatingAnim);

            final Point center = new Point();
            center.x = ResolutionUtils.getScreenWidth(getContext()) / 2;
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(container, "translationX", -UIUtil.dip2px(getContext(), 400), center.x - container.getWidth() / 2).setDuration(500);
            objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            objectAnimator.start();

            ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(container, "alpha", 0.0F, 1.0F).setDuration(500);
            objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
            objectAnimator1.start();


            int totalCoin = giftInfo.getGoldPrice() * giftRecieveInfo.getGiftNum();
            if (giftRecieveInfo.getPersonCount() > 0) {
                totalCoin = giftInfo.getGoldPrice() * giftRecieveInfo.getGiftNum() * giftRecieveInfo.getPersonCount();
            }
            if (!TextUtils.isEmpty(roomId)) {
                imgBg.setImageResource(R.drawable.icon_gift_effect_bg_4);
            } else if (totalCoin >= 520 && totalCoin < 4999) {
                imgBg.setImageResource(R.drawable.icon_gift_effect_bg_1);
            } else if (totalCoin >= 4999 && totalCoin < 9999) {
                imgBg.setImageResource(R.drawable.icon_gift_effect_bg_2);
            } else if (totalCoin >= 9999) {
                imgBg.setImageResource(R.drawable.icon_gift_effect_bg_3);
            }


            effectHandler.sendEmptyMessageDelayed(0, 6000);

            if (giftInfo.isHasEffect() && !StringUtil.isEmpty(giftInfo.getVggUrl()) && TextUtils.isEmpty(roomId)) {
                try {
                    drawSvgaEffect(giftInfo.getVggUrl());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * CP全屏动效 + CP顶部Notify
     */
    public void startCpGiftEffect(GiftReceiveInfo giftRecieveInfo) {
        rl_cp.setVisibility(VISIBLE);
        cpLayout.setVisibility(View.VISIBLE);
        cpLayout.setOnClickListener(v -> CoreManager.notifyClients(SquareClient.class, SquareClient.METHOD_ON_ROOM_CP_TOP_MSG_CLICK, giftRecieveInfo.getTargetUid()));
        ImageLoadUtils.loadCircleImage(getContext(), giftRecieveInfo.getAvatar(), ivCpUser1, R.drawable.default_cover);
        ImageLoadUtils.loadCircleImage(getContext(), giftRecieveInfo.getTargetAvatar(), ivCpUser2, R.drawable.default_cover);
        setTvCpInviteContent(giftRecieveInfo, tvCpContent);

        final Point center = new Point();
        center.x = ResolutionUtils.getScreenWidth(getContext()) / 2;
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(cpLayout, "translationX", -UIUtil.dip2px(getContext(), 100), center.x - DisplayUtils.getScreenWidth(getContext()) / 2).setDuration(500);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.start();

        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(cpLayout, "alpha", 0.0F, 1.0F).setDuration(500);
        objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator1.start();

        avatarLeft.setVisibility(VISIBLE);
        avatarRight.setVisibility(VISIBLE);

        ImageLoadUtils.loadImage(avatarLeft.getContext(), giftRecieveInfo.getAvatar(), avatarLeft);
        ImageLoadUtils.loadImage(avatarRight.getContext(), giftRecieveInfo.getTargetAvatar(), avatarRight);

        SvgaUtils.getInstance(svga_cp.getContext()).cyclePlayAssetsAnim(svga_cp, "anim_room_cp_gift");

        //属性动画
        ObjectAnimator iv1Alpha = ObjectAnimator.ofFloat(avatarLeft, "alpha", 0f, 1f).setDuration(2000);
        ObjectAnimator iv2Alpha = ObjectAnimator.ofFloat(avatarRight, "alpha", 0f, 1f).setDuration(2000);
        float translationX = 10;
        ObjectAnimator translationRight = ObjectAnimator.ofFloat(avatarLeft, "translationX", 0, translationX).setDuration(2000);
        ObjectAnimator translationLeft = ObjectAnimator.ofFloat(avatarRight, "translationX", 0, -translationX).setDuration(2000);
        float translationY = 40;
        ObjectAnimator iv_cp_giftAlpha = ObjectAnimator.ofFloat(iv_cp_gift, "alpha", 0f, 1f).setDuration(2000);
        ObjectAnimator translationTop = ObjectAnimator.ofFloat(iv_cp_gift, "translationY", 0, -translationY).setDuration(2000);
        AnimatorSet animatorSet1 = new AnimatorSet();
        animatorSet1.playTogether(iv1Alpha, iv2Alpha, translationRight, translationLeft); //设置动画
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether(iv_cp_giftAlpha, translationTop); //设置动画
        AnimatorSet animatorSet3 = new AnimatorSet();
        animatorSet2.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                iv_cp_gift.setAlpha(0);
                if (!TextUtils.isEmpty(giftRecieveInfo.getCpVggUrl())) {
                    SvgaUtils.cyclePlayWebAnim(iv_cp_gift.getContext(), iv_cp_gift, giftRecieveInfo.getCpVggUrl());
                }
            }
        });
        animatorSet3.playSequentially(animatorSet1, animatorSet2);
        effectHandler.sendEmptyMessageDelayed(0, 6000);
        animatorSet3.start();
    }

    /**
     * CP顶部Notify  表白了，大家快来围观鸭
     */
    public void startCpTopNotifyEffect(GiftReceiveInfo giftRecieveInfo) {
        rl_cp.setVisibility(VISIBLE);
        cpLayout.setVisibility(View.VISIBLE);
        cpLayout.setOnClickListener(v -> CoreManager.notifyClients(SquareClient.class, SquareClient.METHOD_ON_ROOM_CP_TOP_MSG_CLICK, giftRecieveInfo.getTargetUid()));
        ImageLoadUtils.loadCircleImage(getContext(), giftRecieveInfo.getAvatar(), ivCpUser1, R.drawable.default_cover);
        ImageLoadUtils.loadCircleImage(getContext(), giftRecieveInfo.getTargetAvatar(), ivCpUser2, R.drawable.default_cover);
        setTvCpInviteContent(giftRecieveInfo, tvCpContent);

        final Point center = new Point();
        center.x = ResolutionUtils.getScreenWidth(getContext()) / 2;
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(cpLayout, "translationX", -UIUtil.dip2px(getContext(), 100), center.x - DisplayUtils.getScreenWidth(getContext()) / 2).setDuration(500);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.start();

        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(cpLayout, "alpha", 0.0F, 1.0F).setDuration(500);
        objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator1.start();

        effectHandler.sendEmptyMessageDelayed(0, 5000);
    }

    /**
     * 设置内容 天呐！xxx向xxx表白了，大家快来围观鸭
     */
    private void setTvCpInviteContent(GiftReceiveInfo giftReceiveInfo, TextView tvCpContent) {
        String userName1 = giftReceiveInfo.getInviteUserName();
        String userName2 = giftReceiveInfo.getRecUserName();
        if (!TextUtils.isEmpty(userName1) && userName1.length() > 6) {
            userName1 = userName1.substring(0, 6) + "...";
        }
        if (!TextUtils.isEmpty(userName2) && userName2.length() > 6) {
            userName2 = userName2.substring(0, 6) + "...";
        }

        int normalCore = 0xFFFFFFFF;
        int focusCore = getContext().getResources().getColor(R.color.color_311301);
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list.add(RichTextUtil.getRichTextMap("天呐！", normalCore));
        list.add(RichTextUtil.getRichTextMap(userName1, focusCore));
        list.add(RichTextUtil.getRichTextMap("向", normalCore));
        list.add(RichTextUtil.getRichTextMap(userName2, focusCore));
        list.add(RichTextUtil.getRichTextMap("表白了，大家快来围观鸭！", normalCore));
        SpannableStringBuilder builder = RichTextUtil.getSpannableStringFromList(list);
        tvCpContent.setText(builder);
    }

    /**
     * 设置内容 哇，恭喜xx将xxx赠与xx成为众人羡慕的全服第xx对CP，大家来祝福他们吧。
     */
    private void setTvCpAgreeContent(GiftReceiveInfo giftReceiveInfo, TextView tvCpContent) {
        String userName1 = giftReceiveInfo.getInviteUserName();
        String userName2 = giftReceiveInfo.getRecUserName();
        String giftUrl = giftReceiveInfo.getPicUrl();
        String cpNum = String.valueOf(giftReceiveInfo.getCpNum());
        if (!TextUtils.isEmpty(userName1) && userName1.length() > 6) {
            userName1 = userName1.substring(0, 6) + "...";
        }
        if (!TextUtils.isEmpty(userName2) && userName2.length() > 6) {
            userName2 = userName2.substring(0, 6) + "...";
        }

        int normalCore = 0xFFFFFFFF;
        int focusCore = getContext().getResources().getColor(R.color.color_311301);
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list.add(RichTextUtil.getRichTextMap("哇，恭喜", normalCore));
        list.add(RichTextUtil.getRichTextMap(userName1, focusCore));
        list.add(RichTextUtil.getRichTextMap(" 将 ", normalCore));
        SpannableStringBuilder builder = RichTextUtil.getSpannableStringFromList(list);

        //插入礼物图标
        builder.append(BaseConstant.GIFT_PLACEHOLDER);
        UrlImageSpan imageSpan = new UrlImageSpan(getContext(), giftUrl, tvCpContent);
        imageSpan.setImgWidth(com.juxiao.library_utils.DisplayUtils.dip2px(getContext(), 25));
        imageSpan.setImgHeight(com.juxiao.library_utils.DisplayUtils.dip2px(getContext(), 25));
        builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(), builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        list = new ArrayList<>();
        list.add(RichTextUtil.getRichTextMap(" 赠予 ", normalCore));
        list.add(RichTextUtil.getRichTextMap(userName2, focusCore));
        list.add(RichTextUtil.getRichTextMap("成为众人羡慕的全服第", normalCore));
        list.add(RichTextUtil.getRichTextMap(cpNum, focusCore));
        list.add(RichTextUtil.getRichTextMap("对CP！", normalCore));
        builder.append(RichTextUtil.getSpannableStringFromList(list));

        tvCpContent.setText(builder);
    }

    /**
     * 礼物全屏特效
     *
     * @param url
     * @throws MalformedURLException
     */
    public void drawSvgaEffect(String url) throws Exception {
        SVGAParser parser = new SVGAParser(getContext());
        parser.parse(new URL(url), new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity videoItem) {
                SVGADrawable drawable = new SVGADrawable(videoItem);
                svgaImageView.setImageDrawable(drawable);
                svgaImageView.startAnimation();
                svgaBg.setVisibility(VISIBLE);
                ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(svgaBg, "alpha", 0.0F, 2.0F).setDuration(800);
                objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
                objectAnimator1.start();
            }

            @Override
            public void onError() {

            }
        });

    }

    private void deleteAnim() {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(container, "translationX", container.getX(), ResolutionUtils.getScreenWidth(getContext())).setDuration(500);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.start();
        ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(cpLayout, "translationX", cpLayout.getX(), ResolutionUtils.getScreenWidth(getContext())).setDuration(500);
        objectAnimator2.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator2.start();
        ObjectAnimator objectAnimator3 = ObjectAnimator.ofFloat(cpLayout, "alpha", 1.0F, 0.0F).setDuration(500);
        objectAnimator3.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator3.start();

        SvgaUtils.closeSvga(svga_cp);
        SvgaUtils.closeSvga(iv_cp_gift);
        rl_cp.setVisibility(GONE);
        avatarLeft.setVisibility(GONE);
        avatarRight.setVisibility(GONE);
        avatarLeft.setTranslationX(-10);
        avatarRight.setTranslationX(10);
        iv_cp_gift.setTranslationY(40);
        iv_cp_gift.setAlpha(0);

        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(container, "alpha", 1.0F, 0.0F).setDuration(500);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (giftEffectListener != null) {
                    isAnim = false;
                    giftEffectListener.onGiftEffectEnd();
                }
            }
        });
        objectAnimator1.start();
    }

    public void release() {
        effectHandler.removeMessages(0);
//        svgaImageView.stopAnimation(true);
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onFinished() {
        svgaBg.setVisibility(GONE);
    }

    @Override
    public void onRepeat() {

    }

    @Override
    public void onStep(int i, double v) {

    }

    public interface GiftEffectListener {
        void onGiftEffectEnd();
    }

    private static class EffectHandler extends Handler {
        private WeakReference<GiftEffectView> effectViewWeakReference;

        public EffectHandler(GiftEffectView giftEffectView) {
            effectViewWeakReference = new WeakReference<>(giftEffectView);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (effectViewWeakReference != null) {
                final GiftEffectView giftEffectView = effectViewWeakReference.get();
                if (giftEffectView != null) {
                    if (msg.what == 0) {
                        giftEffectView.deleteAnim();
                    }
                }
            }
        }

    }
}
