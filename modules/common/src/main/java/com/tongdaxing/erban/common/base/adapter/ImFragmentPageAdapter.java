package com.tongdaxing.erban.common.base.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Chen on 2019/4/17.
 */
public class ImFragmentPageAdapter extends FragmentPagerAdapter {

    private List<PagerContent> mContents;

    public ImFragmentPageAdapter(FragmentManager fm, List<PagerContent> contents) {
        super(fm);
        this.mContents = contents;
    }

    @Override
    public Fragment getItem(int position) {
        try {
            PagerContent content = mContents.get(position);
            Fragment baseFragment = content.mFragClass.getConstructor().newInstance();
            baseFragment.setArguments(content.mBundle);
            return baseFragment;
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public int getCount() {
        if (mContents != null) {
            return mContents.size();
        }
        return 0;
    }

    public static class PagerContent {
        public Class<? extends Fragment> mFragClass;
        public String mCate;
        private Bundle mBundle;

        public PagerContent(Class<? extends Fragment> fragClass, Bundle bundle, String cate) {
            this.mFragClass = fragClass;
            this.mBundle = bundle;
            this.mCate = cate;
        }

        public PagerContent(Class<? extends Fragment> fragClass, Bundle bundle) {
            this.mFragClass = fragClass;
            this.mBundle = bundle;
        }
    }
}