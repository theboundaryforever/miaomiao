package com.tongdaxing.erban.common.ui.message.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;

/**
 * Created by chenran on 2017/10/3.
 */

public class FriendListAdapter extends BaseQuickAdapter<NimUserInfo, BaseViewHolder> {

    public FriendListAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, NimUserInfo item) {
        ImageView imageView = helper.getView(R.id.imageView);
        TextView textView = helper.getView(R.id.tv_userName);
        textView.setText(item.getName());
        ImageLoadUtils.loadImage(imageView.getContext(), item.getAvatar(), imageView);
        imageView.setOnClickListener(view -> {
            if ("90000000".equals(item.getAccount())) {
                return;
            }
            NewUserInfoActivity.start(mContext, JavaUtil.str2long(item.getAccount()));
        });
    }
}
