package com.tongdaxing.erban.common.room.function;

import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;

import java.util.List;

/**
 * Created by Chen on 2019/6/6.
 */
public interface IRoomFunctionView {
    void updateMusicView();

    void updateRankView(List<RoomConsumeInfo> data);
}
