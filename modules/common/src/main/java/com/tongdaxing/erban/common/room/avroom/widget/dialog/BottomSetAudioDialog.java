package com.tongdaxing.erban.common.room.avroom.widget.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kyleduo.switchbutton.SwitchButton;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.room.audio.activity.MusicActivity;
import com.tongdaxing.erban.common.room.avroom.adapter.SoundFilterAdapter;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.GridSpaceItemDecoration;
import com.tongdaxing.erban.common.ui.widget.marqueeview.Utils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RoomAudioSettingManager;
import com.tongdaxing.xchat_core.music.IMusicCoreClient;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 调音变声dialog
 */
public class BottomSetAudioDialog extends BottomSheetDialog implements CompoundButton.OnCheckedChangeListener, SeekBar.OnSeekBarChangeListener {
    @BindView(R2.id.dialog_set_audio_song_name)
    TextView dialogSetAudioSongName;
    @BindView(R2.id.dialog_set_audio_song_list)
    ImageView dialogSetAudioSongList;
    @BindView(R2.id.dialog_set_audio_song_name_and_list)
    LinearLayout dialogSetAudioSongNameAndList;
    @BindView(R2.id.dialog_set_audio_last_one)
    ImageView dialogSetAudioLastOne;
    @BindView(R2.id.dialog_set_audio_play_or_pause)
    ImageView dialogSetAudioPlayOrPause;
    @BindView(R2.id.dialog_set_audio_next_one)
    ImageView dialogSetAudioNextOne;
    @BindView(R2.id.dialog_set_audio_play_seek)
    SeekBar dialogSetAudioPlaySeek;
    @BindView(R2.id.dialog_set_audio_play_mode)
    ImageView dialogSetAudioPlayMode;
    @BindView(R2.id.dialog_set_audio_play_content)
    LinearLayout dialogSetAudioPlayContent;
    @BindView(R2.id.sb_audio_monitor)
    SwitchButton sbAudioMonitor;
    @BindView(R2.id.music_seek)
    SeekBar musicSeek;
    @BindView(R2.id.music_number)
    TextView musicNumber;
    @BindView(R2.id.voice_seek)
    SeekBar voiceSeek;
    @BindView(R2.id.voice_number)
    TextView voiceNumber;
    @BindView(R2.id.sound_filter_list)
    RecyclerView soundFilterList;
    @BindView(R2.id.dialog_set_audio_tv_current_time)
    TextView dialogSetAudioTvCurrentTime;
    @BindView(R2.id.dialog_set_audio_tv_music_total_length)
    TextView dialogSetAudioTvMusicTotalLength;
    private Context context;
    private RoomAudioSettingManager roomAudioSettingManager;

    private TextView musicTv, voiceTv;

    //音乐播放器
    private int[] icons = {R.drawable.room_music_repeat_list, R.drawable.room_music_single_recycle, R.drawable.room_music_random};
    private String[] strs = {"列表循环", "单曲循环", "随机播放"};

    private boolean isSBProgressChangedByUserTouch = false;
    private static final String TAG = "BottomSetAudioDialog";

    public BottomSetAudioDialog(Context context) {
        super(context, R.style.RoomBottomSheetDialog);
        this.context = context;
        roomAudioSettingManager = RoomAudioSettingManager.getInstance(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
        CoreUtils.register(this);
        setCanceledOnTouchOutside(true);
        setContentView(R.layout.dialog_set_audio);
        ButterKnife.bind(this);
        FrameLayout bottomSheet = findViewById(android.support.design.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(false);
            BottomSheetBehavior.from(bottomSheet).setPeekHeight(
                    (int) context.getResources().getDimension(R.dimen.dialog_set_audio_height) +
                            (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0));
        }
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        assert windowManager != null;
        Display d = windowManager.getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        assert getWindow() != null;
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

        init();
        //音乐播放器
        updateView();
        initPlayMode();
    }

    private void init() {
        SwitchButton audioMonitorSb = findViewById(R.id.sb_audio_monitor);
        assert audioMonitorSb != null;
        audioMonitorSb.setChecked(roomAudioSettingManager.isAudioMonitorEnable());
        audioMonitorSb.setOnCheckedChangeListener(this);

        musicTv = findViewById(R.id.music_number);
        assert musicSeek != null;
        assert musicTv != null;
        musicSeek.setMax(100);
        musicSeek.setOnSeekBarChangeListener(this);
        int musicVolume = CoreManager.getCore(IPlayerCore.class).getCurrentVolume();
        musicSeek.setProgress(musicVolume);
        musicTv.setText(String.valueOf(musicVolume));

        voiceTv = findViewById(R.id.voice_number);
        assert voiceSeek != null;
        assert voiceTv != null;
        voiceSeek.setMax(100);
        voiceSeek.setOnSeekBarChangeListener(this);
        int voiceVolume = CoreManager.getCore(IPlayerCore.class).getCurrentRecordingVolume();
        voiceSeek.setProgress(voiceVolume);
        voiceTv.setText(String.valueOf(voiceVolume));

        RecyclerView soundFilterList = findViewById(R.id.sound_filter_list);
        assert soundFilterList != null;
        soundFilterList.setLayoutManager(new GridLayoutManager(context, 3));
        soundFilterList.addItemDecoration(new GridSpaceItemDecoration(ScreenUtil.dip2px(25), ScreenUtil.dip2px(12)));
        List<String> soundFilterTexts = new ArrayList<>();
        soundFilterTexts.add(context.getResources().getString(R.string.sound_filter_normal));
        soundFilterTexts.add(context.getResources().getString(R.string.sound_filter_uncle));
        soundFilterTexts.add(context.getResources().getString(R.string.sound_filter_boy));
        soundFilterTexts.add(context.getResources().getString(R.string.sound_filter_female));
        soundFilterTexts.add(context.getResources().getString(R.string.sound_filter_lolita));
        soundFilterTexts.add(context.getResources().getString(R.string.sound_filter_monster));
        SoundFilterAdapter adapter = new SoundFilterAdapter(soundFilterTexts);
        adapter.setCurrSelectPosition(roomAudioSettingManager.getAudioFilter());
        adapter.setOnItemClickListener((adapter1, view, position) -> {
            switch (position) {
                case 0:
                    roomAudioSettingManager.setAudioFilter(RoomAudioSettingManager.VOICE_FILTER_TYPE_NORMAL);
                    break;
                case 1:
                    roomAudioSettingManager.setAudioFilter(RoomAudioSettingManager.VOICE_FILTER_TYPE_UNCLE);
                    break;
                case 2:
                    roomAudioSettingManager.setAudioFilter(RoomAudioSettingManager.VOICE_FILTER_TYPE_BOY);
                    break;
                case 3:
                    roomAudioSettingManager.setAudioFilter(RoomAudioSettingManager.VOICE_FILTER_TYPE_FEMALE);
                    break;
                case 4:
                    roomAudioSettingManager.setAudioFilter(RoomAudioSettingManager.VOICE_FILTER_TYPE_LOLITA);
                    break;
                case 5:
                    roomAudioSettingManager.setAudioFilter(RoomAudioSettingManager.VOICE_FILTER_TYPE_MONSTER);
                    break;
            }
            UmengEventUtil.getInstance().onVoiceChangerClick(context, adapter.getItem(position));//加友盟统计
            adapter.setCurrSelectPosition(position);
        });
        soundFilterList.setAdapter(adapter);

        dialogSetAudioPlaySeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isSBProgressChangedByUserTouch = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                CoreManager.getCore(IPlayerCore.class).setAudioMixCurrPosition(seekBar.getProgress());
                isSBProgressChangedByUserTouch = false;
            }
        });
        setOnMicChange();
    }

    public void setOnMicChange() {
        boolean isOnMic = AvRoomDataManager.get().isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        dialogSetAudioSongNameAndList.setVisibility(isOnMic ? View.VISIBLE : View.GONE);
        dialogSetAudioPlayContent.setVisibility(isOnMic ? View.VISIBLE : View.GONE);
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDismissEvent(SetAudioPlayerEvent.OnDismissDialog onDismissDialog) {
        dismiss();
    }

    @Override
    public void dismiss() {
        try {
            release();
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 监听 开启/关闭
     */
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
        roomAudioSettingManager.enableAudioMonitor(checked);
    }

    /**
     * 音乐、人声 音量调节
     */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        if (seekBar == musicSeek) {
            CoreManager.getCore(IPlayerCore.class).seekVolume(progress);
            musicTv.setText(String.valueOf(progress));
        } else {
            CoreManager.getCore(IPlayerCore.class).seekRecordingVolume(progress);
            voiceTv.setText(String.valueOf(progress));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicProgressUpdate(long total, long current) {
        int progress = (int) (current * 100 / total);
        L.debug(TAG, "total = %d, current = %d, progress = %d, isSBProgressChangedByUserTouch = %b", total, current, progress, isSBProgressChangedByUserTouch);
        if (isSBProgressChangedByUserTouch) {
            return;
        }
        if ("暂无歌曲播放".equals(dialogSetAudioSongName.getText().toString())) {
            MusicLocalInfo musicLocalInfo = CoreManager.getCore(IPlayerCore.class).getCurrent();
            if (!TextUtils.isEmpty(musicLocalInfo.getSongName())) {
                dialogSetAudioSongName.setText(musicLocalInfo.getSongName());
            }
        }
        int state = CoreManager.getCore(IPlayerCore.class).getState();
        if (state == IPlayerCore.STATE_PLAY) {
            dialogSetAudioPlayOrPause.setImageResource(R.drawable.room_music_pause);
        } else {
            dialogSetAudioPlayOrPause.setImageResource(R.drawable.room_music_play);
        }
        dialogSetAudioTvCurrentTime.setText(TimeUtils.getFormatTimeString(current, "min:sec"));
        dialogSetAudioTvMusicTotalLength.setText(TimeUtils.getFormatTimeString(total, "min:sec"));
        dialogSetAudioPlaySeek.setProgress(progress);
    }

    public void updateView() {
        MusicLocalInfo current = CoreManager.getCore(IPlayerCore.class).getCurrent();
        L.debug(TAG, "no parameter updateView(): MusicLocalInfo.getSongName() = %s", current == null
                ? "current is null" : (current.isValid() ? current.getSongName() : "current MusicLocalInfo is invalid"));
        updateView(current);
    }


    private void updateView(MusicLocalInfo current) {
        try {
            if (current != null && current.isValid()) {
                if (!TextUtils.isEmpty(current.getSongName())) {
                    dialogSetAudioSongName.setText(current.getSongName());
                } else {
                    noMusicPlayStatus();
                }
                long curDur = CoreManager.getCore(IPlayerCore.class).getCurrentDuration();
                if (curDur == -1) {
                    curDur = 0;
                }
                dialogSetAudioTvCurrentTime.setText(TimeUtils.getFormatTimeString(curDur, "min:sec"));
                long totalDur = CoreManager.getCore(IPlayerCore.class).getTotalDuration();
                int progress = 0;
                if (totalDur == 0) {
                    dialogSetAudioTvCurrentTime.setText(null);
                    dialogSetAudioTvMusicTotalLength.setText(null);
                } else {
                    progress = (int) (curDur * 100 / totalDur);
                    dialogSetAudioTvMusicTotalLength.setText(TimeUtils.getFormatTimeString(totalDur, "min:sec"));
                }

                dialogSetAudioPlaySeek.setProgress(progress);
                int state = CoreManager.getCore(IPlayerCore.class).getState();
                if (state == IPlayerCore.STATE_PLAY) {
                    dialogSetAudioPlayOrPause.setImageResource(R.drawable.room_music_pause);
                } else {
                    dialogSetAudioPlayOrPause.setImageResource(R.drawable.room_music_play);
                }
            } else {
                noMusicPlayStatus();
            }
        } catch (Exception e) {
            L.uncaughtException(e);
            noMusicPlayStatus();
        }
    }

    private void noMusicPlayStatus() {
        dialogSetAudioSongName.setText("暂无歌曲播放");
        dialogSetAudioPlayOrPause.setImageResource(R.drawable.room_music_play);
        dialogSetAudioTvCurrentTime.setText(null);
        dialogSetAudioTvMusicTotalLength.setText(null);
        dialogSetAudioPlaySeek.setProgress(0);
    }

    @CoreEvent(coreClientClass = IMusicCoreClient.class)
    public void delOperationRefresh(long id, boolean isHidePlayer) {
        if (isHidePlayer) {
            noMusicPlayStatus();
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onRefreshPlayMode(int playMode) {
        dialogSetAudioPlayMode.setImageResource(icons[playMode]);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(MusicLocalInfo localMusicInfo) {
        updateView(localMusicInfo);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(MusicLocalInfo localMusicInfo) {
        updateView(localMusicInfo);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop(MusicLocalInfo localMusicInfo) {
        updateView(localMusicInfo);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onCurrentMusicUpdate(MusicLocalInfo localMusicInfo) {
        updateView(localMusicInfo);
    }

    private void initPlayMode() {
        CoreManager.getCore(IPlayerCore.class).setPlayMode(AvRoomDataManager.currPlayMode % icons.length);
        dialogSetAudioPlayMode.setImageResource(icons[AvRoomDataManager.currPlayMode % icons.length]);
    }

    @OnClick(R2.id.dialog_set_audio_song_list)
    public void onDialogSetAudioSongListClicked() {
        MusicActivity.Companion.start((Activity) context);
    }

    @OnClick(R2.id.dialog_set_audio_last_one)
    public void onDialogSetAudioLastOneClicked() {
        try {
            List<MusicLocalInfo> localMusicInfoList1 = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
            if (localMusicInfoList1 != null && localMusicInfoList1.size() > 0) {
                int result = CoreManager.getCore(IPlayerCore.class).switchPlayLast();
                if (result < 0) {
                    if (result == -3) {
                        ((BaseMvpActivity) getContext()).toast("播放列表中还没有歌曲哦！");
                    } else {
                        ((BaseMvpActivity) getContext()).toast("播放失败，文件异常");
                    }
                }
            } else {
                MusicActivity.Companion.start((Activity) getContext());
            }
        } catch (Exception e) {
            SingleToastUtil.showToast(getContext(), "播放失败，请刷新再试", Toast.LENGTH_LONG);
        }
    }

    @OnClick(R2.id.dialog_set_audio_play_or_pause)
    public void onDialogSetAudioPlayOrPauseClicked() {
        try {
            List<MusicLocalInfo> localMusicInfoList = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
            if (localMusicInfoList != null && localMusicInfoList.size() > 0) {
                int state = CoreManager.getCore(IPlayerCore.class).getState();
                if (state == IPlayerCore.STATE_PLAY) {
                    CoreManager.getCore(IPlayerCore.class).pause();
                } else if (state == IPlayerCore.STATE_PAUSE) {
                    CoreManager.getCore(IPlayerCore.class).play((MusicLocalInfo) null);
                } else {
                    int result = CoreManager.getCore(IPlayerCore.class).switchPlayNext();
                    if (result < 0) {
                        if (result == -3) {
                            ((BaseMvpActivity) getContext()).toast("播放列表中还没有歌曲哦！");
                        } else {
                            ((BaseMvpActivity) getContext()).toast("播放失败，文件异常");
                        }
                    }
                }
            } else {
                MusicActivity.Companion.start((Activity) getContext());
            }
        } catch (Exception e) {
            SingleToastUtil.showToast(getContext(), "播放失败，请刷新再试", Toast.LENGTH_LONG);
        }
    }

    @OnClick(R2.id.dialog_set_audio_next_one)
    public void onDialogSetAudioNextOneClicked() {
        try {
            List<MusicLocalInfo> localMusicInfoList1 = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
            if (localMusicInfoList1 != null && localMusicInfoList1.size() > 0) {
                int result = CoreManager.getCore(IPlayerCore.class).switchPlayNext();
                if (result < 0) {
                    if (result == -3) {
                        ((BaseMvpActivity) getContext()).toast("播放列表中还没有歌曲哦！");
                    } else {
                        ((BaseMvpActivity) getContext()).toast("播放失败，文件异常");
                    }
                }
            } else {
                MusicActivity.Companion.start((Activity) getContext());
            }
        } catch (Exception e) {
            SingleToastUtil.showToast(getContext(), "播放失败，请刷新再试", Toast.LENGTH_LONG);
        }
    }

    @OnClick(R2.id.dialog_set_audio_play_mode)
    public void onDialogSetAudioPlayModeClicked() {
        try {
            if (AvRoomDataManager.currPlayMode % icons.length == 0) {//经过一边循环之后重置。
                AvRoomDataManager.currPlayMode = icons.length;
            }
            AvRoomDataManager.currPlayMode++;
            CoreManager.getCore(IPlayerCore.class).setPlayMode(AvRoomDataManager.currPlayMode % icons.length);
            dialogSetAudioPlayMode.setImageResource(icons[AvRoomDataManager.currPlayMode % icons.length]);
            SingleToastUtil.showToast(strs[AvRoomDataManager.currPlayMode % icons.length]);
        } catch (Exception e) {
            SingleToastUtil.showToast(getContext(), "操作失败，请刷新再试", Toast.LENGTH_LONG);
        }
    }

    public void release() {
        CoreManager.removeClient(this);
        CoreUtils.unregister(this);
    }
}
