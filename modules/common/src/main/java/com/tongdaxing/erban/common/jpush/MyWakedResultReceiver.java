package com.tongdaxing.erban.common.jpush;

import android.content.Context;

import com.tcloud.core.log.L;

import cn.jpush.android.service.WakedResultReceiver;

/**
 * 用户自定义 Receiver 接收被拉起回调
 * <p>
 * Created by zhangjian on 2019/7/12.
 */
public class MyWakedResultReceiver extends WakedResultReceiver {
    private static final String TAG = "MyWakedResultReceiver";

    @Override
    public void onWake(Context context, int i) {
        super.onWake(context, i);
        L.debug(TAG, "onWake(context) i = %d", i);
    }
}
