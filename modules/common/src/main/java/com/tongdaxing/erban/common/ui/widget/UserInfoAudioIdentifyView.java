package com.tongdaxing.erban.common.ui.widget;

import android.content.Context;
import android.media.AudioManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_framework.media.AudioPlayer;
import com.tongdaxing.xchat_framework.media.OnPlayListener;

/**
 * Function:
 * Author: Edward on 2019/3/27
 */
public class UserInfoAudioIdentifyView extends RelativeLayout {

    //    private SecondTimeView secondTimeView;
    private ImageView playIv;

    private AudioPlayer audioPlayer;
    private SVGAImageView svgaImageview;
    private ImageView ivUserInfoAudioPlay;
    private int audioLength;
    private String audioFileUrl;
    private TextView tvVoice;
    private boolean isInterceptClick;
    private int playIconResId, pauseIconResId;
    private OnPlayListener onPlayListener = new OnPlayListener() {
        @Override
        public void onPrepared() {
            isInterceptClick = false;
            //这里场景只有play才会prepare，所以可以写在这里
            setupAudioPlayView();
        }

        @Override
        public void onCompletion() {
            setupAudioStopView();
            isInterceptClick = false;
            ivUserInfoAudioPlay.setVisibility(VISIBLE);
            if (svgaImageview != null) {
                svgaImageview.pauseAnimation();
                svgaImageview.setVisibility(INVISIBLE);
            }
        }

        @Override
        public void onInterrupt() {
            setupAudioStopView();
            isInterceptClick = false;
        }

        @Override
        public void onError(String error) {
            setupAudioStopView();
            isInterceptClick = false;
        }

        @Override
        public void onPlaying(long curPosition) {
        }
    };
    private SVGAParser parser;

    public UserInfoAudioIdentifyView(Context context) {
        super(context);
        init(context, null);
    }

    public UserInfoAudioIdentifyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public UserInfoAudioIdentifyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setAudioDataSource(int audioLength, String audioFileUrl, String str) {
        this.audioLength = audioLength;
        this.audioFileUrl = audioFileUrl;
        tvVoice.setText(str);
//        secondTimeView.setCurrCount(audioLength);
        audioPlayer.setDataSource(audioFileUrl);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.layout_user_info_audio_play_transparent, this);
        playIconResId = R.drawable.user_info_ic_play;
        pauseIconResId = R.drawable.user_info_ic_stop;
        tvVoice = findViewById(R.id.tv_voice);
        svgaImageview = findViewById(R.id.svga_imageview);
        ivUserInfoAudioPlay = findViewById(R.id.iv_user_info_audio_play);
        SvgaUtils.getInstance(context).cyclePlayAssetsAnim(svgaImageview, "anim_user_info_audio_card.svga", View.INVISIBLE);
//        secondTimeView = findViewById(R.id.second_time);
        playIv = findViewById(R.id.iv_play);
        setOnClickListener(view -> {
            if (isInterceptClick) {//正在准备播放音频的时候，不允许点击（避免还没开始就又stop了）
                return;
            }
            if (!audioPlayer.isPlaying()) {
                start();
            } else {
                stop();
            }
        });
        audioPlayer = AudioPlayAndRecordManager.getInstance().createAudioPlayer(context.getApplicationContext(), onPlayListener);
    }

    public void start() {
        if (audioFileUrl != null) {
            isInterceptClick = true;
            audioPlayer.start(AudioManager.STREAM_MUSIC);
            ivUserInfoAudioPlay.setVisibility(INVISIBLE);
            if (svgaImageview != null) {
                svgaImageview.setVisibility(VISIBLE);
                svgaImageview.startAnimation();
            }
        }
    }

    /**
     * 释放资源（放onDestroy前面，否则有泄漏风险）
     */
    public void release() {
        AudioPlayAndRecordManager.getInstance().releaseAudioPlayer(audioPlayer);
        onPlayListener = null;
        if (svgaImageview != null) {
            svgaImageview.stopAnimation();
            svgaImageview = null;
        }
    }

    private void setupAudioPlayView() {
        playIv.setImageResource(pauseIconResId);
//        secondTimeView.startCount(audioLength, 0);
    }

    private void setupAudioStopView() {
        playIv.setImageResource(playIconResId);
//        secondTimeView.stopCount();
//        secondTimeView.setCurrCount(audioLength);
    }

    public void stop() {
        if (audioPlayer.isPlaying()) {
            audioPlayer.stop();
            ivUserInfoAudioPlay.setVisibility(VISIBLE);
            if (svgaImageview != null) {
                svgaImageview.pauseAnimation();
                svgaImageview.setVisibility(INVISIBLE);
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (svgaImageview != null) {
            SvgaUtils.closeSvga(svgaImageview);
        }
    }
}
