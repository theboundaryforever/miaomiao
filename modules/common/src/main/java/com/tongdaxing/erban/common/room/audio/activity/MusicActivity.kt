package com.tongdaxing.erban.common.room.audio.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.text.TextUtils
import android.view.View
import android.widget.LinearLayout
import android.widget.SeekBar
import com.netease.nim.uikit.NimUIKit.getContext
import com.tongdaxing.erban.common.R
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity
import com.tongdaxing.erban.common.room.audio.adapter.MyMusicAdapter.*
import com.tongdaxing.erban.common.room.audio.presenter.MusicPresenter
import com.tongdaxing.erban.common.room.audio.view.IMusicView
import com.tongdaxing.erban.common.room.audio.widget.VoiceSeekDialog
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter
import com.tongdaxing.erban.libcommon.widget.ButtonItem
import com.tongdaxing.xchat_core.home.TabInfo
import com.tongdaxing.xchat_core.manager.AvRoomDataManager
import com.tongdaxing.xchat_core.music.IMusicCore
import com.tongdaxing.xchat_core.music.IMusicCoreClient
import com.tongdaxing.xchat_core.music.IMusicDownloaderCore
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo
import com.tongdaxing.xchat_core.player.IPlayerCore
import com.tongdaxing.xchat_core.player.IPlayerCoreClient
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo
import com.tongdaxing.xchat_framework.coremanager.CoreEvent
import com.tongdaxing.xchat_framework.coremanager.CoreManager
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil
import com.tongdaxing.xchat_framework.util.util.TimeUtils
import kotlinx.android.synthetic.main.activity_music.*
import kotlinx.android.synthetic.main.layout_music_player.*
import java.io.File
import java.util.*

/**
 * Function:音乐页面
 * Author: Edward on 2019/2/13
 */
@CreatePresenter(MusicPresenter::class)
class MusicActivity : BaseMvpActivity<IMusicView, MusicPresenter>(), IMusicView,
        CommonMagicIndicatorAdapter.OnItemSelectListener, SeekBar.OnSeekBarChangeListener {
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        isSBProgressChangedByUserTouch = true
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        CoreManager.getCore(IPlayerCore::class.java).setAudioMixCurrPosition(seekBar!!.progress)
        isSBProgressChangedByUserTouch = false
    }

    override fun onItemClick(myMusicLocalInfo: MyMusicInfo, view: View?, position: Int) {
        try {
            if (myMusicLocalInfo.fileStatus == FILE_STATUS_PLAY ||
                    myMusicLocalInfo.fileStatus == FILE_STATUS_STOP) {
                val state = CoreManager.getCore(IPlayerCore::class.java).state
                if (state == IPlayerCore.STATE_PLAY) {
                    var cur = CoreManager.getCore(IPlayerCore::class.java).current
                    if (myMusicLocalInfo.localUri == cur.localUri) {
                        CoreManager.getCore(IPlayerCore::class.java).pause()
                    } else {
                        CoreManager.getCore(IPlayerCore::class.java).stop()
                        playMusic(myMusicLocalInfo)
                    }
                } else if (state == IPlayerCore.STATE_PAUSE) {
                    var cur = CoreManager.getCore(IPlayerCore::class.java).current
                    if (myMusicLocalInfo.localUri == cur.localUri) {
                        playMusic(myMusicLocalInfo)
                    } else {
                        CoreManager.getCore(IPlayerCore::class.java).stop()
                        playMusic(myMusicLocalInfo)
                    }
                } else {
                    playMusic(myMusicLocalInfo)
                }
                tv_music_name.text = myMusicLocalInfo.title
                rl_player.visibility = View.VISIBLE
            } else if (myMusicLocalInfo.fileStatus == FILE_STATUS_DOWNLOAD) {
                CoreManager.getCore(IMusicDownloaderCore::class.java).addHotMusicToDownQueue(convertBean(myMusicLocalInfo))
            }
        } catch (e: Exception) {
            toast("播放失败，请刷新再试")
        }
    }

    private fun convertBean(myMusicInfo: MyMusicInfo): HotMusicInfo {
        val localMusicInfo = HotMusicInfo()
        localMusicInfo.id = myMusicInfo.id
        localMusicInfo.singerName = myMusicInfo.artist
        localMusicInfo.singName = myMusicInfo.title
        localMusicInfo.singUrl = myMusicInfo.url.toString()
        return localMusicInfo
    }

    private fun playMusic(myMusicInfo: MyMusicInfo) {
        var localUri = myMusicInfo.localUri
        if (TextUtils.isEmpty(localUri)) {
            var list = CoreManager.getCore(IPlayerCore::class.java).playerListMusicInfos
            for (i in 0 until list.size) {
                if (list[i].remoteUri == myMusicInfo.url) {
                    localUri = list[i].localUri
                    myMusicInfo.localUri = localUri
                    break
                }
            }
            if (TextUtils.isEmpty(localUri)) {
                toast("播放失败，音乐文件不存在")
                return
            }
        }
        val result = CoreManager.getCore(IPlayerCore::class.java).play(localUri)
        if (result < 0) {
            toast("播放失败，文件异常")
        }
    }


    companion object {
        fun start(activity: Activity) {
            var intent = Intent(activity, MusicActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onItemSelect(position: Int) {
        view_pager.currentItem = position
    }

    private var mMsgIndicatorAdapter: CommonMagicIndicatorAdapter? = null
    private var currMusicInfo: MusicLocalInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music)
        setSwipeBackEnable(false)//禁止右滑退出
        createLocalMusicFolder()
        initPlayer()
        init()
        initNavigationBar()
    }

    private fun switchPlayMode() {
        if (AvRoomDataManager.currPlayMode % icons.size == 0) {//经过一边循环之后重置。
            AvRoomDataManager.currPlayMode = icons.size
        }
        AvRoomDataManager.currPlayMode++
        CoreManager.getCore(IPlayerCore::class.java).playMode = AvRoomDataManager.currPlayMode % icons.size
        iv_play_mode.setImageResource(icons[AvRoomDataManager.currPlayMode % icons.size])
        SingleToastUtil.showToast(strs[AvRoomDataManager.currPlayMode % icons.size])
        CoreManager.notifyClients(IPlayerCoreClient::class.java, IPlayerCoreClient.METHOD_ON_REFRESH_PLAY_MODE, AvRoomDataManager.currPlayMode % icons.size)
    }

    private fun initPlayer() {
        try {
            val current = CoreManager.getCore(IPlayerCore::class.java).current
            if (current != null && current.isValid) {
                val totalDur = CoreManager.getCore(IPlayerCore::class.java).totalDuration
                val curDur = CoreManager.getCore(IPlayerCore::class.java).currentDuration
                pb_music.progress = (curDur * 100 / totalDur).toInt()
                val mode = CoreManager.getCore(IPlayerCore::class.java).playMode
                iv_play_mode.setImageResource(icons[mode])

                tv_current_time.text = TimeUtils.getFormatTimeString(curDur.toLong(), "min:sec")
                tv_music_total_length.text = resources.getString(R.string.music_play_control_dur, TimeUtils.getFormatTimeString(totalDur.toLong(), "min:sec"))
                rl_player.visibility = View.VISIBLE
                tv_music_name.text = current.songName
                val state = CoreManager.getCore(IPlayerCore::class.java).state
                if (state == IPlayerCore.STATE_PLAY) {
                    iv_player.setImageResource(R.drawable.ic_player_play)
                } else {
                    iv_player.setImageResource(R.drawable.ic_player_stop)
                }
            } else {
                rl_player.visibility = View.GONE
            }
        } catch (e: Exception) {
            toast("播放失败，请刷新再试")
        }
    }

    /**
     * 创建本地音乐文件夹
     */
    private fun createLocalMusicFolder(): File {
        var pathStr = CoreManager.getCore(IMusicCore::class.java).userStoragePath
        var file = File(pathStr)
        if (!file.exists()) {
            file.mkdir()
        }
        return file
    }

    private fun showMoreItems() {
        var current = CoreManager.getCore(IPlayerCore::class.java).current
        var buttonItems = ArrayList<ButtonItem>()
        var buttonItem1 = ButtonItem("色情") {
            mvpPresenter.reportMusic(current.songId, "色情")
        }
        var buttonItem2 = ButtonItem("不良信息") {
            mvpPresenter.reportMusic(current.songId, "不良信息")
        }
        var buttonItem3 = ButtonItem("广告") {
            mvpPresenter.reportMusic(current.songId, "广告")
        }
        var buttonItem4 = ButtonItem("歌名不符合歌曲内容") {
            mvpPresenter.reportMusic(current.songId, "歌名不符合歌曲内容")
        }
        buttonItems.add(buttonItem1)
        buttonItems.add(buttonItem2)
        buttonItems.add(buttonItem3)
        buttonItems.add(buttonItem4)
        var dialogManager = dialogManager
        if (dialogManager != null) {
            dialogManager.showCommonPopupDialog(buttonItems, "取消")
        }
    }

    private fun init() {
        pb_music.max = 100
        pb_music.setOnSeekBarChangeListener(this)
        iv_play_equalizer.setOnClickListener {
            var voiceSeekDialog = VoiceSeekDialog(this)
            voiceSeekDialog.show()
        }
        iv_play_mode.setOnClickListener {
            switchPlayMode()
        }
        tv_report.setOnClickListener {
            showMoreItems()
        }
        iv_go_back.setOnClickListener {
            finish()
        }
        iv_music_more.setOnClickListener {
            LocalMusicLibraryActivity.start(this)
        }
        iv_player.setOnClickListener {
            try {
                val localMusicInfoList = CoreManager.getCore(IPlayerCore::class.java).playerListMusicInfos
                if (localMusicInfoList != null && localMusicInfoList.size > 0) {
                    val state = CoreManager.getCore(IPlayerCore::class.java).state
                    if (state == IPlayerCore.STATE_PLAY) {
                        CoreManager.getCore(IPlayerCore::class.java).pause()
                    } else if (state == IPlayerCore.STATE_PAUSE) {
                        CoreManager.getCore(IPlayerCore::class.java).play(CoreManager.getCore(IPlayerCore::class.java).current)
                    } else {
                        val result = CoreManager.getCore(IPlayerCore::class.java).switchPlayNext()
                        if (result < 0) {
                            if (result == -3) {
                                toast("播放列表中还没有歌曲哦！")
                            } else {
                                toast("播放失败，文件异常")
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                toast("播放失败，请刷新再试")
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicCoreClient::class)
    fun delOperationRefresh(id: Long, isHidePlayer: Boolean) {
        if (isHidePlayer) {
            rl_player.visibility = View.GONE
        }
    }

    private val icons = intArrayOf(R.drawable.ic_music_loop, R.drawable.ic_music_single, R.drawable.ic_music_random)
    private val strs = arrayOf("列表循环", "单曲循环", "随机播放")

    private fun initNavigationBar() {
        val mTabs = ArrayList<Fragment>(2)
        mTabs.add(MyMusicFragment())
        mTabs.add(HotMusicFragment())
        initMagicIndicator()
        view_pager.adapter = ImFragmentPagerAdapter(supportFragmentManager, mTabs)
        view_pager.offscreenPageLimit = 2
        ViewPagerHelper.bind(indicator, view_pager)
        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    private class ImFragmentPagerAdapter internal constructor(fm: FragmentManager, private val mTabs: List<Fragment>) : FragmentPagerAdapter(fm) {

        override fun getCount(): Int {
            return mTabs.size
        }

        override fun getItem(position: Int): Fragment {
            return mTabs[position]
        }
    }

    private var isSBProgressChangedByUserTouch = false

    @CoreEvent(coreClientClass = IPlayerCoreClient::class)
    fun onMusicProgressUpdate(total: Long, current: Long) {
        val progress = current * 100 / total
        if (isSBProgressChangedByUserTouch) {
            return
        }
        tv_current_time.text = TimeUtils.getFormatTimeString(current.toLong(), "min:sec")
        tv_music_total_length.text = resources.getString(R.string.music_play_control_dur, TimeUtils.getFormatTimeString(total.toLong(), "min:sec"))
        pb_music.progress = progress.toInt()
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient::class)
    fun onMusicPlaying(localMusicInfo: MusicLocalInfo) {
        updateView()
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient::class)
    fun onMusicPause(localMusicInfo: MusicLocalInfo) {
        updateView()
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient::class)
    fun onMusicStop(localMusicInfo: MusicLocalInfo) {
        updateView()
    }

    private fun updateView() {
        val current = CoreManager.getCore(IPlayerCore::class.java).current
        if (current != null && current.isValid) {
            tv_music_name.text = current.songName
            val state = CoreManager.getCore(IPlayerCore::class.java).state
            if (state == IPlayerCore.STATE_PLAY) {
                iv_player.setImageResource(R.drawable.ic_player_play)
            } else {
                iv_player.setImageResource(R.drawable.ic_player_stop)
            }
        } else {
            tv_music_name.text = "暂无歌曲播放"
            iv_player.setImageResource(R.drawable.ic_player_stop)
            tv_current_time.text = null
            tv_music_total_length.text = null
            pb_music.progress = 0
        }
    }


    private fun initMagicIndicator() {
        val tabInfoList = ArrayList<TabInfo>()
        tabInfoList.add(TabInfo(1, "我的曲库"))
        tabInfoList.add(TabInfo(2, "热门歌曲"))
        mMsgIndicatorAdapter = CommonMagicIndicatorAdapter(this, tabInfoList, 0)
        mMsgIndicatorAdapter!!.setSize(17)
        mMsgIndicatorAdapter!!.setNormalColorId(R.color.white)
        mMsgIndicatorAdapter!!.setSelectColorId(R.color.mm_theme)
        mMsgIndicatorAdapter!!.setOnItemSelectListener(this)
        val commonNavigator = CommonNavigator(getContext())
        commonNavigator.isAdjustMode = true
        commonNavigator.adapter = mMsgIndicatorAdapter
        indicator.navigator = commonNavigator
        // must after setNavigator
        val titleContainer = commonNavigator.titleContainer
        titleContainer.showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
    }
}