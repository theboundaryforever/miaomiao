package com.tongdaxing.erban.common.room.lover;

import android.util.SparseArray;

import com.erban.ui.mvp.BasePresenter;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * Created by Chen on 2019/5/9.
 */
public class BaseWeekPresenter<T> extends BasePresenter<T> {
    public boolean isSelfOnMic() {
        return AvRoomDataManager.get().isSelfOnMic();
    }

    public String getRoomUid() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            return String.valueOf(roomInfo.getUid());
        }
        return "";
    }

    public String getCpPlayerId() {
        SparseArray<RoomQueueInfo> memberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        String account = null;
        if (AvRoomDataManager.get().isSelfOnMic()) {
            if (memberMap != null && memberMap.size() >= 2) {
                RoomQueueInfo queueInfo0 = memberMap.valueAt(0);
                RoomQueueInfo queueInfo1 = memberMap.valueAt(1);
                if (queueInfo0 != null && queueInfo1 != null) {
                    if (AvRoomDataManager.get().isRoomOwner()) {
                        if (queueInfo1.mChatRoomMember != null) {
                            account = queueInfo1.mChatRoomMember.getAccount();
                        }
                    } else {
                        if (queueInfo0.mChatRoomMember != null) {
                            account = queueInfo0.mChatRoomMember.getAccount();
                        }
                    }
                }
            }
        }
        return account;
    }

    public String getUserId() {
        return String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    public long getUId() {
        return CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }
}
