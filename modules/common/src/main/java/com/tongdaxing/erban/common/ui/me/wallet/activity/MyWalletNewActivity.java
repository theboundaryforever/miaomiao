package com.tongdaxing.erban.common.ui.me.wallet.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.adapter.BaseIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.me.wallet.fragment.ChargeFragment;
import com.tongdaxing.erban.common.ui.me.wallet.fragment.MyIncomeFragment;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 新皮：我的钱包页面
 *
 * @author zwk 2018/5/30
 */
public class MyWalletNewActivity extends BaseActivity implements CommonMagicIndicatorAdapter.OnItemSelectListener {
    public static boolean isRefresh = false;
    private ImageView ivBack;
    private ViewPager vpWallet;
    private MagicIndicator mIndicator;
    private CommonMagicIndicatorAdapter mMsgIndicatorAdapter;

    public static void start(Context context) {
        Intent intent = new Intent(context, MyWalletNewActivity.class);
        if (!(context instanceof Activity)) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet_new);
        initView();
        initListener();
    }

    private void initView() {
        ivBack = (ImageView) findViewById(R.id.iv_wallet_back);
        mIndicator = (MagicIndicator) findViewById(R.id.mi_wallet_indicator);
        vpWallet = (ViewPager) findViewById(R.id.vp_my_wallet);
        List<Fragment> mTabs = new ArrayList<>(2);
        mTabs.add(new ChargeFragment());
        mTabs.add(new MyIncomeFragment());
        // mTabs.add(FansListFragment.newInstance(Constants.FAN_MAIN_PAGE_TYPE));
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(1, getString(R.string.charge)));
        tabInfoList.add(new TabInfo(2, "收益"));

        mMsgIndicatorAdapter = new CommonMagicIndicatorAdapter(this, tabInfoList, 0);
        mMsgIndicatorAdapter.setSize(17);

        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        mIndicator.setNavigator(commonNavigator);
        // must after setNavigator
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        vpWallet.setAdapter(new BaseIndicatorAdapter(getSupportFragmentManager(), mTabs));
        vpWallet.setOffscreenPageLimit(2);
        ViewPagerHelper.bind(mIndicator, vpWallet);
    }

    private void initListener() {
        ivBack.setOnClickListener(this);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        int i = view.getId();
        if (i == R.id.iv_wallet_back) {
            finish();

        }
    }

    @Override
    public void onItemSelect(int position) {
        vpWallet.setCurrentItem(position);
    }
}
