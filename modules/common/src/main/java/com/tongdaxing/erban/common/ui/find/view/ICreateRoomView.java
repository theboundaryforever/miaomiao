package com.tongdaxing.erban.common.ui.find.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.find.bean.CreateRoomLabelInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/18
 */
public interface ICreateRoomView extends IMvpBaseView {
//    void delCustomLabelFailure(String errorStr);

//    void delCustomLabelSucceed();

//    void selectedLabelId(int labelId);

    void requestRoomInfoFailView(String errorStr);

    void loadDataFailure(String errorStr);

    void showCreateRoomConfirmDialog(int tagId, int type);

    void setupSuccessView(List<CreateRoomLabelInfo> createRoomLabelInfos);

    void setupFailView();

    void requestOwnRoomInfoSuccessView(RoomInfo data);

    void requestNoticeFansCdSuccess(Long countDown);
}
