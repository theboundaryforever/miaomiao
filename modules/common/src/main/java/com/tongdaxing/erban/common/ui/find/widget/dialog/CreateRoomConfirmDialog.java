package com.tongdaxing.erban.common.ui.find.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.juxiao.library_ui.widget.ViewHolder;
import com.juxiao.library_ui.widget.dialog.BaseDialog;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.find.model.WarmAccompanyModel;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.find.ICreateRoomClient;
import com.tongdaxing.xchat_core.find.bean.ReopenRoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.uuzuche.lib_zxing.DisplayUtil;

import static com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH;

/**
 * Function:创建房间确认对话框
 * Author: Edward on 2019/3/18
 */
public class CreateRoomConfirmDialog extends BaseDialog {
    private WarmAccompanyModel warmAccompanyModel;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null && getContext() != null) {
                int color = ContextCompat.getColor(getContext(), android.R.color.transparent);
                window.setBackgroundDrawable(new ColorDrawable(color));
                window.setLayout(DisplayUtil.dip2px(getContext(), 280.0f), WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }
    }

    @Override
    public void convertView(ViewHolder viewHolder) {
        Bundle bundle = getArguments();
        if (bundle == null) {
            SingleToastUtil.showToast("数据错误!");
            dismiss();
            return;
        }

        int tagId = bundle.getInt("tagId");
        int type = bundle.getInt("type");
        boolean isCheckNoticeFans = bundle.getBoolean("isCheckNoticeFans");
        String title = bundle.getString("title");

        warmAccompanyModel = new WarmAccompanyModel();
        TextView tvCancel = viewHolder.getView(R.id.tv_cancel);
        tvCancel.setOnClickListener(v -> dismiss());

        TextView tvCreateRoom = viewHolder.getView(R.id.tv_create_room);
        tvCreateRoom.setOnClickListener(v -> {
            loadReopenRoom(tagId, type, isCheckNoticeFans, title);
        });
    }

    /**
     * @param tagId
     * @param type
     */
    private void loadReopenRoom(int tagId, int type, boolean isCheckNoticeFans, String title) {
        DialogManager dialogManager = new DialogManager(getActivity());
        dialogManager.showProgressDialog(getActivity(), getString(R.string.waiting_text));
        warmAccompanyModel.getReopenRoom(tagId, type, isCheckNoticeFans, title, new OkHttpManager.MyCallBack<ServiceResult<ReopenRoomInfo>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<ReopenRoomInfo> response) {
                if (response == null) {
                    onError(new Exception());
                    return;
                }
                if (response.isSuccess() && response.getData() != null) {
                    dialogManager.dismissDialog();
                    CoreManager.notifyClients(ICreateRoomClient.class, ICreateRoomClient.CREATE_ROOM_SUCCEED, tagId, type);
                    dismiss();
                } else if (response.getCode() == RESULT_FAILED_NEED_REAL_NAME_AUTH) {//要求实名认证
                    dialogManager.dismissDialog();
                    onNeedRealNameAuthChecked(getActivity(), response.getErrorMessage());
                    dismiss();
                } else {
                    //实名认证审核期 要有提示
                    onError(new Exception(response.getMessage()));
                }
            }
        });
    }

    private void onNeedRealNameAuthChecked(Context context, String msg) {
        DialogManager mDialogManager = new DialogManager(context);
        mDialogManager.setCanceledOnClickOutside(false);
        mDialogManager.showOkCancelDialog(msg,
                getResources().getString(R.string.real_name_auth_accept),
                getResources().getString(R.string.cancel),
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onOk() {
                        if (context != null) {
                            CommonWebViewActivity.start(context, UriProvider.getRealNameAuthUrl());
                        }
                    }
                });
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_create_room_confirm;
    }
}
