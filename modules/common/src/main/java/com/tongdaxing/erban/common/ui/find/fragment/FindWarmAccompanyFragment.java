package com.tongdaxing.erban.common.ui.find.fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.find.model.WarmAccompanyModel;
import com.tongdaxing.erban.common.ui.find.widget.WarmAccompanyMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.find.widget.dialog.CreateRoomConfirmDialog;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.MainAdapter;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.find.ICreateRoomClient;
import com.tongdaxing.xchat_core.find.bean.LoverMatchInfo;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Function:暖心陪伴
 * Author: Edward on 2019/3/15
 */
public class FindWarmAccompanyFragment extends BaseFragment implements CommonMagicIndicatorAdapter.OnItemSelectListener {
    public static final String TAG = "FindWarmAccompanyFragment";
    @BindView(R2.id.iv_lovers_match)
    TextView ivLoversMatch;
    @BindView(R2.id.view_pager)
    ViewPager mViewPager;
    @BindView(R2.id.magic_indicator)
    MagicIndicator mMagicIndicator;
    private WarmAccompanyModel warmAccompanyModel;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_find_warm_accompany;
    }

    @Override
    public void onFindViews() {
        ButterKnife.bind(this, getActivity());
    }

    @Override
    public void onSetListener() {
        ivLoversMatch.setOnClickListener(v -> {
            startMatch();
        });
    }

    @Override
    public void initiate() {
        setNavigator();
        warmAccompanyModel = new WarmAccompanyModel();
    }

    private List<TabInfo> getTabDefaultList() {
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(0, "全部"));
//        tabInfoList.add(new TabInfo(2, "萌妹子"));
//        tabInfoList.add(new TabInfo(1, "小鲜肉"));
        return tabInfoList;
    }

    private void setNavigator() {
        List<TabInfo> mTabInfoList = getTabDefaultList();
        WarmAccompanyMagicIndicatorAdapter mMsgIndicatorAdapter = new WarmAccompanyMagicIndicatorAdapter(getContext(), mTabInfoList);
        mMsgIndicatorAdapter.setSize(13);
//        mMsgIndicatorAdapter.setNormalColorId(R.color.color_969696);
//        mMsgIndicatorAdapter.setSelectColorId(R.color.mm_theme);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
//        commonNavigator.setAdjustMode(true);
        //不要默认等分
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        mMagicIndicator.setNavigator(commonNavigator);
        SparseArray<Fragment> mFragmentList = new SparseArray<>();
        for (int i = 0; i < mTabInfoList.size(); i++) {
            WarmAccompanyFragment warmAccompanyFragment = new WarmAccompanyFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("data", mTabInfoList.get(i).getId());
            warmAccompanyFragment.setArguments(bundle);
            mFragmentList.put(i, warmAccompanyFragment);
        }
        mViewPager.setAdapter(new MainAdapter(getFragmentManager(), mFragmentList));
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
        mViewPager.setCurrentItem(0);//默认选中首页
    }

    private void startMatch() {
        warmAccompanyModel.getLoverMatch(new OkHttpManager.MyCallBack<ServiceResult<LoverMatchInfo>>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<LoverMatchInfo> response) {
                if (response == null) {
                    SingleToastUtil.showToast("数据错误!");
                    return;
                }
                //1、用户有房间且是情侣房间类型、或者没有房间（服务端自动创建），则直接返回房间信息。
                //2、用户有房间但不是情侣房间返回状态码，客户端调用重新开房接口。
                if (response.isSuccess() && response.getData() != null) {
                    AVRoomActivity.start(getContext(), response.getData().getUid());
                } else if (response.getCode() == 10501) {
                    CreateRoomConfirmDialog dialog = new CreateRoomConfirmDialog();
                    Bundle bundle = new Bundle();
                    bundle.putInt("tagId", -1);
                    bundle.putInt("type", 5);//写死为情侣房
                    dialog.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    if (fragmentManager != null) {
                        dialog.show(fragmentManager, "");
                    }
                } else {
                    onError(new Exception("数据错误!"));
                }
            }
        });
    }

    private void saveCreateRoomTypeAndLabel(int tagId, int type) {
        final String fileName = "create_room_type_and_label" + CoreManager.getCore(IAuthCore.class).getCurrentUid();
        SharedPreferences preferences = getContext().getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("tagId", tagId);
        editor.putInt("roomType", type);
        editor.apply();
    }

    @CoreEvent(coreClientClass = ICreateRoomClient.class)
    public void createRoomSucceed(int tagId, int type) {
        AVRoomActivity.start(getContext(), CoreManager.getCore(IAuthCore.class).getCurrentUid());
        saveCreateRoomTypeAndLabel(tagId, type);
    }

    @Override
    public void onItemSelect(int position) {
        //分类点击
        mViewPager.setCurrentItem(position);
    }
}
