package com.tongdaxing.erban.common.room.lover.chair;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.erban.ui.baseview.BaseFrameLayout;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tcloud.core.util.DensityUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.widget.WaveView;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Chen on 2018/11/29.
 */
public class ChairItemView extends BaseFrameLayout {
    @BindView(R2.id.room_chair_item_waveview)
    WaveView waveview;
    @BindView(R2.id.avatar)
    CircleImageView avatar;
    @BindView(R2.id.iv_headwear)
    ImageView ivHeadwear;
    @BindView(R2.id.iv_mic_gender)
    TextView ivMicGender;
    @BindView(R2.id.avatar_bg)
    RelativeLayout avatarBg;
    @BindView(R2.id.up_image)
    ImageView upImage;
    @BindView(R2.id.lock_image)
    ImageView lockImage;
    @BindView(R2.id.mute_image)
    ImageView muteImage;
    @BindView(R2.id.tv_state)
    ImageView tvState;
    @BindView(R2.id.micro_layout)
    FrameLayout microLayout;
    @BindView(R2.id.iv_room_cp_level)
    ImageView ivRoomCpLevel;
    @BindView(R2.id.nick)
    TextView nick;
    @BindView(R2.id.tv_glamour)
    TextView tvGlamour;
    @BindView(R2.id.ll_mic_glamour)
    LinearLayout llMicGlamour;

    private String ownerNick = "";
    private String avatarPicture = "";
    private boolean showCharm = false;

    public ChairItemView(Context context) {
        super(context);
        init();
    }

    public ChairItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChairItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        LayoutInflater.from(this.getContext()).inflate(R.layout.room_chair_player_view, this);
        ButterKnife.bind(this);
        initWaveView();
    }

    public void setChairItemData(RoomQueueInfo roomQueueInfo, boolean isOwner) {
        if (roomQueueInfo == null) {
            stop();
        } else {
            if (isOwner) {
                // 房主信息
                setOwnerInfo(roomQueueInfo);
            } else {
                // 其他位置信息
                setOtherPlayerInfo(roomQueueInfo);
            }
        }
    }

    private void initWaveView() {
        waveview.setInitialRadius(DensityUtil.dip2px(getContext(), 35.0f));
        waveview.setMaxRadius(DensityUtil.dip2px(getContext(), 50.0f));
        waveview.setInitialAlpha(0.5f);
        waveview.setSpeed(800);
        waveview.setColor(getResources().getColor(R.color.white));
    }

    private void setOwnerInfo(RoomQueueInfo roomQueueInfo) {
        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
        if (chatRoomMember != null) {
            tvState.setVisibility(View.GONE);
            nick.setText(chatRoomMember.getNick());
            ownerNick = chatRoomMember.getNick();
        } else {
            tvState.setVisibility(View.VISIBLE);
        }

        ivMicGender.setVisibility(View.GONE);
        nick.setVisibility(View.VISIBLE);
        lockImage.setVisibility(View.GONE);
        muteImage.setVisibility(View.GONE);
        upImage.setVisibility(View.GONE);
        avatar.setVisibility(View.VISIBLE);
        avatarBg.setVisibility(View.VISIBLE);
        if (roomQueueInfo.isShowCpIcon) {
            ivRoomCpLevel.setVisibility(View.VISIBLE);
            if (roomQueueInfo.cpGiftLevel == 2) {
                ivRoomCpLevel.setImageResource(R.drawable.ic_room_cp_level_2);
            } else if (roomQueueInfo.cpGiftLevel == 3) {
                ivRoomCpLevel.setImageResource(R.drawable.ic_room_cp_level_3);
            } else {//只要isShowCpIcon为true就要显示cp标识，一周cp的cpGiftLevel为0，所以这里其他情况都显示1级标识
                ivRoomCpLevel.setImageResource(R.drawable.ic_room_cp_level_1);
            }
        } else {
            ivRoomCpLevel.setVisibility(View.GONE);
        }

        //--------头饰-----------
        if (chatRoomMember != null) {
            Map<String, Object> extension = chatRoomMember.getExtension();
            String headwearUrl = "";
            if (extension != null && extension.get("headwearUrl") != null) {
                try {
                    headwearUrl = (String) extension.get("headwearUrl");
                } catch (Exception e) {
                }
            }
            if (!TextUtils.isEmpty(headwearUrl)) {
                ImageLoadUtils.loadImage(getContext(), headwearUrl, ivHeadwear);
                ivHeadwear.setVisibility(View.VISIBLE);
            } else {
                ivHeadwear.setVisibility(View.GONE);
            }
        }
        //--------头饰-----------

        if (StringUtil.isEmpty(avatarPicture)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(AvRoomDataManager.get().mCurrentRoomInfo.getUid());
            if (userInfo != null) {
                if (userInfo.getAvatar() != null) {
                    if (!userInfo.getAvatar().equals(avatarPicture)) {
                        avatarPicture = userInfo.getAvatar();
                        ImageLoadUtils.loadAvatar(getContext(), avatarPicture, avatar);
                    }
                }
            }
        } else {
            ImageLoadUtils.loadAvatar(getContext(), avatarPicture, avatar);
        }
        if (StringUtil.isEmpty(ownerNick)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(AvRoomDataManager.get().mCurrentRoomInfo.getUid());
            if (userInfo != null) {
                if (userInfo.getNick() != null) {
                    if (!userInfo.getNick().equals(ownerNick)) {
                        ownerNick = userInfo.getNick();
                        nick.setText(ownerNick);
                    }
                }
            }
        } else {
            nick.setText(ownerNick);
        }
    }

    private void setOtherPlayerInfo(RoomQueueInfo roomQueueInfo) {
        RoomMicInfo roomMicInfo = roomQueueInfo.mRoomMicInfo;
        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
        tvState.setVisibility(GONE);
        // 清除动画
        setMicNewYearBg();
        if (roomQueueInfo.isShowCpIcon) {
            ivRoomCpLevel.setVisibility(View.VISIBLE);
            if (roomQueueInfo.cpGiftLevel == 2) {
                ivRoomCpLevel.setImageResource(R.drawable.ic_room_cp_level_2);
            } else if (roomQueueInfo.cpGiftLevel == 3) {
                ivRoomCpLevel.setImageResource(R.drawable.ic_room_cp_level_3);
            } else {//只要isShowCpIcon为true就要显示cp标识，一周cp的cpGiftLevel为0，所以这里其他情况都显示1级标识
                ivRoomCpLevel.setImageResource(R.drawable.ic_room_cp_level_1);
            }
        } else {
            ivRoomCpLevel.setVisibility(View.GONE);
        }
        if (roomMicInfo == null) {
            upImage.setVisibility(View.VISIBLE);
            lockImage.setVisibility(View.GONE);
            muteImage.setVisibility(View.GONE);
            avatar.setVisibility(View.GONE);
            avatarBg.setVisibility(View.GONE);
            // nick.setVisibility(View.INVISIBLE);
            nick.setText("");
            ivMicGender.setVisibility(View.GONE);
            ivRoomCpLevel.setVisibility(View.GONE);
//                tvPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
            return;
        }

        //显示，先展示人，无视麦的锁
        if (chatRoomMember != null) {
            //--------魅力值-----------
            if (llMicGlamour != null) {//主席位没有魅力值
                if (showCharm) {
                    int charm = 0;
                    JSONObject roomCharmList = IMNetEaseManager.get().roomCharmList;
                    if (roomCharmList != null) {
                        try {
                            if (roomCharmList.has(chatRoomMember.getAccount())) {
                                charm = roomCharmList.getInt(chatRoomMember.getAccount());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    llMicGlamour.setVisibility(View.VISIBLE);
                    tvGlamour.setText(String.valueOf(charm));
                } else {
                    llMicGlamour.setVisibility(View.INVISIBLE);
                }
            }

            lockImage.setVisibility(View.GONE);
            muteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);

            if (!TextUtils.isEmpty(chatRoomMember.getAccount()) && JavaUtil.str2long(chatRoomMember.getAccount()) > 0) {
                upImage.setVisibility(View.GONE);
                avatar.setVisibility(View.VISIBLE);
                avatarBg.setVisibility(View.VISIBLE);
                nick.setVisibility(View.VISIBLE);
                ivMicGender.setVisibility(View.GONE);
                nick.setText(chatRoomMember.getNick());
//                ivMicGender.setBackgroundResource(roomQueueInfo.gender == 1 ? R.drawable.ic_room_male : R.drawable.ic_room_female);
                ImageLoadUtils.loadAvatar(getContext(), chatRoomMember.getAvatar(), avatar);

                //--------头饰-----------
                Map<String, Object> extension = chatRoomMember.getExtension();
                String headwearUrl = "";
                if (extension != null) {
                    try {
                        headwearUrl = (String) extension.get("headwearUrl");
                    } catch (Exception e) {
                    }
                }
                if (!TextUtils.isEmpty(headwearUrl)) {
                    ImageLoadUtils.loadImage(getContext(), headwearUrl, ivHeadwear);
                    ivHeadwear.setVisibility(View.VISIBLE);
                } else {
                    ivHeadwear.setVisibility(View.GONE);
                }
                //--------头饰-----------

                GlideApp.with(getContext())
                        .load(chatRoomMember.getAvatar())
                        .placeholder(R.drawable.ic_no_avatar)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object o,
                                                        Target<Drawable> target, boolean b) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable drawable, Object o,
                                                           Target<Drawable> target,
                                                           DataSource dataSource, boolean b) {
                                avatar.setImageDrawable(drawable);
                                return true;
                            }
                        })
                        .into(avatar);
            } else {
                upImage.setVisibility(View.VISIBLE);
                avatar.setVisibility(View.GONE);
                avatarBg.setVisibility(View.GONE);
                nick.setText("");
                ivMicGender.setVisibility(View.GONE);
//                    tvPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
            }
        } else {
            if (llMicGlamour != null) {
                llMicGlamour.setVisibility(View.INVISIBLE);
            }
            //锁麦
            if (roomMicInfo.isMicLock()) {
                upImage.setVisibility(View.GONE);
                muteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
                lockImage.setVisibility(View.VISIBLE);
                avatar.setVisibility(View.GONE);
                avatarBg.setVisibility(View.GONE);
            } else {
                muteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
                upImage.setVisibility(View.VISIBLE);
                avatar.setVisibility(View.GONE);
                avatarBg.setVisibility(View.GONE);
                lockImage.setVisibility(View.GONE);
            }
            nick.setText("");
            ivMicGender.setVisibility(View.GONE);
            ivRoomCpLevel.setVisibility(View.GONE);
//                tvPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
        }
    }

    private void setMicNewYearBg() {
        upImage.setImageResource(R.drawable.room_lover_up_micro);
    }


    public void setShowCharm(boolean showCharm) {
        this.showCharm = showCharm;
    }

    public void start() {
        if (waveview != null) {
            waveview.start();
        }
    }

    public void stop() {
        if (waveview != null) {
            waveview.stop();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stop();
    }
}
