package com.tongdaxing.erban.common.room.chat;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tongdaxing.erban.common.base.adapter.ImFragmentPageAdapter;
import com.tongdaxing.erban.common.ui.find.fragment.ChatHallFragment;
import com.tongdaxing.erban.common.ui.home.adpater.CommonImageIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.message.fragment.RecentListFragment;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 房间私聊
 */
public class RoomPrivateMsgDialog extends AppCompatDialogFragment implements View.OnClickListener, CommonMagicIndicatorAdapter.OnItemSelectListener {
    private RecentListFragment mRecentContactsFragment;
    private ImageView ivCloss;
    private ViewPager viewPager;
    private MagicIndicator indicator;
    private ImFragmentPageAdapter mBaseIndicatorAdapter;
    private List<ImFragmentPageAdapter.PagerContent> mPagerContents;

    public static RoomPrivateMsgDialog newInstance() {
        Bundle args = new Bundle();
        RoomPrivateMsgDialog fragment = new RoomPrivateMsgDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
    }

    @Override
    public void onItemSelect(int position) {
        viewPager.setCurrentItem(position);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_room_private_msg, window.findViewById(android.R.id.content), false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);
        return view;
    }

    private void initMagicIndicator() {
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(0, "私信"));
        tabInfoList.add(new TabInfo(1, "广场"));
        CommonImageIndicatorAdapter mMsgIndicatorAdapter = new CommonImageIndicatorAdapter(getActivity(), tabInfoList);
        mMsgIndicatorAdapter.setSize(15);
        mMsgIndicatorAdapter.setDefaultDrawableId(R.drawable.hall_voice_top_tab_selected_bg);
        mMsgIndicatorAdapter.setImageIndicatorMarginTop(5.0f);
        mMsgIndicatorAdapter.setImageIndicatorWidth(18.0f);
        mMsgIndicatorAdapter.setImageIndicatorHeight(4.0f);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
//        commonNavigator.setAdjustMode(false);
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        indicator.setNavigator(commonNavigator);
        // must after setNavigator
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager = view.findViewById(R.id.view_pager);
        indicator = view.findViewById(R.id.indicator);
        ivCloss = view.findViewById(R.id.iv_close_dialog);
        ivCloss.setOnClickListener(this);

        initMagicIndicator();
        mPagerContents = new ArrayList<>();

        ImFragmentPageAdapter.PagerContent recentListFragment = new ImFragmentPageAdapter.PagerContent(RecentListFragment.class, new Bundle(), RecentListFragment.class.getName());
        mPagerContents.add(recentListFragment);
        ImFragmentPageAdapter.PagerContent chatHallFragment = new ImFragmentPageAdapter.PagerContent(ChatHallFragment.class, new Bundle(), ChatHallFragment.class.getName());
        mPagerContents.add(chatHallFragment);

        mBaseIndicatorAdapter = new ImFragmentPageAdapter(getChildFragmentManager(), mPagerContents);
        viewPager.setAdapter(mBaseIndicatorAdapter);
        viewPager.setOffscreenPageLimit(mPagerContents.size());
        ViewPagerHelper.bind(indicator, viewPager);

//        Fragment item = mBaseIndicatorAdapter.getItem(0);
//        if (item instanceof RecentListFragment) {
//            mRecentContactsFragment = (RecentListFragment) item;
//            mRecentContactsFragment.setCallback(new RecentContactsCallback() {
//                @Override
//                public void onRecentContactsLoaded() {
//
//                }
//
//                @Override
//                public void onUnreadCountChange(int unreadCount) {
//
//                }
//
//                @Override
//                public void onItemClick(RecentContact recent) {
//                    if (recent.getSessionType() == SessionTypeEnum.P2P) {
//                        RoomPrivateChatDialog chat = RoomPrivateChatDialog.newInstance(recent.getContactId());
//                        chat.show(getFragmentManager(), null);
//                        dismiss();
//                    }
//                }
//
//                @Override
//                public String getDigestOfAttachment(RecentContact recent, MsgAttachment attachment) {
//                    if (attachment instanceof CustomAttachment) {
//                        CustomAttachment customAttachment = (CustomAttachment) attachment;
//                        if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
//                            return "您关注的TA上线啦，快去围观吧~~~";
//                        } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_GIFT) {
//                            return "[礼物]";
//                        } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_NOTICE) {
//                            NoticeAttachment noticeAttachment = (NoticeAttachment) attachment;
//                            return noticeAttachment.getTitle();
//                        } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_PACKET) {
//                            RedPacketAttachment redPacketAttachment = (RedPacketAttachment) attachment;
//                            RedPacketInfoV2 redPacketInfo = redPacketAttachment.getRedPacketInfo();
//                            if (redPacketInfo == null) {
//                                return "您收到一个红包哦!";
//                            }
//                            return "您收到一个" + redPacketInfo.getPacketName() + "红包哦!";
//                        } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_LOTTERY) {
//                            return "恭喜您，获得抽奖机会";
//                        } else if (customAttachment.getFirst() == CUSTOM_MSG_SHARE_FANS) {
//                            return "[房间邀请]";
//                        } else if (customAttachment.getFirst() == CUSTOM_MSG_CP || customAttachment.getFirst() == CUSTOM_MSG_CP_TIP) {
//                            return "[CP消息]";
//                        } else if (customAttachment.getFirst() == CUSTOM_MSG_ROOM_LOVER_WEEK_CP_OFFICE) {
//                            return "[一周CP消息]";
//                        }
//                    } else if (attachment instanceof AudioAttachment) {
//                        return "[语音]";
//                    }
//                    return null;
//                }
//
//                @Override
//                public String getDigestOfTipMsg(RecentContact recent) {
//                    return null;
//                }
//            });
//        }
    }


//    @CoreEvent(coreClientClass = IUserClient.class)
//    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
//        if (mRecentContactsFragment != null) {
//            mRecentContactsFragment.requestMessages(true);
//        }
//    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CoreManager.removeClient(this);
    }

    @Override
    public void onClick(View v) {
        dismissAllowingStateLoss();
        CoreManager.removeClient(this);
    }
}
