package com.tongdaxing.erban.common.ui.login.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.netease.nim.uikit.cache.MsgIgnoreCache;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.ClientType;
import com.netease.nis.captcha.Captcha;
import com.netease.nis.captcha.CaptchaListener;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.constant.BaseUrl;
import com.tongdaxing.erban.common.ui.common.permission.PermissionActivity;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.coremanager.IAppInfoCore;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.constant.AppKey;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 新版手机号登录页面
 * Created by zhangjian on 2019/4/24.
 */
public class PhoneLoginActivity extends BaseActivity {
    private static final String TAG = "LoginActivity";
    private static final String KICK_OUT = "KICK_OUT";

    /**
     * 基本权限管理
     */
    private final String[] BASIC_PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO,
    };
    @BindView(R2.id.login_et_phone)
    EditText loginEtPhone;
    @BindView(R2.id.login_et_password)
    EditText loginEtPassword;
    @BindView(R2.id.login_btn_login)
    Button loginBtnLogin;
    @BindView(R2.id.phone_login_iv_agree_protocol)
    ImageView phoneLoginIvAgreeProtocol;

    private boolean show = false;
    private Captcha mCaptcha;
    private int nextStepId = -1;
    //避免重复点击多吃执行
    private boolean captchaIsShowing = false;
    /**
     * 网易图形验证码操作回调
     */
    CaptchaListener myCaptchaListener = new CaptchaListener() {

        @Override
        public void onValidate(String result, String validate, String message) {
            captchaIsShowing = false;
            //验证结果，valiadte，可以根据返回的三个值进行用户自定义二次验证
            if (validate.length() > 0) {//验证成功
                nextStep();
            } else {
                SingleToastUtil.showToast(PhoneLoginActivity.this, "验证失败：" + message);
            }
        }

        @Override
        public void closeWindow() {
            //请求关闭页面
            captchaIsShowing = false;
        }

        @Override
        public void onError(String errormsg) {
            //出错
            captchaIsShowing = false;
        }

        @Override
        public void onCancel() {
            captchaIsShowing = false;
        }

        @Override
        public void onReady(boolean ret) {
            //该为调试接口，ret为true表示加载Sdk完成
        }

    };

    public static void start(Context context) {
        start(context, false);
    }

    public static void start(Context context, boolean kickOut) {
        Intent intent = new Intent(context, PhoneLoginActivity.class);
        intent.putExtra(KICK_OUT, kickOut);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_by_phone);
        ButterKnife.bind(this);
        initTitleBar("");
        initImgCheck();
        onParseIntent();
        onSetListener();
        permission();
        setSwipeBackEnable(false);

    }

    private void onParseIntent() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        if (getIntent().getBooleanExtra(KICK_OUT, false)) {
            int type = NIMClient.getService(AuthService.class).getKickedClientType();
            String client;
            switch (type) {
                case ClientType.Web:
                    client = "网页端";
                    break;
                case ClientType.Windows:
                    client = "电脑端";
                    break;
                case ClientType.REST:
                    client = "服务端";
                    break;
                default:
                    client = "移动端";
                    break;
            }
            getDialogManager().showOkAndLabelDialog("你的帐号在" + client + "登录，被踢出下线，请确定帐号信息安全", "下线通知", true, true, false, false, new DialogManager.OkDialogListener() {
                @Override
                public void onOk() {
                }
            });
        }
    }

    private void permission() {
        checkPermission(new PermissionActivity.CheckPermListener() {
            @Override
            public void superPermission() {
//                toast("授权成功");
            }
        }, R.string.ask_again, BASIC_PERMISSIONS);
    }

    private void onSetListener() {
        loginEtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                loginBtnLogin.setEnabled(checkEnableLogin());
            }
        });
        loginEtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() < 6 || charSequence.length() > 16) {
//                    loginEtPassword.setEnabled(true);
                    if (!show) {
                        toast("密码长度6-16个字符");
                        show = true;
                    }
                } else {
                    show = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                loginBtnLogin.setEnabled(checkEnableLogin());
            }
        });
    }

    private void openWebView(String url) {
        if (TextUtils.isEmpty(url)) return;
        Intent intent = new Intent(this, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    private void showDialog(String tip, boolean cancelable, DialogInterface.OnDismissListener listener) {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        getDialogManager().showProgressDialog(PhoneLoginActivity.this, tip, cancelable, cancelable, listener);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLoginFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        LogUtil.d("LoginActivity onLogin");
        CoreManager.getCore(IAppInfoCore.class).checkBanned(true);
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        MsgIgnoreCache.getInstance().clearAllMsgIgnoreNumber(this);//登录成功清除一下上次登录用户忽略的 未读消息
        getDialogManager().dismissDialog();
        finish();
    }

    private boolean checkEnableLogin() {
        if ((loginEtPhone != null && loginEtPhone.getText() != null && !StringUtil.isEmpty(loginEtPhone.getText().toString()))
                && (loginEtPassword != null && loginEtPassword.getText() != null && !StringUtil.isEmpty(loginEtPassword.getText().toString()))) {
            return true;
        }
        return false;
    }

    /**
     * 10分钟内操作是否是要验证的
     *
     * @return
     */
    private boolean cleckLoginTimeNeedCaptcha() {
        long l = (Long) SpUtils.get(this, SpEvent.cleckLoginTime, 0L);
        long currentTimeMillis = System.currentTimeMillis();
        int i = 600000;
        if (BasicConfig.isDebug) {
            i = 60000;
        }
        if (currentTimeMillis - l > i) {
            SpUtils.put(this, SpEvent.cleckLoginTime, currentTimeMillis);
            return false;
        }
        return true;
    }

    /**
     * 网易图形验证码初始化
     */
    private void initImgCheck() {
        phoneLoginIvAgreeProtocol.setSelected(true);
        //初始化验证码SDK相关参数，设置CaptchaId、Listener最后调用start初始化。
        if (mCaptcha == null) {
            mCaptcha = new Captcha(this);
        }
        mCaptcha.setCaptchaId(AppKey.getCaptchaId());
        mCaptcha.setCaListener(myCaptchaListener);
        //可选：开启debug
        mCaptcha.setDebug(false);
        //可选：设置超时时间
        mCaptcha.setTimeout(10000);
    }

    /**
     * 图形验证码验证成功后操作
     */
    private void nextStep() {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        if (nextStepId == R.id.login_iv_wx) {
            getDialogManager().showProgressDialog(this, "请稍后");
            CoreManager.getCore(IAuthCore.class).doWxLogin();
            UmengEventUtil.getInstance().onEvent(this, UmengEventId.getLoginWechat());

        } else if (nextStepId == R.id.login_iv_qq) {
            getDialogManager().showProgressDialog(this, "请稍后");
            CoreManager.getCore(IAuthCore.class).doQqLogin();
            UmengEventUtil.getInstance().onEvent(this, UmengEventId.getLoginQQ());

        } else if (nextStepId == R.id.login_btn_login) {
            if (loginEtPhone.getText().toString().length() == 0) {
                toast("手机号码不能为空");
                return;
            } else if (loginEtPassword.getText().toString().length() < 6 || loginEtPassword.getText().toString().length() > 16) {
                toast("密码长度为6到16位");
                return;
            } else if (!phoneLoginIvAgreeProtocol.isSelected()) {
                toast("请同意《喵喵用户协议》！");
                return;
            }
            CoreManager.getCore(IAuthCore.class).doLogin(loginEtPhone.getText().toString(), loginEtPassword.getText().toString());
            showDialog("正在登录...", true, new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                }
            });
            UmengEventUtil.getInstance().onEvent(this, UmengEventId.getLoginPhone());

        } else {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R2.id.login_btn_forget, R2.id.login_btn_login, R2.id.login_tv_agreement, R2.id.login_iv_wx, R2.id.login_iv_qq})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.login_iv_wx || i == R.id.login_iv_qq || i == R.id.login_btn_login) {
            if (captchaIsShowing)
                return;
            nextStepId = view.getId();
            if (cleckLoginTimeNeedCaptcha()) {
                captchaIsShowing = true;
                mCaptcha.start();
                //简单测试是否填写captcha 是否初始化，包括captchaId与设置监听对象
                if (mCaptcha.checkParams()) {
                    if (this.isDestroyed() || this.isFinishing()) {
                        return;
                    }
                    mCaptcha.Validate();
                } else {
                    nextStep();
                }
            } else {
                nextStep();
            }

        } else if (i == R.id.login_btn_forget) {
            UIHelper.showForgetPswAct(PhoneLoginActivity.this);

        } else {
        }
    }

    @OnClick(R2.id.login_tv_agreement)
    public void onLoginTvAgreementClicked() {
        openWebView(BaseUrl.USER_AGREEMENT);
    }

    @OnClick(R2.id.login_ll_agreement)
    public void onLoginLlAgreementClicked() {
        phoneLoginIvAgreeProtocol.setSelected(!phoneLoginIvAgreeProtocol.isSelected());
    }

    @OnClick(R2.id.login_btn_register)
    public void onLoginBtnRegisterClicked() {
        UIHelper.showRegisterAct(PhoneLoginActivity.this);
    }
}
