package com.tongdaxing.erban.common.ui.find.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.find.bean.ConvertFindMakeFriendsInfo;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/14
 */
public interface IFindMakeFriendsView extends IMvpBaseView {
    void showConfirmDialog();

    void enterRoom(long uid);

    void setupFailView(boolean isRefresh);

    void setupSuccessView(List<ConvertFindMakeFriendsInfo> voiceGroupInfoList, boolean isRefresh);
}
