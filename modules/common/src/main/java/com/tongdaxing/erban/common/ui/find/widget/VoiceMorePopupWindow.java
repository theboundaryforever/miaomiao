package com.tongdaxing.erban.common.ui.find.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.base.popupwindow.RelativePopupWindow;
import com.tongdaxing.erban.common.model.ReportModel;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.utils.NewAndOldUserEventUtils;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chen on 2019/6/13.
 */
public class VoiceMorePopupWindow extends RelativePopupWindow {

    @BindView(R2.id.voice_tv_attention)
    TextView mVoiceTvAttention;
    @BindView(R2.id.voice_tv_report)
    TextView mVoiceTvReport;
    private long mUserId;
    private boolean mIsAttention;

    private static HttpRequestCallBack<Object> reportCallBack = new HttpRequestCallBack<Object>() {
        @Override
        public void onFinish() {

        }

        @Override
        public void onSuccess(String message, Object response) {
            SingleToastUtil.showToast("举报成功，我们会尽快为您处理");
        }

        @Override
        public void onFailure(int code, String msg) {
            SingleToastUtil.showToast(msg);
        }
    };

    public VoiceMorePopupWindow(Context context, long userId) {
        super(context);
        this.mUserId = userId;
        initView(context);
    }

    public VoiceMorePopupWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            View view = inflater.inflate(R.layout.voice_popup_window_more, null);
            ButterKnife.bind(this, view);
            this.setContentView(view);
            this.setFocusable(true);
            ColorDrawable dw = new ColorDrawable(00000000);
            this.setBackgroundDrawable(dw);
        }
    }

    public void setVoiceAttention(String attentionText) {
        if (!TextUtils.isEmpty(attentionText) && mVoiceTvAttention != null) {
            mVoiceTvAttention.setText(attentionText);
        }
    }

    public void setVoiceReport(String voiceText) {
        if (!TextUtils.isEmpty(voiceText) && mVoiceTvReport != null) {
            mVoiceTvReport.setText(voiceText);
        }
    }

    public void setIsAttention(boolean isAttention) {
        mIsAttention = isAttention;
        if (mVoiceTvAttention != null) {
            mVoiceTvAttention.setText(isAttention ? "已关注" : "关注");
        }
    }

    @OnClick(R2.id.voice_tv_attention)
    public void onAttentionClicked(View view) {
        NewAndOldUserEventUtils.onCommonTypeEvent(view.getContext(), UmengEventId.getMomentFollow());
        if (mIsAttention) {
            CoreManager.getCore(IPraiseCore.class).cancelPraise(mUserId);
        } else {
            CoreManager.getCore(IPraiseCore.class).praise(mUserId);
        }
        this.dismiss();
    }

    @OnClick(R2.id.voice_tv_report)
    public void onReportClicked(View view) {
        List<ButtonItem> buttons = new ArrayList<>();
        ButtonItem button1 = new ButtonItem("政治敏感", () -> reportCommit(1));
        ButtonItem button2 = new ButtonItem("色情低俗", () -> reportCommit(2));
        ButtonItem button3 = new ButtonItem("广告骚扰", () -> reportCommit(3));
        ButtonItem button4 = new ButtonItem("人身攻击", () -> reportCommit(4));
        buttons.add(button1);
        buttons.add(button2);
        buttons.add(button3);
        buttons.add(button4);
        DialogManager dialogManager = null;

        Context context = view.getContext();
        if (context instanceof BaseMvpActivity) {
            dialogManager = ((BaseMvpActivity) context).getDialogManager();
        } else if (context instanceof BaseActivity) {
            dialogManager = ((BaseActivity) context).getDialogManager();
        }
        if (dialogManager != null) {
            dialogManager.showCommonPopupDialog(buttons, "取消");
        }
        this.dismiss();
    }

    private void reportCommit(int reportType) {
        ReportModel reportModel = new ReportModel();
        reportModel.reportCommit(2, reportType, reportCallBack);
    }
}
