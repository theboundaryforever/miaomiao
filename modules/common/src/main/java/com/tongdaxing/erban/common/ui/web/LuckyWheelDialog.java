package com.tongdaxing.erban.common.ui.web;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.miaomiao.ndklib.JniUtils;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.im.actions.GiftAction;
import com.tongdaxing.erban.common.room.gift.GiftDialog;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SignUtils;
import com.tongdaxing.xchat_framework.util.util.VersionUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 大转盘窗口 魔力转圈圈
 */
public class LuckyWheelDialog extends AppCompatDialog implements GiftDialog.OnGiftDialogBtnClickListener, ChargeListener {

    //下一次打开礼物列表时是否刷新
    private final String TAG = LuckyWheelDialog.class.getSimpleName();
    private Context context;
    private WebView webContent;
    //礼物列表弹窗
    private GiftDialog giftDialog;
    private Callback callback;

    public LuckyWheelDialog(Context context, Callback callback) {
        super(context, R.style.dialog);
        this.context = context;
        this.callback = callback;
        init();
    }

    private void init() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_lucky_wheel, null, false);
        setContentView(view);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = ScreenUtil.getScreenWidth(context);
        params.height = (int) (ScreenUtil.getScreenHeight(context) * 0.85);
        params.gravity = Gravity.BOTTOM;
        getWindow().setAttributes(params);
        setCancelable(true);
        setCanceledOnTouchOutside(true);

        webContent = view.findViewById(R.id.webContent);
        initWebView();

        //webContent.loadUrl("http://10.0.1.73:7000/ttyy/newluckdraw/index.html");
        webContent.loadUrl(UriProvider.getSlyderUrl());
    }

    private void initWebView() {
        JSInterface jsInterface = new JSInterface();
        webContent.addJavascriptInterface(jsInterface, "androidJsObj");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // http 与 https 混合的页面
            webContent.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webContent.setBackgroundColor(Color.WHITE);
        webContent.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (isActivityDestroyed(context)) {
                    return;
                }
                if (newProgress > 99) {
                    webContent.setBackgroundColor(0);
                    if (callback != null && !isShowing()) {
                        callback.succed(LuckyWheelDialog.this);
                    }
                }
            }
        });
        webContent.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                if (callback != null && !isShowing()) {
                    callback.failed(LuckyWheelDialog.this);
                }
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                if (callback != null && !isShowing()) {
                    callback.failed(LuckyWheelDialog.this);
                }
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                if (callback != null && !isShowing()) {
                    callback.failed(LuckyWheelDialog.this);
                }
            }
        });
    }

    /**
     * 需要留意点击引爆礼物墙的礼物跳转礼物列表的时候，礼物列表是否已经展示给用户了
     */
    private void clickBtnToShowGiftDialog() {
        if (giftDialog == null) {
            giftDialog = new GiftDialog(context, AvRoomDataManager.get().mMicQueueMemberMap, 1);
            giftDialog.setGiftDialogBtnClickListener(this);
            giftDialog.setOnDismissListener(dialog -> giftDialog = null);
        }
        if (!giftDialog.isShowing()) {
            giftDialog.show();
        }
    }

    @Override
    public void onRechargeBtnClick() {
        //尝试修复https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/23204?pid=1
        if (null != context) {
            MyWalletNewActivity.start(context);
        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number, int sendGiftType) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) return;
        if (giftInfo == null)
            return;
        List<Long> uids = new ArrayList<>();
        uids.add(uid);
        CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), uids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number, int sendGiftType) {
        if (giftInfo == null) return;
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) return;
        List<Long> targetUids = new ArrayList<>();
        for (int i = 0; i < micMemberInfos.size(); i++) {
            targetUids.add(micMemberInfos.get(i).getUid());
        }
        CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), targetUids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
    }

    /**
     * 赠送按钮点击
     * CP判断
     *
     * @param giftInfo
     * @param uid
     */
    @Override
    public void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) return;
        if (giftInfo != null) {
            L.debug(GiftAction.TAG, "LuckyWheelDialog onSendCpGiftBtnClick giftInfo=%s, uid=%d", giftInfo, uid);
            CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftInfo.getGiftId(), uid,
                    1, currentRoomInfo.getUid(), this);
        }
    }

    @Override
    public void onNeedCharge() {
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    MyWalletNewActivity.start(getContext());
                    fragment.dismiss();
                }
            }
        }).show(((FragmentActivity) getContext()).getSupportFragmentManager(), "charge");

    }

    public interface Callback {
        void succed(LuckyWheelDialog dialog);

        void failed(LuckyWheelDialog dialog);
    }

    public class JSInterface {
        /**
         * 减少金币
         */
        @JavascriptInterface
        public void reduceGold(int number) {
            //CoreManager.getCore(IPayCore.class).minusGold(number);
        }

        /**
         * 跳转到充值页
         */
        @JavascriptInterface
        public void jumpToCharge() {
            MyWalletNewActivity.start(context);
        }

        /**
         * 刷新背包礼物
         */
        @JavascriptInterface
        public void refreshGift(String jsonStr) {
            //刷新背包礼物
            CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
            CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        }

        /**
         * 跳转到礼物页
         */
        @JavascriptInterface
        public void jumpToGift() {
            ((Activity) context).runOnUiThread(() -> {
                clickBtnToShowGiftDialog();
                //房间-打开送礼界面
            });
        }

        @JavascriptInterface
        public String getSn(String jsonStr) {
            String sn = "";
            if (TextUtils.isEmpty(jsonStr)) {
                return sn;
            }
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                String url = jsonObject.getString("url");
                JSONObject jsonParams = jsonObject.getJSONObject("data");
                Map<String, String> params = new HashMap<>();
                for (Iterator<String> it = jsonParams.keys(); it.hasNext(); ) {
                    String key = it.next();
                    params.put(key, jsonParams.getString(key));
                }
                String kp = JniUtils.getDk(context);
                sn = SignUtils.signParams(url, params, kp, String.valueOf(System.currentTimeMillis()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return sn;
        }

        /**
         * 获取uid，ticket，roomId
         */
        @JavascriptInterface
        public String getDrawPageInfo() {
            String uid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
            RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            String version = VersionUtil.getLocalName(BasicConfig.INSTANCE.getAppContext());
            long roomId = 0;
            if (currentRoomInfo != null) {
                roomId = currentRoomInfo.getRoomId();
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("uid", uid);
                jsonObject.put("ticket", ticket);
                jsonObject.put("roomId", roomId);
                jsonObject.put("appVersion", version);
                return jsonObject.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "";
        }
    }

    private boolean isActivityDestroyed(Context context) {
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.isFinishing() || activity.isDestroyed()) {
                return true;
            }
        }
        return false;
    }
}
