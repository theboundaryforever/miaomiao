package com.tongdaxing.erban.common.im.holder;

import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.MsgStatusEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.CpGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CpGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.TipAttachment;
import com.tongdaxing.xchat_core.result.WalletInfoResult;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangjian on 2019/3/28.
 */

public class MsgViewHolderCpGift extends MsgViewHolderBase {
    private TextView tvGiftDesAndName;
    private ImageView ivGift;
    private LinearLayout ll_inviting;
    private TextView tv_refuse;
    private TextView tv_agree;
    private TextView tv_result;
    private LinearLayout container;

    public MsgViewHolderCpGift(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.layout_msg_view_holder_cp_invite;
    }

    @Override
    protected void inflateContentView() {
        container = findViewById(R.id.layout_container);
        tvGiftDesAndName = findViewById(R.id.tv_gift_des_and_name);
        ivGift = findViewById(R.id.iv_gift);
        ll_inviting = findViewById(R.id.ll_inviting);
        tv_refuse = findViewById(R.id.tv_refuse);
        tv_agree = findViewById(R.id.tv_agree);
        tv_result = findViewById(R.id.tv_result);
    }

    @Override
    protected void bindContentView() {
        CpGiftAttachment attachment = (CpGiftAttachment) message.getAttachment();
        final CpGiftReceiveInfo giftRecieveInfo = attachment.getGiftReceiveInfo();
        Spanned str = Html.fromHtml("有一天你说你不在爱做梦了，于是我也醒了。"
                + "<strong>想将【" + giftRecieveInfo.getGiftName() + "】赠予你，邀你共度余生。</strong>");
        tvGiftDesAndName.setText(str);
        ImageLoadUtils.loadImage(ivGift.getContext(), giftRecieveInfo.getPicUrl(), ivGift);

        switch (giftRecieveInfo.getStatus()) {
            case CpGiftAttachment.CUSTOM_MSG_CP_INVITE_AGREE:
                ll_inviting.setVisibility(View.GONE);
                tv_result.setVisibility(View.VISIBLE);
                tv_result.setText("已同意");
                break;
            case CpGiftAttachment.CUSTOM_MSG_CP_INVITE_REFUSE:
                ll_inviting.setVisibility(View.GONE);
                tv_result.setVisibility(View.VISIBLE);
                tv_result.setText("已拒绝");
                break;
            case CpGiftAttachment.CUSTOM_MSG_CP_INVITE_TIMEOUT:
                ll_inviting.setVisibility(View.GONE);
                tv_result.setVisibility(View.VISIBLE);
                tv_result.setText("已超时失效");
                break;
            case CpGiftAttachment.CUSTOM_MSG_CP_REMOVE:
                ll_inviting.setVisibility(View.GONE);
                tv_result.setVisibility(View.VISIBLE);
                tv_result.setText("已解散CP");
                break;
        }

        if (giftRecieveInfo.getStatus() == CpGiftAttachment.CUSTOM_MSG_CP_INVITE) {
            //邀请方的uid
            if (giftRecieveInfo.getInviteUid() == CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid()) {
                ll_inviting.setVisibility(View.GONE);
                tv_result.setVisibility(View.VISIBLE);
                tv_result.setText("等待回应");
            } else {
                tv_refuse.setText("残忍拒绝");
                tv_agree.setText("同意");
                ll_inviting.setVisibility(View.VISIBLE);
                tv_result.setVisibility(View.GONE);
            }
        }
        tv_refuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.d("拒绝CP邀请");
                refuseInvite(giftRecieveInfo);
            }
        });
        tv_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.d("同意CP邀请");
                agreeInvite(giftRecieveInfo);
            }
        });
    }

    @Override
    protected int rightBackground() {
        return R.drawable.nim_message_item_right_white_selector;
    }

    private void agreeInvite(CpGiftReceiveInfo giftRecieveInfo) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("recordId", giftRecieveInfo.getRecordId() + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        ResponseListener Listener = new ResponseListener<ServiceResult>() {

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        LogUtil.d("同意了CP邀请");
                        // todo 提示消息还有问题 同意方不能立马看到这条消息
                        TipAttachment tipAttachment = new TipAttachment(401, 401);
                        tipAttachment.setCpInviteSuccess(true);
                        tipAttachment.setTip("CP配对成功啦！快前往你的专属页");
                        IMMessage msg = MessageBuilder.createCustomMessage(String.valueOf(giftRecieveInfo.getInviteUid()), SessionTypeEnum.P2P, tipAttachment);
                        msg.setStatus(MsgStatusEnum.unread);
                        NIMClient.getService(MsgService.class).sendMessage(msg, true).setCallback(new RequestCallback<Void>() {
                            @Override
                            public void onSuccess(Void param) {
                                //todo
                                getMsgAdapter().notifyDataSetChanged();
                            }

                            @Override
                            public void onFailed(int code) {
                                com.juxiao.library_utils.log.LogUtil.d(code);
                            }

                            @Override
                            public void onException(Throwable exception) {
                                exception.printStackTrace();
                            }
                        });
                    } else {
                        SingleToastUtil.showToast(response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
//                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_PERSONAL_GIFT_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.cpAgree(),
                        CommonParamUtil.getDefaultHeaders(context),
                        requestParam, Listener, errorListener,
                        WalletInfoResult.class, Request.Method.GET);
    }

    private void refuseInvite(CpGiftReceiveInfo giftRecieveInfo) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("recordId", giftRecieveInfo.getRecordId() + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        ResponseListener Listener = new ResponseListener<ServiceResult>() {

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        LogUtil.d("拒绝了CP邀请");
                    } else {
                        SingleToastUtil.showToast(response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
//                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_PERSONAL_GIFT_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.cpRefuse(),
                        CommonParamUtil.getDefaultHeaders(context),
                        requestParam, Listener, errorListener,
                        WalletInfoResult.class, Request.Method.GET);
    }
}
