package com.tongdaxing.erban.common.presenter.me.setting;

import android.util.Log;

import com.tongdaxing.erban.common.ui.me.setting.vew.QrCodeScanResultView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtils;

import java.util.Map;

public class QrCodeScanResultPresenter extends AbstractMvpPresenter<QrCodeScanResultView> {

    private final String TAG = QrCodeScanResultPresenter.class.getSimpleName();

    public QrCodeScanResultPresenter() {

    }


    public void loginForPcByQrCode(String uuid) {
        LogUtils.d(TAG, "loginForPcByQrCode-uuid:" + uuid);
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("qrCode", uuid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.qrCodeScan(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                Log.d(TAG, "getMicroMatchPool-onError");
                e.printStackTrace();
                getMvpView().onQrCodeLogin(false, e.getMessage());
            }

            @Override
            public void onResponse(Json response) {
                Log.d(TAG, "getMicroMatchPool-onResponse");
                if (response != null && response.num("code") == 200) {
                    getMvpView().onQrCodeLogin(true, "登陆成功");
                }
            }
        });

    }

}
