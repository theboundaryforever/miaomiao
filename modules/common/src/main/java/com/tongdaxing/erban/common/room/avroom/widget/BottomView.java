package com.tongdaxing.erban.common.room.avroom.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tongdaxing.erban.common.room.avroom.adapter.RoomQuickMsgAdapter;
import com.tongdaxing.erban.common.room.avroom.other.BottomViewListenerWrapper;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.event.SendQuickMsgEvent;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenran
 * @date 2017/7/26
 */

public class BottomView extends RelativeLayout implements View.OnClickListener {
    private String TAG = "BottomView";
    private BottomViewListenerWrapper wrapper;
    private ImageView openMic;
    private LinearLayout sendMsg;
    private ImageView ivSendMsg;
    private ImageView sendGift;
    private ImageView sendGiftHis;
    private ImageView sendFace;
    //    private ImageView share;
    private ImageView remoteMute;
    private LinearLayout faceLayout;
    private LinearLayout micLayout;
    //    private ImageView lottery_box;
    private LinearLayout micInListLayout;
    private ImageView micInListIcon;
    private RelativeLayout rlMsg;
    private RelativeLayout rlMore;
    private ImageView ivMsgMark;
    private ImageView ivMoreMark;
    private boolean micInListOption;

    private ConstraintLayout mRoomQuickMsgLayout;
    private RecyclerView mRoomQuickMsgRcv;
    private ImageView mRoomQuickMsgIvClose;
    private RoomQuickMsgAdapter mRoomQuickMsgAdapter;
    private ArrayList<String> mQquickMsgList;

    public BottomView(Context context) {
        super(context);
        init();
    }

    public BottomView(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    public BottomView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_bottom_view, this);
        Json configData = CoreManager.getCore(VersionsCore.class).getConfigData();
        int micInListOption = configData.num("micInListOption");
        L.debug(TAG, "init configData: %s", configData);
        this.micInListOption = micInListOption == 1;
        openMic = (ImageView) findViewById(R.id.icon_room_open_mic);
        sendMsg = (LinearLayout) findViewById(R.id.icon_room_send_msg);
        ivSendMsg = findViewById(R.id.iv_room_send_msg);
        sendFace = (ImageView) findViewById(R.id.icon_room_face);
        sendGift = (ImageView) findViewById(R.id.icon_room_send_gift);
        sendGiftHis = (ImageView) findViewById(R.id.icon_room_send_gift_his);
//        share = (ImageView) findViewById(R.id.icon_room_share);
        remoteMute = (ImageView) findViewById(R.id.icon_room_open_remote_mic);
//        lottery_box = (ImageView) findViewById(R.id.icon_room_lottery_box);
        faceLayout = (LinearLayout) findViewById(R.id.room_face_layout);
        micLayout = (LinearLayout) findViewById(R.id.room_mic_layout);
        micInListLayout = (LinearLayout) findViewById(R.id.room_mic_layout_mic_in_list);
        micInListIcon = (ImageView) findViewById(R.id.icon_room_mic_in_list);
        rlMsg = findViewById(R.id.rl_room_msg);
        rlMore = findViewById(R.id.rl_room_more);
        ivMsgMark = findViewById(R.id.iv_room_msg_mark);
        ivMoreMark = findViewById(R.id.iv_room_more_mark);
        mRoomQuickMsgLayout = findViewById(R.id.room_quick_msg_layout);
        mRoomQuickMsgRcv = findViewById(R.id.room_quick_msg_rcv);
        mRoomQuickMsgIvClose = findViewById(R.id.room_quick_msg_iv_close);

//        buMicInListList.setOnClickListener(this);

        openMic.setOnClickListener(this);
        sendMsg.setOnClickListener(this);
        sendFace.setOnClickListener(this);
        sendGift.setOnClickListener(this);
        sendGiftHis.setOnClickListener(this);
        remoteMute.setOnClickListener(this);
//        lottery_box.setOnClickListener(this);
        micInListLayout.setOnClickListener(this);
        rlMsg.setOnClickListener(this);
        rlMore.setOnClickListener(this);

        mQquickMsgList = new ArrayList<>();
        mRoomQuickMsgAdapter = new RoomQuickMsgAdapter(getContext(), mQquickMsgList, new RoomQuickMsgAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                mRoomQuickMsgLayout.setVisibility(GONE);
                //发送消息到房间公屏
                CoreUtils.send(new SendQuickMsgEvent(mQquickMsgList.get(position)));
            }
        });
        mRoomQuickMsgRcv.setAdapter(mRoomQuickMsgAdapter);
        mRoomQuickMsgRcv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mRoomQuickMsgIvClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mRoomQuickMsgLayout.setVisibility(GONE);
            }
        });

        setMicBtnEnable(false);
        setMicBtnOpen(false);
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo != null) {
            if (mCurrentRoomInfo.getTagType() == RoomInfo.ROOMTYPE_PERSONAL) {
                if (AvRoomDataManager.get().isRoomOwner()) {
                    sendGift.setVisibility(GONE);
                    sendGiftHis.setVisibility(VISIBLE);
                } else {
                    sendGift.setVisibility(VISIBLE);
                    sendGiftHis.setVisibility(GONE);
                }
            } else {
                sendGift.setVisibility(VISIBLE);
                sendGiftHis.setVisibility(GONE);
            }
            if (mCurrentRoomInfo.getPublicChatSwitch() == 0) {
                setInputMsgBtnEnable(true);
            } else {
                setInputMsgBtnEnable(false);
            }
        }
//        //宝箱开关
//        if (configData.num("lottery_box_option") == 0) {
//            lottery_box.setVisibility(GONE);
//        }
    }

    public void setQuickMsgListData() {
        RoomInfo roominfo = AvRoomDataManager.get().mServiceRoominfo;
        if (roominfo == null) {
            return;
        }
        List<String> fastReply = roominfo.getFastReply();
        L.debug(TAG, "setQuickMsgListData fastReply %s, size = %d", fastReply == null ? "== null" : "!= null", fastReply == null ? -1 : fastReply.size());
        if (fastReply != null && fastReply.size() > 0) {
            mRoomQuickMsgLayout.setVisibility(VISIBLE);
            mQquickMsgList.clear();
            mQquickMsgList.addAll(fastReply);
            mRoomQuickMsgAdapter.notifyDataSetChanged();
        }
    }

    public void setBottomViewListener(BottomViewListenerWrapper wrapper) {
        this.wrapper = wrapper;
    }

    public void setMicBtnEnable(boolean enable) {
        L.info(TAG, "setMicBtnEnable enable: %b", enable);
        if (enable) {
            openMic.setClickable(true);
            openMic.setOnClickListener(this);
        } else {
            openMic.setClickable(false);
            openMic.setOnClickListener(null);
        }
    }

    public void setMicBtnOpen(boolean isOpen) {
        L.info(TAG, "setMicBtnOpen isOpen = " + isOpen);
        if (isOpen) {
            openMic.setImageResource(R.drawable.icon_room_open_mic);
        } else {
            openMic.setImageResource(R.drawable.icon_room_close_mic);
        }
    }

    public void setRemoteMuteOpen(boolean isOpen) {
        L.info(TAG, "setRemoteMuteOpen isOpen = " + isOpen);
        if (isOpen) {
            remoteMute.setImageResource(R.drawable.icon_remote_mute_close);
        } else {
            remoteMute.setImageResource(R.drawable.icon_remote_mute_open);
        }
    }

    public void showHomePartyUpMicBottom() {
        faceLayout.setVisibility(VISIBLE);
        micLayout.setVisibility(VISIBLE);
        if (micInListOption)
            micInListLayout.setVisibility(GONE);
    }

    public void showHomePartyDownMicBottom() {
        faceLayout.setVisibility(GONE);
        micLayout.setVisibility(GONE);
        if (micInListOption) {
            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (roomInfo != null) {
                int type = roomInfo.getTagType();
                if (type == RoomInfo.ROOMTYPE_LOVERS || type == RoomInfo.ROOMTYPE_PERSONAL) {
                    micInListLayout.setVisibility(GONE);
                } else {
                    micInListLayout.setVisibility(VISIBLE);
                }
            }
        }
    }

    /**
     * 新私聊消息提示
     *
     * @param isShow true 显示 false不显示
     */
    public void showMsgMark(boolean isShow) {
        if (ivMsgMark != null)
            ivMsgMark.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 更多功能消息提示
     *
     * @param isShow true 显示 false不显示
     */
    public void showMoreMark(boolean isShow) {
        if (ivMoreMark != null)
            ivMoreMark.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 公屏发言按钮状态
     *
     * @param enable
     */
    public void setInputMsgBtnEnable(boolean enable) {
        if (!enable) {//禁言
            sendMsg.setEnabled(enable);
            ivSendMsg.setEnabled(enable);
        } else {//
            sendMsg.setEnabled(enable);
            ivSendMsg.setEnabled(enable);
        }
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.icon_room_open_mic) {
            if (wrapper != null) {
                wrapper.onOpenMicBtnClick();
            }

        } else if (i == R.id.icon_room_send_msg) {
            if (wrapper != null) {
                wrapper.onSendMsgBtnClick();
            }

        } else if (i == R.id.icon_room_send_gift) {
            if (wrapper != null) {
                wrapper.onSendGiftBtnClick();
            }

        } else if (i == R.id.icon_room_face) {
            if (wrapper != null) {
                wrapper.onSendFaceBtnClick();
            }

        } else if (i == R.id.icon_room_send_gift_his) {
            if (wrapper != null) {
                wrapper.onSendGiftHis();
            }

        } else if (i == R.id.icon_room_open_remote_mic) {
            if (wrapper != null) {
                wrapper.onRemoteMuteBtnClick();
            }

//            case R.id.icon_room_lottery_box:
//                if (wrapper != null) {
//                    wrapper.onLotteryBoxeBtnClick();
//                }
//                break;
        } else if (i == R.id.room_mic_layout_mic_in_list) {
            if (wrapper != null) {
                wrapper.onBuShowMicInList();
            }

        } else if (i == R.id.rl_room_msg) {
            if (wrapper != null) {
                wrapper.onMsgBtnClick();
            }

        } else if (i == R.id.rl_room_more) {
            if (wrapper != null) {
                wrapper.onMoreBtnClick();
            }

        } else {
        }
    }
}
