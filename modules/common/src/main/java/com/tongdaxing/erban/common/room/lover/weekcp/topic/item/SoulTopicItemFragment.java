package com.tongdaxing.erban.common.room.lover.weekcp.topic.item;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.erban.ui.adapter.BaseAdapterHelper;
import com.erban.ui.adapter.ListAdapter;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.room.lover.weekcp.topic.view.SoulTopicAction;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

/**
 * Created by Chen on 2019/5/8.
 */
@CreatePresenter(SoulTopicItemPresenter.class)
public class SoulTopicItemFragment extends BaseMvpFragment<ISoulTopicItemView, SoulTopicItemPresenter> implements ISoulTopicItemView {
    private static final String currentIndexKey = "currentIndex";
    @BindView(R2.id.room_lover_gv_soul_topic)
    GridView mRoomLoverGvSoulTopic;
    private ListAdapter<String> mSoulTopicAdapter;
    private int mCurrentIndex = 0;

    public static SoulTopicItemFragment newInstance(int mCurrentIndex) {
        Bundle args = new Bundle();
        args.putInt(currentIndexKey, mCurrentIndex);
        SoulTopicItemFragment fragment = new SoulTopicItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.room_lover_soul_topic_grid_view;
    }

    @Override
    public void onFindViews() {
        ButterKnife.bind(this, mView);
    }

    @Override
    public void onSetListener() {
        if (getArguments() != null) {
            mCurrentIndex = getArguments().getInt(currentIndexKey);
        }
    }

    @Override
    public void initiate() {
        initData(getMvpPresenter().getData(mCurrentIndex));
    }

    private void initData(List<String> list) {
        if (list == null) {
            return;
        }
        if (mSoulTopicAdapter == null) {
            mSoulTopicAdapter = new ListAdapter<String>(getContext(), R.layout.room_lover_soul_topic_item, list) {
                @Override
                public void onUpdate(BaseAdapterHelper helper, String item, int position) {
                    TextView textView = helper.getView(R.id.room_lover_soul_question);
                    textView.setText(item);
                }
            };
            mRoomLoverGvSoulTopic.setAdapter(mSoulTopicAdapter);
        } else {
            mSoulTopicAdapter.notifyDataSetChanged();
        }
    }


    @OnItemClick(R2.id.room_lover_gv_soul_topic)
    public void onQuestionItemClicked(AdapterView<?> parent, View view, int position, long id) {
        List<String> questions = getMvpPresenter().getData(mCurrentIndex);
        if (questions == null) {
            return;
        }
        getMvpPresenter().requestRoomPlay(questions.get(position));
        CoreUtils.send(new SoulTopicAction.OnSoulTopicDismiss());
    }
}
