package com.tongdaxing.erban.common.ui.widget.itemdecotion;


public interface GroupListener {
    String getGroupName(int position);
}
