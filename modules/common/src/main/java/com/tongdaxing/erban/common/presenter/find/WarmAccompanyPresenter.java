package com.tongdaxing.erban.common.presenter.find;

import com.tongdaxing.erban.common.ui.find.model.WarmAccompanyModel;
import com.tongdaxing.erban.common.ui.find.view.IWarmAccompanyView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.find.bean.WarmAccompanyInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class WarmAccompanyPresenter extends AbstractMvpPresenter<IWarmAccompanyView> {
    private int currPage = Constants.PAGE_START;
    private WarmAccompanyModel warmAccompanyModel;

    public WarmAccompanyPresenter() {
        warmAccompanyModel = new WarmAccompanyModel();
    }

    public void refreshData(int gender) {
        loadData(Constants.PAGE_START, gender);
    }

    public void loadMoreData(int gender) {
        loadData(currPage + 1, gender);
    }

    public void loadData(int page, int gender) {
        warmAccompanyModel.loadWarmAccompanyData(page, gender, new OkHttpManager.MyCallBack<ServiceResult<List<WarmAccompanyInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(page == Constants.PAGE_START);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<WarmAccompanyInfo>> response) {
                currPage = page;
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().setupSuccessView(response.getData(), page == Constants.PAGE_START);//直接展示数据
                    } else {
                        onError(new Exception());
                    }
                } else {
                    onError(new Exception());
                }
            }
        });
    }
}
