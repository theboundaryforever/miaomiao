package com.tongdaxing.erban.common.ui.me.withdraw;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.ui.login.CodeDownTimer;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCoreClient;
import com.tongdaxing.xchat_core.withdraw.bean.WxVerifyInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.Map;

/**
 * Function:
 * Author: Edward on 2019/1/9
 */
public class WxBindingActivity extends BaseActivity implements View.OnClickListener {

    private Button btnSure, btnGetCode;
    private TextView tvPhone;
    private EditText etInputRealName, etInputCode;

    public static void go(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, WxBindingActivity.class);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wx_binding);
        initTitleBar("绑定微信提现验证");

        btnGetCode = (Button) findViewById(R.id.btn_get_code);
        btnGetCode.setOnClickListener(this);
        btnSure = (Button) findViewById(R.id.btn_sure);
        btnSure.setOnClickListener(this);
        tvPhone = (TextView) findViewById(R.id.tv_phone);
        etInputRealName = (EditText) findViewById(R.id.et_input_real_name);
        etInputCode = (EditText) findViewById(R.id.et_input_code);

        initData();
    }

    private void initData() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            if (!TextUtils.isEmpty(userInfo.getPhone())) {
                tvPhone.setText(userInfo.getPhone());
            }
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btn_sure) {
            submitVerify();

        } else if (i == R.id.btn_get_code) {
            getVerifyCode();

        }
    }

    /**
     * 提交验证
     */
    private void submitVerify() {
        if (TextUtils.isEmpty(etInputRealName.getText().toString())) {
            SingleToastUtil.showToast("真实姓名不能为空!");
            return;
        }

        if (TextUtils.isEmpty(etInputCode.getText().toString())) {
            SingleToastUtil.showToast("验证码不能为空!");
            return;
        }

        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("code", etInputCode.getText().toString());
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.checkCode(), params, new OkHttpManager.MyCallBack<ServiceResult<WxVerifyInfo>>() {
            @Override
            public void onError(Exception e) {
                toast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<WxVerifyInfo> response) {
                if (response != null && response.isSuccess()) {
                    WxVerifyInfo wxVerifyInfo = new WxVerifyInfo();
                    UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                    if (userInfo != null) {
                        wxVerifyInfo.setPhone(userInfo.getPhone());
                    }
                    wxVerifyInfo.setVerifyCode(etInputCode.getText().toString());
                    wxVerifyInfo.setRealName(etInputRealName.getText().toString());
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("data", wxVerifyInfo);
                    Intent intent = new Intent();
                    intent.putExtras(bundle);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    if (response != null && !TextUtils.isEmpty(response.getMessage())) {
                        toast(response.getMessage());
                    } else {
                        toast("数据错误!");
                    }
                }
            }
        });

    }

    /**
     * 获取验证码
     */
    private void getVerifyCode() {
        if (TextUtils.isEmpty(tvPhone.getText().toString())) {
            return;
        }

        CodeDownTimer timer = new CodeDownTimer(btnGetCode, 60000, 1000);
        timer.start();
        CoreManager.getCore(IWithdrawCore.class).getSmsCode(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetSmsCodeFail(String error) {
        toast(error);
    }
}
