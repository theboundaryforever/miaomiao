package com.tongdaxing.erban.common.ui.me.wallet.presenter;

import android.text.TextUtils;

import com.tongdaxing.erban.common.ui.me.wallet.view.IExchangeGoldView;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.bean.ExchangeAwardInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;

import static com.tongdaxing.erban.libcommon.net.rxnet.RxNet.getContext;

/**
 * Created by MadisonRong on 09/01/2018.
 */

public class ExchangeGoldPresenter extends PayPresenter<IExchangeGoldView> {

    public void calculateResult(String input) {
        if (!StringUtil.isEmpty(input) && isNumeric(input)) {
            long value = 0;
            try {
                value = Long.parseLong(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (isTenMultiple(value)) {
                if (getMvpView() != null)
                    getMvpView().displayResult(value + "");
            } else {
                if (getMvpView() != null)
                    getMvpView().displayResult(0 + "");
            }
        } else {
            if (getMvpView() != null)
                getMvpView().displayResult(0 + "");
        }
    }

    public void confirmToExchangeGold(String input) {
        confirmToExchangeGold(input, "");
    }

    public void confirmToExchangeGold(String input, String sms) {
        if (StringUtil.isEmpty(input)) {
            getMvpView().toastForError(R.string.exchange_gold_error_empty_input);
            return;
        }

        long value = Long.parseLong(input);
        if (!isTenMultiple(value)) {
            getMvpView().toastForError(R.string.exchange_gold_error_is_not_ten_multiple);
            return;
        }
        if (walletInfo == null) {
            return;
        }
        if (value > walletInfo.getDiamondNum()) {
            getMvpView().toastForError(R.string.exchange_gold_error_diamond_less);
            return;
        }
        if (TextUtils.isEmpty(sms))
            getMvpView().requestExchangeGold(value);
        else
            getMvpView().requestExchangeGold(value, sms);
    }


    public void exchangeGold(String diamondNum) {
//        execute(payModel.getPayService()
//                        .requestExchangeGold(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()),
//                                diamondNum, CoreManager.getCore(IAuthCore.class).getTicket()),
//                new CallBack<ExchangeAwardInfo>() {
//                    @Override
//                    public void onSuccess(ExchangeAwardInfo data) {
//                        walletInfo = data;
//                        getMvpView().exchangeGold(data);
//                        getMvpView().setupUserWalletBalance(data);
//                        getMvpView().showAward(data);
//                        CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(data);
//                    }
//
//                    @Override
//                    public void onFail(int code, String error) {
//
//                        getMvpView().exchangeGoldFail(code, error);
//                    }
//                });
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        requestParam.put("diamondNum", diamondNum);
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<ServiceResult<ExchangeAwardInfo>>() {

            @Override
            public void onResponse(ServiceResult<ExchangeAwardInfo> data) {
                if (null != data && data.isSuccess() && data.getData() != null) {
                    walletInfo = data.getData();
                    if (getMvpView() != null) {
                        getMvpView().exchangeGold(data.getData());
                        getMvpView().setupUserWalletBalance(data.getData());
                        getMvpView().showAward(data.getData());
                    }
                    CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(data.getData());
                } else {
                    if (getMvpView() != null && data != null)
                        getMvpView().exchangeGoldFail(data.getCode(), data.getErrorMessage());
                }
            }

        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if (getMvpView() != null && error != null)
                    getMvpView().exchangeGoldFail(error.responseData.statusCode, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.changeGold(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    public void exchangeGold(String diamondNum, String sms) {
//        execute(payModel.getPayService()
////                        .requestExchangeGoldSms(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()),
////                                diamondNum, sms, CoreManager.getCore(IAuthCore.class).getTicket()),
////                new CallBack<ExchangeAwardInfo>() {
////                    @Override
////                    public void onSuccess(ExchangeAwardInfo data) {
////                        walletInfo = data;
////                        getMvpView().exchangeGold(data);
////                        getMvpView().setupUserWalletBalance(data);
////                        getMvpView().showAward(data);
////
////                        CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(data);
////                    }
////
////                    @Override
////                    public void onFail(int code, String error) {
////
////                        getMvpView().exchangeGoldFail(code, error);
////                    }
////                });

        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        requestParam.put("diamondNum", diamondNum);
        requestParam.put("smsCode", sms);
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<ServiceResult<ExchangeAwardInfo>>() {

            @Override
            public void onResponse(ServiceResult<ExchangeAwardInfo> data) {
                if (null != data && data.isSuccess()) {
                    walletInfo = data.getData();
                    getMvpView().exchangeGold(data.getData());
                    getMvpView().setupUserWalletBalance(data.getData());
                    getMvpView().showAward(data.getData());
                    CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(data.getData());
                } else {
                    getMvpView().exchangeGoldFail(data.getCode(), data.getErrorMessage());
                }
            }

        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                getMvpView().exchangeGoldFail(error.responseData.statusCode, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.changeGold(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    private boolean isTenMultiple(long number) {
        long value = number % 10;
        if (value == 0) {
            return true;
        }
        return false;
    }

    private boolean isNumeric(String str) {
        for (int i = str.length(); --i >= 0; ) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
