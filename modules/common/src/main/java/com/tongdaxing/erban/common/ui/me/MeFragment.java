package com.tongdaxing.erban.common.ui.me;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.ui.find.activity.CreateRoomActivity;
import com.tongdaxing.erban.common.ui.me.bills.view.BillItemView;
import com.tongdaxing.erban.common.ui.me.setting.activity.QrCodeScanerActivity;
import com.tongdaxing.erban.common.ui.me.task.view.IMeView;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.common.view.LevelView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.room.bean.TaskBean;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by MadisonRong on 04/01/2018.
 */
@CreatePresenter(MePresenter.class)
public class MeFragment extends BaseMvpFragment<IMeView, MePresenter>
        implements View.OnClickListener, IMeView {
    public static final String TAG = "MeFragment";

    @BindView(R2.id.me_tv_user_name)
    TextView mTvUserName;
    @BindView(R2.id.me_tv_user_id)
    TextView mTvUserId;
    @BindView(R2.id.me_iv_user_head)
    RoundedImageView mIvUserHead;
    @BindView(R2.id.me_tv_user_attentions)
    TextView mTvUserAttentions;
    @BindView(R2.id.tv_user_attention_text)
    TextView mTvUserAttentionText;
    @BindView(R2.id.me_tv_user_fans)
    TextView mTvUserFans;
    @BindView(R2.id.tv_user_fan_text)
    TextView mTvFansText;
    @BindView(R2.id.me_tv_friends)
    TextView mMeTvFriend;
    @BindView(R2.id.me_tv_friends_text)
    TextView mTvFriendText;
    @BindView(R2.id.me_item_task)
    BillItemView mMeItemTask;
    @BindView(R2.id.iv_task_logo)
    ImageView ivTaskLogo;
    @BindView(R2.id.me_item_wallet)
    BillItemView mMeItemWallet;
    @BindView(R2.id.me_item_income)
    BillItemView mMeItemIncome;
    @BindView(R2.id.me_item_feedback)
    BillItemView mMeItemSetting;
    @BindView(R2.id.me_item_auth)
    BillItemView mMeItemAuth;
    @BindView(R2.id.me_item_charge)
    BillItemView mMeItemCharge;
    @BindView(R2.id.me_item_create_my_room)
    BillItemView mMeItemCreateMyRoom;
    @BindView(R2.id.me_level_info)
    LevelView mLevelView;
    @BindView(R2.id.iv_custom_label)
    ImageView ivCustomLabel;
    @BindView(R2.id.me_item_level)
    BillItemView mMeItemLevel;
    @BindView(R2.id.me_car)
    BillItemView meCar;
    Unbinder unbinder;
    private long erbanNo;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_me;
    }

    @Override
    public void onFindViews() {
        //
    }

    @Override
    public void onSetListener() {
        mIvUserHead.setOnClickListener(this);
        mTvUserName.setOnClickListener(this);
        mTvUserAttentions.setOnClickListener(this);
        mTvUserAttentionText.setOnClickListener(this);
        mTvUserFans.setOnClickListener(this);
        mTvFansText.setOnClickListener(this);

        mMeItemTask.setOnClickListener(this);
        mMeItemWallet.setOnClickListener(this);
        mMeItemIncome.setOnClickListener(this);
        mMeItemSetting.setOnClickListener(this);
        mMeItemAuth.setOnClickListener(this);
        mMeItemCharge.setOnClickListener(this);
        mMeItemLevel.setOnClickListener(this);
        meCar.setOnClickListener(this);
        mMeItemCreateMyRoom.setOnClickListener(v -> {
//            getDialogManager().showProgressDialog(getActivity(), "请稍后...");
//            CoreManager.getCore(IRoomCore.class).requestRoomInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid(), 0);
            UmengEventUtil.getInstance().onCreateRoom(mContext, "me");
            CreateRoomActivity.start(mContext);
        });
    }

    @Override
    public void initiate() {
        showCustomLabel();
    }

    private void showCustomLabel() {
        Json json = CoreManager.getCore(VersionsCore.class).getConfigData();
        String customLabelUrl = json.str(Constants.MALL_MENU_LABEL);
        if (TextUtils.isEmpty(customLabelUrl)) {
            ivCustomLabel.setVisibility(View.GONE);
        } else {
            ivCustomLabel.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(getActivity(), customLabelUrl, ivCustomLabel);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getMvpPresenter().initUserData();
        //切换用户更新任务
        updateUserTask();
    }

    //关注更新用户资料
    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long uid) {
        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    //取消关注更新用户资料
    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid) {
        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        updateUserInfoUI(userInfo);
    }

    @Override
    public void onClick(View view) {
        getMvpPresenter().clickBy(view.getId(), getContext());
    }

    @Override
    public void updateUserInfoUI(UserInfo userInfo) {
        if (userInfo != null) {
            mTvUserName.setText(userInfo.getNick());
            erbanNo = userInfo.getErbanNo();
            mTvUserId.setText(getString(R.string.me_user_id, erbanNo));
            ImageLoadUtils.loadCircleImage(mContext, userInfo.getAvatar(), mIvUserHead, R.drawable.ic_no_avatar);
            mTvUserAttentions.setText(String.valueOf(userInfo.getFollowNum()));
            mTvUserFans.setText(String.valueOf(userInfo.getFansNum()));
            // todo 好友数量 后台还做不了
//            mMeTvFriend.setText(String.valueOf(userInfo.get()));
            // todo 钱包余额
//            mMeItemWallet.setRightText();
            // todo 收益数量
//            mMeItemIncome.setRightText();
            int charmLevel = userInfo.getCharmLevel();
            mLevelView.setCharmLevel(charmLevel);
            int experLevel = userInfo.getExperLevel();
            mLevelView.setExperLevel(experLevel);

            if (charmLevel > 0 || experLevel > 0) {
                mLevelView.setVisibility(View.VISIBLE);
            } else {
                mLevelView.setVisibility(View.GONE);
            }

        } else {
            mTvUserName.setText("");
            mTvUserId.setText(getString(R.string.me_user_id_null));
            mTvUserAttentions.setText("0");
            mTvUserFans.setText("0");
            mLevelView.setVisibility(View.GONE);
            mLevelView.setExperLevel(0);
            mLevelView.setCharmLevel(0);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void updateUserTask() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getTaskList(), params, new OkHttpManager.MyCallBack<ServiceResult<TaskBean>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<TaskBean> response) {
                if (response != null && response.isSuccess() && response.getData() != null) {
                    if (!ListUtils.isListEmpty(response.getData().getFresh())) {
                        if (ivTaskLogo != null)
                            ivTaskLogo.setVisibility(View.VISIBLE);
                    } else {
                        if (ivTaskLogo != null)
                            ivTaskLogo.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @OnClick(R2.id.me_scan)
    public void onMeScanClicked() {
        QrCodeScanerActivity.start(mContext);
    }

    @OnClick(R2.id.me_setting)
    public void onMeSettingClicked() {
        UIHelper.showSettingAct(mContext);
    }

    @OnClick(R2.id.me_edit_my_user_info)
    public void onMeEditMyUserInfoClicked() {
        //修改个人资料
        UIHelper.showUserInfoModifyAct(mContext, CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    /**
     * 复制喵喵ID到剪切板
     */
    @OnClick(R2.id.me_copy_id)
    public void onMeCopyIdClicked() {
        if (erbanNo != 0 && mContext != null) {
            //获取剪贴板管理器：
            ClipboardManager cm = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
            // 创建普通字符型ClipData
            ClipData mClipData = ClipData.newPlainText("Label", String.valueOf(erbanNo));
            // 将ClipData内容放到系统剪贴板里。
            if (cm != null) {
                cm.setPrimaryClip(mClipData);
                SingleToastUtil.showToast("已复制ID到剪切板");
            } else {
                SingleToastUtil.showToast("复制失败");
            }
        } else {
            SingleToastUtil.showToast("复制失败");
        }
    }
}
