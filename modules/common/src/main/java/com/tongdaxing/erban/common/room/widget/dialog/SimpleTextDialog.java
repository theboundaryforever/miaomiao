package com.tongdaxing.erban.common.room.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.tongdaxing.erban.common.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.common.R;

/**
 * 房间CP邀请拒绝弹窗
 * <p>
 * Created by zhangjian on 2019/3/29.
 */
public class SimpleTextDialog extends BaseDialogFragment {
    private String info;

    public SimpleTextDialog() {
    }

    public SimpleTextDialog(String info) {
        this.info = info;
    }

    public static SimpleTextDialog instance(String info) {
        return new SimpleTextDialog(info);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View view = inflater.inflate(R.layout.dialog_simple_text, ((ViewGroup) window.findViewById(android.R.id.content)), false);//需要用android.R.id.content这个view
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        TextView tv_title = view.findViewById(R.id.tv_title);
        TextView tv_sure = view.findViewById(R.id.tv_sure);
        tv_title.setText(info);
        tv_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
