package com.tongdaxing.erban.common.model.homeattention;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

public class HomeAttentionModel extends BaseMvpModel {

    /**
     * 关注房间列表接口
     *
     * @param pageNum  页码
     * @param pageSize 一页item数
     */
    public void getAttentionListRoom(int pageNum, int pageSize, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getAttentionListRoom(), params, callBack);
    }

    /**
     * 随机热门房间
     */
    public void randomHotRoom(OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doGetRequest(UriProvider.randomHotRoom(), params, callBack);
    }

    /**
     * 关注的人列表接口
     *
     * @param pageNum  页码
     * @param pageSize 一页item数
     */
    public void getAttentionPeopleList(int pageNum, int pageSize, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("pageNo", pageNum + "");
        params.put("pageSize", pageSize + "");

        OkHttpManager.getInstance().doGetRequest(UriProvider.getAllFansOnLine(), params, callBack);
    }

    /**
     * 关注
     */
    public void praise(long likedUid, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("type", String.valueOf(1));
        params.put("likedUid", likedUid + "");

        OkHttpManager.getInstance().doPostRequest(UriProvider.praise(), params, callBack);
    }
}
