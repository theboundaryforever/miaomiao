package com.tongdaxing.erban.common.ui.find.view;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.room.avroom.other.ScrollSpeedLinearLayoutManger;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.DividerItemDecoration;
import com.tongdaxing.erban.common.ui.widget.marqueeview.Utils;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SessionHelper;
import com.tongdaxing.erban.common.view.LevelView;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PublicChatRoomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * 闲聊广场消息界面
 *
 * @author chenran
 * @date 2017/7/26
 */
public class PublicMessageView extends FrameLayout {
    private RecyclerView messageListView;
    private TextView tvBottomTip;
    private MessageAdapter mMessageAdapter;
    private ScrollSpeedLinearLayoutManger layoutManger;
    private int mLastVisibleItemPosition;
    private String TAG = "PublicMessageView";
    private boolean mIsFirst = true;

    public PublicMessageView(Context context) {
        this(context, null);
    }

    public PublicMessageView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public PublicMessageView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    private void init(Context context) {
        // 内容区域
        layoutManger = new ScrollSpeedLinearLayoutManger(context);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.rightMargin = Utils.dip2px(context, 5);
        messageListView = new RecyclerView(context);
        messageListView.setLayoutParams(params);
        messageListView.setOverScrollMode(OVER_SCROLL_NEVER);
        messageListView.setHorizontalScrollBarEnabled(false);
        addView(messageListView);
        messageListView.setLayoutManager(layoutManger);
        messageListView.addItemDecoration(new DividerItemDecoration(context, layoutManger.getOrientation(), 3, R.color.transparent));
        mMessageAdapter = new MessageAdapter();
        mMessageAdapter.setNewData(CoreManager.getCore(IRoomCore.class).getPublicChatRoomManager().getmChatRoomMessages());
        messageListView.setAdapter(mMessageAdapter);

        // 底部有新消息
        tvBottomTip = new TextView(context);
        LayoutParams params1 = new LayoutParams(
                Utils.dip2px(context, 100F), Utils.dip2px(context, 30));
        params1.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        params1.bottomMargin = Utils.dip2px(context, 0);
        tvBottomTip.setBackgroundResource(R.drawable.bg_messge_view_bottom_tip);
        tvBottomTip.setGravity(Gravity.CENTER);
        tvBottomTip.setText(context.getString(R.string.message_view_bottom_tip));
        tvBottomTip.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.white));
        tvBottomTip.setLayoutParams(params1);
        tvBottomTip.setVisibility(GONE);
        tvBottomTip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideBottomTip();
            }
        });
        addView(tvBottomTip);

        messageListView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                refreshLastVisibleItem();
                if (mLastVisibleItemPosition + 1 >= mMessageAdapter.getItemCount()) {
                    tvBottomTip.setVisibility(View.GONE);
                }
            }
        });

    }

    private void hideBottomTip() {
        tvBottomTip.setVisibility(GONE);
        L.debug(TAG, "scrollToPosition %d", mMessageAdapter.getItemCount() - 1);
        messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
    }

    public void refreshLastVisibleItem() {
        if (layoutManger == null) {
            return;
        }
        mLastVisibleItemPosition = layoutManger.findLastVisibleItemPosition();
    }

    private void addMessageList(List<ChatRoomMessage> messages) {
        mMessageAdapter.notifyDataSetChanged();
        if (mIsFirst) {
            mIsFirst = false;
            hideBottomTip();
        }
    }

    private void addMessage(ChatRoomMessage message) {
        mMessageAdapter.notifyDataSetChanged();
    }

    public void onCurrentRoomReceiveNewMsg(ChatRoomMessage message) {
        if (message == null) {
            return;
        }
        addMessage(message);
        showTipsOrScrollToBottom();
    }

    public void onCurrentRoomReceiveNewMsg(List<ChatRoomMessage> messages) {
        if (ListUtils.isListEmpty(messages)) {
            return;
        }
        ChatRoomMessage message = messages.get(0);
        if (messages.size() == 1 && message != null) {//单个消息走单个的添加方法
            onCurrentRoomReceiveNewMsg(message);
            return;
        }
        addMessageList(messages);
        showTipsOrScrollToBottom();
    }


    private void showTipsOrScrollToBottom() {
        // 最后一个item是否显示出来
        int len = mMessageAdapter.getItemCount();
        refreshLastVisibleItem();
        L.debug(TAG, "mLastVisibleItemPosition = %d, len = %d", mLastVisibleItemPosition, len);
        if (mLastVisibleItemPosition + 5 >= len) {
            messageListView.smoothScrollToPosition(len);
            tvBottomTip.setVisibility(View.GONE);
        } else {
            tvBottomTip.setVisibility(View.VISIBLE);
        }
    }

    public void release() {
    }

    private static class MessageAdapter extends BaseQuickAdapter<ChatRoomMessage, BaseViewHolder> implements OnClickListener {
        private String account;

        MessageAdapter() {
            super(R.layout.list_item_public_chatrrom_msg);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        protected void convert(BaseViewHolder baseViewHolder, ChatRoomMessage chatRoomMessage) {
            if (chatRoomMessage == null) {
                return;
            }
            ImageView userIcon = baseViewHolder.getView(R.id.iv_user_icon_chat_room);
            TextView userNick = baseViewHolder.getView(R.id.tv_user_nick_chat_room);
            TextView content = baseViewHolder.getView(R.id.tv_content_chat_room);
            ImageView ivOfficial = baseViewHolder.getView(R.id.iv_chat_hall_official);
            LevelView levelView = baseViewHolder.getView(R.id.level_chat_room);
            LinearLayout linearLayout = baseViewHolder.getView(R.id.ll_content_chat_room_bg);
            if (ivOfficial.getVisibility() == View.VISIBLE) {
                ivOfficial.setVisibility(View.GONE);
            }
            MsgTypeEnum msgType = chatRoomMessage.getMsgType();
            if (msgType == MsgTypeEnum.custom) {
                CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
                int experLevel = -1;
                LogUtil.d("chatRoomMessage", 2 + "");
                if (chatRoomMessage.getAttachment() instanceof CustomAttachment) {

                    experLevel = attachment.getExperLevel();
                    LogUtil.d("chatRoomMessage", experLevel + ";;;;");
                    if (experLevel > 0 && !(chatRoomMessage.getContent() + "").contains("喵喵工作人员")) {
                        levelView.setVisibility(VISIBLE);
                        levelView.setExperLevel(experLevel);
                    } else {
                        levelView.setVisibility(GONE);
                    }
                }
                LogUtil.d("chatRoomMessage", "attachment.getFirst: " + attachment.getFirst());
                if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM) {
                    baseViewHolder.getView(R.id.rl_chate_hall_msg).setVisibility(VISIBLE);
//                    baseViewHolder.getView(R.id.line_chat_room).setVisibility(VISIBLE);
                    setPublicChatRoom(chatRoomMessage, content, levelView, userNick, userIcon, linearLayout, ivOfficial);
                } else {
                    baseViewHolder.getView(R.id.rl_chate_hall_msg).setVisibility(GONE);
//                    baseViewHolder.getView(R.id.line_chat_room).setVisibility(GONE);
                }

            } else {
                baseViewHolder.getView(R.id.rl_chate_hall_msg).setVisibility(GONE);
//                baseViewHolder.getView(R.id.line_chat_room).setVisibility(GONE);
            }
        }

        private void setPublicChatRoom(ChatRoomMessage chatRoomMessage, TextView tvContent, LevelView levelView, TextView userNick, ImageView userIcon, LinearLayout linearLayout, ImageView ivOfficial) {
            MsgAttachment attachment = chatRoomMessage.getAttachment();
            String msg = "";
            com.tongdaxing.xchat_framework.util.util.Json json = null;
            String avatarUrl = "";
            int charmLevel = 0;
            String contentColor = "#1A1A1A";
            if (attachment instanceof PublicChatRoomAttachment) {
                msg = ((PublicChatRoomAttachment) attachment).getMsg();
                String params = ((PublicChatRoomAttachment) attachment).getParams();
                json = com.tongdaxing.xchat_framework.util.util.Json.parse(params);
                charmLevel = json.num("charmLevel");
                avatarUrl = json.str("avatar");
                contentColor = json.str("txtColor", "#1A1A1A");
                LogUtil.d("setPublicChatRoom", params + "");
            } else {
                json = new com.tongdaxing.xchat_framework.util.util.Json();
            }
            com.tongdaxing.xchat_framework.util.util.Json finalJson = json;
            long uid = finalJson.num("uid");
            if (uid == SessionHelper.getChatHallOfficialUid()) {
                if (ivOfficial.getVisibility() == View.GONE)
                    ivOfficial.setVisibility(View.VISIBLE);
            } else {
                if (ivOfficial.getVisibility() == View.VISIBLE)
                    ivOfficial.setVisibility(View.GONE);
            }
            userIcon.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    NewUserInfoActivity.start(mContext, uid);
                }
            });
            linearLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    NewUserInfoActivity.start(mContext, uid);
//                    CoreManager.getCore(IRoomCore.class).getUserRoom(finalJson.num("uid"));
                }
            });
            String senderNick;
            int experLevel = -1;
            if (chatRoomMessage.getChatRoomMessageExtension() == null) {

                senderNick = json.str("nick", "我");
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (userInfo != null) {
                    experLevel = userInfo.getExperLevel();
                }
            } else {
                senderNick = chatRoomMessage.getChatRoomMessageExtension().getSenderNick();
                try {
                    experLevel = (Integer) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get("experLevel");
                } catch (Exception e) {

                }
            }
            if (experLevel > 0 || charmLevel > 0) {
                levelView.setVisibility(VISIBLE);
                levelView.setExperLevel(experLevel);
                levelView.setCharmLevel(charmLevel);
            } else {
                levelView.setVisibility(GONE);
            }
            ImageLoadUtils.loadCircleImage(mContext, avatarUrl, userIcon, R.drawable.ic_no_avatar);
            userNick.setText(senderNick);
            tvContent.setText(msg);
            tvContent.setTextColor(Color.parseColor(contentColor));
        }


        @Override
        public void onClick(View v) {
            ChatRoomMessage chatRoomMessage = (ChatRoomMessage) v.getTag();
            if (chatRoomMessage.getMsgType() != MsgTypeEnum.tip) {
                if (chatRoomMessage.getMsgType() == MsgTypeEnum.text) {
                    account = chatRoomMessage.getFromAccount();
                } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.notification) {
                    account = chatRoomMessage.getFromAccount();
                } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
                    if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                        GiftAttachment giftAttachment = (GiftAttachment) attachment;
                        if (giftAttachment != null) {
                            GiftReceiveInfo giftRecieveInfo = giftAttachment.getGiftRecieveInfo();
                            if (giftRecieveInfo != null) {
                                account = giftRecieveInfo.getUid() + "";
                            }

                        }


                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                        MultiGiftAttachment giftAttachment = (MultiGiftAttachment) attachment;
                        account = giftAttachment.getMultiGiftRecieveInfo().getUid() + "";
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP) {
                        account = ((RoomTipAttachment) attachment).getUid() + "";
                    }
                }
                if (TextUtils.isEmpty(account)) return;
                final List<ButtonItem> buttonItems = new ArrayList<>();
                List<ButtonItem> items = ButtonItemFactory.createAllRoomPublicScreenButtonItems(mContext, account);
                if (items == null) return;
                buttonItems.addAll(items);
                ((BaseMvpActivity) mContext).getDialogManager().showCommonPopupDialog(buttonItems, "取消");
            }
        }
    }
}
