package com.tongdaxing.erban.common.ui.find.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.presenter.find.NotCustomLabelPresenter;
import com.tongdaxing.erban.common.ui.find.view.INotCustomLabelView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.find.bean.CreateRoomLabelInfo;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.List;

import butterknife.BindView;

/**
 * Function:非自定义标签
 * Author: Edward on 2019/3/18
 */
@CreatePresenter(NotCustomLabelPresenter.class)
public class NotCustomLabelFragment extends BaseMvpFragment<INotCustomLabelView, NotCustomLabelPresenter> implements INotCustomLabelView {
    public static String TAG = "NotCustomLabelFragment";
    protected List<CreateRoomLabelInfo.TagListBean> listBeans;
    protected int roomType = 0;
    protected int tagId;
    protected TagAdapter<CreateRoomLabelInfo.TagListBean> adapter;
    @BindView(R2.id.tfl_create_room_label)
    TagFlowLayout tflCreateRoomLabel;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_not_custom_label;
    }

    @Override
    public void delLabelSucceed(int tagId) {

    }

    protected TagAdapter<CreateRoomLabelInfo.TagListBean> getAdapter() {
        TagAdapter<CreateRoomLabelInfo.TagListBean> adapter = new TagAdapter<CreateRoomLabelInfo.TagListBean>(listBeans) {
            @Override
            public View getView(FlowLayout parent, int position, CreateRoomLabelInfo.TagListBean s) {
                CheckBox checkBox = (CheckBox) LayoutInflater.from(getActivity()).inflate(R.layout.flowlayout_create_room_not_custom_label, tflCreateRoomLabel, false);
                checkBox.setChecked(s.isSelected());
                checkBox.setText(s.getName());
                return checkBox;
            }
        };
        return adapter;
    }

    @Override
    public int getLabelId() {
        if (!ListUtils.isListEmpty(listBeans)) {
            for (int i = 0; i < listBeans.size(); i++) {
                if (listBeans.get(i).isSelected()) {
                    return listBeans.get(i).getId();
                }
            }
        }
        return -1;
    }

    @Override
    public void onFindViews() {
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        roomType = bundle.getInt("roomType");
        listBeans = bundle.getParcelableArrayList("data");

        adapter = getAdapter();
        tflCreateRoomLabel.setAdapter(adapter);
        tflCreateRoomLabel.setOnTagClickListener((view, position, parent) -> {
            setClickImpl(adapter, position);
            return false;
        });

        if (tagId == 0) {
            initDefaultSelected();
        } else {
            setDefaultSelected(tagId);
        }
    }

    public void initDefaultSelected() {
        if (!ListUtils.isListEmpty(listBeans)) {
            for (int i = 0; i < listBeans.size(); i++) {
                listBeans.get(i).setSelected(false);
            }
            listBeans.get(0).setSelected(true);//默认选中第一个
            adapter.notifyDataChanged();
        }
    }

    @Override
    public void setDefaultSelected(int tagId) {
        if (!ListUtils.isListEmpty(listBeans)) {
            for (int i = 0; i < listBeans.size(); i++) {
                listBeans.get(i).setSelected(listBeans.get(i).getId() == tagId);
            }
            adapter.notifyDataChanged();
        } else {
            this.tagId = tagId;
        }
    }

    protected void setClickImpl(TagAdapter<CreateRoomLabelInfo.TagListBean> adapter, int position) {
        for (int i = 0; i < listBeans.size(); i++) {//全部设置为未选中
            listBeans.get(i).setSelected(false);
        }
        listBeans.get(position).setSelected(true);//将当前item设置为选中状态
        adapter.notifyDataChanged();//刷新列表
//        CoreManager.notifyClients(ICreateRoomClient.class, ICreateRoomClient.SELECTED_LABEL_ID, listBeans.get(position).getId());
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if(isVisibleToUser) {
//
//        }
//    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {

    }
}
