package com.tongdaxing.erban.common.room.gift;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gcssloop.widget.PagerGridLayoutManager;
import com.gcssloop.widget.PagerGridSnapHelper;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.room.gift.adapter.GiftAdapter;
import com.tongdaxing.erban.common.room.gift.adapter.GiftAvatarAdapter;
import com.tongdaxing.erban.common.room.gift.widget.PageIndicatorView;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.erban.common.room.widget.dialog.CpRingDescriptionDialog;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.ui.widget.marqueeview.Utils;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftTypeConstant;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.ICongratulationDialogCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.zyyoona7.lib.EasyPopup;
import com.zyyoona7.lib.HorizontalGravity;
import com.zyyoona7.lib.VerticalGravity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenran
 * @date 2017/7/27
 * （如果每次都需要最新的数据，请每次new一个实例使用）
 */

public class GiftDialog extends BottomSheetDialog implements View.OnClickListener, GiftAdapter.OnItemClickListener,
        GiftAvatarAdapter.OnItemSelectedListener, PagerGridLayoutManager.PageListener, RadioGroup.OnCheckedChangeListener {
    public static final int ROWS = 2;
    public static final int COLUMNS = 4;
    public static final int POSITION_NORMAL = 0;//普通礼物
    public static final int POSITION_BACKPACK = 1;//背包礼物
    public static final int POSITION_BLESSING = 2;//福星礼物
    public static final int POSITION_LEVEL = 3;//等级和特权礼物
    public static final int POSITION_CP = 4;//cp礼物
    private static final String TAG = "giftdialog";
    private Context context;
    private RecyclerView gridView;
    private RecyclerView avatarList;
    private GiftAdapter adapter;
    private GiftInfo current;
    private OnGiftDialogBtnClickListener giftDialogBtnClickListener;
    private TextView goldText;
    private EasyPopup giftNumberEasyPopup;
    private TextView giftManText;
    private TextView giftNumberText;
    private int giftNumber = 1;
    private long uid;
    private List<MicMemberInfo> micMemberInfos;
    private MicMemberInfo defaultMicMemberInfo;
    private View giftNumLayout;
    private RelativeLayout giftAvatarListLayout;
    private PageIndicatorView giftIndicator;
    private TextView userInfoText, tvAttention;
    private ImageView ivHead;
    private RadioGroup rgIndex;
    private LinearLayout llGiftEmpty;
    private RelativeLayout rlGift;
    private List<GiftInfo> giftInfos;//普通礼物
    private List<GiftInfo> backpackInfos;//背包礼物
    private List<GiftInfo> blessingStarInfos;//福星礼物
    private List<GiftInfo> cpInfos;//cp礼物
    private SVGAImageView svgaImageView, svgaOne;
    private RelativeLayout rlAll;
    private LinearLayout llMemberContainer;
    private HorizontalScrollView hsvSelect;
    private RelativeLayout rlFixation;
    private TextView tvAllMic;
    private ImageView ivAllMicSelected;
    private ImageView ivQuestionHint;
    private RadioButton rbBlessingStarGiftTab;
    private View rbBlessingStarGiftTabInterval;
    private RadioButton rbLevelGiftTab;
    private int defaultMode = R.id.rb_gift_tab;
    private int currentP = POSITION_NORMAL;//0默认普通礼物，1是背包礼物，2是福星礼物，3是等级和特权礼物，4是cp礼物
    private List<GiftInfo> levelInfos;//等级和特权礼物
    private ImageView ivGroundGlass;
    private ImageView ivBlessingBeastDots;
    private RelativeLayout rlDialogGiftLayout;
    private boolean noDefaultMember = false;
    private boolean isAttention = false;

    /**
     * 设置礼物框默认页面入口
     *
     * @param context
     */
    public GiftDialog(Context context, SparseArray<RoomQueueInfo> mMicQueueMemberMap, int currentP) {
        this(context, mMicQueueMemberMap, null);
        this.currentP = currentP;
        switch (currentP) {
            case POSITION_NORMAL:
                defaultMode = R.id.rb_gift_tab;
                break;
            case POSITION_BACKPACK:
                defaultMode = R.id.rb_gift_pack_tab;
                break;
            case POSITION_BLESSING:
                defaultMode = R.id.rb_blessing_star_gift_tab;
                break;
            case POSITION_LEVEL:
                defaultMode = R.id.rb_level_gift_tab;
                break;
            case POSITION_CP:
                defaultMode = R.id.rb_cp_gift_tab;
                break;
        }
    }

    @Deprecated
    public GiftDialog(FragmentActivity context, MicMemberInfo memberInfo) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;
        this.defaultMicMemberInfo = memberInfo;
        List<MicMemberInfo> micMemberInfos = new ArrayList<>();
        micMemberInfos.add(memberInfo);
        this.micMemberInfos = micMemberInfos;
    }

    public GiftDialog(Context context, long uid, boolean isFullScreen) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;
        this.uid = uid;
    }


    public GiftDialog(Context context, long uid, int currentP) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;
        this.uid = uid;
        this.currentP = currentP;
        switch (currentP) {
            case POSITION_NORMAL:
                defaultMode = R.id.rb_gift_tab;
                break;
            case POSITION_BACKPACK:
                defaultMode = R.id.rb_gift_pack_tab;
                break;
            case POSITION_BLESSING:
                defaultMode = R.id.rb_blessing_star_gift_tab;
                break;
            case POSITION_LEVEL:
                defaultMode = R.id.rb_level_gift_tab;
                break;
            case POSITION_CP:
                defaultMode = R.id.rb_cp_gift_tab;
                break;
        }
    }

    @Deprecated
    public GiftDialog(Context context, long uid, String nick, String avatar) {
        this(context, uid);
    }

    public GiftDialog(Context context, long uid) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;
        this.uid = uid;
    }

    public GiftDialog(Context context, SparseArray<RoomQueueInfo> mMicQueueMemberMap, ChatRoomMember chatRoomMember) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;

        List<MicMemberInfo> micMemberInfos = new ArrayList<>();
        if (!checkHasOwner(mMicQueueMemberMap)) {//如果麦位列表没有房主数据，则通过其它方式获取房主数据
            UserInfo roomOwner = CoreManager.getCore(IAVRoomCore.class).getRoomOwner();
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            micMemberInfo.setRoomOwnner(true);
            micMemberInfo.setNick(roomOwner.getNick());
            micMemberInfo.setAvatar(roomOwner.getAvatar());
            micMemberInfo.setMicPosition(-1);
            micMemberInfo.setUid(roomOwner.getUid());
            micMemberInfos.add(micMemberInfo);
        }

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            ChatRoomMember mChatRoomMember = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i)).mChatRoomMember;
            if (mChatRoomMember == null) continue;
            // 合法判断
            String account = mChatRoomMember.getAccount();
            LogUtils.d("checkHasOwner", account + "   dd");
            if (TextUtils.isEmpty(account) ||
                    TextUtils.isEmpty(mChatRoomMember.getNick()) ||
                    TextUtils.isEmpty(mChatRoomMember.getAvatar())) continue;
            // 排除自己
            if (AvRoomDataManager.get().isSelf(account))
                continue;
            // 设置默认人员
            if (chatRoomMember != null && chatRoomMember.getAccount().equals(account)) {
                this.defaultMicMemberInfo = micMemberInfo;
            }
            // 设置房主
            if (AvRoomDataManager.get().isRoomOwner(account))
                micMemberInfo.setRoomOwnner(true);
            micMemberInfo.setNick(mChatRoomMember.getNick());
            micMemberInfo.setAvatar(mChatRoomMember.getAvatar());
            micMemberInfo.setMicPosition(mMicQueueMemberMap.keyAt(i));
            micMemberInfo.setUid(JavaUtil.str2long(account));
            micMemberInfos.add(micMemberInfo);
        }

        //当房主离开时，此时点击了房主的头像，通过其它渠道获取房主信息，并且将房主作为默认选项
        if (defaultMicMemberInfo == null && chatRoomMember != null && TextUtils.isEmpty(chatRoomMember.getAvatar())) {
            UserInfo roomOwner = CoreManager.getCore(IAVRoomCore.class).getRoomOwner();
            if (roomOwner != null) {
                MicMemberInfo micMemberInfo = new MicMemberInfo();
                micMemberInfo.setRoomOwnner(true);
                micMemberInfo.setNick(roomOwner.getNick());
                micMemberInfo.setAvatar(roomOwner.getAvatar());
                micMemberInfo.setMicPosition(-1);
                micMemberInfo.setUid(roomOwner.getUid());
                defaultMicMemberInfo = micMemberInfo;
            }
        }

        this.micMemberInfos = micMemberInfos;
    }

    public void setGiftDialogBtnClickListener(OnGiftDialogBtnClickListener giftDialogBtnClickListener) {
        this.giftDialogBtnClickListener = giftDialogBtnClickListener;
    }

    private boolean checkHasOwner(SparseArray<RoomQueueInfo> mMicQueueMemberMap) {
        UserInfo roomOwner = CoreManager.getCore(IAVRoomCore.class).getRoomOwner();

        if (roomOwner == null)
            return true;

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i));
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                if ((roomOwner.getUid() + "").equals(account))
                    return true;
            }
        }
        return false;
    }

    @CoreEvent(coreClientClass = ICongratulationDialogCoreClient.class)
    public void closeGiftDialogFromCongraDialog() {
        dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
        setCanceledOnTouchOutside(true);
        setContentView(R.layout.dialog_bottom_gift);
        init(findViewById(R.id.ll_dialog_bottom_gift));
//        setBlurBg();
        if (uid != 0) {
            CoreManager.getCore(IPraiseCore.class).isPraised(CoreManager.getCore(IAuthCore.class).getCurrentUid(), uid);
        } else if (defaultMicMemberInfo != null) {
            CoreManager.getCore(IPraiseCore.class).isPraised(CoreManager.getCore(IAuthCore.class).getCurrentUid(), defaultMicMemberInfo.getUid());
        }
        FrameLayout bottomSheet = findViewById(android.support.design.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(false);
            //dialog_gift_height字段设置对话框高度
            BottomSheetBehavior.from(bottomSheet).setPeekHeight(
                    (int) context.getResources().getDimension(R.dimen.dialog_gift_height) +
                            (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0));
        }
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display d = windowManager.getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
    }

    private void init(View root) {
        rlDialogGiftLayout = root.findViewById(R.id.rl_dialog_gift_layout);
        ivGroundGlass = root.findViewById(R.id.iv_ground_glass);
        ivBlessingBeastDots = root.findViewById(R.id.iv_blessing_beast_dots);
        rbBlessingStarGiftTab = root.findViewById(R.id.rb_blessing_star_gift_tab);
        rbBlessingStarGiftTabInterval = root.findViewById(R.id.rb_blessing_star_gift_tab_interval);
        rbLevelGiftTab = root.findViewById(R.id.rb_level_gift_tab);
        ivQuestionHint = root.findViewById(R.id.iv_question_hint);
        ivQuestionHint.setOnClickListener(this);
        llMemberContainer = root.findViewById(R.id.ll_member_container);
        hsvSelect = root.findViewById(R.id.hsv_select);
        rlFixation = root.findViewById(R.id.rl_fixation);
        tvAllMic = root.findViewById(R.id.tv_all_mic);
        ivAllMicSelected = root.findViewById(R.id.iv_all_mic_selected);
        svgaImageView = root.findViewById(R.id.svga_all);
        rlAll = root.findViewById(R.id.rl_all);
        rlAll.setOnClickListener(this);
        rlGift = findViewById(R.id.rl_gift_container);
        llGiftEmpty = findViewById(R.id.ll_gift_empty);
        rgIndex = root.findViewById(R.id.rg_gift_indicator);
        rgIndex.setOnCheckedChangeListener(this);
        rgIndex.check(defaultMode);
        root.findViewById(R.id.btn_recharge).setOnClickListener(this);
        root.findViewById(R.id.btn_send).setOnClickListener(this);
        root.findViewById(R.id.gift_number_layout).setOnClickListener(this);
        giftManText = root.findViewById(R.id.gift_man_text);
        giftAvatarListLayout = root.findViewById(R.id.gift_dialog_to_man_layout);
        giftAvatarListLayout.setOnClickListener(this);
        ivHead = root.findViewById(R.id.iv_head);
        UserInfo tempInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
        if (tempInfo != null) {
            ImageLoadUtils.loadCircleImage(getContext(), tempInfo.getAvatar(), ivHead, R.drawable.ic_no_avatar);
        }
        giftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_NORMAL);
        blessingStarInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_BLESSING_STAR);
        backpackInfos = CoreManager.getCore(IGiftCore.class).getBackpackGiftInfos();
        levelInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_LEVEL);
        levelInfos.addAll(CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_EXCLUSIVE));
        cpInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_CP);
        giftNumLayout = root.findViewById(R.id.gift_number_layout);
        gridView = root.findViewById(R.id.gridView);
        giftIndicator = root.findViewById(R.id.gift_layout_indicator);
        PagerGridSnapHelper pageSnapHelper = new PagerGridSnapHelper();
        pageSnapHelper.attachToRecyclerView(gridView);
        goldText = root.findViewById(R.id.text_gold);
        giftNumberText = root.findViewById(R.id.gift_number_text);

        View avatarListLayout = LayoutInflater.from(getContext()).inflate(R.layout.gift_avatar_list, null, false);
        avatarList = avatarListLayout.findViewById(R.id.avatar_list);
        userInfoText = root.findViewById(R.id.gift_dialog_info_text);
        userInfoText.setOnClickListener(this);
        tvAttention = root.findViewById(R.id.tv_attention);
        tvAttention.setOnClickListener(this);
        adapter = new GiftAdapter(getContext());
        gridView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
        switch (currentP) {
            case 0:
                setAdapterAndIndicator(giftInfos);
                break;
            case 1:
                setAdapterAndIndicator(backpackInfos);
                break;
            case 2:
                setAdapterAndIndicator(blessingStarInfos);
                break;
            case 3:
                setAdapterAndIndicator(levelInfos);
                break;
            case 4:
                ivQuestionHint.setVisibility(View.VISIBLE);
                setAdapterAndIndicator(cpInfos);
                break;
        }
        isShowLevelGiftTab();
        isShowBlessingStarGiftTab();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        CoreManager.getCore(IPayCore.class).getWalletInfo(uid);
        initEasyPop();

        if (this.uid > 0) {
            if (micMemberInfos == null) {
                micMemberInfos = new ArrayList<>();
            }
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(this.uid, true);
            if (userInfo == null)
                userInfo = new UserInfo();
            defaultMicMemberInfo = new MicMemberInfo();
            defaultMicMemberInfo.setAvatar(userInfo.getAvatar());
            defaultMicMemberInfo.setNick(userInfo.getNick());
            defaultMicMemberInfo.setUid(this.uid);
            micMemberInfos.add(defaultMicMemberInfo);
        }

        if (micMemberInfos != null && micMemberInfos.size() > 0) {
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            if (defaultMicMemberInfo != null) {
                hsvSelect.setVisibility(View.GONE);
                rlAll.setVisibility(View.GONE);
                rlFixation.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadCircleImage(context, defaultMicMemberInfo.getAvatar(), ivHead, R.drawable.ic_no_avatar);
                setSelectStatus();
                giftManText.setText(defaultMicMemberInfo.getNick());
                if (this.uid > 0) {
                    giftManText.setCompoundDrawablesRelative(null, null, null, null);
                    giftAvatarListLayout.setOnClickListener(null);
                    micMemberInfos.get(0).setSelect(true);
                }
            } else {
                hsvSelect.setVisibility(View.VISIBLE);
                rlAll.setVisibility(View.VISIBLE);
                rlFixation.setVisibility(View.GONE);

            }
        } else {
            userInfoText.setVisibility(View.GONE);
        }
        loadMicMemberData();
        initAllAnimation();
        updateGold();
    }

    /**
     * 是否显示等级礼物tab
     */
    private void isShowLevelGiftTab() {
        if (!ListUtils.isListEmpty(levelInfos)) {
            rbLevelGiftTab.setVisibility(View.VISIBLE);
        } else {//数据为空表示福星礼物
            rbLevelGiftTab.setVisibility(View.GONE);
        }
    }

    /**
     * 是否显示福星tab
     */
    private void isShowBlessingStarGiftTab() {
        if (!ListUtils.isListEmpty(blessingStarInfos)) {
            rbBlessingStarGiftTab.setVisibility(View.VISIBLE);
            rbBlessingStarGiftTabInterval.setVisibility(View.VISIBLE);
            //是否需要显示红点
            SharedPreferences sharedPreferences = context.getSharedPreferences(getDotsFileName(), Activity.MODE_PRIVATE);
            boolean isShowBlessingBeastDot = sharedPreferences.getBoolean(getDotsFileName(), false);//false则显示，true表示不显示
            ivBlessingBeastDots.setVisibility(!isShowBlessingBeastDot ? View.VISIBLE : View.GONE);
        } else {//数据为空表示福星礼物
            rbBlessingStarGiftTab.setVisibility(View.GONE);
            rbBlessingStarGiftTabInterval.setVisibility(View.GONE);
        }
    }

    private String getDotsFileName() {
        return "dots_" + CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }

    private void initAllAnimation() {
        svgaImageView.setClearsAfterStop(true);
        svgaImageView.setLoops(1);
        svgaImageView.setCallback(new SVGACallback() {
            @Override
            public void onPause() {
            }

            @Override
            public void onFinished() {
                svgaImageView.startAnimation();
            }

            @Override
            public void onRepeat() {
            }

            @Override
            public void onStep(int i, double v) {

            }
        });

        SVGAParser parser = new SVGAParser(getContext());
        parser.parse("gift_dialog_all.svga", new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity videoItem) {
                SVGADrawable drawable = new SVGADrawable(videoItem);
                svgaImageView.setImageDrawable(drawable);
                svgaImageView.startAnimation();
            }

            @Override
            public void onError() {
            }
        });
    }

    private void initMicUserHead(View view, MicMemberInfo micMemberInfo, int index) {
        view.setTag(micMemberInfo.getUid());//使用用户id作为标签
        ImageView svgaImageView = view.findViewById(R.id.svga_mic_head_bg);
        svgaImageView.setTag(false);
        ImageView ivMicHead = view.findViewById(R.id.iv_mic_head);
        ImageLoadUtils.loadCircleImage(context, micMemberInfo.getAvatar(), ivMicHead, R.drawable.ic_no_avatar);
        TextView tvTitle = view.findViewById(R.id.tv_title);
        if (AvRoomDataManager.get().isRoomOwner(micMemberInfo.getUid())) {
            tvTitle.setText("房主");
            tvTitle.setBackgroundResource(R.drawable.ic_gift_dialog_select);
            tvTitle.setTextColor(context.getResources().getColor(R.color.room_rank_tab_selected));
        } else {
            tvTitle.setText(index + "麦");
            tvTitle.setBackgroundResource(R.drawable.ic_gift_dialog_select);
            tvTitle.setTextColor(context.getResources().getColor(R.color.room_rank_tab_selected));
        }

        if (defaultMicMemberInfo != null && defaultMicMemberInfo.getUid() == micMemberInfo.getUid()) {//设置默认选中状态
            noDefaultMember = true;
            tvTitle.setBackgroundResource(R.drawable.ic_gift_dialog_owner_select);
            tvTitle.setTextColor(context.getResources().getColor(R.color.white));
            svgaImageView.setTag(true);
            svgaImageView.setVisibility(View.VISIBLE);
            micMemberInfo.setSelect(true);
        } else {
            svgaImageView.setVisibility(View.GONE);
        }

        view.setOnClickListener(v -> {
            if (onlyOne(view)) {//只有一个麦位且选中的状态下点击无效
                return;
            }

            if ((Boolean) svgaImageView.getTag()) {
                svgaImageView.setTag(false);
                micMemberInfo.setSelect(false);
                tvTitle.setBackgroundResource(R.drawable.ic_gift_dialog_select);
                tvTitle.setTextColor(context.getResources().getColor(R.color.room_rank_tab_selected));
                svgaImageView.setVisibility(View.GONE);
            } else {
                micMemberInfo.setSelect(true);
                svgaImageView.setTag(true);
                svgaImageView.setVisibility(View.VISIBLE);
                tvTitle.setBackgroundResource(R.drawable.ic_gift_dialog_owner_select);
                tvTitle.setTextColor(context.getResources().getColor(R.color.white));
            }
            isSelectedAllMic();
        });
    }

    /**
     * 是否全选了麦位
     */
    private void isSelectedAllMic() {
        if (llMemberContainer == null) {
            return;
        }
        int count = 0;
        for (int i = 0; i < llMemberContainer.getChildCount(); i++) {
            if (!(llMemberContainer.getChildAt(i) instanceof FrameLayout)) {
                continue;
            }
            FrameLayout frameLayout = (FrameLayout) llMemberContainer.getChildAt(i);
            if (!(frameLayout.getChildAt(0) instanceof RelativeLayout)) {
                continue;
            }
            RelativeLayout relativeLayout = (RelativeLayout) frameLayout.getChildAt(0);
            if (relativeLayout != null && relativeLayout.getChildCount() > 0) {
                if (!(relativeLayout.getChildAt(0) instanceof ImageView)) {
                    continue;
                }
                ImageView svgaImageView = (ImageView) relativeLayout.getChildAt(0);
                if (svgaImageView != null && (Boolean) svgaImageView.getTag()) {
                    count++;
                }
            }
        }

        if (count == llMemberContainer.getChildCount()) {//如果全部选中
            tvAllMic.setTag(true);
            tvAllMic.setBackgroundResource(R.drawable.room_gift_all_selected_bg);
            ivAllMicSelected.setVisibility(View.VISIBLE);
        } else {
            ivAllMicSelected.setVisibility(View.GONE);
            tvAllMic.setTag(false);
            tvAllMic.setBackgroundResource(R.drawable.room_gift_all_unselected_bg);
        }
    }

    private boolean onlyOne(View view) {
        int count = 0;
        for (int i = 0; i < llMemberContainer.getChildCount(); i++) {
            if (!(llMemberContainer.getChildAt(i) instanceof FrameLayout)) {
                continue;
            }
            FrameLayout frameLayout = (FrameLayout) llMemberContainer.getChildAt(i);
            if (!(frameLayout.getChildAt(0) instanceof RelativeLayout)) {
                continue;
            }
            RelativeLayout relativeLayout = (RelativeLayout) frameLayout.getChildAt(0);
            if (relativeLayout != null && relativeLayout.getChildCount() > 0) {
                if (!(relativeLayout.getChildAt(0) instanceof ImageView)) {
                    continue;
                }
                ImageView svgaImageView = (ImageView) relativeLayout.getChildAt(0);
                if (svgaImageView != null && (Boolean) svgaImageView.getTag()) {
                    count++;
                }
            }
        }

        return count == 1 && isSelected(view);
    }

    private boolean isSelected(View view) {
        if (!(view instanceof FrameLayout)) {
            return false;
        }
        FrameLayout frameLayout = (FrameLayout) view;
        if (!(frameLayout.getChildAt(0) instanceof RelativeLayout)) {
            return false;
        }
        RelativeLayout relativeLayout = (RelativeLayout) frameLayout.getChildAt(0);
        if (relativeLayout != null && relativeLayout.getChildCount() > 0) {
            if (!(relativeLayout.getChildAt(0) instanceof ImageView)) {
                return false;
            }
            ImageView svgaImageView = (ImageView) relativeLayout.getChildAt(0);
            if (svgaImageView != null && (Boolean) svgaImageView.getTag()) {
                return (Boolean) svgaImageView.getTag();
            }
        }
        return false;
    }

    private void setSvgaBg(SVGAImageView svgaMicHeadBg) {
        L.debug(TAG, "setSvgaBg");
        SvgaUtils.getInstance(getContext()).cyclePlayAssetsAnim(svgaMicHeadBg, "gift_dialog_selected.svga");
    }

    private void loadMicMemberData() {
        if (micMemberInfos == null || micMemberInfos.size() == 0) {
            hsvSelect.setVisibility(View.VISIBLE);
            rlAll.setVisibility(View.VISIBLE);
            rlFixation.setVisibility(View.GONE);
            return;
        }
        for (int i = 0; i < micMemberInfos.size(); i++) {
            MicMemberInfo micMemberInfo = micMemberInfos.get(i);
            if (micMemberInfo != null) {
                View view = getLayoutInflater().inflate(R.layout.layout_gitft_dialog_mic_user_head, null);
                initMicUserHead(view, micMemberInfo, micMemberInfo.getMicPosition() + 1);
//                micMemberInfos.set(i, micMemberInfo);//重新赋值
                llMemberContainer.addView(view);
            }
        }

        if (llMemberContainer != null && llMemberContainer.getChildCount() == 1) {//如果礼物框只有房主一个人，应该设置为全麦选中状态
            selectedAll();
            return;
        }
        defaultSelectedFirstPosition();
    }

    private void defaultSelectedFirstPosition() {//如果没有默认成员，则默认选中麦上列表的第一位
        if (!noDefaultMember) {
            if (llMemberContainer != null && llMemberContainer.getChildCount() > 0) {
                if (!(llMemberContainer.getChildAt(0) instanceof FrameLayout)) {
                    return;
                }
                FrameLayout frameLayout = (FrameLayout) llMemberContainer.getChildAt(0);
                if (!(frameLayout.getChildAt(0) instanceof RelativeLayout)) {
                    return;
                }
                RelativeLayout relativeLayout = (RelativeLayout) frameLayout.getChildAt(0);
                if (relativeLayout != null && relativeLayout.getChildCount() > 0) {
                    if (!(relativeLayout.getChildAt(0) instanceof ImageView)) {
                        return;
                    }
                    ImageView svgaImageView = (ImageView) relativeLayout.getChildAt(0);
                    if (svgaImageView != null) {
                        if (!(Boolean) svgaImageView.getTag()) {
                            svgaImageView.setTag(true);
                            svgaImageView.setVisibility(View.VISIBLE);
                            TextView tvTitle = (TextView) relativeLayout.getChildAt(2);
                            tvTitle.setBackgroundResource(R.drawable.ic_gift_dialog_owner_select);
                            tvTitle.setTextColor(context.getResources().getColor(R.color.white));

                            for (int i = 0; i < micMemberInfos.size(); i++) {
                                MicMemberInfo micMemberInfo = micMemberInfos.get(i);
                                if (micMemberInfo != null) {
                                    micMemberInfos.get(i).setSelect(true);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void setAdapterAndIndicator(List<GiftInfo> giftInfos) {
        if (giftInfos != null) {
            adapter.setGiftInfoList(giftInfos);
            adapter.notifyDataSetChanged();
            PagerGridLayoutManager pagerGridLayoutManager = new PagerGridLayoutManager(ROWS, COLUMNS, PagerGridLayoutManager.HORIZONTAL);
            pagerGridLayoutManager.setPageListener(this);
            gridView.setLayoutManager(pagerGridLayoutManager);
            giftIndicator.initIndicator((int) Math.ceil((float) giftInfos.size() / (ROWS * COLUMNS)));
            current = giftInfos.get(0);
        }
    }

    /**
     * 设置选择状态
     */
    private void setSelectStatus() {
        for (int i = 0; i < micMemberInfos.size(); i++) {
            if (micMemberInfos.get(i).getUid() == defaultMicMemberInfo.getUid()) {
                micMemberInfos.get(i).setSelect(true);
                break;
            }
        }
    }

    private void initEasyPop() {
        giftNumberEasyPopup = new EasyPopup(getContext())
                .setContentView(R.layout.dialog_gift_number)
                .setFocusAndOutsideEnable(true)
                .createPopup();
        giftNumberEasyPopup.getView(R.id.number_1).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_10).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_38).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_66).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_188).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_520).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_1314).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i1 = v.getId();//礼物说明
        if (i1 == R.id.rl_all) {
            if (tvAllMic.getTag() != null && (Boolean) tvAllMic.getTag()) {
                unselectAll();
            } else {
                selectedAll();
            }

        } else if (i1 == R.id.btn_recharge) {
            if (giftDialogBtnClickListener != null) {
                giftDialogBtnClickListener.onRechargeBtnClick();
            }
            dismiss();

        } else if (i1 == R.id.btn_send) {
            if (adapter == null)
                return;
            current = adapter.getIndexGift();
            if (current == null) {
                SingleToastUtil.showToast(getContext(), R.string.gift_should_choose_one);
                return;
            }
            if (current.getGiftType() == GiftTypeConstant.GIFT_TYPE_MYSTERY && current.getUserGiftPurseNum() < giftNumber) {//神秘礼物
                SingleToastUtil.showToast(getContext(), R.string.gift_mystery_gift_lack);
                return;
            }
            if (giftDialogBtnClickListener != null) {
                if (uid > 0) {
                    if (current.getGiftType() == GiftTypeConstant.GIFT_TYPE_CP) {
                        //送cp礼物
                        if (giftNumber == 1) {
                            if (current.isBackpackGift()) {
                                giftDialogBtnClickListener.onSendGiftBtnClick(current, uid, 1, GiftTypeConstant.GIFT_TYPE_CP);
                            } else {
                                giftDialogBtnClickListener.onSendCpGiftBtnClick(current, uid);
                            }
                        } else {
                            SingleToastUtil.showToast(getContext(), R.string.cp_gift_with_1_num);
                        }
                    } else {
                        if (giftNumber >= 1) {
                            giftDialogBtnClickListener.onSendGiftBtnClick(current, uid, giftNumber, currentP);
                        } else {
                            SingleToastUtil.showToast(getContext(), R.string.gift_num_0_error);
                        }
                    }
                } else if (llMemberContainer != null && llMemberContainer.getChildCount() > 0) {
                    if (hsvSelect != null && hsvSelect.getVisibility() == View.VISIBLE &&
                            tvAllMic != null && tvAllMic.getTag() != null && (Boolean) tvAllMic.getTag()) {
                        //勾选全麦
                        if (current.getGiftType() == GiftTypeConstant.GIFT_TYPE_CP) {
                            //送cp礼物
                            if (giftNumber != 1) {
                                SingleToastUtil.showToast(getContext(), R.string.cp_gift_with_1_num);
                                return;
                            }
                            if (micMemberInfos.size() == 1) {
                                if (current.isBackpackGift()) {
                                    giftDialogBtnClickListener.onSendGiftBtnClick(current, micMemberInfos.get(0).getUid(), 1, GiftTypeConstant.GIFT_TYPE_CP);
                                } else {
                                    giftDialogBtnClickListener.onSendCpGiftBtnClick(current, micMemberInfos.get(0).getUid());
                                }
                            } else {
                                SingleToastUtil.showToast(getContext(), R.string.cp_gift_with_single_user);
                            }
                        } else {
                            if (giftNumber >= 1) {
                                giftDialogBtnClickListener.onSendGiftBtnClick(current, micMemberInfos, giftNumber, currentP);
                            } else {
                                SingleToastUtil.showToast(getContext(), R.string.gift_num_0_error);
                            }
                        }
                    } else {
                        //未勾选全麦
                        if (current.getGiftType() == GiftTypeConstant.GIFT_TYPE_CP) {
                            if (giftNumber != 1) {
                                SingleToastUtil.showToast(getContext(), R.string.cp_gift_with_1_num);
                                return;
                            }
                            int selectNum = 0;
                            long targetUid = 0;
                            for (int i = 0; i < micMemberInfos.size(); i++) {
                                if (micMemberInfos.get(i).isSelect()) {
                                    selectNum++;
                                    targetUid = micMemberInfos.get(i).getUid();
                                }
                            }
                            if (selectNum == 1) {
                                if (current.isBackpackGift()) {
                                    giftDialogBtnClickListener.onSendGiftBtnClick(current, targetUid, 1, GiftTypeConstant.GIFT_TYPE_CP);
                                } else {
                                    giftDialogBtnClickListener.onSendCpGiftBtnClick(current, targetUid);
                                }
                            } else {
                                SingleToastUtil.showToast(getContext(), R.string.cp_gift_with_single_user);
                            }
                            return;
                        } else {
                            List<MicMemberInfo> tempMicMemberInfos = new ArrayList<>();
                            for (int i = 0; i < micMemberInfos.size(); i++) {
                                if (micMemberInfos.get(i).isSelect()) {
                                    tempMicMemberInfos.add(micMemberInfos.get(i));
                                }
                            }
                            if (tempMicMemberInfos.size() > 0) {
                                if (giftNumber >= 1) {
                                    giftDialogBtnClickListener.onSendGiftBtnClick(current, tempMicMemberInfos, giftNumber, currentP);
                                } else {
                                    SingleToastUtil.showToast(getContext(), R.string.gift_num_0_error);
                                }
                            } else {
                                SingleToastUtil.showToast(getContext(), R.string.gift_no_mic_member);
                                dismiss();
                            }
                        }
                    }
                } else {
                    SingleToastUtil.showToast(getContext(), R.string.gift_no_mic_member);
                    dismiss();
                }
            }

        } else if (i1 == R.id.number_1) {
            updateNumber(1);

        } else if (i1 == R.id.number_10) {
            updateNumber(10);

        } else if (i1 == R.id.number_38) {
            updateNumber(38);

        } else if (i1 == R.id.number_66) {
            updateNumber(66);

        } else if (i1 == R.id.number_188) {
            updateNumber(188);

        } else if (i1 == R.id.number_520) {
            updateNumber(520);

        } else if (i1 == R.id.number_1314) {
            updateNumber(1314);

        } else if (i1 == R.id.gift_number_layout) {
            showGiftNumberEasyPopup();

        } else if (i1 == R.id.gift_dialog_to_man_layout) {
            hsvSelect.setVisibility(View.VISIBLE);
            rlAll.setVisibility(View.VISIBLE);
            rlFixation.setVisibility(View.GONE);
            int length = compuationScrollPosition();
            scrollUserMic(length);//滚动到指定位置

        } else if (i1 == R.id.gift_dialog_info_text) {
            displayUserInfo();

        } else if (i1 == R.id.tv_attention) {
            if (uid != 0) {
                CoreManager.getCore(IPraiseCore.class).praise(uid);
            } else if (defaultMicMemberInfo != null) {
                CoreManager.getCore(IPraiseCore.class).praise(defaultMicMemberInfo.getUid());
            } else {
                SingleToastUtil.showToast("数据错误!");
            }

        } else if (i1 == R.id.iv_question_hint) {
            if (currentP == POSITION_BLESSING) {
                CommonWebViewActivity.start(getContext(), UriProvider.lookBlessingBeastRule());
            } else if (currentP == POSITION_CP) {
                //打开cp礼物说明弹窗
                CpRingDescriptionDialog cpRingDescriptionDialog = CpRingDescriptionDialog.instance();
                cpRingDescriptionDialog.show(((FragmentActivity) context).getSupportFragmentManager(), CpRingDescriptionDialog.TAG);
            }

        } else {
        }
    }

    private int compuationScrollPosition() {
        int count = 0;
        if (llMemberContainer != null && llMemberContainer.getChildCount() > 0) {//将UI全部设置为全选状态
            for (int i = 0; i < llMemberContainer.getChildCount(); i++) {
                if (!(llMemberContainer.getChildAt(i) instanceof FrameLayout)) {
                    continue;
                }
                FrameLayout frameLayout = (FrameLayout) llMemberContainer.getChildAt(i);
                if (!(frameLayout.getChildAt(0) instanceof RelativeLayout)) {
                    continue;
                }
                RelativeLayout relativeLayout = (RelativeLayout) frameLayout.getChildAt(0);
                if (relativeLayout != null && relativeLayout.getChildCount() > 0) {
                    if (!(relativeLayout.getChildAt(0) instanceof SVGAImageView)) {
                        continue;
                    }
                    SVGAImageView svgaImageView = (SVGAImageView) relativeLayout.getChildAt(0);
                    if (svgaImageView != null) {
                        if ((Boolean) svgaImageView.getTag()) {
                            return count;
                        } else {
                            count = count + ScreenUtil.dip2px(40);
                        }
                    }
                }
            }
        }
        return count;
    }

    private void scrollUserMic(int length) {
        hsvSelect.postDelayed(() -> {
            hsvSelect.smoothScrollTo(length, 0);
        }, 200);
    }

    private void updateNumber(int number) {
        if (adapter == null) {
            return;
        }
        current = adapter.getIndexGift();
        if (current == null) {
            return;
        }
        //在cp礼物列表选择数量时，超过1，则进行提示，只可以送给单个用户
        if (current.getGiftType() == GiftTypeConstant.GIFT_TYPE_CP) {
            if (number > 1) {
                SingleToastUtil.showToast(getContext(), R.string.cp_gift_with_1_num);
                number = 1;
            }
        }
        giftNumber = number;
        giftNumberText.setText(giftNumber + "");
        giftNumberEasyPopup.dismiss();
    }

    private void displayUserInfo() {
        long userId;
        if (uid > 0) {
            userId = uid;
        } else {
            if (defaultMicMemberInfo == null) {
                userId = micMemberInfos.get(0).getUid();
            } else {
                userId = defaultMicMemberInfo.getUid();
            }
        }
        UserInfoDialogManager.showDialogFragment(context, userId);
        dismiss();
    }

    private void showGiftNumberEasyPopup() {
        giftNumberEasyPopup.showAtAnchorView(giftNumLayout, VerticalGravity.ABOVE, HorizontalGravity.CENTER, 50, 0);
    }

    public void updateGold() {
        if (goldText != null) {
            WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
            if (walletInfo != null) {
                goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
            }
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(Boolean islike, long uid) {
        isAttention = islike;
        tvAttention.setVisibility(!isAttention ? View.VISIBLE : View.GONE);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLikedFail(String error) {
        tvAttention.setVisibility(View.GONE);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFail(String error) {
        SingleToastUtil.showToast("关注失败");
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long uid) {
        isAttention = !isAttention;
        tvAttention.setVisibility(!isAttention ? View.VISIBLE : View.GONE);
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onGetWalletInfo(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void refreshFreeGift() {
        if (adapter == null)
            return;
        if (currentP == POSITION_NORMAL) {//普通礼物
            giftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_NORMAL);
            adapter.setGiftInfoList(giftInfos);
        } else if (currentP == POSITION_BLESSING) {//福星礼物
            blessingStarInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_BLESSING_STAR);
            adapter.setGiftInfoList(blessingStarInfos);
        } else if (currentP == POSITION_LEVEL) {//等级礼物
            levelInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_LEVEL);
            levelInfos.addAll(CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_EXCLUSIVE));
            adapter.setGiftInfoList(levelInfos);
        } else if (currentP == POSITION_BACKPACK) {//更新背包礼物数量
            List<GiftInfo> gifts = CoreManager.getCore(IGiftCore.class).getBackpackGiftInfos();
            if (ListUtils.isListEmpty(gifts)) {
                if (llGiftEmpty.getVisibility() == View.GONE)
                    llGiftEmpty.setVisibility(View.VISIBLE);
                if (rlGift.getVisibility() == View.VISIBLE) {
                    rlGift.setVisibility(View.GONE);
                }
                adapter.setIndex(0);
            } else {
                if (backpackInfos != null && gifts.size() < backpackInfos.size()) {
                    adapter.setIndex(0);
                }
            }
            backpackInfos = gifts;
            adapter.setGiftInfoList(backpackInfos);
        } else if (currentP == POSITION_CP) {//cp礼物
            cpInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_CP);
            adapter.setGiftInfoList(cpInfos);
        }
        adapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendCpGiftSuccess() {
        LogUtil.d("GiftDialog onSendCpGiftSuccess ");
        SingleToastUtil.showToast(getContext(), R.string.cp_send_success);
        dismiss();
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftPastDue() {
        ((BaseActivity) getContext()).toast(R.string.gift_expire);
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendGiftFail(String message) {
        SingleToastUtil.showToast(message);
    }

    //10409 失败啦，你已经拥有CP
    //10412 失败啦，对方已有CP
    //10410 失败啦，你有正在处理的CP请求 需要重新发cp请求
    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendCpGiftFail(int errCode, String message, int giftId, long targetUid, int giftNum, long roomUid, ChargeListener chargeListener) {
        L.debug(TAG, "onSendCpGiftFail: code = %d, message = %s", errCode, message);
        if (errCode == 10410) {
            //弹窗提示
            DialogManager mDialogManager = new DialogManager(context);
            mDialogManager.setCanceledOnClickOutside(false);
            mDialogManager.showOkCancelDialog(getContext().getString(R.string.cp_fail_cp_requesting_resend),
                    getContext().getResources().getString(R.string.cp_fail_cp_resend),
                    getContext().getResources().getString(R.string.cancel),
                    new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onOk() {
                            L.debug(TAG, "onSendCpGiftFail: resend cp invite");
                            CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftId, targetUid,
                                    giftNum, roomUid, GiftTypeConstant.GIFT_TYPE_RESEND_CP, chargeListener);
                        }
                    });
        } else {
            SingleToastUtil.showToast(message);
            dismiss();
        }
    }

    //10409 失败啦，你已经拥有CP
    //10412 失败啦，对方已有CP
    //10410 失败啦，你有正在处理的CP请求 需要重新发cp请求
    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendBackpackCpGiftFail(int errCode, String message, int giftId, long targetUid) {
        L.debug(TAG, "onSendBackpackCpGiftFail: code = %d, message = %s", errCode, message);
        if (errCode == 10410) {
            //弹窗提示
            DialogManager mDialogManager = new DialogManager(context);
            mDialogManager.setCanceledOnClickOutside(false);
            mDialogManager.showOkCancelDialog(getContext().getString(R.string.cp_fail_cp_requesting_resend),
                    getContext().getResources().getString(R.string.cp_fail_cp_resend),
                    getContext().getResources().getString(R.string.cancel),
                    new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onOk() {
                            L.info(TAG, "onSendBackpackCpGiftFail: resend cp invite");
                            GiftInfo giftInfo = new GiftInfo();
                            giftInfo.setGiftId(giftId);
                            giftDialogBtnClickListener.onSendGiftBtnClick(giftInfo, targetUid, 1, GiftTypeConstant.GIFT_TYPE_RESEND_CP);
                        }
                    });
        } else {
            SingleToastUtil.showToast(message);
            dismiss();
        }
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftMysteryNotEnough() {
        SingleToastUtil.showToast(getContext(), R.string.gift_mystery_gift_lack);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo tempInfo) {
        if (tempInfo != null && this.uid > 0 && tempInfo.getUid() != CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid()) {
            this.uid = tempInfo.getUid();
            defaultMicMemberInfo.setAvatar(tempInfo.getAvatar());
            defaultMicMemberInfo.setNick(tempInfo.getNick());
            ImageLoadUtils.loadCircleImage(getContext(), tempInfo.getAvatar(), ivHead, R.drawable.ic_no_avatar);
            giftManText.setText(tempInfo.getNick());
        }
    }

    @Override
    public void onItemSelected(int position) {
//        if (avatarListEasyPopup != null)
//            avatarListEasyPopup.dismiss();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (adapter == null)
            return;
        adapter.setIndex(position);
        GiftInfo giftInfo = adapter.getIndexGift();
        if (giftInfo != null) {
            if (!TextUtils.isEmpty(giftInfo.getExt())) {
                SingleToastUtil.showToast(giftInfo.getExt());
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPageSizeChanged(int pageSize) {
        Log.d(TAG, "onPageSizeChanged() called with: pageSize = [" + pageSize + "]");
    }

    @Override
    public void onPageSelect(int pageIndex) {
        giftIndicator.setSelectedPage(pageIndex);
    }

    private void setStatus(long userId, boolean curStatus) {
        for (int i = 0; i < micMemberInfos.size(); i++) {//将数据全部设置全选状态
            if (micMemberInfos.get(i) != null && micMemberInfos.get(i).getUid() == userId) {
                micMemberInfos.get(i).setSelect(curStatus);
            }
        }
    }

    private void selectedAll() {
        ivAllMicSelected.setVisibility(View.VISIBLE);
        tvAllMic.setTag(true);
        tvAllMic.setBackgroundResource(R.drawable.room_gift_all_selected_bg);

        if (llMemberContainer != null && llMemberContainer.getChildCount() > 0) {//将UI全部设置为全选状态
            for (int i = 0; i < llMemberContainer.getChildCount(); i++) {
                if (!(llMemberContainer.getChildAt(i) instanceof FrameLayout)) {
                    continue;
                }
                FrameLayout frameLayout = (FrameLayout) llMemberContainer.getChildAt(i);
                if (!(frameLayout.getChildAt(0) instanceof RelativeLayout)) {
                    continue;
                }
                setStatus((long) frameLayout.getTag(), true);//将数据全部设置全选状态

                RelativeLayout relativeLayout = (RelativeLayout) frameLayout.getChildAt(0);
                if (relativeLayout != null && relativeLayout.getChildCount() > 0) {
                    if (!(relativeLayout.getChildAt(0) instanceof ImageView)) {
                        continue;
                    }
                    ImageView svgaImageView = (ImageView) relativeLayout.getChildAt(0);
                    if (svgaImageView != null) {
                        if (!(Boolean) svgaImageView.getTag()) {
                            svgaImageView.setTag(true);
                            svgaImageView.setVisibility(View.VISIBLE);
                            TextView tvTitle = (TextView) relativeLayout.getChildAt(2);
                            tvTitle.setBackgroundResource(R.drawable.ic_gift_dialog_owner_select);
                            tvTitle.setTextColor(context.getResources().getColor(R.color.white));
                        }
                    }
                }
            }
        }
    }

    private void unselectAll() {
        tvAllMic.setTag(false);
        ivAllMicSelected.setVisibility(View.GONE);
        tvAllMic.setBackgroundResource(R.drawable.room_gift_all_unselected_bg);

        if (llMemberContainer != null && llMemberContainer.getChildCount() > 0) {
            for (int i = 0; i < llMemberContainer.getChildCount(); i++) {
                if (!(llMemberContainer.getChildAt(i) instanceof FrameLayout)) {
                    continue;
                }
                FrameLayout frameLayout = (FrameLayout) llMemberContainer.getChildAt(i);
                if (i == 0) {//默认第1个人不取消选中状态
                    continue;
                }
                setStatus((long) frameLayout.getTag(), false);//第一位除外，将数据全部设置全选状态
                if (!(frameLayout.getChildAt(0) instanceof RelativeLayout)) {
                    continue;
                }
                RelativeLayout relativeLayout = (RelativeLayout) frameLayout.getChildAt(0);
                if (relativeLayout != null && relativeLayout.getChildCount() > 0) {
                    if (!(relativeLayout.getChildAt(0) instanceof ImageView)) {
                        continue;
                    }
                    ImageView svgaImageView = (ImageView) relativeLayout.getChildAt(0);
                    if (svgaImageView != null) {
                        svgaImageView.setTag(false);
                        svgaImageView.setVisibility(View.GONE);

                        TextView tvTitle = (TextView) relativeLayout.getChildAt(2);
                        tvTitle.setBackgroundResource(R.drawable.ic_gift_dialog_select);
                        tvTitle.setTextColor(context.getResources().getColor(R.color.room_rank_tab_selected));
                    }
                }
            }
        }
        scrollUserMic(0);//因为都是默认第一位，因此要重新滚回0位置
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (adapter == null)
            return;
        ivQuestionHint.setVisibility(View.GONE);
        //普通礼物
        //背包礼物
        //福星礼物
        //等级礼物
        //cp礼物
        if (checkedId == R.id.rb_gift_tab) {
            if (currentP == POSITION_NORMAL)
                return;
            currentP = POSITION_NORMAL;
            if (ListUtils.isListEmpty(giftInfos)) {
                showNoDataUI();
            } else {
                giftIndicator.initIndicator((int) Math.ceil((float) giftInfos.size() / (ROWS * COLUMNS)));
                showDataUI();
            }
            adapter.setGiftInfoList(giftInfos);

        } else if (checkedId == R.id.rb_gift_pack_tab) {
            if (currentP == POSITION_BACKPACK)
                return;
            currentP = POSITION_BACKPACK;
            if (ListUtils.isListEmpty(backpackInfos)) {
                showNoDataUI();
            } else {
                giftIndicator.initIndicator((int) Math.ceil((float) backpackInfos.size() / (ROWS * COLUMNS)));
                showDataUI();
            }
            adapter.setGiftInfoList(backpackInfos);

        } else if (checkedId == R.id.rb_blessing_star_gift_tab) {
            if (currentP == POSITION_BLESSING)
                return;
            currentP = POSITION_BLESSING;
            if (ivBlessingBeastDots.getVisibility() == View.VISIBLE) {
                ivBlessingBeastDots.setVisibility(View.GONE);
                SharedPreferences sharedPreferences = context.getSharedPreferences(getDotsFileName(), Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(getDotsFileName(), true);//设置为已读状态
                editor.apply();
            }
            ivQuestionHint.setVisibility(View.VISIBLE);//显示礼物说明入口
            if (ListUtils.isListEmpty(blessingStarInfos)) {
                showNoDataUI();
            } else {
                giftIndicator.initIndicator((int) Math.ceil((float) blessingStarInfos.size() / (ROWS * COLUMNS)));
                showDataUI();
            }
            adapter.setGiftInfoList(blessingStarInfos);

        } else if (checkedId == R.id.rb_level_gift_tab) {
            if (currentP == POSITION_LEVEL)
                return;
            currentP = POSITION_LEVEL;
            if (ListUtils.isListEmpty(levelInfos)) {//没有数据则显示无数据UI
                showNoDataUI();
            } else {
                giftIndicator.initIndicator((int) Math.ceil((float) levelInfos.size() / (ROWS * COLUMNS)));
                showDataUI();
            }
            adapter.setGiftInfoList(levelInfos);

        } else if (checkedId == R.id.rb_cp_gift_tab) {
            if (currentP == POSITION_CP)
                return;
            currentP = POSITION_CP;
            ivQuestionHint.setVisibility(View.VISIBLE);//显示礼物说明入口
            if (ListUtils.isListEmpty(cpInfos)) {
                showNoDataUI();
            } else {
                giftIndicator.initIndicator((int) Math.ceil((float) cpInfos.size() / (ROWS * COLUMNS)));
                showDataUI();
            }
            adapter.setGiftInfoList(cpInfos);

        } else {
        }
        adapter.setIndex(0);
        if (adapter.getGiftInfoList() != null && adapter.getGiftInfoList().size() > 0) {
            current = adapter.getGiftInfoList().get(0);
        }
        adapter.notifyDataSetChanged();
    }

    private void showDataUI() {
        if (rlGift.getVisibility() == View.GONE) {
            rlGift.setVisibility(View.VISIBLE);
        }
        if (llGiftEmpty.getVisibility() == View.VISIBLE) {
            llGiftEmpty.setVisibility(View.GONE);
        }
    }

    private void showNoDataUI() {
        if (llGiftEmpty.getVisibility() == View.GONE) {
            llGiftEmpty.setVisibility(View.VISIBLE);
        }
        if (rlGift.getVisibility() == View.VISIBLE) {
            rlGift.setVisibility(View.GONE);
        }
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnGiftDialogBtnClickListener {
        void onRechargeBtnClick();

        void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number, int sendGiftType);

        void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number, int sendGiftType);

        void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid);
    }
}
