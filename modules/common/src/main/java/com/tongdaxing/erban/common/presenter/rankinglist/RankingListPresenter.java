package com.tongdaxing.erban.common.presenter.rankinglist;

import com.tongdaxing.erban.common.model.RankingListModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.bean.RankingInfo;

public class RankingListPresenter extends AbstractMvpPresenter<IRankingListView> {
    private RankingListModel dataSource;

    public RankingListPresenter() {
        dataSource = new RankingListModel();
    }

    /**
     * 刷新数据
     *
     * @param type     排行榜类型 0巨星榜，1贵族榜，2房间榜
     * @param dateType 榜单周期类型 0日榜，1周榜，2总榜
     */
    public void refreshData(int type, int dateType) {
        getData(type, dateType);
    }

    private void getData(int type, int dateType) {
        dataSource.getRankingList(type, dateType, new OkHttpManager.MyCallBack<RankingInfo>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(e.getMessage());
                }
            }

            @Override
            public void onResponse(RankingInfo response) {
                if (getMvpView() != null) {
                    getMvpView().setupSuccessView(response);
                }
            }
        });
    }
}
