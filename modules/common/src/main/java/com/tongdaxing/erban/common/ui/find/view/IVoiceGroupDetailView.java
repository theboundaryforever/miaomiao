package com.tongdaxing.erban.common.ui.find.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupDetailInfo;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public interface IVoiceGroupDetailView extends IMvpBaseView {

    void insertHead(int page, VoiceGroupDetailInfo voiceGroupDetailInfo);

    void refreshHeadAdapterLikeStatus(int commentId);

    void refreshAdapterLikeStatus(int id);

    void setupSuccessView(List<VoiceGroupDetailInfo.PlayerListBean> voiceGroupInfoList, boolean isRefresh);

    void publishCommentSucceed();

    void publishCommentFailure(String errorStr);

    void setupFailView(boolean isRefresh);

    void deleteVoiceGroupSuccess();
}
