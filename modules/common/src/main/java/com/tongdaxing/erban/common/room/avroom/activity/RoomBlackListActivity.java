package com.tongdaxing.erban.common.room.avroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.room.avroom.adapter.RoomBlackAdapter;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.room.IIMRoomCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.RoomBlackPresenter;
import com.tongdaxing.xchat_core.room.view.IRoomBlackView;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 * 黑名单
 *
 * @author chenran
 * @date 2017/10/11
 */
@CreatePresenter(RoomBlackPresenter.class)
public class RoomBlackListActivity extends BaseMvpActivity<IRoomBlackView, RoomBlackPresenter>
        implements IRoomBlackView, RoomBlackAdapter.RoomBlackDelete {
    private TextView count;
    private RecyclerView recyclerView;
    private RoomBlackAdapter normalListAdapter;
    private long lastUserUpdateTime = 0;
    private int loadSize = 100;


    public static void start(Context context) {
        Intent intent = new Intent(context, RoomBlackListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_black_list);
        initTitleBar("黑名单");
        initView();
        showLoading();
        loadData();
    }

    private void loadData() {
        //一次最多只能加载200条
        getMvpPresenter().queryNormalListFromIm(loadSize, lastUserUpdateTime);
    }

    private void initView() {
        count = (TextView) findViewById(R.id.count);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        normalListAdapter = new RoomBlackAdapter(R.layout.list_item_room_black);
        normalListAdapter.setRoomBlackDelete(this);
        normalListAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                loadData();
            }
        });
        // normalListAdapter.setListOperationClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(normalListAdapter);
    }

    @Override
    public void onDeleteClick(final ChatRoomMember chatRoomMember) {


        getDialogManager().showOkCancelDialog(
                "是否将" + chatRoomMember.getNick() + "移除黑名单列表？",
                true,
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                        if (roomInfo != null) {
                            getMvpPresenter().markBlackList(roomInfo.getRoomId(), chatRoomMember.getAccount(), false);
                        }
                    }
                });
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                loadData();
            }
        };
    }

    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void onMemberBeRemoveManager(String account) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (uid == JavaUtil.str2long(account)) {
            finish();
            toast(R.string.remove_room_manager);
        }
    }

    @Override
    public void queryNormalListSuccess(List<ChatRoomMember> chatRoomMemberList) {
        hideStatus();
        if (chatRoomMemberList != null) {
            normalListAdapter.loadMoreComplete();
            if (chatRoomMemberList.size() < loadSize) {
                normalListAdapter.loadMoreEnd();
            }

        }
        List<ChatRoomMember> blackList = getBlackList(chatRoomMemberList);
        if (blackList != null && blackList.size() > 0) {
            if (normalListAdapter.getData() != null && normalListAdapter.getData().size() > 0) {
                normalListAdapter.addData(blackList);
                count.setText("黑名单" + normalListAdapter.getData().size() + "人");
            } else {
                normalListAdapter.setNewData(blackList);
                count.setText("黑名单" + blackList.size() + "人");
            }

        } else if (normalListAdapter.getData() == null || normalListAdapter.getData().size() < 1) {
            showNoData("暂没有设置黑名单");
            count.setText("黑名单0人");
        }
    }

    private List<ChatRoomMember> getBlackList(List<ChatRoomMember> chatRoomMemberList) {
        List<ChatRoomMember> blackList = null;
        if (!ListUtils.isListEmpty(chatRoomMemberList)) {
            blackList = new ArrayList<>();
            for (ChatRoomMember chatRoomMember : chatRoomMemberList) {
                if (chatRoomMember.isInBlackList()) {
                    blackList.add(chatRoomMember);
                }
            }
            //根据最后的时间戳分页加载
            lastUserUpdateTime = chatRoomMemberList.get(chatRoomMemberList.size() - 1).getUpdateTime();

        }
        return blackList;
    }

    @Override
    public void queryNormalListFail() {
        showNetworkErr();
    }

    @Override
    public void makeBlackListSuccess(ChatRoomMember chatRoomMember, boolean mark) {
        if (chatRoomMember == null) return;
        List<ChatRoomMember> normalList = normalListAdapter.getData();
        if (!ListUtils.isListEmpty(normalList)) {
            hideStatus();
            ListIterator<ChatRoomMember> iterator = normalList.listIterator();
            for (; iterator.hasNext(); ) {
                if (Objects.equals(iterator.next().getAccount(), chatRoomMember.getAccount())) {
                    iterator.remove();
                }
            }
            normalListAdapter.notifyDataSetChanged();
            count.setText("黑名单" + normalList.size() + "人");
            if (normalList.size() == 0) {
                showNoData("暂没有设置黑名单");
            }
        } else {
            showNoData("暂没有设置黑名单");
            count.setText("黑名单0人");
        }
        toast("操作成功");

    }

    @Override
    public void makeBlackListFail(int code, String error, boolean mark) {
//        toast("操作失败，请重试");
    }
}
