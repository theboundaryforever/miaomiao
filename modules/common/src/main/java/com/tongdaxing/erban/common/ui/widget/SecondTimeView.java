package com.tongdaxing.erban.common.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.tongdaxing.erban.common.utils.WeakHandler;

/**
 * 描述
 */
public class SecondTimeView extends AppCompatTextView {

    private WeakHandler handler;
    private int countType;//计时类型 0 倒计时，1 顺计时
    private int secondTime;//秒数
    private int currSecondTime;//当前秒数

    private OnSecondTimeListener onSecondTimeListener;

    public SecondTimeView(Context context) {
        super(context);
        init();
    }

    public SecondTimeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SecondTimeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setCurrCount(0);
        handler = new WeakHandler(message -> {
            int currSecondTime = message.what;
            setCurrCount(currSecondTime);
            if ((countType == 1 && currSecondTime < secondTime) || (countType == 0 && currSecondTime > 0)) {//还没计完
                handler.sendEmptyMessageDelayed(countType == 1 ? currSecondTime + 1 : currSecondTime - 1, 1000);
            } else {
                if (onSecondTimeListener != null) {
                    onSecondTimeListener.onCompletion();
                }
            }
            return false;
        });
    }

    /**
     * 开启计时
     *
     * @param secondTime 计时秒数
     * @param countType  //计时类型 0 倒计时，1 顺计时
     */
    public void startCount(int secondTime, int countType) {
        if (secondTime < 0) {
            return;
        }
        this.secondTime = secondTime;
        this.countType = countType;
        handler.sendEmptyMessage(countType == 1 ? 0 : secondTime);
    }

    /**
     * 结束计时
     */
    public void stopCount() {
        handler.removeCallbacksAndMessages(null);
    }

    /**
     * 重置计时
     */
    public void resetCount() {
        handler.removeCallbacksAndMessages(null);
        this.secondTime = 0;
        this.countType = 0;
        setCurrCount(0);
    }

    /**
     * 设置当前秒数
     */
    public void setCurrCount(int currSecondTime) {
        this.currSecondTime = currSecondTime;
        setText(String.valueOf(currSecondTime).concat("s"));
    }

    /**
     * 获取当前计时秒数
     */
    public int getCurrSecondTime() {
        return currSecondTime;
    }

    public void setOnSecondTimeListener(OnSecondTimeListener onSecondTimeListener) {
        this.onSecondTimeListener = onSecondTimeListener;
    }

    public interface OnSecondTimeListener {
        void onCompletion();
    }
}
