package com.tongdaxing.erban.common.im.actions;

import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.tongdaxing.erban.common.room.gift.GiftDialog;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.room.gift.GiftDialog;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenran on 2017/10/2.
 */

public class GiftAction extends BaseAction implements GiftDialog.OnGiftDialogBtnClickListener, ChargeListener {
    public static final String TAG = "CPGiftTag";
    transient private GiftDialog giftDialog;
    private boolean isShowingChargeDialog;

    public GiftAction() {
        super(R.drawable.icon_gift_action, R.string.gift_action);
    }

    @Override
    public void onClick() {
        isShowingChargeDialog = false;
        if (giftDialog == null) {
            giftDialog = new GiftDialog(getActivity(), Long.valueOf(getAccount()), false);
            giftDialog.setGiftDialogBtnClickListener(this);
            giftDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    giftDialog = null;
                }
            });
        }
        if (!giftDialog.isShowing()) {
            if (giftDialog != null) {
                giftDialog.show();
            }

        }
    }

    @Override
    public void onRechargeBtnClick() {
        MyWalletNewActivity.start(getActivity());
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number, int sendGiftType) {
        if (giftInfo != null) {
            List<Long> uids = new ArrayList<>();
            uids.add(uid);
            CoreManager.getCore(IGiftCore.class).doSendPersonalGiftToNIMNew(giftInfo.getGiftId(), uids,
                    number, new WeakReference<>(getContainer()), this, sendGiftType);
        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number, int sendGiftType) {
    }

    @Override
    public void onNeedCharge() {
        if (isShowingChargeDialog) {
            return;
        }
        isShowingChargeDialog = true;
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    MyWalletNewActivity.start(getActivity());
                    fragment.dismiss();
                }
                isShowingChargeDialog = false;
            }
        }).show(((FragmentActivity) getActivity()).getSupportFragmentManager(), "charge");
    }

    /**
     * 赠送按钮点击
     * CP判断
     *
     * @param giftInfo
     * @param uid
     */
    @Override
    public void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid) {
        if (giftInfo != null) {
            L.debug(GiftAction.TAG, "GiftAction onSendCpGiftBtnClick giftInfo=%s, uid=%d", giftInfo, uid);
            CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftInfo.getGiftId(), uid,
                    1, -1, this);
        }
    }
}