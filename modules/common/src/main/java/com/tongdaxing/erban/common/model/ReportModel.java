package com.tongdaxing.erban.common.model;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * 举报model
 */
public class ReportModel extends BaseMvpModel {

    public void reportCommit(int type, int reportType, HttpRequestCallBack<Object> callBack) {
        UserInfo cacheUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("reportType", reportType + "");
        requestParam.put("type", type + "");
        requestParam.put("phoneNo", cacheUserInfo == null ? "" : cacheUserInfo.getPhone());
        requestParam.put("reportUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("uid", AvRoomDataManager.get().mCurrentRoomInfo == null ? "0" : (AvRoomDataManager.get().mCurrentRoomInfo.getUid() + ""));
        OkHttpManager.getInstance().doGetRequest(UriProvider.reportUserUrl(), requestParam, callBack);
    }
}
