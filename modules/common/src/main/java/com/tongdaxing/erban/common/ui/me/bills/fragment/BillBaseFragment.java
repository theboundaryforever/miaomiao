package com.tongdaxing.erban.common.ui.me.bills.fragment;

import android.animation.ObjectAnimator;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzxiang.pickerview.TimePickerDialog;
import com.jzxiang.pickerview.data.Type;
import com.jzxiang.pickerview.listener.OnDateSetListener;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseLazyFragment;
import com.tongdaxing.erban.common.ui.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.PowerGroupListener;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.PowerfulStickyDecoration;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 新皮：账单模块所有单个页面基类
 *
 * @author zwk 2018/5/31
 */
public abstract class BillBaseFragment extends BaseLazyFragment implements OnDateSetListener {
    protected static final int PAGE_SIZE = Constants.BILL_PAGE_SIZE;
    public TextView tvTime;
    protected RecyclerView mRecyclerView;
    protected SwipeRefreshLayout mRefreshLayout;
    protected int mCurrentCounter = Constants.PAGE_START;//当前页
    protected TimePickerDialog.Builder mDialogYearMonthDayBuild;
    protected long time = System.currentTimeMillis();
    protected List<BillItemEntity> mBillItemEntityList = new ArrayList<>();
    private ImageView ivGoTop, ivDate;
    private boolean isShow;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_bill;
    }

    @Override
    public void onFindViews() {
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.recyclerView);
        mRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipe_refresh);
        ivGoTop = mView.findViewById(R.id.iv_go_today);
        tvTime = (TextView) mView.findViewById(R.id.tv_time);
        ivDate = mView.findViewById(R.id.iv_date);
    }

    @Override
    public void onSetListener() {
        tvTime.setOnClickListener(this);
        ivGoTop.setOnClickListener(this);
        ivDate.setOnClickListener(this);
        mView.findViewById(R.id.iv_go_today).setOnClickListener(this);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentCounter = Constants.PAGE_START;
//                time = System.currentTimeMillis();
                loadData();
            }
        });
        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && !isShow) {
                    isShow = true;
                    ObjectAnimator.ofFloat(ivGoTop, "translationY", 0, 300).setDuration(200).start();
                } else if (dy < 0 && isShow) {
                    isShow = false;
                    ObjectAnimator.ofFloat(ivGoTop, "translationY", 300, 0).setDuration(200).start();
                }
            }
        });
    }

    @Override
    public void initiate() {
        setDate(System.currentTimeMillis());
        mDialogYearMonthDayBuild = new TimePickerDialog.Builder()
                .setType(Type.YEAR_MONTH_DAY)
                .setTitleStringId("日期选择")
                .setThemeColor(getResources().getColor(R.color.line_background))
                .setWheelItemTextNormalColor(getResources().getColor(R.color
                        .timetimepicker_default_text_color))
                .setWheelItemTextSelectorColor(getResources().getColor(R.color.black))
                .setCallBack(this);

        PowerfulStickyDecoration decoration = PowerfulStickyDecoration.Builder
                .init(new PowerGroupListener() {
                    @Override
                    public String getGroupName(int position) {
                        //获取组名，用于判断是否是同一组
                        if (mBillItemEntityList.size() > position && position >= 0) {
                            return mBillItemEntityList.get(position).time;
                        }
                        return null;
                    }

                    @Override
                    public View getGroupView(int position) {
                        //获取自定定义的组View
                        if (mBillItemEntityList.size() > position && position >= 0) {
                            View view = getLayoutInflater().inflate(R.layout.item_group, null, false);
                            TextView text = (TextView) view.findViewById(R.id.tv);
                            text.setText(TimeUtils.getDateTimeString(JavaUtil.str2long(mBillItemEntityList.get(position).time), "yyyy-MM-dd"));
                            return view;
                        } else {
                            return null;
                        }
                    }
                })
                .setGroupHeight(UIUtil.dip2px(mContext, 44))       //设置高度
                .isAlignLeft(true)                                 //靠右边显示   默认左边
                .setGroupBackground(getResources().getColor(R.color.colorPrimaryDark))    //设置背景   默认透明
                .build();
        mRecyclerView.addItemDecoration(decoration);
        RecyclerViewNoBugLinearLayoutManager manager = new RecyclerViewNoBugLinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(manager);
    }

    protected void setDate(long time) {
        tvTime.setText(new SimpleDateFormat("yyyy-MM-dd").format(new Date(time)));
    }

    private void firstLoadDate() {
        mCurrentCounter = Constants.PAGE_START;
        showLoading();
//        onLoadGoldNum();
        loadData();
    }

    protected abstract void loadData();

    @Override
    protected void onLazyLoadData() {
        firstLoadDate();
    }

    @Override
    public void onDateSet(TimePickerDialog timePickerView, long millseconds) {
        this.time = millseconds;
        setDate(millseconds);
        mCurrentCounter = Constants.PAGE_START;
        showLoading();
        loadData();
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentCounter = Constants.PAGE_START;
                showLoading();
                loadData();
            }
        };
    }

    @Override
    public void showNetworkErr() {
        mRefreshLayout.setRefreshing(false);
        super.showNetworkErr();

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.iv_go_today) {
            mCurrentCounter = Constants.PAGE_START;
            time = System.currentTimeMillis();
            setDate(time);
            showLoading();
            loadData();

        } else if (i == R.id.iv_date) {
            mDialogYearMonthDayBuild.build().show(getFragmentManager(), "year_month_day");

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDialogYearMonthDayBuild != null) {
            mDialogYearMonthDayBuild.setCallBack(null);
            mDialogYearMonthDayBuild = null;
        }
    }
}
