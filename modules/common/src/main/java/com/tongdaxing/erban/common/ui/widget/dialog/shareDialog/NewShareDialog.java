package com.tongdaxing.erban.common.ui.widget.dialog.shareDialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.room.avroom.activity.ShareFansActivity;
import com.tongdaxing.erban.common.ui.find.model.CreateRoomModel;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.GridRightSpaceItemDecoration;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.uuzuche.lib_zxing.DisplayUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * @author xiaoyu
 * @date 2017/12/13
 */

public class NewShareDialog extends BottomSheetDialog {
    private static final String TAG = "ShareDialog";
    @BindView(R2.id.share_dialog_rcv)
    RecyclerView shareDialogRcv;
    private Context context;
    private OnShareDialogItemClick onShareDialogItemClick;
    private CreateRoomModel createRoomModel;

    private boolean hasFriend = false;//是否有邀请好友 默认无
    private boolean hasBroadCast = false;//是否有寻友广播 默认无
    private boolean hasNoticeFans = false;//是否有一键通知粉丝 默认无

    private NewShareDialogCountDownTimer countDownTimer;
    private boolean isClickNoticeFans;

    private boolean isLinearLayoutManager = false;
    private ShareBean noticeFansShareBean;
    private NewShareDialogRcvAdapter adapter;

    public NewShareDialog(Context context) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
    }

    public NewShareDialog(Context context, boolean isLinearLayoutManager) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
        this.isLinearLayoutManager = isLinearLayoutManager;
    }

    public void setOnShareDialogItemClick(OnShareDialogItemClick onShareDialogItemClick) {
        this.onShareDialogItemClick = onShareDialogItemClick;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreUtils.register(this);
        setContentView(R.layout.share_dialog);
        ButterKnife.bind(this);
        setCanceledOnTouchOutside(true);
        FrameLayout bottomSheet = (FrameLayout) findViewById(android.support.design.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(false);

//            BottomSheetBehavior.from(bottomSheet).setPeekHeight((int) context.getResources().getDimension(R.dimen.dialog_share_new_height));
        }
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

        createRoomModel = new CreateRoomModel();

        initRcv();
    }

    private void initRcv() {
        shareDialogRcv.setLayoutManager(isLinearLayoutManager ?
                new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false) :
                new GridLayoutManager(context, 4));
        shareDialogRcv.setItemAnimator(null);
        ArrayList<ShareBean> shareBeans = new ArrayList<>();
        if (hasNoticeFans) {
            noticeFansShareBean = new ShareBean(ShareBean.TYPE_NOTICE_FANS, R.string.share_notice_fans, R.drawable.share_dialog_notice_fans,
                    true, R.drawable.share_dialog_tag_new);
            shareBeans.add(noticeFansShareBean);
        }
        if (hasBroadCast) {
            shareBeans.add(new ShareBean(ShareBean.TYPE_BROADCAST, R.string.share_broadcast, R.drawable.share_dialog_broadcast, true, R.drawable.share_dialog_tag_hot));
        }
        if (hasFriend) {
            shareBeans.add(new ShareBean(ShareBean.TYPE_FRIEND, R.string.share_friend, R.drawable.share_dialog_friend));
        }
        shareBeans.add(new ShareBean(ShareBean.TYPE_WECHAT, R.string.share_wechat, R.drawable.share_dialog_wechat));
        shareBeans.add(new ShareBean(ShareBean.TYPE_WECHAT_MOMENTS, R.string.share_wechat_moments, R.drawable.share_dialog_wechat_moments));
        shareBeans.add(new ShareBean(ShareBean.TYPE_QQ, R.string.share_qq, R.drawable.share_dialog_qq));
        shareBeans.add(new ShareBean(ShareBean.TYPE_QZONE, R.string.share_qzone, R.drawable.share_dialog_qzone));
        if (isLinearLayoutManager) {
            GridRightSpaceItemDecoration decoration = new GridRightSpaceItemDecoration(DisplayUtil.dip2px(context, 15), shareBeans.size());
            shareDialogRcv.addItemDecoration(decoration, 0);
        }
        adapter = new NewShareDialogRcvAdapter(isLinearLayoutManager, shareBeans);
        shareDialogRcv.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                switch (shareBeans.get(position).getType()) {
                    case ShareBean.TYPE_NOTICE_FANS:
                        UmengEventUtil.getInstance().onCommonTypeEvent(context, UmengEventId.getRoomShare(), ShareBean.UMENG_TYPE_NOTICE_FANS);
                        isClickNoticeFans = true;
                        getRoomNoticeFansCd();
                        break;
                    case ShareBean.TYPE_BROADCAST:
                        UmengEventUtil.getInstance().onCommonTypeEvent(context, UmengEventId.getRoomShare(), ShareBean.UMENG_TYPE_BROADCAST);
                        if (onShareDialogItemClick != null) {
                            onShareDialogItemClick.onSendBroadcastMsg();
                        }
                        dismiss();
                        break;
                    case ShareBean.TYPE_WECHAT:
                        UmengEventUtil.getInstance().onCommonTypeEvent(context, UmengEventId.getRoomShare(), ShareBean.UMENG_TYPE_WECHAT);
                        if (onShareDialogItemClick != null) {
                            onShareDialogItemClick.onSharePlatformClick(ShareSDK.getPlatform(Wechat.NAME));
                        }
                        dismiss();
                        break;

                    case ShareBean.TYPE_WECHAT_MOMENTS:
                        UmengEventUtil.getInstance().onCommonTypeEvent(context, UmengEventId.getRoomShare(), ShareBean.UMENG_TYPE_WECHAT_MOMENTS);
                        if (onShareDialogItemClick != null) {
                            onShareDialogItemClick.onSharePlatformClick(ShareSDK.getPlatform(WechatMoments.NAME));
                        }
                        dismiss();
                        break;
                    case ShareBean.TYPE_QQ:
                        UmengEventUtil.getInstance().onCommonTypeEvent(context, UmengEventId.getRoomShare(), ShareBean.UMENG_TYPE_QQ);
                        if (onShareDialogItemClick != null) {
                            onShareDialogItemClick.onSharePlatformClick(ShareSDK.getPlatform(QQ.NAME));
                        }
                        dismiss();
                        break;
                    case ShareBean.TYPE_QZONE:
                        UmengEventUtil.getInstance().onCommonTypeEvent(context, UmengEventId.getRoomShare(), ShareBean.UMENG_TYPE_QZONE);
                        if (onShareDialogItemClick != null) {
                            onShareDialogItemClick.onSharePlatformClick(ShareSDK.getPlatform(QZone.NAME));
                        }
                        dismiss();
                        break;
                    case ShareBean.TYPE_FRIEND:
                        //邀请好友
                        UmengEventUtil.getInstance().onCommonTypeEvent(context, UmengEventId.getRoomShare(), ShareBean.UMENG_TYPE_FRIEND);
                        if (context == null)
                            return;
                        context.startActivity(new Intent(context, ShareFansActivity.class));
                        dismiss();
                        break;
                    default:
                        break;
                }
            }
        });
        if (hasNoticeFans) {
            getRoomNoticeFansCd();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCountDownEvent(CountDownEvent event) {
        noticeFansShareBean.setCountDown(event.getCountDownText());
        if (!TextUtils.isEmpty(event.getCountDownText())) {
            noticeFansShareBean.setItemType(NewShareDialogRcvAdapter.NOTICE_FANS);
        } else {
            noticeFansShareBean.setItemType(NewShareDialogRcvAdapter.OTHER);
        }
        adapter.notifyItemChanged(0);
    }

    public boolean isHasFriend() {
        return hasFriend;
    }

    public void setHasFriend(boolean hasFriend) {
        this.hasFriend = hasFriend;
    }

    public boolean isHasBroadCast() {
        return hasBroadCast;
    }

    public void setHasBroadCast(boolean hasBroadCast) {
        this.hasBroadCast = hasBroadCast;
    }

    public boolean isHasNoticeFans() {
        return hasNoticeFans;
    }

    public void setHasNoticeFans(boolean hasNoticeFans) {
        this.hasNoticeFans = hasNoticeFans;
    }

    public boolean isLinearLayoutManager() {
        return isLinearLayoutManager;
    }

    public void setLinearLayoutManager(boolean linearLayoutManager) {
        isLinearLayoutManager = linearLayoutManager;
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R2.id.share_dialog_cancel)
    public void onCancelClicked() {
        dismiss();
    }

    public interface OnShareDialogItemClick {
        void onSharePlatformClick(Platform platform);

        void onSendBroadcastMsg();
    }

    public void getRoomNoticeFansCd() {
        if (AvRoomDataManager.get().mCurrentRoomInfo == null || AvRoomDataManager.get().mCurrentRoomInfo.getUid() <= 0) {
            SingleToastUtil.showToast("房间信息有误，请稍后再试");
            return;
        }
        createRoomModel.getRoomNoticeFansCd(AvRoomDataManager.get().mCurrentRoomInfo.getUid(), new OkHttpManager.MyCallBack<ServiceResult<Long>>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast("请求失败");
            }

            @Override
            public void onResponse(ServiceResult<Long> response) {
                if (response != null && response.getData() != null) {
                    if (response.getData() > 0) {
                        //开始显示倒计时
                        if (countDownTimer != null) {
                            countDownTimer.cancel();
                            countDownTimer = null;
                        }
                        countDownTimer = new NewShareDialogCountDownTimer(response.getData(), 1000);
                        countDownTimer.start();
                        if (isClickNoticeFans) {
                            SingleToastUtil.showToast("该功能正在冷却中，暂不可用");
                        }
                    } else {
                        if (isClickNoticeFans) {
                            getRoomNoticeFans();
                        }
                    }
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    public void getRoomNoticeFans() {
        if (AvRoomDataManager.get().mCurrentRoomInfo == null || AvRoomDataManager.get().mCurrentRoomInfo.getUid() <= 0) {
            SingleToastUtil.showToast("房间信息有误，请稍后再试");
            return;
        }
        createRoomModel.getRoomNoticeFans(AvRoomDataManager.get().mCurrentRoomInfo.getUid(), new OkHttpManager.MyCallBack<ServiceResult<Long>>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast("通知失败，请稍后再试");
            }

            @Override
            public void onResponse(ServiceResult<Long> response) {
                if (response.isSuccess() && response.getData() != null && response.getData() >= 0) {
                    SingleToastUtil.showToast("通知成功");
                    dismiss();
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreUtils.unregister(this);
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }
}
