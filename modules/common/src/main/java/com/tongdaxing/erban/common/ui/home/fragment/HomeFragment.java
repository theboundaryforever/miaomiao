
package com.tongdaxing.erban.common.ui.home.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.ui.find.activity.CreateRoomActivity;
import com.tongdaxing.erban.common.ui.home.adpater.CommonImageIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.MainAdapter;
import com.tongdaxing.erban.common.ui.search.SearchActivity;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.home.IHomeCore;
import com.tongdaxing.xchat_core.home.IHomeCoreClient;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.ColorUtil;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>首页  </p>
 *
 * @author Administrator
 * @date 2017/11/14
 */
public class HomeFragment extends BaseFragment implements CommonMagicIndicatorAdapter.OnItemSelectListener,
        View.OnClickListener {
    //    private ImageView ivCreateRoomEnter;
    public static final String TAG = "HomeFragment";
    public int TAB_FIXED_NUMBER;//固定的tab的数量
    private int mHotTagTabPosition = 0;//热门tab的在所有tab中的位置 注意还有个默认的关注tab页在第一位
    private List<TabInfo> mTabInfoList;
    private MagicIndicator mMagicIndicator;
    private ViewPager mViewPager;
    private ImageView mNavMasking;
    private View mIvSearch;
    private View mOpenMyRoom;
    private View mToolBar;
    private int hindH = 400;//滑动别色距离
    private int currPosition = 0;
    private boolean isSave = false;
    private float saveToolBarOffset;
    private float currToolBarOffset;
    private OnScrollChangedListener onScrollChangedListener = new OnScrollChangedListener() {
        @Override
        public void onScrollChanged(RecyclerView recyclerView) {
            if (currPosition == mHotTagTabPosition) {
                float verticalOffset = recyclerView.computeVerticalScrollOffset();
                setupToolBar(verticalOffset / (float) hindH);
            }
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!ListUtils.isListEmpty(mTabInfoList)) {
            outState.putParcelableArrayList(Constants.KEY_MAIN_TAB_LIST, (ArrayList<? extends Parcelable>) mTabInfoList);
        }
    }

    @Override
    protected void restoreState(@Nullable Bundle savedInstanceState) {
        super.restoreState(savedInstanceState);
        if (savedInstanceState != null) {
            mTabInfoList = savedInstanceState.getParcelableArrayList(Constants.KEY_MAIN_TAB_LIST);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_main;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onFindViews() {
//        ivCreateRoomEnter = mView.findViewById(R.id.iv_create_room_enter);
        mMagicIndicator = mView.findViewById(R.id.main_indicator);
        mViewPager = mView.findViewById(R.id.main_viewpager);
        mNavMasking = mView.findViewById(R.id.home_fragment_nav_masking);
        mIvSearch = mView.findViewById(R.id.iv_main_search);
        mOpenMyRoom = mView.findViewById(R.id.iv_create_room);
        mToolBar = mView.findViewById(R.id.tool_bar);
        mViewPager.setOffscreenPageLimit(7);
    }

    @Override
    public void onSetListener() {
//        ivCreateRoomEnter.setOnClickListener(v -> CreateRoomActivity.start(getActivity()));
        mIvSearch.setOnClickListener(this);
        mOpenMyRoom.setOnClickListener(this);
    }

    @Override
    public void initiate() {
//        CoreManager.getCore(IHomeCore.class).getMainTabData();
        CoreManager.getCore(IHomeCore.class).getMainDataByMenu();
//        setupView(null);
    }

    public void setupView(List<TabInfo> tabInfos) {
        mTabInfoList = TabInfo.getTabDefaultList();
        TAB_FIXED_NUMBER = mTabInfoList.size();
        if (!ListUtils.isListEmpty(tabInfos)) {
            mTabInfoList.addAll(tabInfos);
        }
        CommonImageIndicatorAdapter mMsgIndicatorAdapter = new CommonImageIndicatorAdapter(mContext, mTabInfoList);
        mMsgIndicatorAdapter.setSize(18);
        mMsgIndicatorAdapter.setNormalColorId(R.color.common_color_14_transparent_87);
        mMsgIndicatorAdapter.setSelectColorId(R.color.color_000000);
        mMsgIndicatorAdapter.setImageIndicatorWidth(35.0f);
        mMsgIndicatorAdapter.setImageIndicatorHeight(14.0f);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        //不要默认等分
//        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        mMagicIndicator.setNavigator(commonNavigator);

        SparseArray<Fragment> mFragmentList = new SparseArray<>();
        mFragmentList.put(0, HomeAttentionFragment.newInstance());//关注tab固定在第一位

//        mFragmentList.put(2, MakeFriendsFragment.newInstance());
        for (int i = TAB_FIXED_NUMBER; i < mTabInfoList.size(); i++) {//mTabInfoList已经包含了"关注"这个tab 所以i从TAB_FIXED_NUMBER开始
            if (mTabInfoList.get(i).getId() == BaseApp.getHomeHotTagID() && mHotTagTabPosition == 0) {//mHotTagTabPosition == 0限制只添加一次热门
                HotTagFragment hotFragment = new HotTagFragment();
                hotFragment.setOnScrollChangedListener(onScrollChangedListener);
                mFragmentList.put(i, hotFragment);
                mHotTagTabPosition = i;
            } else {
                TabInfo tabInfo = mTabInfoList.get(i);
                Fragment fragment = OtherTagFragment.getInstance(tabInfo.getId());
                mFragmentList.put(i, fragment);
            }
        }
        mViewPager.setAdapter(new MainAdapter(getChildFragmentManager(), mFragmentList));
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
        mViewPager.setCurrentItem(mHotTagTabPosition);//默认选中首页
        currPosition = mHotTagTabPosition;
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {//右滑时这个position为用户可能要打开的page的索引，左滑时为当前page
                if (currPosition == mHotTagTabPosition) {
                    mNavMasking.setVisibility(View.INVISIBLE);
                }
                //左滑 进入后一个页面；position + 1 != mTabInfoList.size() - 1 最后一个tab不显示遮罩，避免出现最后一个tab变模糊的情况
                if (position == mHotTagTabPosition && (int) (positionOffset * 100) > 38 && position + 1 != mTabInfoList.size() - 1) {
                    mNavMasking.setVisibility(View.VISIBLE);
                }
                if (position == mHotTagTabPosition - 1 && (int) (positionOffset * 100) < 32) {//右滑 进入前一个页面
                    mNavMasking.setVisibility(View.VISIBLE);
                }
                L.debug(TAG, "position = %d, positionOffset = %f, positionOffsetPixels = %d", position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                L.debug(TAG, "onPageSelected position = %d", position);
                currPosition = position;
                if (position == mHotTagTabPosition) {//热门
                    MobclickAgent.onEvent(mContext, UmengEventId.getHomeTopTab1());
                    setupToolBar(saveToolBarOffset);
                } else {
                    setupToolBar(1);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                L.debug(TAG, "onPageScrollStateChanged state = %d", state);
                if (currPosition == mHotTagTabPosition) {
                    if (state == 1 && isSave) {//开始滑动的时候记录一下状态
                        isSave = false;
                        saveToolBarOffset = currToolBarOffset;
                    } else {//不是最开始滑动的时候，不用记录
                        isSave = true;
                    }
                }
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    if (currPosition != mHotTagTabPosition && currPosition != mTabInfoList.size() - 1) {//最后一个tab不显示遮罩，避免出现最后一个tab变模糊的情况
                        mNavMasking.setVisibility(View.VISIBLE);
                        mNavMasking.setImageResource(R.drawable.home_nav_masking_white);
                    } else {
                        mNavMasking.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
    }

    public void setupToolBar(float toolBarOffset) {
        if (mToolBar == null) {
            return;
        }
        if (toolBarOffset > 1) {
            toolBarOffset = 1;
        }
        if (this.currToolBarOffset != toolBarOffset) {
            this.currToolBarOffset = toolBarOffset;
            int color = ColorUtil.changeColor(toolBarOffset);
            boolean isSelected = toolBarOffset >= 0.5;
            mToolBar.setBackgroundColor(color);
            mToolBar.setSelected(isSelected);
        }
    }

    @Override
    public void onItemSelect(int position) {
        //分类点击
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.iv_main_search) {
            MobclickAgent.onEvent(mContext, UmengEventId.getHomeSearchClick());
            SearchActivity.start(mContext);

        } else if (i == R.id.iv_create_room) {// 打开创建房间
            UmengEventUtil.getInstance().onCreateRoom(mContext, "home");
            CreateRoomActivity.start(mContext);
//                startActivity(new Intent(mContext, AudioCardActivity.class));
//                startActivity(new Intent(getContext(), RankingListActivity.class));//排行榜
//                MobclickAgent.onEvent(mContext, UmengEventId.getHomeRoomBtnClick());
//                getDialogManager().showProgressDialog(getActivity(), "请稍后...");
//                CoreManager.getCore(IRoomCore.class).requestRoomInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid(), 0);

        } else {
        }
    }

    @CoreEvent(coreClientClass = IHomeCoreClient.class)
    public void onGetHomeDataByMenuSuccess(List<TabInfo> response) {
        setupView(response);
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetHomeDataByMenuFail(String message) {
    }

    public interface OnScrollChangedListener {
        void onScrollChanged(RecyclerView recyclerView);
    }
}