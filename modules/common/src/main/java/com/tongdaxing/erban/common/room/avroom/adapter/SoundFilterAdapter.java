package com.tongdaxing.erban.common.room.avroom.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.R;

import java.util.List;


public class SoundFilterAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    private int currSelectPosition = 0;

    public SoundFilterAdapter(List<String> stringList) {
        super(R.layout.item_sound_filter, stringList);
    }

    @Override
    protected void convert(BaseViewHolder holder, String item) {
        TextView soundFilterTv = holder.getView(R.id.sound_filter_tv);
        soundFilterTv.setSelected(currSelectPosition == holder.getAdapterPosition());//选中
        soundFilterTv.setText(item);
    }

    public void setCurrSelectPosition(int position) {
        this.currSelectPosition = position;
        notifyDataSetChanged();
    }
}
