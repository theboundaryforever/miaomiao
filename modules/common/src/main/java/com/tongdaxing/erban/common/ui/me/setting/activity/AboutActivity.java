package com.tongdaxing.erban.common.ui.me.setting.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.Locale;

public class AboutActivity extends BaseActivity {

    TextView versinos;
    TextView slogan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initTitleBar(getString(R.string.title_about_text));
        initView();
        initData();
    }

    private void initData() {
        versinos.setText(String.format(Locale.getDefault(), getString(R.string.about_version_name),
                BasicConfig.getLocalVersionName(getApplication())));
//        SpannableString spannableString = new SpannableString(slogan.getText());
//        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.color_fe5b69)),
//                slogan.getText().length() - 2, slogan.getText().length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        slogan.setText(BaseApp.getAboutUs());
    }

    private void initView() {
        versinos = (TextView) findViewById(R.id.versions);
        slogan = (TextView) findViewById(R.id.about_slogan);
    }
}
