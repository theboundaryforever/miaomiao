package com.tongdaxing.erban.common.room.lover.weekcp.tacit;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.LongSparseArray;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.adapter.BaseAdapterHelper;
import com.erban.ui.adapter.ListAdapter;
import com.erban.ui.baseview.BaseDialogFragment;
import com.tcloud.core.log.L;
import com.tcloud.core.util.DensityUtil;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.room.lover.RoomLoverRoute;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.weekcp.bean.TacitAnswerBean;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTextInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Chen on 2019/5/7.
 */
@Route(path = RoomLoverRoute.ROOM_LOVER_RESULT_TEST_PATH)
public class TestResultDialogFragment extends BaseDialogFragment {
    private final int mTacitResultGrade = 20;   // 默契指数每道题分数
    @BindView(R2.id.room_lover_tacit_index)
    TextView mTacitIndex;
    @BindView(R2.id.room_lover_tacit_answer_count)
    TextView mTacitAnswerCount;
    @BindView(R2.id.room_lover_tacit_listview)
    ListView mTacitListview;
    @Autowired(name = "currentId")
    String mQuestionId;
    private String TAG = "TestResultDialogFragment";
    private ListAdapter<TacitAnswerBean.DataBean.AnswerBean> mTacitAnswerAdapter;
    private List<TacitAnswerBean.DataBean.AnswerBean> mTacitAnswers;
    private TacitAnswerBean.DataBean mAnswerData;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                WindowManager.LayoutParams params = window.getAttributes();
                params.gravity = Gravity.BOTTOM; // 显示在底部
                params.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度填充满屏
                params.height = (int) (DensityUtil.getDisplayHeight(BaseApp.mBaseContext) * 0.6f);
                window.setAttributes(params);
                // 这里用透明颜色替换掉系统自带背景
                int color = ContextCompat.getColor(BaseApp.mBaseContext, android.R.color.transparent);
                window.setBackgroundDrawable(new ColorDrawable(color));
            }
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_lover_dialog_fragment_result_test;
    }

    @Override
    public void initBefore() {
    }

    @Override
    public void findView() {
        ARouter.getInstance().inject(this);
        ButterKnife.bind(this, mContentView);
    }

    @Override
    public void setView() {
        int count = getAnswerCount();
        String index = String.valueOf(count * mTacitResultGrade);
        L.debug(TAG, "setView count: %d, index: %s, mQuestionId: %s", count, index, mQuestionId);
        mTacitIndex.setText(index);
        String text = String.format(getResources().getString(R.string.room_lover_await_tacit_answer), count);
        SpannableString spannableString = new SpannableString(text);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#C162FA"));
        spannableString.setSpan(colorSpan, 3, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        mTacitAnswerCount.setText(spannableString);
    }

    @Override
    public void setListener() {
        updateAnswerView();
    }

    private void updateAnswerView() {
        TacitTextInfo tacitTextInfo = getTacitTextInfo();
        if (tacitTextInfo == null) {
            return;
        }
        mAnswerData = tacitTextInfo.getTacitAnswerBean();
        if (mAnswerData == null) {
            return;
        }
        mTacitAnswers = mAnswerData.getAnswer();
        if (mTacitAnswers == null) {
            return;
        }
        if (mTacitAnswerAdapter == null) {
            mTacitAnswerAdapter = new ListAdapter<TacitAnswerBean.DataBean.AnswerBean>(getContext(), R.layout.room_lover_tacit_answer_item, mTacitAnswers) {
                @Override
                public void onUpdate(BaseAdapterHelper helper, TacitAnswerBean.DataBean.AnswerBean item, int position) {
                    TextView answerName = helper.getView(R.id.room_lover_tacit_question_name);
                    ImageView myPhoto = helper.getView(R.id.room_lover_my_photo);
                    ImageView cpPhoto = helper.getView(R.id.room_lover_cp_photo);
                    TextView myAnswer = helper.getView(R.id.room_lover_my_answer);
                    TextView cpAnswer = helper.getView(R.id.room_lover_cp_answer);

                    answerName.setText(item.getQuestion());
                    myAnswer.setText(item.getUidAnswer());
                    cpAnswer.setText(item.getCpAnswer());

                    String cpAvatar = mAnswerData.getCpAvatar();
                    String uidAvatar = mAnswerData.getUidAvatar();
                    int defaultId = R.drawable.ic_no_avatar;
                    if (TextUtils.isEmpty(cpAvatar)) {
                        ImageLoadUtils.loadCircleImage(getContext(), defaultId, cpPhoto, defaultId);
                    } else {
                        ImageLoadUtils.loadCircleImage(getContext(), cpAvatar, cpPhoto, defaultId);
                    }
                    if (TextUtils.isEmpty(uidAvatar)) {
                        ImageLoadUtils.loadCircleImage(getContext(), defaultId, myPhoto, defaultId);
                    } else {
                        ImageLoadUtils.loadCircleImage(getContext(), uidAvatar, myPhoto, defaultId);
                    }
                }
            };
            mTacitListview.setAdapter(mTacitAnswerAdapter);
        } else {
            mTacitAnswerAdapter.replaceAll(mTacitAnswers);
        }
    }

    public int getAnswerCount() {
        TacitTextInfo textInfo = getTacitTextInfo();
        int count = 0;
        if (textInfo != null) {
            TacitAnswerBean.DataBean answerData = textInfo.getTacitAnswerBean();
            L.debug(TAG, "getAnswerCount answerData: %s", answerData);
            if (answerData != null) {
                List<TacitAnswerBean.DataBean.AnswerBean> answer = answerData.getAnswer();
                if (answer != null) {
                    int size = answer.size();
                    for (int i = 0; i < size; i++) {
                        TacitAnswerBean.DataBean.AnswerBean answerBean = answer.get(i);
                        String cpAnswer = answerBean.getCpAnswer();
                        String uidAnswer = answerBean.getUidAnswer();
                        if (!TextUtils.isEmpty(cpAnswer)) {
                            if (cpAnswer.equals(uidAnswer)) {
                                count++;
                            }
                        }
                    }
                }
            }
        }
        return count;
    }

//    public String getAnswerIndex() {
//        TacitTextInfo textInfo = getTacitTextInfo();
//        String index = "";
//        if (textInfo != null) {
//            List<TacitTestBean.DataBean.QuestionBean> dataBeans = textInfo.getDataBeans();
//            float general = dataBeans.size();
//            float count = getAnswerCount();
//            index = String.valueOf((int) ((count / general) * mMaxTacitResult));
//        }
//        return index;
//    }

    private TacitTextInfo getTacitTextInfo() {
        TacitTextInfo textInfo = null;
        if (!TextUtils.isEmpty(mQuestionId)) {
            LongSparseArray<TacitTextInfo> roomTalkTacitMsgs = CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTestCtrl().getRoomTalkTacitMsgs();
            textInfo = roomTalkTacitMsgs.get(Long.valueOf(mQuestionId));
        }
        return textInfo;
    }
}
