package com.tongdaxing.erban.common.im.actions;

import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.tongdaxing.erban.common.room.cpGift.CpGiftDialog;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.room.cpGift.CpGiftDialog;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

/**
 * 聊天中加号更多中的申请CP
 * Created by zhangjian on 2019/3/26.
 */
@Deprecated
public class CpGiftAction extends BaseAction implements CpGiftDialog.OnCpGiftDialogBtnClickListener, ChargeListener {


    transient private CpGiftDialog giftDialog;
    private boolean isShowingChargeDialog;

    public CpGiftAction() {
        super(R.drawable.cp_ic_apply, R.string.cp_gift_action);
    }

    @Override
    public void onClick() {
        isShowingChargeDialog = false;
        if (giftDialog == null) {
            giftDialog = new CpGiftDialog(getActivity(), Long.valueOf(getAccount()));
            giftDialog.setCpGiftDialogBtnClickListener(this);
            giftDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    giftDialog = null;
                }
            });
        }
        if (!giftDialog.isShowing()) {
            if (giftDialog != null) {
                giftDialog.show();
            }

        }
    }

    /**
     * 充值按钮点击
     */
    @Override
    public void onRechargeBtnClick() {
        MyWalletNewActivity.start(getActivity());
    }

    /**
     * 赠送按钮点击
     * CP判断
     *
     * @param giftInfo
     * @param uid
     */
    @Override
    public void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid) {
        if (giftInfo != null) {
            LogUtil.d("CpGiftAction onSendCpGiftBtnClick");
            CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftInfo.getGiftId(), uid,
                    1, -1, this);
        }
    }

    @Override
    public void onNeedCharge() {
        if (isShowingChargeDialog) {
            return;
        }
        isShowingChargeDialog = true;
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    MyWalletNewActivity.start(getActivity());
                    fragment.dismiss();
                }
                isShowingChargeDialog = false;
            }
        }).show(((FragmentActivity) getActivity()).getSupportFragmentManager(), "charge");

    }
}