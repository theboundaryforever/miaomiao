package com.tongdaxing.erban.common.presenter.find;

import com.tongdaxing.erban.common.ui.find.view.IVoiceGroupView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public class VoiceGroupPresenter extends AbstractMvpPresenter<IVoiceGroupView> {
}
