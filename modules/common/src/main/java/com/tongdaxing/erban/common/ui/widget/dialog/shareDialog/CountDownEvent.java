package com.tongdaxing.erban.common.ui.widget.dialog.shareDialog;

/**
 * <p>
 * Created by zhangjian on 2019/7/18.
 */
public class CountDownEvent {
    private String countDownText;

    public CountDownEvent(String countDownText) {
        this.countDownText = countDownText;
    }

    public String getCountDownText() {
        return countDownText;
    }

    public void setCountDownText(String countDownText) {
        this.countDownText = countDownText;
    }

    public static class OnFinish {

    }
}
