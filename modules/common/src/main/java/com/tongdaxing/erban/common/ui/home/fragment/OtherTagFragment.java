package com.tongdaxing.erban.common.ui.home.fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.presenter.home.IOtherTagView;
import com.tongdaxing.erban.common.presenter.home.OtherTagPresenter;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.home.adpater.RoomListAdapter;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.banner.BannerClickModel;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;

import java.util.List;

/**
 * 热门、关注外其他的其他标签fragment
 */
@CreatePresenter(OtherTagPresenter.class)
public class OtherTagFragment extends BaseMvpFragment<IOtherTagView, OtherTagPresenter> implements IOtherTagView {

    public static final int ITEM_COUNT_BY_ROW = 2;//每行显示多少个
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private RoomListAdapter mAdapter;
    private int tagId;

    public static OtherTagFragment getInstance(int tagId) {
        OtherTagFragment tagFragment = new OtherTagFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("tagId", tagId);
        tagFragment.setArguments(bundle);
        return tagFragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_home_other_tag;
    }

    @Override
    public void onFindViews() {
        recyclerView = mView.findViewById(R.id.recycler_view);
        GridLayoutManager manager = new GridLayoutManager(mContext, ITEM_COUNT_BY_ROW);
        recyclerView.setLayoutManager(manager);
        tagId = 0;
        if (getArguments() != null) {
            tagId = getArguments().getInt("tagId");
        }
        if (tagId == BaseApp.getHomeLiveTagID()) {
            mAdapter = new RoomListAdapter(mContext, BannerClickModel.TAB_LIVE_BANNER);
        } else {
            mAdapter = new RoomListAdapter(mContext);
        }
        recyclerView.setAdapter(mAdapter);
        int itemMargin = getResources().getDimensionPixelSize(R.dimen.home_room_item_margin);//每一行间距
        RecyclerView.ItemDecoration decoration = new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                int itemPosition = parent.getChildLayoutPosition(view) - (mAdapter != null ? mAdapter.getHeaderLayoutCount() : 0);
                if (itemPosition >= 0) {
                    outRect.left = 0;
                    outRect.top = itemMargin;
                }
            }
        };
        recyclerView.addItemDecoration(decoration, 0);

        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.mm_theme));
    }

    @Override
    public void onSetListener() {
        mAdapter.setOnLoadMoreListener(() -> {
            if (NetworkUtil.isNetAvailable(getContext())) {
                getMvpPresenter().loadMoreData();
            } else {
                mAdapter.loadMoreEnd(true);
            }
        }, recyclerView);
        mAdapter.setOnItemClickListener((baseQuickAdapter, view, i) -> {
            AVRoomActivity.start(getActivity(), mAdapter.getData().get(i).getUid());
            UmengEventUtil.getInstance().onHomeCategoryList(mContext, mAdapter.getData().get(i).getRoomTag());
        });


        swipeRefreshLayout.setOnRefreshListener(() -> getMvpPresenter().refreshData());
    }

    @Override
    public void initiate() {
        getMvpPresenter().setTagId(tagId);
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    @Override
    public void onReloadData() {
        showLoading();
        getMvpPresenter().refreshData();
    }

    @Override
    public void setupListView(boolean isRefresh, List<HomeRoom> homeRooms, HomeRoom homeRoomBanner, int bannerPosition) {
        if (isRefresh) {
            swipeRefreshLayout.setRefreshing(false);
            if (bannerPosition != -1 && homeRoomBanner != null) {
                mAdapter.setBannerPosition(bannerPosition);
                mAdapter.getData().add(bannerPosition, homeRoomBanner);
            }
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.setNewData(homeRooms);
                }
            });
        } else {
            if (bannerPosition != -1 && homeRoomBanner != null) {
                mAdapter.setBannerPosition(bannerPosition);
                mAdapter.getData().add(bannerPosition, homeRoomBanner);
                mAdapter.notifyItemInserted(bannerPosition);
            } else {
                if (!ListUtils.isListEmpty(homeRooms)) {
                    mAdapter.addData(homeRooms);
                    mAdapter.loadMoreComplete();
                } else {
                    mAdapter.loadMoreEnd(true);
                }
            }
        }
    }

    @Override
    public void setupFailView(boolean isRefresh) {
        if (isRefresh) {
            swipeRefreshLayout.setRefreshing(false);
        } else {
            mAdapter.loadMoreFail();
        }
    }
}