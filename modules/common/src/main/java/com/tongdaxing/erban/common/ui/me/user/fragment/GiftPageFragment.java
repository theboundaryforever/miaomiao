package com.tongdaxing.erban.common.ui.me.user.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.ui.me.shopping.adapter.NewGiftWallAdapter;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/27
 */
public class GiftPageFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener {
    private RecyclerView recyclerView;
    private RadioButton rgUserInfoGift;
    private LinearLayout llUserGiftEmpty;
    private RadioGroup radioGroup;
    private TextView tvGiftEmpty;
    private NewGiftWallAdapter adapter;
    private long userId;
    private int currentIndex = 0;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_gift_page;
    }

    @Override
    public void onFindViews() {
        recyclerView = mView.findViewById(R.id.recycler_view);
        llUserGiftEmpty = mView.findViewById(R.id.ll_user_gift_empty);
        tvGiftEmpty = mView.findViewById(R.id.gift_empty);
        radioGroup = mView.findViewById(R.id.rg_user_info_gift);

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 4);
        recyclerView.setLayoutManager(manager);
        adapter = new NewGiftWallAdapter(R.layout.list_item_gift_wall_info);
        recyclerView.setAdapter(adapter);
//        recyclerView.setNestedScrollingEnabled(false);
    }

    private void showEmpty(boolean isError, String msg) {
        if (llUserGiftEmpty.getVisibility() == View.GONE)
            llUserGiftEmpty.setVisibility(View.VISIBLE);
        if (recyclerView.getVisibility() == View.VISIBLE)
            recyclerView.setVisibility(View.GONE);
        if (isError) {
            tvGiftEmpty.setText(StringUtils.isEmpty(msg) ? "网络异常" : msg);
        } else {
            tvGiftEmpty.setText(currentIndex == 0 ? "暂没有收到礼物" : "还木有收到神秘礼物>_< 赶紧收集吧~");
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestGiftWallFail(long uid, String msg) {
        if (uid != userId) {
            return;
        }
        if (getDialogManager().isDialogShowing())
            getDialogManager().dismissDialog();
        showEmpty(true, msg);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestGiftWall(long uid, List<GiftWallInfo> giftWallInfoList) {
        if (uid != userId) {
            return;
        }
        if (getDialogManager().isDialogShowing())
            getDialogManager().dismissDialog();
        if (ListUtils.isListEmpty(giftWallInfoList)) {
            showEmpty(false, "");
        } else {
            if (llUserGiftEmpty.getVisibility() == View.VISIBLE)
                llUserGiftEmpty.setVisibility(View.GONE);
            if (recyclerView.getVisibility() == View.GONE)
                recyclerView.setVisibility(View.VISIBLE);
            adapter.setNewData(giftWallInfoList);
        }
    }

    @Override
    public void onSetListener() {
        radioGroup.setOnCheckedChangeListener(this);
    }

    @Override
    public void initiate() {
        Bundle bundle = getArguments();
        if (bundle == null) {
            showNoData();
            return;
        }
        userId = bundle.getLong("userId", 0);
    }

    public void refreshData() {
        if (radioGroup != null) {
            radioGroup.check(R.id.rb_user_gift_normal);
        }
        CoreManager.getCore(IUserCore.class).requestUserGiftWall(userId, 2);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rb_user_gift_normal) {
            if (currentIndex == 0)
                return;
            currentIndex = 0;
            getDialogManager().showProgressDialog(getActivity(), "加载中...");
            CoreManager.getCore(IUserCore.class).requestUserGiftWall(userId, 2);

        } else if (checkedId == R.id.rb_user_gift_mystery) {
            if (currentIndex == 1)
                return;
            currentIndex = 1;
            getDialogManager().showProgressDialog(getActivity(), "加载中...");
            CoreManager.getCore(IUserCore.class).requestUserMysteryGiftWall(CoreManager.getCore(IAuthCore.class).getCurrentUid(), userId, 2);

        }
    }
}
