package com.tongdaxing.erban.common.room.talk.factory;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.web.LuckyWheelDialog;
import com.tongdaxing.erban.common.view.UrlImageSpan;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.AttentionSuccessAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.BlessingBeastAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRuleAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.SquareCpMsgAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.talk.message.CommonMessage;
import com.tongdaxing.xchat_core.room.turnTable.bean.TurnTableDetonationAttachment;
import com.tongdaxing.xchat_core.room.turnTable.bean.TurnTablePiecesExchangeAttachment;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.RichTextUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Administrator on 5/31 0031.
 */

public class CommonFactory implements AbsBaseViewHolder.ViewHolderFactory {
    @NonNull
    @Override
    public CommonViewHolder create(@NonNull ViewGroup parent) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_talk_common_item, parent, false);
        return new CommonViewHolder(v);
    }

    public static class CommonViewHolder extends AbsBaseViewHolder<CommonMessage> {
        private TextView mTvContent;
        private LinearLayout mLlRootView;
        private String mAccount;

        public CommonViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvContent = itemView.findViewById(R.id.talk_iv_common_content);
            mLlRootView = itemView.findViewById(R.id.talk_ll_common_view);
            mTvContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatRoomMessage chatRoomMessage = (ChatRoomMessage) v.getTag();
                    CoreUtils.send(new RoomTalkEvent.OnRoomMsgItemClicked(chatRoomMessage));
                }
            });
        }

        @Override
        public void bind(CommonMessage message) {
            super.bind(message);
            mLlRootView.setBackground(null);
            mTvContent.setBackground(null);
            ChatRoomMessage chatRoomMessage = message.getChatRoomMessage();
            mTvContent.setTag(chatRoomMessage);
            CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
            int first = attachment.getFirst();
            if (first == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE) {
                setSystemMsg(attachment);
            } else if (first == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_FIRST) {
                setPkMsg(attachment);
            } else if (first == CustomAttachment.CUSTOM_MSG_BLESSING_BEAST) {
                BlessingBeastAttachment blessingBeastAttachment = (BlessingBeastAttachment) attachment;
                if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_BLESSING_BEAST) {
                    showSummonBlessingBeastMsg(blessingBeastAttachment);
                } else if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_BLESSING_BEAST_SECONDS) {//福兽的中奖消息
                    showBlessingBeastPrizeMsg(blessingBeastAttachment);
                }
            } else if (first == CustomAttachment.CUSTOM_MSG_ROOM_CP_BOTTOM) {
                showRoomCpBottomTipMsg((SquareCpMsgAttachment) attachment);
            } else if (first == CustomAttachment.CUSTOM_MSG_TURN_TABLE_WITH_BUBBLE ||
                    first == CustomAttachment.CUSTOM_MSG_TURN_TABLE_NO_BUBBLE) {
                setTurnTableInfo((LotteryBoxAttachment) attachment, first == CustomAttachment.CUSTOM_MSG_TURN_TABLE_WITH_BUBBLE);
            } else if (first == CustomAttachment.CUSTOM_MSG_TURN_TABLE_DETONATING) {
                setWheelWent((TurnTableDetonationAttachment) attachment);
            } else if (first == CustomAttachment.CUSTOM_MSG_TURN_TABLE_PIECES_EXCHANGE) {
                setTurnTableInfo((TurnTablePiecesExchangeAttachment) attachment);
            } else if (first == CustomAttachment.CUSTOM_MSG_ROOM_PERSONAL_ATTENTION_SUCCESS_MSG) {
                mTvContent.setText(((AttentionSuccessAttachment) attachment).getContent());
            } else if (first == CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST) {
                setRule((RoomRuleAttachment) attachment);
            } else if (first == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP) {
                setMsgRoomTip(attachment);
            }
        }

        private void setSystemMsg(CustomAttachment attachment) {
            int second = attachment.getSecond();
            RoomQueueMsgAttachment roomQueueMsgAttachment = (RoomQueueMsgAttachment) attachment;
            String uid = roomQueueMsgAttachment.getUid();
            String handleNick;
            if (AvRoomDataManager.get().isRoomOwner(uid)) {
                handleNick = "房主";
            } else if (AvRoomDataManager.get().isRoomAdmin(uid)) {
                handleNick = "管理员";
            } else {
                return;
            }
            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN) {
                list.add(RichTextUtil.getRichTextMap(handleNick, 0xFFFFFFFF));
                list.add(RichTextUtil.getRichTextMap("已屏蔽该房间小礼物特效", 0xffffffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE) {
                list.add(RichTextUtil.getRichTextMap(handleNick, 0xFFFFFFFF));
                list.add(RichTextUtil.getRichTextMap("已开启该房间小礼物特效", 0xffffffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
                list.add(RichTextUtil.getRichTextMap(handleNick, 0xFFFFFFFF));
                list.add(RichTextUtil.getRichTextMap("关闭了房间内聊天", 0xffffffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
                list.add(RichTextUtil.getRichTextMap(handleNick, 0xFFFFFFFF));
                list.add(RichTextUtil.getRichTextMap("开启了房间内聊天", 0xffffffff));
            }
            mTvContent.setText(RichTextUtil.getSpannableStringFromList(list));
            mTvContent.setTextColor(mTvContent.getContext().getResources().getColor(R.color.white_op_60));
            mTvContent.setBackground(null);
            mLlRootView.setBackground(mLlRootView.getContext().getResources().getDrawable(R.drawable.room_talk_tip_bg_shape));
        }

        private void setPkMsg(CustomAttachment attachment) {
            mTvContent.setTextColor(mTvContent.getContext().getResources().getColor(R.color.white));
            PkCustomAttachment chatRoomMessage = (PkCustomAttachment) attachment;
            PkVoteInfo info = chatRoomMessage.getPkVoteInfo();
            LogUtil.d("chatRoomMessage", "first = " + chatRoomMessage.getFirst() + " second = " + chatRoomMessage.getSecond());
            if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_SECOND_START) {
                String nick = (AvRoomDataManager.get().isRoomOwner(info.getOpUid()) ? "房主" : "管理员") + " 开起了 ";
                String targetNick = info.getNick();
                String targetNick2 = info.getPkNick();
                String content = nick + targetNick + " 和 " + targetNick2 + " 的PK";
                mTvContent.setText(content);
            } else if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_SECOND_END) {
                String content = "本场PK结果：";
                String result = "";
                if (info.getVoteCount() == info.getPkVoteCount()) {
                    result = "平局!";
                } else if (info.getVoteCount() > info.getPkVoteCount()) {
                    content += info.getNick();
                    result = " 胜利!";
                } else {
                    content += info.getPkNick();
                    result = " 胜利!";
                }
                content = content + result;
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                ForegroundColorSpan redSpan1 = new ForegroundColorSpan(mTvContent.getContext().getResources().getColor(R.color.color_ffd800));
                if (!TextUtils.isEmpty(result))
                    builder.setSpan(redSpan1, content.length() - result.length(), content.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                mTvContent.setText(builder);
            } else if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_SECOND_CANCEL) {
                String nick = (AvRoomDataManager.get().isRoomOwner(info.getOpUid()) ? "房主" : "管理员") + " 取消了 ";
                String targetNick = info.getNick();
                String targetNick2 = info.getPkNick();
                if (!TextUtils.isEmpty(targetNick) && targetNick.length() > 6) {
                    targetNick = targetNick.substring(0, 6) + "...";
                }
                if (!TextUtils.isEmpty(targetNick2) && targetNick2.length() > 6) {
                    targetNick2 = targetNick2.substring(0, 6) + "..." + " 的PK";
                } else {
                    targetNick2 = targetNick2 + " 的PK";
                }
                String content = nick + targetNick + " 和 " + targetNick2;
                mTvContent.setText(content);
            }
        }

        /**
         * 显示召唤福兽消息
         *
         * @param blessingBeastAttachment
         */
        private void showSummonBlessingBeastMsg(BlessingBeastAttachment blessingBeastAttachment) {
            if (blessingBeastAttachment == null) {
                return;
            }

            String content = blessingBeastAttachment.getContent();
            if (!TextUtils.isEmpty(content)) {
                mTvContent.setText(content);
            }

            if (blessingBeastAttachment.getRoomUid() != AvRoomDataManager.get().mCurrentRoomInfo.getUid()) {
                mTvContent.setOnClickListener(v -> {
                    //跳转到另一个房间
                    AVRoomActivity.start(mTvContent.getContext(), blessingBeastAttachment.getRoomUid());
                });
            } else {
                if (blessingBeastAttachment.getUid() > 0) {
                    mTvContent.setOnClickListener(v -> {
                        UserInfoDialogManager.showDialogFragment(mTvContent.getContext(), blessingBeastAttachment.getUid());
                    });
                }
            }

            if (blessingBeastAttachment.getType() == 2) {
                mTvContent.setBackgroundResource(R.drawable.bg_blessing_beast);
            } else {
                mTvContent.setBackgroundResource(0);
            }
        }

        /**
         * 显示福兽中奖消息
         *
         * @param blessingBeastAttachment
         */
        private void showBlessingBeastPrizeMsg(BlessingBeastAttachment blessingBeastAttachment) {
            if (blessingBeastAttachment == null) {
                return;
            }

            String content = blessingBeastAttachment.getContent();
            if (!TextUtils.isEmpty(content)) {
                mTvContent.setText(content);
            }

            if (blessingBeastAttachment.getUid() > 0) {
                mTvContent.setOnClickListener(v -> {
                    UserInfoDialogManager.showDialogFragment(mTvContent.getContext(), blessingBeastAttachment.getUid());
                });
            }

            if (blessingBeastAttachment.getType() == 2) {
                mTvContent.setBackgroundResource(R.drawable.bg_blessing_beast);
            } else {
                mTvContent.setBackgroundResource(0);
            }
        }

        /**
         * 显示cp结成消息
         * <p>
         * 哇，恭喜xx和xx在xxx[戒指]的见证下结合成为全服第xx对CP，让我们为他们欢呼祝福吧。
         *
         * @param squareCpMsgAttachment
         */
        private void showRoomCpBottomTipMsg(SquareCpMsgAttachment squareCpMsgAttachment) {
            String userName1 = squareCpMsgAttachment.getInviteUser().getNick();
            String userName2 = squareCpMsgAttachment.getRecUser().getNick();
            String cpNum = String.valueOf(squareCpMsgAttachment.getCpNum());

            int normalCore = 0xFFFFFFFF;
            int focusCore = 0xFFFFD800;
            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            list.add(RichTextUtil.getRichTextMap("哇，恭喜", normalCore));
            list.add(RichTextUtil.getRichTextMap(userName1, normalCore));
            list.add(RichTextUtil.getRichTextMap("和", normalCore));
            list.add(RichTextUtil.getRichTextMap(userName2, normalCore));
            list.add(RichTextUtil.getRichTextMap("在 ", normalCore));
            list.add(RichTextUtil.getRichTextMap(squareCpMsgAttachment.getGiftName(), focusCore));
            SpannableStringBuilder builder = RichTextUtil.getSpannableStringFromList(list);

            //插入礼物图标
            builder.append(BaseConstant.GIFT_PLACEHOLDER);
            UrlImageSpan imageSpan = new UrlImageSpan(mTvContent.getContext(), squareCpMsgAttachment.getPicUrl(), mTvContent);
            imageSpan.setImgWidth(DisplayUtils.dip2px(mTvContent.getContext(), 20));
            imageSpan.setImgHeight(DisplayUtils.dip2px(mTvContent.getContext(), 20));
            builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(), builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            list = new ArrayList<>();
            list.add(RichTextUtil.getRichTextMap(" 的见证下结合成为全服第", normalCore));
            list.add(RichTextUtil.getRichTextMap(cpNum, focusCore));
            list.add(RichTextUtil.getRichTextMap("对CP，让我们为他们欢呼祝福吧。", normalCore));
            builder.append(RichTextUtil.getSpannableStringFromList(list));

            mTvContent.setText(builder);
        }

        /**
         * 大转盘暴走消息
         *
         * @param attachment
         */
        private void setWheelWent(TurnTableDetonationAttachment attachment) {
            String params = attachment.getParams();
            Json json = Json.parse(params);
            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            HashMap<String, Object> map;
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, json.str("string"));
            map.put(RichTextUtil.RICHTEXT_COLOR, Color.parseColor(json.str("color")));
            list.add(map);
            mTvContent.setText(RichTextUtil.getSpannableStringFromList(list));

            mTvContent.setOnClickListener(v -> {
                DialogManager mDialogManager = new DialogManager(mTvContent.getContext());
                mDialogManager.setCanceledOnClickOutside(false);
                mDialogManager.showProgressDialog(mTvContent.getContext(), "加载中...");
                new LuckyWheelDialog(mTvContent.getContext(), new LuckyWheelDialog.Callback() {
                    @Override
                    public void succed(LuckyWheelDialog dialog) {
                        dialog.show();
                        mDialogManager.dismissDialog();
                    }

                    @Override
                    public void failed(LuckyWheelDialog dialog) {
                        mDialogManager.dismissDialog();
                        com.juxiao.library_utils.log.LogUtil.dToFile("大转盘", "打开大转盘失败");
                    }
                });
            });
        }

        private void setTurnTableInfo(TurnTablePiecesExchangeAttachment attachment) {
            String params = attachment.getMessage();
            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            HashMap<String, Object> map;
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, params);
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            mTvContent.setText(RichTextUtil.getSpannableStringFromList(list));

            mTvContent.setOnClickListener(v -> {
                if (attachment.getUid() > 0) {
                    UserInfoDialogManager.showDialogFragment(mTvContent.getContext(), attachment.getUid());
                }
            });
        }

        /**
         * 大转盘获奖消息
         *
         * @param attachment
         * @param withBubble 是否带气泡
         */
        private void setTurnTableInfo(LotteryBoxAttachment attachment, boolean withBubble) {
            String params = attachment.getParams();
            Json json = Json.parse(params);
            String senderNick = json.str("nick");
            String giftName = json.str("giftName");
            long uid = json.num_l("uid");
            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            HashMap<String, Object> map;
            map = new HashMap<String, Object>();
            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (roomInfo == null) {
                return;
            }
            boolean isInCurrentRoom = roomInfo.getRoomId() == json.num_l("roomId");
            if (!isInCurrentRoom) {
                map.put(RichTextUtil.RICHTEXT_STRING, "厉害了, ");//在其他房间
            } else {
                map.put(RichTextUtil.RICHTEXT_STRING, "哇塞~");//在本房间
            }
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, senderNick);
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            map = new HashMap<String, Object>();
            if (!isInCurrentRoom) {
                map.put(RichTextUtil.RICHTEXT_STRING, " 在魔力转圈圈中，转到了");
            } else {
                map.put(RichTextUtil.RICHTEXT_STRING, " 在本房间魔力转圈圈中，转到了");//在本房间
            }
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            map = new HashMap<String, Object>();
            if (json.has("goldPrice")) {
                map.put(RichTextUtil.RICHTEXT_STRING, giftName + "（" + json.num("goldPrice") + "金币）");
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFD800);
            } else {
                map.put(RichTextUtil.RICHTEXT_STRING, giftName );
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFD800);
            }
            list.add(map);
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, " x" + json.str("count"));
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFD800);
            list.add(map);
            mTvContent.setText(RichTextUtil.getSpannableStringFromList(list));
            setWheelBg(json);

            mTvContent.setOnClickListener(v -> {
                UserInfoDialogManager.showDialogFragment(mTvContent.getContext(), uid);
            });
        }

        private void setWheelBg(Json json) {
            int goldPrice = json.num("goldPrice");
            if (goldPrice < 500) {
                mTvContent.setBackground(null);
            } else if (goldPrice < 5000) {
                mTvContent.setBackground(ContextCompat.getDrawable(mTvContent.getContext(), R.drawable.wheel_small_bg));
            } else if (goldPrice < 30000) {
                mTvContent.setBackground(ContextCompat.getDrawable(mTvContent.getContext(), R.drawable.wheel_middle_bg));
            } else {
                mTvContent.setBackground(ContextCompat.getDrawable(mTvContent.getContext(), R.drawable.wheel_big_bg));
            }
        }

        private void setRule(RoomRuleAttachment attachment) {
            mTvContent.setTextColor(mTvContent.getContext().getResources().getColor(R.color.color_ffd800));
            mTvContent.setText(attachment.getRule());
        }

        private void setMsgRoomTip(CustomAttachment attachment) {
            RoomTipAttachment roomTipAttachment = (RoomTipAttachment) attachment;
            String content = "";
            if (StringUtil.isEmpty(roomTipAttachment.getNick())) {
                roomTipAttachment.setNick(" ");
            }
            if (roomTipAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_SHARE_ROOM) {
                content = roomTipAttachment.getNick() + " 分享了房间";
            } else if (roomTipAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_ATTENTION_ROOM_OWNER) {
                content = roomTipAttachment.getNick() + " 关注了房主";
            }
            SpannableStringBuilder builder = new SpannableStringBuilder(content);
            mTvContent.setText(builder);
            mTvContent.setTextColor(mTvContent.getContext().getResources().getColor(R.color.white));
        }
    }
}

