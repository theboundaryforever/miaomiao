package com.tongdaxing.erban.common.ui.me.shopping.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;

/**
 * Function:
 * Author: Edward on 2019/3/30
 */
public class NewGiftWallAdapter extends BaseQuickAdapter<GiftWallInfo, BaseViewHolder> {
    public NewGiftWallAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, GiftWallInfo item) {
        helper.setText(R.id.gift_name, item.getGiftName());
        helper.setText(R.id.gift_num, item.getReciveCount() + "");
        ImageView imageView = helper.getView(R.id.gift_img);
        ImageLoadUtils.loadImage(mContext, item.getPicUrl(), imageView);
    }
}
