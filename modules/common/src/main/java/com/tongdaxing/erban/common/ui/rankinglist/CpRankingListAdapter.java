package com.tongdaxing.erban.common.ui.rankinglist;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.ui.cpexclusive.CpExclusivePageActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.NumberFormatUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.bean.RankingInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> 排行榜adapter </p>
 */
public class CpRankingListAdapter extends BaseQuickAdapter<RankingInfo.RankVo, BaseViewHolder> {
    private final static int HEADER_VIEW_DATA_COUNT = 3;//头部数据个数
    private Context context;
    private View headerView;
    private int rankingType;

    public CpRankingListAdapter(int layoutRes, Context context, int rankingType) {
        super(layoutRes);
        this.context = context;
        this.rankingType = rankingType;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, RankingInfo.RankVo info) {
        if (info == null) {
            return;
        }
        if (baseViewHolder.getAdapterPosition() == getData().size()) {//第一个item，这里注意有headerView
            baseViewHolder.getView(R.id.line).setVisibility(View.GONE);//不显示顶部的灰色线
        } else {
            baseViewHolder.getView(R.id.line).setVisibility(View.VISIBLE);
        }
        //排名
        ((TextView) baseViewHolder.getView(R.id.ranking_number)).setText(String.valueOf(HEADER_VIEW_DATA_COUNT
                + (baseViewHolder.getAdapterPosition() - getHeaderLayoutCount() + 1)));
        //按性别放置左右
        int genderFlag = 0;//0-邀请者男左 接受者女右 1-接受者男左 邀请者女右 2-男同性就邀请者左 接受者右 3女同性
        RankingInfo.RankVo.InviteUserBean inviteUser = info.getInviteUser();
        RankingInfo.RankVo.RecUserBean recUser = info.getRecUser();
        if (inviteUser != null && recUser != null) {
            if (inviteUser.getGenderX() == UserInfo.GENDER_BOY && recUser.getGenderX() == UserInfo.GENDER_GIRL) {
                genderFlag = 0;
            } else if (inviteUser.getGenderX() == UserInfo.GENDER_GIRL && recUser.getGenderX() == UserInfo.GENDER_BOY) {
                genderFlag = 1;
            } else if (inviteUser.getGenderX() == UserInfo.GENDER_BOY && recUser.getGenderX() == UserInfo.GENDER_BOY) {
                genderFlag = 2;
            } else if (inviteUser.getGenderX() == UserInfo.GENDER_GIRL && recUser.getGenderX() == UserInfo.GENDER_GIRL) {
                genderFlag = 3;
            }
            //头像 性别 昵称
            ImageView boyAvatar = baseViewHolder.getView(R.id.boy_avatar);
            ImageView imgHead = baseViewHolder.getView(R.id.avatar);
            ImageView boyGender = baseViewHolder.getView(R.id.ranking_list_item_boy_gender);
            ImageView imgGender = baseViewHolder.getView(R.id.ranking_list_item_gender);
            TextView nickNameBoy = baseViewHolder.getView(R.id.nickname_boy);
            TextView nickName = baseViewHolder.getView(R.id.nickname);
            if (genderFlag != 1) {
                //邀请者左 接受者右
                //头像
                ImageLoadUtils.loadAvatar(context, inviteUser.getAvatarX(), boyAvatar);
                ImageLoadUtils.loadAvatar(context, recUser.getAvatarX(), imgHead);
                boyAvatar.setOnClickListener(new CpRankingListAdapter.UserHeadImgClickImpl(inviteUser.getUidX()));
                imgHead.setOnClickListener(new CpRankingListAdapter.UserHeadImgClickImpl(recUser.getUidX()));
                //性别
                boyGender.setImageResource(inviteUser.getGenderX() == UserInfo.GENDER_BOY ? R.drawable.user_info_boy : R.drawable.user_info_girl);
                imgGender.setImageResource(recUser.getGenderX() == UserInfo.GENDER_BOY ? R.drawable.user_info_boy : R.drawable.user_info_girl);
                //昵称
                nickNameBoy.setText(inviteUser.getNickX());
                nickName.setText(recUser.getNickX());
            } else {
                //接受者左 邀请者右
                ImageLoadUtils.loadAvatar(context, recUser.getAvatarX(), boyAvatar);
                ImageLoadUtils.loadAvatar(context, inviteUser.getAvatarX(), imgHead);
                boyGender.setImageResource(recUser.getGenderX() == UserInfo.GENDER_BOY ? R.drawable.user_info_boy : R.drawable.user_info_girl);
                imgGender.setImageResource(inviteUser.getGenderX() == UserInfo.GENDER_BOY ? R.drawable.user_info_boy : R.drawable.user_info_girl);
                boyAvatar.setOnClickListener(new CpRankingListAdapter.UserHeadImgClickImpl(recUser.getUidX()));
                imgHead.setOnClickListener(new CpRankingListAdapter.UserHeadImgClickImpl(inviteUser.getUidX()));
                nickNameBoy.setText(recUser.getNickX());
                nickName.setText(inviteUser.getNickX());
            }

            TextView numberText = baseViewHolder.getView(R.id.number);
            if (info.getDistance() == 0) {//0时显示1
                numberText.setText("0");
            } else if (info.getDistance() == -1) {//-1代表不显示
                numberText.setText("");
            } else if (info.getDistance() >= 10000) {
                numberText.setText(NumberFormatUtils.formatDoubleDecimalPointWithMax2Digit(info.getDistance() / 10000) + "万");
            } else {
                numberText.setText(String.valueOf((long) info.getDistance()));
            }
        }
    }

    @Override
    public int setHeaderView(View headerView) {
        this.headerView = headerView;
        return super.setHeaderView(headerView);
    }

    @Override
    public void setNewData(@Nullable List<RankingInfo.RankVo> dataList) {
        //这里拆分头部和adapter部分显示
        List<RankingInfo.RankVo> headerDataList = new ArrayList<>();
        List<RankingInfo.RankVo> adapterDataList = new ArrayList<>();
        if (dataList != null) {
            if (dataList.size() > HEADER_VIEW_DATA_COUNT) {//头部显示不完
                headerDataList.addAll(dataList.subList(0, HEADER_VIEW_DATA_COUNT));
                adapterDataList.addAll(dataList.subList(HEADER_VIEW_DATA_COUNT, dataList.size()));
            } else {
                headerDataList.addAll(dataList);
            }
        }
        setupHeaderView(headerDataList);
        super.setNewData(adapterDataList);
    }

    /**
     * 显示头部
     * （数据空的时候各项显示为空，并不是都隐藏）
     */
    private void setupHeaderView(List<RankingInfo.RankVo> headerDataList) {
        TextView numberSecond = headerView.findViewById(R.id.number_second);
        TextView numberThird = headerView.findViewById(R.id.number_third);
        Drawable drawable = mContext.getResources().getDrawable(R.drawable.ranking_list_love);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        numberSecond.setCompoundDrawables(drawable, null, null, null);
        numberThird.setCompoundDrawables(drawable, null, null, null);

        if (!ListUtils.isListEmpty(headerDataList) && headerDataList.get(0) != null) {
            headerDataList.get(0).setDistance(-1);//第一个不显示距离上一名
        }
        setupHeaderChildView(headerDataList != null && headerDataList.size() > 0 ? headerDataList.get(0) : null,
                R.id.avatar_first_boy, R.id.avatar_first, R.id.nickname_first_boy, R.id.nickname_first, R.id.number_first);
        setupHeaderChildView(headerDataList != null && headerDataList.size() > 1 ? headerDataList.get(1) : null,
                R.id.avatar_second_boy, R.id.avatar_second, R.id.nickname_second_boy, R.id.nickname_second, R.id.number_second);
        setupHeaderChildView(headerDataList != null && headerDataList.size() > 2 ? headerDataList.get(2) : null,
                R.id.avatar_third_boy, R.id.avatar_third, R.id.nickname_third_boy, R.id.nickname_third, R.id.number_third);
    }

    /**
     * 显示头部每一个子view
     */
    private void setupHeaderChildView(RankingInfo.RankVo info, int leftAvatarId, int rightAvatarId, int leftNicknameId, int rightNicknameId, int numberId) {
        ImageView boyAvatar = headerView.findViewById(leftAvatarId);
        ImageView imgHead = headerView.findViewById(rightAvatarId);
        TextView nickNameBoy = headerView.findViewById(leftNicknameId);
        TextView nickName = headerView.findViewById(rightNicknameId);
        TextView numberText = headerView.findViewById(numberId);
        if (info != null) {
            //按性别放置左右
            int genderFlag = 0;//0-邀请者男左 接受者女右 1-接受者男左 邀请者女右 2-男同性就邀请者左 接受者右 3女同性
            RankingInfo.RankVo.InviteUserBean inviteUser = info.getInviteUser();
            RankingInfo.RankVo.RecUserBean recUser = info.getRecUser();
            if (inviteUser != null && recUser != null) {
                if (inviteUser.getGenderX() == UserInfo.GENDER_BOY && recUser.getGenderX() == UserInfo.GENDER_GIRL) {
                    genderFlag = 0;
                } else if (inviteUser.getGenderX() == UserInfo.GENDER_GIRL && recUser.getGenderX() == UserInfo.GENDER_BOY) {
                    genderFlag = 1;
                } else if (inviteUser.getGenderX() == UserInfo.GENDER_BOY && recUser.getGenderX() == UserInfo.GENDER_BOY) {
                    genderFlag = 2;
                } else if (inviteUser.getGenderX() == UserInfo.GENDER_GIRL && recUser.getGenderX() == UserInfo.GENDER_GIRL) {
                    genderFlag = 3;
                }
                //头像 性别 昵称
                Drawable boyDrawable = mContext.getResources().getDrawable(R.drawable.user_info_boy);
                boyDrawable.setBounds(0, 0, boyDrawable.getIntrinsicWidth(), boyDrawable.getIntrinsicHeight());
                Drawable girlDrawable = mContext.getResources().getDrawable(R.drawable.user_info_girl);
                girlDrawable.setBounds(0, 0, girlDrawable.getIntrinsicWidth(), girlDrawable.getIntrinsicHeight());
                if (genderFlag != 1) {
                    //邀请者左 接受者右
                    //头像
                    ImageLoadUtils.loadAvatar(context, inviteUser.getAvatarX(), boyAvatar);
                    ImageLoadUtils.loadAvatar(context, recUser.getAvatarX(), imgHead);
                    boyAvatar.setOnClickListener(new CpRankingListAdapter.UserHeadImgClickImpl(inviteUser.getUidX()));
                    imgHead.setOnClickListener(new CpRankingListAdapter.UserHeadImgClickImpl(recUser.getUidX()));
                    //昵称 性别
                    nickNameBoy.setText(inviteUser.getNickX());
                    nickNameBoy.setCompoundDrawables(inviteUser.getGenderX() == UserInfo.GENDER_BOY ? boyDrawable : girlDrawable, null, null, null);
                    nickName.setText(recUser.getNickX());
                    nickName.setCompoundDrawables(recUser.getGenderX() == UserInfo.GENDER_BOY ? boyDrawable : girlDrawable, null, null, null);
                } else {
                    //接受者左 邀请者右
                    ImageLoadUtils.loadAvatar(context, recUser.getAvatarX(), boyAvatar);
                    ImageLoadUtils.loadAvatar(context, inviteUser.getAvatarX(), imgHead);
                    boyAvatar.setOnClickListener(new CpRankingListAdapter.UserHeadImgClickImpl(recUser.getUidX()));
                    imgHead.setOnClickListener(new CpRankingListAdapter.UserHeadImgClickImpl(inviteUser.getUidX()));
                    nickNameBoy.setText(recUser.getNickX());
                    nickNameBoy.setCompoundDrawables(recUser.getGenderX() == UserInfo.GENDER_BOY ? boyDrawable : girlDrawable, null, null, null);
                    nickName.setText(inviteUser.getNickX());
                    nickName.setCompoundDrawables(inviteUser.getGenderX() == UserInfo.GENDER_BOY ? boyDrawable : girlDrawable, null, null, null);
                }

                //距离前一名相差多少
                if (info.getDistance() == 0) {//0时显示0
                    numberText.setText("0");
                } else if (info.getDistance() == -1) {//-1代表不显示
                    numberText.setText("");
                } else if (info.getDistance() >= 10000) {
                    numberText.setText(NumberFormatUtils.formatDoubleDecimalPointWithMax2Digit(info.getDistance() / 10000) + "万");
                } else {
                    numberText.setText(String.valueOf((long) info.getDistance()));
                }
            }
        } else {//数据空的时候各项显示为空
            boyAvatar.setOnClickListener(null);
            imgHead.setOnClickListener(null);
            ImageLoadUtils.loadImage(context, R.drawable.default_cover, boyAvatar);
            ImageLoadUtils.loadImage(context, R.drawable.default_cover, imgHead);
            nickNameBoy.setText("");
            nickNameBoy.setCompoundDrawables(null, null, null, null);
            nickName.setText("");
            nickName.setCompoundDrawables(null, null, null, null);
            numberText.setText("");
        }
    }

    private class UserHeadImgClickImpl implements View.OnClickListener {
        private long userId;

        public UserHeadImgClickImpl(long userId) {
            this.userId = userId;
        }

        @Override
        public void onClick(View v) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo.getUid() == userId) {
                if (userInfo.getCpUser() != null) {
                    //打开自己和cp的CP专属页 有解除CP功能
                    CpExclusivePageActivity.start(mContext, userId, true);
                }
            } else {
                if (userInfo.getCpUser() != null && userInfo.getCpUser().getUid() == userId) {
                    //打开自己和cp的CP专属页 有解除CP功能
                    CpExclusivePageActivity.start(mContext, userId, true);
                } else {
                    //打开他人的CP专属页 没有解除CP功能
                    CpExclusivePageActivity.start(mContext, userId);
                }
            }
        }
    }
}
