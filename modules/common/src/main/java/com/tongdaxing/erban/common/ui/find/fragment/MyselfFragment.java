package com.tongdaxing.erban.common.ui.find.fragment;

import android.os.Bundle;

import com.tongdaxing.erban.common.presenter.find.RecommendPresenter;
import com.tongdaxing.erban.common.ui.find.activity.VoiceGroupDetailActivity;
import com.tongdaxing.erban.common.ui.find.view.IRecommendView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupInfo;

/**
 * Function:查看自己的动态圈
 * Author: Edward on 2019/3/28
 */
@CreatePresenter(RecommendPresenter.class)
public class MyselfFragment extends RecommendFragment implements IRecommendView {

    @Override
    protected void startVoiceGroupDetailPage(VoiceGroupInfo voiceGroupInfo, int index) {
        VoiceGroupDetailActivity.start(getContext(), voiceGroupInfo, index);
    }

    @Override
    public void setRefreshData() {
        Bundle bundle = getArguments();
        long userId = bundle.getLong("userId");
        getMvpPresenter().refreshData("2", String.valueOf(userId));
    }

    @Override
    protected void setLoadMoreData() {
        Bundle bundle = getArguments();
        long userId = bundle.getLong("userId");
        getMvpPresenter().loadMoreData("2", String.valueOf(userId));
    }
}
