package com.tongdaxing.erban.common.room.avroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/5/2.
 * 设置房间话题
 */

public class RoomTopicActivity extends BaseActivity {
    @BindView(R2.id.title_bar)
    TitleBar titleBar;
    @BindView(R2.id.edt_topic_title)
    EditText edtTopicTitle;

    @BindView(R2.id.edt_topic_content)
    EditText edtTopicContent;
    @BindView(R2.id.room_setting_tv_save)
    TextView mTvSave;

    public static void start(Context context) {
        Intent intent = new Intent(context, RoomTopicActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;

        setContentView(R.layout.room_topic_edit);

        ButterKnife.bind(this);

        String roomNotice = roomInfo.getRoomNotice();
        if (!TextUtils.isEmpty(roomNotice))
            edtTopicContent.setText(roomNotice);
        edtTopicTitle.setText(TextUtils.isEmpty(roomInfo.getRoomDesc()) ? "" : roomInfo.getRoomDesc());
        edtTopicTitle.setSelection(edtTopicTitle.getText().length());
        initTitleBar("设置房间话题");
    }

    private void save(String url, String roomDesc, String roomNotice) {


        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null)
            return;
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();

        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("roomDesc", roomDesc);
        requestParam.put("tagId", roomInfo.tagId + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("roomUid", roomInfo.getUid() + "");
        requestParam.put("roomNotice", roomNotice);
        OkHttpManager.getInstance().doGetRequest(UriProvider.JAVA_WEB_URL + url, requestParam, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                toast("网络异常");
            }

            @Override
            public void onResponse(Json json) {
                if (json.num("code") == 200) {
                    toast("保存成功");
                    finish();
                } else {
                    toast(json.str("message", "网络异常"));
                }

            }
        });

    }

    @OnClick(R2.id.room_setting_tv_save)
    public void onSaveClicked() {
        String roomDesc = edtTopicTitle.getText().toString();
        String roomNotice = edtTopicContent.getText().toString();

        if (TextUtils.isEmpty(roomDesc)) {
            toast("标题不能为空");
            return;
        }


        String url;

        if (AvRoomDataManager.get().isRoomOwner()) {
            url = "/room/update";
        } else if (AvRoomDataManager.get().isRoomAdmin()) {
            url = "/room/updateByAdmin";
        } else {
            return;
        }
        save(url, roomDesc, roomNotice);
    }
}
