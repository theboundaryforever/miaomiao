package com.tongdaxing.erban.common.ui.me.wallet.presenter;

import android.content.Context;

import com.google.gson.JsonObject;
import com.tongdaxing.erban.common.ui.me.wallet.model.PayModel;
import com.tongdaxing.erban.common.ui.me.wallet.view.IChargeView;
import com.tcloud.core.log.L;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.pay.bean.ChargeBean;
import com.tongdaxing.xchat_core.pay.bean.PaymentResponseBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.NetworkUtils;

import java.util.HashMap;
import java.util.List;

/**
 * Created by MadisonRong on 05/01/2018.
 */
public class ChargePresenter extends PayPresenter<IChargeView> {

    private static final String TAG = "ChargePresenter";

    public ChargePresenter() {
        this.payModel = new PayModel();
    }

    /**
     * 加载用户信息(显示用户账号和钱包余额)
     */
    public void loadUserInfo() {
        UserInfo userInfo = payModel.getUserInfo();
        if (userInfo != null) {
            getMvpView().setupUserInfo(userInfo);
        }
        refreshWalletInfo(false);
    }

    /**
     * 获取充值产品列表
     */
    public void getChargeList() {
//        execute(payModel.getPayService().getChargeList(1),
//                new CallBack<List<ChargeBean>>() {
//
//                    @Override
//                    public void onSuccess(List<ChargeBean> data) {
//                        getMvpView().buildChargeList(data);
//                    }
//
//                    @Override
//                    public void onFail(int code, String error) {
//                        getMvpView().getChargeListFail(error);
//                    }
//                });
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("channelType", "1");
        ResponseListener listener = new ResponseListener<ServiceResult<List<ChargeBean>>>() {
            @Override
            public void onResponse(ServiceResult<List<ChargeBean>> response) {
                if (response != null && getMvpView() != null) {
                    getMvpView().buildChargeList(response.getData());
                } else {
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if (getMvpView() != null)
                    getMvpView().getChargeListFail(error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getChargeList(),
                        CommonParamUtil.getDefaultHeaders(),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);
    }

    /**
     * 显示充值选项(支付宝或者微信)
     */
    public void showChargeOptionsDialog() {
        if (getMvpView() != null)
            getMvpView().displayChargeOptionsDialog();

    }

    /**
     * 发起充值
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_ALIPAY} 和
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_WX}
     */
    public void doRequestCharge(Context context, String chargeProdId, String payChannel) {
        doRequestCharge(context, chargeProdId,  payChannel, UriProvider.requestCharge());
    }

    /**
     * 发起充值
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_ALIPAY} 和
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_WX}
     */
    public void doRequestCharge(Context context, String chargeProdId, String payChannel, String url) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("clientIp", NetworkUtils.getIPAddress(context));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        L.info(TAG, "doRequestCharge url: %s, param: %s", url, param);
        ResponseListener listener = new ResponseListener<ServiceResult<JsonObject>>() {
            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                L.info(TAG, "onResponse response: %s", response);
                if (response != null && response.getData() != null && response.isSuccess()) {
                    getMvpView().getChargeOrOrderInfo(response.getData().toString(), payChannel);
                } else {
                    L.info(TAG, "onResponse response: %s", response == null ? "请求失败" : response.getErrorMessage());
                    getMvpView().getChargeOrOrderInfoFail(response == null ? "请求失败" : response.getErrorMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                L.info(TAG, "onResponse onErrorResponse: ", error);
                if (getMvpView() != null && error != null) {
                    getMvpView().getChargeOrOrderInfoFail(error.getErrorStr());
                }
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(url,
                CommonParamUtil.getDefaultHeaders(),
                param,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST
        );
    }

    /**
     * 发起汇聚支付
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_JOIN_PAY_WX}
     */
    public void doRequestJoinPayCharge(Context context, String chargeProdId, String payChannel) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        ResponseListener listener = new ResponseListener<ServiceResult<PaymentResponseBean>>() {
            @Override
            public void onResponse(ServiceResult<PaymentResponseBean> response) {
                if (response != null && response.getData() != null && response.isSuccess()) {
                    getMvpView().getJoinPayOrderInfo(response.getData(), payChannel);
                } else {
                    getMvpView().getJoinPayOrderInfoFail(response == null ? "请求失败" : response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if (getMvpView() != null && error != null) {
                    getMvpView().getJoinPayOrderInfoFail(error.getErrorStr());
                }
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.requestJoinPayCharge(),
                CommonParamUtil.getDefaultHeaders(),
                param,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST
        );
    }

    /**
     * 调起微信支付
     *
     * @param context
     * @param data
     */
    public void createJoinPayment(Context context, PaymentResponseBean data) {
        final IWXAPI msgApi = WXAPIFactory.createWXAPI(context, null);
        msgApi.registerApp(data.getAppId());// 将该app注册到微信
        PayReq request = new PayReq();
        request.appId = data.getAppId();
        request.partnerId = data.getPartnerId();
        request.prepayId = data.getPrepayId();
        request.packageValue = "Sign=WXPay";
        request.nonceStr = data.getNonceStr();
        request.timeStamp = data.getTimeStamp();
        request.sign = data.getPaySign();
        msgApi.sendReq(request);
    }
}
