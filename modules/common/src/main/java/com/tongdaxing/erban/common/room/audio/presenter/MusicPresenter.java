package com.tongdaxing.erban.common.room.audio.presenter;

import com.tongdaxing.erban.common.room.audio.view.IMusicView;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.Map;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public class MusicPresenter extends AbstractMvpPresenter<IMusicView> {
    public void reportMusic(String songId, String reason) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("songId", songId);
        params.put("reason", reason);
        OkHttpManager.getInstance().doPostRequest(UriProvider.getSongReport(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    SingleToastUtil.showToast("举报成功!");
                } else {
                    SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.data_exception));
                }
            }
        });
    }
}
