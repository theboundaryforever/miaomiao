package com.tongdaxing.erban.common.ui.me.shopping.fragment;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.shopping.activity.DressUpMallActivity;
import com.tongdaxing.erban.common.ui.me.shopping.activity.GiveGoodsActivity;
import com.tongdaxing.erban.common.ui.me.shopping.adapter.CarAdapter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.DESUtils;
import com.tongdaxing.xchat_framework.util.util.Json;

import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.tongdaxing.xchat_core.UriProvider.JAVA_WEB_URL;
import static com.tongdaxing.xchat_framework.util.util.DESUtils.giftCarSecret;

/**
 * Created by Administrator on 2018/5/7.
 */

public class ShopFragment extends BaseFragment {

    private CarAdapter shopAdapter;
    private RecyclerView recyclerView;
    private SVGAImageView svgaImageview;
    private boolean showVgg = false;

    private String shortUrl;
    private String prizeName;
    private boolean isShowVgg;
    private String prizeParamsName = "";

    public String getPrizeParamsName() {
        return prizeParamsName;
    }

    public void setPrizeParamsName(String prizeParamsName) {
        this.prizeParamsName = prizeParamsName;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public boolean isShowVgg() {
        return isShowVgg;
    }

    public void setShowVgg(boolean showVgg) {
        isShowVgg = showVgg;
    }

    @Override
    public void onFindViews() {
        recyclerView = mView.findViewById(R.id.rv_shop);
        svgaImageview = mView.findViewById(R.id.svga_imageview);
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        svgaImageview.setVisibility(View.GONE);
        svgaImageview.setClearsAfterStop(true);
        svgaImageview.setLoops(1);
        svgaImageview.setCallback(new SVGACallback() {
            @Override
            public void onPause() {

            }

            @Override
            public void onFinished() {
                showVgg = false;
                svgaImageview.setVisibility(View.GONE);
                svgaImageview.stopAnimation();
            }

            @Override
            public void onRepeat() {

            }

            @Override
            public void onStep(int i, double v) {

            }
        });
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        shopAdapter = new CarAdapter(getContext());

        shopAdapter.setVggAction(new CarAdapter.VggAction() {
            @Override
            public void showVgg(String url) {
                try {
                    url = DESUtils.DESAndBase64Decrypt(url, giftCarSecret);
                    drawSvgaEffect(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        shopAdapter.setPayAction(new CarAdapter.PayAction() {
            @Override
            public void buyCar(int type, String carId, String carName) {

                showBottonMenu(type, carId, carName);


            }
        });
        shopAdapter.prizeParamasName = getPrizeParamsName();
        shopAdapter.isShowSvga = isShowVgg();
        recyclerView.setAdapter(shopAdapter);

        if (isShowVgg()) {
            getDialogManager().showProgressDialog(getContext(), "加载中...");
        }


        getData();
    }

    private void drawSvgaEffect(String url) throws Exception {
        if (showVgg || !isShowVgg()) {
            return;
        }
        showVgg = true;
        SVGAParser parser = new SVGAParser(getContext());
        parser.parse(new URL(url), new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity videoItem) {

                SVGADrawable drawable = new SVGADrawable(videoItem);
                svgaImageview.setImageDrawable(drawable);
                svgaImageview.startAnimation();
                svgaImageview.setVisibility(View.VISIBLE);
                ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(svgaImageview, "alpha", 0.0F, 2.0F).setDuration(800);
                objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
                objectAnimator1.start();
            }

            @Override
            public void onError() {
                showVgg = false;
            }
        });
    }

    private void getData() {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        requestParam.put("pageNum", "1");
        requestParam.put("pageSize", "50");
        OkHttpManager.getInstance().doGetRequest(JAVA_WEB_URL + getShortUrl() + "/list", requestParam, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getDialogManager() != null) {
                    getDialogManager().dismissDialog();
                }

                toast("网络异常");
            }

            @Override
            public void onResponse(Json response) {

                try {
                    if (getDialogManager() != null)
                        getDialogManager().dismissDialog();
                } catch (Exception e) {
                    return;
                }

                if (response.num("code") != 200) {
                    toast(response.str("message", "网络异常"));
                    return;
                }


                List<Json> data = response.jlist("data");
                shopAdapter.setNewData(data);

            }
        });

    }

    private boolean checkHasSelect() {
        List<Json> data = shopAdapter.getData();
        for (Json json : data) {
            if (json.num("isPurse") == 2) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (shopAdapter != null) {
            String selectId = shopAdapter.selectId;
            saveUserCar(selectId);
        }
    }

    public void saveUserCar(String selectId) {


        if (TextUtils.isEmpty(selectId)) {
            return;
        }
//        if (checkHasSelect()) {
//            return;
//        }


        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        //如果为空的话传-1表明不选座驾
        requestParam.put(getPrizeParamsName() + "Id", TextUtils.isEmpty(selectId) ? "-1" : selectId);

        OkHttpManager.getInstance().doGetRequest(JAVA_WEB_URL + getShortUrl() + "/use", requestParam, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                toast("网络异常");
            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") == 200) {
                    CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                }
            }
        });

    }

    private void showBottonMenu(int type, String carId, String carName) {
        List<ButtonItem> buttonItems = new ArrayList<>();

        ButtonItem buy = ButtonItemFactory.createMsgBlackListItem("为自己购买", new ButtonItemFactory.OnItemClick() {
            @Override
            public void itemClick() {
                getDialogManager().showOkCancelDialog(
                        "确认购买" + getPrizeName() + "“" + carName + "”", true,
                        new DialogManager.AbsOkDialogListener() {
                            @Override
                            public void onOk() {

                                getDialogManager().showProgressDialog(getContext(), "请稍后");
                                Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
                                requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
                                requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
                                requestParam.put("type", type + "");
                                requestParam.put(getPrizeParamsName() + "Id", carId);
                                OkHttpManager.getInstance().doGetRequest(JAVA_WEB_URL + getShortUrl() + "/purse", requestParam, new OkHttpManager.MyCallBack<Json>() {
                                    @Override
                                    public void onError(Exception e) {

                                        getDialogManager().dismissDialog();
                                        toast("网络异常");
                                    }

                                    @Override
                                    public void onResponse(Json response) {
                                        getDialogManager().dismissDialog();
                                        if (response.num("code") != 200) {
                                            toast(response.str("message", "网络异常"));
                                            return;
                                        }
                                        toast(response.str("message", "购买成功"));
                                        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                                        getData();


                                    }
                                });

                            }
                        });

            }
        });

        buttonItems.add(buy);

        ButtonItem giveFriend = ButtonItemFactory.createMsgBlackListItem("赠送给ta", new ButtonItemFactory.OnItemClick() {
            @Override
            public void itemClick() {
                Intent intent = new Intent(mContext, GiveGoodsActivity.class);
                intent.putExtra("carName", carName);
                intent.putExtra("goodsId", carId);
                intent.putExtra("type", "car".equals(prizeParamsName) ? DressUpMallActivity.DRESS_CAR : DressUpMallActivity.DRESS_HEADWEAR);
                mContext.startActivity(intent);
            }
        });

        buttonItems.add(giveFriend);

        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.shop_fragment;
    }
}
