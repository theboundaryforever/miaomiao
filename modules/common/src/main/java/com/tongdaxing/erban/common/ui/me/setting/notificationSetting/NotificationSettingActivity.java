package com.tongdaxing.erban.common.ui.me.setting.notificationSetting;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.presenter.me.setting.MessageNotDisturbPresenter;
import com.tongdaxing.erban.common.ui.me.setting.utils.NotificationPermissionUtil;
import com.tongdaxing.erban.common.ui.me.setting.vew.IMsgNotDisturbView;
import com.tongdaxing.erban.common.ui.widget.dialog.CommonDialog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.UserEvent;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@CreatePresenter(MessageNotDisturbPresenter.class)
public class NotificationSettingActivity extends BaseMvpActivity<IMsgNotDisturbView, MessageNotDisturbPresenter> implements IMsgNotDisturbView {

    @BindView(R2.id.notification_permission)
    TextView notificationPermission;
    @BindView(R2.id.notification_permission_iv)
    ImageView notificationPermissionIv;
    @BindView(R2.id.notification_attention_iv)
    ImageView notificationAttentionIv;
    @BindView(R2.id.notification_room)
    TextView notificationRoom;
    @BindView(R2.id.notification_room_iv)
    ImageView notificationRoomIv;
    @BindView(R2.id.notification_audio_match)
    TextView notificationAudioMatch;
    @BindView(R2.id.notification_audio_match_iv)
    ImageView notificationAudioMatchIv;
    @BindView(R2.id.notification_voice_group_iv)
    ImageView notificationVoiceGroupIv;
    @BindView(R2.id.notification_p2p_disturb)
    TextView notificationP2pDisturb;
    @BindView(R2.id.notification_p2p_disturb_tv)
    TextView notificationP2pDisturbTv;
    @BindView(R2.id.notification_p2p_disturb_iv)
    ImageView notificationP2pDisturbIv;
    @BindView(R2.id.notification_p2p_disturb_content)
    ConstraintLayout notificationP2pDisturbContent;

    private int disturbState = 0;
    private boolean isFirstLoad = true;
    private boolean lastPermissionIsOpen;//上一次通知权限是否开启
    private long userUid = 0;
    private boolean isSysNoticeOptionOpen;
    private boolean isAttentionOpen;
    private boolean isRoomOpen;
    private boolean isAudioMatchOpen;
    private boolean isVoiceGroupOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_setting);
        ButterKnife.bind(this);
        CoreUtils.register(this);
        initTitleBar(getString(R.string.nitofy_setting));
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CoreUtils.unregister(this);
    }

    private void initView() {
        notificationP2pDisturbContent.setEnabled(false);
        UserInfo info = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (info != null) {
            userUid = info.getUid();
            if (info.getExperLevel() >= 30) {
                notificationP2pDisturbContent.setVisibility(View.VISIBLE);
            }
        }
        getDialogManager().showProgressDialog(this, "加载中...");
        getMvpPresenter().getMsgDisturbState();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getUserInfo(UserEvent.OnUserInfoUpdate onUserInfoUpdate) {
        boolean result = NotificationPermissionUtil.areNotificationsEnabled(getApplicationContext());
        UserInfo userInfo = onUserInfoUpdate.getUserInfo();
        notificationAttentionIv.setEnabled(result);
        notificationRoomIv.setEnabled(result);
        notificationAudioMatchIv.setEnabled(result);
        notificationVoiceGroupIv.setEnabled(result);

        isSysNoticeOptionOpen = userInfo.getSysNoticeOption() == null || userInfo.getSysNoticeOption() == 1;
        isAttentionOpen = userInfo.getFocusNoticeOption() == null || userInfo.getFocusNoticeOption() == 1;
        isRoomOpen = userInfo.getRoomNoticeOption() == null || userInfo.getRoomNoticeOption() == 1;
        isAudioMatchOpen = userInfo.getSoundNoticeOption() == null || userInfo.getSoundNoticeOption() == 1;
        isVoiceGroupOpen = userInfo.getMonentNoticeOption() == null || userInfo.getMonentNoticeOption() == 1;
        if (result) {
            if (isSysNoticeOptionOpen) {
                //通知权限显示为开
                notificationPermissionIv.setImageResource(R.drawable.notification_setting_permission_ic_on);
                //其他4个根据字段值显示开关

                notificationAttentionIv.setImageResource(isAttentionOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off);
                notificationRoomIv.setImageResource(isRoomOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off);
                notificationAudioMatchIv.setImageResource(isAudioMatchOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off);
                notificationVoiceGroupIv.setImageResource(isVoiceGroupOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off);
            } else {
                //通知权限示为关
                notificationPermissionIv.setImageResource(R.drawable.notification_setting_permission_ic_off);
                //其他4个也显示为关
                notificationAttentionIv.setImageResource(R.drawable.notification_setting_permission_ic_off);
                notificationRoomIv.setImageResource(R.drawable.notification_setting_permission_ic_off);
                notificationAudioMatchIv.setImageResource(R.drawable.notification_setting_permission_ic_off);
                notificationVoiceGroupIv.setImageResource(R.drawable.notification_setting_permission_ic_off);
            }
        } else {
            //系统通知权限关闭
            notificationPermissionIv.setImageResource(R.drawable.notification_setting_permission_ic_off);
            //置灰
            notificationAttentionIv.setImageResource(isAttentionOpen
                    ? R.drawable.notification_setting_permission_ic_on_disabled
                    : R.drawable.notification_setting_permission_ic_off_disabled);
            notificationRoomIv.setImageResource(isRoomOpen
                    ? R.drawable.notification_setting_permission_ic_on_disabled
                    : R.drawable.notification_setting_permission_ic_off_disabled);
            notificationAudioMatchIv.setImageResource(isAudioMatchOpen
                    ? R.drawable.notification_setting_permission_ic_on_disabled
                    : R.drawable.notification_setting_permission_ic_off_disabled);
            notificationVoiceGroupIv.setImageResource(isVoiceGroupOpen
                    ? R.drawable.notification_setting_permission_ic_on_disabled
                    : R.drawable.notification_setting_permission_ic_off_disabled);
            //弹出弹窗提示授予权限
            showNoPermissionDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean result = NotificationPermissionUtil.areNotificationsEnabled(getApplicationContext());
        if (result) {
            notificationPermission.setText(R.string.notification_permission);
        } else {
            notificationPermission.setText(R.string.notification_no_permission);
        }
        if (isFirstLoad) {
            lastPermissionIsOpen = result;
            isFirstLoad = false;

            //请求用户信息 获取通知设置
            CoreManager.getCore(IUserCore.class).requestUserInfo(userUid);
        } else {
            if (result) {
                //通知权限显示开
                notificationPermissionIv.setImageResource(R.drawable.notification_setting_permission_ic_on);
                if (!lastPermissionIsOpen) {
                    //本来权限是关的
                    //其他4个根据字段值显示开关，去掉【置灰】
                    notificationAttentionIv.setImageResource(isAttentionOpen
                            ? R.drawable.notification_setting_permission_ic_on
                            : R.drawable.notification_setting_permission_ic_off);
                    notificationRoomIv.setImageResource(isRoomOpen
                            ? R.drawable.notification_setting_permission_ic_on
                            : R.drawable.notification_setting_permission_ic_off);
                    notificationAudioMatchIv.setImageResource(isAudioMatchOpen
                            ? R.drawable.notification_setting_permission_ic_on
                            : R.drawable.notification_setting_permission_ic_off);
                    notificationVoiceGroupIv.setImageResource(isVoiceGroupOpen
                            ? R.drawable.notification_setting_permission_ic_on
                            : R.drawable.notification_setting_permission_ic_off);
                    notificationAttentionIv.setEnabled(true);
                    notificationRoomIv.setEnabled(true);
                    notificationAudioMatchIv.setEnabled(true);
                    notificationVoiceGroupIv.setEnabled(true);
                }
            } else {
                if (lastPermissionIsOpen) {
                    //本来权限是开的
                    //通知权限显示为关
                    notificationPermissionIv.setImageResource(R.drawable.notification_setting_permission_ic_off);
                    //其他4个根据字段值显示开关，但是【置灰】
                    notificationAttentionIv.setImageResource(isAttentionOpen
                            ? R.drawable.notification_setting_permission_ic_on_disabled
                            : R.drawable.notification_setting_permission_ic_off_disabled);
                    notificationRoomIv.setImageResource(isRoomOpen
                            ? R.drawable.notification_setting_permission_ic_on_disabled
                            : R.drawable.notification_setting_permission_ic_off_disabled);
                    notificationAudioMatchIv.setImageResource(isAudioMatchOpen
                            ? R.drawable.notification_setting_permission_ic_on_disabled
                            : R.drawable.notification_setting_permission_ic_off_disabled);
                    notificationVoiceGroupIv.setImageResource(isVoiceGroupOpen
                            ? R.drawable.notification_setting_permission_ic_on_disabled
                            : R.drawable.notification_setting_permission_ic_off_disabled);
                    notificationAttentionIv.setEnabled(false);
                    notificationRoomIv.setEnabled(false);
                    notificationAudioMatchIv.setEnabled(false);
                    notificationVoiceGroupIv.setEnabled(false);
                }
            }
            lastPermissionIsOpen = result;
        }
    }

    private void showNoPermissionDialog() {
        CommonDialog commonDialog = new CommonDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", getString(R.string.notification_no_permission_dialog_title));
        commonDialog.setArguments(bundle);
        commonDialog.show(getSupportFragmentManager(), "");
        commonDialog.setmOnClickListener(new CommonDialog.OnClickListener() {
            @Override
            public void onOKClick() {
                NotificationPermissionUtil.requestNotify(getApplicationContext());
            }

            @Override
            public void onCancelClick() {

            }
        });
    }

    private void showPermissionCloseDialog() {
        CommonDialog commonDialog = new CommonDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", getString(R.string.notification_permission_close_dialog_title));
        commonDialog.setArguments(bundle);
        commonDialog.show(getSupportFragmentManager(), "");
        commonDialog.setmOnClickListener(new CommonDialog.OnClickListener() {
            @Override
            public void onOKClick() {
                //访问接口置为关
                getMvpPresenter().setNoticeOption(NotificationSettingModel.sysNoticeOption, 0);
                //访问接口把4个开关也置为关
                getMvpPresenter().setNoticeOption(NotificationSettingModel.all, 0);
            }

            @Override
            public void onCancelClick() {

            }
        });
    }

    @OnClick(R2.id.notification_p2p_disturb_content)
    public void onViewClicked() {
        List<ButtonItem> items = new ArrayList<>();
        if (disturbState != 1) {
            ButtonItem buttonItem1 = new ButtonItem("所有对象", new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    getDialogManager().showProgressDialog(NotificationSettingActivity.this, "正在修改...");
                    getMvpPresenter().changeMsgDisturbState(1);
                }
            });
            items.add(buttonItem1);
        }
        if (disturbState != 2) {
            ButtonItem buttonItem2 = new ButtonItem("LV.10以下", new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    getDialogManager().showProgressDialog(NotificationSettingActivity.this, "正在修改...");
                    getMvpPresenter().changeMsgDisturbState(2);
                }
            });
            items.add(buttonItem2);
        }
        if (disturbState != 0) {
            ButtonItem buttonItem = new ButtonItem("关闭", new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    getDialogManager().showProgressDialog(NotificationSettingActivity.this, "正在修改...");
                    getMvpPresenter().changeMsgDisturbState(0);
                }
            });
            items.add(buttonItem);
        }
        getDialogManager().showCommonPopupDialog(items, "取消");
    }

    @Override
    public void getDisturbStateSuccess(int state) {
        getDialogManager().dismissDialog();
        disturbState = state;
        notificationP2pDisturbContent.setEnabled(true);
        setDisturbState(state);
    }

    @Override
    public void getDisturbStateFail(String message) {
        getDialogManager().dismissDialog();
        SingleToastUtil.showToast(message);
    }

    @Override
    public void saveDisturbStateSuccess(int state) {
        getDialogManager().dismissDialog();
        disturbState = state;
        setDisturbState(state);
    }

    @Override
    public void saveDisturbStateFail(String message) {
        getDialogManager().dismissDialog();
        SingleToastUtil.showToast(message);
    }

    private void setDisturbState(int state) {
        switch (state) {
            case 0://关闭
                notificationP2pDisturbTv.setText("关闭");
                break;
            case 1://所有人
                notificationP2pDisturbTv.setText("所有人");
                break;
            case 2://10级以下
                notificationP2pDisturbTv.setText("LV.10以下");
                break;
        }
    }

    @Override
    public void saveNotifySettingSuccess(String configId, int configValue) {
        boolean result = configValue == 1;
        switch (configId) {
            case NotificationSettingModel.sysNoticeOption:
                isSysNoticeOptionOpen = result;
                notificationPermissionIv.setImageResource(result
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off);
                break;
            case NotificationSettingModel.all:
                isAttentionOpen = result;
                isRoomOpen = result;
                isAudioMatchOpen = result;
                isVoiceGroupOpen = result;
                notificationAttentionIv.setImageResource(isAttentionOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off_disabled);
                notificationRoomIv.setImageResource(isRoomOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off_disabled);
                notificationAudioMatchIv.setImageResource(isAudioMatchOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off_disabled);
                notificationVoiceGroupIv.setImageResource(isVoiceGroupOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off_disabled);
                notificationAttentionIv.setEnabled(result);
                notificationRoomIv.setEnabled(result);
                notificationAudioMatchIv.setEnabled(result);
                notificationVoiceGroupIv.setEnabled(result);
                break;
            case NotificationSettingModel.focusNoticeOption:
                isAttentionOpen = result;
                notificationAttentionIv.setImageResource(isAttentionOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off);
                break;
            case NotificationSettingModel.roomNoticeOption:
                isRoomOpen = result;
                notificationRoomIv.setImageResource(isRoomOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off);
                break;
            case NotificationSettingModel.soundNoticeOption:
                isAudioMatchOpen = result;
                notificationAudioMatchIv.setImageResource(isAudioMatchOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off);
                break;
            case NotificationSettingModel.monentNoticeOption:
                isVoiceGroupOpen = result;
                notificationVoiceGroupIv.setImageResource(isVoiceGroupOpen
                        ? R.drawable.notification_setting_permission_ic_on
                        : R.drawable.notification_setting_permission_ic_off);
                break;
        }
    }

    @Override
    public void saveNotifySettingFail(String message) {
        SingleToastUtil.showToast(message);
    }

    @OnClick(R2.id.notification_permission_iv)
    public void onNotificationPermissionIvClicked() {
        boolean result = NotificationPermissionUtil.areNotificationsEnabled(getApplicationContext());
        if (result) {
            if (isSysNoticeOptionOpen) {
                showPermissionCloseDialog();
            } else {
                //访问接口置为开
                getMvpPresenter().setNoticeOption(NotificationSettingModel.sysNoticeOption, 1);
                getMvpPresenter().setNoticeOption(NotificationSettingModel.all, 1);
            }
        } else {
            //通知权限显示关
            notificationPermissionIv.setImageResource(R.drawable.notification_setting_permission_ic_off);
            //没有通知权限时，打开系统页面请求权限
            NotificationPermissionUtil.requestNotify(getApplicationContext());
        }
    }

    @OnClick({R2.id.notification_attention_iv, R2.id.notification_room_iv, R2.id.notification_audio_match_iv, R2.id.notification_voice_group_iv})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.notification_attention_iv) {
            getMvpPresenter().setNoticeOption(NotificationSettingModel.focusNoticeOption, isAttentionOpen ? 0 : 1);

        } else if (i == R.id.notification_room_iv) {
            getMvpPresenter().setNoticeOption(NotificationSettingModel.roomNoticeOption, isRoomOpen ? 0 : 1);

        } else if (i == R.id.notification_audio_match_iv) {
            getMvpPresenter().setNoticeOption(NotificationSettingModel.soundNoticeOption, isAudioMatchOpen ? 0 : 1);

        } else if (i == R.id.notification_voice_group_iv) {
            getMvpPresenter().setNoticeOption(NotificationSettingModel.monentNoticeOption, isVoiceGroupOpen ? 0 : 1);

        }
    }
}
