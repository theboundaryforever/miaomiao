package com.tongdaxing.erban.common.room.lover.weekcp.topic;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.erban.ui.baseview.BaseFragment;
import com.tcloud.core.CoreUtils;

import java.util.List;

/**
 * Created by Chen on 2018/11/20.
 */
public class RoomSoulTopicPageAdapter extends FragmentPagerAdapter {
    private List<PagerContent> mContents;

    public RoomSoulTopicPageAdapter(FragmentManager fm, List<PagerContent> contents) {
        super(fm);
        this.mContents = contents;
    }

    @Override
    public BaseFragment getItem(int position) {
        try {
            PagerContent content = mContents.get(position);
            BaseFragment baseFragment = content.mFragClass.getConstructor().newInstance();
            baseFragment.setArguments(content.mBundle);
            return baseFragment;
        } catch (Exception e) {
            CoreUtils.crashIfDebug(e, "getItem: %d", position);
        }
        return null;
    }

    @Override
    public int getCount() {
        if (mContents != null) {
            return mContents.size();
        }
        return 0;
    }

    public static class PagerContent {
        public String mCate;
        private Class<? extends BaseFragment> mFragClass;
        private Bundle mBundle;

        public PagerContent(Class<? extends BaseFragment> fragClass, Bundle bundle, String cate) {
            this.mFragClass = fragClass;
            this.mBundle = bundle;
            this.mCate = cate;
        }

        public PagerContent(Class<? extends BaseFragment> fragClass, Bundle bundle) {
            this.mFragClass = fragClass;
            this.mBundle = bundle;
        }
    }
}
