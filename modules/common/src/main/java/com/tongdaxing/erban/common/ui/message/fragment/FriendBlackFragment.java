package com.tongdaxing.erban.common.ui.message.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.friend.FriendService;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.message.adapter.FriendBlackAdapter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;

import java.util.List;

/**
 * 黑名单列表
 *
 * @author zwk
 */
public class FriendBlackFragment extends BaseFragment implements FriendBlackAdapter.RoomBlackDelete, SwipeRefreshLayout.OnRefreshListener {
    public static String TAG = "FriendBlackFragment";
    private SwipeRefreshLayout srlRefresh;
    private RecyclerView recyclerView;
    private FriendBlackAdapter blackAdapter;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_black_list;
    }

    @Override
    public void onFindViews() {
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);
        srlRefresh = mView.findViewById(R.id.srl_refresh);
        blackAdapter = new FriendBlackAdapter(R.layout.item_rv_friend_black_list);
        blackAdapter.setRoomBlackDelete(this);
        blackAdapter.setOnItemClickListener((adapter, view, position) -> {
            NimUserInfo nimUserInfo = blackAdapter.getItem(position);
            if (nimUserInfo != null) {
                try {
                    long l1 = Long.valueOf(nimUserInfo.getAccount());
                    NewUserInfoActivity.start(getActivity(), l1);
                } catch (Exception e) {
                    toast("数据错误！");
                }
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(blackAdapter);
    }

    @Override
    public void onSetListener() {
        srlRefresh.setOnRefreshListener(this);
    }

    @Override
    public void initiate() {
        getData();
    }

    private void getData() {
        //先获取黑名单账号列表
        List<String> blackList = NIMClient.getService(FriendService.class).getBlackList();
        if (ListUtils.isListEmpty(blackList)) {
            srlRefresh.setRefreshing(false);
            showNoData();
            //显示空布局
            return;
        }
        //在获取黑名单账号信息
        NimUserInfoCache.getInstance().getUserInfoFromRemote(blackList, new RequestCallback<List<NimUserInfo>>() {
            @Override
            public void onSuccess(List<NimUserInfo> param) {
                hideStatus();
                srlRefresh.setRefreshing(false);
                if (blackAdapter != null)
                    blackAdapter.setNewData(param);
            }

            @Override
            public void onFailed(int code) {
                hideStatus();
                srlRefresh.setRefreshing(false);
                showNetworkErr();
            }

            @Override
            public void onException(Throwable exception) {
                hideStatus();
                srlRefresh.setRefreshing(false);
                showNetworkErr();
            }
        });
    }

    @Override
    public void onDeleteClick(NimUserInfo item) {
        getDialogManager().showOkCancelDialog(
                "是否将" + item.getName() + "移除黑名单列表？",
                true,
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        removeBlackUser(item);
                    }
                });
    }

    private void removeBlackUser(NimUserInfo item) {
        NIMClient.getService(FriendService.class).removeFromBlackList(item.getAccount()).setCallback(new RequestCallback<Void>() {

            @Override
            public void onSuccess(Void param) {
                getData();
            }

            @Override
            public void onFailed(int code) {

            }

            @Override
            public void onException(Throwable exception) {

            }
        });
    }

    @Override
    public void onRefresh() {
        getData();
    }
}
