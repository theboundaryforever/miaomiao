package com.tongdaxing.erban.common.ui.cpexclusive.editSign;

/**
 * <p>
 * Created by zhangjian on 2019/4/30.
 */
public class CpExclusivePageARouterPath {
    public static final String CP_EXCLUSIVE_PAGE_EDIT_SIGN_PATH = "/erban_client/CpEditSignDialogFragment";
}
