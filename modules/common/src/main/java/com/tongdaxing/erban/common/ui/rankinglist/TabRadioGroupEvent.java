package com.tongdaxing.erban.common.ui.rankinglist;

/**
 * <p>
 * Created by zhangjian on 2019/7/2.
 */
public class TabRadioGroupEvent {

    public static class OnTabClick {
        private int rankingType;
        private int position;

        public int getPosition() {
            return position;
        }

        public OnTabClick(int rankingType, int position) {
            this.rankingType = rankingType;
            this.position = position;
        }

        public int getRankingType() {
            return rankingType;
        }
    }
}
