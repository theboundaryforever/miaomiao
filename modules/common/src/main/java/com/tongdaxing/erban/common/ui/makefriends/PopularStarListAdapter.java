package com.tongdaxing.erban.common.ui.makefriends;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.ui.widget.AudioPlayView;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

/**
 * <p> 魅力之星adapter </p>
 */
public class PopularStarListAdapter extends BaseQuickAdapter<UserInfo, BaseViewHolder> {
    private Context context;

    private View.OnClickListener onToFindHerBtnClickListener;

    private SparseArray<AudioPlayView> audioPlayViews;//存起来，用来释放资源用

    public PopularStarListAdapter(int layoutRes, Context context) {
        super(layoutRes);
        this.context = context;
        audioPlayViews = new SparseArray<>();
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, UserInfo popularStarInfo) {
        //去掉语音播放
        AudioPlayView audioPlayView = viewHolder.getView(R.id.audio_play_view);
        audioPlayViews.put(viewHolder.getAdapterPosition(), audioPlayView);
        if (TextUtils.isEmpty(popularStarInfo.getUserVoice())) {
            audioPlayView.setVisibility(View.GONE);
        } else {
            audioPlayView.setVisibility(View.VISIBLE);
            audioPlayView.setAudioDataSource(popularStarInfo.getVoiceDura(), popularStarInfo.getUserVoice());
        }
        ((TextView) viewHolder.getView(R.id.nickname)).setText(popularStarInfo.getNick());
        ((TextView) viewHolder.getView(R.id.charm_level)).setText(String.valueOf(popularStarInfo.getCharmLevel()));
        ((TextView) viewHolder.getView(R.id.user_desc)).setText(popularStarInfo.getUserDesc());
        ImageLoadUtils.loadBigAvatar(context, popularStarInfo.getAvatar(), viewHolder.getView(R.id.avatar), true);
        ((ImageView) viewHolder.getView(R.id.ic_gender)).setImageResource(popularStarInfo.getGender() == 2 ? R.drawable.icon_female : R.drawable.icon_man);
        viewHolder.getView(R.id.to_find_her).setTag(String.valueOf(popularStarInfo.getUid()));
        viewHolder.getView(R.id.to_find_her).setOnClickListener(onToFindHerBtnClickListener);
    }

    public void setOnToFindHerBtnClickListener(View.OnClickListener onToFindHerBtnClickListener) {
        this.onToFindHerBtnClickListener = onToFindHerBtnClickListener;
    }

    /**
     * 释放播放器资源（放onDestroy前面，否则有泄漏风险）
     */
    public void releaseAudioPlayers() {
        for (int i = 0; i < audioPlayViews.size(); i++) {
            AudioPlayView audioPlayView = audioPlayViews.valueAt(i);
            audioPlayView.release();
        }
    }

    /**
     * 停止播放音频
     */
    public void stopAudioPlayers() {
        for (int i = 0; i < audioPlayViews.size(); i++) {
            AudioPlayView audioPlayView = audioPlayViews.valueAt(i);
            audioPlayView.stop();
        }
    }
}
