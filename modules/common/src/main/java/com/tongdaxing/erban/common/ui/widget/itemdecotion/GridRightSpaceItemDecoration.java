package com.tongdaxing.erban.common.ui.widget.itemdecotion;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * 等间隔grid的ItemDecoration
 * 这里不设置最右边的space，最右边的间隔在RecyclerView布局设
 */
public class GridRightSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int itemHorizontalSpace;
    private int totalNum;

    /**
     * @param itemHorizontalSpace 横间隔
     */
    public GridRightSpaceItemDecoration(int itemHorizontalSpace, int totalNum) {
        this.itemHorizontalSpace = itemHorizontalSpace;
        this.totalNum = totalNum;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int itemPosition = parent.getChildLayoutPosition(view);
        if (itemPosition >= 0 && itemPosition < totalNum - 1) {
            outRect.right = itemHorizontalSpace;
        }
    }
}
