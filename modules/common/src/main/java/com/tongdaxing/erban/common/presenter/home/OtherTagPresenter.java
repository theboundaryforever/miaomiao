package com.tongdaxing.erban.common.presenter.home;

import android.os.Handler;
import android.os.Looper;

import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.model.room.RoomModel;
import com.tongdaxing.erban.common.ui.home.fragment.OtherTagFragment;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

public class OtherTagPresenter extends AbstractMvpPresenter<IOtherTagView> {
    private static final String TAG = "OtherTagPresenter";
    private RoomModel dataSource;
    private int currPage = Constants.PAGE_START;
    private int tagId;//0为热门

    private Handler mHandler = new Handler(Looper.getMainLooper());//避免出现IllegalStateException：RecyclerView is computing a layout or scrolling

    private List<HomeRoom> mHomeRoomDatas;
    private boolean isFirst = true;

    public OtherTagPresenter() {
        dataSource = new RoomModel();
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    /**
     * 刷新数据
     */
    public void refreshData() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                getData(Constants.PAGE_START);
            }
        });
    }

    public void loadMoreData() {
        L.debug(TAG, "loadMoreData page = %d", currPage + 1);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                getData(currPage + 1);
            }
        });
    }

    private void getData(int page) {
        dataSource.getOtherTabData(tagId, page, new HttpRequestCallBack<List<HomeRoom>>() {
            @Override
            public void onFinish() {

            }

            @Override
            public void onSuccess(String message, List<HomeRoom> response) {
                currPage = page;
                int bannerPosition = -1;
                if (response == null) {
                    response = new ArrayList<>();
                }
                getMvpView().setupListView(page == Constants.PAGE_START, response, null, bannerPosition);
                if (tagId == BaseApp.getHomeLiveTagID() && page == Constants.PAGE_START) {//直播页第一页才拿banner数据
                    mHomeRoomDatas = response;
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            getBannerData();
                        }
                    });
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                getMvpView().setupFailView(page == Constants.PAGE_START);
            }
        });
    }

    private void getBannerData() {
        dataSource.getLiveBanner(new HttpRequestCallBack<HomeRoom>() {
            @Override
            public void onFinish() {

            }

            @Override
            public void onSuccess(String message, HomeRoom response) {
                if (response != null) {
                    if (response.getBannerInfos() == null || response.getBannerInfos().size() == 0) {
                        //没数据不显示banner
                        return;
                    }
                    response.setType(-1);
                    response.setItemType(1);
                    int bannerPosition = insertBanner(mHomeRoomDatas, response.getPos());
                    getMvpView().setupListView(false, mHomeRoomDatas, response, bannerPosition);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                getMvpView().setupFailView(true);
            }
        });
    }

    @Override
    public void onResumePresenter() {
        super.onResumePresenter();
        if (isFirst && CoreManager.getCore(IAuthCore.class).isLogin()) {
            isFirst = false;
            getMvpView().setRefreshing(true);
            refreshData();
        }
    }

    /**
     * 加进去的时候考虑不满一行的情况
     *
     * @param roomList
     * @param index    从0开始 插入到第几行 后台给的数据为从1开始 所以有--index
     * @return bannerPosition banner要插入的位置
     */
    public int insertBanner(List<HomeRoom> roomList, int index) {
        int roomSize = roomList.size();
        int dataRowNum = (int) Math.ceil(1f * roomSize / OtherTagFragment.ITEM_COUNT_BY_ROW);//数据的总行数
        int bannerPosition;
        if (index < 1) {
            index = 1;
        }
        if (--index >= 0 && index < dataRowNum) {
            bannerPosition = index * OtherTagFragment.ITEM_COUNT_BY_ROW;//插入到第0至第dataRowNum-1行
        } else {
            //其他情况都这样处理：如果最后一行不满一行，放到倒数第二行，满一行放到最后一行
            bannerPosition = roomSize - roomSize % OtherTagFragment.ITEM_COUNT_BY_ROW;
        }
        return bannerPosition;
    }

    @Override
    public void onDestroyPresenter() {
        super.onDestroyPresenter();
        mHandler.removeCallbacksAndMessages(null);
    }
}
