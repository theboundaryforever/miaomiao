package com.tongdaxing.erban.common.ui.widget.dialog;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.JsonObject;
import com.juxiao.library_ui.widget.ViewHolder;
import com.juxiao.library_ui.widget.dialog.BaseDialog;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.pingplusplus.android.Pingpp;
import com.tcloud.core.log.L;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.pay.bean.PaymentResponseBean;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.NetworkUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Function:一元爆音
 * Author: Edward on 2019/1/2
 */
public class OneAudioDialog extends BaseDialog implements View.OnClickListener {
    private static final String TAG = "OneAudioDialog";
    /**
     * 充值ID
     */
    private final String RECHARGE_ID = "28";
    private ImageView ivClose, ivRightNowGet;

    @Override
    public void convertView(ViewHolder viewHolder) {
        ivClose = viewHolder.getView(R.id.iv_close);
        ivRightNowGet = viewHolder.getView(R.id.iv_right_now_get);
        ivClose.setOnClickListener(this);
        ivRightNowGet.setOnClickListener(this);
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_one_audio;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.iv_close) {
            dismiss();

        } else if (i == R.id.iv_right_now_get) {
            startPay();

        }
    }

    private void startPay() {
        ButtonItem buttonItem = new ButtonItem(getString(R.string.charge_alipay),
                () -> {
                    L.info(TAG, "charge channel: %d", Constants.PAY_ALIPAY_CHANNEL);
                    if (Constants.PAY_ALIPAY_CHANNEL == Constants.PAY_CHANNEL_ECPSS) {
                        //汇潮支付
                        doRequestCharge(getContext(), RECHARGE_ID, Constants.CHARGE_ALIPAY, UriProvider.requestEcpssCharge());
                    } else {
                        //ping++支付
                        doRequestCharge(getContext(), RECHARGE_ID, Constants.CHARGE_ALIPAY);
                    }
                    //ping++支付
                    doRequestCharge(getContext(), RECHARGE_ID, Constants.CHARGE_ALIPAY);
                });
        ButtonItem buttonItem1 = new ButtonItem(getString(R.string.charge_webchat),
                () -> {
                    if (Constants.PAY_CHANNEL == 2) {
                        //汇聚支付
                        doRequestJoinPayCharge(getContext(), RECHARGE_ID, Constants.CHARGE_JOIN_PAY_WX);
                    } else {
                        //ping++支付
                        doRequestCharge(getContext(), RECHARGE_ID, Constants.CHARGE_WX);
                    }
                });
        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(buttonItem);
        buttonItems.add(buttonItem1);
        DialogManager dialogManager = new DialogManager(getActivity());
        dialogManager.setCanceledOnClickOutside(false);
        dialogManager.showCommonPopupDialog(buttonItems, getString(R.string.cancel), false);
    }

    /**
     * 发起充值
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link Constants#CHARGE_ALIPAY} 和
     *                     {@link Constants#CHARGE_WX}
     */
    public void doRequestCharge(Context context, String chargeProdId, String payChannel) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("clientIp", NetworkUtils.getIPAddress(context));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<ServiceResult<JsonObject>>() {
            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                if (response == null) {
                    SingleToastUtil.showToast("数据错误");
                    return;
                }
                if (response.isSuccess()) {
                    getChargeOrOrderInfo(response.getData().toString());
                } else {
                    getChargeOrOrderInfoFail(response.getErrorMessage());
                }
                dismiss();
            }
        };
        ResponseErrorListener errorListener = error -> getChargeOrOrderInfoFail(error.getErrorStr());
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.requestCharge(),
                CommonParamUtil.getDefaultHeaders(),
                param,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST
        );
    }

    /**
     * 发起汇聚支付
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link Constants#CHARGE_JOIN_PAY_WX}
     */
    public void doRequestJoinPayCharge(Context context, String chargeProdId, String payChannel) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        ResponseListener listener = new ResponseListener<ServiceResult<PaymentResponseBean>>() {
            @Override
            public void onResponse(ServiceResult<PaymentResponseBean> response) {
                if (response != null && response.getData() != null && response.isSuccess()) {
                    getJoinPayOrderInfo(response.getData(), payChannel);
                } else {
                    getJoinPayOrderInfoFail(response == null ? "请求失败" : response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if (error != null) {
                    getJoinPayOrderInfoFail(error.getErrorStr());
                }
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.requestJoinPayCharge(),
                CommonParamUtil.getDefaultHeaders(),
                param,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST
        );
    }

    public void getChargeOrOrderInfoFail(String error) {
        SingleToastUtil.showToast("发起充值失败" + error);
    }

    public void getChargeOrOrderInfo(String data) {
        if (data == null || getActivity() == null) {
            return;
        }
        if (Constants.PAY_ALIPAY_CHANNEL == Constants.PAY_CHANNEL_ECPSS) {
            L.info(TAG, "data: %s", data);
            try {
                String payUrl = new Json(data).str("payUrl");
                L.info(TAG, "payUrl: %s", payUrl);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("alipays://platformapi/startApp?appId=10000011&url=" + URLEncoder.encode(payUrl, "UTF-8")));
                startActivity(intent);
            } catch (Exception e) {
                L.error(TAG, "getChargeOrOrderInfo:", e);
                SingleToastUtil.showToast("发起充值失败，请联系客服人员");
            }
        } else {
            Pingpp.createPayment(this, data);
        }
    }

    /**
     * 汇聚支付
     *
     * @param data
     * @param payChannel
     */
    public void getJoinPayOrderInfo(PaymentResponseBean data, String payChannel) {
        createJoinPayment(getContext(), data);
    }

    public void getJoinPayOrderInfoFail(String error) {
        SingleToastUtil.showToast("发起充值失败" + error);
    }

    /**
     * 调起微信支付
     *
     * @param context
     * @param data
     */
    public void createJoinPayment(Context context, PaymentResponseBean data) {
        final IWXAPI msgApi = WXAPIFactory.createWXAPI(context, null);
        msgApi.registerApp(data.getAppId());// 将该app注册到微信
        PayReq request = new PayReq();
        request.appId = data.getAppId();
        request.partnerId = data.getPartnerId();
        request.prepayId = data.getPrepayId();
        request.packageValue = "Sign=WXPay";
        request.nonceStr = data.getNonceStr();
        request.timeStamp = data.getTimeStamp();
        request.sign = data.getPaySign();
        msgApi.sendReq(request);
    }

    /**
     * 发起充值 汇潮支付
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link Constants#CHARGE_ALIPAY} 和
     *                     {@link Constants#CHARGE_WX}
     */
    public void doRequestCharge(Context context, String chargeProdId, String payChannel, String url) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("clientIp", NetworkUtils.getIPAddress(context));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        L.info(TAG, "doRequestCharge url: %s, param: %s", url, param);
        ResponseListener listener = new ResponseListener<ServiceResult<JsonObject>>() {
            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                L.info(TAG, "onResponse response: %s", response);
                if (response != null && response.getData() != null && response.isSuccess()) {
                    getChargeOrOrderInfo(response.getData().toString());
                } else {
                    L.info(TAG, "onResponse response: %s", response == null ? "请求失败" : response.getErrorMessage());
                    getChargeOrOrderInfoFail(response == null ? "请求失败" : response.getErrorMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                L.info(TAG, "onResponse onErrorResponse: ", error);
                if (error != null) {
                    getChargeOrOrderInfoFail(error.getErrorStr());
                }
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(url,
                CommonParamUtil.getDefaultHeaders(),
                param,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST
        );
    }
}
