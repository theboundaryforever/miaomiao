package com.tongdaxing.erban.common.ui.home.view;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.blur.BlurTransformation;
import com.tongdaxing.xchat_core.home.HomeRoom;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/5/18.
 */

public class HotHeader extends LinearLayout {


    @BindView(R2.id.iv_hot_item_icon_1)
    RoundedImageView ivHotItemIcon1;
    @BindView(R2.id.iv_user_icon_1)
    RoundedImageView ivUserIcon1;
    @BindView(R2.id.rl_hot_header_1)
    RelativeLayout rlHotHeader1;
    @BindView(R2.id.tv_hot_item_title_1)
    TextView tvHotItemTitle1;
    @BindView(R2.id.tv_hot_item_count_1)
    TextView tvHotItemCount1;
    @BindView(R2.id.iv_hot_item_icon_2)
    RoundedImageView ivHotItemIcon2;
    @BindView(R2.id.iv_user_icon_2)
    RoundedImageView ivUserIcon2;
    @BindView(R2.id.rl_hot_header_2)
    RelativeLayout rlHotHeader2;
    @BindView(R2.id.tv_hot_item_title_2)
    TextView tvHotItemTitle2;
    @BindView(R2.id.tv_hot_item_count_2)
    TextView tvHotItemCount2;
    @BindView(R2.id.iv_hot_item_icon_3)
    RoundedImageView ivHotItemIcon3;
    @BindView(R2.id.iv_user_icon_3)
    RoundedImageView ivUserIcon3;
    @BindView(R2.id.rl_hot_header_3)
    RelativeLayout rlHotHeader3;
    @BindView(R2.id.tv_hot_item_title_3)
    TextView tvHotItemTitle3;
    @BindView(R2.id.tv_hot_item_count_3)
    TextView tvHotItemCount3;
    @BindView(R2.id.ll_hot_header_bg)
    LinearLayout llHotHeaderBg;
    @BindView(R2.id.hot_state_1)
    ImageView hotState1;
    @BindView(R2.id.hot_state_2)
    ImageView hotState2;
    @BindView(R2.id.hot_state_3)
    ImageView hotState3;
    private Context context;

    public HotHeader(Context context) {
        this(context, null);
    }

    public HotHeader(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_header_hot, this);
        ButterKnife.bind(this, inflate);
        this.context = context;

    }

    public void setData(HomeRoom dataFirst, HomeRoom dataSecond) {
        setItemData(dataFirst, ivHotItemIcon1, ivUserIcon1, tvHotItemTitle1, tvHotItemCount1, hotState1, rlHotHeader1);
        setItemData(dataSecond, ivHotItemIcon2, ivUserIcon2, tvHotItemTitle2, tvHotItemCount2, hotState2, rlHotHeader2);
//        setItemData(dataThird, ivHotItemIcon3, ivUserIcon3, tvHotItemTitle3, tvHotItemCount3, hotState3, rlHotHeader3);
    }

    private void setItemData(HomeRoom data, RoundedImageView ivHotItemIcon, RoundedImageView ivUserIcon, TextView tvHotItemTitle, TextView tvHotItemCount, ImageView hotState, RelativeLayout rlHotHeader) {

        if (data == null) {
            rlHotHeader.setVisibility(INVISIBLE);
            return;
        } else {
            rlHotHeader.setVisibility(VISIBLE);
        }

        long uid = data.getUid();
        rlHotHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AVRoomActivity.start(context, uid);
            }
        });
        GlideApp.with(context)
                .load(data.getAvatar())
                .transform(new BlurTransformation(context, 15, 10))
                .into(ivHotItemIcon);
        ImageLoadUtils.loadImage(context, data.getAvatar(), ivUserIcon);
        tvHotItemTitle.setText(data.title);
        tvHotItemCount.setText(data.onlineNum + "人");
        hotState.setVisibility(data.getSeqNo() > 0 ? VISIBLE : GONE);


    }
}
