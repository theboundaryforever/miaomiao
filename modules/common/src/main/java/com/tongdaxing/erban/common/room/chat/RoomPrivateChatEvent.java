package com.tongdaxing.erban.common.room.chat;

/**
 * 房间内用户是否可以私聊
 * <p>
 * Created by zhangjian on 2019/5/22.
 */
public class RoomPrivateChatEvent {

    public static class OnSuccess {

        String userId;

        public OnSuccess(String userId) {

            this.userId = userId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

    }
}
