package com.tongdaxing.erban.common.ui.home.adpater;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tcloud.core.util.DensityUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * <p> 公共多个滑动tab样式 </p>
 *
 * @author zeda
 * @date 2019/1/19
 */
public class CommonImageIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;

    private int normalColorId = R.color.color_999999;
    private int selectColorId = R.color.color_1A1A1A;

    private int mDefaultDrawableId = R.drawable.ic_nav_selected;
    private float mImageIndicatorWidth = 10.0f;
    private float mImageIndicatorHeight = 3.0f;
    private float mImageIndicatorMarginTop = 1.0f;
    private float mImageIndicatorMarginBottom = 1.0f;

    private boolean isWrap = false;
    private int size = 16;
    private CommonMagicIndicatorAdapter.OnItemSelectListener mOnItemSelectListener;
    private CommonPagerTitleView.OnPagerTitleChangeListener mOnPagerTitleChangeListener;

    public CommonImageIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mContext = context;
        mTitleList = titleList;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    public void setNormalColorId(@ColorRes int normalColorId) {
        this.normalColorId = normalColorId;
    }

    public void setSelectColorId(@ColorRes int selectColorId) {
        this.selectColorId = selectColorId;
    }

    public void setDefaultDrawableId(@DrawableRes int drawableId) {
        this.mDefaultDrawableId = drawableId;
    }

    public void setImageIndicatorWidth(float width) {
        this.mImageIndicatorWidth = width;
    }

    public void setImageIndicatorHeight(float height) {
        this.mImageIndicatorHeight = height;
    }

    public void setImageIndicatorMarginTop(float top) {
        this.mImageIndicatorMarginTop = top;
    }

    public void setImageIndicatorMarginBottom(float bottom) {
        this.mImageIndicatorMarginBottom = bottom;
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {

        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.layout_img_indicator);

        final ImageView indicator = pagerTitleView.findViewById(R.id.indicator_iv);
        final TextView indicatorText = pagerTitleView.findViewById(R.id.indicator);
        indicatorText.setTextSize(size);

        indicator.setImageDrawable(context.getResources().getDrawable(mDefaultDrawableId));
        indicatorText.setText(mTitleList.get(i).getName());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.width = DensityUtil.dip2px(context, mImageIndicatorWidth);
        params.height = DensityUtil.dip2px(context, mImageIndicatorHeight);
        params.topMargin = DensityUtil.dip2px(context, mImageIndicatorMarginTop);
        params.bottomMargin = DensityUtil.dip2px(context, mImageIndicatorMarginBottom);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        indicator.setLayoutParams(params);

        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {

            @Override
            public void onSelected(int index, int totalCount) {
                indicatorText.getPaint().setFakeBoldText(true);
                if (mOnPagerTitleChangeListener != null) {
                    mOnPagerTitleChangeListener.onSelected(index, totalCount);
                }
            }

            @Override
            public void onDeselected(int index, int totalCount) {
                indicatorText.getPaint().setFakeBoldText(false);
                if (mOnPagerTitleChangeListener != null) {
                    mOnPagerTitleChangeListener.onDeselected(index, totalCount);
                }
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
//                // 实现颜色渐变
//                int color = ArgbEvaluatorHolder.eval(leavePercent,
//                        ContextCompat.getColor(mContext, selectColorId),
//                        ContextCompat.getColor(mContext, normalColorId));
//                indicatorText.setTextColor(color);
//                indicatorText.setScaleX(1.4f + (1f - 1.4f) * leavePercent);
//                indicatorText.setScaleY(1.4f + (1f - 1.4f) * leavePercent);

                if (leavePercent > 0.75) {
                    indicatorText.getPaint().setFakeBoldText(false);
                    indicator.setVisibility(View.INVISIBLE);
                    indicatorText.setTextColor(ContextCompat.getColorStateList(mContext, normalColorId));
                }
                if (mOnPagerTitleChangeListener != null) {
                    mOnPagerTitleChangeListener.onDeselected(index, totalCount);
                }
            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
//                // 实现颜色渐变
//                int color = ArgbEvaluatorHolder.eval(enterPercent,
//                        ContextCompat.getColor(mContext, normalColorId),
//                        ContextCompat.getColor(mContext, selectColorId));
//                indicatorText.setTextColor(color);
//                indicatorText.setScaleX(1f + (1.4f - 1f) * enterPercent);
//                indicatorText.setScaleY(1f + (1.4f - 1f) * enterPercent);
                if (enterPercent > 0.75) {
                    indicatorText.getPaint().setFakeBoldText(true);
                    indicator.setVisibility(View.VISIBLE);
                    indicatorText.setTextColor(ContextCompat.getColorStateList(mContext, selectColorId));
                }
                if (mOnPagerTitleChangeListener != null) {
                    mOnPagerTitleChangeListener.onSelected(index, totalCount);
                }
            }
        });
        pagerTitleView.setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }
        });
        return pagerTitleView;
    }

    @Override
    public IPagerIndicator getIndicator(Context context) {
        return null;
    }

    public void setOnItemSelectListener(CommonMagicIndicatorAdapter.OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public void setOnPagerTitleChangeListener(CommonPagerTitleView.OnPagerTitleChangeListener onPagerTitleChangeListener) {
        mOnPagerTitleChangeListener = onPagerTitleChangeListener;
    }
}