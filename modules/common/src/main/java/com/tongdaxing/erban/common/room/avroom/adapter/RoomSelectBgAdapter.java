package com.tongdaxing.erban.common.room.avroom.adapter;


import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.ChatSelectBgBean;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

/**
 * Created by Administrator on 2018/3/23.
 */

public class RoomSelectBgAdapter extends BaseQuickAdapter<ChatSelectBgBean, BaseViewHolder> {


    public String selectIndex = "0";
    itemAction itemAction;

    public RoomSelectBgAdapter() {
        super(R.layout.item_select_chat_room_bg);
    }

    public void setItemAction(RoomSelectBgAdapter.itemAction itemAction) {
        this.itemAction = itemAction;
    }

    @Override
    protected void convert(BaseViewHolder helper, ChatSelectBgBean item) {
        helper.getView(R.id.iv_select_icon).setVisibility(selectIndex.equals(item.id) ? View.VISIBLE : View.GONE);
        helper.setText(R.id.tv_select_bg_name, item.getBackName() + "");
        ImageView ivBack = helper.getView(R.id.iv_select_bg);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectIndex = item.id;
                notifyDataSetChanged();
                if (itemAction != null) {
                    itemAction.itemSelect(item);
                }
            }
        });
        if (StringUtil.isEmpty(item.picUrl)) {
            RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            int defaultBgResId = R.drawable.chat_room_bg_for_non_cp;
            if (mCurrentRoomInfo != null && mCurrentRoomInfo.getTagType() == RoomInfo.ROOMTYPE_LOVERS) {
                defaultBgResId = R.drawable.chat_room_bg_for_cp;
            }
            ImageLoadUtils.loadImageResNoCache(mContext, defaultBgResId, ivBack, R.drawable.ic_empty_bg);
        } else {
            ImageLoadUtils.loadImage(mContext, item.picUrl, ivBack, R.drawable.ic_empty_bg, R.drawable.ic_empty_bg);
        }
    }

    public interface itemAction {
        void itemSelect(ChatSelectBgBean item);
    }
}
