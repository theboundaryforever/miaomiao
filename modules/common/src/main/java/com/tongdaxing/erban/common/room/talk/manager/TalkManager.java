package com.tongdaxing.erban.common.room.talk.manager;


import android.util.SparseArray;

import com.tongdaxing.xchat_core.room.talk.TalkType;
import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;

/**
 * Created by Chen on 2019/4/18.
 */
public class TalkManager {

    public final static int TALK_MAX_MESSAGE_COUNT = 500;
    private static volatile TalkManager mInstance;

    private SparseArray<AbsBaseViewHolder.ViewHolderFactory> mTalkClazzMap = new SparseArray<>();

    private TalkManager() {

    }

    public static TalkManager getInstance() {
        if (mInstance == null) {
            synchronized (TalkManager.class) {
                if (mInstance == null) {
                    mInstance = new TalkManager();
                }
            }
        }
        return mInstance;
    }

    public void registerFactory(@TalkType.Type int type, AbsBaseViewHolder.ViewHolderFactory factory) {
        mTalkClazzMap.put(type, factory);
    }

    public AbsBaseViewHolder.ViewHolderFactory getFactory(@TalkType.Type int type) {
        return mTalkClazzMap.get(type);
    }

    public SparseArray<AbsBaseViewHolder.ViewHolderFactory> getTalkClazzSparseArray() {
        return mTalkClazzMap;
    }


}
