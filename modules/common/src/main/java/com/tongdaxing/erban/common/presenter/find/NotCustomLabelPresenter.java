package com.tongdaxing.erban.common.presenter.find;

import com.tongdaxing.erban.common.ui.find.model.CreateRoomModel;
import com.tongdaxing.erban.common.ui.find.view.INotCustomLabelView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

/**
 * Function:
 * Author: Edward on 2019/3/18
 */
public class NotCustomLabelPresenter extends AbstractMvpPresenter<INotCustomLabelView> {
    private CreateRoomModel createRoomModel;

    public NotCustomLabelPresenter() {
        createRoomModel = new CreateRoomModel();
    }

    public void delCustomLabel(int tagId) {
        createRoomModel.delCustomLabel(tagId, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null && e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response == null || getMvpView() == null) {
                    onError(new Exception());
                    return;
                }
                if (response.num("code") == 200) {
                    SingleToastUtil.showToast("刪除标签成功!");
                    getMvpView().delLabelSucceed(tagId);
                } else {
                    onError(new Exception());
                }
            }
        });
    }
}
