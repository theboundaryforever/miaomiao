package com.tongdaxing.erban.common.room.operation;

/**
 * Created by Chen on 2019/6/11.
 */
public class RoomOperationMoreType {
    public static final int ROOM_OPERATION_MORE_SETTING = 1;
    public static final int ROOM_OPERATION_MORE_PK = 2;
    public static final int ROOM_OPERATION_MORE_SCREEN = 3;
    public static final int ROOM_OPERATION_MORE_GIFT = 4;
    public static final int ROOM_OPERATION_MORE_WHEEL = 5;
    public static final int ROOM_OPERATION_MORE_MUSIC = 6;
    public static final int ROOM_OPERATION_MORE_QUESTION = 7;
    public static final int ROOM_OPERATION_MORE_REPORT = 8;
}
