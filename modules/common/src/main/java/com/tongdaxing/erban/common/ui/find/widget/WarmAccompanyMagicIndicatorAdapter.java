package com.tongdaxing.erban.common.ui.find.widget;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/17
 */
public class WarmAccompanyMagicIndicatorAdapter extends CommonNavigatorAdapter {
    private Context mContext;
    private List<TabInfo> mTitleList;

    private int normalColorId = R.color.color_999999;
    private int selectColorId = R.color.color_1A1A1A;
    private boolean isWrap = false;
    private int size = 16;
    private CommonMagicIndicatorAdapter.OnItemSelectListener mOnItemSelectListener;

    public WarmAccompanyMagicIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mContext = context;
        mTitleList = titleList;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    public void setNormalColorId(@ColorRes int normalColorId) {
        this.normalColorId = normalColorId;
    }

    public void setSelectColorId(@ColorRes int selectColorId) {
        this.selectColorId = selectColorId;
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {

        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.layout_warm_accompany_indicator);

        final TextView indicatorText = pagerTitleView.findViewById(R.id.indicator);
        indicatorText.setTextSize(size);
        int startAndEnd = DisplayUtils.dip2px(mContext, 10);
        int topAndBottom = DisplayUtils.dip2px(mContext, 5);
        indicatorText.setPadding(startAndEnd, topAndBottom, startAndEnd, topAndBottom);

        indicatorText.setText(mTitleList.get(i).getName());
        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {

            @Override
            public void onSelected(int index, int totalCount) {
            }

            @Override
            public void onDeselected(int index, int totalCount) {
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
                indicatorText.setTextColor(ContextCompat.getColorStateList(mContext, R.color.color_626269));
                indicatorText.setBackgroundResource(R.drawable.shape_warm_accompany_not_selected);
            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
                indicatorText.setTextColor(ContextCompat.getColorStateList(mContext, R.color.white));
                indicatorText.setBackgroundResource(R.drawable.shape_warm_accompany_selected);
            }
        });
        pagerTitleView.setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }
        });
        return pagerTitleView;
    }

    @Override
    public IPagerIndicator getIndicator(Context context) {
        return null;
    }

    public void setOnItemSelectListener(CommonMagicIndicatorAdapter.OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

}
