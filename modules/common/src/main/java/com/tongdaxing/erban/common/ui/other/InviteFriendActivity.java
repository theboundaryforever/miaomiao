package com.tongdaxing.erban.common.ui.other;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.constant.BaseUrl;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

public class InviteFriendActivity extends BaseActivity {

    @BindView(R2.id.title_bar)
    TitleBar titleBar;
    @BindView(R2.id.tv_cancel)
    TextView tvCancel;
    @BindView(R2.id.tv_weixin)
    TextView tvWeixin;
    @BindView(R2.id.tv_weixinpy)
    TextView tvWeixinpy;
    @BindView(R2.id.tv_qq)
    TextView tvQq;
    @BindView(R2.id.tv_qq_zone)
    TextView tvQqZone;
    @BindView(R2.id.ll)
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friend);
        ButterKnife.bind(this);
        initTitleBar("邀请好友");
    }

    @OnClick({R2.id.tv_weixin, R2.id.tv_weixinpy, R2.id.tv_qq, R2.id.tv_qq_zone})
    public void onClick(View view) {
        super.onClick(view);
        WebViewInfo info = new WebViewInfo();
        info.setShowUrl(BaseUrl.SHARE_DOWNLOAD);
        info.setTitle("喵喵，语音交友神器！");
        info.setDesc("喵喵语音交友速配处CP，你想要的我们都有。");
        info.setImgUrl(BaseUrl.SHARE_DEFAULT_LOGO);
        int i = view.getId();
        if (i == R.id.tv_weixin) {
            CoreManager.getCore(IShareCore.class).shareH5(info, ShareSDK.getPlatform(Wechat.NAME));

        } else if (i == R.id.tv_weixinpy) {
            CoreManager.getCore(IShareCore.class).shareH5(info, ShareSDK.getPlatform(WechatMoments.NAME));

        } else if (i == R.id.tv_qq) {
            CoreManager.getCore(IShareCore.class).shareH5(info, ShareSDK.getPlatform(QQ.NAME));

        } else if (i == R.id.tv_qq_zone) {
            CoreManager.getCore(IShareCore.class).shareH5(info, ShareSDK.getPlatform(QZone.NAME));

        }
    }
}
