package com.tongdaxing.erban.common.model;

import com.tongdaxing.erban.common.ui.rankinglist.RankingListFragment;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RankingInfo;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * 排行榜 model
 */
public class RankingListModel extends BaseMvpModel {

    /**
     * 排行榜
     *
     * @param type     排行榜类型 0魅力榜，1土豪榜，3CP榜
     * @param dateType 榜单周期类型 0日榜，1周榜，2总榜
     */
    public void getRankingList(int type, int dateType, final OkHttpManager.MyCallBack<RankingInfo> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        if (type == RankingListFragment.RANKING_TYPE_CP) {
            params.put("type", "4");//服务器的cp榜是4
            params.put("datetype", dateType + 2 + "");//cp榜dateType从2开始
        } else {
            params.put("type", type + 1 + "");//服务器的type从1开始
            params.put("datetype", dateType + 1 + "");//服务器的dateType从1开始
        }

        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        getRequest(UriProvider.getRankingList(), params, new OkHttpManager.MyCallBack<ServiceResult<RankingInfo>>() {
            @Override
            public void onError(Exception e) {
                if (callBack != null) {
                    callBack.onError(new Exception("加载失败"));
                }
            }

            @Override
            public void onResponse(ServiceResult<RankingInfo> response) {
                if (response != null && response.isSuccess()) {
                    if (callBack != null) {
                        callBack.onResponse(response.getData());
                    }
                } else {
                    if (callBack != null) {
                        callBack.onError(new Exception(response != null ? response.getMessage() : "加载失败"));
                    }
                }
            }
        });
    }
}
