package com.tongdaxing.erban.common.ui.me.withdraw;

import android.os.Bundle;
import android.webkit.WebView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.xchat_core.UriProvider;

public class WithdrawRuleActivity extends BaseActivity {
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_rule);
        initTitleBar("提现规则");
        mWebView = (WebView) findViewById(R.id.wv_view);
        mWebView.loadUrl(UriProvider.IM_SERVER_URL + "/modules/guide/withdraw.html");
    }
}
