package com.tongdaxing.erban.common.room.lover.weekcp.topic;

import com.erban.ui.mvp.BasePresenter;
import com.tongdaxing.erban.common.room.lover.weekcp.topic.view.SoulTopicAction;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.weekcp.topic.SoulTopicEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by Chen on 2019/5/8.
 */
public class SoulTopicDialogPresenter extends BasePresenter<ISoulTopicDialogView> {

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDimissDialog(SoulTopicAction.OnSoulTopicDismiss dismiss) {
        if (getView() == null) {
            return;
        }
        getView().dismiss();
    }

    public void requestSoulTopic() {
        CoreManager.getCore(IRoomCore.class).getWeekManager().getSoulTopicCtrl().requestAskQuestion();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetAskQuestionSuccess(SoulTopicEvent.OnSoulTopicSuccess soulTopicSuccess) {
        if (getView() == null) {
            return;
        }
        getView().updateView(CoreManager.getCore(IRoomCore.class).getWeekManager().getSoulTopicInfo().getSoulTittle());
    }

    public void requestRoomPlay(String msg) {
        CoreManager.getCore(IRoomCore.class).getWeekManager().getSoulTopicCtrl().doRoomPlay(msg);
    }

    public List<String> getQuestions() {
        return CoreManager.getCore(IRoomCore.class).getWeekManager().getSoulTopicInfo().getSoulTabQuestions();
    }

    public int getCurrentTab() {
        return CoreManager.getCore(IRoomCore.class).getWeekManager().getSoulTopicInfo().getQuestionTab();
    }

    public void setCurrentTab(int position) {
        CoreManager.getCore(IRoomCore.class).getWeekManager().getSoulTopicInfo().setQuestionTab(position);
    }
}
