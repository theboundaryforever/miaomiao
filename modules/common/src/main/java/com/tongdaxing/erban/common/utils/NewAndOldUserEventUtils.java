package com.tongdaxing.erban.common.utils;

import android.content.Context;

import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;

/**
 * 新老用户点击统计
 * <p>
 * Created by zhangjian on 2019/7/3.
 */
public class NewAndOldUserEventUtils {
    public static void onCommonTypeEvent(Context context, String eventId) {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            if (CoreManager.getCore(IUserCore.class).isNewUser(userInfo) && userInfo.getChargeNum() <= 0) {
                //用户处于新手期，且没有充值过
                UmengEventUtil.getInstance().onCommonTypeEvent(context, eventId, "新用户");
            } else { //充值过或者超过新手期
                UmengEventUtil.getInstance().onCommonTypeEvent(context, eventId, "老用户");
            }
        }
    }
}
