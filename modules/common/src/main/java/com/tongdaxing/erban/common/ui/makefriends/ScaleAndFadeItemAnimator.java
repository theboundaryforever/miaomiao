package com.tongdaxing.erban.common.ui.makefriends;

import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v7.widget.RecyclerView;
import android.view.animation.OvershootInterpolator;

/**
 * RecyclerView item的动画 加载时从0.6到1缩放、透明度变化
 * <p>
 * Created by zhangjian on 2019/6/21.
 */
public class ScaleAndFadeItemAnimator extends BaseItemAnimator {

    private void start(RecyclerView.ViewHolder holder) {
        ViewCompat.animate(holder.itemView)
                .scaleX(0.8f)
                .scaleY(0.8f)
//                .setInterpolator(new OvershootInterpolator())
                .alpha(0.8f);
    }

    private void start(ViewPropertyAnimatorCompat animator) {
        animator.scaleX(0.8f)
                .scaleY(0.8f)
//                .setInterpolator(new OvershootInterpolator())
                .alpha(0.8f);
    }

    private void end(RecyclerView.ViewHolder holder) {
        ViewCompat.animate(holder.itemView)
                .scaleX(1f)
                .scaleY(1f)
                .setInterpolator(new OvershootInterpolator())
                .alpha(1);
    }

    private void end(ViewPropertyAnimatorCompat animator) {
        animator.scaleX(1f)
                .scaleY(1f)
                .setInterpolator(new OvershootInterpolator())
                .alpha(1);
    }


    /**
     * 执行移除动画
     *
     * @param holder   被移除的ViewHolder
     * @param animator 被移动的ViewHolder对应动画对象
     */
    @Override
    public void setRemoveAnimation(RecyclerView.ViewHolder holder, ViewPropertyAnimatorCompat animator) {
        animator.scaleX(0.8f)
                .scaleY(0.8f)
                .alpha(0.5f);
    }

    /**
     * 执行移除动画结束，执行还原，因为该ViewHolder会被复用
     *
     * @param holder 被移除的ViewHolder
     */
    @Override
    public void removeAnimationEnd(RecyclerView.ViewHolder holder) {
        ViewCompat.animate(holder.itemView)
                .scaleX(1f)
                .scaleY(1f)
                .alpha(1f);
    }

    /**
     * 执行添加动画初始化 这里设置透明为0添加时就会有渐变效果当然你可以在执行动画代码之前执行
     *
     * @param holder 添加的ViewHolder
     */
    @Override
    public void addAnimationInit(RecyclerView.ViewHolder holder) {
        start(holder);
    }

    /**
     * 执行添加动画
     *
     * @param holder   添加的ViewHolder
     * @param animator 添加的ViewHolder对应动画对象
     */
    @Override
    public void setAddAnimation(RecyclerView.ViewHolder holder, ViewPropertyAnimatorCompat animator) {
        end(animator);
    }

    /**
     * 取消添加还原状态以复用
     *
     * @param holder 添加的ViewHolder
     */
    @Override
    public void addAnimationCancel(RecyclerView.ViewHolder holder) {
        end(holder);
    }

    /**
     * 更新时旧的ViewHolder动画
     *
     * @param holder   旧的ViewHolder
     * @param animator ViewHolder对应动画对象
     */
    @Override
    public void setOldChangeAnimation(RecyclerView.ViewHolder holder, ViewPropertyAnimatorCompat animator) {
        start(animator);
    }

    /**
     * 更新时旧的ViewHolder动画结束，执行还原
     *
     * @param holder
     */
    @Override
    public void oldChangeAnimationEnd(RecyclerView.ViewHolder holder) {
        end(holder);
    }

    /**
     * 更新时新的ViewHolder初始化
     *
     * @param holder 更新时新的ViewHolder
     */
    @Override
    public void newChangeAnimationInit(RecyclerView.ViewHolder holder) {
        start(holder);
    }

    /**
     * 更新时新的ViewHolder动画
     *
     * @param holder   新的ViewHolder
     * @param animator ViewHolder对应动画对象
     */
    @Override
    public void setNewChangeAnimation(RecyclerView.ViewHolder holder, ViewPropertyAnimatorCompat animator) {
        end(animator);
    }

    /**
     * 更新时新的ViewHolder动画结束，执行还原
     *
     * @param holder
     */
    @Override
    public void newChangeAnimationEnd(RecyclerView.ViewHolder holder) {
        end(holder);
    }
}
