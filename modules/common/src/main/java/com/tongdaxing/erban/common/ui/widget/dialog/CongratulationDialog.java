package com.tongdaxing.erban.common.ui.widget.dialog;

import android.view.View;
import android.widget.ImageView;

import com.juxiao.library_ui.widget.ViewHolder;
import com.juxiao.library_ui.widget.dialog.BaseDialog;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.user.ICongratulationDialogCoreClient;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * Function:单例对话框
 * Author: Edward on 2019/1/2
 */
public class CongratulationDialog extends BaseDialog implements View.OnClickListener {
    private ImageView ivClose, ivExamineBackpack;

    @Override
    public void convertView(ViewHolder viewHolder) {
        ivExamineBackpack = viewHolder.getView(R.id.iv_examine_backpack);
        ivClose = viewHolder.getView(R.id.iv_close);
        ivExamineBackpack.setOnClickListener(this);
        ivClose.setOnClickListener(this);
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_congratulation;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();//点击查看背包
        if (i == R.id.iv_close) {
            dismiss();

        } else if (i == R.id.iv_examine_backpack) {
            CoreManager.notifyClients(ICongratulationDialogCoreClient.class, ICongratulationDialogCoreClient.OPEN_GIFT_DIALOG_FROM_CONGRA_DIALOG);
            dismiss();

        }
    }
}
