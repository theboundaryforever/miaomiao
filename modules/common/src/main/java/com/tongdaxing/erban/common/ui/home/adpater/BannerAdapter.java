package com.tongdaxing.erban.common.ui.home.adpater;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.banner.BannerClickModel;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;

import java.util.List;

/**
 * @author Administrator
 * @date 2017/8/7
 */

public class BannerAdapter extends StaticPagerAdapter {
    private Context context;
    private List<BannerInfo> bannerInfoList;
    private LayoutInflater mInflater;
    private boolean mIsImageRound;
    private int mRoundingRadiusId = R.dimen.common_cover_round_size;//5dp
    private String mBannerLocation;
    private BannerClickModel mBannerClickModel;

    public BannerAdapter(List<BannerInfo> bannerInfoList, Context context, String bannerLocation) {
        this.context = context;
        this.bannerInfoList = bannerInfoList;
        mBannerLocation = bannerLocation;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(ViewGroup container, int position) {
        final BannerInfo bannerInfo = bannerInfoList.get(position);
        ImageView imgBanner = (ImageView) mInflater.inflate(R.layout.banner_page_item, container, false);
        if (mIsImageRound) {
            GlideApp.with(context)
                    .load(bannerInfo.getBannerPic())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transforms(new CenterCrop(),
                            new RoundedCorners(context.getResources().getDimensionPixelOffset(mRoundingRadiusId)))
                    .placeholder(R.drawable.default_cover)
                    .into(imgBanner);
        } else {
            GlideApp.with(context)
                    .load(bannerInfo.getBannerPic())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.default_cover)
                    .into(imgBanner);
        }
        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBannerClickModel == null) {
                    mBannerClickModel = new BannerClickModel();
                }
                mBannerClickModel.bannerStat(bannerInfo.getBannerId(), mBannerLocation);
                UmengEventUtil.getInstance().onHomeBanner(context, bannerInfo.getBannerName(), bannerInfo.getBannerId(), bannerInfo.getSkipUri(), position + 1);
                if (bannerInfo.getSkipType() == 3) {
                    Intent intent = new Intent(context, CommonWebViewActivity.class);
                    intent.putExtra("url", bannerInfo.getSkipUri());
                    context.startActivity(intent);
                } else if (bannerInfo.getSkipType() == 2) {
                    AVRoomActivity.start(context, JavaUtil.str2long(bannerInfo.getSkipUri()));
                }
            }
        });
        return imgBanner;
    }

    public void setIsImageRound(boolean isImageRound) {
        mIsImageRound = isImageRound;
    }

    public void setImageRoundRadiusId(int roundingRadiusId) {
        mIsImageRound = true;
        mRoundingRadiusId = roundingRadiusId;
    }

    @Override
    public int getCount() {
        if (bannerInfoList == null) {
            return 0;
        } else
            return bannerInfoList.size();
    }
}
