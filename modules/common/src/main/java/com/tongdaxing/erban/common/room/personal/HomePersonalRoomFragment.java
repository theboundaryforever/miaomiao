package com.tongdaxing.erban.common.room.personal;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.opensource.svgaplayer.SVGAImageView;
import com.pingplusplus.android.Pingpp;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.im.actions.GiftAction;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.room.avroom.activity.RoomInviteActivity;
import com.tongdaxing.erban.common.room.avroom.activity.RoomTopicActivity;
import com.tongdaxing.erban.common.room.avroom.adapter.MicroViewAdapter;
import com.tongdaxing.erban.common.room.avroom.other.BottomViewListenerWrapper;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.room.avroom.other.SoftKeyBoardListener;
import com.tongdaxing.erban.common.room.avroom.widget.BottomView;
import com.tongdaxing.erban.common.room.avroom.widget.dialog.RoomTopicDIalog;
import com.tongdaxing.erban.common.room.avroom.widget.dialog.SetAudioPlayerEvent;
import com.tongdaxing.erban.common.room.chat.RoomPrivateMsgDialog;
import com.tongdaxing.erban.common.room.egg.PoundEggDialog;
import com.tongdaxing.erban.common.room.face.DynamicFaceDialog;
import com.tongdaxing.erban.common.room.function.RoomFunctionView;
import com.tongdaxing.erban.common.room.gift.GiftDialog;
import com.tongdaxing.erban.common.room.operation.RoomBottomMoreDialog;
import com.tongdaxing.erban.common.room.profile.RoomProfileView;
import com.tongdaxing.erban.common.room.talk.RoomTalkView;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.erban.common.room.widget.dialog.BigListDataDialog;
import com.tongdaxing.erban.common.room.widget.dialog.GiftRecordDialog;
import com.tongdaxing.erban.common.room.widget.dialog.MicInListDialog;
import com.tongdaxing.erban.common.room.widget.dialog.RewardGiftDialog;
import com.tongdaxing.erban.common.room.widget.dialog.RoomCpInviteDialog;
import com.tongdaxing.erban.common.room.widget.dialog.SimpleTextDialog;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.ui.widget.GlideImageLoader;
import com.tongdaxing.erban.common.ui.widget.dialog.CongratulationDialog;
import com.tongdaxing.erban.common.ui.widget.dialog.OneAudioDialog;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.banner.BannerClickModel;
import com.tongdaxing.xchat_core.banner.BannerInfo;
import com.tongdaxing.xchat_core.bean.BlessingBeastInfo;
import com.tongdaxing.xchat_core.bean.RoomMemberComeInfo;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.count.IChargeClient;
import com.tongdaxing.xchat_core.cp.IRoomCpCoreClient;
import com.tongdaxing.xchat_core.find.SquareClient;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.CpGiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.BlessingBeastAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.BurstGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomBottomMarkManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.RoomAction;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomStatus;
import com.tongdaxing.xchat_core.room.charm.ICharmClient;
import com.tongdaxing.xchat_core.room.charm.ICharmCore;
import com.tongdaxing.xchat_core.room.event.SendQuickMsgEvent;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.room.presenter.HomePersonalPresenter;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.view.IHomePartyView;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpEvent;
import com.tongdaxing.xchat_core.share.IShareCoreClient;
import com.tongdaxing.xchat_core.user.ICongratulationDialogCoreClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.IUserInfoDialogCoreClient;
import com.tongdaxing.xchat_core.user.UserEvent;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.ChatUtil;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_BLESSING_BEAST;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_TYPE_BURST_GIFT;

/**
 * 个人房间
 *
 * @author chenran
 * @date 2017/7/26
 */
@CreatePresenter(HomePersonalPresenter.class)
public class HomePersonalRoomFragment extends BaseMvpFragment<IHomePartyView, HomePersonalPresenter> implements View.OnClickListener, OnBannerListener,
        GiftDialog.OnGiftDialogBtnClickListener, IHomePartyView, MicroViewAdapter.OnMicroItemClickListener, View.OnTouchListener, ChargeListener {

    public static final String INPUT_FILE = "input_file";
    private final String ONE_AUDIO = "one_audio";
    long mDownTimeMillis = 0;
    int xDelta = 0;
    int yDelta;
    @BindView(R2.id.micro_view)
    RoomProfileView microView;
    @BindView(R2.id.room_function_view)
    RoomFunctionView roomFunctionView;
    @BindView(R2.id.tv_nick)
    TextView tvNick;
    @BindView(R2.id.tv_content)
    TextView tvContent;
    @BindView(R2.id.tv_nick_target)
    TextView tvNickTarget;
    @BindView(R2.id.iv_gift)
    ImageView ivGift;
    @BindView(R2.id.tv_gift_number)
    TextView tvGiftNumber;
    @BindView(R2.id.msg_container)
    LinearLayout msgContainer;
    @BindView(R2.id.rl_msg_layout)
    RelativeLayout rlMsgLayout;
    @BindView(R2.id.room_talk_view)
    RoomTalkView roomTalkView;
    @BindView(R2.id.bottom_view)
    BottomView bottomView;
    @BindView(R2.id.activity_banner)
    Banner activityBanner;
    @BindView(R2.id.play_together)
    ImageView playTogether;
    @BindView(R2.id.input_edit)
    EditText inputEdit;
    @BindView(R2.id.input_send)
    ImageView inputSend;
    @BindView(R2.id.input_layout)
    RelativeLayout inputLayout;
    @BindView(R2.id.vs_music_player)
    ViewStub vsMusicPlayer;
    @BindView(R2.id.bu_mic_in_list_count)
    Button buMicInListCount;
    @BindView(R2.id.room_iv_come_level)
    ImageView roomIvComeLevel;
    @BindView(R2.id.tv_msg_name)
    TextView tvMsgName;
    @BindView(R2.id.tv_msg_car)
    TextView tvMsgCar;
    @BindView(R2.id.layout_come_msg)
    LinearLayout layoutComeMsg;
    @BindView(R2.id.lottery_box_iv)
    ImageView lotteryBoxIv;
    @BindView(R2.id.svga_blessing_beast)
    SVGAImageView svgaBlessingBeast;
    @BindView(R2.id.tv_blessing_star_count_down)
    TextView tvBlessingStarCountDown;
    @BindView(R2.id.rl_blessing_beast)
    RelativeLayout rlBlessingBeast;
    @BindView(R2.id.rl_home_party_room_fragment)
    RelativeLayout rlHomePartyRoomFragment;
    private String TAG = "HomePartyRoomFragmentClass";
    private long myUid;
    private UserInfo roomOwnner;
    //    private MusicPlayerView musicPlayerView;
    private ViewStub mVsMusicPlayer;
    private boolean micInListOption = true;
    private View lotteryBoxView;
    private SVGAImageView mSvgaWeekCpMerry;
    private RoomPrivateMsgDialog mMsgDialog;
    private String mMsgTag = "RoomPrivateMsgDialog";
    private RoomCpInviteDialog roomCpInviteDialog;
    private List<BannerInfo> actionDialogInfoList;
    private CustomAttachment giftAttachment;
    private List<ChatRoomMessage> giftMsgList;
    private GiftRecordDialog record;
    private CountDownTimer countDownTimer;
    private OneAudioDialog oneAudioDialog;
    /**
     * 控制view的位置
     */
    private int mWidthPixels;
    private int mHeightPixels;
    private boolean isPlayingBlessingBeast = false;//是否正在触发福兽
    /**
     * 福兽倒计时
     */
    private CountDownTimer blessingBeastCountDownTimer;
    //是否有新的礼物消息更新标签
    private boolean hasChange = false;
    private UserComeAction userComeAction;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private GameRoomBottomViewWrapper wrapper;
    private SoftKeyBoardListener.OnSoftKeyBoardChangeListener
            mOnSoftKeyBoardChangeListener = new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
        @Override
        public void keyBoardShow(int height) {
            /*软键盘显示：执行隐藏title动画，并修改listview高度和装载礼物容器的高度*/
        }

        @Override
        public void keyBoardHide(int height) {
            /*软键盘隐藏：隐藏聊天输入框并显示聊天按钮，执行显示title动画，并修改listview高度和装载礼物容器的高度*/
            inputLayout.setVisibility(View.GONE);
        }
    };
    private DynamicFaceDialog dynamicFaceDialog = null;
    private GiftDialog giftDialog = null;
    //防止外部新消息回调过来这边正在执行动画影响显示
    private boolean isShowing = false;

    //banner点击统计
    private BannerClickModel mBannerClickModel;

    private static boolean isActivityDestroyed(Context context) {
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.isFinishing() || activity.isDestroyed()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_av_room_personal;
    }

    @Override
    public void onFindViews() {
        ButterKnife.bind(this, mView);
//        ivGroundGlass = mView.findViewById(R.id.iv_ground_glass);
        rlHomePartyRoomFragment = mView.findViewById(R.id.rl_home_party_room_fragment);
        svgaBlessingBeast = mView.findViewById(R.id.svga_blessing_beast);
        rlBlessingBeast = mView.findViewById(R.id.rl_blessing_beast);
        tvBlessingStarCountDown = mView.findViewById(R.id.tv_blessing_star_count_down);
        mVsMusicPlayer = mView.findViewById(R.id.vs_music_player);
        mSvgaWeekCpMerry = mView.findViewById(R.id.room_lover_week_cp_merry);
        lotteryBoxView = mView.findViewById(R.id.lottery_box_iv);
        buMicInListCount = mView.findViewById(R.id.bu_mic_in_list_count);
        buMicInListCount.setOnClickListener(v -> showMicInListDialog());

        loadRoomBannerData();

        giftMsgList = new ArrayList<>();
        List<ChatRoomMessage> hisGiftMessage = CoreManager.getCore(IRoomCore.class).getTalkCtrl().getHisGiftMessage();
        giftMsgList.addAll(hisGiftMessage);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreUtils.register(this);
    }

    private void loadRoomBannerData() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
            params.put("roomUid", String.valueOf(AvRoomDataManager.get().mCurrentRoomInfo.getUid()));
        }
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getRoomCarousel(), params, new OkHttpManager.MyCallBack<ServiceResult<List<BannerInfo>>>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(ServiceResult<List<BannerInfo>> response) {
                if (response.isSuccess()) {
                    actionDialogInfoList = response.getData();
                    if (actionDialogInfoList != null) {
                        if (isNewUser() && isNewbieGift(actionDialogInfoList)) {//如果没有返回新手礼物入口，表明已经领取了新手礼物，因此不再弹出礼物框
                            startCountDown();
                        }
                        initBanner(actionDialogInfoList);
                    }
                }
            }
        });
    }

    /**
     * 初始化广告banner
     */
    private void initBanner(List<BannerInfo> bannerInfos) {
        if (ListUtils.isListEmpty(bannerInfos)) {//没有数据把轮播控件隐藏
            activityBanner.setVisibility(View.GONE);
            return;
        }
        List<String> list = new ArrayList<>();
        for (int i = 0; i < bannerInfos.size(); i++) {
            list.add(bannerInfos.get(i).getBannerPic());
        }
        activityBanner.setVisibility(View.VISIBLE);
        activityBanner.setImages(list)
                .setDelayTime(5 * 1000)
                .setImageLoader(new GlideImageLoader())
                .setOnBannerListener(this)
                .start();
    }

    /**
     * 判断是否新用户
     *
     * @return
     */
    private boolean isNewUser() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo == null) {
            return false;
        }
        if (System.currentTimeMillis() - userInfo.getCreateTime() < (86400 * 7000)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 检查这个列表里面是否有新手礼物
     *
     * @param actionDialogInfoList
     * @return
     */
    private boolean isNewbieGift(List<BannerInfo> actionDialogInfoList) {
        for (int i = 0; i < actionDialogInfoList.size(); i++) {
            if (actionDialogInfoList.get(i).getSkipType() == AVRoomActivity.NEW_USER_SKIP_TYPE) {
                return true;
            }
        }
        return false;
    }

    /**
     * 一元爆音倒计时
     */
    private void startCountDown() {
        if (getActivity() == null) {
            return;
        }
        long currentId = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(ONE_AUDIO + currentId, Activity.MODE_PRIVATE);

        long time = sharedPreferences.getLong(ONE_AUDIO + currentId, 0);
        if (System.currentTimeMillis() - time <= (86400 * 1000)) {//1小时有(86400 * 1000)毫秒
            return;
        }
        countDownTimer = new CountDownTimer(10 * 1000, 1000) {//一天仅自动弹出一次
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                if (getFragmentManager() == null) {
                    return;
                }
                Fragment fragment = getFragmentManager().findFragmentByTag(ONE_AUDIO);
                if (fragment == null) {
                    oneAudioDialog = new OneAudioDialog();
                    showAudioDialog(currentId);
                } else {
                    if (fragment instanceof OneAudioDialog) {
                        oneAudioDialog = (OneAudioDialog) fragment;
                        showAudioDialog(currentId);
                    }
                }

            }
        };
        countDownTimer.start();
    }

    private void showAudioDialog(long currentId) {
        oneAudioDialog.show(getFragmentManager(), ONE_AUDIO);
        editor.putLong(ONE_AUDIO + currentId, System.currentTimeMillis());//存入今天当前日期
        editor.apply();
    }

    /**
     * 设置福猪的初始位置
     */
    private void setBlessingBeastInitPosition() {
        rlBlessingBeast.setOnTouchListener(this);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) rlBlessingBeast.getLayoutParams();
        int viewWidth = layoutParams.width;
        int viewHeight = layoutParams.height;
        int distance = DisplayUtils.dip2px(getActivity(), 8);

        int mw = mWidthPixels - viewWidth - distance;
        int mh = mHeightPixels - viewHeight - DisplayUtils.dip2px(getActivity(), 240);

        layoutParams.leftMargin = mw;
        layoutParams.topMargin = mh;
        rlBlessingBeast.setLayoutParams(layoutParams);
        rlHomePartyRoomFragment.invalidate();
    }

    /**
     * 触发福兽
     */
    public void triggerBlessingBeast(BlessingBeastInfo blessingBeastInfo) {
        if (rlBlessingBeast != null && rlBlessingBeast.getVisibility() == View.GONE && blessingBeastInfo.getRemainTime() > 0) {
            rlBlessingBeast.setVisibility(View.VISIBLE);
            mHeightPixels = getResources().getDisplayMetrics().heightPixels;
            mWidthPixels = getResources().getDisplayMetrics().widthPixels;
            setBlessingBeastInitPosition();
            startPlayBlessingBeast(blessingBeastInfo.getSvgaUrl());
            startBlessingBeastCountDown(blessingBeastInfo.getRemainTime());
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int x = (int) event.getRawX();
        final int y = (int) event.getRawY();
//        Log.d(TAG, "onTouch: x= " + x + "y=" + y);
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mDownTimeMillis = System.currentTimeMillis();
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();
                xDelta = x - params.leftMargin;
                yDelta = y - params.topMargin;
//                Log.d(TAG, "ACTION_DOWN: xDelta= " + xDelta + "yDelta=" + yDelta);
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();
                int width = layoutParams.width;
                int height = layoutParams.height;
                int xDistance = x - xDelta;
                int yDistance = y - yDelta;

                int outX = (mWidthPixels - width) - 10;
                if (xDistance > outX) {
                    xDistance = outX;
                }

                int outY = (mHeightPixels - height) - DisplayUtils.dip2px(getActivity(), 20);
                if (yDistance > outY) {
                    yDistance = outY;
                }

                if (yDistance < 100) {
                    yDistance = 100;
                }
                if (xDistance < 10) {
                    xDistance = 10;
                }


                layoutParams.leftMargin = xDistance;
                layoutParams.topMargin = yDistance;
                view.setLayoutParams(layoutParams);
                break;
            case MotionEvent.ACTION_UP:
                if (System.currentTimeMillis() - mDownTimeMillis < 150) {
                    CommonWebViewActivity.start(getActivity(), UriProvider.lookBlessingBeastRule());//产品要求写死地址
                }
                break;
        }
        rlHomePartyRoomFragment.invalidate();
        return true;
    }

    /**
     * 开始播放福兽动画
     */
    private void startPlayBlessingBeast(String localAssets) {
        SvgaUtils.cyclePlayWebAnim(getContext(), svgaBlessingBeast, localAssets);
    }

    /**
     * 开始福兽倒计时
     */
    private void startBlessingBeastCountDown(long countDownTime) {
        blessingBeastCountDownTimer = new CountDownTimer(countDownTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                isPlayingBlessingBeast = true;
                tvBlessingStarCountDown.setText(TimeUtils.formatDuring(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                isPlayingBlessingBeast = false;
                SvgaUtils.closeSvga(svgaBlessingBeast);
                rlBlessingBeast.setVisibility(View.GONE);
            }
        };
        blessingBeastCountDownTimer.start();
    }

    @Override
    public void initiate() {
        micInListOption = CoreManager.getCore(VersionsCore.class).getConfigData().num("micInListOption") == 1;

        if (!AvRoomDataManager.get().isMinimize()) {
            //设置快捷文案数据
            bottomView.setQuickMsgListData();
        }

        AvRoomDataManager.get().setMinimize(false);
        myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        updateView();
        updateMicBtn();
        // todo
//        MicroViewAdapter microViewAdapter = microView.getAdapter();
//        if (microViewAdapter != null) {
//            microViewAdapter.setOnMicroItemClickListener(this);
//        }
//        activityImg.setAlpha(200);
        IMNetEaseManager.get().subscribeChatRoomEventObservable(new Consumer<RoomEvent>() {
            @Override
            public void accept(RoomEvent roomEvent) throws Exception {
                if (roomEvent == null) return;
                int event = roomEvent.getEvent();
                switch (event) {
                    case RoomEvent.ROOM_CHAT_RECONNECTION://断网重连
                        //从新获取队列信息
                        getMvpPresenter().chatRoomReConnect(roomEvent.roomQueueInfo);

                        break;
                    case RoomEvent.ROOM_EXIT://退出房间
                        break;
                    case RoomEvent.DOWN_CROWDED_MIC://挤下麦
                        if (AvRoomDataManager.get().isSelf(roomEvent.getAccount()))
                            toast(R.string.crowded_down);
//                        updateMatchView(true);
                        break;
                    case RoomEvent.ROOM_MANAGER_ADD://添加管理员权限
                    case RoomEvent.ROOM_MANAGER_REMOVE://移除管理员权限
                        updateView();
                        break;
                    case RoomEvent.ENTER_ROOM:
                        //初始化关注按钮显示
                        setupAttentionView();
                        //初始化魅力值显示
                        setupCharmView();
                    case RoomEvent.ROOM_INFO_UPDATE:
                        updateView();
                        updateRemoteMuteBtn();
                        onMicInListChange();
                        break;
                    case RoomEvent.ADD_BLACK_LIST:
                        onChatRoomMemberBlackAdd(roomEvent.getAccount());
                        break;
                    case RoomEvent.MIC_QUEUE_STATE_CHANGE:
                        onQueueMicStateChange(roomEvent.getMicPosition(), roomEvent.getPosState());
                        break;
                    case RoomEvent.KICK_DOWN_MIC://踢下麦
                        /*getMvpPresenter().downMicroPhone(roomEvent.getMicPosition(), false);*/
                        SingleToastUtil.showToast(mContext.getResources().getString(R.string.kick_mic));
//                        updateMatchView(true);
                        break;
                    case RoomEvent.DOWN_MIC://下麦
                        onDownMicro(roomEvent.getMicPosition());
//                        updateMatchView(true);
                        break;
                    case RoomEvent.UP_MIC://上麦
                        onUpMicro();
//                        updateMatchView(false);
                        break;
                    case RoomEvent.INVITE_UP_MIC://邀请上麦
                        onInviteUpMic(roomEvent.getMicPosition());
//                        updateMatchView(false);
                        break;
                    case RoomEvent.KICK_OUT_ROOM://踢出房间
                        ChatRoomKickOutEvent reason = roomEvent.getReason();
                        if (reason != null && reason.getReason() == ChatRoomKickOutEvent.ChatRoomKickOutReason.CHAT_ROOM_INVALID) {
                            releaseView();
                        }
                        break;
                    case RoomEvent.ROOM_CHARM://魅力值更新
                        // todo
                        updateChairView();
//                        if (gameBinding != null) {
//                            microView.getAdapter().notifyDataSetChanged();
//                        }
                        break;
                    default:
                }
            }
        }, this);
        //房主自动上麦
        ownerUpMic();
//        if (rmvMatch != null)
//            rmvMatch.getRoomMatchState(true);
        //最小化检测
        changeState();

        setupLotteryBoxView();
    }

    private void updateChairView() {
        CoreUtils.send(new RoomAction.OnUpdateChairView());
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void updateRoomBlessingBeast(BlessingBeastAttachment blessingBeastAttachment) {
        if (blessingBeastAttachment != null && !isPlayingBlessingBeast) {
            if (blessingBeastAttachment.getFirst() == CUSTOM_MSG_BLESSING_BEAST &&
                    blessingBeastAttachment.getSecond() == CUSTOM_MSG_BLESSING_BEAST) {//触发福兽
                BlessingBeastInfo blessingBeastInfo = blessingBeastAttachment.getBlessingBeastInfo();
                if (blessingBeastInfo != null) {
                    triggerBlessingBeast(blessingBeastInfo);
                }
            }
        }
    }

    private void setupLotteryBoxView() {
        Json configData = CoreManager.getCore(VersionsCore.class).getConfigData();
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (configData != null && userInfo != null && configData.num("lottery_box_option", 0) != 0) {
            lotteryBoxView.setVisibility(View.VISIBLE);
        } else {
            lotteryBoxView.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化关注按钮
     */
    private void setupAttentionView() {
        // todo
//        MicroViewAdapter microViewAdapter = microView.getAdapter();
//        if (microViewAdapter != null) {
//            if (AvRoomDataManager.get().isRoomOwner()) {//如果我就是房主，那么没有关注按钮
//                microViewAdapter.setAttentionAddBtnEnable(false);
//            } else {
//                if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
//                    CoreManager.getCore(IPraiseCore.class).isPraised(CoreManager.getCore(IAuthCore.class).getCurrentUid(), AvRoomDataManager.get().mCurrentRoomInfo.getUid());
//                }
//            }
//        }
    }

    /**
     * 初始化魅力值
     */
    private void setupCharmView() {
        if (AvRoomDataManager.get().mCurrentRoomInfo != null && AvRoomDataManager.get().mCurrentRoomInfo.getCharmOpen() == 1) {
            CoreManager.getCore(ICharmCore.class).requestRoomCharm(AvRoomDataManager.get().mCurrentRoomInfo.getRoomId(), AvRoomDataManager.get().mCurrentRoomInfo.getUid());
        }
    }

    @CoreEvent(coreClientClass = ICharmClient.class)
    public void onRequestCharmSuccess(JSONObject charmJson) {
        IMNetEaseManager.get().roomCharmList = charmJson;
        // todo
        updateChairView();
//        microView.getAdapter().notifyDataSetChanged();
    }

    private List<ChatRoomMessage> msgFilter(List<ChatRoomMessage> chatRoomMessages) {
        if (ListUtils.isListEmpty(chatRoomMessages))
            return null;
        List<ChatRoomMessage> messages = new ArrayList<>();
        for (int i = chatRoomMessages.size() - 1; i >= 0; i--) {
            if (chatRoomMessages.get(i).getAttachment() instanceof CustomAttachment) {
                if (((CustomAttachment) chatRoomMessages.get(i).getAttachment()).getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT ||
                        ((CustomAttachment) chatRoomMessages.get(i).getAttachment()).getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                    messages.add(chatRoomMessages.get(i));
                }
            }
        }
        return messages;
    }

    private void ownerUpMic() {
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        final String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (AvRoomDataManager.get().isRoomOwner(currentUid)) {
            onOwnerUpMicroClick(-1, currentRoom.getUid());
        }
    }

    /**
     * 监听送礼物和谁来了的消息
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoomMsgCome(RoomTalkEvent.OnRoomMsg roomMsg) {
        ChatRoomMessage message = roomMsg.getMessage();
        CustomAttachment attachment = (CustomAttachment) message.getAttachment();
        if (attachment.getFirst() == CUSTOM_MSG_TYPE_BURST_GIFT) {
            if (attachment instanceof BurstGiftAttachment) {
                BurstGiftAttachment info = (BurstGiftAttachment) attachment;
                if (info.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
                    RewardGiftDialog reward = RewardGiftDialog.newInstance(info.getGiftName(), info.getGiftNum(), info.getPicUrl());
                    reward.show(getChildFragmentManager(), "gift_reward");
                    GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(info.getGiftId());
                    if (giftInfo != null) {
                        giftInfo.setUserGiftPurseNum(giftInfo.getUserGiftPurseNum() + info.getGiftNum());
                    }
                    CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift);
                }
            }
        } else if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
            bottomView.setInputMsgBtnEnable(true);
        } else if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
            bottomView.setInputMsgBtnEnable(false);
        } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT ||
                attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
            giftMsgList.add(0, message);
            if (record != null && record.isShowing()) {
                record.loadData(giftMsgList);
            }
        }
    }

    /**
     * 显示单独赠送的礼物
     *
     * @param attachment
     */
    private void setMsgHeaderGift(GiftAttachment attachment) {
        GiftReceiveInfo giftRecieveInfo = attachment.getGiftRecieveInfo();
        if (giftRecieveInfo == null)
            return;
        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftRecieveInfo.getGiftId());
        if (giftInfo != null) {
            String nick = giftRecieveInfo.getNick();
            String targetNick = giftRecieveInfo.getTargetNick();
            if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
                nick = nick.substring(0, 6) + "...";
            }
            if (!TextUtils.isEmpty(targetNick) && targetNick.length() > 6) {
                targetNick = targetNick.substring(0, 6) + "...";
            }
            tvNick.setText(nick);
            tvContent.setText("赠送给");
            tvNickTarget.setVisibility(View.VISIBLE);
            tvNickTarget.setText(targetNick);
            ImageLoadUtils.loadImage(getContext(), giftInfo.getGiftUrl(), ivGift);
            tvGiftNumber.setText("X" + giftRecieveInfo.getGiftNum());
        }
    }

    /***
     * 显示全麦赠送的礼物
     * @param attachment
     */
    private void setMsgMultiGift(MultiGiftAttachment attachment) {
        MultiGiftReceiveInfo multiGiftRecieveInfo = attachment.getMultiGiftRecieveInfo();
        if (multiGiftRecieveInfo == null) {
            return;
        }
        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(multiGiftRecieveInfo.getGiftId());
        if (giftInfo != null) {
            String nick = multiGiftRecieveInfo.getNick();
            if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
                nick = nick.substring(0, 6) + "...";
            }
            tvNick.setText(nick);
            tvContent.setText("全麦送出");
            tvNickTarget.setVisibility(View.GONE);
            ImageLoadUtils.loadImage(getContext(), giftInfo.getGiftUrl(), ivGift);
            tvGiftNumber.setText("X" + multiGiftRecieveInfo.getGiftNum());
        }
    }

    /**
     * 处理自定义消息
     *
     * @param attachment
     */
    private void dealWithMsg(CustomAttachment attachment) {
        LogUtil.d("dealWithMsg", "second = " + attachment.getSecond());
        if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
            bottomView.setInputMsgBtnEnable(true);
        } else if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
            bottomView.setInputMsgBtnEnable(false);
        }
        if (attachment.getFirst() == CUSTOM_MSG_TYPE_BURST_GIFT) {
            if (attachment instanceof BurstGiftAttachment) {
                BurstGiftAttachment info = (BurstGiftAttachment) attachment;
                if (info.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
                    RewardGiftDialog reward = RewardGiftDialog.newInstance(info.getGiftName(), info.getGiftNum(), info.getPicUrl());
                    reward.show(getChildFragmentManager(), "gift_reward");
                    GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(info.getGiftId());
                    if (giftInfo != null) {
                        giftInfo.setUserGiftPurseNum(giftInfo.getUserGiftPurseNum() + info.getGiftNum());
                    }
                    CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift);
                }
            }
        }
    }

    public void setUserComeAction(UserComeAction userComeAction) {
        this.userComeAction = userComeAction;
    }

    /**
     * 从CongratulationDialog对话框跳转过来
     */
    @CoreEvent(coreClientClass = ICongratulationDialogCoreClient.class)
    public void openGiftDialogFromCongraDialog() {
        CoreManager.notifyClients(ICongratulationDialogCoreClient.class, ICongratulationDialogCoreClient.CLOSE_GIFT_DIALOG_FROM_CONGRA_DIALOG);//通知giftdialog内部关闭对话框
        CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        new Handler().postDelayed(() -> {
            GiftDialog giftDialog = new GiftDialog(getActivity(), AvRoomDataManager.get().mMicQueueMemberMap, 1);
            giftDialog.setGiftDialogBtnClickListener(HomePersonalRoomFragment.this);
            giftDialog.show();
        }, 500);
    }

    /**
     * 个人信息跳转过来，打开送礼对话框
     *
     * @param userId
     */
    @CoreEvent(coreClientClass = IUserInfoDialogCoreClient.class)
    public void showGiftDialogFromUserInfo(String userId) {
        GiftDialog giftDialog = new GiftDialog(getContext(), Long.valueOf(userId), false);
        giftDialog.setGiftDialogBtnClickListener(this);
        giftDialog.show();
    }

    @Override
    public void onNeedCharge() {
        if (getActivity() == null) {
            return;
        }
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    MyWalletNewActivity.start(getActivity());
                    fragment.dismiss();
                }
            }
        }).show(getActivity().getSupportFragmentManager(), "charge");
    }

    @Override
    public void OnBannerClick(int position) {
        if (actionDialogInfoList == null) {
            return;
        }
        BannerInfo bannerInfo = actionDialogInfoList.get(position);
        if (bannerInfo != null) {
            if (mBannerClickModel == null) {
                mBannerClickModel = new BannerClickModel();
            }
            mBannerClickModel.bannerStat(bannerInfo.getBannerId(), BannerClickModel.ROOM_ACTIVITY_BANNER);
            if (bannerInfo.getSkipType() == AVRoomActivity.NEW_USER_SKIP_TYPE) {//只有心动礼包才跳原生页面
                if (oneAudioDialog != null && oneAudioDialog.getDialog() != null && oneAudioDialog.getDialog().isShowing()) {
                    return;
                }
                if (getFragmentManager() != null) {
                    oneAudioDialog = new OneAudioDialog();
                    oneAudioDialog.show(getFragmentManager(), "");
                }
            } else {
                //房间内轮播广告点击事件
                if (TextUtils.isEmpty(bannerInfo.getSkipUri())) {
                    return;
                }
                UmengEventUtil.getInstance().onRoomBanner(getContext(), bannerInfo.getBannerName(), String.valueOf(bannerInfo.getBannerId()),
                        bannerInfo.getSkipUri(), position + 1);
                CommonWebViewActivity.start(getContext(), bannerInfo.getSkipUri());
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // 释放公屏和麦上的所有信息信息和动画
        long roomUid = intent.getLongExtra(Constants.ROOM_UID, 0);
        if (roomUid != 0 && roomUid != AvRoomDataManager.get().mCurrentRoomInfo.getUid()) {
            releaseView();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (musicPlayerView != null) {
//            musicPlayerView.updateVoiceValue();
//        }
        dealUserComeMsg();//检查一次是否有用户进人房间的消息未处理
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            getMvpPresenter().queryRoomBlessingBeast(String.valueOf(roomInfo.getUid()));
        }

    }

    @Override
    public void onSetListener() {
        if (getActivity() == null) {
            return;
        }

        lotteryBoxView.setOnClickListener(view -> {
            PoundEggDialog lotteryBoxDialog = PoundEggDialog.newOnlineUserListInstance();
            lotteryBoxDialog.show(getChildFragmentManager(), null);
        });

        sharedPreferences = getActivity().getSharedPreferences(INPUT_FILE, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        inputEdit.setOnFocusChangeListener((v, hasFocus) -> {
            if (!(v instanceof EditText)) {
                return;
            }
            if (hasFocus) {
                String content = sharedPreferences.getString(INPUT_FILE, "");
                ((EditText) v).setText(content);
                ((EditText) v).setSelection(((EditText) v).length());//将光标移到末尾
            } else {
                String content = ((EditText) v).getText().toString();
                editor.putString(INPUT_FILE, content);
                editor.apply();
            }
        });
        wrapper = new GameRoomBottomViewWrapper();
        bottomView.setBottomViewListener(wrapper);

        inputLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                inputEdit.clearFocus();
                inputLayout.setVisibility(View.GONE);
                hideKeyBoard();
                return false;
            }
        });
        softKeyboardListener();
    }

    private void onOwnerUpMicroClick(final int micPosition, final long currentUid) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null) return;
        getMvpPresenter().upMicroPhone(micPosition, currentUid + "", false);
    }

    private void releaseView() {
        // todo
//        microView.release();
//        if (musicPlayerView != null) {
//            musicPlayerView.release();
//        }
        //发送dismiss事件
        CoreUtils.send(new SetAudioPlayerEvent.OnDismissDialog());
        giftMsgList = null;
    }

    private void updateView() {
        // todo
        updateChairView();
//        microView.getAdapter().setShowCharm(AvRoomDataManager.get().mCurrentRoomInfo != null && AvRoomDataManager.get().mCurrentRoomInfo.getCharmOpen() == 1);
//        microView.getAdapter().notifyDataSetChanged();

        // 更新底栏
        showBottomViewForDifRole();
    }

    /**
     * 根据角色显示不同的状态
     */
    private void showBottomViewForDifRole() {
        boolean isOnMic = AvRoomDataManager.get().isOnMic(myUid);
        // 更新播放器界面
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo == null) return;
        roomOwnner = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(mCurrentRoomInfo.getUid());

        if (isOnMic) {
            bottomView.showHomePartyUpMicBottom();
        } else {
            //发送dismiss事件
            CoreUtils.send(new SetAudioPlayerEvent.OnDismissDialog());
            bottomView.showHomePartyDownMicBottom();
        }
        //解决最小化远程声音未关闭问题
        updateRemoteMuteBtn();
    }

    private void updateMicBtn() {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo != null) {
            if (RtcEngineManager.get().isAudienceRole()) {
                bottomView.setMicBtnEnable(false);
                bottomView.setMicBtnOpen(false);
            } else {
                if (RtcEngineManager.get().isMute()) {
                    bottomView.setMicBtnEnable(true);
                    bottomView.setMicBtnOpen(false);
                } else {
                    bottomView.setMicBtnEnable(true);
                    bottomView.setMicBtnOpen(true);
                }
            }
        } else {
            bottomView.setMicBtnEnable(false);
            bottomView.setMicBtnOpen(false);
        }

    }

    private void updateRemoteMuteBtn() {
        if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
            bottomView.setRemoteMuteOpen(!RtcEngineManager.get().isRemoteMute());
        }
    }

    public void onMicStateChanged() {
        updateMicBtn();
    }

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceiveRecentContactChanged() {
        changeState();
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onBottomMarkClear() {
        changeState();
    }

    private void changeState() {
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        if (unreadCount > 0) {
            bottomView.showMsgMark(true);
        } else {
            bottomView.showMsgMark(false);
        }

        //+号更多按钮的红点
        bottomView.showMoreMark(RoomBottomMarkManager.getInstance(getContext()).isHaveButtonMark());
    }

    //------------------------------IShareCoreClient----------------------------------
    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoom() {
        toast("分享成功");
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoomFail() {
        toast("分享失败，请重试");
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoomCancel() {
        getDialogManager().dismissDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        releaseView();
        isShowing = false;

        // todo
//        if (microView != null && microView.getAdapter() != null) {
//            microView.getAdapter().setOnMicroItemClickListener(null);
//        }
        if (blessingBeastCountDownTimer != null) {
            blessingBeastCountDownTimer.cancel();
            blessingBeastCountDownTimer = null;
        }
    }

    /**
     * 收到发送快捷文案的消息
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void sendQuickMsg(SendQuickMsgEvent event) {
        hideKeyBoard();
        if (checkRoomChat()) return;
        String content = event.getMsg();
        UmengEventUtil.getInstance().onRoomQuickMsg(getContext(), content);
        getMvpPresenter().sendTextMsg(content);
    }

    @OnClick({R2.id.input_send, R2.id.play_together})
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.input_send) {
            if (checkRoomChat()) return;
            String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
            String content = inputEdit.getText().toString().trim();
            if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(content)) {
                if (content.matches(sensitiveWordData)) {
                    SingleToastUtil.showToast(this.getString(R.string.sensitive_word_data));
                    return;
                }
            }
            getMvpPresenter().sendTextMsg(content);
            inputEdit.setText("");

        } else if (i == R.id.play_together) {
            if (!CoreManager.getCore(IFaceCore.class).isShowingFace()) {
                FaceInfo faceInfo = CoreManager.getCore(IFaceCore.class).getPlayTogetherFace();
                if (faceInfo != null) {
                    CoreManager.getCore(IFaceCore.class).sendAllFace(faceInfo);
                } else {
                    toast("加载失败，请重试!");
                }
            }

        } else {
        }
    }

    /**
     * 检查房间是否可以发送公屏消息
     *
     * @return
     */
    private boolean checkRoomChat() {
        if (ChatUtil.checkRoomChatBanned())
            return true;
        if (CoreManager.getCore(VersionsCore.class).getConfigData() == null || !CoreManager.getCore(VersionsCore.class).getConfigData().boo("publicChatCloseValidation")) {//关闭公聊限制验证  true 关闭验证  false 开启验证
            if (!RealNameAuthStatusChecker.getInstance().phoneStatus(getActivity(), getResources().getString(R.string.real_name_auth_tips,
                    getResources().getString(R.string.real_name_auth_tips_send_msg)))) {
                return true;
            }
        }

        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo != null && mCurrentRoomInfo.getPublicChatSwitch() == 1) {
            toast("公屏消息已经被管理员禁止，请稍候再试");
            return true;
        }
        return false;
    }

    private void onUserAvatarClick() {
        List<ButtonItem> buttonItems = new ArrayList<>();
        if (!AvRoomDataManager.get().isRoomOwner()) {
            ButtonItem buttonItem1 = new ButtonItem("送礼物", new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    if (AvRoomDataManager.get().mMicQueueMemberMap == null) {
                        return;
                    }
                    GiftDialog giftDialog = new GiftDialog(getContext(), AvRoomDataManager.get().mMicQueueMemberMap, null);
                    giftDialog.setGiftDialogBtnClickListener(HomePersonalRoomFragment.this);
                    giftDialog.show();
                }
            });
            buttonItems.add(buttonItem1);
        }
        ButtonItem buttonItem2 = ButtonItemFactory.createCheckUserInfoDialogItem(mContext,
                String.valueOf(roomOwnner.getUid()));
        buttonItems.add(buttonItem2);
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    /**
     * 打开软键盘并显示头布局
     */
    public void showKeyBoard() {
        inputLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = null;
                if (getActivity() != null) {
                    imm = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                }
                if (imm == null) return;
                imm.showSoftInput(inputEdit, InputMethodManager.SHOW_FORCED);
            }
        }, 80);
    }

    /**
     * 隐藏软键盘并隐藏头布局
     */
    public void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm == null) return;
        imm.hideSoftInputFromWindow(inputEdit.getWindowToken(), 0);
    }

    /**
     * 软键盘显示与隐藏的监听
     */
    private void softKeyboardListener() {
        SoftKeyBoardListener.setListener(getActivity(), mOnSoftKeyBoardChangeListener);
    }

    @Override
    public void onRechargeBtnClick() {
        MyWalletNewActivity.start(getContext());
    }

    /**
     * 打开CP礼物戒指栏
     *
     * @param uid
     */
    @CoreEvent(coreClientClass = IRoomCpCoreClient.class)
    public void onOpenCpGiftList(long uid) {
        com.tongdaxing.xchat_framework.util.util.LogUtil.d("IRoomCpCoreClient onOpenCpGiftList");
        GiftDialog giftDialog = new GiftDialog(mContext, uid, GiftDialog.POSITION_CP);
        giftDialog.setGiftDialogBtnClickListener(this);
        giftDialog.show();
    }

    /**
     * 发送CP礼物
     */
    @Override
    public void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid) {
        if (giftInfo != null) {
            L.debug(GiftAction.TAG, "HomePersonalRoomFragment onSendCpGiftBtnClick giftInfo=%s, uid=%d", giftInfo, uid);
            RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (currentRoomInfo == null) return;
            CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftInfo.getGiftId(), uid,
                    1, currentRoomInfo.getUid(), this);
        }
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendPersonalGiftFail(String message) {
        com.tongdaxing.xchat_framework.util.util.LogUtil.d("onSendPersonalGiftFail");
        SingleToastUtil.showToast(message);
    }

    @CoreEvent(coreClientClass = IRoomCpCoreClient.class)
    public void onCpInvite(CpGiftReceiveInfo giftReceiveInfo) {
        L.info(TAG, "IRoomCpCoreClient onCpInvite giftReceiveInfo = %s", giftReceiveInfo);
        //收到邀请
        if (giftReceiveInfo.getRecUid() == CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid()) {
            //自己是接收方才处理消息
            if (roomCpInviteDialog != null
                    && roomCpInviteDialog.getDialog() != null
                    && roomCpInviteDialog.getDialog().isShowing()
                    && !roomCpInviteDialog.isRemoving()) {
                //dialog is showing so do something
                L.debug(TAG, "resend cp invite: reinvite me");
                roomCpInviteDialog.dismissAllowingStateLoss();
            }
            roomCpInviteDialog = RoomCpInviteDialog.instance(giftReceiveInfo);
            roomCpInviteDialog.show(getFragmentManager(), "onCpInvite");
        }
    }

    @CoreEvent(coreClientClass = IRoomCpCoreClient.class)
    public void onCpInviteRefuse(CpGiftReceiveInfo giftReceiveInfo) {
        com.tongdaxing.xchat_framework.util.util.LogUtil.d("IRoomCpCoreClient onCpInviteRefuse 收到拒绝CP");
        //收到拒绝CP
        if (giftReceiveInfo.getInviteUid() == CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid()) {
            //自己是接收方才处理消息
            SimpleTextDialog.instance(getString(R.string.cp_room_refused)).show(getFragmentManager(), "onCpInviteRefuse");
        }
    }

    @CoreEvent(coreClientClass = SquareClient.class)
    public void onRoomCpTopClick(long recUserId) {
        L.debug(TAG, "45 SquareClient 房间顶部公屏消息 展示全服CP表白 recUserId = %d", recUserId);
        //45 房间顶部公屏消息 展示全服CP表白
        loadRoomStatus(recUserId);
    }

    public void loadRoomStatus(long recUserId) {
        getDialogManager().showProgressDialog(getActivity(), "请稍后...");
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("queryUid", String.valueOf(recUserId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getTheRoomStatus(), params, new OkHttpManager.MyCallBack<ServiceResult<RoomStatus>>() {
            @Override
            public void onError(Exception e) {
                getDialogManager().dismissDialog();
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RoomStatus> response) {
                getDialogManager().dismissDialog();
                if (response == null) {
                    return;
                }
                if (response.isSuccess() && response.getData() != null && response.getData().getUid() > 0) {
                    AVRoomActivity.start(getActivity(), response.getData().getUid());
                }
            }
        });
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number, int sendGiftType) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) return;
        if (giftInfo == null)
            return;
        List<Long> uids = new ArrayList<>();
        uids.add(uid);
        CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), uids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos,
                                   int number, int sendGiftType) {
        if (giftInfo == null) return;
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) return;
        List<Long> targetUids = new ArrayList<>();
        for (int i = 0; i < micMemberInfos.size(); i++) {
            targetUids.add(micMemberInfos.get(i).getUid());
        }
        CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), targetUids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
    }

    void onUpMicro() {
        showBottomViewForDifRole();
        updateMicBtn();
        refreshMicroAdapter();
    }

    private void refreshMicroAdapter() {
        // todo
        updateChairView();
//        microView.getAdapter().notifyDataSetChanged();
    }

    void onInviteUpMic(final int micPosition) {
        AvRoomDataManager.get().mIsNeedOpenMic = false;
        getMvpPresenter().upMicroPhone(micPosition, String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()), true);
        ((BaseMvpActivity) getActivity()).getDialogManager()
                .showOkBigTips(getString(R.string.tip_tips),
                        getString(R.string.embrace_on_mic), true, null);
    }

    void onDownMicro(int micPosition) {
        showBottomViewForDifRole();
        updateMicBtn();
        // todo
        updateChairView();
//        microView.getAdapter().notifyDataSetChanged();
    }

    void onQueueMicStateChange(int micPosition, int micPosState) {
        // todo
        updateChairView();
//        microView.getAdapter().notifyItemChanged(micPosition);
        onMicStateChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlayerOnMicClick(UserEvent.OnAdminUserShow show) {
        final List<ButtonItem> buttonItems = new ArrayList<>();
        List<ButtonItem> items = ButtonItemFactory.createAllRoomPublicScreenButtonItems(mContext, String.valueOf(show.getPlayerId()));
        if (items == null) return;
        buttonItems.addAll(items);
        ((BaseMvpActivity) mContext).getDialogManager().showCommonPopupDialog(buttonItems, "取消");
//        getMvpPresenter().avatarClick(AvRoomDataManager.get().getMicPosition(show.getPlayerId()));
    }

//    @Override
//    public void onSharePlatformClick(Platform platform) {
//        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
//        if (currentRoomInfo != null) {
//            CoreManager.getCore(IShareCore.class).shareRoom(platform, currentRoomInfo.getUid(), currentRoomInfo.getTitle());
//        }
//    }

    @Override
    public void resultLoadNormalMembers(List<ChatRoomMember> chatRoomMemberList) {

    }

    @Override
    public SparseArray<ButtonItem> getAvatarButtonItemList(final int position,
                                                           final ChatRoomMember chatRoomMember, RoomInfo currentRoom) {
        if (chatRoomMember == null || currentRoom == null) {
            return null;
        }
        SparseArray<ButtonItem> buttonItemMap = new SparseArray<>(10);
        ButtonItem buttonItem1 = ButtonItemFactory.createSendGiftItem(getContext(), chatRoomMember, this);
        ButtonItem buttonItem2 = ButtonItemFactory.createLockMicItem(position, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                getMvpPresenter().closeMicroPhone(position);
            }
        });
        ButtonItem buttonItem3 = ButtonItemFactory.createKickDownMicItem(chatRoomMember.getAccount());
        ButtonItem buttonItem4 = ButtonItemFactory.createKickOutRoomItem(mContext, chatRoomMember, String.valueOf(currentRoom.getRoomId()), chatRoomMember.getAccount());
        ButtonItem buttonItem5 = ButtonItemFactory.createCheckUserInfoDialogItem(getContext(), chatRoomMember.getAccount());
        ButtonItem buttonItem6 = ButtonItemFactory.createDownMicItem();
        ButtonItem buttonItem7 = ButtonItemFactory.createFreeMicItem(position, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                getMvpPresenter().openMicroPhone(position);
            }
        });
        ButtonItem buttonItem8 = ButtonItemFactory.createMarkManagerListItem(String.valueOf(currentRoom.getRoomId()), chatRoomMember.getAccount(), true);
        ButtonItem buttonItem9 = ButtonItemFactory.createMarkManagerListItem(String.valueOf(currentRoom.getRoomId()), chatRoomMember.getAccount(), false);
        ButtonItem buttonItem10 = ButtonItemFactory.createMarkBlackListItem(getContext(), chatRoomMember, String.valueOf(currentRoom.getRoomId()));
        buttonItemMap.put(0, buttonItem1);
        buttonItemMap.put(1, buttonItem2);
        buttonItemMap.put(2, buttonItem3);
        buttonItemMap.put(3, buttonItem4);
        buttonItemMap.put(4, buttonItem5);
        buttonItemMap.put(5, buttonItem6);
        buttonItemMap.put(6, buttonItem7);
        buttonItemMap.put(7, buttonItem8);
        buttonItemMap.put(8, buttonItem9);
        buttonItemMap.put(9, buttonItem10);
        return buttonItemMap;
    }

    @Override
    public void showMicAvatarClickDialog(List<ButtonItem> buttonItemList) {
        if (ListUtils.isListEmpty(buttonItemList)) {
            return;
        }
        if (isActivityDestroyed(getActivity())) {
            return;
        }
        getDialogManager().showCommonPopupDialog(buttonItemList, getString(R.string.cancel));
    }

    @Override
    public void showGiftDialog(ChatRoomMember chatRoomMember) {
        if (isActivityDestroyed(getActivity())) {
            return;
        }
        GiftDialog giftDialog = new GiftDialog(getActivity(), AvRoomDataManager.get().mMicQueueMemberMap, chatRoomMember);
        giftDialog.setGiftDialogBtnClickListener(this);
        giftDialog.show();
    }

    @Override
    public void showRoomOwnerLeaveGiftDialog(ChatRoomMember chatRoomMember) {
        if (isActivityDestroyed(getActivity())) {
            return;
        }
        MicMemberInfo micMemberInfo = new MicMemberInfo();
        micMemberInfo.setNick(chatRoomMember.getNick());
        micMemberInfo.setAvatar(chatRoomMember.getAvatar());
        micMemberInfo.setMicPosition(-1);
        micMemberInfo.setUid(JavaUtil.str2long(chatRoomMember.getAccount()));

        GiftDialog giftDialog = new GiftDialog(getActivity(), micMemberInfo);
        giftDialog.setGiftDialogBtnClickListener(this);
        giftDialog.show();
    }

    @Override
    public void showOwnerSelfInfo(ChatRoomMember chatRoomMember) {
        if (isActivityDestroyed(getActivity())) {
            return;
        }
        UserInfoDialogManager.showDialogFragment(getContext(), JavaUtil.str2long(chatRoomMember.getAccount()));
    }

    @Override
    public void notifyRefresh() {
        rlHomePartyRoomFragment.postDelayed(() -> {
            RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(-1);
            if (null == roomQueueInfo) return;
            if (roomQueueInfo.mChatRoomMember == null) {
//                    toast("网络错误");
//                    getActivity().finish();
                LogUtil.e(TAG, "房主信息空的");
                ownerUpMic();
            }
        }, 500);
        // todo
        updateChairView();
//        MicroViewAdapter microViewAdapter = microView.getAdapter();
//        if (microViewAdapter != null) {
//            microViewAdapter.notifyDataSetChanged();
//        }
    }

    @Override
    public void showMicAvatarUserInfoDialog(String uId) {
        if (TextUtils.isEmpty(uId)) {
            return;
        }
        if (isActivityDestroyed(getActivity())) {
            return;
        }
        UserInfoDialogManager.showDialogFragment(getActivity(), JavaUtil.str2long(uId));
    }

    @Override
    public void kickDownMicroPhoneSuccess() {
        updateMicBtn();
        toast(R.string.kick_mic);
    }

    @Override
    public void showOwnerClickDialog(final RoomMicInfo roomMicInfo, final int micPosition,
                                     final long roomUid, boolean isOwner) {
        List<ButtonItem> buttonItems = new ArrayList<>(4);
        final ButtonItem buttonItem1 = new ButtonItem(getString(R.string.embrace_up_mic), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                RoomInviteActivity.openActivity(getActivity(), micPosition);
            }
        });
        ButtonItem buttonItem2 = new ButtonItem(roomMicInfo.isMicMute() ? getString(R.string.no_forbid_mic) : getString(R.string.forbid_mic), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (roomMicInfo.isMicMute()) {
                    getMvpPresenter().openMicroPhone(micPosition);
                } else {
                    getMvpPresenter().closeMicroPhone(micPosition);
                }
            }
        });
        ButtonItem buttonItem3 = new ButtonItem(roomMicInfo.isMicLock() ? getString(R.string.unlock_mic) :
                getString(R.string.lock_mic), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (roomMicInfo.isMicLock()) {
                    getMvpPresenter().unLockMicroPhone(micPosition);
                } else {
                    getMvpPresenter().lockMicroPhone(micPosition, roomUid);
                }
            }
        });
        ButtonItem buttonItem4 = new ButtonItem("移到此座位", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                getMvpPresenter().upMicroPhone(micPosition, CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", false);
            }
        });
        buttonItems.add(buttonItem1);
        buttonItems.add(buttonItem2);
        buttonItems.add(buttonItem3);
        if (!isOwner) {
            buttonItems.add(buttonItem4);
        }
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    @Override
    public void chatRoomReConnectView() {
        // todo
        updateChairView();
//        if (microView != null && microView.getAdapter() != null)
//            microView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void notifyBottomBtnState() {
        showBottomViewForDifRole();
    }

    @Override
    public void onAvatarBtnClick(int position) {
        getMvpPresenter().avatarClick(position);
    }

    @Override
    public void onUpMicBtnClick(int position, ChatRoomMember chatRoomMember) {
        getMvpPresenter().microPhonePositionClick(position, chatRoomMember);
    }

    @Override
    public void onLockBtnClick(int position) {
        getMvpPresenter().unLockMicroPhone(position);
    }

    @Override
    public void onRoomSettingsClick() {

        if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
            RoomTopicActivity.start(getContext());
//            RoomSettingActivity.start(getContext(), AvRoomDataManager.get().mCurrentRoomInfo);
        } else {
            if (isActivityDestroyed(getActivity())) {
                return;
            }
            RoomTopicDIalog roomTopicDIalog = new RoomTopicDIalog();
            roomTopicDIalog.show(getChildFragmentManager());
        }
    }

    @Override
    public void onContributeListClick() {
        BigListDataDialog bigListDataDialog = BigListDataDialog.newContributionListInstance(getActivity());
        bigListDataDialog.setSelectOptionAction(new BigListDataDialog.SelectOptionAction() {
            @Override
            public void optionClick() {
                getDialogManager().showProgressDialog(mContext, "请稍后");
            }

            @Override
            public void onDataResponse() {

                //请求结束前退出可能会导致奔溃，直接捕获没关系
                try {
                    getDialogManager().dismissDialog();
                } catch (Exception e) {

                }

            }
        });
        bigListDataDialog.show(getChildFragmentManager());

//        ListDataDialog.newContributionListInstance(getActivity()).show(getChildFragmentManager());
    }

    @Override
    public void onAttentionAddBtnClick(View attentionBtn) {
        getDialogManager().showProgressDialog(getContext(), getResources().getString(R.string.please_wait));
        CoreManager.getCore(IPraiseCore.class).praise(AvRoomDataManager.get().mCurrentRoomInfo.getUid());
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(boolean Boolean, long uid) {
        if (uid == Long.valueOf(AvRoomDataManager.get().mRoomCreateMember.getAccount())) {
            // todo
//            MicroViewAdapter adapter = microView.getAdapter();
//            if (adapter != null) {
//                adapter.setAttentionAddBtnEnable(!isLiked);//关注按钮
//            }
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLikedFail(String error) {
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void onAvatarSendMsgClick(int micPosition) {
        if (ChatUtil.checkRoomChatBanned())
            return;
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) return;
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null) return;
        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
        RoomMicInfo roomMicInfo = roomQueueInfo.mRoomMicInfo;
        if (chatRoomMember == null && micPosition == -1) {
            chatRoomMember = new ChatRoomMember();
            RoomInfo info = AvRoomDataManager.get().mCurrentRoomInfo;
            chatRoomMember.setNick("房主");
            chatRoomMember.setAvatar("");
            chatRoomMember.setAccount(info.getUid() + "");
        }
        if (chatRoomMember == null || roomMicInfo == null) return;
        String str = "@" + chatRoomMember.getNick() + " ";
        if (inputLayout.getVisibility() == View.VISIBLE) {
            if (inputEdit.getText() != null) {
                String content = sharedPreferences.getString(INPUT_FILE, "");
                str = content + str;
            }
            editor.putString(INPUT_FILE, str);
            editor.apply();
        } else {
            inputLayout.setVisibility(View.VISIBLE);
            String content = sharedPreferences.getString(INPUT_FILE, "");
            str = content + str;
            editor.putString(INPUT_FILE, str);
            editor.apply();
            inputEdit.requestFocus();
            showKeyBoard();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSendMsgClick(WeekCpEvent.OnWeekCpSoulSend soulSend) {
        if (wrapper != null) {
            wrapper.onSendMsgBtnClick();
        }
    }

    public void sendGift() {
        if (isActivityDestroyed(getActivity())) {
            return;
        }
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(-1);
        if (roomQueueInfo == null || roomQueueInfo.mRoomMicInfo == null) {
            L.error(TAG, "microPhonePositionClick roomQueueInfo or roomQueueInfo.mRoomMicInfo is null.");
            return;
        }
        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;

        if (chatRoomMember == null) {
            chatRoomMember = new ChatRoomMember();
            RoomInfo info = AvRoomDataManager.get().mCurrentRoomInfo;
            chatRoomMember.setNick("房主");
            chatRoomMember.setAvatar("");
            chatRoomMember.setAccount(info.getUid() + "");
        }

        GiftDialog giftDialog = new GiftDialog(getActivity(), AvRoomDataManager.get().mMicQueueMemberMap, chatRoomMember);
        giftDialog.setGiftDialogBtnClickListener(HomePersonalRoomFragment.this);
        giftDialog.show();
    }

    @Override
    public void showMicInListDialog() {
        if (isActivityDestroyed(getActivity())) {
            return;
        }

        if (!micInListOption)
            return;
        MicInListDialog micInListDialog = new MicInListDialog(mContext);
        boolean isRoomOwner = AvRoomDataManager.get().isRoomOwner();
        micInListDialog.isAdmin = AvRoomDataManager.get().isRoomAdmin() || isRoomOwner;
        micInListDialog.isRoomOwner = isRoomOwner;
        micInListDialog.iSubmitAction = () -> {
            boolean checkInMicInlist = AvRoomDataManager.get().checkInMicInlist();
            if (checkInMicInlist)
                getMvpPresenter().removeMicInList();
            else
                getMvpPresenter().addMicInList();
        };
        micInListDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200 && resultCode == 100) {
            if (data != null && data.getExtras() != null) {
                String account = data.getExtras().getString("account");
                if (TextUtils.isEmpty(account)) return;
                int micPosition = data.getExtras().getInt(Constants.KEY_POSITION, Integer.MIN_VALUE);
                if (micPosition == Integer.MIN_VALUE) return;

                //抱人上麦
                getMvpPresenter().inviteMicroPhone(JavaUtil.str2long(account), micPosition);
            }
        } else if (requestCode == Pingpp.REQUEST_CODE_PAYMENT) {
            if (data != null && data.getExtras() != null) {
                String result = data.getExtras().getString("pay_result");
                if (result != null) {

                    /* 处理返回值
                     * "success" - 支付成功
                     * "fail"    - 支付失败
                     * "cancel"  - 取消支付
                     * "invalid" - 支付插件未安装（一般是微信客户端未安装的情况）
                     * "unknown" - app进程异常被杀死(一般是低内存状态下,app进程被杀死)
                     */
                    // 错误信息
                    String errorMsg = data.getExtras().getString("error_msg");
                    String extraMsg = data.getExtras().getString("extra_msg");
                    CoreManager.notifyClients(IChargeClient.class, IChargeClient.chargeAction, result);


//                    LogUtil.d(TAG, "errorMsg:" + errorMsg + "extraMsg:" + extraMsg);
                    if ("success".equals(result)) {
                        onPaySuccess();
                    } else if ("cancel".equals(result)) {
                        SingleToastUtil.showToast("支付被取消！");
                    } else {
                        SingleToastUtil.showToast("支付失败！");
                    }
                }
            }
        }
    }

    private void onPaySuccess() {
        SingleToastUtil.showToast("支付成功！");
        //刷新礼物数据
        CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        CongratulationDialog congratulationDialog = new CongratulationDialog();
        congratulationDialog.show(getFragmentManager(), "");
        if (actionDialogInfoList != null) {
            for (int i = 0; i < actionDialogInfoList.size(); i++) {
                if (actionDialogInfoList.get(i).getSkipType() == AVRoomActivity.NEW_USER_SKIP_TYPE) {
                    actionDialogInfoList.remove(i);
                    break;
                }
            }
            initBanner(actionDialogInfoList);
        }
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onJoinPayWxResp(int errCode) {
        if (errCode == 0) {
            onPaySuccess();
        } else if (errCode == -2) {
            SingleToastUtil.showToast("支付被取消！");
        } else {
            SingleToastUtil.showToast("支付失败！");
        }
    }

    void onChatRoomMemberBlackAdd(String account) {
        //拉黑
        if (AvRoomDataManager.get().isOnMic(JavaUtil.str2long(account))) {
            int micPosition = AvRoomDataManager.get().getMicPosition(JavaUtil.str2long(account));
            getMvpPresenter().downMicroPhone(micPosition, true);
        }

        ListIterator<ChatRoomMember> memberListIterator = AvRoomDataManager.get().mRoomManagerList.listIterator();
        for (; memberListIterator.hasNext(); ) {
            if (Objects.equals(memberListIterator.next().getAccount(), account)) {
                memberListIterator.remove();
            }
        }


        if (AvRoomDataManager.get().isRoomOwner(account)) {
            //当前是房主
            AvRoomDataManager.get().mRoomCreateMember = null;
        } else {
            //当前不是房主，从游客集合中移除
//                ListIterator<ChatRoomMember> iterator = guestMembers.listIterator();+
//                for (; iterator.hasNext(); ) {
//                    ChatRoomMember member = iterator.next();
//                    if (account.equals(member.getAccount())) {
//                        iterator.remove();
//                        break;
//                    }
//                }
        }



       /* if (Objects.equals(account, String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()))) {
            //退出房间，
            AvRoomDataManager.get().releaseAllAudioPlayer();
        }*/
    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void dealUserComeMsg() {
        if (isShowing)
            return;
        final RoomMemberComeInfo comeInfo = AvRoomDataManager.get().getAndRemoveFirstMemberComeInfo();
        if (comeInfo != null) {
            isShowing = true;
            /*显示座驾动画*/
            if (userComeAction != null) {
                userComeAction.showCar(comeInfo.getCarImgUrl());
            }

            /*显示弹幕 等级标志 + XXX + 骑着“XXX” 来了*/
            int level = comeInfo.getExperLevel();
//            level = 39;
            if (level >= 10) {//大于10级，显示用户进来的弹幕
                if (level > 50) {
                    level = 50;
                }
                int drawableId = getResources().getIdentifier("lv" + level, "drawable", mContext.getPackageName());
                roomIvComeLevel.setImageResource(drawableId);
                roomIvComeLevel.setVisibility(View.VISIBLE);

                if (level < 20) {
                    layoutComeMsg.setBackgroundResource(R.drawable.bg_come_msg_tip_lv20_29);
                } else if (level < 30) {
                    layoutComeMsg.setBackgroundResource(R.drawable.bg_come_msg_tip_lv30_39);
                } else if (level < 40) {
                    layoutComeMsg.setBackgroundResource(R.drawable.bg_come_msg_tip_lv40_49);
                } else {
                    layoutComeMsg.setBackgroundResource(R.drawable.bg_come_msg_tip_lv50_59);
                }
                tvMsgName.setText(comeInfo.getNickName());
                String carName = comeInfo.getCarName();
                tvMsgCar.setText(TextUtils.isEmpty(carName) ? getString(R.string.come_msg_tip_not_car) : getString(R.string.come_msg_tip_car, carName));

                //做弹幕动画
                float inInterval = 0.16f;
                float inSpeed = 3f;
                float inValue = inInterval * inSpeed;

                float middleInterval = 0.95f;
                float middleSpeed = 0.06f;
                float middleValue = inValue + (middleInterval - inInterval) * middleSpeed;

                float outSpeed = 2.65f;
                TranslateAnimation animation = new TranslateAnimation(ScreenUtil.getDisplayWidth(), -getResources().getDimensionPixelOffset(R.dimen.layout_come_msg_width),
                        0, 0);
                animation.setDuration(4000);
                animation.setInterpolator(new Interpolator() {
                    @Override
                    public float getInterpolation(float v) {
                        if (v < inInterval) {
                            return inSpeed * v;
                        } else if (v < middleInterval) {
                            return inValue + middleSpeed * (v - inInterval);
                        } else {
                            return middleValue + outSpeed * (v - middleInterval);
                        }
                    }
                });
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        layoutComeMsg.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        layoutComeMsg.setVisibility(View.INVISIBLE);
//                        AvRoomDataManager.get().addMemberComeInfo(comeInfo);
                        isShowing = false;
                        if (AvRoomDataManager.get().getMemberComeSize() > 0) {//队列中还有其他弹幕未处理完
                            dealUserComeMsg();//继续处理
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                });
                layoutComeMsg.startAnimation(animation);
            } else {
                roomIvComeLevel.setVisibility(View.GONE);
                isShowing = false;
            }

        } else {
            isShowing = false;
        }
    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void sendMsg(String msg) {
        if (getMvpPresenter() != null) {
            getMvpPresenter().sendTextMsg(msg);
        }

    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void micInlistMoveToTop(int micPosition, String roomId, String value) {
        if (getMvpPresenter() != null) {

            if (TextUtils.isEmpty(roomId))
                roomId = AvRoomDataManager.get().mCurrentRoomInfo.getRoomId() + "";
            getMvpPresenter().updataQueueExBySdk(micPosition, roomId, value);
        }

    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onMicInListToUpMic(int micPosition, String uid) {
        if (getMvpPresenter() != null && micPosition != -1) {

            AvRoomDataManager.get().mIsNeedOpenMic = false;
            getMvpPresenter().upMicroPhone(micPosition, uid, false, true);
            toast("您上麦了");
        }
    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onMicInListChange() {
        int size = AvRoomDataManager.get().mMicInListMap.size();
        com.tongdaxing.xchat_framework.util.util.LogUtil.d("有人上麦了 onMicInListChange size" + size);
        boolean b = size == 0 || !micInListOption || (!AvRoomDataManager.get().isRoomAdmin() && !AvRoomDataManager.get().isRoomOwner());
        buMicInListCount.setVisibility(b ? View.GONE : View.VISIBLE);
        buMicInListCount.setText(size + "");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onMicInListChange();
        L.debug(TAG, "onViewCreated");
    }

    /**
     * 发现麦上说话者异常
     */
    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onSpeakerException(String exceptionUserId) {
        RoomInfo currRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currRoomInfo != null) {
            getMvpPresenter().checkSpeakerException(getContext(), currRoomInfo.getRoomId(), currRoomInfo.getUid(), exceptionUserId);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMsgDialog != null) {
            if (mMsgDialog.isStateSaved()) {
                mMsgDialog.dismissAllowingStateLoss();
            }
            mMsgDialog = null;
        }
        if (roomCpInviteDialog != null) {
            if (roomCpInviteDialog.isStateSaved()) {
                roomCpInviteDialog.dismissAllowingStateLoss();
            }
            roomCpInviteDialog = null;
        }
        CoreUtils.unregister(this);
    }

    public interface UserComeAction {
        void showCar(String carImageUrl);
    }

    /**
     * 底部按钮点击处理
     */
    private class GameRoomBottomViewWrapper extends BottomViewListenerWrapper {
        @Override
        public void onOpenMicBtnClick() {
            RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByAccount(
                    String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            if (roomQueueInfo == null || roomQueueInfo.mRoomMicInfo == null) return;
            //先判断麦上是否是开麦的
            if (!roomQueueInfo.mRoomMicInfo.isMicMute() && !RtcEngineManager.get().isAudienceRole()) {
                boolean isMute = !RtcEngineManager.get().isMute();
                AvRoomDataManager.get().mIsNeedOpenMic = !isMute;
                RtcEngineManager.get().setMute(isMute);
                updateMicBtn();
            }
        }

        @Override
        public void onSendFaceBtnClick() {
            if (isActivityDestroyed(getActivity())) {
                return;
            }
            if (AvRoomDataManager.get().isOnMic(myUid) || AvRoomDataManager.get().isRoomOwner()) {
                if (dynamicFaceDialog == null) {
                    dynamicFaceDialog = new DynamicFaceDialog(getContext());
                    dynamicFaceDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            dynamicFaceDialog = null;
                        }
                    });
                }
                if (!dynamicFaceDialog.isShowing()) {
                    dynamicFaceDialog.show();
                }
            } else {
                toast("上麦才能发表情哦!");
            }
        }

        @Override
        public void onSendGiftHis() {
            if (isActivityDestroyed(getActivity())) {
                return;
            }
            if (giftMsgList != null && !giftMsgList.isEmpty()) {
                record = new GiftRecordDialog(mContext);
                record.show();
                record.loadData(giftMsgList);
            } else {
                toast("暂时还没有人送礼哦");
            }
        }

        @Override
        public void onSendMsgBtnClick() {
            if (ChatUtil.checkRoomChatBanned())
                return;
            inputLayout.setVisibility(View.VISIBLE);
            inputEdit.setText("");
            inputEdit.setFocusableInTouchMode(true);
            inputEdit.requestFocus();
            showKeyBoard();
        }

        @Override
        public void onSendGiftBtnClick() {
            sendGift();
        }

        @Override
        public void onRemoteMuteBtnClick() {
            RtcEngineManager.get().setRemoteMute(!RtcEngineManager.get().isRemoteMute());
            updateRemoteMuteBtn();
        }


        @Override
        public void onBuShowMicInList() {
            if (AvRoomDataManager.get().isSelfOnMic()) {
                toast("您已经在麦上");
                return;
            }
            showMicInListDialog();
        }

        @Override
        public void onMsgBtnClick() {
            if (isActivityDestroyed(getActivity())) {
                return;
            }
            Fragment fragment = getChildFragmentManager().findFragmentByTag(mMsgTag);
            if (fragment instanceof RoomPrivateMsgDialog) {
                mMsgDialog = (RoomPrivateMsgDialog) fragment;
            } else {
                mMsgDialog = RoomPrivateMsgDialog.newInstance();
            }
            if (mMsgDialog.isAdded()) {
                mMsgDialog.dismissAllowingStateLoss();
            } else {
                mMsgDialog.show(getChildFragmentManager(), mMsgTag);
            }
        }

        @Override
        public void onMoreBtnClick() {
            if (isActivityDestroyed(getActivity())) {
                return;
            }

            FragmentManager fragmentManager = getChildFragmentManager();
            String tag = "RoomBottomMoreDialog";
            RoomBottomMoreDialog moreDialog = (RoomBottomMoreDialog) fragmentManager.findFragmentByTag(tag);
            if (moreDialog == null) {
                moreDialog = RoomBottomMoreDialog.newInstance();
            }
            if (moreDialog.isAdded()) {
                if (moreDialog.isStateSaved()) {
                    moreDialog.dismissAllowingStateLoss();
                }
            } else {
                moreDialog.show(fragmentManager, tag);
            }
        }
    }
}
