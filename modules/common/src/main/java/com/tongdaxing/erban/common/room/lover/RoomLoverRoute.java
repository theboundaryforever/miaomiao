package com.tongdaxing.erban.common.room.lover;

/**
 * Created by Chen on 2019/5/7.
 */
public class RoomLoverRoute {
    public static final String ROOM_LOVER_PLAYING_PATH = "/erban_client/LoverPlayingDialogFragment";
    public static final String ROOM_LOVER_SOUL_TOPIC_PATH = "/erban_client/SoulTopicDialogFragment";
    public static final String ROOM_LOVER_TACIT_TEST_PATH = "/erban_client/TacitTestDialogFragment";
    public static final String ROOM_LOVER_RECEIVE_TEST_PATH = "/erban_client/ReceiveTestDialogFragment";
    public static final String ROOM_LOVER_RESULT_TEST_PATH = "/erban_client/TestResultDialogFragment";
}
