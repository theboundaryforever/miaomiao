package com.tongdaxing.erban.common.jpush;

/**
 * <p>
 * Created by zhangjian on 2019/7/16.
 */
public class PushExtras {
    public static final String P2P_CHAT = "1";//私聊页
    public static final String ROOM = "2";//房间
    public static final String H5 = "3";//H5
    public static final String VOICE_GROUP_IM = "4";//点赞评论页面
    public static final String NOTIFICATION_IM = "5";//通知消息页面
    /**
     * 跳转类型 上面12345 其他打开首页（应用死掉的情况）或不操作（应用在运行）即可
     */
    private String skipType;
    /**
     * 跳转数据
     */
    private String skipUri;

    private String msgType;

    public String getSkipType() {
        return skipType;
    }

    public void setSkipType(String skipType) {
        this.skipType = skipType;
    }

    public String getSkipUri() {
        return skipUri;
    }

    public void setSkipUri(String skipUri) {
        this.skipUri = skipUri;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    @Override
    public String toString() {
        return "PushExtras{" +
                "skipType='" + skipType + '\'' +
                ", skipUri='" + skipUri + '\'' +
                ", msgType='" + msgType + '\'' +
                '}';
    }
}
