package com.tongdaxing.erban.common.room.user;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.mvp.MVPBaseDialogFragment;
import com.netease.nim.uikit.custom.UserEvent;
import com.netease.nim.uikit.glide.GlideApp;
import com.opensource.svgaplayer.SVGAImageView;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.util.DensityUtil;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.room.RoomRoutePath;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.cpexclusive.CpExclusivePageActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.message.activity.AttentionListActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.tongdaxing.erban.common.view.LevelView;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.cp.IRoomCpCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomManager;
import com.tongdaxing.xchat_core.user.IUserDbCore;
import com.tongdaxing.xchat_core.user.IUserInfoDialogCoreClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chen on 2019/6/12.
 */
@Route(path = RoomRoutePath.ROOM_USER_INFO_PATH)
public class UserInfoDialogFragment extends MVPBaseDialogFragment<IUserInfoDialogView, UserInfoPresenter> implements IUserInfoDialogView {
    @BindView(R2.id.user_info_tv_admin)
    TextView mTvAdmin;
    @BindView(R2.id.user_info_tv_report)
    TextView mTvReport;
    @BindView(R2.id.user_info_tv_black)
    TextView mTvBlack;
    @BindView(R2.id.user_info_iv_close)
    ImageView mIvClose;
    @BindView(R2.id.user_info_iv_decorate)
    ImageView mIvDecorate;
    @BindView(R2.id.user_info_iv_photo)
    ImageView mIvPhoto;
    @BindView(R2.id.user_info_iv_into_room)
    ImageView mIvIntoRoom;
    @BindView(R2.id.user_info_tv_gender)
    ImageView mTvGender;
    @BindView(R2.id.user_info_tv_name)
    TextView mTvName;
    @BindView(R2.id.user_info_lv_level)
    LevelView mIvLevelView;
    @BindView(R2.id.user_info_tv_id)
    TextView mTvId;
    @BindView(R2.id.user_info_tv_attention_count)
    TextView mTvAttentionCount;
    @BindView(R2.id.user_info_tv_fans)
    TextView mTvFans;
    @BindView(R2.id.user_info_tv_send_gift)
    TextView mTvSendGift;
    @BindView(R2.id.user_info_tv_chat)
    TextView mTvChat;
    @BindView(R2.id.user_info_tv_attention)
    TextView mTvAttention;
    @BindView(R2.id.user_info_ll_gift_chat_attention)
    LinearLayout mLlBottomLayout;
    @BindView(R2.id.user_info_iv_cp)
    ImageView mIvCp;
    @BindView(R2.id.user_info_iv_cp_gift)
    SVGAImageView mIvCpGift;
    @BindView(R2.id.user_info_riv_cp_user_icon1)
    CircleImageView mRivCpUserIcon1;
    @BindView(R2.id.user_info_riv_cp_user_icon2)
    CircleImageView mRivCpUserIcon2;
    @BindView(R2.id.user_info_fl_have_cp)
    FrameLayout mFlHaveCp;
    @BindView(R2.id.user_info_fl_no_cp)
    FrameLayout mFlNoCp;
    @BindView(R2.id.user_info_rl_cp)
    RelativeLayout mRlCp;
    @BindView(R2.id.user_info_fl_photo_layout)
    FrameLayout FlPhotoLayout;

    private boolean mIsAttention;
    @Autowired(name = RoomRoutePath.ROOM_USER_INFO_USER_ID)
    long mUserId;
    private int mCpState = 0;//CP状态 1有cp 0没有

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            WindowManager.LayoutParams params = window.getAttributes();
            params.width = DensityUtil.dip2px(BaseApp.mBaseContext, 293.0f); // 宽度填充满屏
            params.height = DensityUtil.dip2px(BaseApp.mBaseContext, 270.0f);
            window.setAttributes(params);
            // 这里用透明颜色替换掉系统自带背景
            int color = ContextCompat.getColor(BaseApp.mBaseContext, android.R.color.transparent);
            window.setBackgroundDrawable(new ColorDrawable(color));
        }
    }

    @Override
    protected UserInfoPresenter createPresenter() {
        return new UserInfoPresenter();
    }

    @Override
    public int getContentViewId() {
        return R.layout.user_info_dialog_fragment;
    }

    @Override
    public void initBefore() {

    }

    @Override
    public void findView() {
        ButterKnife.bind(this, mContentView);
    }

    @Override
    public void setView() {
        ARouter.getInstance().inject(this);
        RoomInfo roomInfo = mPresenter.getRoomInfo();
        if (roomInfo != null) {
            int roomType = roomInfo.getTagType();
            if (roomType == RoomInfo.ROOMTYPE_PERSONAL) {
                if ((AvRoomDataManager.get().isRoomOwner()) || !AvRoomDataManager.get().isRoomOwner(mUserId)) {
                    mTvSendGift.setVisibility(View.GONE);
                } else {
                    mTvSendGift.setVisibility(View.VISIBLE);
                }
            } else {
                mTvSendGift.setVisibility(View.VISIBLE);
            }
        }

        mTvAdmin.setVisibility(View.GONE);
        if (mPresenter.isSelf(mUserId)) {
            mLlBottomLayout.setVisibility(View.GONE);
            mTvReport.setVisibility(View.INVISIBLE);
            mTvBlack.setVisibility(View.INVISIBLE);
        } else {
            mPresenter.checkAttention(mUserId);
            mPresenter.checkRoomStatus(mUserId);
            boolean checkBlack = mPresenter.checkBlack(mUserId);
            if (checkBlack) {
                mTvBlack.setVisibility(View.INVISIBLE);
            } else {
                mTvBlack.setVisibility(View.VISIBLE);
            }
            mLlBottomLayout.setVisibility(View.VISIBLE);
            mTvReport.setVisibility(View.VISIBLE);
        }

        UserInfo userInfo = mPresenter.getUserInfo(mUserId);
        updateView(userInfo);
    }

    @Override
    public void updateView(UserInfo userInfo) {
        if (userInfo != null && userInfo.getUid() == mUserId) {
            Context context = getContext();
            L.debug("UserInfoDialogFragment", "updateView");
            if (context != null) {
                String avatar = userInfo.getAvatar();
                if (TextUtils.isEmpty(avatar)) {
                    ImageLoadUtils.loadImage(context, R.drawable.ic_no_avatar, mIvPhoto);
                } else {
                    ImageLoadUtils.loadAvatar(context, avatar, mIvPhoto, true);
                }
                mTvName.setText(userInfo.getNick());
                mTvId.setText(String.format(Locale.getDefault(),
                        context.getString(R.string.me_user_id), userInfo.getErbanNo()));
                Drawable drawable;
                if (userInfo.getGender() == 1) {
                    drawable = context.getResources().getDrawable(R.drawable.room_user_man);
                } else {
                    drawable = context.getResources().getDrawable(R.drawable.room_user_girl);
                }
                mTvGender.setImageDrawable(drawable);
//            nick.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
                //设置星座

                String headwearUrl = userInfo.getHeadwearUrl();
                if (!TextUtils.isEmpty(headwearUrl)) {
                    mIvDecorate.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(context, headwearUrl, mIvDecorate);
                } else {
                    mIvDecorate.setVisibility(View.GONE);
                }

                //设置粉丝数量
                mTvFans.setText(String.format(Locale.getDefault(),
                        context.getString(R.string.dialog_user_info_fans_number_text), userInfo.getFansNum()));
                mTvAttentionCount.setText(String.format(Locale.getDefault(),
                        context.getString(R.string.dialog_user_info_attention_btn_text), userInfo.getFollowNum()));
                mIvLevelView.setExperLevel(userInfo.getExperLevel());
                mIvLevelView.setCharmLevel(userInfo.getCharmLevel());
            }

            RoomInfo roomInfo = mPresenter.getRoomInfo();
            if (roomInfo != null) {
                if (roomInfo.getTagType() == RoomInfo.ROOMTYPE_PERSONAL) {
                    mTvSendGift.setVisibility(View.GONE);
                } else {
                    mTvSendGift.setVisibility(View.VISIBLE);
                }
            }

            setCpContent(userInfo);
        }

        if (!mPresenter.isSelf(mUserId)) {
            if (AvRoomDataManager.get().isRoomOwner()) {
                setAdminView();
            } else if (AvRoomDataManager.get().isRoomAdmin()) {
                if (AvRoomDataManager.get().isRoomAdmin(String.valueOf(mUserId)) || AvRoomDataManager.get().isRoomOwner(mUserId)) {
                    setNormalView();
                } else {
                    setAdminView();
                }
            } else {
                setNormalView();
            }
        }
    }

    private void setAdminView() {
        mTvAdmin.setVisibility(View.VISIBLE);
        mTvBlack.setVisibility(View.GONE);
        mTvReport.setVisibility(View.GONE);
    }

    private void setNormalView() {
        mTvAdmin.setVisibility(View.GONE);
        mTvBlack.setVisibility(View.VISIBLE);
        mTvReport.setVisibility(View.VISIBLE);
    }

    @Override
    public void showIntoRoomImage() {
        mIvIntoRoom.setVisibility(View.VISIBLE);
    }

    @Override
    public void enterRoom(long roomId) {
        long formalRoomId = BasicConfig.isDebug ? PublicChatRoomManager.devRoomId : PublicChatRoomManager.formalRoomId;
        if (roomId == formalRoomId) {
            toast("对方不在房间内");
            return;
        }
        RoomInfo current = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomId > 0) {
            if (current != null) {
                if (current.getUid() == roomId) {
                    toast("已经和对方在同一个房间");
                    return;
                }
            }
            AVRoomActivity.start(getActivity(), roomId);
        } else {
            toast("对方不在房间内");
        }
    }

    private void toast(String content) {
        SingleToastUtil.showToast(content);
    }

    @Override
    public void setListener() {

    }

    @OnClick(R2.id.user_info_tv_admin)
    public void onAdminClicked() {
        CoreUtils.send(new com.tongdaxing.xchat_core.user.UserEvent.OnAdminUserShow(mUserId));
        this.dismissAllowingStateLoss();
    }

    @OnClick(R2.id.user_info_tv_report)
    public void onReportClicked() {
        List<ButtonItem> buttons = new ArrayList<>();
        ButtonItem button1 = new ButtonItem("政治敏感", () -> mPresenter.reportCommit(1));
        ButtonItem button2 = new ButtonItem("色情低俗", () -> mPresenter.reportCommit(2));
        ButtonItem button3 = new ButtonItem("广告骚扰", () -> mPresenter.reportCommit(3));
        ButtonItem button4 = new ButtonItem("人身攻击", () -> mPresenter.reportCommit(4));
        buttons.add(button1);
        buttons.add(button2);
        buttons.add(button3);
        buttons.add(button4);
        DialogManager dialogManager = null;

        Context context = getContext();
        if (context instanceof BaseMvpActivity) {
            dialogManager = ((BaseMvpActivity) context).getDialogManager();
        } else if (context instanceof BaseActivity) {
            dialogManager = ((BaseActivity) context).getDialogManager();
        }
        if (dialogManager != null) {
            dialogManager.showCommonPopupDialog(buttons, "取消");
        }
    }

    @OnClick(R2.id.user_info_tv_black)
    public void onBlackClicked() {
        mPresenter.doBlack(String.valueOf(mUserId));
        this.dismissAllowingStateLoss();
    }

    @OnClick(R2.id.user_info_iv_close)
    public void onCloseClicked() {
        this.dismissAllowingStateLoss();
    }

    @OnClick(R2.id.user_info_iv_photo)
    public void onPhotoClicked() {
        if (getContext() == null) {
            return;
        }
        NewUserInfoActivity.start(getContext(), mUserId);
    }

    @OnClick(R2.id.user_info_iv_into_room)
    public void onIntoRoomClicked() {
        mPresenter.enterRoom(mUserId);
    }

    @OnClick(R2.id.user_info_tv_send_gift)
    public void onSendGiftClicked() {
        CoreManager.notifyClients(IUserInfoDialogCoreClient.class, IUserInfoDialogCoreClient.METHOD_SHOW_GIFT_DIALOG, String.valueOf(mUserId));
        CoreUtils.send(new UserEvent(mUserId));
        this.dismissAllowingStateLoss();
    }

    @OnClick(R2.id.user_info_tv_chat)
    public void onChatClicked() {
        CoreManager.notifyClients(IUserInfoDialogCoreClient.class, IUserInfoDialogCoreClient.METHOD_SHOW_MSG_CHAT_DIALOG, String.valueOf(mUserId));
        this.dismissAllowingStateLoss();
    }

    @OnClick(R2.id.user_info_tv_attention)
    public void onAttentionClicked() {
        mPresenter.attention(mUserId, mIsAttention);
        this.dismissAllowingStateLoss();
    }

    @OnClick(R2.id.user_info_rl_cp)
    public void onCpClicked() {
        UserInfo userInfo = mPresenter.getUserInfo(mUserId);
        if (userInfo != null) {
            if (mPresenter.isSelf(mUserId)) {
                if (userInfo.getCpUser() != null) {
                    CpExclusivePageActivity.start(getContext(), mUserId, true);
                } else {
                    //打开我的关注页 出现邀请按钮
                    AttentionListActivity.start(getContext(), true);
                }
            } else {
                if (userInfo.getCpUser() != null) {
                    if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == userInfo.getCpUser().getUid()) {
                        CpExclusivePageActivity.start(getContext(), mUserId, true);
                    } else {
                        CpExclusivePageActivity.start(getContext(), mUserId);
                    }
                } else {
                    CoreManager.notifyClients(IRoomCpCoreClient.class, IRoomCpCoreClient.METHOD_ON_OPEN_CP_GIFT_LIST, mUserId);
                    EventBus.getDefault().post(new UserEvent(mUserId, true));
                }
            }
        }
        this.dismissAllowingStateLoss();
    }

    @Override
    public void attentionSuccess(long uid, boolean attention) {
        if (uid == mUserId) {
            mIsAttention = attention;
            if (attention) {
                mTvAttention.setText(getString(R.string.user_info_is_attention));
            } else {
                mTvAttention.setText(getString(R.string.user_info_attention));
            }
        }
    }

    /**
     * CP状态 1有cp 0没有
     */
    private void setCpContent(UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }
        //设置cp头像
        mCpState = userInfo.getCpUser() == null ? 0 : 1;
        if (mCpState == 1) {
            mFlHaveCp.setVisibility(View.VISIBLE);
            mFlNoCp.setVisibility(View.GONE);
            GlideApp.with(getContext())
                    .load(userInfo.getAvatar())
                    .placeholder(R.drawable.default_cover)
                    .error(R.drawable.default_cover)
                    .into(mRivCpUserIcon1);
            GlideApp.with(getContext())
                    .load(userInfo.getCpUser().getAvatar())
                    .placeholder(R.drawable.default_cover)
                    .error(R.drawable.default_cover)
                    .into(mRivCpUserIcon2);
            if (userInfo.getCpUser() != null && userInfo.getCpUser().getCpGiftLevel() == 0) {
                CoreManager.getCore(IUserDbCore.class).updateCpGiftLevel(userInfo.getUid(), 1);
            }
            if (userInfo.getCpUser() != null && userInfo.getCpUser().getCpGiftLevel() >= 1 && userInfo.getCpUser().getCpGiftLevel() <= 3) {
                mIvCpGift.setVisibility(View.VISIBLE);
                mIvCp.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(userInfo.getCpUser().getVggUrl())) {
                    SvgaUtils.cyclePlayWebAnim(getContext(), mIvCpGift, userInfo.getCpUser().getVggUrl());
                }
            }
        } else {
            mFlHaveCp.setVisibility(View.GONE);
            mFlNoCp.setVisibility(View.VISIBLE);
            mIvCpGift.setVisibility(View.GONE);
            mIvCp.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onP2PCpInviteAgree() {
        mCpState = 1;//CP状态 1有cp 0没有
        UserInfo userInfo = mPresenter.getUserInfo(mUserId);
        setCpContent(userInfo);
    }

    @Override
    public void onP2PCpRemove() {
        mCpState = 0;//CP状态 1有cp 0没有
        UserInfo userInfo = mPresenter.getUserInfo(mUserId);
        setCpContent(userInfo);
        if (userInfo != null) {
            userInfo.setCpUser(null);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SvgaUtils.closeSvga(mIvCpGift);
    }
}
