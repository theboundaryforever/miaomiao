package com.tongdaxing.erban.common.ui.rankinglist;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * 排行榜 榜单切换indicatorAdapter
 */
public class RankingListMagicIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;
    private OnItemSelectListener mOnItemSelectListener;

    public RankingListMagicIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mContext = context;
        mTitleList = titleList;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {
        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.layout_ranking_list_indicator);
        TextView indicatorText = pagerTitleView.findViewById(R.id.indicator);
        View indicator = pagerTitleView.findViewById(R.id.indicator_iv);

        indicatorText.setText(mTitleList.get(i).getName());
        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {
            @Override
            public void onSelected(int index, int totalCount) {
                indicatorText.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                indicatorText.setTextSize(19);
                indicator.setVisibility(View.VISIBLE);
                if (mOnItemSelectListener != null) {
                    mOnItemSelectListener.onItemSelect(i);
                }
            }

            @Override
            public void onDeselected(int index, int totalCount) {
                indicatorText.setTextColor(ContextCompat.getColor(mContext, R.color.color_FEFEFE));
                indicatorText.setTextSize(18);
                indicator.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {

            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {

            }
        });
        pagerTitleView.setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }
        });
        return pagerTitleView;
    }

    @Override
    public IPagerIndicator getIndicator(Context context) {
//        LinePagerIndicator indicator = new LinePagerIndicator(context);
//        indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
//        indicator.setLineHeight(UIUtil.dip2px(mContext, 3));
//        indicator.setRoundRadius(UIUtil.dip2px(mContext, 2));
//        indicator.setLineWidth(UIUtil.dip2px(mContext, 15));
//        indicator.setYOffset(UIUtil.dip2px(mContext, 8));
//        indicator.setColors(ContextCompat.getColor(mContext, R.color.white));
//        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        lp.topMargin = ScreenUtil.dip2px(8);
//        lp.bottomMargin = ScreenUtil.dip2px(8);
//        indicator.setLayoutParams(lp);
        return null;
    }

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}