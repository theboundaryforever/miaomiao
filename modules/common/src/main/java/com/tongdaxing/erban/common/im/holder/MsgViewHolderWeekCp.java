package com.tongdaxing.erban.common.im.holder;

import android.view.View;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderText;
import com.tongdaxing.erban.common.ui.cpexclusive.CpExclusivePageActivity;
import com.tongdaxing.xchat_core.im.custom.bean.WeekCpMsgAttachment;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

/**
 * tip消息的MsgViewHolder
 * <p>
 * Created by zhangjian on 2019/3/29.
 */
public class MsgViewHolderWeekCp extends MsgViewHolderText {


    public MsgViewHolderWeekCp(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected void bindContentView() {
        super.bindContentView();
        WeekCpMsgAttachment attachment = (WeekCpMsgAttachment) message.getAttachment();
        bodyTextView.setText(attachment.getContent());
        bodyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (attachment.getSecond() == WeekCpMsgAttachment.ROOM_WEEK_CP_OFFICE_MSG_MERRY) {
                    if (cacheLoginUserInfo != null && cacheLoginUserInfo.getCpUser() != null) {
                        CpExclusivePageActivity.start(bodyTextView.getContext(), cacheLoginUserInfo.getUid(), true);
                    } else {
                        SingleToastUtil.showToast("你们的CP关系已解除");
                    }
                }
            }
        });
    }
}
