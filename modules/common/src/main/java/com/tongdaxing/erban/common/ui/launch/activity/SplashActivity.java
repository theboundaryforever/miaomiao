package com.tongdaxing.erban.common.ui.launch.activity;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.callback.NavCallback;
import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.juxiao.safetychecker.SafetyChecker;
import com.juxiao.safetychecker.bean.SafetyCheckResultBean;
import com.netease.nim.uikit.glide.GlideApp;
import com.tcloud.core.log.L;
import com.tcloud.core.util.EasyTimer;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.model.safetychecker.SafetyCheckerModel;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.ui.MainActivity;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.banner.BannerClickModel;
import com.tongdaxing.xchat_core.initial.IInitView;
import com.tongdaxing.xchat_core.initial.InitInfo;
import com.tongdaxing.xchat_core.initial.InitPresenter;
import com.tongdaxing.xchat_core.utils.ThreadUtil;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;

import java.util.List;

/**
 * @author xiaoyu
 * @date 2017/12/30
 */
@CreatePresenter(InitPresenter.class)
public class SplashActivity extends BaseMvpActivity<IInitView, InitPresenter> implements IInitView, View.OnClickListener {
    private static final String TAG = "SplashActivityTAG";
    private ImageView ivActivity;
    private InitInfo mLocalSplashVo;
    private TextView mTvSkipSplash;
    private EasyTimer mEasyTimer = new EasyTimer();
    private int mSkipTime = 3;
    private boolean isStarted;

    private BannerClickModel mBannerClickModel;

    private boolean isExistMainActivity(Class<?> activity) {
        Intent intent = new Intent(this, activity);
        ComponentName cmpName = intent.resolveActivity(getPackageManager());
        boolean flag = false;
        if (cmpName != null) { // 说明系统中存在这个activity
            ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            if (am != null) {
                List<ActivityManager.RunningTaskInfo> taskInfoList = am.getRunningTasks(10);  //获取从栈顶开始往下查找的10个activity
                for (ActivityManager.RunningTaskInfo taskInfo : taskInfoList) {
                    if (taskInfo.baseActivity.equals(cmpName)) { // 说明它已经启动了
                        flag = true;
                        break;  //跳出循环，优化效率
                    }
                }
            }
        }
        return flag;
    }

    /**
     * 检查是否需要关闭当前Activity
     */
    private void checkCloseActivity() {
        if (!this.isTaskRoot() && isExistMainActivity(MainActivity.class)) {
            Intent mainIntent = getIntent();
            String action = mainIntent.getAction();
            L.debug(TAG, "isTaskRoot action " + action + " Categories " + mainIntent.getCategories());
            if (mainIntent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action)) {
                finish();
                return;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkCloseActivity();
        setContentView(R.layout.activity_splash);
        ivActivity = (ImageView) findViewById(R.id.iv_activity);
        mTvSkipSplash = (TextView) findViewById(R.id.common_tv_skip);
        ivActivity.setOnClickListener(this);
        mTvSkipSplash.setOnClickListener(this);
        setSwipeBackEnable(false);
        if (savedInstanceState != null) {
            // 从堆栈恢复，不再重复解析之前的intent
            setIntent(new Intent());
        }
        checkFirstOpen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isStarted) {
            isStarted = true;
            initiate();
        }
    }

    @Override
    public void activatingSaveSuccess() {
        SpUtils.put(this, SpEvent.SP_ACTIVATING_SAVE, true);
    }

    private void checkFirstOpen() {
        String isFirst = (String) SpUtils.get(this, SpEvent.first_open, "");
        if (TextUtils.isEmpty(isFirst)) {
            SpUtils.put(this, SpEvent.first_open, "open");
//            StatisticManager.get().onEvent(this, EventId.first_open, "首次打开");
            UmengEventUtil.getInstance().onEvent(this, UmengEventId.getFirstOpenApp());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    protected void initiate() {
        if (!(Boolean) SpUtils.get(this, SpEvent.SP_ACTIVATING_SAVE, false)) {
            getMvpPresenter().activatingSave();
        }
        showSplash();
//        CoreManager.getCore(IHomeCore.class).getMainTabData();
        getMvpPresenter().init();
        animation();
        safetyCheck();
    }

    private void showSplash() {
        L.debug(TAG, "showSplash getMvpPresenter() = %s", getMvpPresenter());
        // 不过期的，并且已经下载出来图片的闪屏页数据
        mLocalSplashVo = getMvpPresenter().getLocalSplashVo();
        L.debug(TAG, "mLocalSplashVo = %s", mLocalSplashVo == null ? "null" : mLocalSplashVo.toString());
        ivActivity.setImageResource(R.drawable.icon_splash);
        if (mLocalSplashVo != null && mLocalSplashVo.getSplashVo() != null
                && !TextUtils.isEmpty(mLocalSplashVo.getSplashVo().getPict())) {
            String pictUrl = mLocalSplashVo.getSplashVo().getPict();
            L.info(TAG, "showSplash pictUrl: %s", pictUrl);
            GlideApp.with(this).load(pictUrl)
                    .dontAnimate()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .placeholder(R.drawable.icon_splash)
                    .error(R.drawable.icon_splash)
                    .into(ivActivity);
        } else {
            L.debug(TAG, "showSplash image url is null");
            // 其他情况显示的是默认的图片
            ivActivity.setImageResource(R.drawable.icon_splash);
//            skipToMain();
        }
        startTimer();
    }

    private void animation() {
        ivActivity.setAlpha(0.4F);
        ivActivity.animate().alpha(1).setDuration(300).start();
        ivActivity.animate().setDuration(3000).start();
    }

    /**
     * 安全检测
     */
    private void safetyCheck() {
        ThreadUtil.getThreadPool().execute(() -> {
            SafetyCheckResultBean checkResult = SafetyChecker.getInstance().check(this.getApplicationContext());
            if (checkResult.getCheckStatus() != 0) {//不安全的，上报
                ThreadUtil.runOnUiThread(() -> {
                    SafetyCheckerModel safetyCheckerModel = new SafetyCheckerModel();
                    safetyCheckerModel.reportSafetyCheckResult(checkResult);
                });
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_activity) {
            gotoMain();
        } else if (v.getId() == R.id.common_tv_skip) {
            skipToMain();
        }
    }

    private void startTimer() {
        L.debug(TAG, "startTimer");
        mEasyTimer.setDuration(1000);
        mTvSkipSplash.setVisibility(View.VISIBLE);
        mTvSkipSplash.setText(getResources().getString(R.string.common_splash_skip, mSkipTime));
        mEasyTimer.setRunnable(new Runnable() {
            @Override
            public void run() {
                mTvSkipSplash.setText(getResources().getString(R.string.common_splash_skip, mSkipTime--));
                if (mSkipTime < 0) {
                    skipToMain();
                }
            }
        });
        mEasyTimer.start();
    }

    private void gotoMain() {
        L.debug(TAG, "gotoMain");
        if (mLocalSplashVo == null || mLocalSplashVo.getSplashVo() == null) {
            return;
        }
        String link = mLocalSplashVo.getSplashVo().getLink();
        int type = mLocalSplashVo.getSplashVo().getType();
        L.debug(TAG, "gotoMain link = %s, type = %d", link, type);

        if (TextUtils.isEmpty(link) || type == 0) {
            return;
        }
        L.debug(TAG, "gotoMain start MainActivity");
        cancelTimer();

        //splash点击上报
        if (mBannerClickModel == null) {
            mBannerClickModel = new BannerClickModel();
        }
        mBannerClickModel.bannerStat(mLocalSplashVo.getSplashVo().getId(), BannerClickModel.SPLASH);

        Intent intent = getIntent();
        L.debug(TAG, "getIntent.getStringExtra = %s", intent == null ? null : intent.getStringExtra(Constants.PUSH_SKIP_EXTRA));
        ARouter.getInstance().build(CommonRoutePath.MAIN_ACTIVITY_ROUTE_PATH)
                .withString("link", link)
                .withInt("type", type)
                .withString(Constants.PUSH_SKIP_EXTRA, intent == null ? null : intent.getStringExtra(Constants.PUSH_SKIP_EXTRA))
                .navigation(this, new NavCallback() {
                    @Override
                    public void onArrival(Postcard postcard) {
                        SplashActivity.this.finish();
                    }
                });
//        RoomModulesService.enterRoom(this);
    }

    private synchronized void skipToMain() {
        L.debug(TAG, "skipToMain");
        cancelTimer();
        Intent intent = getIntent();
        L.debug(TAG, "getIntent.getStringExtra = %s", intent == null ? null : intent.getStringExtra(Constants.PUSH_SKIP_EXTRA));
        ARouter.getInstance().build(CommonRoutePath.MAIN_ACTIVITY_ROUTE_PATH)
                .withString(Constants.PUSH_SKIP_EXTRA, intent == null ? null : intent.getStringExtra(Constants.PUSH_SKIP_EXTRA))
                .navigation(this, new NavCallback() {
                    @Override
                    public void onArrival(Postcard postcard) {
                        SplashActivity.this.finish();
                    }
                });

//        RoomModulesService.enterRoom(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelTimer();
        L.debug(TAG, "onDestroy");
    }

    private void cancelTimer() {
        if (mEasyTimer != null) {
            L.debug(TAG, "cancelTimer");
            mEasyTimer.stop();
            mEasyTimer = null;
        }
    }
}
