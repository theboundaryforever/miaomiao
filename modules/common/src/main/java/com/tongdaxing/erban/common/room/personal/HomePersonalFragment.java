package com.tongdaxing.erban.common.room.personal;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.FrameLayout;

import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.room.avroom.fragment.AbsRoomFragment;
import com.tongdaxing.erban.common.room.avroom.widget.GiftV2View;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DESUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.tongdaxing.xchat_framework.util.util.DESUtils.giftCarSecret;

/**
 * Created by Chen on 2019/4/28.
 */
public class HomePersonalFragment extends AbsRoomFragment
        implements HomePersonalRoomFragment.UserComeAction {
    @BindView(R2.id.fm_content)
    FrameLayout fmContent;
    @BindView(R2.id.gift_view)
    GiftV2View giftView;
    private String TAG = "HomePersonalFragment";
    private HomePersonalRoomFragment roomFragment;

    public static HomePersonalFragment newInstance(long roomUid) {
        HomePersonalFragment homePersonalFragment = new HomePersonalFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.ROOM_UID, roomUid);
        homePersonalFragment.setArguments(bundle);
        return homePersonalFragment;
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // clear views
        if (roomFragment != null) {
            roomFragment.onNewIntent(intent);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_chatroom_personal_main;
    }

    @Override
    public void onFindViews() {
        ButterKnife.bind(this, mView);
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {
        CoreUtils.register(this);
        roomFragment = new HomePersonalRoomFragment();
        roomFragment.setUserComeAction(this);
        getChildFragmentManager().beginTransaction().replace(R.id.fm_content, roomFragment).commitAllowingStateLoss();
        updateGiftInfo();
    }


    /**
     * 更新礼物信息
     */
    private void updateGiftInfo() {
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
    }


    @Override
    public void release() {
        super.release();
        if (roomFragment != null) {
            roomFragment = null;
        }
    }

    @Override
    public void onShowActivity(List<ActionDialogInfo> dialogInfos) {
//        if (roomFragment != null && roomFragment.isAdded()) {
//            roomFragment.showActivity(dialogInfos);
//        }
    }

    @Override
    public void onDestroy() {
        if (giftView != null) {
            giftView.release();
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        roomFragment.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 定时拉取房间在线人数
     *
     * @param onlineNumber
     */
    @Override
    public void onRoomOnlineNumberSuccess(int onlineNumber) {
//        L.debug(TAG,"onRoomOnlineNumberSuccess() -> setIdOnlineData()");
        super.onRoomOnlineNumberSuccess(onlineNumber);
//        setIdOnlineData();
    }

    @Override
    public void showCar(String carImageUrl) {
        try {
            final String carUrl = DESUtils.DESAndBase64Decrypt(carImageUrl, giftCarSecret);
            if (!TextUtils.isEmpty(carUrl)) {
                giftView.giftEffectView.drawSvgaEffect(carUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


