package com.tongdaxing.erban.common.room.audio.widget;

import android.os.Bundle;

import com.juxiao.library_ui.widget.ViewHolder;
import com.juxiao.library_ui.widget.dialog.BaseDialog;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.audio.adapter.MyMusicAdapter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.music.IMusicCore;
import com.tongdaxing.xchat_core.music.IMusicCoreClient;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCore;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerDbCore;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.Map;

import static com.tongdaxing.xchat_core.player.IPlayerCore.PLAY_STATUS_FILE_NOT_FOUND;
import static com.tongdaxing.xchat_core.player.IPlayerCore.PLAY_STATUS_MUSIC_LIST_EMPTY;

/**
 * Function:删除音乐对话框
 * Author: Edward on 2019/2/18
 */
public class MusicDelDialog extends BaseDialog {
    private MyMusicInfo myMusicInfo;
    //    private IMyMusicView iMyMusicView;
//    private int clickPosition = -1;

    @Override
    public void convertView(ViewHolder viewHolder) {
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        myMusicInfo = bundle.getParcelable("data");
//        clickPosition = bundle.getInt("position");

        viewHolder.setOnClickListener(R.id.tv_sure, v -> {
            delSongFromService();
        });

        viewHolder.setOnClickListener(R.id.tv_cancel, v -> {
            dismiss();
        });
    }

    /**
     * 从服务器中删除歌曲数据
     */
    private void delSongFromService() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("songId", String.valueOf(myMusicInfo.getId()));
        OkHttpManager.getInstance().doPostRequest(UriProvider.getDelSong(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast(e.getMessage());
                dismiss();
            }

            @Override
            public void onResponse(Json response) {
                dismiss();
                if (response != null && response.num("code") == 200) {
                    if (myMusicInfo.getFileStatus() == MyMusicAdapter.FILE_STATUS_DOWNLOAD) {//可下载文件，直接删除
                        CoreManager.notifyClients(IMusicCoreClient.class, IMusicCoreClient.DEL_OPERATION, myMusicInfo.getId(), false);
                    } else if (myMusicInfo.getFileStatus() == MyMusicAdapter.FILE_STATUS_PLAY) {//可播放状态(歌曲还未播放)
                        CoreManager.getCore(IPlayerCore.class).deleteMusicFromPlayerList(convertBean(myMusicInfo));//从播放列表移除
                        CoreManager.getCore(IMusicCore.class).deleteFileFromCacheFolder(convertBean(myMusicInfo));//删除音乐缓存
                        CoreManager.getCore(IPlayerDbCore.class).deleteFromLocalMusics(myMusicInfo.getLocalUri());//从数据库删除
                        CoreManager.notifyClients(IMusicCoreClient.class, IMusicCoreClient.DEL_OPERATION, myMusicInfo.getId(), false);
                    } else if (myMusicInfo.getFileStatus() == MyMusicAdapter.FILE_STATUS_STOP) {//可停止状态(歌曲正在播放中)
                        CoreManager.getCore(IPlayerCore.class).deleteMusicFromPlayerList(convertBean(myMusicInfo));//从播放列表移除
                        CoreManager.getCore(IMusicCore.class).deleteFileFromCacheFolder(convertBean(myMusicInfo));//删除音乐缓存
                        CoreManager.getCore(IPlayerDbCore.class).deleteFromLocalMusics(myMusicInfo.getLocalUri());//从数据库删除
                        int result = CoreManager.getCore(IPlayerCore.class).switchPlayNext();
                        if (result == PLAY_STATUS_MUSIC_LIST_EMPTY || result == PLAY_STATUS_FILE_NOT_FOUND) {
                            CoreManager.notifyClients(IMusicCoreClient.class, IMusicCoreClient.DEL_OPERATION, myMusicInfo.getId(), true);
                            SingleToastUtil.showToast("播放列表暂无歌曲!");
                        } else {
                            CoreManager.notifyClients(IMusicCoreClient.class, IMusicCoreClient.DEL_OPERATION, myMusicInfo.getId(), false);
                        }
                    } else if (myMusicInfo.getFileStatus() == MyMusicAdapter.FILE_STATUS_DOWNLOADING) {//文件正在下载
                        CoreManager.getCore(IMusicDownloaderCore.class).deleteDownloadQueue(myMusicInfo.getUrl());//删除下载队列
                        CoreManager.getCore(IMusicDownloaderCore.class).deleteDownloadCache(myMusicInfo.getTitle());//删除下载缓存
                        CoreManager.notifyClients(IMusicCoreClient.class, IMusicCoreClient.DEL_OPERATION, myMusicInfo.getId(), false);
                    } else {
                        SingleToastUtil.showToast("音乐文件删除失败！");
                    }
                } else {
                    SingleToastUtil.showToast("音乐文件删除失败！");
                }
            }
        });
    }

    private MusicLocalInfo convertBean(MyMusicInfo myMusicInfo) {
        MusicLocalInfo musicLocalInfo = new MusicLocalInfo();
        musicLocalInfo.setLocalUri(myMusicInfo.getLocalUri());
        musicLocalInfo.setSongName(myMusicInfo.getTitle());
        musicLocalInfo.setSingerName(myMusicInfo.getArtist());
        musicLocalInfo.setFileSize(myMusicInfo.getSize());
        return musicLocalInfo;
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_music_del;
    }
}
