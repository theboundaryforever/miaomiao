package com.tongdaxing.erban.common.room.lover.weekcp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.mvp.MVPBaseFrameLayout;
import com.opensource.svgaplayer.SVGAImageView;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.room.lover.RoomLoverRoute;
import com.tongdaxing.erban.common.room.widget.dialog.CpRingDescriptionDialog;
import com.tongdaxing.erban.common.room.widget.dialog.RoomRemoveCurrentCpDialog;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.makefriends.FindFriendsBroadCastRoute;
import com.tongdaxing.erban.common.ui.widget.dialog.shareDialog.NewShareDialog;
import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.weekcp.week.IWeekCpCtrl;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.framework.Platform;

/**
 * Created by Chen on 2019/4/30.
 */
public class WeekCpView extends MVPBaseFrameLayout<IWeekCpView, WeekCpPresenter> implements IWeekCpView, NewShareDialog.OnShareDialogItemClick {
    @BindView(R2.id.room_cp_heart_svga_view)
    SVGAImageView mRoomCpSvgaView;
    @BindView(R2.id.room_cp_topic)
    ImageView mRoomCpTopic;
    @BindView(R2.id.room_cp_test)
    ImageView mRoomCpTest;
    @BindView(R2.id.room_week_cp_svga_view)
    SVGAImageView mRoomWeekCpSvgaView;
    @BindView(R2.id.room_week_to_cp)
    LinearLayout mRoomWeekToCp;
    @BindView(R2.id.room_week_is_cp)
    LinearLayout mRoomWeekIsCp;
    @BindView(R2.id.room_week_to_cp_value)
    TextView mRoomWeekToCpValue;
    @BindView(R2.id.room_week_is_cp_value)
    TextView mRoomWeekIsCpValue;
    private String TAG = "WeekCpView";
    private String mTacitTag = "TacitTestDialogFragment";
    private String mReceiveTag = "ReceiveTestDialogFragment";

    public WeekCpView(@NonNull Context context) {
        super(context);
    }

    public WeekCpView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WeekCpView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @NonNull
    @Override
    protected WeekCpPresenter createPresenter() {
        return new WeekCpPresenter();
    }

    @Override
    protected void findView() {
        ButterKnife.bind(this);
    }

    @Override
    protected void setView() {
        mRoomWeekToCp.post(new Runnable() {
            @Override
            public void run() {
                showView(mPresenter.isSelfOnMic());
            }
        });
        SvgaUtils.getInstance(getContext()).cyclePlayAssetsAnim(mRoomWeekCpSvgaView, "room_lover_week_cp");
    }

    @Override
    protected void setListener() {
        int value = mPresenter.getWeekCpInfo().getHeartValue();
        updateUI(value);
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_week_cp_view;
    }

    @Override
    public void updateUI(int value) {
        String text = String.format(getResources().getString(R.string.room_lover_week_cp_max_heart), value);
        mRoomWeekIsCp.setVisibility(VISIBLE);
        mRoomWeekIsCp.setEnabled(true);
        mRoomWeekToCp.setVisibility(GONE);
        mRoomWeekToCp.setEnabled(false);
        boolean isHasPlayer = TextUtils.isEmpty(mPresenter.getCpPlayerId());
        boolean selfOnMic = mPresenter.isSelfOnMic();
        L.debug(TAG, "updateUI value: %d, isHasPlayer: %b, selfOnMic: %b, text: %s", value, isHasPlayer, selfOnMic, text);
        if (!selfOnMic) {
            resetUI();
        } else {
            if (!isHasPlayer) {
                if (value >= IWeekCpCtrl.ROOM_CP_MAX_VALUE) {
                    if (mPresenter.isCpPlayer()) {
                        mRoomWeekIsCpValue.setText(getResources().getString(R.string.room_lover_week_cp_get_heart));
                        SvgaUtils.getInstance(getContext()).cyclePlayAssetsAnim(mRoomCpSvgaView, "room_lover_add_week_cp_value.svga");
                    } else {
                        mRoomWeekIsCpValue.setText(getResources().getString(R.string.room_lover_week_cp_merry_heart));
                        SvgaUtils.getInstance(getContext()).cyclePlayAssetsAnim(mRoomCpSvgaView, "room_lover_form_week_cp.svga");
                    }
                } else {
                    SvgaUtils.getInstance(getContext()).cyclePlayAssetsAnim(mRoomCpSvgaView, "room_lover_week_cp_trigger.svga");
                    mRoomWeekIsCpValue.setText(text);
                }
            } else {
                resetUI();
            }
        }
        showView(mPresenter.isSelfOnMic());
    }

    @Override
    public void showView(boolean isOnMic) {
        L.debug(TAG, "showView isOnMic: %b", isOnMic);
        if (isOnMic) {
            mRoomCpTopic.setVisibility(VISIBLE);
            mRoomCpTest.setVisibility(VISIBLE);
            mRoomCpTopic.setEnabled(true);
            mRoomCpTest.setEnabled(true);
        } else {
            mRoomCpTopic.setVisibility(INVISIBLE);
            mRoomCpTest.setVisibility(INVISIBLE);
            mRoomCpTopic.setEnabled(false);
            mRoomCpTest.setEnabled(false);
        }
    }

    @Override
    public void resetUI() {
        L.debug(TAG, "resetUI");
        mRoomWeekIsCp.setEnabled(false);
        mRoomWeekToCp.setEnabled(true);
        mRoomWeekIsCp.setVisibility(GONE);
        mRoomWeekToCp.setVisibility(VISIBLE);
        SvgaUtils.getInstance(getContext()).cyclePlayAssetsAnim(mRoomWeekCpSvgaView, "room_lover_week_cp");
    }

    @OnClick(R2.id.room_week_is_cp)
    public void onCpHeartClicked() {
        mPresenter.checkHasCp();
    }

    @OnClick(R2.id.room_cp_topic)
    public void onCpTopicClicked() {
        if (mPresenter.isSelfOnMic()) {
            showDialogFragment(RoomLoverRoute.ROOM_LOVER_SOUL_TOPIC_PATH, "SoulTopicDialogFragment");
        } else {
            showNotCpOnMic();
        }
    }

    @OnClick(R2.id.room_cp_test)
    public void onCpTestClicked() {
        if (TextUtils.isEmpty(mPresenter.getCpPlayerId())) {
            showNotCpOnMic();
        } else {
            showDialogFragment(RoomLoverRoute.ROOM_LOVER_RECEIVE_TEST_PATH, mReceiveTag, true, mPresenter.getCpPlayerId());
        }
    }

    @Override
    public void showTacitTestView() {
        showDialogFragment(RoomLoverRoute.ROOM_LOVER_TACIT_TEST_PATH, mTacitTag);
    }

    @Override
    public void showNotCpOnMic() {
        DialogManager mDialogManager = new DialogManager(getContext());
        mDialogManager.setCanceledOnClickOutside(false);
        mDialogManager.showOkCancelDialog("另一半还没来哦~快邀请TA吧！",
                getResources().getString(R.string.cp_invite),
                getResources().getString(R.string.cancel),
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        NewShareDialog shareDialog = new NewShareDialog(getContext(), true);
                        shareDialog.setHasBroadCast(true);
                        shareDialog.setHasFriend(true);//邀请好友分享
                        if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
                            shareDialog.setHasNoticeFans(true);
                        }
                        shareDialog.setOnShareDialogItemClick(WeekCpView.this);
                        shareDialog.show();
                    }
                });
    }

    @Override
    public void showTip() {
        SingleToastUtil.showToast(getContext(), getContext().getString(R.string.room_lover_await_tacit_test));
    }

    @Override
    public void dismissTacitView() {
        FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
        DialogFragment dialogFragment = (DialogFragment) fragmentManager.findFragmentByTag(mTacitTag);
        L.debug(TAG, "dismissTacitView dialogFragment: %s", dialogFragment);
        if (dialogFragment != null) {
            try {
                dialogFragment.dismissAllowingStateLoss();
            } catch (Exception e) {
                CoreUtils.crashIfDebug(e, "dismissTacitView exception");
            }
            dialogFragment = null;
        }
    }

    @Override
    public void showMeHasCp(String nick) {
        RoomRemoveCurrentCpDialog dialog = RoomRemoveCurrentCpDialog.instance("是否解除当前CP关系，与" + nick + "结成一周CP");
        dialog.show(((FragmentActivity) getContext()).getSupportFragmentManager(), "RoomRemoveCurrentCpDialog");
    }

    @Override
    public void showWeekCpDes() {
        CpRingDescriptionDialog cpRingDescriptionDialog = CpRingDescriptionDialog.instance("一周CP玩法", UriProvider.getWeekCpDesH5());
        cpRingDescriptionDialog.show(((FragmentActivity) getContext()).getSupportFragmentManager(), CpRingDescriptionDialog.TAG);
    }

    public void showReceiveView(String cpId) {
        showDialogFragment(RoomLoverRoute.ROOM_LOVER_RECEIVE_TEST_PATH, mReceiveTag, cpId);
    }

    @Override
    public void showResultTestView(String questionId) {
        showDialogFragment(RoomLoverRoute.ROOM_LOVER_RESULT_TEST_PATH, "TestResultDialogFragment", questionId);
    }

    @OnClick(R2.id.room_week_to_cp)
    public void onRoomCpClicked() {
        mPresenter.checkHasCp();
    }

    private void showDialogFragment(String path, String tag) {
        showDialogFragment(path, tag, false);
    }

    private void showDialogFragment(String path, String tag, boolean isInitiate) {
        showDialogFragment(path, tag, isInitiate, "");
    }

    private void showDialogFragment(String path, String tag, String cpId) {
        showDialogFragment(path, tag, false, cpId);
    }

    private void showDialogFragment(String path, String tag, boolean isInitiate, String cpId) {
        DialogFragment dialogFragment = (DialogFragment) ARouter.getInstance().build(path)
                .withBoolean("initiate", isInitiate)
                .withString("currentId", cpId)
                .navigation(getContext());
        if (dialogFragment.isAdded()) {
            if (dialogFragment.isStateSaved()) {
                dialogFragment.dismissAllowingStateLoss();
            }
        } else {
            FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
            dialogFragment.show(fragmentManager, tag);
        }
    }

    @Override
    public void onSharePlatformClick(Platform platform) {
        mPresenter.share(platform);
    }

    @Override
    public void onSendBroadcastMsg() {
        // 发送寻友广播
        DialogFragment dialogFragment = (DialogFragment) ARouter.getInstance().build(FindFriendsBroadCastRoute.SEND_DIALOG)
                .navigation(getContext());
        if (dialogFragment.isAdded()) {
            if (dialogFragment.isStateSaved()) {
                dialogFragment.dismissAllowingStateLoss();
            }
        } else {
            FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
            dialogFragment.show(fragmentManager, "RoomBroadcastSendDialog");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SvgaUtils.closeSvga(mRoomCpSvgaView);
        SvgaUtils.closeSvga(mRoomWeekCpSvgaView);
    }
}
