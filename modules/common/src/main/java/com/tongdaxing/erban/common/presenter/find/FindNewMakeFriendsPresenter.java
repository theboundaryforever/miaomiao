package com.tongdaxing.erban.common.presenter.find;


import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tongdaxing.erban.common.ui.makefriends.IFindNewMakeFriendsView;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.im.custom.bean.FriendsBroadcastAttachment;
import com.tongdaxing.xchat_core.makefriedns.BroadcastMsgEvent;
import com.tongdaxing.xchat_core.room.broadcastMsg.FindNewMakeFriendsModel;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * 发现-交友页
 * <p>
 * Created by zhangjian on 2019/6/13.
 */
public class FindNewMakeFriendsPresenter extends AbstractMvpPresenter<IFindNewMakeFriendsView> {
    public static final String TAG = "FindNewMakeFriendsPresenter";
    private FindNewMakeFriendsModel model;

    public FindNewMakeFriendsPresenter() {
        model = new FindNewMakeFriendsModel();
    }

    @Override
    public void onCreatePresenter(@Nullable Bundle saveState) {
        super.onCreatePresenter(saveState);
        CoreUtils.register(this);
    }

    @Override
    public void onDestroyPresenter() {
        super.onDestroyPresenter();
        CoreUtils.unregister(this);
    }

    public void requestMsgList(int pageNum) {
        model.requestMsgList(pageNum, new OkHttpManager.MyCallBack<ServiceResult<List<FriendsBroadcastAttachment>>>() {

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(ServiceResult<List<FriendsBroadcastAttachment>> response) {
                List<FriendsBroadcastAttachment> data = response.getData();
                if (response.isSuccess() && data != null) {
                    if (getMvpView() != null) {
                        for (int i = 0; i < data.size(); i++) {
                            data.get(i).setBlueBg(i % 2 == 0);
                        }
                        getMvpView().setupView(data);
                    }
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveNewMsg(BroadcastMsgEvent.OnReceiveNewMsg onReceiveNewMsg) {
        ChatRoomMessage chatRoomMessage = onReceiveNewMsg.getChatRoomMessage();
        if (chatRoomMessage != null) {
            FriendsBroadcastAttachment attachment = (FriendsBroadcastAttachment) chatRoomMessage.getAttachment();
            if (attachment != null && attachment.getRoomUid() > 0) {
                if (getMvpView() != null) {
                    getMvpView().addAttachment(attachment);
                }
            }
        } else {
            //获取历史消息
            List<FriendsBroadcastAttachment> attachments = onReceiveNewMsg.getAttachments();
            L.debug(TAG, "onReceiveNewMsg attachments.size() = %d", attachments == null ? -1 : attachments.size());
            if (attachments != null && attachments.size() > 0) {
                for (int i = 0; i < attachments.size(); i++) {
                    attachments.get(i).setBlueBg(i % 2 == 0);
                }
                if (getMvpView() != null) {
                    getMvpView().setupView(attachments);
                }
            }
        }
    }
}
