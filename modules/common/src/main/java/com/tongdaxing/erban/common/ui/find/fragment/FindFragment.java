package com.tongdaxing.erban.common.ui.find.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.presenter.find.FindPresenter;
import com.tongdaxing.erban.common.ui.find.activity.CreateRoomActivity;
import com.tongdaxing.erban.common.ui.find.view.IFindView;
import com.tongdaxing.erban.common.ui.home.adpater.CommonImageIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.MainAdapter;
import com.tongdaxing.erban.common.ui.home.fragment.HomeFragment;
import com.tongdaxing.erban.common.ui.makefriends.FindNewMakeFriendsFragment;
import com.tongdaxing.erban.common.ui.rankinglist.RankingListActivity;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.ColorUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Function:新版发现页面
 * Author: Edward on 2019/3/8
 */
@CreatePresenter(FindPresenter.class)
public class FindFragment extends BaseMvpFragment<IFindView, FindPresenter> implements IFindView,
        CommonMagicIndicatorAdapter.OnItemSelectListener {
    public static final String TAG = "FindFragment";
    @BindView(R2.id.main_indicator)
    MagicIndicator mainIndicator;
    @BindView(R2.id.main_viewpager)
    ViewPager mainViewpager;
    @BindView(R2.id.new_find_top_layout)
    RelativeLayout newFindTopLayout;
    Unbinder unbinder;
    private List<TabInfo> mTabInfoList;

    private int hindH = 400;//滑动别色距离
    private float saveToolBarOffset;
    private float currToolBarOffset;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_new_find;
    }

    private List<TabInfo> getTabDefaultList() {
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(1, "交友"));
        tabInfoList.add(new TabInfo(2, "处对象"));
        return tabInfoList;
    }

    public void setupToolBar(float toolBarOffset) {
        if (newFindTopLayout == null) {
            return;
        }
        if (toolBarOffset > 1) {
            toolBarOffset = 1;
        }
        if (this.currToolBarOffset != toolBarOffset) {
            this.currToolBarOffset = toolBarOffset;
            int color = ColorUtil.changeColor(0xFFBACDFB, 0xFFFFFFFF, toolBarOffset);
            newFindTopLayout.setBackgroundColor(color);
            return;
        }
        if (toolBarOffset == 1) {
            newFindTopLayout.setBackgroundColor(0xFFFFFFFF);
        }
    }


    @Override
    public void onSetListener() {
//        ivVoiceGroupPublish.setOnClickListener(v -> {
//            PublishVoiceGroupActivity.start(FindFragment.this);
//        });
    }

    @Override
    public void initiate() {

    }

    @Override
    public void onItemSelect(int position) {
        //分类点击
        mainViewpager.setCurrentItem(position);
    }

    @OnClick(R2.id.find_new_make_friends_rank_lsit)
    public void onFindNewMakeFriendsRankLsitClicked() {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), RankingListActivity.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.anim_bottom_in, R.anim.anim_bottom_out);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onFindViews() {
        mainViewpager.setOffscreenPageLimit(1);

        mTabInfoList = getTabDefaultList();
        CommonImageIndicatorAdapter mMsgIndicatorAdapter = new CommonImageIndicatorAdapter(mContext, mTabInfoList);
        mMsgIndicatorAdapter.setSize(18);
        mMsgIndicatorAdapter.setNormalColorId(R.color.common_color_14_transparent_87);
        mMsgIndicatorAdapter.setSelectColorId(R.color.color_000000);
        mMsgIndicatorAdapter.setImageIndicatorWidth(35.0f);
        mMsgIndicatorAdapter.setImageIndicatorHeight(14.0f);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        mainIndicator.setNavigator(commonNavigator);

        SparseArray<Fragment> mFragmentList = new SparseArray<>();

        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(FindNewMakeFriendsFragment.TAG);
        FindNewMakeFriendsFragment findNewMakeFriendsFragment;
        if (fragment != null) {
            findNewMakeFriendsFragment = (FindNewMakeFriendsFragment) fragment;
        } else {
            findNewMakeFriendsFragment = new FindNewMakeFriendsFragment();
        }
        fragment = fragmentManager.findFragmentByTag(FindWarmAccompanyFragment.TAG);
        FindWarmAccompanyFragment findWarmAccompanyFragment;
        if (fragment != null) {
            findWarmAccompanyFragment = (FindWarmAccompanyFragment) fragment;
        } else {
            findWarmAccompanyFragment = new FindWarmAccompanyFragment();
        }

        mFragmentList.put(0, findNewMakeFriendsFragment);
        mFragmentList.put(1, findWarmAccompanyFragment);
        mainViewpager.setAdapter(new MainAdapter(getChildFragmentManager(), mFragmentList));
        ViewPagerHelper.bind(mainIndicator, mainViewpager);
        mainViewpager.setCurrentItem(0);//默认选中首页
        mainViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    newFindTopLayout.setBackgroundColor(getResources().getColor(R.color.find_new_make_friends_title_bg));
                    UmengEventUtil.getInstance().onEvent(getContext(), UmengEventId.getFindSocials());
                    setupToolBar(saveToolBarOffset);
                } else {
                    UmengEventUtil.getInstance().onFindSocialsCouplesList(mContext, UmengEventId.getFindSocialsCouplesListItem(0));
                    newFindTopLayout.setBackgroundColor(getResources().getColor(R.color.white));
                    UmengEventUtil.getInstance().onEvent(getContext(), UmengEventId.getFindWarmAccompany());
                    setupToolBar(1);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    saveToolBarOffset = currToolBarOffset;
                }
            }
        });
        findNewMakeFriendsFragment.setOnScrollChangedListener(new HomeFragment.OnScrollChangedListener() {
            @Override
            public void onScrollChanged(RecyclerView recyclerView) {
                if (mainViewpager != null && mainViewpager.getCurrentItem() == 0) {
                    float verticalOffset = recyclerView.computeVerticalScrollOffset();
                    setupToolBar(verticalOffset / (float) hindH);
                }
            }
        });
    }

    @OnClick(R2.id.find_new_make_friends_create_room)
    public void onFindNewMakeFriendsCreateRoomClicked() {
        UmengEventUtil.getInstance().onCreateRoom(mContext, "find");
        CreateRoomActivity.start(mContext, mainViewpager.getCurrentItem() == 0 ? 3 : 2);
    }

    //--------------------------寻友广播时长统计埋点操作--------------------------

    /**
     * 首次初始化
     */
    private boolean mFirstInit = true;
    /**
     * 是否可见
     */
    private boolean mVisiable;

    @Override
    public void onHiddenChanged(boolean hidden) {
        L.debug(TAG, "onHiddenChanged----" + this.toString());
        if (!hidden) {
            mVisiable = true;
            if (mainViewpager.getCurrentItem() == 0) {
                UmengEventUtil.getInstance().startStatFindBroadCast(FindFragment.class);
            }
        } else {
            mVisiable = false;
            UmengEventUtil.getInstance().endStatFindBroadCast(FindFragment.class);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        L.debug(TAG, "onResume----" + this.toString());
        mVisiable = true;
        //若首次初始化,默认可见并开启友盟统计
        if (mFirstInit) {
            mFirstInit = false;
            UmengEventUtil.getInstance().startStatFindBroadCast(FindFragment.class);
            return;
        }

        //若当前界面可见,调用友盟开启跳转统计
        if (mainViewpager.getCurrentItem() == 0) {
            UmengEventUtil.getInstance().startStatFindBroadCast(FindFragment.class);
        }
    }

    @Override
    public void onPause() {
        L.debug(TAG, "onPause----" + this.toString());
        super.onPause();
        //若当前界面可见,调用友盟结束跳转统计
        if (mVisiable) {
            UmengEventUtil.getInstance().endStatFindBroadCast(FindFragment.class);
        }
    }
    //--------------------------寻友广播时长统计埋点操作--------------------------
}
