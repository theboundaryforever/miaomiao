package com.tongdaxing.erban.common.ui.widget.itemdecotion;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;

/**
 * 等间隔grid的ItemDecoration
 * 这里不设置最右边的space，最右边的间隔在RecyclerView布局设
 */
public class GridSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int itemHorizontalSpace, itemVerticalSpace;
    private BaseQuickAdapter adapter;

    /**
     * @param itemHorizontalSpace 横间隔
     * @param itemVerticalSpace   竖间隔
     */
    public GridSpaceItemDecoration(int itemHorizontalSpace, int itemVerticalSpace) {
        this.itemHorizontalSpace = itemHorizontalSpace;
        this.itemVerticalSpace = itemVerticalSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int itemPosition = parent.getChildLayoutPosition(view) - (adapter != null ? adapter.getHeaderLayoutCount() : 0);
        if (itemPosition >= 0) {
            outRect.left = itemHorizontalSpace;
            outRect.bottom = itemVerticalSpace;
        }
    }

    /**
     * 设置绑定的adapter
     */
    public void setupBindAdapter(BaseQuickAdapter adapter) {
        this.adapter = adapter;
    }
}
