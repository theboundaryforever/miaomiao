package com.tongdaxing.erban.common.ui.message.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.user.bean.FansInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

/**
 * Created by chenran on 2017/10/2.
 */

public class FansViewAdapter extends BaseQuickAdapter<FansInfo, BaseViewHolder> {

    private OnItemClickListener onItemClickListener;

    public FansViewAdapter(List<FansInfo> fansInfoList) {
        super(R.layout.fans_list_item, fansInfoList);
    }

    public void setRylListener(OnItemClickListener onClickListener) {
        onItemClickListener = onClickListener;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final FansInfo fansInfo) {
        if (fansInfo == null) return;
        baseViewHolder.setText(R.id.tv_userName, fansInfo.getNick())
                .setVisible(R.id.tv_state, fansInfo.isValid())
                .setVisible(R.id.view_line, baseViewHolder.getLayoutPosition() != getItemCount() - 1)
                .setOnClickListener(R.id.rly, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(fansInfo);
                        }
                    }
                })
                .setOnClickListener(R.id.attention_img, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onAttentionBtnClick(fansInfo);
                        }
                    }
                })
        ;
        TextView attention = baseViewHolder.getView(R.id.attention_img);
        boolean state = !CoreManager.getCore(IIMFriendCore.class).isMyFriend(fansInfo.getUid() + "");
        attention.setEnabled(state);
        if (state) {
            attention.setText("关注");
        } else {
            attention.setText("已关注");
        }
        ImageView avatar = baseViewHolder.getView(R.id.imageView);
        ImageLoadUtils.loadCircleImage(mContext, fansInfo.getAvatar(), avatar, R.drawable.ic_no_avatar);
    }

    public interface OnItemClickListener {
        void onItemClick(FansInfo fansInfo);

        void onAttentionBtnClick(FansInfo fansInfo);
    }
}
