package com.tongdaxing.erban.common.room.lover.weekcp.tacit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.erban.ui.adapter.BaseAdapterHelper;
import com.erban.ui.adapter.ListAdapter;
import com.erban.ui.mvp.MVPBaseFrameLayout;
import com.opensource.svgaplayer.SVGAImageView;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.utils.SvgaUtils;
import com.tongdaxing.xchat_core.room.weekcp.bean.TacitTestBean;
import com.tongdaxing.xchat_core.room.weekcp.tacit.ITacitTestCtrl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by Chen on 2019/5/7.
 */
public class TacitTestView extends MVPBaseFrameLayout<ITacitTestView, TacitTestPresenter> implements ITacitTestView {
    @BindView(R2.id.room_lover_tacit_question_count)
    TextView mTacitQuestionCount;
    @BindView(R2.id.room_lover_tacit_question_tittle)
    TextView mTacitQuestionTittle;
    @BindView(R2.id.room_lover_tacit_question_answer)
    GridView mTacitQuestionAnswer;
    @BindView(R2.id.room_lover_ask_question_time)
    TextView mAskQuestionTime;
    @BindView(R2.id.room_lover_question_layout)
    LinearLayout mQuestionLayout;
    @BindView(R2.id.room_lover_tacit_await)
    SVGAImageView mTacitAwait;
    @BindView(R2.id.room_lover_tacit_await_layout)
    LinearLayout mTacitAwaitLayout;
    @BindView(R2.id.room_lover_tacit_close)
    ImageView mTacitClose;
    private String TAG = "TacitTestView";
    private ListAdapter<String> mTacitTestAdapter;
    private int mQuestionTab;
    private List<String> mTacitQuestions;
    private int mQuestionCount;

    public TacitTestView(@NonNull Context context) {
        super(context);
        init();
    }

    public TacitTestView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TacitTestView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

    }

    @Override
    public int getContentViewId() {
        return R.layout.room_lover_tacit_test_view;
    }

    @NonNull
    @Override
    protected TacitTestPresenter createPresenter() {
        return new TacitTestPresenter();
    }

    @Override
    protected void findView() {
        ButterKnife.bind(this);
    }

    @Override
    protected void setView() {
        int state = mPresenter.getTacitCurrentState();
        L.debug(TAG, "setView state: %s", state);
        showQuestionView();
    }

    private void showQuestionView() {
        mQuestionLayout.setVisibility(VISIBLE);
        mAskQuestionTime.setVisibility(VISIBLE);
        mTacitAwaitLayout.setVisibility(GONE);

        mQuestionTab = mPresenter.getCurrentQuestion();
        updateQuestionView();
    }

    private void showAwaitView() {
        mQuestionLayout.setVisibility(GONE);
        mAskQuestionTime.setVisibility(GONE);
        mTacitAwaitLayout.setVisibility(VISIBLE);

        showAnswerAwaitSvga();
    }

    private void updateQuestionView() {
        mQuestionCount = mPresenter.getTacitQuestions().size();
        if (mQuestionTab < mQuestionCount) {
            TacitTestBean.DataBean.QuestionBean dataBean = mPresenter.getTacitQuestions().get(mQuestionTab);
            if (dataBean != null) {
                mTacitQuestions = dataBean.getItems();
                L.debug(TAG, "updateQuestionView mTacitQuestions: %s, question: %s", mTacitQuestions, dataBean.getQuestion());
                mTacitQuestionTittle.setText(dataBean.getQuestion());

                setQuestionText();

                setAdapter();
            }
        }
    }

    private void setQuestionText() {
        mTacitQuestionCount.setText((mQuestionTab + 1) + "/" + mQuestionCount);
    }

    private void setTimerText(int time) {
        if (time % ITacitTestCtrl.TACIT_TEST_TIME == 0) {
            next(0);
        }
        mAskQuestionTime.setText(time + "S");
    }

    @Override
    protected void setListener() {

    }

    private void setAdapter() {
        if (mTacitTestAdapter == null) {
            mTacitTestAdapter = new ListAdapter<String>(getContext(), R.layout.room_lover_soul_topic_item, mTacitQuestions) {
                @Override
                public void onUpdate(BaseAdapterHelper helper, String item, int position) {
                    TextView textView = helper.getView(R.id.room_lover_soul_question);
                    textView.setText(item);
                }
            };
            mTacitQuestionAnswer.setAdapter(mTacitTestAdapter);
        } else {
            mTacitTestAdapter.replaceAll(mTacitQuestions);
        }
    }

    @OnClick(R2.id.room_lover_tacit_close)
    public void onCloseView() {
        DialogManager mDialogManager = new DialogManager(getContext());
        mDialogManager.setCanceledOnClickOutside(false);
        mDialogManager.showOkCancelDialog(getResources().getString(R.string.room_lover_tacit_exit_text),
                getResources().getString(R.string.room_lover_tacit_exit),
                getResources().getString(R.string.cancel),
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        mPresenter.exitTacit();
                        CoreUtils.send(new TacitTestAction.OnDismissTacitTestView());
                    }
                });
    }

    @OnItemClick(R2.id.room_lover_tacit_question_answer)
    public void onAskQuestionClicked(AdapterView<?> parent, View view, int position, long id) {
        if (mTacitQuestions != null && position < mTacitQuestions.size()) {
            next(position);
        }
    }

    private void next(int position) {
        String question = mTacitQuestions.get(position);
        L.debug(TAG, "onAskQuestionClicked: %s", question);
        mPresenter.addAnswer(question);
        mQuestionTab++;
        setQuestionText();
        if (mQuestionTab >= mQuestionCount) {
            showAwaitView();
            mPresenter.submitAnswer();
        } else {
            nextQuestion();
        }
    }

    private void nextQuestion() {
        updateQuestionView();
    }

    @Override
    public void updateTime(int time) {
        setTimerText(time);
    }

    private void showAnswerAwaitSvga() {
        SvgaUtils.getInstance(getContext()).cyclePlayAssetsAnim(mTacitAwait, "room_lover_tacit_await.svga");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SvgaUtils.closeSvga(mTacitAwait);
    }
}
