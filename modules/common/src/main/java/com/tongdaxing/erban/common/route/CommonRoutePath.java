package com.tongdaxing.erban.common.route;

/**
 * Created by Chen on 2019/5/15.
 */
public class CommonRoutePath {
    public final static String MAIN_ACTIVITY_ROUTE_PATH = "/erban_client/MainActivity";

    //发现页
    public final static String INVITE_AWARD_ACTIVITY_ROUTE_PATH = "/erban_client/InviteAwardActivity";
    public final static String CHAT_HALL_ACTIVITY_ROUTE_PATH = "/erban_client/ChatHallActivity";
    public final static String AUDIO_MATCH_ACTIVITY_ROUTE_PATH = "/erban_client/AudioMatchActivity";
    public final static String AUDIO_CARD_2_ACTIVITY_ROUTE_PATH = "/erban_client/AudioCard2Activity";
    public final static String BLACK_LIST_ACTIVITY_ROUTE_PATH = "/erban_client/BlackListActivity";
}
