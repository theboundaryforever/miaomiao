package com.tongdaxing.erban.common.ui.cpexclusive.editSign;

import android.text.TextUtils;

import com.erban.ui.mvp.BasePresenter;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.cp.CpEditSignEvent;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.Map;

/**
 * Created by zhangjian on 2019/4/30.
 */
public class CpSignPresenter extends BasePresenter<ICpEditSignView> {

    private static final String TAG = CpSignPresenter.class.getSimpleName();

    public void setCpSign(final String sign) {
        String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
        if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(sign)) {
            if (sign.matches(sensitiveWordData)) {
                L.info(TAG, "setCpSign() sign = %s, it has sensitiveWordData", sign);
                SingleToastUtil.showToast("修改失败，小喵提醒您文明用语~");
                return;
            }
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("sign", sign);
        OkHttpManager.getInstance().doGetRequest(UriProvider.getSetCpSign(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "setCpSign() sign = " + sign, e);
                if (getView() != null) {
                    getView().setCpSignFailure(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response == null || getView() == null) {
                    L.error(TAG, "setCpSign() sign = %s, response == null: %b, getView() == null: %b", sign, response == null, getView() == null);
                    return;
                }
                L.info(TAG, "setCpSign() sign = %s, response = %s", sign, response);
                int code = response.getCode();
                if (code == 200) {
                    CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid());//更新本地登录用户信息
                    CoreUtils.send(new CpEditSignEvent.OnEditSuccess(sign));
                    getView().setCpSignSucceed();
                } else if (code == 2401) {//敏感词
                    getView().editFailureSensitiveWord();
                } else if (code == 2103) {//余额不足
                    CoreUtils.send(new CpEditSignEvent.OnEditNeedCharge());
                } else {
                    onError(new Exception(response.getMessage()));
                }
            }
        });
    }

}
