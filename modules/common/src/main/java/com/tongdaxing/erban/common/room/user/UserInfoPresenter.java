package com.tongdaxing.erban.common.room.user;

import android.text.TextUtils;

import com.erban.ui.mvp.BasePresenter;
import com.tongdaxing.erban.common.model.ReportModel;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.praise.PraiseEvent;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomStatus;
import com.tongdaxing.xchat_core.room.weekcp.RoomLoverEvent;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.UserEvent;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Map;

/**
 * Created by Chen on 2019/6/12.
 */
public class UserInfoPresenter extends BasePresenter<IUserInfoDialogView> {
    private String TAG = "UserInfoPresenter";
    private static ReportModel mReportModel;

    private static HttpRequestCallBack<Object> reportCallBack = new HttpRequestCallBack<Object>() {
        @Override
        public void onFinish() {

        }

        @Override
        public void onSuccess(String message, Object response) {
            SingleToastUtil.showToast("举报成功，我们会尽快为您处理");
        }

        @Override
        public void onFailure(int code, String msg) {
            SingleToastUtil.showToast(msg);
        }
    };

    public void checkAttention(long userId) {
        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        CoreManager.getCore(IPraiseCore.class).isPraised(currentUid, userId);
    }

    public void reportCommit(int reportType) {
        if (mReportModel == null) {
            mReportModel = new ReportModel();
        }
        mReportModel.reportCommit(2, reportType, reportCallBack);
    }

    public void doBlack(String userId) {
        IIMFriendCore core = CoreManager.getCore(IIMFriendCore.class);
        if (core.isUserInBlackList(userId)) {
            core.removeFromBlackList(userId);
        } else {
            core.addToBlackList(userId);
        }
    }

    public void attention(long userId, boolean isAttention) {
        if (isAttention) {
            CoreManager.getCore(IPraiseCore.class).cancelPraise(userId);
        } else {
            CoreManager.getCore(IPraiseCore.class).praise(userId);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionSuccess(PraiseEvent.OnAttentionSuccess success) {
        if (getView() == null) {
            return;
        }
        long uid = success.getUid();
        L.debug(TAG, "onAttentionSuccess uid: %d", uid);

        getView().attentionSuccess(uid, success.isAttention());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserInfoUpdate(UserEvent.OnUserInfoUpdate update) {
        L.debug(TAG, "onUserInfoUpdate");
        if (getView() == null) {
            return;
        }
        getView().updateView(update.getUserInfo());
    }

    public boolean isSelf(long userId) {
        return AvRoomDataManager.get().isSelf(userId);
    }

    public RoomInfo getRoomInfo() {
        return AvRoomDataManager.get().mCurrentRoomInfo;
    }

    public UserInfo getUserInfo(long userId) {
        return CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId, true);
    }

    public boolean checkBlack(long userId) {
        IIMFriendCore core = CoreManager.getCore(IIMFriendCore.class);
        return core.isUserInBlackList(String.valueOf(userId));
    }

    public void checkRoomStatus(long userId) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("queryUid", String.valueOf(userId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getTheRoomStatus(), params, new OkHttpManager.MyCallBack<ServiceResult<RoomStatus>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<RoomStatus> response) {
                if (response == null) {
                    return;
                }
                if (response.isSuccess() && response.getData() != null && response.getData().getUid() > 0) {
                    if (getView() != null) {
                        getView().showIntoRoomImage();
                    }
                }
            }
        });
    }

    public void enterRoom(long userId) {
        CoreManager.getCore(IRoomCore.class).getUserRoom(userId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCheckEnterRoomStatus(NewRoomEvent.OnCheckEnterRoomStatus status) {
        if (getView() == null) {
            return;
        }
        if (status.isSuccess()) {
            getView().enterRoom(status.getRoomId());
        } else {
            SingleToastUtil.showToast(status.getMessage());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWeekCpOffice(RoomLoverEvent.OnCpStateChange stateChange) {
        if (getView() == null) {
            return;
        }
        String cpId = stateChange.getCpId();
        if (TextUtils.isEmpty(cpId)) {
            getView().onP2PCpRemove();
        } else {
            getView().onP2PCpInviteAgree();
        }
        CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid());//更新本地登录用户信息

    }
}
