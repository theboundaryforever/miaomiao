package com.tongdaxing.erban.common.room.audio.widget;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import com.juxiao.library_ui.widget.ViewHolder;
import com.juxiao.library_ui.widget.dialog.BaseDialog;
import com.tongdaxing.erban.common.R;

/**
 * Function:
 * Author: Edward on 2019/2/18
 */
public class CloudUploadDialog extends BaseDialog {
    private final String URL = "https://www.miaomiaofm.com";

    @Override
    public void convertView(ViewHolder viewHolder) {
        TextView textView = viewHolder.getView(R.id.tv_title);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(
                "电脑前往" + URL + "\n自行上传音乐及编辑我的曲库");
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.mm_theme));
        spannableStringBuilder.setSpan(foregroundColorSpan, "电脑前往".length(),
                ("电脑前往" + URL).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spannableStringBuilder);

        viewHolder.setOnClickListener(R.id.tv_sure, v -> {
            dismiss();
        });
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_cloud_upload;
    }
}
