package com.tongdaxing.erban.common.room;

import com.erban.ui.mvp.BasePresenter;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * Created by Chen on 2019/6/6.
 */
public class RoomBasePresenter<T> extends BasePresenter<T> {

    public RoomInfo getRoomInfo() {
        return AvRoomDataManager.get().mCurrentRoomInfo;
    }

    public UserInfo getRoomOwner() {
        return CoreManager.getCore(IAVRoomCore.class).getRoomOwner();
    }

    public boolean isRoomOwner() {
        return AvRoomDataManager.get().isRoomOwner();
    }

    public boolean isSelfOnMic() {
        return AvRoomDataManager.get().isSelfOnMic();
    }
}
