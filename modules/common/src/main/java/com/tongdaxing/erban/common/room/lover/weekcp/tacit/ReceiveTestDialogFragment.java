package com.tongdaxing.erban.common.room.lover.weekcp.tacit;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.baseview.BaseDialogFragment;
import com.tcloud.core.util.DensityUtil;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.room.lover.RoomLoverRoute;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.weekcp.tacit.ITacitTestCtrl;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chen on 2019/5/7.
 */
@Route(path = RoomLoverRoute.ROOM_LOVER_RECEIVE_TEST_PATH)
public class ReceiveTestDialogFragment extends BaseDialogFragment {

    @BindView(R2.id.room_lover_tacit_content)
    TextView mTacitContent;
    @BindView(R2.id.room_lover_initiate_receive)
    TextView mInitiateReceive;
    @Autowired(name = "initiate")
    boolean mInitiate;
    @Autowired(name = "currentId")
    String mCpId;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                WindowManager.LayoutParams params = window.getAttributes();
                params.gravity = Gravity.BOTTOM; // 显示在底部
                params.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度填充满屏
                params.height = (int) (DensityUtil.getDisplayHeight(BaseApp.mBaseContext) * 0.6f);
                window.setAttributes(params);
                // 这里用透明颜色替换掉系统自带背景
                int color = ContextCompat.getColor(BaseApp.mBaseContext, android.R.color.transparent);
                window.setBackgroundDrawable(new ColorDrawable(color));
            }
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_lover_dialog_fragment_receive_test;
    }

    @Override
    public void initBefore() {

    }

    @Override
    public void findView() {
        ARouter.getInstance().inject(this);
        ButterKnife.bind(this, mContentView);
    }

    @Override
    public void setView() {
        if (mInitiate) {
            mInitiateReceive.setText(getResources().getString(R.string.room_lover_initiate));
            mTacitContent.setText(getResources().getString(R.string.room_lover_initiate_tacit_text));
        } else {
            mInitiateReceive.setText(getResources().getString(R.string.room_lover_receive));
            mTacitContent.setText(getResources().getString(R.string.room_lover_receive_tacit_text));
        }
    }

    @Override
    public void setListener() {

    }


    @OnClick(R2.id.room_lover_initiate_receive)
    public void onViewClicked() {
        if (AvRoomDataManager.get().isSelfOnMic()) {
            ITacitTestCtrl tacitTestCtrl = CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTestCtrl();
            if (mInitiate) {
                tacitTestCtrl.doAskTacit(mCpId);
            } else {
                tacitTestCtrl.doAnswerTacit(mCpId);
            }
        } else {
            SingleToastUtil.showToast(getContext(), "接受失败，你当前不在情侣位上", Toast.LENGTH_SHORT);
        }
        dismissAllowingStateLoss();
    }
}
