package com.tongdaxing.erban.common.base.fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;

/**
 * 解决AppCompatDialogFragment show方法的异常问题
 */
public class BaseDialogFragment extends AppCompatDialogFragment {

    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        if (fragmentManager != null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            if (this.isAdded()) {
                transaction.show(this);
            } else {
                transaction.add(this, tag).addToBackStack(null);
            }
            transaction.commitAllowingStateLoss();
        }
    }


    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
