package com.tongdaxing.erban.common.base.fragment;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.umeng.analytics.MobclickAgent;

/**
 * 该基类适用于填充在Viewpager中的Fragment,用于友盟统计
 * <p>
 * Created by zhangjian on 2019/7/3.
 */
public class BaseVPFragment<V extends IMvpBaseView, P extends AbstractMvpPresenter<V>> extends BaseMvpFragment {
    private static final String TAG = "BaseVPFragment";

    /**
     * 是否可见
     */
    private boolean mVisiable;

    /**
     * 是否已经开始统计
     */
    private boolean hasStarted;

    @Override
    public void onResume() {
        L.debug(TAG, this.toString() + "----onResume");
        super.onResume();
        //若当前界面可见,调用友盟开启跳转统计
        if (mVisiable && !hasStarted) {
            L.debug(TAG, "开始统计%s", getClass().getName());
            MobclickAgent.onPageStart(getClass().getName());
        }
    }

    @Override
    public void onPause() {
        L.debug(TAG, this.toString() + "----onPause");
        super.onPause();
        //若当前界面可见,调用友盟结束跳转统计
        if (mVisiable) {
            hasStarted = false;
            L.debug(TAG, "结束统计%s", getClass().getName());
            MobclickAgent.onPageEnd(getClass().getName());
        }
    }

    @Override
    public int getRootLayoutId() {
        return 0;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        L.debug(TAG, this.toString() + "----setUserVisibleHint----" + isVisibleToUser);
        mVisiable = isVisibleToUser;
        if (isVisibleToUser) {
            hasStarted = true;
            L.debug(TAG, "开始统计%s", getClass().getName());
            MobclickAgent.onPageStart(getClass().getName());
        } else {
            if (hasStarted) {
                hasStarted = false;
                L.debug(TAG, "结束统计%s", getClass().getName());
                MobclickAgent.onPageEnd(getClass().getName());
            }
        }
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {

    }
}
