package com.tongdaxing.erban.common.ui.audiomatch;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.R;

/**
 * 声音匹配（声控星球）页面
 * <p>
 * Created by zhangjian on 2019/6/14.
 */
@Route(path = CommonRoutePath.AUDIO_MATCH_ACTIVITY_ROUTE_PATH)
public class AudioMatchActivity extends BaseActivity {

    @Override
    public boolean blackStatusBar() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_match_activity);
        initView();
    }

    private void initView() {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        AudioMatchFragment audioMatchFragment = (AudioMatchFragment) supportFragmentManager.findFragmentByTag(AudioMatchFragment.TAG);
        if (audioMatchFragment == null) {
            audioMatchFragment = new AudioMatchFragment();
        }
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.audio_match_container, audioMatchFragment, AudioMatchFragment.TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }
}
