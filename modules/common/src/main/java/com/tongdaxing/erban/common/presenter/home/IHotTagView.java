package com.tongdaxing.erban.common.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;

import java.util.List;

public interface IHotTagView extends IMvpBaseView {

    void setViewLoading();

    void setupListView(boolean isRefresh, List<HomeRoom> homeRooms, int bannerPosition);

    void setupBanner(List<BannerInfo> bannerInfoList);

    void setupPopularList(List<HomeRoom> popularInfoList);

    void setupFailView(boolean isRefresh, String msg);

    void showNewUserRoomGuideDialog(HomeRoom homeRoom);
}

