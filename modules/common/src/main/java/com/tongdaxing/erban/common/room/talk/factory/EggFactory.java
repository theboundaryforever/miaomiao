package com.tongdaxing.erban.common.room.talk.factory;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.juxiao.library_ui.widget.EmojiTextView;
import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NewPushAttachment;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.talk.message.EggMessage;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.RichTextUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Administrator on 5/31 0031.
 */

public class EggFactory implements AbsBaseViewHolder.ViewHolderFactory {
    @NonNull
    @Override
    public EggViewHolder create(@NonNull ViewGroup parent) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_talk_egg_item, parent, false);
        return new EggViewHolder(v);
    }

    public static class EggViewHolder extends AbsBaseViewHolder<EggMessage> {
        private EmojiTextView mTvContent;
        private String mAccount;

        public EggViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvContent = itemView.findViewById(R.id.talk_iv_egg_content);
            mTvContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatRoomMessage chatRoomMessage = (ChatRoomMessage) v.getTag();
                    CoreUtils.send(new RoomTalkEvent.OnRoomMsgItemClicked(chatRoomMessage));
                }
            });
        }

        @Override
        public void bind(EggMessage message) {
            super.bind(message);
            ChatRoomMessage chatRoomMessage = message.getChatRoomMessage();
            mTvContent.setTag(chatRoomMessage);
            mTvContent.setBackground(null);
            CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
            int first = attachment.getFirst();
            if (first == CustomAttachment.CUSTOM_MSG_LOTTERY_BOX) {
                setLotteryInfo((LotteryBoxAttachment) attachment);
            } else if (first == CustomAttachment.CUSTOM_MSG_TYPE_NEW_PUSH_FIRST) {
                setNewPush((NewPushAttachment) attachment);
            }
        }

        private void setLotteryInfo(LotteryBoxAttachment attachment) {
            String params = attachment.getParams();
            Json json = Json.parse(params);
            String senderNick = json.str("nick");
            String giftName = json.str("giftName");
            long uid = json.num_l("uid");
            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            HashMap<String, Object> map;
            map = new HashMap<String, Object>();
            if (json.boo("isFull")) {
                map.put(RichTextUtil.RICHTEXT_STRING, "厉害了, ");
            } else {
                map.put(RichTextUtil.RICHTEXT_STRING, "哇塞~");
            }
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, senderNick);
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            map = new HashMap<String, Object>();
            if (json.boo("isFull")) {
                map.put(RichTextUtil.RICHTEXT_STRING, " 在砸金蛋中，砸中了");
            } else {
                map.put(RichTextUtil.RICHTEXT_STRING, " 在本房间砸金蛋，砸中了");
            }
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            map = new HashMap<String, Object>();
            if (json.has("goldPrice")) {
                map.put(RichTextUtil.RICHTEXT_STRING, giftName + "（" + json.num("goldPrice") + "金币）");
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFD800);
            } else {
                map.put(RichTextUtil.RICHTEXT_STRING, giftName);
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFD800);
            }
            list.add(map);
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, " x" + json.str("count"));
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFD800);
            list.add(map);
            mTvContent.setText(RichTextUtil.getSpannableStringFromList(list));

            setEggBg(json);

            mTvContent.setOnClickListener(v -> {
                UserInfoDialogManager.showDialogFragment(mTvContent.getContext(), uid);
            });
        }

        private void setNewPush(NewPushAttachment attachment) {
            String params = attachment.getParams();
            Json json = Json.parse(params);
            long uid = json.num_l("uid");
            String senderNick = json.str("nick");
            String giftName = json.str("giftName");
            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            HashMap<String, Object> map;
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, "厉害了, ");
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, senderNick);
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, " 在砸金蛋中，砸中了");
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);
            map = new HashMap<String, Object>();
            if (json.has("goldPrice")) {
                map.put(RichTextUtil.RICHTEXT_STRING, giftName + "（" + json.num("goldPrice") + "金币）");
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFD800);
            } else {
                map.put(RichTextUtil.RICHTEXT_STRING, giftName);
                map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFD800);
            }
            list.add(map);
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, " x" + json.str("count"));
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFD800);
            list.add(map);
            mTvContent.setText(RichTextUtil.getSpannableStringFromList(list));

            setEggBg(json);

            mTvContent.setOnClickListener(v -> {
                UserInfoDialogManager.showDialogFragment(mTvContent.getContext(), uid);
            });
        }

        private void setEggBg(Json json) {
            int goldPrice = json.num("goldPrice");
            if (goldPrice < 500) {
                mTvContent.setBackground(null);
            } else if (goldPrice < 5000) {
                mTvContent.setBackground(ContextCompat.getDrawable(mTvContent.getContext(), R.drawable.egg_small_bg));
            } else if (goldPrice < 30000) {
                mTvContent.setBackground(ContextCompat.getDrawable(mTvContent.getContext(), R.drawable.egg_middle_bg));
            } else {
                mTvContent.setBackground(ContextCompat.getDrawable(mTvContent.getContext(), R.drawable.egg_big_bg));
            }
        }
    }
}

