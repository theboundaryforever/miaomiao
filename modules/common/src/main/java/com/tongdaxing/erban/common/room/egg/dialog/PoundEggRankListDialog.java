package com.tongdaxing.erban.common.room.egg.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.common.room.egg.adapter.PoundEggRankListAdapter;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by MadisonRong on 13/01/2018.
 */

public class PoundEggRankListDialog extends BaseDialogFragment implements View.OnClickListener, BaseQuickAdapter.OnItemClickListener {

    public static final String TYPE_ONLINE_USER = "ONLINE_USER";
    public static final String TYPE_CONTRIBUTION = "ROOM_CONTRIBUTION";
    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_TYPE = "KEY_TYPE";
    @BindView(R2.id.bu_pay_tab)
    ImageView buTodayTab;
    @BindView(R2.id.bu_imcome_tab)
    ImageView buYesterdayTab;
    @BindView(R2.id.iv_close_dialog)
    ImageView ivCloseDialog;
    @BindView(R2.id.rv_pay_income_list)
    RecyclerView rvPayIncomeList;
    Unbinder unbinder;


    private int type = 1;
    private long roomId;
    private PoundEggRankListAdapter rankListAdapter;
    private View noDataView;
    private SelectOptionAction selectOptionAction;

    public PoundEggRankListDialog() {
    }

    public static PoundEggRankListDialog newContributionListInstance(Context context) {
        return newInstance(context.getString(R.string.pound_egg_record), TYPE_CONTRIBUTION);
    }

    public static PoundEggRankListDialog newInstance(String title, String type) {
        PoundEggRankListDialog listDataDialog = new PoundEggRankListDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE, title);
        bundle.putString(KEY_TYPE, type);
        listDataDialog.setArguments(bundle);
        return listDataDialog;
    }

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, null);
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_pound_egg_list_data, window.findViewById(android.R.id.content), false);
        unbinder = ButterKnife.bind(this, view);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null)
            roomId = roomInfo.getUid();
        view.findViewById(R.id.iv_close_dialog).setOnClickListener(this);
        noDataView = view.findViewById(R.id.tv_no_data);
        unbinder = ButterKnife.bind(this, view);
        initView();
        initRv();
        return view;
    }

    private void initRv() {
        rvPayIncomeList.setLayoutManager(new LinearLayoutManager(getContext()));
        rankListAdapter = new PoundEggRankListAdapter(getContext());
        rankListAdapter.setOnItemClickListener(this);
        rvPayIncomeList.setAdapter(rankListAdapter);
        getData();

    }

    private void initView() {
        buTodayTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeChange(1);
            }
        });
        buYesterdayTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeChange(2);
            }
        });
    }

    private void typeChange(int i) {
        if (i == type) {
            return;
        }
        type = i;
        buTodayTab.setImageResource(i == 1 ? R.drawable.ic_egg_today_select : R.drawable.ic_egg_today);
        buYesterdayTab.setImageResource(i == 2 ? R.drawable.ic_egg_yesterday_select : R.drawable.ic_egg_yesterday);
        getData();
    }

    private void getData() {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("roomId", roomId + "");
        requestParam.put("type", type + "");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getPoundEggRank(), requestParam, new OkHttpManager.MyCallBack<ServiceResult<List<UserInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (selectOptionAction != null)
                    selectOptionAction.onDataResponse();
            }

            @Override
            public void onResponse(ServiceResult<List<UserInfo>> response) {
                if (selectOptionAction != null)
                    selectOptionAction.onDataResponse();
                if (response.isSuccess()) {
                    if (response.getData() != null) {
                        noDataView.setVisibility(response.getData().size() > 0 ? View.GONE : View.VISIBLE);
                        rankListAdapter.setNewData(response.getData());
                    }

                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.iv_close_dialog) {
            dismiss();

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        List<UserInfo> list = rankListAdapter.getData();
        if (ListUtils.isListEmpty(list)) {
            return;
        }
        UserInfo userInfo = list.get(i);
        UserInfoDialogManager.showDialogFragment(getContext(), userInfo.getUid());
    }

    public void setSelectOptionAction(SelectOptionAction selectOptionAction) {
        this.selectOptionAction = selectOptionAction;
    }

    public interface SelectOptionAction {
        void optionClick();

        void onDataResponse();
    }

}
