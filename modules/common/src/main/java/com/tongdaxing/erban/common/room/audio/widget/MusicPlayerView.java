package com.tongdaxing.erban.common.room.audio.widget;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tongdaxing.erban.common.room.audio.activity.MusicActivity;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.music.IMusicCoreClient;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.List;

/**
 * 音乐播放入口
 * Created by chenran on 2017/10/28.
 */

public class MusicPlayerView extends FrameLayout implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private RelativeLayout musicBoxLayout;
    private ImageView packUp;
    private ImageView musicListMore;
    private ImageView ivSwitchPlayMode;
    private ImageView musicPlayPause;
    private ImageView nextBtn;
    private SeekBar volumeSeekBar;
    private TextView musicName;
    private String imageBg;
    private View rootView;
    private int[] icons = {R.drawable.ic_fj_loop, R.drawable.ic_fj_single_cycle, R.drawable.ic_fj_random};
    private String[] strs = {"列表循环", "单曲循环", "随机播放"};

    public MusicPlayerView(Context context) {
        super(context);
        init();
    }

    public MusicPlayerView(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    public MusicPlayerView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init();
    }

    private void init() {
        CoreManager.addClient(this);
        CoreUtils.register(this);
        LayoutInflater.from(getContext()).inflate(R.layout.layout_music_player_view, this, true);
        rootView = findViewById(R.id.fl_root);
        rootView.setOnClickListener(this);
        rootView.setClickable(false);
        ivSwitchPlayMode = (ImageView) findViewById(R.id.iv_switch_play_mode);
        ivSwitchPlayMode.setOnClickListener(this);

        packUp = (ImageView) findViewById(R.id.pack_up);
        packUp.setOnClickListener(this);
        musicBoxLayout = (RelativeLayout) findViewById(R.id.music_box_layout);
        musicBoxLayout.setOnClickListener(this);
        musicListMore = (ImageView) findViewById(R.id.music_list_more);
        musicListMore.setOnClickListener(this);

        musicPlayPause = (ImageView) findViewById(R.id.music_play_pause);
        musicPlayPause.setOnClickListener(this);
        volumeSeekBar = (SeekBar) findViewById(R.id.voice_seek);
        volumeSeekBar.setMax(100);
        volumeSeekBar.setProgress(CoreManager.getCore(IPlayerCore.class).getCurrentVolume());
        volumeSeekBar.setOnSeekBarChangeListener(this);
        musicName = (TextView) findViewById(R.id.music_name);
        nextBtn = (ImageView) findViewById(R.id.music_play_next);
        nextBtn.setOnClickListener(this);
        updateView();
        initPlayMode();
    }

    public void setImageBg(String imageBg) {
        this.imageBg = imageBg;
    }


    public void updateVoiceValue() {
        volumeSeekBar.setProgress(CoreManager.getCore(IPlayerCore.class).getCurrentVolume());
    }

    public void release() {
        CoreManager.removeClient(this);
        CoreUtils.unregister(this);
    }

    private void initPlayMode() {
        CoreManager.getCore(IPlayerCore.class).setPlayMode(AvRoomDataManager.currPlayMode % icons.length);
        ivSwitchPlayMode.setImageResource(icons[AvRoomDataManager.currPlayMode % icons.length]);
    }

    private void switchPlayMode() {
        if (AvRoomDataManager.currPlayMode % icons.length == 0) {//经过一边循环之后重置。
            AvRoomDataManager.currPlayMode = icons.length;
        }
        AvRoomDataManager.currPlayMode++;
        CoreManager.getCore(IPlayerCore.class).setPlayMode(AvRoomDataManager.currPlayMode % icons.length);
        ivSwitchPlayMode.setImageResource(icons[AvRoomDataManager.currPlayMode % icons.length]);
        SingleToastUtil.showToast(strs[AvRoomDataManager.currPlayMode % icons.length]);
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMusicPlay(RoomProfileAction.OnMusicClicked musicClicked) {
//        musicBoxLayout.setVisibility(VISIBLE);
//        rootView.setClickable(true);
//    }

    @Override
    public void onClick(View v) {
        try {
            int i = v.getId();
            if (i == R.id.iv_switch_play_mode) {
                switchPlayMode();

            } else if (i == R.id.fl_root) {
                rootView.setClickable(false);
                musicBoxLayout.setVisibility(GONE);

            } else if (i == R.id.music_list_more) {//                AddMusicListActivity.start(getContext(), imageBg);
                MusicActivity.Companion.start((Activity) getContext());

            } else if (i == R.id.music_play_pause) {// : 2018/3/29
                List<MusicLocalInfo> localMusicInfoList = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
                if (localMusicInfoList != null && localMusicInfoList.size() > 0) {
                    int state = CoreManager.getCore(IPlayerCore.class).getState();
                    if (state == IPlayerCore.STATE_PLAY) {
                        CoreManager.getCore(IPlayerCore.class).pause();
                    } else if (state == IPlayerCore.STATE_PAUSE) {
                        CoreManager.getCore(IPlayerCore.class).play((MusicLocalInfo) null);
                    } else {
                        int result = CoreManager.getCore(IPlayerCore.class).switchPlayNext();
                        if (result < 0) {
                            if (result == -3) {
                                SingleToastUtil.showToast("播放列表中还没有歌曲哦！");
                            } else {
                                SingleToastUtil.showToast("播放失败，文件异常");
                            }
                        }
                    }
                } else {
                    MusicActivity.Companion.start((Activity) getContext());
                }

            } else if (i == R.id.music_play_next) {
                List<MusicLocalInfo> localMusicInfoList1 = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
                if (localMusicInfoList1 != null && localMusicInfoList1.size() > 0) {
                    int result = CoreManager.getCore(IPlayerCore.class).switchPlayNext();
                    if (result < 0) {
                        if (result == -3) {
                            SingleToastUtil.showToast("播放列表中还没有歌曲哦！");
                        } else {
                            SingleToastUtil.showToast("播放失败，文件异常");
                        }
                    }
                } else {
                    MusicActivity.Companion.start((Activity) getContext());
                }

            } else {
            }
        } catch (Exception e) {
            SingleToastUtil.showToast(getContext(), "播放失败，请刷新再试", Toast.LENGTH_LONG);
        }
    }

    private void updateView() {
        try {
            MusicLocalInfo current = CoreManager.getCore(IPlayerCore.class).getCurrent();
            if (current != null) {
                if (!TextUtils.isEmpty(current.getSongName())) {
                    musicName.setText(current.getSongName());
                } else {
                    musicName.setText("暂无歌曲播放");
                }
                int state = CoreManager.getCore(IPlayerCore.class).getState();
                if (state == IPlayerCore.STATE_PLAY) {
                    musicPlayPause.setImageResource(R.drawable.icon_music_play);
                } else {
                    musicPlayPause.setImageResource(R.drawable.icon_music_pause_small);
                }
            } else {
                musicName.setText("暂无歌曲播放");
                musicPlayPause.setImageResource(R.drawable.icon_music_pause_small);
            }
        } catch (Exception e) {
            musicName.setText("暂无歌曲播放");
            musicPlayPause.setImageResource(R.drawable.icon_music_pause_small);
        }
    }

    @CoreEvent(coreClientClass = IMusicCoreClient.class)
    public void delOperationRefresh(long id, boolean isHidePlayer) {
        if (isHidePlayer) {
            musicName.setText("暂无歌曲播放");
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onRefreshPlayMode(int playMode) {
        ivSwitchPlayMode.setImageResource(icons[playMode]);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(MusicLocalInfo localMusicInfo) {
        updateView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(MusicLocalInfo localMusicInfo) {
        updateView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop(MusicLocalInfo localMusicInfo) {
        updateView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onCurrentMusicUpdate(MusicLocalInfo localMusicInfo) {
        updateView();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        CoreManager.getCore(IPlayerCore.class).seekVolume(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
