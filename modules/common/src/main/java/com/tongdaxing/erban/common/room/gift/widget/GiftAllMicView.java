package com.tongdaxing.erban.common.room.gift.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;

@Deprecated
public class GiftAllMicView extends LinearLayout {
    private View view;
    private TextView tvAll;
    private CheckBox cbSelect;
    private boolean isSelect = false;
    private OnAllMicSelectListener onAllMicSelectListener;

    public GiftAllMicView(Context context) {
        super(context);
        view = LayoutInflater.from(context).inflate(R.layout.layout_gift_all_mic, this);
        tvAll = view.findViewById(R.id.tv_gift_all);
        cbSelect = view.findViewById(R.id.cb_gift_all);
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbSelect.isChecked()) {
                    setCheck(false);
                } else {
                    setCheck(true);
                }
                if (onAllMicSelectListener != null)
                    onAllMicSelectListener.onCheck(isSelect);
            }
        });

    }

    public void setCheck(boolean state) {
        if (cbSelect != null)
            cbSelect.setChecked(state);
        if (tvAll != null)
            tvAll.setSelected(state);
        isSelect = state;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setOnAllMicSelectListener(OnAllMicSelectListener onAllMicSelectListener) {
        this.onAllMicSelectListener = onAllMicSelectListener;
    }

    public interface OnAllMicSelectListener {
        void onCheck(boolean isAllMic);
    }
}
