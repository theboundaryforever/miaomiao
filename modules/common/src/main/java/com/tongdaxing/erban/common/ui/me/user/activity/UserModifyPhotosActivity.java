package com.tongdaxing.erban.common.ui.me.user.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.TResult;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.common.permission.PermissionActivity;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.file.JXFileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.bingoogolapple.photopicker.widget.BGASortableNinePhotoLayout;
import io.realm.RealmList;

/**
 * 用户相册
 * Created by chenran on 2017/7/24.
 */

public class UserModifyPhotosActivity extends TakePhotoActivity implements BGASortableNinePhotoLayout.Delegate {
    private static final int RC_CHOOSE_PHOTO = 1;
    private static final int RC_PHOTO_PREVIEW = 2;
    private final int MAX_PICTURE = 50;//最大照片数量
    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
        @Override
        public void superPermission() {
            takePhoto();
        }
    };
    private long userId;
    private UserInfo userInfo;
    private UserModifyPhotosActivity mActivity;
    private boolean isSelf = true;
    private BGASortableNinePhotoLayout bga;
    private TextView tvEdit;
    private TextView tvShowHint;
    private RealmList<UserPhoto> realmList;
    private int delPosition = -1;
    private ArrayList<UserPhoto> models;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_photos_modify);
        mActivity = this;
        userId = getIntent().getLongExtra("userId", 0);
        isSelf = getIntent().getBooleanExtra("isSelf", true);
        initView();
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId);
        if (userInfo != null) {
            updateView();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == userId) {
            userInfo = info;
            updateView();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        if (info.getUid() == userId) {
            userInfo = info;
            updateView();
            getDialogManager().dismissDialog();
        }
    }

    private void initView() {
        initTitleBar("相册");
        tvEdit = (TextView) findViewById(R.id.tv_edit);
        tvShowHint = (TextView) findViewById(R.id.tv_show_hint);
        bga = (BGASortableNinePhotoLayout) findViewById(R.id.snpl_moment_add_photos);
        bga.setDelegate(this);
        if (isSelf) {
            tvEdit.setVisibility(View.VISIBLE);
            tvEdit.setOnClickListener(v -> {
                ((TextView) v).setText(bga.isEditable() ? "编辑" : "完成");
                tvShowHint.setVisibility(bga.isEditable() ? View.GONE : View.VISIBLE);
                if (bga.isEditable()) {
                    uploadPictureSort();
                }
                bga.setEditable(!bga.isEditable());
            });
        } else {
            bga.setAlwaysShowPlus(false);
            tvEdit.setVisibility(View.GONE);
        }
    }

    private void uploadPictureSort() {
        getDialogManager().showProgressDialog(this, "请稍后...");
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        String str = getPictureSortPid();
        if (TextUtils.isEmpty(str)) {
            getDialogManager().dismissDialog();
            return;
        }
        params.put("pids", str);
        OkHttpManager.getInstance().doPostRequest(UriProvider.modifyAlbumPosition(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                getDialogManager().dismissDialog();
            }

            @Override
            public void onResponse(Json response) {
                getDialogManager().dismissDialog();
            }
        });
    }

    private void updateView() {
        if (userInfo == null) {
            return;
        }
        realmList = userInfo.getPrivatePhoto();
        if (realmList == null) {
            return;
        }
        ArrayList<UserPhoto> strings = new ArrayList<>(realmList);
        bga.setData(strings, true);
    }

    /**
     * 获取图片排序Pid字符串
     *
     * @return
     */
    private String getPictureSortPid() {
        StringBuilder stringBuilder = new StringBuilder();
        if (models != null && realmList != null) {
            for (int i = 0; i < models.size(); i++) {
                for (int j = 0; j < realmList.size(); j++) {
                    if (models.get(i).getPid() == realmList.get(j).getPid()) {
                        stringBuilder.append(models.get(i).getPid());
                        stringBuilder.append(",");
                    }
                }
            }
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }
        return stringBuilder.toString();
    }

    private void takePhoto() {
        File cameraOutFile = JXFileUtils.getTempFile(this, "picture_" + System.currentTimeMillis() + ".jpg");
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        CompressConfig compressConfig = new CompressConfig.Builder().create();
        compressConfig.setMaxSize(500 * 1024);
        getTakePhoto().onEnableCompress(compressConfig, false);
        getTakePhoto().onPickFromCapture(uri);
    }

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, android.Manifest.permission.CAMERA);
    }

    private void onPhotoDeleteClick(int position) {
        if (position >= 0) {
            getDialogManager().showProgressDialog(this, "请稍后");
            UserPhoto userPhoto = userInfo.getPrivatePhoto().get(position);
            CoreManager.getCore(IUserCore.class).requestDeletePhoto(userPhoto.getPid());
        } else {
            toast("数据异常");
        }
    }

    private void uploadPicture(int position) {
        if (isSelf) {
            if (userInfo.getPrivatePhoto() != null && userInfo.getPrivatePhoto().size() >= 50) {
                toast("照片已达到最大上传数");
                return;
            }
            ButtonItem upItem = new ButtonItem("拍照上传", this::checkPermissionAndStartCamera);
            ButtonItem loaclItem = new ButtonItem("本地相册", () -> {
                CompressConfig compressConfig = new CompressConfig.Builder().create();
                compressConfig.setMaxSize(500 * 1024);
                getTakePhoto().onEnableCompress(compressConfig, true);
                getTakePhoto().onPickFromGallery();
            });
            List<ButtonItem> buttonItemList = new ArrayList<>();
            buttonItemList.add(upItem);
            buttonItemList.add(loaclItem);
            getDialogManager().showCommonPopupDialog(buttonItemList, "取消", false);
        } else {
            ArrayList<UserPhoto> userPhotos1 = new ArrayList<>();
            userPhotos1.addAll(userInfo.getPrivatePhoto());
            Intent intent = new Intent(mActivity, ShowPhotoActivity.class);
            int position1 = isSelf ? position - 1 : position;
            intent.putExtra("position", position1);
            intent.putExtra("photoList", userPhotos1);
            startActivity(intent);
        }
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadPhoto(String url) {
        CoreManager.getCore(IUserCore.class).requestAddPhoto(url);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadPhotoFail() {
        toast("操作失败，请检查网络");
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestAddPhoto() {
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestAddPhotoFaith(String msg) {
        toast("操作失败，请检查网络");
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestDeletePhoto() {
        if (delPosition != -1) {
            bga.removeItem(delPosition);
        }
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestDeletePhotoFaith(String msg) {
        toast("上传失败");
        getDialogManager().dismissDialog();
    }

    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(this, "请稍后");
        //有空指针异常
        if (result == null || result.getImage() == null || StringUtil.isEmpty(result.getImage().getCompressPath())) {
            toast("图片地址异常，请重试！");
            getDialogManager().dismissDialog();
            return;
        }
        CoreManager.getCore(IFileCore.class).uploadPhoto(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
    }

    @Override
    public void onClickAddNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, ArrayList<UserPhoto> models) {
        uploadPicture(position);
    }

    @Override
    public void onClickDeleteNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, UserPhoto model, ArrayList<UserPhoto> models) {
        onPhotoDeleteClick(position);
        delPosition = position;
    }

    /**
     * 图片点击事件
     *
     * @param sortableNinePhotoLayout
     * @param view
     * @param position
     * @param model
     * @param models
     */
    @Override
    public void onClickNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, UserPhoto model, ArrayList<UserPhoto> models) {
        ArrayList<String> tempList = new ArrayList<>();
        for (int i = 0; i < models.size(); i++) {
            tempList.add(models.get(i).getPhotoUrl());
        }
        Json photoDataJson = getPhotoDataJson(tempList);
        if (photoDataJson == null)
            return;
        Intent intent = new Intent(mActivity, ShowPhotoActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("photoJsonData", photoDataJson.toString());
        startActivity(intent);
    }

    private Json getPhotoDataJson(ArrayList<String> models) {
        if (models == null)
            return null;
        Json json = new Json();
        for (int i = 0; i < models.size(); i++) {
            Json j = new Json();
            long temp = getPid(models.get(i));
            if (temp == 0) {
                continue;
            }
            j.set("pid", temp);
            j.set("photoUrl", models.get(i));
            json.set(i + "", j.toString());
        }
        return json;
    }

    private long getPid(String url) {
        if (!ListUtils.isListEmpty(realmList)) {
            for (int i = 0; i < realmList.size(); i++) {
                if (url.equals(realmList.get(i).getPhotoUrl())) {
                    return realmList.get(i).getPid();
                }
            }
        }
        return 0;
    }

    @Override
    public void onNinePhotoItemExchanged(BGASortableNinePhotoLayout sortableNinePhotoLayout, int fromPosition, int toPosition, ArrayList<UserPhoto> models) {
        this.models = models;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK && requestCode == RC_CHOOSE_PHOTO && data != null) {
//            bga.addMoreData(BGAPhotoPickerActivity.getSelectedPhotos(data));
//        } else if (requestCode == RC_PHOTO_PREVIEW && data != null) {
//            bga.setData(BGAPhotoPickerPreviewActivity.getSelectedPhotos(data));
//        }
    }
}
