package com.tongdaxing.erban.common.ui.me.user.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.me.user.adapter.DatumPageAdapter;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.find.bean.DatumPageInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomStatus;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Function:
 * Author: Edward on 2019/3/27
 */
public class DatumPageFragment extends BaseFragment {
    List<DatumPageInfo> list;
    private RecyclerView recyclerView;
    private DatumPageAdapter datumPageAdapter;
    private UserInfo userInfo;
    private long userId;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_datum_page;
    }

    @Override
    public void onFindViews() {
        recyclerView = mView.findViewById(R.id.recycler_view);

        datumPageAdapter = new DatumPageAdapter(null);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(datumPageAdapter);
        if (userId == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
            recyclerView.setPadding(0, 0, 0, 0);
        }
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {
        Bundle bundle = getArguments();
        if (bundle == null) {
            showNoData();
            return;
        }
        userId = bundle.getLong("userId", 0);
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId, false);
        if (userInfo == null) {
            showNoData();
            return;
        }
    }

    public void refreshData(UserInfo userInfo) {
        this.userInfo = userInfo;
        list = new ArrayList<>();
        setPersonalData();
        setAlbumData();
        if (datumPageAdapter != null) {
            datumPageAdapter.setNewData(list);
        }
        loadRoomStatus();
    }

    /**
     * 资料头部只有声鉴卡
     */
    private void setAudioIdentifyCard() {
        if (userInfo != null && userInfo.getVoiceCard() != null &&
                userInfo.getVoiceDura() > 0 && !TextUtils.isEmpty(userInfo.getUserVoice())) {//没有数据则隐藏UI
            DatumPageInfo datumPageInfo = new DatumPageInfo();
            datumPageInfo.setUserInfo(userInfo);
            datumPageInfo.setItemType(DatumPageAdapter.DATUM_PAGE_IDENTIFY_CARD);
            boolean flag = checkData(DatumPageAdapter.DATUM_PAGE_IDENTIFY_CARD, datumPageInfo);
            if (!flag) {
                list.add(0, datumPageInfo);//添加到头部
            }
            datumPageAdapter.notifyDataSetChanged();
        }
    }

    private void setPersonalData() {
        DatumPageInfo datumPageInfo = new DatumPageInfo();
        datumPageInfo.setUserInfo(userInfo);
        datumPageInfo.setItemType(DatumPageAdapter.DATUM_PAGE_PERSONAL_INFO);
        list.add(datumPageInfo);
    }

    private void setAlbumData() {
        DatumPageInfo datumPageInfo = new DatumPageInfo();
        datumPageInfo.setUserInfo(userInfo);
        datumPageInfo.setItemType(DatumPageAdapter.DATUM_PAGE_ALBUM);
        list.add(datumPageInfo);
    }

    private void setRoomAndAudioIdentifyCard(RoomStatus roomStatus) {
//        if (!TextUtils.isEmpty(roomStatus.getRoomPwd())) {
//            return;//有密码不展示房间
//        }
        DatumPageInfo datumPageInfo = new DatumPageInfo();
        datumPageInfo.setRoomStatus(roomStatus);
        datumPageInfo.setUserInfo(userInfo);
        boolean flag;
        if (datumPageAdapter != null && !ListUtils.isListEmpty(list)) {
            if (userInfo != null && userInfo.getVoiceCard() != null &&
                    userInfo.getVoiceDura() > 0 && !TextUtils.isEmpty(userInfo.getUserVoice())) {//没有数据则隐藏UI
//                //资料头部有房间数据和声鉴卡
                datumPageInfo.setItemType(DatumPageAdapter.DATUM_PAGE_ENTER_ROOM_AND_IDENTIFY_CARD);
                flag = checkData(DatumPageAdapter.DATUM_PAGE_ENTER_ROOM_AND_IDENTIFY_CARD, datumPageInfo);
            } else {
                //只有房间
                datumPageInfo.setItemType(DatumPageAdapter.DATUM_PAGE_ENTER_ROOM);
                flag = checkData(DatumPageAdapter.DATUM_PAGE_ENTER_ROOM, datumPageInfo);
            }
            if (!flag) {
                list.add(0, datumPageInfo);//添加到头部
            }
            datumPageAdapter.notifyDataSetChanged();
        }
    }

    private boolean checkData(int itemType, DatumPageInfo datumPageInfo) {//检查数据
        if (!ListUtils.isListEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getItemType() == itemType) {
                    list.set(i, datumPageInfo);//刷新数据
                    return true;
                }
            }
        }
        return false;
    }

    public void loadRoomStatus() {
        if (userInfo == null) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("queryUid", String.valueOf(userInfo.getUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getTheRoomStatus(), params, new OkHttpManager.MyCallBack<ServiceResult<RoomStatus>>() {
            @Override
            public void onError(Exception e) {
                setAudioIdentifyCard();
            }

            @Override
            public void onResponse(ServiceResult<RoomStatus> response) {
                if (response == null) {
                    setAudioIdentifyCard();
                    return;
                }
                if (response.isSuccess() && response.getData() != null && response.getData().getUid() > 0) {
                    setRoomAndAudioIdentifyCard(response.getData());
                } else {
                    setAudioIdentifyCard();
                }
            }
        });
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGotoUserRoom(long userId) {
        getDialogManager().showProgressDialog(mContext, "请稍后...");
        CoreManager.getCore(IRoomCore.class).getUserRoom(userId);
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetUserRoom(RoomInfo roomInfo) {
        long formalRoomId = BasicConfig.isDebug ? PublicChatRoomManager.devRoomId : PublicChatRoomManager.formalRoomId;
        if (roomInfo != null && roomInfo.getRoomId() == formalRoomId) {
            toast("对方不在房间内");
            return;
        }
        getDialogManager().dismissDialog();
        RoomInfo current = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null && roomInfo.getUid() > 0) {
            if (current != null) {
                if (current.getUid() == roomInfo.getUid()) {
                    toast("已经和对方在同一个房间");
                    return;
                }
            }
            if (userInfo != null) {
                AVRoomActivity.start(getActivity(), roomInfo.getUid(), new HerUserInfo(userInfo.getUid(), userInfo.getNick()));
            } else {
                AVRoomActivity.start(getActivity(), roomInfo.getUid());
            }
        } else {
            toast("对方不在房间内");
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetUserRoomFail(String msg) {
        getDialogManager().dismissDialog();
        toast(msg);
    }


}
