package com.tongdaxing.erban.common;

import com.tcloud.core.thread.ExecutorCenter;
import com.tcloud.core.thread.WorkRunnable;
import com.tongdaxing.erban.room.api.IRoomModulesService;
import com.tongdaxing.xchat_framework.coremanager.CoreFactory;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chen on 2019/7/12.
 */
public class ModulesInit {

    private final static String TAG = "ModelInit";

    private static ModulesInit mInstance;

    private ModulesInit() {
    }

    public static ModulesInit getInstance() {
        if (mInstance == null) {
            synchronized (ModulesInit.class) {
                if (mInstance == null) {
                    mInstance = new ModulesInit();
                }
            }
        }
        return mInstance;
    }

    // 模块注册
    private Map<Class<? extends IBaseCore>, String> getServicesMap() {
        Map<Class<? extends IBaseCore>, String> map = new HashMap<>();
        map.put(IRoomModulesService.class, "com.tongdaxing.erban.room.RoomModulesService");
        return map;
    }

    public void init() {
        onMainInit();

        ExecutorCenter.getInstance().post(new WorkRunnable() {
            @android.support.annotation.NonNull
            @Override
            public String getTag() {
                return TAG;
            }

            @Override
            public void run() {
                onDelayInit();
            }
        });
    }

    private void onMainInit() {

    }

    private void onDelayInit() {
        registerModules();
    }

    private void registerModules() {
        Map<Class<? extends IBaseCore>, String> servicesMap = getServicesMap();
        if (servicesMap != null) {
            for (Map.Entry<Class<? extends IBaseCore>, String> entry : servicesMap.entrySet()) {
                try {
                    Class<? extends IBaseCore> mapKey = entry.getKey();

                    String mapValue = entry.getValue();
                    boolean hasRegistered = CoreFactory.hasRegisteredCoreClass(mapKey);
                    MLog.info(TAG, "registerModel hasRegistered: %b", hasRegistered);
                    if (!hasRegistered) {
                        CoreFactory.registerCoreClass(mapKey, mapValue);
                    }
                } catch (Exception e) {
                    MLog.error(TAG, "registerCoreClass exception: ", e);
                }

            }
        }
    }


}
