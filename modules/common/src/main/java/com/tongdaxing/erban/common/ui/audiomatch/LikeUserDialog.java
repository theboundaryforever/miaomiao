package com.tongdaxing.erban.common.ui.audiomatch;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.GridItemDecoration;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SessionHelper;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.AudioMatchRandomInfo;
import com.tongdaxing.xchat_core.bean.FocusMsgSwitchInfo;
import com.tongdaxing.xchat_core.bean.LikeUserGiftInfo;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCoreClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.utils.StarUtils;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class LikeUserDialog extends DialogFragment implements ChargeListener {
    private static final String CURR_INFO = "curr_info", ANIM_POINT = "anim_point";

    private final static int DEFAULT_GIFT_ID = -1;
    private static final String TAG = "LikeUserDialog";

    private LikeGiftAdapter giftAdapter;

    private AudioMatchRandomInfo currInfo;

    private View view;
    private View contentView, animBg;
    private Point animPoint;
    private DialogManager mDialogManager;
    private DialogInterface.OnDismissListener onDismissListener;
    private boolean isShowingChargeDialog;

//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        Window window = getDialog().getWindow();
//        window.requestFeature(Window.FEATURE_NO_TITLE);
//        super.onActivityCreated(savedInstanceState);
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//    }

    public static LikeUserDialog newInstance(AudioMatchRandomInfo currInfo, Point animPoint) {
        if (currInfo == null) {
            throw new IllegalArgumentException("currInfo can not null");
        }
        LikeUserDialog dialog = new LikeUserDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CURR_INFO, currInfo);
        bundle.putParcelable(ANIM_POINT, animPoint);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        CoreManager.addClient(getActivity());
        setStyle(DialogFragment.STYLE_NORMAL, R.style.fullDialog);
        if (savedInstanceState != null) {
            currInfo = savedInstanceState.getParcelable(CURR_INFO);
            animPoint = savedInstanceState.getParcelable(ANIM_POINT);
        } else {
            if (getArguments() != null) {
                currInfo = getArguments().getParcelable(CURR_INFO);
                animPoint = getArguments().getParcelable(ANIM_POINT);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.activity_like_user, window.findViewById(android.R.id.content), false);
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                v.removeOnLayoutChangeListener(this);
                startEnterAnim();
            }
        });
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.view = view;
        ImageView ivBack = (ImageView) view.findViewById(R.id.iv_back);
        ImageView ivMore = (ImageView) view.findViewById(R.id.iv_more);
        TextView tvContinue = (TextView) view.findViewById(R.id.tv_continue);
        TextView btnNext = (TextView) view.findViewById(R.id.btn_next);
        RecyclerView giftRv = (RecyclerView) view.findViewById(R.id.rv_gift);
        contentView = view.findViewById(R.id.content);
        animBg = view.findViewById(R.id.anim_bg);

        getDialog().setOnKeyListener((dialog, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                leave();
                return true;
            }
            return false;
        });

        giftRv.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        giftRv.addItemDecoration(new GridItemDecoration(3, DisplayUtils.dip2px(getActivity(), 10), DisplayUtils.dip2px(getActivity(), 10)));
        giftAdapter = new LikeGiftAdapter(getActivity());
        giftRv.setAdapter(giftAdapter);
        giftAdapter.setOnItemClickListener((adapter, v, position) -> {
            SpUtils.put(BaseApp.mBaseContext, SpEvent.audioMatchLikeSelectedGiftId, Objects.requireNonNull(giftAdapter.getItem(position)).getGiftId());
            giftAdapter.setGift(position);
        });

        setupGift(null);

        tvContinue.setOnClickListener(v -> leave());
        ivBack.setOnClickListener(v -> leave());
        btnNext.setOnClickListener(v -> {
            LikeUserGiftInfo giftInfo = giftAdapter.getSelectedGift();
            if (giftInfo.getGiftId() == -1) {
                UmengEventUtil.getInstance().onMatchLikeClick(getContext(), "0金币");
                toChat();
            } else {
                if (currInfo == null) {
                    return;
                }
                UmengEventUtil.getInstance().onMatchLikeClick(getContext(), String.valueOf(giftInfo.getGoldPrice()).concat("金币"));
                List<Long> uids = new ArrayList<>();
                uids.add((long) currInfo.getUid());
                CoreManager.getCore(IGiftCore.class).doSendPersonalGiftToNIMNew(giftInfo.getGiftId(), uids,
                        1, null, this, 2);
            }
        });
        ivMore.setOnClickListener(v -> createUserMoreOperate());

        getGift();

        setupUserInfo();
    }

    private void startEnterAnim() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (animPoint == null) {
                return;
            }
            int cicularR = (int) Math.round(Math.sqrt(animPoint.x * animPoint.x + animPoint.y * animPoint.y));
            Animator animator = ViewAnimationUtils.createCircularReveal(animBg, animPoint.x, animPoint.y, 0, cicularR);
            // Animator animator = ViewAnimationUtils.createCircularReveal(mainContainer, UIUtil.dip2px(this,40),  UIUtil.dip2px(this,70) , 0, cicular_R);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    animBg.setVisibility(View.VISIBLE);
                    contentView.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    contentView.setVisibility(View.VISIBLE);
                    AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0);
                    alphaAnimation.setDuration(300);
                    alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            animBg.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    animBg.startAnimation(alphaAnimation);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    animBg.setVisibility(View.GONE);
                    contentView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animator.setDuration(500);
            animator.setInterpolator(new AccelerateInterpolator());
            animator.start();
        } else {
            animBg.setVisibility(View.GONE);
            contentView.setVisibility(View.VISIBLE);
        }
    }

    private void leave() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (animPoint == null) {
                return;
            }
            int cicularR = (int) Math.round(Math.sqrt(animPoint.x * animPoint.x + animPoint.y * animPoint.y));
            Animator animator = ViewAnimationUtils.createCircularReveal(animBg, animPoint.x, animPoint.y, cicularR, 0);
            // Animator animator = ViewAnimationUtils.createCircularReveal(mainContainer, UIUtil.dip2px(this,40),  UIUtil.dip2px(this,70) , 0, cicular_R);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    contentView.setVisibility(View.GONE);
                    animBg.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    animBg.setVisibility(View.GONE);
                    dismiss();
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    dismiss();
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animator.setDuration(400);
            animator.setInterpolator(new AccelerateInterpolator());
            animator.start();
        } else {
            dismiss();
        }
    }

    private void setupUserInfo() {
        if (currInfo == null) {
            return;
        }
        ImageLoadUtils.loadCircleImage(getActivity(), currInfo.getAvatar(), (ImageView) view.findViewById(R.id.iv_user_head), R.drawable.ic_no_avatar);
        TextView ageGenderTv = (TextView) view.findViewById(R.id.age_gender);
        if (TimeUtils.getAge(currInfo.getBirth()) > 0) {
            ageGenderTv.setVisibility(View.VISIBLE);
            ageGenderTv.setBackgroundResource(currInfo.getGender() == 1 ? R.drawable.ic_audio_match_like_boy : R.drawable.ic_audio_match_like_girl);
            ageGenderTv.setText(String.valueOf(TimeUtils.getAge(currInfo.getBirth())));
        } else {
            ageGenderTv.setVisibility(View.GONE);
        }
        TextView starTv = ((TextView) view.findViewById(R.id.star));
        String star = StarUtils.getConstellation(new Date(currInfo.getBirth()));
        L.debug(TAG, "age = %d, star = %s", TimeUtils.getAge(currInfo.getBirth()), star);
        if (!TextUtils.isEmpty(star)) {
            starTv.setVisibility(View.VISIBLE);
            starTv.setText(star);
        } else {
            starTv.setVisibility(View.GONE);
        }
        ((TextView) view.findViewById(R.id.signature)).setText(currInfo.getUserDesc());
        ((TextView) view.findViewById(R.id.nickname)).setText(currInfo.getNick());
        view.findViewById(R.id.online).setVisibility(currInfo.getOperatorStatus() == 2 ? View.VISIBLE : View.GONE);
    }

    private void showDeFriendDialog() {
        if (CoreManager.getCore(IIMFriendCore.class).isUserInBlackList(currInfo.getUid() + "")) {
            getDialogManager().showOkCancelDialog("是否取消拉黑", true, new DialogManager.AbsOkDialogListener() {
                @Override
                public void onOk() {
                    CoreManager.getCore(IIMFriendCore.class).removeFromBlackList("" + currInfo.getUid());
                }
            });
        } else {
            getDialogManager().showOkCancelDialog("加入黑名单后，将不再收到对方信息", true, new DialogManager.AbsOkDialogListener() {
                @Override
                public void onOk() {
                    CoreManager.getCore(IIMFriendCore.class).addToBlackList("" + currInfo.getUid());
                }
            });
        }
    }

    public DialogManager getDialogManager() {
        if (mDialogManager == null) {
            mDialogManager = new DialogManager(getActivity());
            mDialogManager.setCanceledOnClickOutside(false);
        }
        return mDialogManager;
    }

    /**
     * 生成用户功能按钮
     */
    private void createUserMoreOperate() {
        if (currInfo == null) {
            return;
        }
        List<ButtonItem> buttonItems = new ArrayList<>();
        ButtonItem msgBlackListItem = ButtonItemFactory.createMsgBlackListItem(!CoreManager.getCore(IIMFriendCore.class).isUserInBlackList(currInfo.getUid() + "") ? "拉黑" : "取消拉黑", () -> showDeFriendDialog());
        buttonItems.add(ButtonItemFactory.createReportItem(getActivity(), "举报", 1));
        buttonItems.add(msgBlackListItem);
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void removeBlackListSuccess() {
        SingleToastUtil.showToast("已取消拉黑");
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void addBlackListSuccess() {
        SingleToastUtil.showToast("已拉黑");
    }

    private void getGift() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getListGift(), params, new OkHttpManager.MyCallBack<ServiceResult<List<LikeUserGiftInfo>>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<List<LikeUserGiftInfo>> response) {
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getMessage());
                } else {
                    setupGift(response.getData());
                }
            }
        });
    }

    private void setupGift(List<LikeUserGiftInfo> giftList) {
        LikeUserGiftInfo noData = new LikeUserGiftInfo();
        noData.setGiftId(DEFAULT_GIFT_ID);
        if (giftList == null) {
            giftList = new ArrayList<>();
        }
        giftList.add(noData);

        int giftId = (int) SpUtils.get(getContext(), SpEvent.audioMatchLikeSelectedGiftId, DEFAULT_GIFT_ID);
        int selectPosition = giftList.size() - 1;//默认选中最后一个
        if (giftId != DEFAULT_GIFT_ID) {
            for (int i = 0; i < giftList.size(); i++) {
                if (giftId == giftList.get(i).getGiftId()) {
                    selectPosition = i;
                    break;
                }
            }
        }
        giftAdapter.replaceData(giftList);
        giftAdapter.setGift(selectPosition);
    }

    private void toChat() {
        boolean isFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(currInfo.getUid() + "");
        if (isFriend) {
            SessionHelper.startPrivateChat(getActivity(), currInfo.getUid());
        } else {
            checkFocusMsgSwitch(new OkHttpManager.MyCallBack<Integer>() {
                @Override
                public void onError(Exception e) {
                    SingleToastUtil.showToast(e.getMessage());
                }

                @Override
                public void onResponse(Integer status) {
                    if (status == 1) {
                        SingleToastUtil.showToast("对方已开启免打扰模式");
                    } else {
                        SessionHelper.startPrivateChat(getActivity(), currInfo.getUid());
                    }
                }
            });
        }
    }

    public void checkFocusMsgSwitch(OkHttpManager.MyCallBack<Integer> callBack) {
        if (currInfo == null) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", currInfo.getUid() + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getFocusMsgSwitch(), null, params, new OkHttpManager.MyCallBack<ServiceResult<FocusMsgSwitchInfo>>() {
            @Override
            public void onError(Exception e) {
                callBack.onError(new Exception("网络不稳定，请稍后再试"));
            }

            @Override
            public void onResponse(ServiceResult<FocusMsgSwitchInfo> response) {
                int isFocusMsgSwitchCheckStatus = 0;//1免打扰
                if (response.isSuccess() && response.getData() != null) {
                    int chatPermission = response.getData().getChatPermission();
                    if (chatPermission == 1) {
                        isFocusMsgSwitchCheckStatus = 1;//免打扰
                    } else if (chatPermission == 2) {
                        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                        if (userInfo == null || userInfo.getExperLevel() < 10) {
                            isFocusMsgSwitchCheckStatus = 1;//对小于10级的用户免打扰
                        }
                    }
                    callBack.onResponse(isFocusMsgSwitchCheckStatus);
                } else {
                    callBack.onError(new Exception(response.getMessage()));
                }
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onNeedCharge() {
        if (isShowingChargeDialog) {
            return;
        }
        isShowingChargeDialog = true;
        ChargeDialogFragment.instance("余额不足，是否充值", (view, fragment) -> {
            if (view.getId() == R.id.btn_cancel) {
                fragment.dismiss();
            } else if (view.getId() == R.id.btn_ok) {
                MyWalletNewActivity.start(getActivity());
                fragment.dismiss();
            }
            isShowingChargeDialog = false;
        }).show(getFragmentManager(), "charge");

    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftPastDue() {
        SingleToastUtil.showToast(getContext(), R.string.gift_expire);
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendGiftFail(String message) {
        SingleToastUtil.showToast(message);
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftMysteryNotEnough() {
        SingleToastUtil.showToast("您的神秘礼物数量不够哦！");
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendGiftSuccess(int giftId, long targetUid, int giftNum) {
        toChat();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CoreManager.addClient(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        CoreManager.removeClient(this);
    }
}
