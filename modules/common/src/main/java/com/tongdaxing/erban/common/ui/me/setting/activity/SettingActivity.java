package com.tongdaxing.erban.common.ui.me.setting.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.constant.BaseUrl;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.setting.notificationSetting.NotificationSettingActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.BinderPhoneActivity;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.PreferencesUtils;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.music.IMusicCore;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.FileUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.umeng.commonsdk.UMConfigure.getTestDeviceInfo;

/**
 * Created by zhouxiangfeng on 2017/4/16.
 */
public class SettingActivity extends BaseActivity {

    @BindView(R2.id.title_bar)
    TitleBar titleBar;
    @BindView(R2.id.binder_right)
    ImageView binderRight;
    @BindView(R2.id.rly_binder)
    RelativeLayout rlyBinder;
    @BindView(R2.id.reset_pwd_right)
    ImageView resetPwdRight;
    @BindView(R2.id.rly_reset_pwd)
    RelativeLayout rlyResetPwd;
    @BindView(R2.id.rl_msg_not_disturb)
    RelativeLayout rlMsgNotDisturb;
    @BindView(R2.id.rl_blacklist)
    RelativeLayout rlBlacklist;
    @BindView(R2.id.setting_right1)
    ImageView settingRight1;
    @BindView(R2.id.rly_content)
    RelativeLayout rlyContent;
    @BindView(R2.id.rly_contact_us)
    RelativeLayout rlyContactUs;
    @BindView(R2.id.rly_help)
    RelativeLayout rlyHelp;
    @BindView(R2.id.setting_right)
    TextView settingRight;
    @BindView(R2.id.rly_update)
    FrameLayout rlyUpdate;
    @BindView(R2.id.tv_scan)
    TextView tvScan;
    @BindView(R2.id.tv_clear_cache)
    TextView tvClearCache;
    @BindView(R2.id.tv_cache_count)
    TextView tvCacheCount;
    @BindView(R2.id.rly_user_protocol)
    RelativeLayout rlyUserProtocol;
    @BindView(R2.id.lab_right)
    ImageView labRight;
    @BindView(R2.id.rly_lab)
    RelativeLayout rlyLab;
    @BindView(R2.id.et)
    EditText et;
    @BindView(R2.id.btn_login_out)
    TextView btnLoginOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        initTitleBar("设置");
        initView();
        initData();
        UserInfo info = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//        if (info != null && info.getExperLevel() >= 30)
//            rlMsgNotDisturb.setVisibility(View.VISIBLE);
    }

    private void initData() {
        if (BasicConfig.isDebug) {
            et.setVisibility(View.VISIBLE);
            String[] deviceInfo = getTestDeviceInfo(this);
            if (deviceInfo != null && deviceInfo.length >= 2) {
                et.setText("{\"device_id\":\"" + deviceInfo[0] + "\",\"mac\":\"" + deviceInfo[1] + "\"}");
            }
        }

        refreshMusicCache();
//        tv
    }

    private void refreshMusicCache() {
        long fileSize = CoreManager.getCore(IMusicCore.class).getMusicCacheSize();
        tvCacheCount.setText(FileUtil.getFileSizeMB(fileSize) + "M");
    }

    private void showMoreItems() {
        List<ButtonItem> buttonItems = new ArrayList<>();
        ButtonItem buttonItem1 = new ButtonItem("确定", () -> {
            CoreManager.getCore(IMusicCore.class).delMusicCache();
            refreshMusicCache();
        });
        buttonItems.add(buttonItem1);
        DialogManager dialogManager = getDialogManager();
        if (dialogManager != null) {
            dialogManager.showCommonPopupDialog(buttonItems, "取消");
        }
    }

    @OnClick({R2.id.tv_clear_cache, R2.id.rl_msg_not_disturb, R2.id.rly_binder, R2.id.rly_nitofy_setting, R2.id.rly_reset_pwd, R2.id.rly_contact_us,
            R2.id.rly_help, R2.id.rly_update, R2.id.rly_user_protocol, R2.id.rly_lab, R2.id.btn_login_out, R2.id.rl_blacklist})
    public void onClick(View view) {
        super.onClick(view);
        int i = view.getId();//绑定手机
//重置密码
//关于我们
//用户协议
//退出登录
//黑名单
        if (i == R.id.tv_clear_cache) {
            showMoreItems();

//            case R.id.tv_scan://扫一扫
//                QrCodeScanerActivity.start(this);
//                break;
//            case R.id.rly_content://反馈
//                startActivity(new Intent(getApplicationContext(), FeedbackActivity.class));
//                break;
        } else if (i == R.id.rl_msg_not_disturb) {
            MessageNotDisturbActivity.start(this);

        } else if (i == R.id.rly_binder) {
            getDialogManager().showProgressDialog(SettingActivity.this, "正在查询请稍后...");
            CoreManager.getCore(IAuthCore.class).isPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid());

        } else if (i == R.id.rly_nitofy_setting) {
            startActivity(new Intent(this, NotificationSettingActivity.class));
        } else if (i == R.id.rly_reset_pwd) {
            UIHelper.showForgetPswAct(SettingActivity.this);

        } else if (i == R.id.rly_contact_us) {
            UIHelper.openContactUs(this);

        } else if (i == R.id.rly_help) {
            UIHelper.showUsinghelp(this);

        } else if (i == R.id.rly_update) {//                CommonWebViewActivity.start(this, "http://beta.tiantianyuyin.com/ttyy/real_name/index.html");
            startActivity(new Intent(this, AboutActivity.class));

        } else if (i == R.id.rly_user_protocol) {
            CommonWebViewActivity.start(this, BaseUrl.USER_PRIVATE_AGREEMENT);

        } else if (i == R.id.rly_lab) {
            startActivity(new Intent(this, LabActivity.class));

        } else if (i == R.id.btn_login_out) {//                JPushHelper.getInstance().onAliasAction(this,CoreManager.getCore(IAuthCore.class).getCurrentUid()+"",JPushHelper.ACTION_DELETE_ALIAS);
            CoreManager.getCore(IAuthCore.class).logout();
            CoreManager.getCore(IPlayerCore.class).clearPlayerListMusicInfos();
            PreferencesUtils.setFristQQ(true);
            finish();

        } else if (i == R.id.rl_blacklist) {
            ARouter.getInstance().build(CommonRoutePath.BLACK_LIST_ACTIVITY_ROUTE_PATH).navigation();

        }
    }

    private void initView() {
        if (BasicConfig.INSTANCE.isDebuggable()) {
            rlyLab.setVisibility(View.VISIBLE);
        } else {
            rlyLab.setVisibility(View.GONE);
        }
        settingRight.setText("V" + BasicConfig.getLocalVersionName(getApplication()));
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onIsPhone() {
        getDialogManager().dismissDialog();

        Intent intent = new Intent(this, BinderPhoneActivity.class);
        intent.putExtra(BinderPhoneActivity.hasBand, BinderPhoneActivity.modifyBand);
        startActivity(intent);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onIsphoneFail(String error) {
        getDialogManager().dismissDialog();
        Intent intent = new Intent(this, BinderPhoneActivity.class);

        startActivity(intent);
    }
}
