package com.tongdaxing.erban.common.ui.me.setting.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.uuzuche.lib_zxing.activity.CaptureFragment;
import com.uuzuche.lib_zxing.activity.CodeUtils;

public class QrCodeScanerActivity extends BaseActivity
        implements CodeUtils.AnalyzeCallback {

    private final String TAG = QrCodeScanerActivity.class.getSimpleName();

    public static void start(Context context) {
        Intent intent = new Intent(context, QrCodeScanerActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_scan);
        initTitleBar();
        /**
         * 执行扫面Fragment的初始化操作
         */
        CaptureFragment captureFragment = new CaptureFragment();
        // 为二维码扫描界面设置定制化界面
        CodeUtils.setFragmentArgs(captureFragment, R.layout.fragment_qrcode_scaner);
        captureFragment.setAnalyzeCallback(this);
        /**
         * 替换我们的扫描控件
         */
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_my_container, captureFragment).commit();
    }

    public void initTitleBar() {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            mTitleBar.setBackground(new ColorDrawable(Color.BLACK));
            mTitleBar.setTitle(getResources().getString(R.string.setting_qrcode_scaner));
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.title_212121));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(v -> onLeftClickListener());
        }
    }

    @Override
    public void onAnalyzeSuccess(Bitmap mBitmap, String result) {
        LogUtils.d(TAG, "onAnalyzeSuccess-result:" + result);
        QrCodeScanResultActivity.start(this, result);
        finish();
    }

    @Override
    public void onAnalyzeFailed() {
        LogUtils.d(TAG, "onAnalyzeFailed");

    }
}
