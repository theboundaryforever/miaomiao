package com.tongdaxing.erban.common.ui.me.audiocard;

import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.presenter.audio.AudioCardPresenter;
import com.tongdaxing.erban.common.presenter.audio.IAudioCardView;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.ui.widget.SecondTimeView;
import com.tongdaxing.erban.common.ui.widget.WaveView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.home.AudioCardContent;
import com.tongdaxing.xchat_core.home.AudioCardInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.media.AudioPlayer;
import com.tongdaxing.xchat_framework.media.OnPlayListener;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 声卡
 */
@Deprecated
@CreatePresenter(AudioCardPresenter.class)
public class AudioCardActivity extends BaseMvpActivity<IAudioCardView, AudioCardPresenter> implements IAudioCardView, View.OnClickListener {

    public static final String AUDIO_FILE_URL = "AUDIO_FILE_URL";
    public static final String AUDIO_LENGTH = "AUDIO_LENGTH";

    //录制状态，0初始，1录制中，2停止，播放，暂停
    private final static int RECORD_STATUS_INIT = 0, RECORD_STATUS_RECORDING = 1, RECORD_STATUS_STOP = 2,
            RECORD_STATUS_PLAY = 3, RECORD_STATUS_PAUSE = 4, RECORD_STATUS_RESUME = 5;

    SecondTimeView timeTv;
    WaveView waveView;
    ImageView imageView, bgTopIv;
    SVGAImageView svgaImageView;
    View recordBtn, completeBtn, reRecordBtn;

    private int currRecordStatus = RECORD_STATUS_INIT;//当前状态

    private SVGADrawable recordAnimDrawable;
    private TextView tvNext;
    private TextView tvContent, tvTitle;
    private AudioRecorder audioRecorder;
    private AudioPlayer player;

    private int audioLength;
    private String audioFileUrl;
    private List<AudioCardContent> list;
    private List<AudioCardContent> randomList;
    private Random random;
    private boolean isLoadingData = false;//是否加载数据
    private TitleBar.TextAction clearAudioCardAction;
    private IAudioRecordCallback audioRecordCallback = new IAudioRecordCallback() {
        @Override
        public void onRecordReady() {

        }

        @Override
        public void onRecordStart(File audioFile, RecordType recordType) {
            if (audioRecorder != null) {//这里做释放后为null，加判断就好
                setupAudioCardRecordView();
            }
        }

        @Override
        public void onRecordSuccess(File file, long audioLength, RecordType recordType) {
            if (audioRecorder != null) {//这里做释放后为null，加判断就好
                audioFileUrl = file.getAbsolutePath();
                player.setDataSource(audioFileUrl);
                setupAudioCardRecordStopView();
            }
        }

        @Override
        public void onRecordFail() {
            if (audioRecorder != null) {//这里做释放后为null，加判断就好
                setupAudioCardInitStatusView();
                toast(R.string.record_fail);
            }
        }

        @Override
        public void onRecordCancel() {
            if (audioRecorder != null) {//这里做释放后为null，加判断就好
                audioRecorder.completeRecord(false);
            }
        }

        @Override
        public void onRecordReachedMaxTime(int maxTime) {

        }
    };
    //监听读秒
    private SecondTimeView.OnSecondTimeListener onSecondTimeListener = () -> {
        if (currRecordStatus == RECORD_STATUS_RECORDING) {//秒读完,正在录音的话，正常结束录音
            audioRecorder.completeRecord(false);
        }
    };
    private OnPlayListener onPlayListener = new OnPlayListener() {
        @Override
        public void onPrepared() {
            setupPlayAudioCardView();//这里场景只有play才会prepare，所以可以写在这里
        }

        @Override
        public void onCompletion() {
            setupPauseAudioCardView();
        }

        @Override
        public void onInterrupt() {
            setupPauseAudioCardView();
        }

        @Override
        public void onError(String error) {
            setupPauseAudioCardView();
            toast(R.string.audio_play_fail);
            LogUtil.e(error);
        }

        @Override
        public void onPlaying(long curPosition) {
        }
    };
    private int current = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_card);
        initTitleBar(getString(R.string.audio_card));
        mTitleBar.setCommonBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        mTitleBar.setTitleColor(ContextCompat.getColor(this, R.color.white));
        mTitleBar.setLeftImageResource(R.drawable.arrow_left_white);
        mTitleBar.setActionTextColor(getResources().getColor(R.color.white));
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvContent = (TextView) findViewById(R.id.tv_content);
//        tvContent.setOnClickListener(this);
        tvNext = (TextView) findViewById(R.id.tv_next);
        tvNext.setOnClickListener(this);
        timeTv = (SecondTimeView) findViewById(R.id.time_tv);
        waveView = (WaveView) findViewById(R.id.wave_view);
        imageView = (ImageView) findViewById(R.id.image_view);
        bgTopIv = (ImageView) findViewById(R.id.bg_top);
        svgaImageView = (SVGAImageView) findViewById(R.id.svga_imageview);
        completeBtn = findViewById(R.id.complete_btn);
        reRecordBtn = findViewById(R.id.re_record_btn);
        recordBtn = findViewById(R.id.record_layout);
        recordBtn.setOnClickListener(view -> {
            switch (currRecordStatus) {
                case RECORD_STATUS_INIT:
                    audioRecorder.startRecord();
                    break;
                case RECORD_STATUS_RECORDING:
                    if (timeTv.getCurrSecondTime() < 5) {
                        toast(R.string.audio_length_tip);
                    } else {
                        audioRecorder.completeRecord(false);
                    }
                    break;
                case RECORD_STATUS_STOP:
                case RECORD_STATUS_PAUSE:
                case RECORD_STATUS_RESUME:
                    if (!TextUtils.isEmpty(audioFileUrl)) {
                        player.start(AudioManager.STREAM_MUSIC);
                    } else {
                        toast(R.string.audio_play_fail);
                    }
                    break;
                case RECORD_STATUS_PLAY:
                    player.stop();
                    break;
            }
        });
        reRecordBtn.setOnClickListener(view -> {
            if (audioRecorder.isRecording()) {
                audioRecorder.completeRecord(true);
            }
            if (player.isPlaying()) {
                player.stop();
            }
            setupAudioCardInitStatusView();
        });
        completeBtn.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(audioFileUrl)) {
                getDialogManager().showProgressDialog(AudioCardActivity.this, getString(R.string.please_wait));
                CoreManager.getCore(IFileCore.class).upload(new File(audioFileUrl));
            }
        });

        waveView.setInitialRadius(getResources().getDimension(R.dimen.audio_card_init_radius));
        waveView.setMaxRadius(getResources().getDimension(R.dimen.audio_card_max_radius));
        waveView.setInitialAlpha(0.5f);
        waveView.setColor(getResources().getColor(R.color.white));
        svgaImageView.setClearsAfterStop(true);
        svgaImageView.setLoops(Integer.MAX_VALUE);

        ViewGroup.LayoutParams bgLp = bgTopIv.getLayoutParams();
        bgLp.height = DisplayUtils.getScreenWidth(this) * 353 / 375;
        bgTopIv.setLayoutParams(bgLp);

        audioRecorder = AudioPlayAndRecordManager.getInstance().getAudioRecorder(getApplicationContext(), 0, audioRecordCallback);
        timeTv.setOnSecondTimeListener(onSecondTimeListener);
        player = AudioPlayAndRecordManager.getInstance().createAudioPlayer(getApplicationContext(), onPlayListener);

        Intent intent = getIntent();
        audioFileUrl = intent.getStringExtra(AUDIO_FILE_URL);
        audioLength = intent.getIntExtra(AUDIO_LENGTH, 0);
        if (!TextUtils.isEmpty(audioFileUrl)) {
            setupAudioCardRecordResumeView();
            player.setDataSource(audioFileUrl);
        } else {
            setupAudioCardInitStatusView();
        }

        loadAudioCardSignature(Constants.PAGE_START);
    }

    private void loadAudioCardSignature(int page) {
        if (isLoadingData) {
            return;
        }
        isLoadingData = true;
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        params.put("pageNum", String.valueOf(page));
        params.put("pageSize", "20");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getAudioCardSignature(), params, new OkHttpManager.MyCallBack<ServiceResult<List<AudioCardContent>>>() {
            @Override
            public void onError(Exception e) {
                isLoadingData = false;
            }

            @Override
            public void onResponse(ServiceResult<List<AudioCardContent>> response) {
                isLoadingData = false;
                if (response.isSuccess()) {
                    if (list == null) {
                        list = new ArrayList<>();
                    } else {
                        list.clear();
                    }
                    list = response.getData();
                    if (randomList == null) {
                        randomList = new ArrayList<>();
                    } else {
                        randomList.clear();
                    }
                    randomList.addAll(list);
                    if (list.size() > 0) {
                        current = page;
                        switchContent();
                    } else {
                        current = 1;
                        loadAudioCardSignature(current);
                    }
                }
            }
        });
    }

    /**
     * 随机一个内容
     */
    private AudioCardContent randomContent() {
        if (random == null) {
            random = new Random();
        }
        int randomInt = random.nextInt(randomList.size());
        AudioCardContent randomContent = randomList.get(randomInt);
        randomList.remove(randomInt);
        return randomContent;
    }

    private void switchContent() {
        if (list != null && !ListUtils.isListEmpty(randomList)) {//还没随机完
            AudioCardContent content = randomContent();
            tvTitle.setText(content.getTitle());
            tvContent.setText(content.getContent());
        } else {
            loadAudioCardSignature(++current);
        }
    }

    /**
     * 设置为声卡录制视图
     */
    private void setupAudioCardRecordView() {
        currRecordStatus = RECORD_STATUS_RECORDING;
        timeTv.startCount(30, 1);//最长30秒
        waveView.start();
        completeBtn.setEnabled(false);
        if (recordAnimDrawable == null) {//未加载的话，先加载一次
            SVGAParser parser = new SVGAParser(this);
            parser.parse("anim_audio_card_record", new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity videoItem) {
                    recordAnimDrawable = new SVGADrawable(videoItem);
                    svgaImageView.setImageDrawable(recordAnimDrawable);
                    imageView.setVisibility(View.GONE);
                    svgaImageView.setVisibility(View.VISIBLE);
                    svgaImageView.startAnimation();
                }

                @Override
                public void onError() {
                }
            });
        } else {
            imageView.setVisibility(View.GONE);
            svgaImageView.setVisibility(View.VISIBLE);
            svgaImageView.startAnimation();
        }
    }

    /**
     * 设置为声卡初始状态视图
     */
    private void setupAudioCardInitStatusView() {
        currRecordStatus = RECORD_STATUS_INIT;
        audioFileUrl = null;
        audioLength = 0;
        timeTv.resetCount();
        waveView.stop();
        imageView.setVisibility(View.VISIBLE);
        svgaImageView.setVisibility(View.GONE);
        imageView.setImageResource(R.drawable.ic_audio_card_record);
        completeBtn.setEnabled(false);
        if (clearAudioCardAction != null) {
            mTitleBar.removeAction(clearAudioCardAction);//移除右上角清空声卡按钮\
        }
    }

    /**
     * 设置为声卡进入时待播放视图
     */
    private void setupAudioCardRecordResumeView() {
        currRecordStatus = RECORD_STATUS_RESUME;
        imageView.setVisibility(View.VISIBLE);
        svgaImageView.setVisibility(View.GONE);
        timeTv.setCurrCount(audioLength);//显示最大秒数
        imageView.setImageResource(R.drawable.ic_audio_card_play_big);
        completeBtn.setEnabled(false);
        clearAudioCardAction = new TitleBar.TextAction(getResources().getString(R.string.audio_card_clear)) {
            @Override
            public void performAction(View view) {
                clearAudioCard();
            }
        };
        mTitleBar.addAction(clearAudioCardAction);//显示右上角清空声卡按钮
    }

    /**
     * 设置为声卡停止录制视图
     */
    private void setupAudioCardRecordStopView() {
        currRecordStatus = RECORD_STATUS_STOP;
        timeTv.stopCount();
        audioLength = timeTv.getCurrSecondTime();
        waveView.stop();
        imageView.setVisibility(View.VISIBLE);
        svgaImageView.setVisibility(View.GONE);
        imageView.setImageResource(R.drawable.ic_audio_card_play_big);
        completeBtn.setEnabled(true);
    }

    /**
     * 设置为播放声卡视图
     */
    private void setupPlayAudioCardView() {
        currRecordStatus = RECORD_STATUS_PLAY;
        timeTv.startCount(audioLength, 0);
        imageView.setImageResource(R.drawable.ic_audio_card_pause_big);
    }

    /**
     * 设置为暂停（停止）播放声卡视图
     */
    private void setupPauseAudioCardView() {
        currRecordStatus = RECORD_STATUS_PAUSE;
        timeTv.resetCount();
        timeTv.setCurrCount(audioLength);//设置一下，保持显示最大秒数
        imageView.setImageResource(R.drawable.ic_audio_card_play_big);
    }

    /**
     * 清空声卡
     */
    private void clearAudioCard() {
        getDialogManager().showOkCancelDialog(getResources().getString(R.string.audio_card_clear_tip), true, new DialogManager.OkCancelDialogListener() {
            @Override
            public void onCancel() {

            }

            @Override
            public void onOk() {
                UserInfo user = new UserInfo();
                user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                user.setUserVoice("");
                user.setVoiceDura(0);
                getDialogManager().showProgressDialog(AudioCardActivity.this, getString(R.string.please_wait));
                CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user, false, "userVoice", "voiceDura");
            }
        });
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        getMvpPresenter().getAudioCard(url, audioLength);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        getDialogManager().dismissDialog();
        toast(R.string.audio_upload_fail);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        getDialogManager().dismissDialog();
        if (currRecordStatus == RECORD_STATUS_RESUME) {//在进入待播放页面状态下更新用户信息（也就是清空声卡）
            audioFileUrl = "";
            audioLength = 0;
        }
        Intent intent = new Intent();
        intent.putExtra(AUDIO_FILE_URL, audioFileUrl);
        intent.putExtra(AUDIO_LENGTH, audioLength);
        setResult(RESULT_OK, intent);
        if (currRecordStatus == RECORD_STATUS_RESUME) {
            setupAudioCardInitStatusView();
        } else {
            toast("声音上传成功，审核中");
            CommonWebViewActivity.start(this, UriProvider.getAudioIdentifyCard());
            finish();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        toast(error);
        getDialogManager().dismissDialog();
    }

    @Override
    protected void onDestroy() {
        waveView.stop();
        timeTv.stopCount();
        if (svgaImageView.isAnimating()) {
            svgaImageView.stopAnimation();
        }
//        if (audioRecorder.isRecording()) {
//            audioRecorder.completeRecord(true);
//        }
        audioRecorder.destroyAudioRecorder();
//        audioRecorder = null;
//        audioRecordCallback = null;
        AudioPlayAndRecordManager.getInstance().releaseAudioPlayer(player);
        onPlayListener = null;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (audioRecorder.isRecording()) {
            audioRecorder.completeRecord(true);
        }
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tv_next) {
            switchContent();

        }
    }

    @Override
    public void getAudioIdentifyCard(AudioCardInfo audioCardInfo, String url, long audioDuration) {
        audioFileUrl = url;
        UserInfo user = new UserInfo();
        user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        user.setUserVoice(audioFileUrl);
        user.setVoiceDura(audioLength);
        CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user, false);
    }

    @Override
    public void closeProgressDialog() {
        getDialogManager().dismissDialog();
    }
}
