package com.tongdaxing.erban.common.room.avroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.donkingliang.labels.LabelsView;
import com.kyleduo.switchbutton.SwitchButton;
import com.netease.nimlib.sdk.StatusCode;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.NewTabInfo;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.RoomSettingPresenter;
import com.tongdaxing.xchat_core.room.view.IRoomSettingView;
import com.tongdaxing.xchat_core.utils.StringUtils;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 房间设置
 *
 * @author chenran
 * @date 2017/9/26
 */
@CreatePresenter(RoomSettingPresenter.class)
public class RoomSettingActivity extends BaseMvpActivity<IRoomSettingView, RoomSettingPresenter>
        implements LabelsView.OnLabelClickListener, View.OnClickListener, IRoomSettingView, CompoundButton.OnCheckedChangeListener {
    private EditText nameEdit;
    private EditText topicEdit;
    private EditText pwdEdit;
    private LabelsView labelsView;
    private RoomInfo roomInfo;
    private List<String> labels;
    private String selectLabel;
    private RelativeLayout managerLayout;
    private RelativeLayout blackLayout;
    private RelativeLayout rlRoomPlay;
    private LinearLayout mLabelLayout;
    private TextView tvClearExceptionAudio;
    private NewTabInfo mSelectTabInfo;
    private List<NewTabInfo> mTabInfoList;
    private RelativeLayout bgLayout;
    private String mBackPic;
    private String mBackPicUrl = "";//新增字段网络背景图片地址
    //    private TextView bgName;
    private RelativeLayout topicLayout;//房间话题
    private RelativeLayout smallEffect;//小礼物特效
    private SwitchButton sbSetting;//小礼物特效开关
    private RelativeLayout rlEggMsgSwitch;//砸蛋消息layout
    private SwitchButton sbEggMsgSwitch;//砸蛋消息开关
    private int changeGiftEffect = -1;
    private int drawMsgOption = 0;


    public static void start(Context context, RoomInfo roomInfo) {
        Intent intent = new Intent(context, RoomSettingActivity.class);
        intent.putExtra("roomInfo", roomInfo);
        context.startActivity(intent);
    }

    public static void start(Context context, RoomInfo roomInfo, int isPermitRoom) {
        Intent intent = new Intent(context, RoomSettingActivity.class);
        intent.putExtra("roomInfo", roomInfo);
        intent.putExtra("isPermitRoom", isPermitRoom);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_setting);
        initTitleBar(getString(R.string.room_setting));
        roomInfo = getIntent().getParcelableExtra("roomInfo");
        if (roomInfo == null) {
            return;
        }
        int isPermitRoom = getIntent().getIntExtra("isPermitRoom", -2);
        if (isPermitRoom != -2) {
            roomInfo.setIsPermitRoom(isPermitRoom);
        }
        //初始化界面要在获取数据之后
        initView();

        mBackPic = roomInfo.getBackPic();
//        bgName.setText(BgTypeHelper.getBgName(mBackPic));

        getMvpPresenter().requestTagAll(roomInfo.getTagType());
        if (!AvRoomDataManager.get().isRoomOwner(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()))) {
            managerLayout.setVisibility(View.GONE);
        } else {
            managerLayout.setVisibility(View.VISIBLE);
        }
        labelsView.setOnLabelClickListener(this);
    }

    /**
     * 隐藏虚拟键盘
     */
    public void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && imm.isActive()) {
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);

        }
    }

    private void initView() {
        tvClearExceptionAudio = (TextView) findViewById(R.id.tv_clear_exception_audio);
        nameEdit = (EditText) findViewById(R.id.name_edit);
        topicEdit = (EditText) findViewById(R.id.topic_edit);
        pwdEdit = (EditText) findViewById(R.id.pwd_edit);
        labelsView = (LabelsView) findViewById(R.id.labels_view);
        managerLayout = (RelativeLayout) findViewById(R.id.manager_layout);
        bgLayout = (RelativeLayout) findViewById(R.id.bg_layout);
        blackLayout = (RelativeLayout) findViewById(R.id.black_layout);
        mLabelLayout = (LinearLayout) findViewById(R.id.label_layout);
//        bgName = (TextView) findViewById(R.id.tv_bg_name);
        topicLayout = (RelativeLayout) findViewById(R.id.topic_layout);
        rlRoomPlay = (RelativeLayout) findViewById(R.id.rl_room_tip);
        smallEffect = (RelativeLayout) findViewById(R.id.rl_small_gift_effect);
        sbSetting = (SwitchButton) findViewById(R.id.sb_small_gift_effect);
        rlEggMsgSwitch = (RelativeLayout) findViewById(R.id.rl_egg_msg_switch);
        sbEggMsgSwitch = (SwitchButton) findViewById(R.id.sb_egg_msg_switch);
        changeGiftEffect = roomInfo.getGiftEffectSwitch();
        sbSetting.setChecked(roomInfo.getGiftEffectSwitch() == 0);
        tvClearExceptionAudio.setOnClickListener(this);
        sbSetting.setOnCheckedChangeListener(this);
        rlRoomPlay.setOnClickListener(this);
        managerLayout.setOnClickListener(this);
        blackLayout.setOnClickListener(this);
        bgLayout.setOnClickListener(this);
        topicLayout.setOnClickListener(this);

        findViewById(R.id.tv_save).setOnClickListener(this);

        if (roomInfo != null) {
            nameEdit.setText(roomInfo.getTitle());
            topicEdit.setText(roomInfo.getRoomDesc());
            pwdEdit.setText(roomInfo.getRoomPwd());
            selectLabel = roomInfo.getRoomTag();
            drawMsgOption = roomInfo.getDrawMsgOption();
            sbEggMsgSwitch.setChecked(drawMsgOption == 1);
        }
        sbEggMsgSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                drawMsgOption = isChecked ? 1 : 0;
            }
        });

        if (selectLabel == null) {
            selectLabel = "";
        }
    }

    private void save() {
        String name = roomInfo.getTitle();
//        String desc = roomInfo.getRoomDesc();
        String pwd = roomInfo.getRoomPwd();
        boolean pwdModified = false;
        String label = roomInfo.getRoomTag();
        String backPic = roomInfo.getBackPic();
        if (!nameEdit.getText().toString().equals(roomInfo.getTitle())) {
            name = nameEdit.getText().toString();
        }
//        if (!topicEdit.getText().toString().equals(roomInfo.getRoomDesc())) {
//            desc = topicEdit.getText().toString();
//        }
        if (!pwdEdit.getText().toString().equals(roomInfo.getRoomPwd())) {
            pwdModified = true;
            pwd = pwdEdit.getText().toString();
        }
        if (!selectLabel.equals(roomInfo.getRoomTag())) {
            label = selectLabel;
        }
        if (!mBackPic.equals(roomInfo.getBackPic())) {
            backPic = mBackPic;
        }
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            if (TextUtils.isEmpty(name) && !pwdModified && TextUtils.isEmpty(label)
                    && ((TextUtils.isEmpty(backPic) && TextUtils.isEmpty(roomInfo.getRoomTag())) || backPic.equals(roomInfo.getRoomTag()))
                    && changeGiftEffect == roomInfo.getGiftEffectSwitch() && drawMsgOption == roomInfo.getDrawMsgOption()) {
                toast("暂无更改");
            } else {
                hideKeyboard(nameEdit);
                getDialogManager().showProgressDialog(this, "请稍后...");

                int id = roomInfo.tagId;
                if (mSelectTabInfo != null) {
                    id = mSelectTabInfo.getId();
                }

                if (AvRoomDataManager.get().isRoomOwner()) {
                    getMvpPresenter().updateRoomInfo(name, pwd, label, id, backPic, changeGiftEffect, drawMsgOption);
                } else if (AvRoomDataManager.get().isRoomAdmin()) {
                    getMvpPresenter().updateByAdmin(roomInfo.getUid(), name, pwd, label, id, backPic, changeGiftEffect, drawMsgOption);
                }
            }
        }
    }


    @Override
    public void onLabelClick(View label, String labelText, int position) {
        if (!ListUtils.isListEmpty(mTabInfoList)) {
            mSelectTabInfo = mTabInfoList.get(position);
        }
        selectLabel = labelText;
        labelsView.setSelects(position);
    }

    private void clearExceptionAudio() {
        getDialogManager().showProgressDialog(this, "请稍后...");
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", String.valueOf(roomInfo.getRoomId()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.clearExceptionAudio(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                toast("校验失败");
                getDialogManager().dismissDialog();
            }

            @Override
            public void onResponse(Json response) {
                getDialogManager().dismissDialog();
                if (response != null && response.num("code") == 200) {
                    toast("校验成功");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();//设置进入房间提示
        if (i == R.id.tv_clear_exception_audio) {
            clearExceptionAudio();

        } else if (i == R.id.manager_layout) {
            RoomManagerListActivity.start(this);

        } else if (i == R.id.rl_room_tip) {
            RoomPlayTipActivity.start(this);

        } else if (i == R.id.tv_save) {
            save();

        } else if (i == R.id.black_layout) {
            RoomBlackListActivity.start(this);

        } else if (i == R.id.bg_layout) {
            if (roomInfo == null)
                return;
//                if (roomInfo.getIsPermitRoom() != 2) {
            Intent intent = new Intent(this, RoomSelectBgActivity.class);
            intent.putExtra("backPic", roomInfo.getBackPic());
            startActivityForResult(intent, 2);
//                } else {
//                    toast("只有牌照厅才支持更换背景哦");
//                }

        } else if (i == R.id.topic_layout) {
            RoomTopicActivity.start(this);

        } else {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == 2) {
                if (data != null) {
                    String selectIndex = data.getStringExtra("selectIndex");
//                    String selectName = data.getStringExtra("selectName");
                    mBackPicUrl = data.getStringExtra("selectUrl");
                    mBackPic = StringUtils.isEmpty(selectIndex) ? "0" : selectIndex;
//                    bgName.setText(selectName);
//                    toast(mBackPic);
                }
            }
        }

    }

    @Override
    public void onResultRequestTagAllSuccess(List<NewTabInfo> tabInfoList) {
        mTabInfoList = tabInfoList;
        if (ListUtils.isListEmpty(tabInfoList)) {
            mLabelLayout.setVisibility(View.GONE);
            return;
        }
        mLabelLayout.setVisibility(View.VISIBLE);

        labels = new ArrayList<>();
        for (NewTabInfo tabInfo : tabInfoList) {
            labels.add(tabInfo.getName());
        }

        labelsView.setLabels((ArrayList<String>) labels);
        if (roomInfo != null) {
            for (int i = 0; i < tabInfoList.size(); i++) {
                if (tabInfoList.get(i).getId() == roomInfo.tagId) {
                    labelsView.setSelects(labels.indexOf(tabInfoList.get(i).getName()));
                    return;
                }
            }
        }
    }

    @Override
    public void onResultRequestTagAllFail(String error) {
        toast(error);
    }

    @Override
    public void updateRoomInfoSuccess(RoomInfo roomInfo) {
        getDialogManager().dismissDialog();
        finish();
    }

    @Override
    public void updateRoomInfoFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }


    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }

    @Override
    protected void onReceiveChatRoomEvent(RoomEvent roomEvent) {
        super.onReceiveChatRoomEvent(roomEvent);
        int event = roomEvent.getEvent();
        switch (event) {
            case RoomEvent.KICK_OUT_ROOM:
                finish();
                break;
            case RoomEvent.ROOM_MANAGER_REMOVE:
                long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                if (!AvRoomDataManager.get().isRoomOwner(uid)) {
                    toast(R.string.remove_room_manager);
                    finish();
                }
            default:
        }
    }

    //小礼物特效开关
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked) {
            changeGiftEffect = 1;
        } else {
            changeGiftEffect = 0;
        }
    }
}
