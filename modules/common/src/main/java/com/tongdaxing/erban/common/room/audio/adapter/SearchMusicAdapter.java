package com.tongdaxing.erban.common.room.audio.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.util.FileUtil;

/**
 * Function:
 * Author: Edward on 2019/2/20
 */
public class SearchMusicAdapter extends MyMusicAdapter {
    public SearchMusicAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyMusicInfo item) {
        helper.setText(R.id.tv_music_title, item.getTitle());
        helper.setText(R.id.tv_singer_name, resolveUnknownName(item.getArtist()) + "-" + FileUtil.getFileSizeMB(item.getSize()) + "m");
        helper.setText(R.id.tv_upload_name, "上传者: " + item.getUserNick());

        ImageView ivMusicStatus = helper.getView(R.id.iv_music_status);

        if (item.getFileStatus() == FILE_STATUS_DOWNLOAD) {//搜索结果只有3个状态
            ivMusicStatus.setImageResource(R.drawable.ic_download_music);
        } else if (item.getFileStatus() == FILE_STATUS_DOWNLOADING) {
            ImageLoadUtils.loadImage(mContext, R.drawable.ic_music_upload_loading, ivMusicStatus);
        } else {
            ivMusicStatus.setImageResource(FILE_STATUS_DOWNLOAD);
        }
    }
}
