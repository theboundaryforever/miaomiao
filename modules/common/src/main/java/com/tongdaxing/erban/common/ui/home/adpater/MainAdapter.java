package com.tongdaxing.erban.common.ui.home.adpater;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

/**
 * <p> 主页adapter  热门等 </p>
 * Created by Administrator on 2017/11/15.
 */
public class MainAdapter extends FragmentPagerAdapter {
    private SparseArray<Fragment> mFragmentList;

    public MainAdapter(FragmentManager fm, SparseArray<Fragment> fragmentSparseArray) {
        super(fm);
        this.mFragmentList = fragmentSparseArray;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList == null ? 0 : mFragmentList.size();
    }
}