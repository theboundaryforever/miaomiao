package com.tongdaxing.erban.common.ui.me.wallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.ui.me.wallet.presenter.IncomePresenter;
import com.tongdaxing.erban.common.ui.me.wallet.view.IIncomeView;
import com.tongdaxing.erban.common.ui.me.withdraw.WithdrawActivity;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MadisonRong on 08/01/2018.
 */
@CreatePresenter(IncomePresenter.class)
public class MyIncomeActivity extends BaseMvpActivity<IIncomeView, IncomePresenter>
        implements IIncomeView, View.OnClickListener {

    private static final String TAG = "MyIncomeActivity";

    @BindView(R2.id.tv_my_income_diamond_balance)
    TextView diamondBalanceTextView;
    @BindView(R2.id.btn_my_income_exchange_gold)
    Button exchangeGoldButton;
    @BindView(R2.id.btn_my_income_withdraw)
    Button withdrawButton;
    @BindString(R2.string.my_income)
    String titleContent;
    @BindView(R2.id.iv_exchange_awards)
    ImageView ivExchangeAwards;
    private boolean isRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_income);
        ButterKnife.bind(this);
        initTitleBar(titleContent);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMvpPresenter().loadWalletInfo();
    }

    private void initViews() {
        diamondBalanceTextView.setOnClickListener(this);
        exchangeGoldButton.setOnClickListener(this);
        withdrawButton.setOnClickListener(this);
        if (CoreManager.getCore(VersionsCore.class).getConfigData().num("isExchangeAwards") == 1)
            ivExchangeAwards.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        getMvpPresenter().handleClick(view.getId());
    }

    @Override
    public void setupUserWalletBalance(WalletInfo walletInfo) {
        L.info(TAG, "setupUserWalletBalance walletInfo: %s", walletInfo);
        diamondBalanceTextView.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.diamondNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
        diamondBalanceTextView.setText("0");
    }

    @Override
    public void handleClick(int id) {
        if (id == R.id.btn_my_income_exchange_gold) {
            startActivity(new Intent(this, ExchangeGoldActivity.class));

        } else if (id == R.id.btn_my_income_withdraw) {
            isRequest = true;
            getMvpPresenter().hasBindPhone();

        }
    }

    @Override
    public void hasBindPhone() {
        if (!isRequest) {
            return;
        }
        isRequest = false;
        boolean authStatus = RealNameAuthStatusChecker.getInstance().authStatus(this,
                getResources().getString(R.string.real_name_auth_tips_withdraw));
        L.info(TAG, "hasBindPhone authStatus: %b", authStatus);
        if (!authStatus) {
            return;
        }
        startActivity(new Intent(this, WithdrawActivity.class));
    }

    @Override
    public void hasBindPhoneFail(String error) {
        L.info(TAG, "hasBindPhoneFail error: %s, isRequest: %b", error, isRequest);
        if (!isRequest) {
            return;
        }
        startActivity(new Intent(this, BinderPhoneActivity.class));
    }
}
