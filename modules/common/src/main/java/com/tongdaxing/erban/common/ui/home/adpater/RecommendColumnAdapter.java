package com.tongdaxing.erban.common.ui.home.adpater;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.home.RecommendColumnInfo;

/**
 * Function:
 * Author: Edward on 2019/3/28
 */
public class RecommendColumnAdapter extends BaseQuickAdapter<RecommendColumnInfo, BaseViewHolder> {
    public RecommendColumnAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, RecommendColumnInfo item) {
        ImageView ivCover = helper.getView(R.id.iv_cover);
        GlideApp.with(mContext)
                .load(item.getAvatar())
                .placeholder(R.drawable.default_cover)
                .error(R.drawable.default_cover)
                .into(ivCover);

        helper.setText(R.id.tv_title, item.getTitle());
        helper.setText(R.id.tv_nick, item.getNick());
        helper.setText(R.id.tv_online_num, String.valueOf(item.getOnlineNum()));


        TextView tvCustomLabel = helper.getView(R.id.tv_custom_label);

        String roomTag = item.getRoomTag();
        if (TextUtils.isEmpty(roomTag)) {
            roomTag = "";
        }
        tvCustomLabel.setText(roomTag);
        tvCustomLabel.setVisibility(View.VISIBLE);

        ImageView ivPlay = helper.getView(R.id.iv_play);
        GlideApp.with(mContext)
                .load(R.drawable.icon_music_play_white)
                .into(ivPlay);
//        SVGAImageView svgaPlay = helper.getView(R.id.svga_play);
//
//        SvgaUtils.getInstance(mContext).cyclePlayAssetsAnim(svgaPlay, SvgaUtils.HALL_ROOM_ITEM_PLAY_SVGA_ASSETS);

        ImageView ivTabCategory = helper.getView(R.id.iv_tab_category);
        if (!TextUtils.isEmpty(item.getBadge())) {
            ivTabCategory.setImageDrawable(null);
            ivTabCategory.setVisibility(View.VISIBLE);
            GlideApp.with(mContext)
                    .load(item.getBadge())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Drawable> target, boolean b) {
                            ivTabCategory.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable drawable, Object o, Target<Drawable> target, DataSource dataSource, boolean b) {
                            float ratio = (drawable.getIntrinsicHeight() + 0.F) / drawable.getIntrinsicWidth();
                            int width = Math.round(mContext.getResources().getDimensionPixelOffset(R.dimen.tag_height) / ratio);
                            int height = mContext.getResources().getDimensionPixelOffset(R.dimen.tag_height);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ivTabCategory.getLayoutParams();
                            params.width = width;
                            params.height = height;
                            ivTabCategory.setLayoutParams(params);
                            ivTabCategory.setImageDrawable(drawable);
                            return true;
                        }
                    })
                    .into(ivTabCategory);
        } else {
            ivTabCategory.setVisibility(View.GONE);
        }

        ImageView ivTabCustom = helper.getView(R.id.iv_custom_tag);
        if (!TextUtils.isEmpty(item.getCustomTag())) {
            ivTabCustom.setImageDrawable(null);
            ivTabCustom.setVisibility(View.VISIBLE);
            GlideApp.with(mContext)
                    .load(item.getCustomTag())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Drawable> target, boolean b) {
                            ivTabCustom.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable drawable, Object o, Target<Drawable> target, DataSource dataSource, boolean b) {
                            float ratio = (drawable.getIntrinsicHeight() + 0.F) / drawable.getIntrinsicWidth();
                            int width = Math.round(mContext.getResources().getDimensionPixelOffset(R.dimen.custom_tag_height) / ratio);
                            int height = mContext.getResources().getDimensionPixelOffset(R.dimen.custom_tag_height);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ivTabCustom.getLayoutParams();
                            params.width = width;
                            params.height = height;
                            ivTabCustom.setLayoutParams(params);
                            ivTabCustom.setImageDrawable(drawable);
                            return true;
                        }
                    })
                    .into(ivTabCustom);
        } else {
            ivTabCustom.setVisibility(View.GONE);
        }
    }
}

