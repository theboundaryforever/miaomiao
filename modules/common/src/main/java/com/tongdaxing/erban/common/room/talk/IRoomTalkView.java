package com.tongdaxing.erban.common.room.talk;

import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;

import java.util.List;

/**
 * Created by Chen on 2019/4/18.
 */
public interface IRoomTalkView {
    void showTalkMessage(IRoomTalkMessage message);

    void showAllTalkMessage(List<IRoomTalkMessage> messageList);

    void showSendFailMessage(int errorCode, String errorMessage);

    void showLimitTip();

    void updateTalkView();
}
