package com.tongdaxing.erban.common.ui.widget.dialog.shareDialog;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.R;

import java.util.List;

/**
 * <p>
 * Created by zhangjian on 2019/6/19.
 */
public class NewShareDialogRcvAdapter extends BaseMultiItemQuickAdapter<ShareBean, BaseViewHolder> {
    public static final int OTHER = 0;//其他
    public static final int NOTICE_FANS = 1;//一键通知粉丝

    public NewShareDialogRcvAdapter(boolean isLinearLayoutManager, @Nullable List<ShareBean> data) {
        super(data);
        addItemType(NOTICE_FANS, isLinearLayoutManager ? R.layout.share_dialog_notice_fans_linear_item : R.layout.share_dialog_notice_fans_item);
        addItemType(OTHER, isLinearLayoutManager ? R.layout.share_dialog_linear_item : R.layout.share_dialog_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, ShareBean item) {
        switch (item.getItemType()) {
            case NOTICE_FANS:
                setNoticeFans(helper, item);
                break;
            case OTHER:
                setDefaultView(helper, item);
                break;
        }
    }

    private void setNoticeFans(BaseViewHolder helper, ShareBean item) {
        ((TextView) helper.getView(R.id.share_dialog_item_tv)).setText(item.getNameResId());
        TextView countDownTextView = helper.getView(R.id.share_dialog_item_count_down_tv);
        //显示倒计时
        countDownTextView.setText(item.getCountDown());
    }

    private void setDefaultView(BaseViewHolder helper, ShareBean item) {
        ImageView bgView = helper.getView(R.id.share_dialog_item_iv);
        ((TextView) helper.getView(R.id.share_dialog_item_tv)).setText(item.getNameResId());
        ImageView tagView = helper.getView(R.id.share_dialog_item_tag_iv);
        tagView.setVisibility(item.isShowTag() ? View.VISIBLE : View.GONE);
        int tagResId = item.getTagResId();
        tagView.setImageResource(tagResId == -1 ? R.drawable.share_dialog_tag_hot : tagResId);
        bgView.setImageResource(item.getResId());
    }
}
