package com.tongdaxing.erban.common.room.profile;

import android.util.SparseArray;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;

import java.util.List;

/**
 * Created by Chen on 2019/6/5.
 */
public interface IRoomProfileView {
    void updateChairView(SparseArray<RoomQueueInfo> queueMemberMap);

    void updateSpeakState(boolean state);

    void onReceiveFace(List<FaceReceiveInfo> receiveInfos);

    void showOwnerSelfInfo(ChatRoomMember chatRoomMember);

    void showGiftDialog(ChatRoomMember chatRoomMember);

    void attentionSuccess(long uid, boolean attention);

    void attentionFailed(String message);

    void attentionCancel(long uid);

    void updateOnlineUserNum(int onlineUserNum);

    void updateProfileView();
}
