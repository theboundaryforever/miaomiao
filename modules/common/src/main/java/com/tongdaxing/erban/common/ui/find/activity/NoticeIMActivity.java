package com.tongdaxing.erban.common.ui.find.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.juxiao.library_ui.widget.NestedScrollSwipeRefreshLayout;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.ui.find.adapter.NoticeIMAdapter;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.find.bean.NoticeIMBean;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * IM通知消息列表
 */
public class NoticeIMActivity extends BaseActivity {

    @BindView(R2.id.swipe_refresh)
    NestedScrollSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R2.id.recycler_view)
    RecyclerView recyclerView;
    private NoticeIMAdapter mAdapter;
    private String sessionId;
    private int currPage = Constants.PAGE_START;

    public static void start(Context context, String sessionId) {
        Intent intent = new Intent(context, NoticeIMActivity.class);
        intent.putExtra("data", sessionId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_group_im);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (intent == null) {
            showPageError(R.string.data_exception);
            return;
        }
        sessionId = intent.getStringExtra("data");
        if (TextUtils.isEmpty(sessionId)) {
            showPageError(R.string.data_exception);
            return;
        }
        initTitleBar(getString(R.string.im_notification));

        mAdapter = new NoticeIMAdapter(R.layout.adapter_voice_group_im_item);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
//
        mSwipeRefreshLayout.setRefreshing(true);
        showLoading();
        refreshData();
        setCallback();
    }

    @Override
    protected void onResume() {
        super.onResume();
        NIMClient.getService(MsgService.class).setChattingAccount(sessionId, SessionTypeEnum.P2P);
    }

    @Override
    protected void onPause() {
        super.onPause();
        NIMClient.getService(MsgService.class).setChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_NONE, SessionTypeEnum.P2P);
    }

    public void setupFailView(boolean isRefresh) {
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            showNoData("暂无通知消息");
        } else {
            mAdapter.loadMoreFail();
        }
    }

    public void setupSuccessView(List<NoticeIMBean> listBeanList, boolean isRefresh) {
        hideStatus();
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            if (ListUtils.isListEmpty(listBeanList)) {
                showNoData();
                listBeanList = new ArrayList<>();
            }
            mAdapter.setNewData(listBeanList);
        } else {
            mAdapter.loadMoreComplete();
            if (!ListUtils.isListEmpty(listBeanList)) {
                mAdapter.addData(listBeanList);
            }
        }
        //不够10个的话，不显示加载更多
        if (listBeanList.size() < Constants.PAGE_SIZE) {
            mAdapter.setEnableLoadMore(false);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        refreshData();
    }

    public void refreshData() {
        loadData(Constants.PAGE_START);
    }

    public void loadMoreData() {
        loadData(currPage + 1);
    }

    private void setCallback() {
        mSwipeRefreshLayout.setOnRefreshListener(this::refreshData);
        mAdapter.setOnLoadMoreListener(this::loadMoreData, recyclerView);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            NoticeIMBean bean = mAdapter.getItem(position);
            if (bean != null && bean.getLid() > 0) {
                NewUserInfoActivity.start(NoticeIMActivity.this, bean.getLid());
            } else {
                toast("数据异常!");
            }
        });
    }

    public void loadData(int page) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("pageNum", String.valueOf(page));
        params.put("pageSize", String.valueOf(Constants.PAGE_HOME_HOT_SIZE));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getNoticeList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<NoticeIMBean>>>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    setupFailView(page == Constants.PAGE_START);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<NoticeIMBean>> response) {
                currPage = page;
                if (response != null && response.isSuccess()) {
                    setupSuccessView(response.getData(), page == Constants.PAGE_START);
                } else {
                    onError(new Exception());
                }
            }
        });
    }

}
