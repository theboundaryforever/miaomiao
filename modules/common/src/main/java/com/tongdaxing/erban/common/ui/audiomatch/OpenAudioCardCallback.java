package com.tongdaxing.erban.common.ui.audiomatch;

public interface OpenAudioCardCallback {
    void openAudioCard();
}
