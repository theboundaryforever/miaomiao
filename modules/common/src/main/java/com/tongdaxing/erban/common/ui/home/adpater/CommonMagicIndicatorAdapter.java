package com.tongdaxing.erban.common.ui.home.adpater;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.widget.ScaleTransitionPagerTitleView;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * <p> 公共多个滑动tab样式 </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class CommonMagicIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;
    private int mBottomMargin;

    private int normalColorId = R.color.color_999999;
    private int selectColorId = R.color.color_1A1A1A;
    private int size = 16;
    private OnItemSelectListener mOnItemSelectListener;

    public CommonMagicIndicatorAdapter(Context context, List<TabInfo> titleList, int bottomMargin) {
        mContext = context;
        mTitleList = titleList;
        mBottomMargin = bottomMargin;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    public void setNormalColorId(@ColorRes int normalColorId) {
        this.normalColorId = normalColorId;
    }

    public void setSelectColorId(@ColorRes int selectColorId) {
        this.selectColorId = selectColorId;
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {
        ScaleTransitionPagerTitleView scaleTransitionPagerTitleView = new ScaleTransitionPagerTitleView(context);
        scaleTransitionPagerTitleView.setNormalColor(mContext.getResources().getColor(normalColorId));
        scaleTransitionPagerTitleView.setSelectedColor(mContext.getResources().getColor(selectColorId));
        scaleTransitionPagerTitleView.setMinScale(1);
        scaleTransitionPagerTitleView.setTextSize(size);
        scaleTransitionPagerTitleView.setText(mTitleList.get(i).getName());
        scaleTransitionPagerTitleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemSelectListener != null) {
                    mOnItemSelectListener.onItemSelect(i);
                }

            }
        });
        return scaleTransitionPagerTitleView;
    }

    @Override
    public IPagerIndicator getIndicator(Context context) {
        LinePagerIndicator indicator = new LinePagerIndicator(context);
        indicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
        indicator.setLineHeight(UIUtil.dip2px(mContext, 2.5));
        indicator.setRoundRadius(UIUtil.dip2px(mContext, 1.25));
        indicator.setLineWidth(UIUtil.dip2px(mContext, 27));
        indicator.setColors(ContextCompat.getColor(mContext, R.color.mm_theme));
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.bottomMargin = mBottomMargin;
        indicator.setLayoutParams(lp);
        return indicator;
    }

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}