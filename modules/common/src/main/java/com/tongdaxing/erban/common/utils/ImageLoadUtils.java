package com.tongdaxing.erban.common.utils;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.tongdaxing.erban.common.utils.blur.BlurTransformation;
import com.tongdaxing.erban.common.utils.blur.BlurTransformation;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;

import java.io.File;

/**
 * 图片加载处理
 * Created by chenran on 2017/11/9.
 */
public class ImageLoadUtils {
    private static final String PIC_PROCESSING = "?imageslim";
    private static final String ACCESS_URL = "img.erbanyy.com";

    public static void loadAvatar(Context context, String avatar, ImageView imageView, boolean isCircle) {
        if (isActivityDestroyed(context)) {
            return;
        }
        if (StringUtil.isEmpty(avatar)) {
            return;
        }

        StringBuilder sb = new StringBuilder(avatar);
        if (avatar.contains(ACCESS_URL)) {
            if (!avatar.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/100/h/100");
        }
        if (isCircle) {
            loadCircleImage(context, sb.toString(), imageView, R.drawable.ic_no_avatar);
        } else {
            loadImage(context, sb.toString(), imageView, R.drawable.ic_no_avatar);
        }
    }

    public static void loadBigAvatar(Context context, String avatar, ImageView imageView, boolean isCircle) {
        if (StringUtil.isEmpty(avatar)) {
            return;
        }
        StringBuffer sb = new StringBuffer(avatar);
        if (avatar.contains(ACCESS_URL)) {
            if (!avatar.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/150/h/150");
        }
        if (isCircle) {
            loadCircleImage(context, sb.toString(), imageView, R.drawable.ic_no_avatar);
        } else {
            loadImage(context, sb.toString(), imageView, R.drawable.ic_no_avatar);
        }
    }

    public static void loadAvatar(Context context, String avatar, ImageView imageView) {
        loadAvatar(context, avatar, imageView, false);
    }

    public static void loadSmallRoundBackground(Context context, String url, ImageView imageView) {
        if (isActivityDestroyed(context)) {
            return;
        }
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuilder sb = new StringBuilder(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/220/h/220");
        }
        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .transforms(new CenterCrop(),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size)))
                .placeholder(R.drawable.default_cover)
                .error(R.drawable.default_cover)
                .into(imageView);
    }

    public static void loadImageWithBlurTransformationAndCorner(Context context, String url, ImageView imageView) {
        if (isActivityDestroyed(context)) {
            return;
        }
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/75/h/75");
        }
        GlideApp.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new BlurTransformation(context, 25, 3),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size))
                )
                .into(imageView);

    }

    public static void loadRectangleImageWithBlurTransformationAndCorner(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/75/h/75");
        }
        GlideApp.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new BlurTransformation(context, 25, 3),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.dp_2))
                )
                .into(imageView);
    }


    /**
     * 圆角矩形
     *
     * @param context
     * @param url 或者 resId
     * @param imageView
     * @param defaultRes
     */
    public static void loadRectangleImage(Context context, Object url, ImageView imageView, int defaultRes) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .transforms(new CenterCrop(), new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size)))
                .error(defaultRes)
                .placeholder(defaultRes)
                .into(imageView);
    }

    /**
     * 圆角矩形
     *
     * @param context
     * @param url
     * @param imageView
     * @param defaultRes
     * @param radiusId   圆角尺寸id
     */
    public static void loadRectangleImage(Context context, Object url, ImageView imageView, int defaultRes, int radiusId) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .transforms(new CenterCrop(), new RoundedCorners(context.getResources().getDimensionPixelOffset(radiusId)))
                .error(defaultRes)
                .placeholder(defaultRes)
                .into(imageView);
    }

    public static void loadCircleImage(Context context, String url, ImageView imageView, int defaultRes) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .circleCrop()
                .placeholder(defaultRes)
                .into(imageView);
    }

    public static void loadCircleImage(Context context, int resId, ImageView imageView, int defaultRes) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(resId)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .circleCrop()
                .placeholder(defaultRes)
                .into(imageView);
    }

    public static void loadImage(Context context, String url, ImageView imageView, int defaultRes) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(url)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(defaultRes)
                .error(defaultRes)
                .into(imageView);
    }

    public static void loadImage(Context context, File file, ImageView imageView, int defaultRes) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(file).dontAnimate().placeholder(defaultRes).into(imageView);
    }

    public static void loadImage(Context context, @DrawableRes int id, ImageView imageView) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(id).into(imageView);
    }

    /**
     * 加载app资源图片方法
     *
     * @param context
     * @param id
     * @param imageView
     */
    public static void loadImageResNoCache(Context context, @DrawableRes int id, ImageView imageView, @DrawableRes int holderRes) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(id).placeholder(holderRes).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);
    }

    public static void loadImage(Context context, File file, ImageView imageView) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(file).dontAnimate().into(imageView);
    }

    public static void loadImage(Context context, String url, ImageView imageView) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(url).dontAnimate().into(imageView);
    }

    /**
     * 可以设置错误图和占位图方法
     *
     * @param context
     * @param url
     * @param imageView
     * @param holder
     * @param error
     */
    public static void loadImage(Context context, String url, ImageView imageView, int holder, int error) {
        if (isActivityDestroyed(context)) {
            return;
        }
        GlideApp.with(context).load(url).placeholder(holder).error(error).into(imageView);
    }

    private static boolean isActivityDestroyed(Context context) {
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.isFinishing() || activity.isDestroyed()) {
                return true;
            }
        }
        return false;
    }
}
