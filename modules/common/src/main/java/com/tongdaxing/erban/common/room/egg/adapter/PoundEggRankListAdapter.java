package com.tongdaxing.erban.common.room.egg.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.view.LevelView;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.Locale;

/**
 * <p>房间消费adapter  </p>
 *
 * @author Administrator
 * @date 2017/11/20
 */
public class PoundEggRankListAdapter extends BaseQuickAdapter<UserInfo, BaseViewHolder> {
    public int rankType = 0;
    private Drawable mManDrawable, mFemaleDrawable;

    public PoundEggRankListAdapter(Context context) {
        super(R.layout.adapter_pound_egg_rank);
        mManDrawable = context.getResources().getDrawable(R.drawable.icon_man);
        mFemaleDrawable = context.getResources().getDrawable(R.drawable.icon_female);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }
        baseViewHolder.setImageDrawable(R.id.gender, userInfo.getGender() == 1 ? mManDrawable : mFemaleDrawable)
                .setText(R.id.coin_text, String.format(Locale.getDefault(), String.valueOf(userInfo.getTol())));
        TextView nick = baseViewHolder.getView(R.id.nick);
        nick.setText(userInfo.getNick());
        ImageView avatar = baseViewHolder.getView(R.id.avatar);
        ImageLoadUtils.loadAvatar(mContext, userInfo.getAvatar(), avatar, true);
        TextView numberText = baseViewHolder.getView(R.id.auction_number_text);
        LevelView levelView = baseViewHolder.getView(R.id.level_info_room_user_list);
        levelView.setVisibility(View.GONE);
//        if (rankType == 0) {
//            levelView.setExperLevel(userInfo.getExperLevel());
//            levelView.setCharmLevel(0);
//        } else {
//            levelView.setCharmLevel(userInfo.getCharmLevel());
//            levelView.setExperLevel(0);
//        }
        numberText.setVisibility(View.GONE);
    }
}
