package com.tongdaxing.erban.common.ui.cpexclusive.editSign;

import com.erban.ui.mvp.BasePresenter;
import com.tcloud.core.log.L;
import com.tongdaxing.xchat_core.cp.CpEditSignEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * <p>
 * Created by zhangjian on 2019/5/5.
 */
public class CpEditSignDialogPresenter extends BasePresenter<ICpEditSignDialogView> {

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void closeDialog(CpEditSignEvent.OnEditClose onEditClose) {
        L.debug(CpEditSignView.TAG, "CpEditSignDialogFragment closeDialog");
        getView().dismissDialog();
    }
}
