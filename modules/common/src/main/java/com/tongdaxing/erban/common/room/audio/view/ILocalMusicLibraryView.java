package com.tongdaxing.erban.common.room.audio.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public interface ILocalMusicLibraryView extends IMvpBaseView {
    void copyDirectly(MyMusicInfo myMusicInfo, MusicLocalInfo localMusicInfo);

    void copyAfterUpload(MusicLocalInfo localMusicInfo);

    void refreshAdapter();

    void loadSongSuccess(List<MyMusicInfo> myMusicInfoList);

    void loadSongFailure();
}
