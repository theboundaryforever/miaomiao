package com.tongdaxing.erban.common.room.talk.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.tongdaxing.erban.common.room.talk.manager.TalkManager;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2019/4/18.
 */

public class RoomTalkAdapter<VH extends AbsBaseViewHolder, M extends IRoomTalkMessage> extends RecyclerView.Adapter<VH> implements IRecyclerView<M> {

    private final List<M> mMessages = new ArrayList<>();

    private final SparseArray<AbsBaseViewHolder.ViewHolderFactory> mViewHolderArrays = TalkManager.getInstance().getTalkClazzSparseArray();

    public RoomTalkAdapter() {
        setHasStableIds(true);
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return (VH) mViewHolderArrays.get(viewType).create(parent);  //这里把数据交给holder类
    }

    @Override
    public void onViewRecycled(VH holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public void onBindViewHolder(AbsBaseViewHolder holder, int position) {
        final M message = mMessages.get(position);
        holder.setPosition(position);
        holder.bind(message);
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        final M message = mMessages.get(position);
        return message.getRoomTalkType();
    }

    @Override
    public long getItemId(int position) {
        IRoomTalkMessage message = mMessages.get(position);
        if (message.getLocalId() == null) {
            return message.hashCode();
        } else {
            return message.getLocalId().hashCode() * 31 + message.getRoomTalkType();
        }
    }

    @Override
    public List<M> getData() {
        return mMessages;
    }

    @Override
    public void add(@NonNull M elem) {
        if (mMessages.size() > TalkManager.TALK_MAX_MESSAGE_COUNT) {
            remove(0);
        } else {
            mMessages.add(elem);
            notifyItemInserted(mMessages.size());
        }
    }

    @Override
    public void addAll(@NonNull List<M> elem) {
        final int oldSize = mMessages.size();
        mMessages.addAll(elem);
        notifyItemRangeInserted(oldSize, elem.size());
    }

    @Override
    public void add(@NonNull int index, @NonNull List<M> elem) {
        if (mMessages.size() > TalkManager.TALK_MAX_MESSAGE_COUNT) {
            remove(0);
        } else {
            mMessages.addAll(index, elem);
            notifyDataSetChanged();
        }
    }

    @Override
    public void set(@NonNull M oldElem, @NonNull M newElem) {
        set(mMessages.indexOf(oldElem), newElem);
    }

    @Override
    public void set(@NonNull int index, @NonNull M elem) {
        mMessages.set(index, elem);
        notifyItemChanged(index);
    }

    @Override
    public void remove(@NonNull M elem) {
        final int position = mMessages.indexOf(elem);
        mMessages.remove(elem);
        notifyItemRemoved(position);
    }

    @Override
    public void remove(@NonNull int index) {
        mMessages.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public void replaceAll(@NonNull List<M> elem) {
        mMessages.clear();
        mMessages.addAll(elem);
        notifyDataSetChanged();
    }

    @Override
    public boolean contains(@NonNull M elem) {
        return mMessages.contains(elem);
    }

    @Override
    public void clear() {
        mMessages.clear();
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataChange(int index) {
        notifyItemChanged(index);
    }

    public M getItem(int position) {
        return position >= mMessages.size() ? null : mMessages.get(position);
    }


}

