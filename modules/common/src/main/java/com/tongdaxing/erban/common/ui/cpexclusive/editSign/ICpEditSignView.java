package com.tongdaxing.erban.common.ui.cpexclusive.editSign;

/**
 * Created by zhangjian on 2019/4/30.
 */
public interface ICpEditSignView {
    void setCpSignSucceed();

    void setCpSignFailure(String message);

    void editFailureSensitiveWord();
}
