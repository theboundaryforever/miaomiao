package com.tongdaxing.erban.common.ui.me.wallet.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.ui.me.bills.activity.WithdrawBillsActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.BinderPhoneActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.ExchangeGoldActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.me.wallet.presenter.IncomePresenter;
import com.tongdaxing.erban.common.ui.me.wallet.view.IIncomeView;
import com.tongdaxing.erban.common.ui.me.withdraw.WithdrawActivity;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 新皮：收益页面
 *
 * @author zwk 2018/5/30
 */
@CreatePresenter(IncomePresenter.class)
public class MyIncomeFragment extends BaseMvpFragment<IIncomeView, IncomePresenter> implements IIncomeView, View.OnClickListener {
    @BindView(R2.id.tv_my_income_desc)
    TextView tvMyIncomeDesc;
    @BindView(R2.id.tv_my_income_diamond_balance)
    TextView diamondBalanceTextView;
    @BindView(R2.id.btn_my_income_exchange_gold)
    Button exchangeGoldButton;
    @BindView(R2.id.btn_my_income_withdraw)
    Button withdrawButton;
    @BindString(R2.string.my_income)
    String titleContent;
    @BindView(R2.id.iv_exchange_awards)
    ImageView ivExchangeAwards;
    @BindView(R2.id.title_bar)
    TitleBar mTitleBar;
    @BindView(R2.id.tv_exchange)
    TextView tvExchange;
    @BindView(R2.id.rl_my_income_exchange_gold)
    RelativeLayout rlMyIncomeExchangeGold;
    Unbinder unbinder;

    private boolean isRequest;
    private UserInfo mUserInfo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid(), true);
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.activity_my_income;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {
        diamondBalanceTextView.setOnClickListener(this);
        exchangeGoldButton.setOnClickListener(this);
        withdrawButton.setOnClickListener(this);
        tvExchange.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MyWalletNewActivity.isRefresh) {
            MyWalletNewActivity.isRefresh = false;
            getMvpPresenter().loadWalletInfo();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo userInfo) {
        mUserInfo = userInfo;
        setWithDrawVisible();
    }

    /**
     * 该用户不为工会主播 才出现提现按钮
     */
    private void setWithDrawVisible() {
        if (mUserInfo != null) {
            L.info(TAG, "setWithDrawVisible() -> mUserInfo.getLaborName() = %s", mUserInfo.getLaborName());
            //该用户不为工会主播 才出现提现按钮
            if (TextUtils.isEmpty(mUserInfo.getLaborName())) {
                tvMyIncomeDesc.setVisibility(View.VISIBLE);
                rlMyIncomeExchangeGold.setVisibility(View.VISIBLE);
                withdrawButton.setVisibility(View.VISIBLE);
            } else {
                tvMyIncomeDesc.setVisibility(View.GONE);
                rlMyIncomeExchangeGold.setVisibility(View.GONE);
                withdrawButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void initiate() {
        CoreManager.addClient(this);
        mTitleBar.setVisibility(View.GONE);
//        if (CoreManager.getCore(VersionsCore.class).getConfigData().num("isExchangeAwards") == 1)
//            ivExchangeAwards.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            getMvpPresenter().loadWalletInfo();
    }

    @Override
    public void onClick(View view) {
        getMvpPresenter().handleClick(view.getId());
    }

    @Override
    public void setupUserWalletBalance(WalletInfo walletInfo) {
        L.info(TAG, "setupUserWalletBalance() -> walletInfo = %s, mUserInfo.getLaborName() = %s", walletInfo, mUserInfo == null ? "mUserInfo = null" : mUserInfo.getLaborName());
        setWithDrawVisible();
        diamondBalanceTextView.setText(String.format(Locale.getDefault(), "%.1f", walletInfo.diamondNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
        diamondBalanceTextView.setText("0");
    }

    @Override
    public void handleClick(int id) {
        if (id == R.id.btn_my_income_exchange_gold) {
            startActivity(new Intent(mContext, ExchangeGoldActivity.class));

        } else if (id == R.id.btn_my_income_withdraw) {
            isRequest = true;
            getMvpPresenter().hasBindPhone();

        } else if (id == R.id.tv_exchange) {
            startActivity(new Intent(mContext, WithdrawBillsActivity.class));

        }
    }

    @Override
    public void hasBindPhone() {
        if (!isRequest) {
            return;
        }
        isRequest = false;
        if (!RealNameAuthStatusChecker.getInstance().authStatus(getActivity(), getResources().getString(R.string.real_name_auth_tips_withdraw))) {
            return;
        }
        startActivity(new Intent(mContext, WithdrawActivity.class));
    }

    @Override
    public void hasBindPhoneFail(String error) {
        if (!isRequest) {
            return;
        }
        startActivity(new Intent(mContext, BinderPhoneActivity.class));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
