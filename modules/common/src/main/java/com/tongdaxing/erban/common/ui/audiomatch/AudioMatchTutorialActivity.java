package com.tongdaxing.erban.common.ui.audiomatch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.presenter.audiomatch.AudioMatchPresenter;
import com.tongdaxing.erban.common.presenter.audiomatch.IAudioMatchView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * 声音匹配-新手教程
 */
@CreatePresenter(AudioMatchPresenter.class)
public class AudioMatchTutorialActivity extends BaseMvpActivity<IAudioMatchView, AudioMatchPresenter> implements View.OnClickListener, IAudioMatchView {
    private ViewPager viewPager;
    private int selectedPosition = 0;

    private Button nextBtn;
    private View skipBtn;

    private String[] nextBtnStr = {"下一步", "下一步", "开启缘分匹配"};
    private ImageView[] imageViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_match_tutorial);
        initPoint();
        setSwipeBackEnable(false);
        List<Fragment> mTabs = new ArrayList<>(3);
        mTabs.add(new FirstFragment());
        mTabs.add(new SecondFragment());
//        mTabs.add(new ThirdFragment());
        mTabs.add(new FourthFragment());
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(new OnPageChangeImpl());
        viewPager.setAdapter(new AudioMatchTutorialFragmentPagerAdapter(getSupportFragmentManager(), mTabs));
        viewPager.setOffscreenPageLimit(3);

        nextBtn = (Button) findViewById(R.id.btn_next);
        skipBtn = findViewById(R.id.iv_skip);
        skipBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        viewPager.setCurrentItem(0);
        onPageSelected(0);
    }

    private void initPoint() {
        imageViews = new ImageView[]{(ImageView) findViewById(R.id.iv_point_one),
                (ImageView) findViewById(R.id.iv_point_two),
                (ImageView) findViewById(R.id.iv_point_four)};
    }

    public void startAudioMatch() {
        setResult(RESULT_OK);
        finish();
    }

    public void nextPage(int position) {
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onClick(View v) {
        if (v == nextBtn) {
            switch (selectedPosition) {
                case 0:
                case 1:
                    nextPage(selectedPosition + 1);
                    break;
                case 2:
//                    String audioFileUrl = "";
//                    int audioLength = 0;
//                    UserInfo currUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//                    if (currUserInfo != null) {
//                        audioFileUrl = currUserInfo.getUserVoice();
//                        audioLength = currUserInfo.getVoiceDura();
//                    }
//                    Intent intent = new Intent(AudioMatchTutorialActivity.this, AudioCardActivity.class);
//                    intent.putExtra(AudioCardActivity.AUDIO_FILE_URL, audioFileUrl);
//                    intent.putExtra(AudioCardActivity.AUDIO_LENGTH, audioLength);
//                    checkPermission(() -> {
//                                startActivityForResult(intent, 10086);
//                                AudioCardMarkManager.getInstance(AudioMatchTutorialActivity.this).clearMark();
//                                nextPage(selectedPosition + 1);
//                            }, R.string.ask_again,
//                            Manifest.permission.RECORD_AUDIO);
//                    break;
                    startAudioMatch();
                    break;
//                case 3:
//                    startAudioMatch();
//                    break;
            }
        } else if (v == skipBtn) {
            startAudioMatch();
        }
    }

    private void onPageSelected(int position) {
        imageViews[position].setImageResource(R.mipmap.ic_blue_point);
        selectedPosition = position;
        nextBtn.setText(nextBtnStr[position]);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10086) {
            nextPage(selectedPosition + 1);
        }
    }

    private static class AudioMatchTutorialFragmentPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragments;

        AudioMatchTutorialFragmentPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.mFragments = fragments;
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

    private class OnPageChangeImpl implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            imageViews[position].setImageResource(R.mipmap.ic_blue_point);
            imageViews[selectedPosition].setImageResource(R.mipmap.ic_black_point);
            selectedPosition = position;

            nextBtn.setText(nextBtnStr[selectedPosition]);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
