package com.tongdaxing.erban.common.ui.find.adapter;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupDetailInfo;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public class VoiceGroupDetailAdapter extends BaseQuickAdapter<VoiceGroupDetailInfo.PlayerListBean, BaseViewHolder> {
    private CommentLikeCallback commentLikeCallback;

    public VoiceGroupDetailAdapter(int layoutResId) {
        super(layoutResId);
    }

    public void setCommentLikeCallback(CommentLikeCallback commentLikeCallback) {
        this.commentLikeCallback = commentLikeCallback;
    }

    @Override
    protected void convert(BaseViewHolder helper, VoiceGroupDetailInfo.PlayerListBean item) {
        ImageView ivUserHead = helper.getView(R.id.iv_user_head);
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), ivUserHead, R.drawable.ic_no_avatar);
        ivUserHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewUserInfoActivity.start(mContext, item.getUid());
            }
        });
        helper.setText(R.id.tv_name, item.getNick());
        helper.setText(R.id.tv_time, TimeUtils.getPostTimeString(mContext, item.getCreateDate(), true, false));
        TextView tvContent = helper.getView(R.id.tv_content);
        if (item.getType() == 2) {
            SpannableString mSpannableString = new SpannableString("回复 " + item.getPlayerNick() + ": " + item.getContext());
            ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.color_fcad22));
            int end = ("回复 " + item.getPlayerNick() + ": ").length();
            mSpannableString.setSpan(foregroundColorSpan, 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvContent.setText(mSpannableString);
        } else {
            tvContent.setText(item.getContext());
        }
        RadioButton radioButton = helper.getView(R.id.rb_like);
        radioButton.setOnClickListener(v -> {
            if (commentLikeCallback != null) {
                commentLikeCallback.likeClick(item.getId());
            }
        });
        radioButton.setChecked(item.isIsLike());
        if (item.getLikeNum() > 999) {
            radioButton.setText("999+");
        } else {
            radioButton.setText(String.valueOf(item.getLikeNum()));
        }
    }

    public interface CommentLikeCallback {
        void likeClick(int id);
    }
}
