package com.tongdaxing.erban.common.ui.rankinglist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.adapter.BaseIndicatorAdapter;
import com.tongdaxing.erban.common.ui.widget.TabRadioGroup;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.tongdaxing.erban.common.ui.rankinglist.RankingListFragment.RANKING_TYPE_CHARM;
import static com.tongdaxing.erban.common.ui.rankinglist.RankingListFragment.RANKING_TYPE_CP;
import static com.tongdaxing.erban.common.ui.rankinglist.RankingListFragment.RANKING_TYPE_TYCOON;

/**
 * 排行榜 页面
 */
public class RankingListActivity extends BaseActivity {
    private int rankingType = RankingListFragment.RANKING_TYPE_CHARM;
    private int[] dataTypes = new int[]{0, 0, 0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking_list);
        ButterKnife.bind(this);
        setSwipeBackEnable(false);
        initView();
    }

    private void initView() {
        MagicIndicator mIndicator = (MagicIndicator) findViewById(R.id.magic_indicator);
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        TabRadioGroup tabRadioGroup = (TabRadioGroup) findViewById(R.id.rg_tab);
        ImageView tabBg = (ImageView) findViewById(R.id.ranking_list_tab_bg);
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(1, getString(R.string.ranking_list_for_charm)));
        tabInfoList.add(new TabInfo(2, getString(R.string.ranking_list_for_tycoon)));
        tabInfoList.add(new TabInfo(3, getString(R.string.ranking_list_for_cp)));

        RankingListMagicIndicatorAdapter indicatorAdapter = new RankingListMagicIndicatorAdapter(this, tabInfoList);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(indicatorAdapter);
        mIndicator.setNavigator(commonNavigator);
        // must after setNavigator
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);

        List<Fragment> mTabs = new ArrayList<>();
        mTabs.add(RankingListFragment.newInstance(RankingListFragment.RANKING_TYPE_CHARM));
        mTabs.add(RankingListFragment.newInstance(RANKING_TYPE_TYCOON));
        mTabs.add(RankingListFragment.newInstance(RANKING_TYPE_CP));
        viewPager.setAdapter(new BaseIndicatorAdapter(getSupportFragmentManager(), mTabs));
        viewPager.setOffscreenPageLimit(2);
        ViewPagerHelper.bind(mIndicator, viewPager);

        List<String> tabTextList = new ArrayList<>();
        tabTextList.add(getResources().getString(R.string.ranking_list_for_day));
        tabTextList.add(getResources().getString(R.string.ranking_list_for_week));
        tabTextList.add(getResources().getString(R.string.ranking_list_for_all));
        List<String> cpTabTextList = new ArrayList<>();
        cpTabTextList.add(getResources().getString(R.string.ranking_list_for_week));
        cpTabTextList.add(getResources().getString(R.string.ranking_list_for_all));
        indicatorAdapter.setOnItemSelectListener(new RankingListMagicIndicatorAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                rankingType = position;
                viewPager.setCurrentItem(position);

                switch (rankingType) {
                    case RANKING_TYPE_CHARM:
                        tabBg.setImageResource(R.drawable.ranking_list_charm_bg_top);
                        tabRadioGroup.setupTabList(tabTextList, ContextCompat.getColorStateList(RankingListActivity.this, R.color.ranking_list_charm_selector));
                        break;
                    case RANKING_TYPE_TYCOON:
                        tabBg.setImageResource(R.drawable.ranking_list_tycoon_bg_top);
                        tabRadioGroup.setupTabList(tabTextList, ContextCompat.getColorStateList(RankingListActivity.this, R.color.ranking_list_tycoon_selector));
                        break;
                    case RANKING_TYPE_CP:
                        tabBg.setImageResource(R.drawable.ranking_list_cp_bg_top);
                        tabRadioGroup.setupTabList(cpTabTextList, ContextCompat.getColorStateList(RankingListActivity.this, R.color.ranking_list_cp_selector));
                        break;
                }
                tabRadioGroup.checkPosition(dataTypes[rankingType]);
            }
        });

        tabRadioGroup.checkPosition(0);//默认选中第一个 日榜
        tabRadioGroup.setOnCheckedChangePositionListener((radioButton, position) -> {
            CoreUtils.send(new TabRadioGroupEvent.OnTabClick(rankingType, position));
            dataTypes[rankingType] = position;
        });
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.anim_bottom_in, R.anim.anim_bottom_out);
    }

    @OnClick(R2.id.ranking_list_close)
    public void onCloseClicked() {
        finish();
    }
}
