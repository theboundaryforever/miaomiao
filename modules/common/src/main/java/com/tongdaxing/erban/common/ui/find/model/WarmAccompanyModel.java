package com.tongdaxing.erban.common.ui.find.model;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class WarmAccompanyModel extends BaseMvpModel {
    public void loadWarmAccompanyData(int page, int gender, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("pageNum", String.valueOf(page));
        params.put("pageSize", String.valueOf(Constants.PAGE_HOME_HOT_SIZE));
        params.put("gender", String.valueOf(gender));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getWarmRoomList(), params, myCallBack);
    }

    public void getLoverMatch(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getLoverMatch(), params, myCallBack);
    }

    /**
     * @param tagId
     * @param type
     * @param fansNoticeOption 0关1开
     * @param myCallBack
     */
    public void getReopenRoom(int tagId, int type, boolean fansNoticeOption, String title, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        if (tagId == -1) {
            params.put("tagId", "");
        } else {
            params.put("tagId", String.valueOf(tagId));
        }
        params.put("tagType", String.valueOf(type));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("fansNoticeOption", fansNoticeOption ? "1" : "0");
        params.put("title", title);
        OkHttpManager.getInstance().doPostRequest(UriProvider.getReopenRoom(), params, myCallBack);
    }
}
