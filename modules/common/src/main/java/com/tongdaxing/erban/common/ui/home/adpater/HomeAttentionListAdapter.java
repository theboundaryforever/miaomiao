package com.tongdaxing.erban.common.ui.home.adpater;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

/**
 * <p> 首页顶部 关注 列表 </p>
 */
public class HomeAttentionListAdapter extends BaseQuickAdapter<AttentionInfo, BaseViewHolder> {

    public HomeAttentionListAdapter(int layoutResId, List<AttentionInfo> attentionInfoList) {
        super(layoutResId, attentionInfoList);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final AttentionInfo attentionInfo) {
        if (attentionInfo == null) return;
        if (attentionInfo.getUid() == -1) {//第一个按钮：关注人
            baseViewHolder.setText(R.id.nickname, "关注的人")
                    .setVisible(R.id.online, false)
                    .setVisible(R.id.avatar, false);
            ImageView attentionBg = baseViewHolder.getView(R.id.attention_bg);
            attentionBg.setImageResource(R.mipmap.bg_home_attention_number);
            TextView attentionNumberTv = baseViewHolder.getView(R.id.attention_number_tv);
            attentionNumberTv.setVisibility(View.VISIBLE);
            UserInfo currUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            long attentionNum = currUserInfo == null ? 0 : currUserInfo.getFollowNum();
            attentionNumberTv.setText(String.valueOf(attentionNum));
        } else {
            baseViewHolder.setText(R.id.nickname, attentionInfo.getNick())
                    .setVisible(R.id.online, attentionInfo.getOperatorStatus() != 2);
            ImageView avatar = baseViewHolder.getView(R.id.avatar);
            avatar.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadCircleImage(mContext, attentionInfo.getAvatar(), avatar, R.drawable.ic_no_avatar);

            ImageView attentionBg = baseViewHolder.getView(R.id.attention_bg);
            attentionBg.setImageResource(R.mipmap.bg_home_attention);
            TextView attentionNumberTv = baseViewHolder.getView(R.id.attention_number_tv);
            attentionNumberTv.setVisibility(View.GONE);
        }
    }
}
