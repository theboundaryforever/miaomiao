package com.tongdaxing.erban.common.ui.widget;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

/**
 * <p> 底部tab导航 </p>
 * Created by Administrator on 2017/11/14.
 */
public class MainTabLayout extends RelativeLayout implements View.OnClickListener {

    private final String OPEN_FILE = "open_file";
    private final String OPEN_VALUE = "open_value";
    private MainTab mHomeTab, mAttentionTab, mStarBallTab, mMeTab;
    //    private View mAudioMatchTab;
    private MainRedPointTab mMsgTab;
    private int mLastPosition = -1;
    private OnTabClickListener mOnTabClickListener;

    public MainTabLayout(Context context) {
        this(context, null);
    }


    public MainTabLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainTabLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        inflate(context, R.layout.main_tab_layout, this);

        mHomeTab = (MainTab) findViewById(R.id.main_home_tab);
        mAttentionTab = (MainTab) findViewById(R.id.main_attention_tab);
        mMsgTab = (MainRedPointTab) findViewById(R.id.main_msg_tab);
        mMeTab = (MainTab) findViewById(R.id.main_me_tab);
//        mAudioMatchTab = findViewById(R.id.main_audio_match_tab);
        mStarBallTab = (MainTab) findViewById(R.id.main_star_ball_tab);

        mStarBallTab.setOnClickListener(this);
        mHomeTab.setOnClickListener(this);
        mAttentionTab.setOnClickListener(this);
        mMsgTab.setOnClickListener(this);
        mMeTab.setOnClickListener(this);
//        mAudioMatchTab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.main_home_tab) {
            select(0);

        } else if (i == R.id.main_attention_tab) {
            select(1);

        } else if (i == R.id.main_star_ball_tab) {
            select(2);

        } else if (i == R.id.main_msg_tab) {
            select(3);

        } else if (i == R.id.main_me_tab) {
            select(4);

//            case R.id.main_audio_match_tab:
//                MobclickAgent.onEvent(getContext(), UmengEventId.getMatch());//增加友盟统计
//                select(4);//主要是调用onTabClick方法
//                break;
        }
    }

    private void saveSharedPreferences(boolean isOpen) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(OPEN_FILE, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(OPEN_VALUE, isOpen);
        editor.apply();
    }

    private boolean readSharedPreferences() {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(OPEN_FILE, Activity.MODE_PRIVATE);
        return sharedPreferences.getBoolean(OPEN_VALUE, false);
    }

    public void setMsgNum(int number) {
        mMsgTab.setNumber(number);
    }

    public void select(int position) {
        LogUtil.d("MainTabLayout select " + position);
        if (mLastPosition == position) return;
        mHomeTab.select(position == 0);
        mAttentionTab.select(position == 1);
        mStarBallTab.select(position == 2);
        mMsgTab.select(position == 3);
        mMeTab.select(position == 4);

        if (mOnTabClickListener != null) {
            mOnTabClickListener.onTabClick(position);
        }
        mLastPosition = position;
    }

    public void setOnTabClickListener(OnTabClickListener onTabClickListener) {
        mOnTabClickListener = onTabClickListener;
    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);

    }

    public interface OnTabClickListener {
        void onTabClick(int position);
    }
}
