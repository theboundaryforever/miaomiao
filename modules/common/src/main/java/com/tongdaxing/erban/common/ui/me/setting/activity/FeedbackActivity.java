package com.tongdaxing.erban.common.ui.me.setting.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.IHomeCore;
import com.tongdaxing.xchat_core.home.IHomeCoreClient;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

public class FeedbackActivity extends BaseActivity {
    private EditText edtContent;
    private EditText edtContact;
    private TitleBar titleBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initTitleBar("我要反馈");
        initView();
        initData();
        SetListener();
    }

    private void SetListener() {
        titleBar.setActionTextColor(getResources().getColor(R.color.text_1A1A1A));
        titleBar.addAction(new TitleBar.TextAction("提交") {
            @Override
            public void performAction(View view) {
                if (edtContent.getText().toString().length() == 0 || edtContact.getText().toString().length() == 0) {
                    toast("反馈内容和联系方式不能为空哦。");
                    return;
                }
                getDialogManager().showProgressDialog(FeedbackActivity.this, "正在提交，请稍后...");
                CoreManager.getCore(IHomeCore.class).commitFeedback(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                        edtContent.getText().toString(), edtContact.getText().toString()
                );
            }
        });
    }

    private void initData() {

    }

    private void initView() {
        edtContent = (EditText) findViewById(R.id.edt_content);
        edtContact = (EditText) findViewById(R.id.edt_contact);
        titleBar = (TitleBar) findViewById(R.id.title_bar);
    }

    @CoreEvent(coreClientClass = IHomeCoreClient.class)
    public void onCommitFeedback() {
        getDialogManager().dismissDialog();
        toast("提交成功");
        finish();

    }

    @CoreEvent(coreClientClass = IHomeCoreClient.class)
    public void onCommitFeedbackFail(String error) {
        getDialogManager().dismissDialog();
        toast("提交失败，请稍后再试");
    }
}
