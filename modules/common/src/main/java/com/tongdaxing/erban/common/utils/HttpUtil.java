package com.tongdaxing.erban.common.utils;

import android.content.Context;

import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.room.chat.RoomPrivateChatEvent;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.Map;

public class HttpUtil {


    /**
     * 保存关注消息发送状态
     *
     * @param uid
     * @param likedSend
     */
    public static void saveFocusMsgState(long uid, int likedSend) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid + "");
        params.put("likedSend", likedSend + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.saveFocusMsgSwitch(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    SingleToastUtil.showToast(likedSend == 1 ? "已开启消息通知" : "已关闭消息通知");
                } else {
                    SingleToastUtil.showToast(response != null ? response.str("message") : "数据异常");
                }
            }
        });
    }

    /**
     * 验证用户是否可以和对方进行私聊
     *
     * @param context
     * @param userId
     */
    public static void checkUserIsDisturb(Context context, long userId) {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo == null) {
            SingleToastUtil.showToast("进入私聊失败！");
            return;
        }
        if (userInfo.getCreateTime() <= 0) {
            UIHelper.showAddInfoAct(context);
            return;
        }
        if (userInfo.getUid() == userId) {
            return;//不可以跟自己私聊
        }
        checkUserIsDisturb(context, CoreManager.getCore(IIMFriendCore.class).isMyFriend(userId + ""), userId, false);
    }

    /**
     * 验证用户是否可以和对方进行私聊
     *
     * @param context
     * @param isFriend
     * @param userId
     */
    public static void checkUserIsDisturb(Context context, boolean isFriend, long userId, boolean cpInvite) {
        if (context == null) {
            return;
        }
        if (isFriend) {
            SessionHelper.startPrivateChat(context, userId, cpInvite);
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", userId + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getFocusMsgSwitch(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast("进入私聊失败！");
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    Json json = response.json("data");
                    if (json != null) {
                        int chatPermission = json.num("chatPermission");
                        if (chatPermission == 1) {
                            SingleToastUtil.showToast("对方已设置私信免打扰");
                            return;
                        }
                        if (chatPermission == 2) {
                            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                            if (userInfo == null || userInfo.getExperLevel() < 10) {
                                SingleToastUtil.showToast("对方已设置私信免打扰");
                                return;
                            }
                        }
                        SessionHelper.startPrivateChat(context, userId, cpInvite);
                    } else {
                        SingleToastUtil.showToast("进入私聊失败！");
                    }
                } else {
                    SingleToastUtil.showToast("进入私聊失败！");
                }
            }
        });
    }

    /**
     * 检查房间内用户是否可以私聊
     *
     * @param userId
     */
    public static void checkUserIsDisturbInRoom(String userId) {
        boolean isFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(userId);
        if (isFriend) {
            CoreUtils.send(new RoomPrivateChatEvent.OnSuccess(userId));
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", userId + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getFocusMsgSwitch(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast("进入私聊失败！");
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    Json json = response.json("data");
                    if (json != null) {
                        int chatPermission = json.num("chatPermission");
                        if (chatPermission == 1) {
                            SingleToastUtil.showToast("对方已设置私信免打扰");
                            return;
                        }
                        if (chatPermission == 2) {
                            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                            if (userInfo == null || userInfo.getExperLevel() < 10) {
                                SingleToastUtil.showToast("对方已设置私信免打扰");
                                return;
                            }
                        }
                        CoreUtils.send(new RoomPrivateChatEvent.OnSuccess(userId));
                    } else {
                        SingleToastUtil.showToast("进入私聊失败！");
                    }
                } else {
                    SingleToastUtil.showToast("进入私聊失败！");
                }
            }
        });
    }
}
