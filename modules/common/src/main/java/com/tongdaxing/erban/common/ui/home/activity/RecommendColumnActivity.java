package com.tongdaxing.erban.common.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;

import com.juxiao.library_ui.widget.AppToolBar;
import com.juxiao.library_ui.widget.NestedScrollSwipeRefreshLayout;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.home.adpater.RecommendColumnAdapter;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.GridSpaceItemDecoration;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.RecommendColumnInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Function:
 * Author: Edward on 2019/3/26
 */
public class RecommendColumnActivity extends BaseActivity {
    protected int currPage = Constants.PAGE_START;
    @BindView(R2.id.swipe_refresh)
    NestedScrollSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R2.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R2.id.app_tool_bar)
    AppToolBar appToolBar;
    private RecommendColumnAdapter adapter;
    private long id;

    public static void start(Context context, long id) {
        Intent intent = new Intent(context, RecommendColumnActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }

    private void setAppTitleBar() {
        appToolBar.setOnLeftImgBtnClickListener(this::finish);
        appToolBar.setTitle("");
        TextPaint tp = appToolBar.getTvTitle().getPaint();
        tp.setFakeBoldText(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        ButterKnife.bind(this);
        setAppTitleBar();
        appToolBar.setTitle("推荐栏目");
        id = getIntent().getLongExtra("id", 0);
        if (id <= 0) {
            showNoData();
            return;
        }

        adapter = new RecommendColumnAdapter(R.layout.adapter_custom_column_more_item);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        int itemMargin = getResources().getDimensionPixelSize(R.dimen.space_medium);//每一行间距
        GridSpaceItemDecoration decoration = new GridSpaceItemDecoration(0, itemMargin);
        decoration.setupBindAdapter(adapter);
        recyclerView.addItemDecoration(decoration, 0);
        recyclerView.setAdapter(adapter);

        mSwipeRefreshLayout.setRefreshing(true);
        showLoading();
        refreshData();
        setCallback();
    }

    public void setupFailView(boolean isRefresh) {
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            showNoData();
        } else {
            adapter.loadMoreFail();
        }
    }

    public void setupSuccessView(List<RecommendColumnInfo> listBeanList, boolean isRefresh) {
        hideStatus();
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            if (ListUtils.isListEmpty(listBeanList)) {
                showNoData();
                listBeanList = new ArrayList<>();
            }
            adapter.setNewData(listBeanList);
        } else {
            adapter.loadMoreComplete();
            if (!ListUtils.isListEmpty(listBeanList)) {
                adapter.addData(listBeanList);
            }
        }
        //不够10个的话，不显示加载更多
        if (listBeanList.size() < Constants.PAGE_SIZE) {
            adapter.setEnableLoadMore(false);
        }
    }

    public void refreshData() {
        loadData(Constants.PAGE_START, id);
    }

    public void loadMoreData() {
        loadData(currPage + 1, id);
    }

    private void setCallback() {
        mSwipeRefreshLayout.setOnRefreshListener(this::refreshData);
        adapter.setOnLoadMoreListener(this::loadMoreData, recyclerView);
        adapter.setOnItemClickListener((adapter, view, position) -> {
            RecommendColumnAdapter ad = (RecommendColumnAdapter) adapter;
            RecommendColumnInfo bean = ad.getItem(position);
            if (bean != null && bean.getUid() > 0) {
                AVRoomActivity.start(this, bean.getUid());
            } else {
                SingleToastUtil.showToast("数据错误!");
            }
        });
    }

    protected void loadData(int page, long columnId) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("pageNum", String.valueOf(page));
        params.put("columnId", String.valueOf(columnId));
        params.put("pageSize", String.valueOf(Constants.PAGE_HOME_HOT_SIZE));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getRecommendColumn(), params, new OkHttpManager.MyCallBack<ServiceResult<List<RecommendColumnInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    setupFailView(page == Constants.PAGE_START);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<RecommendColumnInfo>> response) {
                currPage = page;
                if (response != null && response.isSuccess()) {
                    setupSuccessView(response.getData(), page == Constants.PAGE_START);
                } else {
                    onError(new Exception());
                }
            }
        });
    }
}
