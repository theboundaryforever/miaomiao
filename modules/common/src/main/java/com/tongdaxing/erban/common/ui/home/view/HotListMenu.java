package com.tongdaxing.erban.common.ui.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.home.activity.SortListActivity;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.List;

/**
 * Created by Administrator on 2018/4/19.
 */

public class HotListMenu extends RelativeLayout {

    IOnScrollUp iOnScrollUp;
    long scrollTime = 0;
    private View inflate;
    private Scroller scroller;
    private TagFlowLayout flowlayout;
    private List<Json> data;
    private TagAdapter<Json> adapter;
    private LayoutInflater mInflater;

    public HotListMenu(Context context) {
        this(context, null);
    }

    public HotListMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate = LayoutInflater.from(context).inflate(R.layout.menu_hot_list, this);
        scroller = new Scroller(context);
        flowlayout = (TagFlowLayout) inflate.findViewById(R.id.flowlayout_menu);
        mInflater = LayoutInflater.from(context);
    }

    public List<Json> getData() {
        return data;
    }

    public void setData(List<Json> data) {
        this.data = data;
        setFlowlayoutData(data);
    }

    public TagAdapter<Json> getAdapter() {
        return adapter;
    }

    public TagFlowLayout getFlowlayout() {
        return flowlayout;
    }

    public void setFlowlayout(TagFlowLayout flowlayout) {
        this.flowlayout = flowlayout;
    }

    public void setFlowlayoutData(List<Json> flowlayoutData) {

        adapter = new TagAdapter<Json>(flowlayoutData) {
            @Override
            public View getView(FlowLayout parent, int position, Json s) {
                TextView tv = (TextView) mInflater.inflate(R.layout.tv_flowlayout_menu,
                        flowlayout, false);
                tv.setText(s.str("name"));

                return tv;
            }

        };
        flowlayout.setAdapter(adapter);
        flowlayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {

                SortListActivity.start(getContext(), position);
                return false;
            }
        });

    }

    @Override
    public void computeScroll() {

        super.computeScroll();
        if (scroller.computeScrollOffset()) {
            ((View) getParent()).scrollTo(scroller.getCurrX(), scroller.getCurrY());
            //通过不断的重绘不断的调用computeScroll方法
            invalidate();
        }
    }

    public boolean smoothScrollUp() {
        if (System.currentTimeMillis() - scrollTime < 500) {
            return false;
        }
        int destY = getMeasuredHeight();
        int scrollY = getScrollY();
        int delta = destY - scrollY;
        //1000秒内滑向destX
        scroller.startScroll(0, scrollY, 0, delta, 500);
        scrollTime = System.currentTimeMillis();

        invalidate();
        if (iOnScrollUp != null)
            iOnScrollUp.ScrollUpFinish();
        return true;
    }

    public void setiOnScrollUp(IOnScrollUp iOnScrollUp) {
        this.iOnScrollUp = iOnScrollUp;
    }

    public boolean smoothScrollDown() {


        if (System.currentTimeMillis() - scrollTime < 500) {
            return false;
        }

        int destY = getMeasuredHeight();
        int scrollY = getScrollY();
        int delta = destY - scrollY;

        //1000秒内滑向destX
        scroller.startScroll(0, delta, 0, -delta, 500);
        scrollTime = System.currentTimeMillis();
        invalidate();
        return true;
    }


    public void smoothScrollTo(int destX, int destY) {
        int scrollY = getScrollY();
        int delta = destX - scrollY;
        //1000秒内滑向destX
        scroller.startScroll(0, scrollY, 0, delta, 2000);
        invalidate();
    }

    public interface IOnScrollUp {
        void ScrollUpFinish();
    }
}
