package com.tongdaxing.erban.common.ui.me.shopping.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/3/30.
 */

public class CarAdapter extends BaseQuickAdapter<Json, CarAdapter.ViewHolder> {

    private final Context context;
    //判断是否有选择过
    public String selectId = "";
    public boolean isShowSvga = true;
    public String prizeParamasName = "";
    private int selectIndex = -1;
    private VggAction vggAction;
    private PayAction payAction;

    public CarAdapter(Context context) {
        super(R.layout.item_car_select);
        this.context = context;
    }

    public void setVggAction(VggAction vggAction) {
        this.vggAction = vggAction;
    }

    public void setPayAction(PayAction payAction) {
        this.payAction = payAction;
    }

    @Override
    public void setNewData(@Nullable List<Json> data) {
        super.setNewData(data);
        for (int i = 0; i < data.size(); i++) {
            Json json = data.get(i);
            if (json.num("isPurse") == 2) {
                selectIndex = i;
                break;
            }
        }
        selectId = "";
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        Json item = getData().get(position);

        int isPurse = item.num("isPurse", 0);
        String vggUrl = item.str("vggUrl");

        holder.tvCarType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPurse == 0) {

                    if (vggAction != null) {
                        vggAction.showVgg(vggUrl);
                    }
                } else if (isPurse == 1) {
                    selectId = item.str(prizeParamasName + "Id");
                    if (selectIndex > -1)
                        getData().get(selectIndex).set("isPurse", 1);
                    selectIndex = position;
                    item.set("isPurse", 2);

                } else if (isPurse == 2) {
                    item.set("isPurse", 1);
                    selectId = "-1";

                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void convert(ViewHolder helper, Json item) {

        String carName = item.str(prizeParamasName + "Name");
        helper.tvCarName.setText(carName);

        ImageLoadUtils.loadImage(context, item.str("picUrl"), helper.ivCarStyle);
        //0未购买，1购买未选中，2选中
        int isPurse = item.num("isPurse", 0);
        helper.tvCarTime.setText(isPurse == 0 ? item.str("effectiveTime") + "天" : item.str("daysRemaining") + "天");
        String vggUrl = item.str("vggUrl");

        helper.ivCarStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPurse != 0) {
                    if (vggAction != null) {
                        if (isShowSvga) {
                            vggAction.showVgg(vggUrl);
                        }

                    }
                }
            }
        });

        if (item.str("goldPrice").equals("0")) {
            helper.tvCarTime.setVisibility(View.GONE);
            helper.buCarBuy.setVisibility(View.GONE);
            helper.tvCarPrice.setVisibility(View.GONE);
        } else {
            helper.tvCarTime.setVisibility(View.VISIBLE);
            helper.buCarBuy.setVisibility(View.VISIBLE);
            helper.tvCarPrice.setVisibility(View.VISIBLE);


        }

        String carId = item.str(prizeParamasName + "Id");
        helper.buCarBuy.setBackgroundResource(R.drawable.shape_car_pay);
        if (isPurse == 0) {
            if (isShowSvga) {
                helper.tvCarType.setVisibility(View.VISIBLE);
                setDrawableInTv(helper.tvCarType, 0, " 点击试驾", Color.parseColor("#23CFB1"));
            } else {
                helper.tvCarType.setVisibility(View.GONE);
            }


        } else if (isPurse == 1) {
            helper.tvCarType.setVisibility(View.VISIBLE);
            setDrawableInTv(helper.tvCarType, R.mipmap.car_option_0, " 点击使用", Color.parseColor("#999999"));
        } else if (isPurse == 2) {
            helper.tvCarType.setVisibility(View.VISIBLE);
            setDrawableInTv(helper.tvCarType, R.mipmap.car_option_1, " 正在使用", Color.parseColor("#999999"));
        }


        helper.buCarBuy.setText(isPurse == 0 ? "购买" : "继续购买");

        helper.tvCarPrice.setText(item.str("goldPrice"));


        helper.buCarBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (payAction == null)
                    return;
                if (isPurse == 0) {
                    payAction.buyCar(1, carId, carName);
                } else if (isPurse == 1 || isPurse == 2) {
                    payAction.buyCar(2, carId, carName);
                }
            }
        });

//        helper.buCarBuy.setText(isPurse == 0 ? "购买" : "续期");

    }

    private void setDrawableInTv(TextView textView, int id, String content, int color) {
        Drawable drawable = null;
        if (id != 0) {
            drawable = mContext.getResources().getDrawable(id);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }

        textView.setCompoundDrawables(drawable, null, null, null);
        textView.setText(content);
        textView.setTextColor(color);
    }

    public interface VggAction {
        void showVgg(String url);
    }

    public interface PayAction {
        //type 1 是购买，2是续费
        void buyCar(int type, String carId, String carName);
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tv_car_time)
        TextView tvCarTime;
        @BindView(R2.id.tv_car_type)
        TextView tvCarType;
        @BindView(R2.id.iv_car_style)
        ImageView ivCarStyle;
        @BindView(R2.id.tv_car_price)
        TextView tvCarPrice;
        @BindView(R2.id.tv_car_name)
        TextView tvCarName;
        @BindView(R2.id.bu_car_buy)
        Button buCarBuy;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
