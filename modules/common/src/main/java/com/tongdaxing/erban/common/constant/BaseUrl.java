package com.tongdaxing.erban.common.constant;

import com.tongdaxing.xchat_core.UriProvider;

public class BaseUrl {
//    public static final boolean isDebug = BuildConfig.IS_DEBUG;

    //声网appid
//    public static final String AGORA_APPID = "314a1f874912455baeb1ad5ff838664a";//甜甜
    public static final String AGORA_APPID = "9fec61b770264a498108d1518ddfee8e";//喵喵

    //云信appkey 喵喵
    public static final String NIM_APPKEY = "801631bce2ff7bb299f7edde56bbbd1a";
    public static final String APP_SECRET = "3ba6bccf4e2d";
    //甜甜
//    public static final String nimAppKey = "e655919f13b02388594d43451b801560";
//    public static final String nimAppSecret = "1d6a1808087c";

    public static final String miaomiao_BASE_URL = "miaomiaofm";
    public static final String TIANTIAN_BASE_URL = "tian9k9";

    /**
     * app固定跳转H5地址
     */
//    public static final String BASE_APP_TAG = UriProvider.JAVA_WEB_URL + "";

    //用户协议
    public static final String USER_AGREEMENT = UriProvider.JAVA_WEB_URL + "/mm/agreement/agreement.html";
    public static final String USER_PRIVATE_AGREEMENT = UriProvider.JAVA_WEB_URL + "/mm/agreement/index.html";
    //常见问题
    public static final String COMMON_PROBLEM = UriProvider.JAVA_WEB_URL + "/mm/question/question.html";
    //我的等级
    public static final String MY_LEVEL = UriProvider.JAVA_WEB_URL + "/mm/level/index.html";
    //提现规则
    public static final String WITHDRAW_RULES = UriProvider.IM_SERVER_URL + "/modules/guide/withdraw.html";


    //邀请奖励排行榜
    public static final String INVITE_REWARD_RANK = UriProvider.JAVA_WEB_URL + "/mm/invitationRank/index.html";
    //我的邀请人数
    public static final String MY_INVITE_PEOPLE = UriProvider.IM_SERVER_URL + "/mm/invitation/index.html";
    //我的分成奖励
    public static final String MY_INVITE_PERCENTAGE = UriProvider.IM_SERVER_URL + "/mm/percentage/index.html";
    //我的邀请规则说明
    public static final String MY_INVITE_RULES = UriProvider.IM_SERVER_URL + "/mm/method/method.html";
    //房间土豪榜
    public static final String ROOM_RANK = UriProvider.JAVA_WEB_URL + "/mm/rank/index.html";

    //分享模块的下载页地址
    public static final String SHARE_DOWNLOAD = UriProvider.JAVA_WEB_URL + "/mm/download/download.html";
    //分享默认logo图
    public static final String SHARE_DEFAULT_LOGO = UriProvider.JAVA_WEB_URL + "/home/images/logo.png";

    //首页推荐人气厅列表 -- 我要上推荐
    public static final String HOME_UP_RECOMMEND = UriProvider.IM_SERVER_URL + "/mm/hotroom/roomType.html";

}
