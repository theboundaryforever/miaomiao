package com.tongdaxing.erban.common.room.user;

import com.tongdaxing.xchat_core.user.bean.UserInfo;

/**
 * Created by Chen on 2019/6/12.
 */
public interface IUserInfoDialogView {
    void attentionSuccess(long uid, boolean attention);

    void updateView(UserInfo userInfo);

    void showIntoRoomImage();

    void enterRoom(long roomId);

    void onP2PCpRemove();

    void onP2PCpInviteAgree();
}
