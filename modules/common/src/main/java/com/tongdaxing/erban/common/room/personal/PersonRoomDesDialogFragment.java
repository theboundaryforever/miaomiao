package com.tongdaxing.erban.common.room.personal;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.erban.ui.baseview.BaseDialogFragment;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.uuzuche.lib_zxing.DisplayUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chen on 2019/5/5.
 */
@Route(path = RoomPersonalRoute.ROOM_PERSONAL_DESC_PATH)
public class PersonRoomDesDialogFragment extends BaseDialogFragment {

    @BindView(R2.id.tv_title)
    TextView tvTitle;
    @BindView(R2.id.tv_content)
    TextView tvContent;
    @BindView(R2.id.room_iv_topic_close)
    ImageView mTopicClose;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null && getContext() != null) {
                int color = ContextCompat.getColor(getContext(), android.R.color.transparent);
                window.setBackgroundDrawable(new ColorDrawable(color));
                window.setLayout(DisplayUtil.dip2px(getContext(), 301.0f), DisplayUtil.dip2px(getContext(), 209.0f));
            }
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.dialog_person_room_desc;
    }

    @Override
    public void initBefore() {
    }

    @Override
    public void findView() {
        ButterKnife.bind(this, mContentView);
    }

    @Override
    public void setView() {
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo != null) {
            tvTitle.setText(TextUtils.isEmpty(mCurrentRoomInfo.getRoomDesc()) ? "暂无房间玩法" : mCurrentRoomInfo.getRoomDesc());
            tvContent.setText(TextUtils.isEmpty(mCurrentRoomInfo.getRoomNotice()) ? "暂无内容" : mCurrentRoomInfo.getRoomNotice());
            tvContent.setMovementMethod(ScrollingMovementMethod.getInstance());
        }
    }

    @Override
    public void setListener() {
    }

    @OnClick(R2.id.room_iv_topic_close)
    public void onCloseClicked() {
        this.dismissAllowingStateLoss();
    }
}
