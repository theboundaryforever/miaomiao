package com.tongdaxing.erban.common.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.media.AudioManager;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_framework.media.AudioPlayer;
import com.tongdaxing.xchat_framework.media.OnPlayListener;

/**
 * 音频播放小挂件
 */
public class AudioPlayView extends RelativeLayout {

    private SecondTimeView secondTimeView;
    private ImageView playIv;

    private AudioPlayer audioPlayer;

    private int audioLength;
    private String audioFileUrl;

    private boolean isInterceptClick;

    private int playIconResId, pauseIconResId;
    private OnPlayListener onPlayListener = new OnPlayListener() {
        @Override
        public void onPrepared() {
            isInterceptClick = false;
            //这里场景只有play才会prepare，所以可以写在这里
            setupAudioPlayView();
        }

        @Override
        public void onCompletion() {
            setupAudioStopView();
            isInterceptClick = false;
        }

        @Override
        public void onInterrupt() {
            setupAudioStopView();
            isInterceptClick = false;
        }

        @Override
        public void onError(String error) {
            setupAudioStopView();
            isInterceptClick = false;
        }

        @Override
        public void onPlaying(long curPosition) {
        }
    };

    public AudioPlayView(Context context) {
        super(context);
        init(context, null);
    }

    public AudioPlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public AudioPlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AudioPlayView);
        int themeType = typedArray.getInteger(R.styleable.AudioPlayView_apv_theme_type, 0);//风格，0为白色，1为透明，2为橙色风格，暂时支持这三种
        typedArray.recycle();
        if (themeType == 1) {//透明风格
            inflate(context, R.layout.layout_audio_play_transparent, this);
            playIconResId = R.drawable.ic_audio_card_play_small;
            pauseIconResId = R.drawable.ic_audio_card_pause_small;
        } else if (themeType == 2) {//橙色风格
            inflate(context, R.layout.layout_audio_play_orange, this);
            playIconResId = R.mipmap.ic_audio_card_play_small_orange;
            pauseIconResId = R.mipmap.ic_audio_card_pause_small_orange;
        } else {//白色风格
            inflate(context, R.layout.layout_audio_play_white, this);
            playIconResId = R.drawable.ic_audio_card_play_small;
            pauseIconResId = R.drawable.ic_audio_card_pause_small;
        }
        secondTimeView = findViewById(R.id.second_time);
        playIv = findViewById(R.id.iv_play);
        setOnClickListener(view -> {
            if (isInterceptClick) {//正在准备播放音频的时候，不允许点击（避免还没开始就又stop了）
                return;
            }
            if (!audioPlayer.isPlaying()) {
                start();
            } else {
                stop();
            }
        });
        audioPlayer = AudioPlayAndRecordManager.getInstance().createAudioPlayer(context.getApplicationContext(), onPlayListener);
    }

    public void setAudioDataSource(int audioLength, String audioFileUrl) {
        this.audioLength = audioLength;
        this.audioFileUrl = audioFileUrl;
        secondTimeView.setCurrCount(audioLength);
        audioPlayer.setDataSource(audioFileUrl);
    }

    public void start() {
        if (audioFileUrl != null) {
            isInterceptClick = true;
            audioPlayer.start(AudioManager.STREAM_MUSIC);
        }
    }

    public void stop() {
        if (audioPlayer.isPlaying()) {
            audioPlayer.stop();
        }
    }

    /**
     * 释放资源（放onDestroy前面，否则有泄漏风险）
     */
    public void release() {
        AudioPlayAndRecordManager.getInstance().releaseAudioPlayer(audioPlayer);
        onPlayListener = null;
    }

    private void setupAudioPlayView() {
        playIv.setImageResource(pauseIconResId);
        secondTimeView.startCount(audioLength, 0);
    }

    private void setupAudioStopView() {
        playIv.setImageResource(playIconResId);
        secondTimeView.stopCount();
        secondTimeView.setCurrCount(audioLength);
    }
}
