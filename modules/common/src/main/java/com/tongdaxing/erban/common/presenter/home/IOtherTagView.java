package com.tongdaxing.erban.common.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.HomeRoom;

import java.util.List;

public interface IOtherTagView extends IMvpBaseView {

    void setRefreshing(boolean refreshing);

    void setupListView(boolean isRefresh, List<HomeRoom> homeRooms, HomeRoom homeRoom, int bannerPosition);

    void setupFailView(boolean isRefresh);
}

