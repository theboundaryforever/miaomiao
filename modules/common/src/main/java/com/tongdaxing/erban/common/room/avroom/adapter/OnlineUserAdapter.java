package com.tongdaxing.erban.common.room.avroom.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.uinfo.constant.GenderEnum;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * <p> 房间在线人数列表 （上麦，房主，游客，管理员）  </p>
 *
 * @author Administrator
 * @date 2017/12/4
 */
public class OnlineUserAdapter extends BaseQuickAdapter<OnlineChatMember, BaseViewHolder> {

    private boolean mIsHomeParty;
    private Disposable mDisposable;
    private OnRoomOnlineNumberChangeListener mListener;

    public OnlineUserAdapter(boolean isHomeParty) {
        super(R.layout.list_item_online_user);
        mIsHomeParty = isHomeParty;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        registerRoomEvent();
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, OnlineChatMember onlineChatMember) {

        if (onlineChatMember != null && onlineChatMember.chatRoomMember != null) {
            final ImageView sexImage = baseViewHolder.getView(R.id.sex);

            NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(onlineChatMember.chatRoomMember.getAccount());
            if (nimUserInfo == null) {
                NimUserInfoCache.getInstance().getUserInfoFromRemote(onlineChatMember.chatRoomMember.getAccount(),
                        new RequestCallbackWrapper<NimUserInfo>() {
                            @Override
                            public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                                if (nimUserInfo != null) {
                                    if (nimUserInfo.getGenderEnum() == GenderEnum.MALE) {
                                        sexImage.setVisibility(View.VISIBLE);
                                        sexImage.setImageResource(R.drawable.room_user_man);
                                    } else if (nimUserInfo.getGenderEnum() == GenderEnum.FEMALE) {
                                        sexImage.setVisibility(View.VISIBLE);
                                        sexImage.setImageResource(R.drawable.room_user_girl);
                                    } else {
                                        sexImage.setVisibility(View.GONE);
                                    }
                                }
                            }
                        });
            } else {
                if (nimUserInfo.getGenderEnum() == GenderEnum.MALE) {
                    sexImage.setVisibility(View.VISIBLE);
                    sexImage.setImageResource(R.drawable.room_user_man);
                } else if (nimUserInfo.getGenderEnum() == GenderEnum.FEMALE) {
                    sexImage.setVisibility(View.VISIBLE);
                    sexImage.setImageResource(R.drawable.room_user_girl);
                } else {
                    sexImage.setVisibility(View.GONE);
                }
            }

            ImageView roomOwnnerTag = baseViewHolder.getView(R.id.room_owner_logo);
            ImageView managerLogo = baseViewHolder.getView(R.id.manager_logo);
            if (onlineChatMember.isOnMic) {
                baseViewHolder.setGone(R.id.manager_logo, onlineChatMember.isAdmin || onlineChatMember.isRoomOwer);
                managerLogo.setImageResource(onlineChatMember.isAdmin ? R.drawable.icon_admin_logo
                        : R.drawable.icon_user_list_room_owner);
                if (!onlineChatMember.isRoomOwer) {
                    roomOwnnerTag.setImageResource(R.drawable.icon_user_list_up_mic);
                    roomOwnnerTag.setVisibility(View.VISIBLE);
                } else {
                    roomOwnnerTag.setVisibility(View.GONE);
                }
            } else {
                baseViewHolder.setGone(R.id.manager_logo, false);
                roomOwnnerTag.setVisibility((onlineChatMember.isAdmin || onlineChatMember.isRoomOwer)
                        ? View.VISIBLE : View.GONE);
                roomOwnnerTag.setImageResource(onlineChatMember.isAdmin ? R.drawable.icon_admin_logo
                        : R.drawable.icon_user_list_room_owner);
            }


            baseViewHolder.setText(R.id.nick, onlineChatMember.chatRoomMember.getNick());

            ImageView avatar = baseViewHolder.getView(R.id.avatar);
            ImageLoadUtils.loadAvatar(mContext, onlineChatMember.chatRoomMember.getAvatar(), avatar);

            View container = baseViewHolder.getView(R.id.container);
            int position = baseViewHolder.getLayoutPosition();

            TextView nick = baseViewHolder.getView(R.id.nick);
            if (mIsHomeParty) {
                nick.setTextColor(ContextCompat.getColor(mContext, R.color.color_1A1A1A));
//                container.setBackgroundColor(mChairPosition % 2 == 0 ? Color.parseColor("#05FFFFFF") : 0);
            } else {
                nick.setTextColor(ContextCompat.getColor(mContext, R.color.color_333333));
                container.setBackgroundResource(R.drawable.bg_common_touch_while);
            }
        }
    }

    private void registerRoomEvent() {
        mDisposable = IMNetEaseManager.get()
                .getChatRoomEventObservable()
                .subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        if (roomEvent == null) return;
                        int event = roomEvent.getEvent();
                        if (roomEvent.getEvent() == RoomEvent.ADD_BLACK_LIST ||
                                roomEvent.getEvent() == RoomEvent.DOWN_MIC ||
                                event == RoomEvent.ROOM_MEMBER_EXIT ||
                                roomEvent.getEvent() == RoomEvent.KICK_OUT_ROOM) {
                            if (roomEvent.getEvent() == RoomEvent.ADD_BLACK_LIST ||
                                    roomEvent.getEvent() == RoomEvent.KICK_OUT_ROOM) {
                                if (mListener != null
                                        && !AvRoomDataManager.get().isSelf(CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
                                    mListener.addMemberBlack();
                                    return;
                                }
                            }
                            if (ListUtils.isListEmpty(mData)) return;
                            if (mIsHomeParty && roomEvent.getEvent() == RoomEvent.DOWN_MIC) {
                                updateDownUpMic(roomEvent.getAccount(), false);
                                return;
                            }
                            ListIterator<OnlineChatMember> iterator = mData.listIterator();
                            for (; iterator.hasNext(); ) {
                                OnlineChatMember onlineChatMember = iterator.next();
                                if (onlineChatMember.chatRoomMember != null
                                        && Objects.equals(onlineChatMember.chatRoomMember.getAccount(), roomEvent.getAccount())) {
                                    iterator.remove();
                                }
                            }
                            notifyDataSetChanged();
                        } else if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_ADD
                                || roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_REMOVE) {
                            updateManager(roomEvent);
                        } else if (roomEvent.getEvent() == RoomEvent.UP_MIC) {
                            updateDownUpMic(roomEvent.getAccount(), true);
                        } else if (event == RoomEvent.ROOM_MEMBER_IN) {
                            updateMemberIn(roomEvent);
                        }
                    }
                });
    }

    private void updateMemberIn(RoomEvent roomEvent) {
        if (mListener != null) {
            mListener.onMemberIn(roomEvent.getAccount(), mData);
        }
    }

    private void updateManager(RoomEvent roomEvent) {
        if (mListener != null)
            mListener.onUpdateMemberManager(roomEvent.getAccount(),
                    roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_REMOVE, mData);
    }

    private void updateDownUpMic(String account, boolean isUpMic) {
        if (mListener != null) {
            mListener.onMemberDownUpMic(account, isUpMic, mData);
        }
    }

    public void release() {
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
    }

    public void setListener(OnRoomOnlineNumberChangeListener listener) {
        mListener = listener;
    }

    public interface OnRoomOnlineNumberChangeListener {
        /**
         * 成员进来回调
         *
         * @param account
         */
        void onMemberIn(String account, List<OnlineChatMember> dataList);

        /**
         * 成员上下麦更新
         *
         * @param account
         * @param isUpMic
         * @param dataList
         */
        void onMemberDownUpMic(String account, boolean isUpMic,
                               List<OnlineChatMember> dataList);

        /**
         * 设置管理员回调
         *
         * @param account
         * @param dataList
         */
        void onUpdateMemberManager(String account, boolean isRemoveManager,
                                   List<OnlineChatMember> dataList);

        void addMemberBlack();
    }
}
