package com.tongdaxing.erban.common.room.audio.presenter;

import com.tongdaxing.erban.common.room.audio.view.IMyMusicView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public class MyMusicPresenter extends AbstractMvpPresenter<IMyMusicView> {

    private List<MyMusicInfo> mMusicInfoList = new CopyOnWriteArrayList<>();

    public void getMySongList() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageSize", String.valueOf(200));//产品要求不刷新，直接拿200条数据
        params.put("pageNum", String.valueOf(1));
        OkHttpManager.getInstance().doGetRequest(UriProvider.getMySongList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<MyMusicInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    getMvpView().refreshMyMusicListFailure(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<MyMusicInfo>> response) {
                if (response != null && response.isSuccess() && response.getData() != null) {
                    mMusicInfoList.clear();
                    mMusicInfoList.addAll(response.getData());
                    getMvpView().refreshMyMusicListSucceed(mMusicInfoList);
                }
            }
        });
    }

    public MyMusicInfo getMusicInfo(int position) {
        if (position < mMusicInfoList.size()) {
            return mMusicInfoList.get(position);
        }
        return null;
    }
}
