package com.tongdaxing.erban.common.room.audio.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.juxiao.library_ui.widget.NestedScrollSwipeRefreshLayout;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.room.audio.adapter.MyMusicAdapter;
import com.tongdaxing.erban.common.room.audio.presenter.MyMusicPresenter;
import com.tongdaxing.erban.common.room.audio.view.IMusicView;
import com.tongdaxing.erban.common.room.audio.view.IMyMusicView;
import com.tongdaxing.erban.common.room.audio.widget.MusicDelDialog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.music.IMusicCore;
import com.tongdaxing.xchat_core.music.IMusicCoreClient;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.List;

import butterknife.BindView;

/**
 * Function:我的曲库
 * Author: Edward on 2019/2/13
 */
@CreatePresenter(MyMusicPresenter.class)
public class MyMusicFragment extends BaseMvpFragment<IMyMusicView, MyMusicPresenter> implements IMyMusicView {
    @BindView(R2.id.tv_music_count)
    TextView tvMusicCount;
    @BindView(R2.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R2.id.swipe_refresh)
    NestedScrollSwipeRefreshLayout swipeRefreshLayout;

    private MyMusicAdapter myMusicAdapter;
    private IMusicView iMusicView;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_my_music;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IMusicView) {
            iMusicView = (IMusicView) context;
        }
    }

    @Override
    public void onFindViews() {
        myMusicAdapter = new MyMusicAdapter(R.layout.adapter_my_music);
        recyclerView.setAdapter(myMusicAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onSetListener() {
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.mm_theme));
        swipeRefreshLayout.setOnRefreshListener(this::refreshData);
        myMusicAdapter.setOnItemLongClickListener((adapter, view, position) -> {
            MyMusicInfo myMusicInfo = myMusicAdapter.getItem(position);
            Bundle bundle = new Bundle();
            bundle.putParcelable("data", myMusicInfo);
            MusicDelDialog musicDelDialog = new MusicDelDialog();
            musicDelDialog.setArguments(bundle);
            musicDelDialog.show(getFragmentManager(), "");
            return false;
        });
        myMusicAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (iMusicView != null) {
                MyMusicInfo musicInfo = getMvpPresenter().getMusicInfo(position);
                if (musicInfo != null) {
                    iMusicView.onItemClick(musicInfo, view, position);
                } else {
                    toast(getString(R.string.data_exception));
                }
            }
        });
    }

    @Override
    public void initiate() {
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }

    public void refreshData() {
        showLoading();
        getMvpPresenter().getMySongList();
    }

    @Override
    public void refreshMyMusicListFailure(String error) {
        SingleToastUtil.showToast(error);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void refreshMyMusicListSucceed(List<MyMusicInfo> myMusicInfoList) {
        swipeRefreshLayout.setRefreshing(false);
        CoreManager.getCore(IMusicCore.class).setMusicStatus(myMusicInfoList);
        tvMusicCount.setText("（共" + myMusicInfoList.size() + "首）");
        musicSort(myMusicInfoList);
        myMusicAdapter.setNewData(myMusicInfoList);
    }

    /**
     * 音乐排序
     */
    private void musicSort(List<MyMusicInfo> myMusicInfoList) {
        List<MusicLocalInfo> playList = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
        int index = 0;//歌曲位置，0表示第一位
        for (int i = 0; i < playList.size(); i++) {
            for (int j = 0; j < myMusicInfoList.size(); j++) {
                if (playList.get(i).getSongId().equals(String.valueOf(myMusicInfoList.get(j).getId()))) {
                    if (index < myMusicInfoList.size()) {
                        MyMusicInfo temp = myMusicInfoList.get(index);
                        myMusicInfoList.set(index, myMusicInfoList.get(j));
                        myMusicInfoList.set(j, temp);
                        index++;
                    }
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadError(String msg, MusicLocalInfo localMusicInfo, HotMusicInfo hotMusicInfo) {
        notifyDataStatus(Long.parseLong(localMusicInfo.getSongId()), 0);
    }

    @CoreEvent(coreClientClass = IMusicCoreClient.class)
    public void delOperationRefresh(long id, boolean isHidePlayer) {
        if (myMusicAdapter != null) {
            List<MyMusicInfo> list = myMusicAdapter.getData();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getId() == id) {
                    myMusicAdapter.remove(i);
                    break;
                }
            }

            refreshMyMusicListSucceed(myMusicAdapter.getData());
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(MusicLocalInfo localMusicInfo) {
        notifyDataStatus(Long.parseLong(localMusicInfo.getSongId()), MyMusicAdapter.FILE_STATUS_STOP);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(MusicLocalInfo localMusicInfo) {
        notifyDataStatus(Long.parseLong(localMusicInfo.getSongId()), MyMusicAdapter.FILE_STATUS_PLAY);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop(MusicLocalInfo localMusicInfo) {
        notifyDataStatus(Long.parseLong(localMusicInfo.getSongId()), MyMusicAdapter.FILE_STATUS_PLAY);
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicInfoUpdateToLocalCompleted() {
        getDialogManager().dismissDialog();
    }


    /**
     * 开始下载
     */
    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicStartDownload(HotMusicInfo hotMusicInfo) {
        notifyDataStatus(hotMusicInfo.getId(), MyMusicAdapter.FILE_STATUS_DOWNLOADING);
    }

    private void notifyDataStatus(long id, int status) {
        if (myMusicAdapter != null) {
            List<MyMusicInfo> list = myMusicAdapter.getData();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getId() == id) {
                    MyMusicInfo myMusicInfo = list.get(i);
                    myMusicInfo.setFileStatus(status);
                    myMusicAdapter.setData(i, myMusicInfo);
                    myMusicAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void addSongSucceed(HotMusicInfo hotMusicInfo) {
        getMvpPresenter().getMySongList();//刷新数据
        notifyDataStatus(hotMusicInfo.getId(), MyMusicAdapter.FILE_STATUS_PLAY);
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void addSongFailure(HotMusicInfo hotMusicInfo) {
        notifyDataStatus(hotMusicInfo.getId(), MyMusicAdapter.FILE_STATUS_DOWNLOAD);
    }
}
