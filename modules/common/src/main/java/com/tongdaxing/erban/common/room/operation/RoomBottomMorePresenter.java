package com.tongdaxing.erban.common.room.operation;

import android.widget.Toast;

import com.erban.ui.mvp.BasePresenter;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.model.ReportModel;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.RoomSettingModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN;


/**
 * Created by Chen on 2019/6/11.
 */
public class RoomBottomMorePresenter extends BasePresenter<IRoomBottomMoreView> {
    private String TAG = "RoomBottomMorePresenter";
    private static ReportModel mReportModel;
    private RoomSettingModel mRoomSettingModel;

    private static HttpRequestCallBack<Object> reportCallBack = new HttpRequestCallBack<Object>() {
        @Override
        public void onFinish() {

        }

        @Override
        public void onSuccess(String message, Object response) {
            SingleToastUtil.showToast("举报成功，我们会尽快为您处理");
        }

        @Override
        public void onFailure(int code, String msg) {
            SingleToastUtil.showToast(msg);
        }
    };

    public boolean isRoomOwner() {
        return AvRoomDataManager.get().isRoomOwner();
    }

    public boolean isRoomAdmin() {
        return AvRoomDataManager.get().isRoomAdmin();
    }

    public void reportCommit(int reportType) {
        if (mReportModel == null) {
            mReportModel = new ReportModel();
        }
        mReportModel.reportCommit(2, reportType, reportCallBack);
    }

    public void setScreenShow(boolean publicChatSwitch) {
        IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);
        if (iAuthCore == null) {
            return;
        }
        String ticket = iAuthCore.getTicket();
        long currentUid = iAuthCore.getCurrentUid();
        CoreManager.getCore(IAVRoomCore.class).changeRoomMsgFilter(AvRoomDataManager.get().isRoomOwner(), publicChatSwitch ? 1 : 0, ticket, currentUid);
    }

    private void updateRoomInfo(String title, String pwd, String label, int tagId, String backPic, final int giftEffect, int drawMsgOption) {
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if ("".equals(title)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                title = userInfo.getNick() + "的房间";
            }
        }
        ResponseListener listener = new ResponseListener<ServiceResult<RoomInfo>>() {
            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                if (data == null) {
                    return;
                }
                boolean success = data.isSuccess();
                L.debug(TAG, "updateRoomInfo success: %b, giftEffect: %d", success, giftEffect);
                if (success) {
                    IMNetEaseManager.get().systemNotificationBySdk(uid, giftEffect == 1 ?
                            CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN :
                            CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE, -1);
                } else {
                    SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), data.getErrorMessage(), Toast.LENGTH_LONG);

                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), error.getErrorStr(), Toast.LENGTH_LONG);
            }
        };
        if (mRoomSettingModel == null) {
            mRoomSettingModel = new RoomSettingModel();
        }
        mRoomSettingModel.doUpdateRoomInfo(title, pwd, label, tagId, uid,
                CoreManager.getCore(IAuthCore.class).getTicket(), backPic, giftEffect, drawMsgOption, listener, errorListener);
    }

    private void updateByAdmin(long roomUid, String title, String pwd, String label, int tagId, String backPic, final int giftEffect, int drawMsgOption) {
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if ("".equals(title)) {
            title = null;
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                title = userInfo.getNick() + "的房间";
            }
        }
        ResponseListener listener = new ResponseListener<ServiceResult<RoomInfo>>() {
            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                if (data == null) {
                    return;
                }
                boolean success = data.isSuccess();
                L.debug(TAG, "updateByAdmin success: %b, giftEffect: %d", success, giftEffect);
                if (success) {
                    IMNetEaseManager.get().systemNotificationBySdk(uid, giftEffect == 1 ?
                            CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN :
                            CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE, -1);
                } else {
                    SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), data.getErrorMessage(), Toast.LENGTH_LONG);
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), error.getErrorStr(), Toast.LENGTH_LONG);
            }
        };
        if (mRoomSettingModel == null) {
            mRoomSettingModel = new RoomSettingModel();
        }
        mRoomSettingModel.updateByAdmin(roomUid, title, pwd, label, tagId, uid,
                CoreManager.getCore(IAuthCore.class).getTicket(), backPic, giftEffect, drawMsgOption, listener, errorListener);
    }

    public void setGiftShow(int giftEffectSwitch) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            if (isRoomOwner()) {
                updateRoomInfo(roomInfo.getTitle(), roomInfo.getRoomPwd(), roomInfo.getRoomTag(), roomInfo.tagId, roomInfo.getBackPic(), giftEffectSwitch, roomInfo.getDrawMsgOption());
            } else if (isRoomAdmin()) {
                updateByAdmin(roomInfo.getUid(), roomInfo.getTitle(), roomInfo.getRoomPwd(), roomInfo.getRoomTag(), roomInfo.tagId, roomInfo.getBackPic(), giftEffectSwitch, roomInfo.getDrawMsgOption());
            }
        }
    }
}
