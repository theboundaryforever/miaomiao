package com.tongdaxing.erban.common.room.lover.weekcp.tacit;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.erban.ui.baseview.BaseDialogFragment;
import com.tcloud.core.util.DensityUtil;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.lover.RoomLoverRoute;

/**
 * Created by Chen on 2019/5/7.
 */
@Route(path = RoomLoverRoute.ROOM_LOVER_TACIT_TEST_PATH)
public class TacitTestDialogFragment extends BaseDialogFragment {

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                WindowManager.LayoutParams params = window.getAttributes();
                params.gravity = Gravity.BOTTOM; // 显示在底部
                params.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度填充满屏
                params.height = (int) (DensityUtil.getDisplayHeight(BaseApp.mBaseContext) * 0.6f);
                window.setAttributes(params);
                // 这里用透明颜色替换掉系统自带背景
                int color = ContextCompat.getColor(BaseApp.mBaseContext, android.R.color.transparent);
                window.setBackgroundDrawable(new ColorDrawable(color));
            }

            // 主要是这两句起了作用
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    switch (keyCode) {
                        // 返回键
                        case KeyEvent.KEYCODE_BACK:
                            return true;
                        default:
                            break;
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_lover_dialog_fragment_tacit;
    }

    @Override
    public void initBefore() {

    }

    @Override
    public void findView() {

    }

    @Override
    public void setView() {

    }

    @Override
    public void setListener() {

    }
}
