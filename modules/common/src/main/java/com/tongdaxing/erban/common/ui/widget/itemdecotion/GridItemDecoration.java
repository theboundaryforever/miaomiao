package com.tongdaxing.erban.common.ui.widget.itemdecotion;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * 可设置间隔的gridItemDecoration
 * （整一行的左右间距 + 非第一列的每一列的左边距 实现）
 */
public class GridItemDecoration extends RecyclerView.ItemDecoration {
    private int itemCount;//一列多少个

    private int marginLeftAndRightByRow;//每行的行左间距和右间距
    private int marginLeftNotFirst;//每行非第一个view的左边距

    private int headerViewCount;//多少个headerView（头部view目前只能做左右margin）
    private int headerViewLeftAndRightMargin;//头部左和右margin

    /**
     * @param itemCount               一列多少个
     * @param marginLeftAndRightByRow 每行的行左间距和右间距
     * @param marginLeftNotFirst      每行非第一个view的左边距
     */
    public GridItemDecoration(int itemCount, int marginLeftAndRightByRow, int marginLeftNotFirst) {
        this.itemCount = itemCount;
        this.marginLeftAndRightByRow = marginLeftAndRightByRow;
        this.marginLeftNotFirst = marginLeftNotFirst;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int itemPosition = parent.getChildLayoutPosition(view) - headerViewCount;
        if (itemPosition >= 0) {//非头部
            if (itemPosition % itemCount == 0) {  //每行的第一个
                outRect.left = marginLeftAndRightByRow;
            } else if (itemPosition % itemCount == (itemCount - 1)) {//每行的最后一个
                outRect.left = marginLeftNotFirst;
                outRect.right = marginLeftAndRightByRow;
            } else {
                outRect.left = marginLeftNotFirst;
            }
        } else {//头部
            outRect.left = headerViewLeftAndRightMargin;
            outRect.right = headerViewLeftAndRightMargin;
        }
    }

    /**
     * 设置headerView的offsets
     *
     * @param count              头部的数量
     * @param leftAndRightMargin 左和右margin
     */
    public void setHeaderViewMargin(int count, int leftAndRightMargin) {
        this.headerViewCount = count;
        this.headerViewLeftAndRightMargin = leftAndRightMargin;
    }
}
