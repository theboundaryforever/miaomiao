package com.tongdaxing.erban.common.ui.find.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.graphics.Point;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.juxiao.library_utils.log.LogUtil;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.fragment.BaseLazyFragment;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.cpexclusive.CpExclusivePageActivity;
import com.tongdaxing.erban.common.ui.find.view.PublicMessageView;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.SquareCountDownTimer;
import com.tongdaxing.erban.common.view.UrlImageSpan;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.find.ICreateRoomClient;
import com.tongdaxing.xchat_core.find.SquareClient;
import com.tongdaxing.xchat_core.find.bean.SquareQueueInfo;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomStatus;
import com.tongdaxing.xchat_core.room.publicchatroom.ChatHallMsgEvent;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.ChatUtil;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.RichTextUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 新皮：发现模块下的大厅页面页面
 * <p>
 * 发现页---广场
 *
 * @author zwk 2018/6/1
 */
public class ChatHallFragment extends BaseLazyFragment {
    @BindView(R2.id.title_bar)
    TitleBar mTitleBar;
    @BindView(R2.id.message_view)
    PublicMessageView mMessageView;
    @BindView(R2.id.input_edit)
    EditText mInputEdit;
    @BindView(R2.id.input_send)
    ImageView mInputSend;
    @BindView(R2.id.tv_count_down)
    TextView mTvCountDown;
    @BindView(R2.id.iv_cp_user1)
    CircleImageView ivCpUser1;
    @BindView(R2.id.iv_cp_user2)
    CircleImageView ivCpUser2;
    @BindView(R2.id.tv_cp_content)
    TextView tvCpContent;
    @BindView(R2.id.cp_layout)
    View cpLayout;
    public static final String TAG = "ChatHallFragment";

    private PublicChatRoomManager mPublicChatRoomManager;
    private int index = 0;
    private CpCountDownTimerImpl cpCountDownTimerImpl;
    private EffectHandler effectHandler;
    private SquareCountDownTimer mCountDownTimer;
    //避免重复提交
    private boolean isReport = true;

    private List<SquareQueueInfo> queueInfos;
    public static final String IS_NEED_SHOW_TITLE_BAR = "isNeedShowTitleBar";
    @BindView(R2.id.chat_hall_title_bg)
    ImageView chatHallTitleBg;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_chat_hall;
    }

    @Override
    public void bindView() {
        ButterKnife.bind(this, mView);
        CoreUtils.register(this);
    }

    @Override
    public void onResume() {
        if (effectHandler == null) {
            effectHandler = new EffectHandler(this);
        }
        super.onResume();
    }

    @Override
    protected void onLazyLoadData() {
        queryUnfinishedCpBroadcastMsg();
        initChatHall();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (effectHandler != null) {
            effectHandler.removeCallbacksAndMessages(null);
        }
        CoreUtils.unregister(this);
    }

    public void initChatHall() {
//        mMessageView.clear();
        initRoomController();
        enterNeteaseRoom();
        checkCountDown();
    }

    private void initRoomController() {
        mPublicChatRoomManager = CoreManager.getCore(IRoomCore.class).getPublicChatRoomManager();
    }

    private void enterNeteaseRoom() {
        mPublicChatRoomManager.enterRoom(new PublicChatRoomManager.ActionCallBack() {
            @Override
            public void success() {
                L.info(TAG, "enterNeteaseRoom() success()");
            }

            @Override
            public void error(int code) {
                L.error(TAG, "enterNeteaseRoom() error() code = %d", code);
                showNoData("进入大厅失败，请重试");
            }

            @Override
            public void error(Throwable exception) {
                L.error(TAG, "enterNeteaseRoom() error() exception: ", exception);
                showNoData("进入大厅失败，请重试");
            }
        });
        mPublicChatRoomManager.initCacheTime();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveNewMsg(ChatHallMsgEvent.OnReceiveNewMsg onReceiveNewMsg) {
        ChatRoomMessage chatRoomMessage = onReceiveNewMsg.getChatRoomMessage();
        if (chatRoomMessage != null) {
            L.debug(TAG, "onReceiveNewMsg chatRoomMessage");
            //单条消息
            mMessageView.onCurrentRoomReceiveNewMsg(chatRoomMessage);
        } else {
            //多条消息
            List<ChatRoomMessage> chatRoomMessages = onReceiveNewMsg.getChatRoomMessages();
            if (chatRoomMessages != null && chatRoomMessages.size() > 0) {
                mMessageView.onCurrentRoomReceiveNewMsg(chatRoomMessages);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSendMsgFailure(ChatHallMsgEvent.OnSendMsgFailure onSendMsgFailure) {
        SingleToastUtil.showToast(mContext, R.string.send_failure);
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        if (mPublicChatRoomManager != null) {
            mPublicChatRoomManager.refreshTime();
        }
        onLazyLoadData();
    }

    private void showQueueBroadcastMsg() {
        if (!ListUtils.isListEmpty(queueInfos) && index < queueInfos.size()) {
            SquareQueueInfo squareQueueInfo = queueInfos.get(index);
            if (squareQueueInfo == null || squareQueueInfo.getInviteUser() == null ||
                    squareQueueInfo.getRecUser() == null || cpLayout.getVisibility() == View.VISIBLE) {
                return;
            }
            cpLayout.setVisibility(View.VISIBLE);
            cpLayout.setOnClickListener(v -> loadRoomStatus(squareQueueInfo));
            cpCountDownTimerImpl = new CpCountDownTimerImpl(squareQueueInfo.getCountTime(), 1000);
            cpCountDownTimerImpl.start();
            startAnim();
            ImageLoadUtils.loadCircleImage(getActivity(), squareQueueInfo.getInviteUser().getAvatar(), ivCpUser1, R.drawable.default_cover);
            ImageLoadUtils.loadCircleImage(getActivity(), squareQueueInfo.getRecUser().getAvatar(), ivCpUser2, R.drawable.default_cover);
            String userName1 = squareQueueInfo.getInviteUser().getNick();
            String userName2 = squareQueueInfo.getRecUser().getNick();
            if (!TextUtils.isEmpty(userName1) && userName1.length() > 6) {
                userName1 = userName1.substring(0, 6) + "...";
            }

            if (!TextUtils.isEmpty(userName2) && userName2.length() > 6) {
                userName2 = userName2.substring(0, 6) + "...";
            }
            setTvCpContent(userName1, userName2, squareQueueInfo.getPicUrl(), tvCpContent);
        }
    }

    public void loadRoomStatus(SquareQueueInfo squareQueueInfo) {
        if (getActivity() == null) {
            return;
        }
        getDialogManager().showProgressDialog(getActivity(), "请稍后...");
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("queryUid", String.valueOf(squareQueueInfo.getRecUser().getUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getTheRoomStatus(), params, new OkHttpManager.MyCallBack<ServiceResult<RoomStatus>>() {
            @Override
            public void onError(Exception e) {
                getDialogManager().dismissDialog();
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RoomStatus> response) {
                getDialogManager().dismissDialog();
                if (response == null) {
                    return;
                }
                if (response.isSuccess() && response.getData() != null && response.getData().getUid() > 0) {
                    AVRoomActivity.start(getActivity(), response.getData().getUid());
                } else {
                    long userId = squareQueueInfo.getRecUser().getUid();
                    UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                    if (userInfo == null) {
                        return;
                    }
                    if (userInfo.getUid() == userId || userInfo.getUid() == squareQueueInfo.getInviteUser().getUid()) {
                        //我是cp中的接受者或者邀请者
                        if (userInfo.getCpUser() != null) {
                            //打开自己和cp的CP专属页 有解除CP功能
                            CpExclusivePageActivity.start(getActivity(), userId, true);
                        }
                    } else {
                        //我不是cp的接受者、邀请者
                        //打开他人的CP专属页 没有解除CP功能
                        CpExclusivePageActivity.start(getActivity(), userId);
                    }
                }
            }
        });
    }

    @CoreEvent(coreClientClass = SquareClient.class)
    public void onCpBroadcastMsgNotify(SquareQueueInfo squareQueueInfo) {
        if (!ListUtils.isListEmpty(queueInfos)) {
            queueInfos.add(squareQueueInfo);
        } else {
            queueInfos = new ArrayList<>();
            queueInfos.add(squareQueueInfo);
        }
        showQueueBroadcastMsg();
    }

    private void startAnim() {
        final Point center = new Point();
        center.x = DisplayUtils.getScreenWidth(BaseApp.mBaseContext) / 2;
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(cpLayout, "translationX", -UIUtil.dip2px(BaseApp.mBaseContext, 100), center.x - DisplayUtils.getScreenWidth(BaseApp.mBaseContext) / 2).setDuration(500);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.start();

        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(cpLayout, "alpha", 0.0F, 1.0F).setDuration(500);
        objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator1.start();
    }

    private void deleteAnim() {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(cpLayout, "translationX", cpLayout.getX(), DisplayUtils.getScreenWidth(BaseApp.mBaseContext)).setDuration(500);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.start();

        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(cpLayout, "alpha", 1.0F, 0.0F).setDuration(500);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                index++;
                cpLayout.setVisibility(View.GONE);
                showQueueBroadcastMsg();
            }
        });
        objectAnimator1.start();
    }

    /**
     * 设置内容
     */
    private void setTvCpContent(String userName1, String userName2, String giftUrl, TextView tvCpContent) {
        int normalCore = 0xFFFFFFFF;
        int focusCore = mContext.getResources().getColor(R.color.color_311301);
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list.add(RichTextUtil.getRichTextMap("哇，恭喜", normalCore));
        list.add(RichTextUtil.getRichTextMap(userName1, focusCore));
        list.add(RichTextUtil.getRichTextMap(" 将 ", normalCore));
        SpannableStringBuilder builder = RichTextUtil.getSpannableStringFromList(list);

        //插入礼物图标
        builder.append(BaseConstant.GIFT_PLACEHOLDER);
        UrlImageSpan imageSpan = new UrlImageSpan(mContext, giftUrl, tvCpContent);
        imageSpan.setImgWidth(com.juxiao.library_utils.DisplayUtils.dip2px(mContext, 25));
        imageSpan.setImgHeight(com.juxiao.library_utils.DisplayUtils.dip2px(mContext, 25));
        builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(), builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        list = new ArrayList<>();
        list.add(RichTextUtil.getRichTextMap(" 赠予 ", normalCore));
        list.add(RichTextUtil.getRichTextMap(userName2, focusCore));
        list.add(RichTextUtil.getRichTextMap("成为众人羡慕的CP，大家来祝福他们吧。", normalCore));
        builder.append(RichTextUtil.getSpannableStringFromList(list));

        tvCpContent.setText(builder);
    }

    /**
     * 重新查询未播放完毕CP广播消息
     */
    public void queryUnfinishedCpBroadcastMsg() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getQueryBroadcastMsg(), params, new OkHttpManager.MyCallBack<ServiceResult<List<SquareQueueInfo>>>() {
            @Override
            public void onError(Exception e) {
                LogUtil.dToFile(UriProvider.getQueryBroadcastMsg(), "重新查询未播放完毕CP广播消息失败！");
            }

            @Override
            public void onResponse(ServiceResult<List<SquareQueueInfo>> response) {
                if (response == null || ListUtils.isListEmpty(response.getData())) {
                    return;
                }
                if (response.isSuccess()) {
                    queueInfos = response.getData();
                    index = 0;
                    showQueueBroadcastMsg();
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {
        mInputSend.setOnClickListener(this);
        mTvCountDown.setOnClickListener(this);
    }

    @Override
    public void initiate() {
        Bundle arguments = getArguments();
        if (arguments != null && arguments.getBoolean(IS_NEED_SHOW_TITLE_BAR)) {
            if (getActivity() != null) {
                mTitleBar.setVisibility(View.VISIBLE);
                ((BaseActivity) getActivity()).initTitleBarWhite("闲聊广场");
                chatHallTitleBg.setVisibility(View.VISIBLE);
            }
        } else {
            mTitleBar.setVisibility(View.GONE);
            chatHallTitleBg.setVisibility(View.GONE);
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetUserRoom(RoomInfo roomInfo) {
        long formalRoomId = BasicConfig.isDebug ? PublicChatRoomManager.devRoomId : PublicChatRoomManager.formalRoomId;
        if (roomInfo.getRoomId() == formalRoomId) {
            toast("对方不在房间内");
        } else {
            getDialogManager().dismissDialog();
            RoomInfo current = AvRoomDataManager.get().mCurrentRoomInfo;
            if (roomInfo != null && roomInfo.getUid() > 0) {
                if (current != null) {
                    if (current.getUid() == roomInfo.getUid()) {
                        toast("已经和对方在同一个房间");
                        return;
                    }
                }
                AVRoomActivity.start(mContext, roomInfo.getUid());
            } else {
                toast("对方不在房间内");
            }
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        int i = view.getId();
        if (i == R.id.input_send) {//                if (!RealNameAuthStatusChecker.getInstance().authStatus(getActivity(), getResources().getString(R.string.real_name_auth_tips,
//                        getResources().getString(R.string.real_name_auth_tips_publish_content)))) {
//                    return;
//                }
//
            MobclickAgent.onEvent(mContext, UmengEventId.getFindHallSendBtn());
            if (!checkSendPower()) {
                return;
            }
            if (ChatUtil.checkPublicRoomChatBanned())
                return;
            String trim = mInputEdit.getText().toString().trim();
            if (StringUtils.isEmpty(trim))
                return;
            String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
            if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(trim)) {
                if (trim.matches(sensitiveWordData)) {
                    SingleToastUtil.showToast(this.getString(R.string.sensitive_word_data));
                    return;
                }
            }
            if (trim.length() > 40) {
                trim = trim.substring(0, 40);
            }
            mPublicChatRoomManager.sendMsg(trim);
            mInputEdit.setText("");
            checkCountDown();
            if (mCountDownTimer != null) {
                mCountDownTimer.startCountDownTimer();
            }
            if (isReport) {
                report();
            }

        } else if (i == R.id.tv_count_down) {
            checkSendPower();

        } else {
        }
    }

    private void report() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.reportPublic(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    isReport = false;
                }
            }
        });
    }

    public boolean checkSendPower() {
        if (mCountDownTimer != null && mCountDownTimer.isRunning) {
            toast("广播发送间隔为" + PublicChatRoomManager.WAITING_TIME + "秒，请稍等");
            return false;
        }
        return true;
    }

    public void checkCountDown() {
//        long time = System.currentTimeMillis();
//        long l = time - mPublicChatRoomManager.cacheTime;
//        l = 30000 - l;
        mCountDownTimer = SquareCountDownTimer.getInstance();
        if (mCountDownTimer.isRunning) {
            mInputSend.setVisibility(View.GONE);
            mTvCountDown.setVisibility(View.VISIBLE);
        } else {
            mInputSend.setVisibility(View.VISIBLE);
            mTvCountDown.setVisibility(View.GONE);
        }
    }

    @CoreEvent(coreClientClass = ICreateRoomClient.class)
    public void finishSquareNotify() {
        mInputSend.setVisibility(View.VISIBLE);
        mTvCountDown.setVisibility(View.GONE);
    }

    @CoreEvent(coreClientClass = ICreateRoomClient.class)
    public void refreshSquareNotify(int countDownTime) {
        mInputSend.setVisibility(View.GONE);
        mTvCountDown.setVisibility(View.VISIBLE);
        mTvCountDown.setText(countDownTime + "S");
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {//用户进入该页面才会有回调
        //退出后离开公聊
        if (mPublicChatRoomManager != null) {
            Constants.isNeedJoin = true;
            isReport = true;
        }
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onImLoginSuccess(LoginInfo loginInfo) {
        //如果重新登陆在尝试重新进入大厅 -- 避免切换账号后当前账号不在公聊大厅
        if (Constants.isNeedJoin) {
            Constants.isNeedJoin = false;
            if (mPublicChatRoomManager != null) {
                mPublicChatRoomManager.refreshTime();
            }
            onLazyLoadData();
        }
    }

    private static class EffectHandler extends Handler {
        private WeakReference<ChatHallFragment> effectViewWeakReference;

        public EffectHandler(ChatHallFragment giftEffectView) {
            effectViewWeakReference = new WeakReference<>(giftEffectView);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (effectViewWeakReference != null) {
                final ChatHallFragment giftEffectView = effectViewWeakReference.get();
                if (giftEffectView != null) {
                    if (msg.what == 0) {
                        giftEffectView.deleteAnim();
                    }
                }
            }
        }
    }

    private class CpCountDownTimerImpl extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public CpCountDownTimerImpl(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            Log.e("广场倒计时", millisUntilFinished / 1000 + "秒");
        }

        @Override
        public void onFinish() {
            if (effectHandler != null) {
                effectHandler.sendEmptyMessage(0);
            }
        }
    }
}
