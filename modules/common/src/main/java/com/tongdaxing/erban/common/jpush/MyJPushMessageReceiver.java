package com.tongdaxing.erban.common.jpush;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.find.activity.NoticeIMActivity;
import com.tongdaxing.erban.common.ui.find.activity.VoiceGroupIMActivity;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.utils.HttpUtil;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;

import cn.jpush.android.api.CmdMessage;
import cn.jpush.android.api.CustomMessage;
import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.api.NotificationMessage;
import cn.jpush.android.service.JPushMessageReceiver;

/**
 * 自定义JPush message 接收器,包括操作tag/alias的结果返回(仅仅包含tag/alias新接口部分)
 * <p>
 * 3.3.0开始是通过继承 JPushMessageReceiver并配置来接收所有事件回调
 */
public class MyJPushMessageReceiver extends JPushMessageReceiver {
    private static final String TAG = "MyJPushMessageReceiver";

    @Override
    public void onTagOperatorResult(Context context, JPushMessage jPushMessage) {
        JPushHelper.getInstance().onTagOperatorResult(context, jPushMessage);
        L.info(TAG, "onTagOperatorResult jPushMssage: %s", jPushMessage.toString());
        super.onTagOperatorResult(context, jPushMessage);
    }

    @Override
    public void onCheckTagOperatorResult(Context context, JPushMessage jPushMessage) {
        JPushHelper.getInstance().onCheckTagOperatorResult(context, jPushMessage);
        super.onCheckTagOperatorResult(context, jPushMessage);
    }

    @Override
    public void onAliasOperatorResult(Context context, JPushMessage jPushMessage) {
        JPushHelper.getInstance().onAliasOperatorResult(context, jPushMessage);
        L.info(TAG, "onAliasOperatorResult jPushMessage: %s", jPushMessage.toString());
        super.onAliasOperatorResult(context, jPushMessage);
    }

    @Override
    public void onMobileNumberOperatorResult(Context context, JPushMessage jPushMessage) {
        JPushHelper.getInstance().onMobileNumberOperatorResult(context, jPushMessage);
        super.onMobileNumberOperatorResult(context, jPushMessage);
    }

    /**
     * 注册成功回调
     *
     * @param context
     * @param s
     */
    @Override
    public void onRegister(Context context, String s) {
        super.onRegister(context, s);
        L.info(TAG, "onRegister s: %s", s);
    }

    /**
     * 注册失败回调
     *
     * @param context
     * @param cmdMessage
     */
    @Override
    public void onCommandResult(Context context, CmdMessage cmdMessage) {
        super.onCommandResult(context, cmdMessage);
        L.info(TAG, "onCommandResult cmdMessage: %s", cmdMessage.toString());
    }

    /**
     * 长连接状态回调
     *
     * @param context
     * @param b       true 连接正常
     */
    @Override
    public void onConnected(Context context, boolean b) {
        super.onConnected(context, b);
        L.info(TAG, "onConnected b: %b", b);
    }

    /**
     * 收到自定义消息回调
     *
     * @param context
     * @param customMessage
     */
    @Override
    public void onMessage(Context context, CustomMessage customMessage) {
        super.onMessage(context, customMessage);
        L.info(TAG, "onMessage customMessage: %s", customMessage.toString());
    }

    /**
     * 收到通知回调
     * <p>
     * 极光推送如何区分前后台 来决定是否收到推送
     * 1、服务端推送不展示的信息：自定义消息（默认不展示），或者推送alert为空（通知内容为空）的通知消息（不展示），需要的信息放在 extra 里面。
     * 2、客户端获取到数据后，判断app的运行状态。（或者判断用户是否登录等，该消息是否满足展示条件等）
     * 3、然后决定是否展示，不需要展示则：存储数据或者根据你需求进行其他操作；需要展示则：自定义消息需要自行写代码实现展示；可以创建一个本地通知来展示。
     *
     * @param context
     * @param notificationMessage
     */
    @Override
    public void onNotifyMessageArrived(Context context, NotificationMessage notificationMessage) {
        super.onNotifyMessageArrived(context, notificationMessage);
        L.info(TAG, "onNotifyMessageArrived notificationMessage: %s", notificationMessage.toString());
//        if (!BasicConfig.INSTANCE.isAppRunningForeground()) {
//            //应用在后台 那么发送本地通知显示
//            L.info(TAG, "onMessage show LocalNotify");
//            if (notificationMessage.notificationType == 0) {//0是远程通知 1是本地通知 因为创建本地通知addLocalNotification也会触发onNotifyMessageArrived，所以这里要做过滤
//                JPushHelper.addLocalNotification(context, "lalala", "test", notificationMessage.notificationExtras);
//            }
//        } else {
//            //不需要展示通知
//        }
    }

    /**
     * 点击通知回调
     *
     * @param context
     * @param notificationMessage
     */
    @Override
    public void onNotifyMessageOpened(Context context, NotificationMessage notificationMessage) {
        super.onNotifyMessageOpened(context, notificationMessage);
        L.info(TAG, "onNotifyMessageOpened notificationMessage: %s", notificationMessage.toString());
        if (!BasicConfig.INSTANCE.isMainActivityLive()) {
            //主程序并没有运行 先启动应用 再打开目标页面
            if (Build.BRAND.equalsIgnoreCase("Huawei") || Build.BRAND.equalsIgnoreCase("HONOR")
                    || Build.BRAND.equalsIgnoreCase("OPPO")) {
                L.info(TAG, "BasicConfig.INSTANCE.setPushExtras: %s", notificationMessage.notificationExtras);
                BasicConfig.INSTANCE.setPushExtras(notificationMessage.notificationExtras);//针对华为和OPPO不支持先启动应用 再打开目标页面 只能在内存中保存要跳转的页面的数据
            }
            L.info(TAG, "isAppRunningForeground false");
            String packageName = context.getApplicationContext().getPackageName();
            Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
            launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            launchIntent.putExtra(Constants.PUSH_SKIP_EXTRA, notificationMessage.notificationExtras);
            context.startActivity(launchIntent);
        } else {
            //应用在运行 直接打开目标页面
            L.info(TAG, "isAppRunningForeground true");
            if (!TextUtils.isEmpty(notificationMessage.notificationExtras)) {
                dealWithPushData(context, notificationMessage.notificationExtras);
            }
        }
    }

    /**
     * 处理推送通知点击跳转
     *
     * @param pushExtrasJson
     */
    private void dealWithPushData(Context context, String pushExtrasJson) {
        Json json = Json.parse(pushExtrasJson);
        String skipType = json.str("skipType");
        String skipUri = json.str("skipUri");
        String msgType = json.str("msgType");
        L.info(TAG, "dealWithPushData skipUri = %s, skipType = %s, msgType = %s", skipType, skipUri, msgType);
        if (TextUtils.isEmpty(skipType)) {
            return;
        }
        if (!TextUtils.isEmpty(msgType)) {
            UmengEventUtil.getInstance().onCommonTypeEvent(context, UmengEventId.getPushType(), msgType);
        }
        switch (skipType) {
            case PushExtras.P2P_CHAT:
                try {
                    if (TextUtils.isEmpty(skipUri)) {
                        return;
                    }
                    long userId = Long.parseLong(skipUri);
                    HttpUtil.checkUserIsDisturb(context, userId);
                } catch (NumberFormatException e) {
                    CoreUtils.crashIfDebug(e, "push skipType NumberFormatException");
                }
                break;
            case PushExtras.ROOM:
                try {
                    if (TextUtils.isEmpty(skipUri)) {
                        return;
                    }
                    long userId = Long.parseLong(skipUri);
                    AVRoomActivity.start(context, userId);
                } catch (NumberFormatException e) {
                    CoreUtils.crashIfDebug(e, "push skipType NumberFormatException");
                }
                break;
            case PushExtras.H5:
                if (TextUtils.isEmpty(skipUri)) {
                    return;
                }
                CommonWebViewActivity.start(context, skipUri);
                break;
            case PushExtras.VOICE_GROUP_IM:
                VoiceGroupIMActivity.start(context, BaseConstant.getVoiceGroupAccount());
                break;
            case PushExtras.NOTIFICATION_IM:
                NoticeIMActivity.start(context, BaseConstant.getNotificationAccount());
                break;
        }
    }

    /**
     * 清除通知回调
     * 1.同时删除多条通知，可能不会多次触发清除通知的回调
     * <p>
     * 2.只有用户手动清除才有回调，调接口清除不会有回调
     *
     * @param context
     * @param notificationMessage
     */
    @Override
    public void onNotifyMessageDismiss(Context context, NotificationMessage notificationMessage) {
        super.onNotifyMessageDismiss(context, notificationMessage);
        L.info(TAG, "onNotifyMessageDismiss notificationMessage: %s", notificationMessage.toString());
    }

    /**
     * 通知的MultiAction回调
     * 注意这个方法里面禁止再调super.onMultiActionClicked,因为会导致逻辑混乱
     *
     * @param context
     * @param intent
     */
    @Override
    public void onMultiActionClicked(Context context, Intent intent) {
        L.info(TAG, "onMultiActionClicked");
    }
}
