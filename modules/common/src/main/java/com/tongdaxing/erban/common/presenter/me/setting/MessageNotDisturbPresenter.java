package com.tongdaxing.erban.common.presenter.me.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tongdaxing.erban.common.ui.me.setting.notificationSetting.NotificationSettingModel;
import com.tongdaxing.erban.common.ui.me.setting.vew.IMsgNotDisturbView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;

public class MessageNotDisturbPresenter extends AbstractMvpPresenter<IMsgNotDisturbView> {
    private NotificationSettingModel model;

    @Override
    public void onCreatePresenter(@Nullable Bundle saveState) {
        super.onCreatePresenter(saveState);
        model = new NotificationSettingModel();
    }

    /**
     * 获取免打扰状态
     */
    public void getMsgDisturbState() {
        model.getMsgDisturbState(new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().getDisturbStateFail(e != null ? e.getMessage() : "网络异常");
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    int chatPermission = 0;
                    Json json = response.json("data");
                    if (json != null) {
                        chatPermission = json.num("chatPermission");
                    }
                    if (getMvpView() != null)
                        getMvpView().getDisturbStateSuccess(chatPermission);
                } else {
                    if (getMvpView() != null)
                        getMvpView().getDisturbStateFail(response != null ? response.str("message") : "接口异常");
                }
            }
        });
    }

    /**
     * 修改免打扰状态
     *
     * @param state
     */
    public void changeMsgDisturbState(int state) {
        model.changeMsgDisturbState(state, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().saveDisturbStateFail(e != null ? e.getMessage() : "网络异常");
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    if (getMvpView() != null)
                        getMvpView().saveDisturbStateSuccess(state);
                } else {
                    if (getMvpView() != null)
                        getMvpView().saveDisturbStateFail(response != null ? response.str("message") : "接口异常");
                }
            }
        });
    }

    public void setNoticeOption(String configId, int configValue) {
        model.getSetNoticeOption(configId, configValue, new OkHttpManager.MyCallBack<ServiceResult<Boolean>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().saveDisturbStateFail(e != null ? e.getMessage() : "网络异常");
            }

            @Override
            public void onResponse(ServiceResult<Boolean> response) {
                if (response.isSuccess() && response.getData() != null && response.getData()) {
                    if (getMvpView() != null)
                        getMvpView().saveNotifySettingSuccess(configId, configValue);
                } else {
                    if (getMvpView() != null)
                        getMvpView().saveNotifySettingFail(response.getMessage());
                }
            }
        });
    }
}
