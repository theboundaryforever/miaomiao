package com.tongdaxing.erban.common.ui.find.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.find.bean.CpUserPageVo;

/**
 * Function:
 * Author: Edward on 2019/3/28
 */
public interface ICpExclusivePageView extends IMvpBaseView {
    void relieveCpSucceed();

    void relieveCpFailure();

    void setCpSignSucceed();

    void setCpSignFailure();

    void loadFailure(String errorStr);

    void loadSucceed(CpUserPageVo cpUserPageVo);

    void onFreshSignFromEdit(String sign);

    void onNeedCharge();
}
