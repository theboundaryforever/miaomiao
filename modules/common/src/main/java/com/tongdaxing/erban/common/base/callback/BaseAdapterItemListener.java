package com.tongdaxing.erban.common.base.callback;

/**
 * Created by Administrator on 2018/5/21.
 */

public interface BaseAdapterItemListener {
    void onItemClick(int index);
}
