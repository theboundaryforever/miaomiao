package com.tongdaxing.erban.common.ui.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.callback.BaseAdapterItemListener;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_core.home.HomeRoom;

import java.util.List;

/**
 * Created by chenran on 2017/10/3.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    public BaseAdapterItemListener baseAdapterItemListener;
    private Context context;
    private List<HomeRoom> homeRoomList;

    public SearchAdapter(Context context, List<HomeRoom> homeRoomList) {
        this.context = context;
        this.homeRoomList = homeRoomList;
    }

    public void setHomeRoomList(List<HomeRoom> homeRoomList) {
        this.homeRoomList = homeRoomList;
    }

    @Override
    public int getItemCount() {
        return homeRoomList != null ? homeRoomList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.list_item_search, parent, false);
        return new SearchAdapter.ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(final SearchAdapter.ViewHolder holder, int position) {
        final HomeRoom item = homeRoomList.get(position);
        holder.userName.setText(item.getNick());
        ImageLoadUtils.loadCircleImage(holder.avatar.getContext(), item.getAvatar(), holder.avatar, R.drawable.ic_no_avatar);
        final boolean isUserInRoom = item.getRoomId() > 0;
        if (!isUserInRoom) {
            holder.tvCustomLabel.setVisibility(View.GONE);
            holder.roomTitle.setVisibility(View.GONE);
            holder.searchListItemInRoomBtn.setVisibility(View.GONE);
        } else {
            holder.roomTitle.setVisibility(View.VISIBLE);
            holder.searchListItemInRoomBtn.setVisibility(View.VISIBLE);
            String roomTag = item.getRoomTag();
            if (TextUtils.isEmpty(roomTag)) {
                holder.tvCustomLabel.setVisibility(View.GONE);
            } else {
                holder.tvCustomLabel.setVisibility(View.VISIBLE);
                holder.tvCustomLabel.setText(roomTag);
            }
            holder.roomTitle.setText(item.getTitle());
        }

        holder.erbanNo.setText("ID:" + item.getErbanNo());

        holder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 点击头像跳转对应的个人主页
                NewUserInfoActivity.start(context, item.getUid());
                if (baseAdapterItemListener != null)
                    baseAdapterItemListener.onItemClick(position);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 如有“房间中”标识，点击整栏则跳转至对应房间
                if (isUserInRoom) {
                    AVRoomActivity.start(context, item.getRoomId());
                    if (baseAdapterItemListener != null)
                        baseAdapterItemListener.onItemClick(position);
                } else {
                    holder.avatar.performClick();
                }
            }
        });

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView userName;
        TextView tvCustomLabel;
        TextView roomTitle;
        TextView erbanNo;
        TextView searchListItemInRoomBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            userName = (TextView) itemView.findViewById(R.id.user_name);
            tvCustomLabel = (TextView) itemView.findViewById(R.id.item_search_custom_label_tv);
            roomTitle = (TextView) itemView.findViewById(R.id.room_title);
            erbanNo = (TextView) itemView.findViewById(R.id.erban_no);
            searchListItemInRoomBtn = (TextView) itemView.findViewById(R.id.search_list_item_in_room_btn);
        }
    }
}
