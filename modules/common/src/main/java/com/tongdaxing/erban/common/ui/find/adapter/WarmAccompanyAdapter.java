package com.tongdaxing.erban.common.ui.find.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.home.fragment.HotTagFragment;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.ruffian.library.widget.RView;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.find.bean.WarmAccompanyInfo;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class WarmAccompanyAdapter extends BaseQuickAdapter<WarmAccompanyInfo, BaseViewHolder> {
    private int genderId;
    private int itemMargin, listMargin;

    public WarmAccompanyAdapter(int layoutResId, int genderId) {
        super(layoutResId);
        this.genderId = genderId;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder baseViewHolder = super.onCreateViewHolder(parent, viewType);
        listMargin = mContext.getResources().getDimensionPixelSize(R.dimen.find_warm_item_left_or_right);//recyclerview的左边距和右边距
        itemMargin = mContext.getResources().getDimensionPixelSize(R.dimen.find_warm_item_margin);//item间的边距
        return baseViewHolder;
    }

    @Override
    protected void convert(BaseViewHolder helper, WarmAccompanyInfo item) {
        int positionByRow = helper.getAdapterPosition();
        if (positionByRow % HotTagFragment.ITEM_COUNT_BY_ROW == 0) {//当前行的第一个
            helper.itemView.setPadding(listMargin, 0, itemMargin / 2, 0);
        } else if (positionByRow % HotTagFragment.ITEM_COUNT_BY_ROW == HotTagFragment.ITEM_COUNT_BY_ROW - 1) {//当前行的最后一个
            helper.itemView.setPadding(itemMargin / 2, 0, listMargin, 0);
        } else {
            //如果要支持列数不等于2，这里要考虑
        }

        ImageView ivCover = helper.getView(R.id.iv_cover);
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), ivCover, R.drawable.default_cover);

        helper.setText(R.id.tv_title, item.getTitle());
        helper.setText(R.id.tv_nick, item.getNick());
        RView rView = helper.getView(R.id.item_home_room_rview);
        rView.getHelper().setBorderColorNormal(mContext.getResources().getColor(item.getGender() == 2 ?
                R.color.item_find_warm_girl_circle_bg : R.color.item_find_warm_boy_circle_bg));
        helper.setImageResource(R.id.find_warm_gender, item.getGender() == 2 ? R.drawable.find_warm_ic_girl : R.drawable.find_warm_ic_boy);

        TextView tvCustomLabel = helper.getView(R.id.tv_custom_label);

        String roomTag = item.getRoomTag();
        if (TextUtils.isEmpty(roomTag)) {
            tvCustomLabel.setVisibility(View.INVISIBLE);
        } else {
            tvCustomLabel.setText(roomTag);
            tvCustomLabel.setVisibility(View.VISIBLE);
        }

        helper.itemView.setOnClickListener(v -> {
            if (item.getUid() > 0) {
                AVRoomActivity.start(mContext, item.getUid());
            } else {
                SingleToastUtil.showToast("数据错误!");
            }
        });
    }
}
