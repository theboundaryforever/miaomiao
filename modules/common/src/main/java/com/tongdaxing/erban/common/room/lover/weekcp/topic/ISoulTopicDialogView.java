package com.tongdaxing.erban.common.room.lover.weekcp.topic;

import java.util.List;

/**
 * Created by Chen on 2019/5/8.
 */
public interface ISoulTopicDialogView {
    void dismiss();

    void updateView(List<String> tittles);
}
