package com.tongdaxing.erban.common.room.lover.chair;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.erban.ui.mvp.MVPBaseRelativeLayout;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.im.actions.GiftAction;
import com.tongdaxing.erban.common.room.avroom.activity.RoomInviteActivity;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.room.face.anim.AnimFaceFactory;
import com.tongdaxing.erban.common.room.gift.GiftDialog;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.erban.common.room.widget.dialog.BigListDataDialog;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chen on 2019/4/26.
 */
public class RoomLoverChairView extends MVPBaseRelativeLayout<IRoomLoverChairView, RoomLoverChairPresenter> implements IRoomLoverChairView,
        GiftDialog.OnGiftDialogBtnClickListener, ChargeListener {

    @BindView(R2.id.contribute_list)
    ImageView contributeList;
    @BindView(R2.id.tv_room_boos_name)
    TextView tvRoomBoosName;
    @BindView(R2.id.room_lover_owner)
    ChairItemView roomLoverOwner;
    @BindView(R2.id.room_lover_player)
    ChairItemView roomLoverPlayer;
    @BindView(R2.id.owner_layout)
    RelativeLayout ownerLayout;

    private SparseArray<ImageView> faceImageViews;

    private int mMicPosition = 0;

    private int giftWidth;
    private int giftHeight;
    private int faceWidth;
    private int faceHeight;

    private DialogManager mDialogManager;

    public RoomLoverChairView(@NonNull Context context) {
        super(context);
    }

    public RoomLoverChairView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RoomLoverChairView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private static boolean isActivityDestroyed(Context context) {
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.isFinishing() || activity.isDestroyed()) {
                return true;
            }
        }
        return false;
    }

    @NonNull
    @Override
    protected RoomLoverChairPresenter createPresenter() {
        return new RoomLoverChairPresenter();
    }

    @Override
    protected void findView() {
        ButterKnife.bind(this);
    }

    @Override
    protected void setView() {
        if (mDialogManager == null) {
            mDialogManager = new DialogManager(getContext());
        }

        giftWidth = UIUtil.dip2px(getContext(), 80);
        giftHeight = UIUtil.dip2px(getContext(), 80);
        faceWidth = UIUtil.dip2px(getContext(), 100);
        faceHeight = UIUtil.dip2px(getContext(), 100);

        faceImageViews = new SparseArray<>(2);

        roomLoverOwner.postDelayed(new Runnable() {
            @Override
            public void run() {
                setMicCenterPoint();
            }
        }, 1000);
    }

    @Override
    protected void setListener() {

    }

    @Override
    public int getContentViewId() {
        return R.layout.layout_lover_micro_view;
    }

    @Override
    public void initChairInfo(final List<RoomQueueInfo> chairList) {

    }

    public void setMicCenterPoint() {
        SparseArray<Point> centerPoints = new SparseArray<>();

        centerPoints.put(-1, getPoint(roomLoverOwner, -1));
        centerPoints.put(0, getPoint(roomLoverPlayer, 0));
        AvRoomDataManager.get().mMicPointMap = centerPoints;
    }

    private Point getPoint(View itemView, int position) {
        int[] location = new int[2];
        int[] nameLocation = new int[2];
        // 找到头像
        View nameView = itemView.findViewById(R.id.nick);
        View view = itemView.findViewById(R.id.micro_layout);
        if (view != null) {
            itemView = view;
        }
        itemView.getLocationInWindow(location);
        nameView.getLocationInWindow(nameLocation);
        int x = (location[0] + itemView.getWidth() / 2) - giftWidth / 2;
        int y = location[1] >= nameLocation[1] ?
                ((location[1] + itemView.getHeight() * 7 / 8) - giftHeight / 2) :
                ((nameLocation[1] + nameView.getHeight() / 2) - giftHeight / 2);

        // 放置表情占位image view
        if (faceImageViews.get(position) == null) {
            LayoutParams params = new LayoutParams(faceWidth, faceHeight);
            itemView.getLocationInWindow(location);
            int[] containerLocation = new int[2];
            this.getLocationInWindow(containerLocation);
            params.leftMargin = ((location[0] - containerLocation[0] + itemView.getWidth() / 2) - faceWidth / 2);
            params.topMargin = ((location[1] - containerLocation[1] + itemView.getHeight() / 2) - faceHeight / 2);
            L.debug(TAG, "function:sendFace position = %d, params.leftMargin = %d, params.topMargin = %d", position, params.leftMargin, params.topMargin);
            ImageView imageView = new ImageView(getContext());
            imageView.setLayoutParams(params);
            faceImageViews.put(position, imageView);
            addView(imageView);
        }
        return new Point(x, y);
    }

    @Override
    public void onReceiveFace(List<FaceReceiveInfo> faceReceiveInfos) {
        L.debug(TAG, "function:sendFace RoomLoverChairView onReceiveFace faceReceiveInfos: %s", faceReceiveInfos);
        if (faceReceiveInfos == null || faceReceiveInfos.size() <= 0) return;
        for (FaceReceiveInfo faceReceiveInfo : faceReceiveInfos) {
            int position = AvRoomDataManager.get().getMicPosition(faceReceiveInfo.getUid());
            if (position < -1) continue;
            ImageView imageView = faceImageViews.get(position);
            if (imageView == null) {
                continue;
            }
            L.debug(TAG, "function:sendFace position = %d, imageView.getX() = %f, imageView.getY() = %f, imageView.getWidth() = %d, imageView.getHeight() = %d",
                    position, imageView.getX(), imageView.getY(), imageView.getWidth(), imageView.getHeight());
            AnimationDrawable drawable = AnimFaceFactory.get(faceReceiveInfo, getContext(), imageView.getWidth(), imageView.getHeight());
            if (drawable == null) {
                continue;
            }
            imageView.setImageDrawable(drawable);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            drawable.setOneShot(true);
            drawable.start();
        }
    }

    @Override
    public void updateChairView(SparseArray<RoomQueueInfo> roomQueue) {
        roomLoverOwner.setChairItemData(roomQueue.valueAt(0), true);
        roomLoverPlayer.setChairItemData(roomQueue.valueAt(1), false);
    }

    @Override
    public void showOwnerClickDialog(RoomMicInfo roomMicInfo, int micPosition, final long roomUid) {
        if (roomMicInfo == null) {
            return;
        }
        List<ButtonItem> buttonItems = new ArrayList<>(4);
        final ButtonItem buttonItem1 = new ButtonItem(getContext().getString(R.string.embrace_up_mic), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                RoomInviteActivity.openActivity((FragmentActivity) getContext(), micPosition);
            }
        });
        ButtonItem buttonItem2 = new ButtonItem(roomMicInfo.isMicMute() ? getContext().getString(R.string.no_forbid_mic) : getContext().getString(R.string.forbid_mic), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (roomMicInfo.isMicMute()) {
                    mPresenter.openMicroPhone(micPosition);
                } else {
                    mPresenter.closeMicroPhone(micPosition);
                }
            }
        });
        ButtonItem buttonItem3 = new ButtonItem(roomMicInfo.isMicLock() ? getContext().getString(R.string.unlock_mic) :
                getContext().getString(R.string.lock_mic), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (roomMicInfo.isMicLock()) {
                    mPresenter.unLockMicroPhone(micPosition);
                } else {
                    mPresenter.lockMicroPhone(micPosition, roomUid);
                }
            }
        });

        buttonItems.add(buttonItem1);
        buttonItems.add(buttonItem2);
        buttonItems.add(buttonItem3);
        mDialogManager.showCommonPopupDialog(buttonItems, getContext().getString(R.string.cancel));
    }

    @OnClick(R2.id.contribute_list)
    public void onContributeClick() {
        Context context = getContext();
        if (context instanceof FragmentActivity) {
            BigListDataDialog bigListDataDialog = BigListDataDialog.newContributionListInstance(context);
            bigListDataDialog.setSelectOptionAction(new BigListDataDialog.SelectOptionAction() {
                @Override
                public void optionClick() {
                    mDialogManager.showProgressDialog(context, "请稍后");
                }

                @Override
                public void onDataResponse() {
                    //请求结束前退出可能会导致奔溃，直接捕获没关系
                    try {
                        mDialogManager.dismissDialog();
                    } catch (Exception e) {

                    }
                }
            });
            bigListDataDialog.show(((FragmentActivity) context).getSupportFragmentManager());
        }
    }

    @OnClick(R2.id.room_lover_owner)
    public void onOwnerClick() {
        mMicPosition = mPresenter.mCurrentOwnerPositionInQueue;
        mPresenter.microPhonePositionClick(mMicPosition);
    }

    @OnClick(R2.id.room_lover_player)
    public void onPlayerClick() {
        mMicPosition = mPresenter.mLoverPositionInQueue;
        mPresenter.microPhonePositionClick(mMicPosition);
    }

    @Override
    public SparseArray<ButtonItem> getAvatarButtonItemList(int micPosition, ChatRoomMember chatRoomMember, RoomInfo currentRoom) {
        if (chatRoomMember == null || currentRoom == null) {
            return null;
        }
        SparseArray<ButtonItem> buttonItemMap = new SparseArray<>(10);
        ButtonItem buttonItem1 = ButtonItemFactory.createSendGiftItem(getContext(), chatRoomMember, this);
        ButtonItem buttonItem2 = ButtonItemFactory.createLockMicItem(mMicPosition, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                mPresenter.closeMicroPhone(mMicPosition);
            }
        });
        ButtonItem buttonItem3 = ButtonItemFactory.createKickDownMicItem(chatRoomMember.getAccount());
        ButtonItem buttonItem4 = ButtonItemFactory.createKickOutRoomItem(getContext(), chatRoomMember, String.valueOf(currentRoom.getRoomId()), chatRoomMember.getAccount());
        ButtonItem buttonItem5 = ButtonItemFactory.createCheckUserInfoDialogItem(getContext(), chatRoomMember.getAccount());
        ButtonItem buttonItem6 = ButtonItemFactory.createDownMicItem();
        ButtonItem buttonItem7 = ButtonItemFactory.createFreeMicItem(mMicPosition, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                mPresenter.openMicroPhone(mMicPosition);
            }
        });
        ButtonItem buttonItem8 = ButtonItemFactory.createMarkBlackListItem(getContext(), chatRoomMember, String.valueOf(currentRoom.getRoomId()));
        buttonItemMap.put(0, buttonItem1);
        buttonItemMap.put(1, buttonItem2);
        buttonItemMap.put(2, buttonItem3);
        buttonItemMap.put(3, buttonItem4);
        buttonItemMap.put(4, buttonItem5);
        buttonItemMap.put(5, buttonItem6);
        buttonItemMap.put(6, buttonItem7);
        buttonItemMap.put(7, buttonItem8);
        return buttonItemMap;
    }

    @Override
    public void showOwnerSelfInfo(ChatRoomMember chatRoomMember) {
        if (isActivityDestroyed(getContext())) {
            return;
        }
        UserInfoDialogManager.showDialogFragment(getContext(), JavaUtil.str2long(chatRoomMember.getAccount()));
    }

    @Override
    public void showMicAvatarClickDialog(List<ButtonItem> buttonItems) {
        if (ListUtils.isListEmpty(buttonItems)) {
            return;
        }
        if (isActivityDestroyed(getContext())) {
            return;
        }
        mDialogManager.showCommonPopupDialog(buttonItems, getContext().getString(R.string.cancel));
    }

    @Override
    public void showGiftDialog(ChatRoomMember chatRoomMember) {
        if (isActivityDestroyed(getContext())) {
            return;
        }
        GiftDialog giftDialog = new GiftDialog(getContext(), AvRoomDataManager.get().mMicQueueMemberMap, chatRoomMember);
        giftDialog.setGiftDialogBtnClickListener(this);
        giftDialog.show();
    }

    @Override
    public void onRechargeBtnClick() {
        MyWalletNewActivity.start(getContext());
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number, int sendGiftType) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) return;
        if (giftInfo == null)
            return;
        List<Long> uids = new ArrayList<>();
        uids.add(uid);
        CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), uids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number, int sendGiftType) {
        if (giftInfo == null) return;
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) return;
        List<Long> targetUids = new ArrayList<>();
        for (int i = 0; i < micMemberInfos.size(); i++) {
            targetUids.add(micMemberInfos.get(i).getUid());
        }
        CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), targetUids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
    }

    @Override
    public void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid) {
        if (giftInfo != null) {
            L.debug(GiftAction.TAG, "HomePartyRoomFragment onSendCpGiftBtnClick giftInfo=%s, uid=%d", giftInfo, uid);
            LogUtil.d("HomePartyRoomFrag onSendCpGiftBtnClick");
            RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (currentRoomInfo == null) return;
            CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftInfo.getGiftId(), uid,
                    1, currentRoomInfo.getUid(), this);
        }
    }

    @Override
    public void onNeedCharge() {
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    MyWalletNewActivity.start(getContext());
                    fragment.dismiss();
                }
            }
        }).show(((FragmentActivity) getContext()).getSupportFragmentManager(), "charge");
    }

    @Override
    public void updateSpeakState(int position, boolean hasSound) {
        if (position == -1) {
            if (hasSound) {
                roomLoverOwner.start();
            } else {
                roomLoverOwner.stop();
            }
        } else if (position == 0){
            if (hasSound) {
                roomLoverPlayer.start();
            } else {
                roomLoverPlayer.stop();
            }
        }
    }
}
