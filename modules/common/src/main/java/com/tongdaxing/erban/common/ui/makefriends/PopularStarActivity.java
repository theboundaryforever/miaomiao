package com.tongdaxing.erban.common.ui.makefriends;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.presenter.makefriedns.IPopularStarView;
import com.tongdaxing.erban.common.presenter.makefriedns.PopularStarPresenter;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 魅力之星 页面
 */
@CreatePresenter(PopularStarPresenter.class)
public class PopularStarActivity extends BaseMvpActivity<IPopularStarView, PopularStarPresenter> implements IPopularStarView {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private PopularStarListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular_star);
        ((AppToolBar) findViewById(R.id.base_title_bar)).setOnBackBtnListener(view -> finish());

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new PopularStarListAdapter(R.layout.item_popular_star, this);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            UserInfo info = (UserInfo) adapter.getItem(position);
            if (info != null) {
                NewUserInfoActivity.start(PopularStarActivity.this, info.getUid());
            }
        });
        mAdapter.setOnToFindHerBtnClickListener(view -> {//去找TA
            getDialogManager().showProgressDialog(PopularStarActivity.this, getResources().getString(R.string.please_wait));
            //检查是否在房间
            getMvpPresenter().checkUserInRoom(Long.valueOf(view.getTag().toString()));
        });
        recyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getMvpPresenter().refreshData();
        });
        mAdapter.setOnLoadMoreListener(() -> {
            getMvpPresenter().loadMoreData();
        }, recyclerView);

        onReloadData();//进来加载一次
    }

    @Override
    public void setupFailView(boolean isRefresh) {
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            showNetworkErr();
        } else {
            mAdapter.loadMoreFail();
        }
    }

    @Override
    public void setupSuccessView(List<UserInfo> popularStarList, boolean isRefresh) {
        hideStatus();
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            if (ListUtils.isListEmpty(popularStarList)) {
                showNoData();
                popularStarList = new ArrayList<>();
            }
            mAdapter.setNewData(popularStarList);
        } else {
            mAdapter.loadMoreComplete();
            if (!ListUtils.isListEmpty(popularStarList)) {
                mAdapter.addData(popularStarList);
            }
        }
        //不够10个的话，不显示加载更多
        if (popularStarList.size() < Constants.PAGE_SIZE) {
            mAdapter.setEnableLoadMore(false);
        }
    }

    @Override
    public void setupFindHerFail(String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = getResources().getString(R.string.state_load_error);
        }
        getDialogManager().dismissDialog();
        toast(msg);
    }

    @Override
    public void toHerRoom(long uid) {
        getDialogManager().dismissDialog();
        AVRoomActivity.start(this, uid);
    }

    @Override
    public void toUserInfoPage(long uid) {
        getDialogManager().dismissDialog();
        NewUserInfoActivity.start(this, uid);
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        showLoading();
        getMvpPresenter().refreshData();
    }

    @Override
    protected void onDestroy() {
        mAdapter.releaseAudioPlayers();//放onDestroy前面，否则有泄漏风险
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mAdapter.stopAudioPlayers();
    }
}
