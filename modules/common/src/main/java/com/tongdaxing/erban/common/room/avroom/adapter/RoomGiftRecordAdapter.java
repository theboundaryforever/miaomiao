package com.tongdaxing.erban.common.room.avroom.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/3/23.
 */

public class RoomGiftRecordAdapter extends RecyclerView.Adapter<RoomGiftRecordAdapter.GiftRecordViewHolder> {
    private List<ChatRoomMessage> datas = new ArrayList<>();
    private Context mContext;
    private OnGiftRecordListener onGiftRecordListener;

    public RoomGiftRecordAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public GiftRecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.list_item_gift_record, parent, false);
        GiftRecordViewHolder holder = new GiftRecordViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(GiftRecordViewHolder holder, int position) {
        ChatRoomMessage message = datas.get(position);
        CustomAttachment attachment = (CustomAttachment) message.getAttachment();
        String nick = "";
        String targetNick = "全麦";
        String num = "0";
        String name = "";
        if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
            //单人礼物
            GiftAttachment giftAttachment = (GiftAttachment) attachment;
            GiftReceiveInfo giftRecieveInfo = giftAttachment.getGiftRecieveInfo();
            if (giftRecieveInfo == null)
                return;
            GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftRecieveInfo.getGiftId());
            if (giftInfo != null) {
                nick = giftAttachment.getGiftRecieveInfo().getNick();
                targetNick = giftAttachment.getGiftRecieveInfo().getTargetNick();
//                if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
//                    nick = nick.substring(0, 6) + "...";
//                }
//                if (!TextUtils.isEmpty(targetNick) && targetNick.length() > 6) {
//                    targetNick = targetNick.substring(0, 6) + "...";
//                }
                name = giftInfo.getGiftName();
                num = "X" + giftAttachment.getGiftRecieveInfo().getGiftNum();
                ImageLoadUtils.loadImage(mContext, giftInfo.getGiftUrl(), holder.ivGiftLogo);
            }
        } else {
            //多人礼物
            MultiGiftAttachment giftAttachment = (MultiGiftAttachment) attachment;
            MultiGiftReceiveInfo multiGiftRecieveInfo = giftAttachment.getMultiGiftRecieveInfo();
            if (multiGiftRecieveInfo == null) {
                return;
            }

            //计算到底是全麦礼物还是多人非全麦礼物 多人非全麦礼物显示最后一个被赠送者的昵称
            SparseArray<RoomQueueInfo> sparseArray = AvRoomDataManager.get().mMicQueueMemberMap;//size为9
            int micUserNum = 0;
            for (int i = 0; i < sparseArray.size(); i++) {
                RoomQueueInfo roomQueueInfo = sparseArray.valueAt(i);
                if (roomQueueInfo != null) {
                    ChatRoomMember c = roomQueueInfo.mChatRoomMember;
                    if (c != null && !AvRoomDataManager.get().isRoomOwner(c.getAccount())) {//先把房主排除
                        micUserNum++;
                    }
                }
            }
            micUserNum++;//再把房主算进去

            long senderUid = giftAttachment.getMultiGiftRecieveInfo().getUid();
            if (AvRoomDataManager.get().isOnMic(senderUid)) {
                //赠送者在麦上
                micUserNum--;
            }
            if (multiGiftRecieveInfo.getTargetUids().size() < micUserNum && multiGiftRecieveInfo.getNickList() != null && multiGiftRecieveInfo.getNickList().size() > 0) {
                //多人 只显示最后一个人
                //"赠送者 送给 被赠送者 "
                targetNick = multiGiftRecieveInfo.getNickList().get(multiGiftRecieveInfo.getNickList().size() - 1);
            }
            //计算

            GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(multiGiftRecieveInfo.getGiftId());
            if (giftInfo != null) {
                nick = giftAttachment.getMultiGiftRecieveInfo().getNick();
//                if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
//                    nick = nick.substring(0, 6) + "...";
//                }
                name = giftInfo.getGiftName();
                num = "X" + giftAttachment.getMultiGiftRecieveInfo().getGiftNum();
                ImageLoadUtils.loadImage(mContext, giftInfo.getGiftUrl(), holder.ivGiftLogo);
            }
        }
        holder.tvNick.setText(nick);
        holder.tvTarget.setText(targetNick);
        holder.tvGiftName.setText(name);
        holder.tvGiftNum.setText(num);
        holder.tvTime.setText(TimeUtils.getPostTimeString(mContext, message.getTime(), true, false));
        holder.rlItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onGiftRecordListener != null && message != null)
                    onGiftRecordListener.OnGiftRecordItemListener(message.getFromAccount());
            }
        });
    }

    public void setDatas(List<ChatRoomMessage> datas) {
        if (datas != null && !datas.isEmpty()) {
            if (this.datas != null && !this.datas.isEmpty())
                this.datas.clear();
            this.datas.addAll(datas);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public void setOnGiftRecordListener(OnGiftRecordListener onGiftRecordListener) {
        this.onGiftRecordListener = onGiftRecordListener;
    }

    public interface OnGiftRecordListener {
        void OnGiftRecordItemListener(String account);
    }

    static class GiftRecordViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout rlItem;
        private ImageView ivGiftLogo;
        private TextView tvNick, tvTarget, tvGiftName, tvGiftNum, tvTime;

        GiftRecordViewHolder(View itemView) {
            super(itemView);
            rlItem = itemView.findViewById(R.id.rl_gift_record);
            ivGiftLogo = itemView.findViewById(R.id.iv_gift_record);
            tvNick = itemView.findViewById(R.id.tv_gift_record_nick);
            tvTarget = itemView.findViewById(R.id.tv_gift_record_target);
            tvGiftName = itemView.findViewById(R.id.tv_gift_record_name);
            tvGiftNum = itemView.findViewById(R.id.tv_gift_record_num);
            tvTime = itemView.findViewById(R.id.tv_gift_record_time);
        }
    }
}
