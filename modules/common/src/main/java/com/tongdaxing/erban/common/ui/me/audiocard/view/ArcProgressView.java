package com.tongdaxing.erban.common.ui.me.audiocard.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

/**
 * 弧形进度条 两端是用圆绘制，来变现圆角的（下面的头圆指的是进度条头部的圆，尾圆同理）
 * <p>
 * 该View分为几种状态：
 * 1. 初始状态
 * 2. 进度条正在走
 * 3. 进度条停止绘制（可能是用户松手，也可能是时间到了）
 * <p>
 * Created by zhangjian on 2019/3/15.
 */
public class ArcProgressView extends View {
    private Paint progressPaint;//绘制弧形进度条
    private Paint roundHalfCirclePaint;//绘制头圆 尾圆

    private int width, height;
    private float outRadius;//最外侧圆的半径
    private float progressCircleRadius;//进度中头圆的半径
    private RectF rectF;//灰色圆底的矩形范围 画圆弧时需要使用
    private float ringWidth;//圆环宽度
    private int progress = 0;//实时进度
    private int maxProgress = 3000;//最大进度 毫秒
    private boolean progressDrawing = false;//是否正在绘制进度条
    private ValueAnimator ringValueAnimator;

    public ArcProgressView(Context context) {
        this(context, null, 0);
    }

    public ArcProgressView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ArcProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        ringWidth = DisplayUtils.dip2px(context, 10);
        progressCircleRadius = ringWidth / 2;

        progressPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setColor(0xFFFDF6A4);
        progressPaint.setStrokeWidth(ringWidth);

        roundHalfCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        roundHalfCirclePaint.setStyle(Paint.Style.FILL);
        roundHalfCirclePaint.setColor(0xFFFDF6A4);

        rectF = new RectF();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (widthMode == MeasureSpec.EXACTLY) {
            // layout_width为设置了明确的值或者是MATCH_PARENT
            width = MeasureSpec.getSize(widthMeasureSpec);
        } else {
            width = getPaddingLeft() + 200 + getPaddingRight();
        }

        if (heightMode == MeasureSpec.EXACTLY) {
            height = MeasureSpec.getSize(heightMeasureSpec);
        } else {
            if (widthMode == MeasureSpec.EXACTLY) {
                height = width;
            } else {
                height = getPaddingTop() + 200 + getPaddingBottom();
            }
        }
        setMeasuredDimension(width, height);
    }

    /**
     * 确定最终的宽高，进行相应的一些计算
     *
     * @param w    Current width of this view.
     * @param h    Current height of this view.
     * @param oldw Old width of this view.
     * @param oldh Old height of this view.
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        width = w;
        height = h;
        outRadius = width / 2;

        rectF.set(-outRadius + ringWidth / 2, -outRadius + ringWidth / 2,
                outRadius - ringWidth / 2, outRadius - ringWidth / 2);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.translate(width / 2, height / 2);
        // 进度头是一个圆 这个圆会占据一个角度 尾部的圆也是如此 这2个角度在计算实际进度对应的角度时
        if (progress == maxProgress) {
            Log.d("ProgressRoundArcView", "progress == maxProgress");
            // 达到最大进度 绘制没有普通圆环
            canvas.drawCircle(0, 0, outRadius - ringWidth / 2, progressPaint);
        } else {
            float theta = (float) Math.atan(Math.sqrt(Math.pow(outRadius - progressCircleRadius, 2) - Math.pow(progressCircleRadius, 2)) / progressCircleRadius);//弧度
            // 绘制进度条圆弧
            canvas.drawArc(rectF, -(float) (180 / Math.PI * theta)
                    , (float) ((float) progress / maxProgress * (360 - (90 - 180 / Math.PI * theta) * 2)), false, progressPaint);// 角度 = 180 / Math.PI * 弧度

            if (progressDrawing) {
                //当前进度所在的角度 从接近-90度开始 到 接近270度
                float currentAngle = -(float) (180 / Math.PI * theta) + (float) ((float) progress / maxProgress * (360 - (90 - 180 / Math.PI * theta) * 2));
                //计算尾圆圆心的坐标
                float endRoundProgressX = (float) ((outRadius - progressCircleRadius) * Math.cos(currentAngle * Math.PI / 180));//cos需要用弧度计算
                if (currentAngle > 90 && currentAngle < 360) {
                    endRoundProgressX = -Math.abs(endRoundProgressX);
                }
                float endRoundProgressY = (float) ((outRadius - progressCircleRadius) * Math.sin(currentAngle * Math.PI / 180));
                if (currentAngle < 0 || currentAngle > 180) {
                    endRoundProgressY = -Math.abs(endRoundProgressY);
                }
                // 绘制头圆
                canvas.drawCircle(progressCircleRadius, -(float) Math.sqrt(Math.pow(outRadius - progressCircleRadius, 2) - Math.pow(progressCircleRadius, 2)),
                        progressCircleRadius, roundHalfCirclePaint);
                // 绘制尾圆
                canvas.drawCircle(endRoundProgressX,
                        endRoundProgressY, progressCircleRadius, roundHalfCirclePaint);
            }
        }
    }

    /**
     * 进度条开始动画
     */
    public void startAnim() {
        if (ringValueAnimator != null) {
            if (!progressDrawing) {
                progress = 0;
                progressDrawing = true;
                ringValueAnimator.start();
            }
        }
    }

    /**
     * 进度条停止动画
     */
    public void stopAnim() {
        if (ringValueAnimator != null) {
            ringValueAnimator.cancel();
            progressDrawing = false;
        }
    }

    /**
     * 进度条是否还在绘制
     *
     * @return true 正在绘制 false 已停止绘制
     */
    public boolean isProgressDrawing() {
        return progressDrawing;
    }

    /**
     * 还原为初始状态
     */
    public void backToInitState() {
        if (ringValueAnimator != null) {
            ringValueAnimator.cancel();
        }
        progressDrawing = false;
        progress = 0;
        invalidate();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (ringValueAnimator != null) {
            ringValueAnimator.cancel();
            progressDrawing = false;
        }
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
        invalidate();
    }

    public void setMaxProgress(int maxProgress) {
        this.maxProgress = maxProgress;

        ringValueAnimator = ValueAnimator.ofInt(0, maxProgress)
                .setDuration(maxProgress);
        ringValueAnimator.setInterpolator(new LinearInterpolator());
        ringValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                setProgress((Integer) animation.getAnimatedValue());
            }
        });
        ringValueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                progressDrawing = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                progressDrawing = false;
            }

            @Override
            public void onAnimationPause(Animator animation) {
                super.onAnimationPause(animation);
                progressDrawing = false;
            }
        });
    }
}
