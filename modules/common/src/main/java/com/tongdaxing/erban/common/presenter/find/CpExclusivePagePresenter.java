package com.tongdaxing.erban.common.presenter.find;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.ui.find.model.CpExclusivePageModel;
import com.tongdaxing.erban.common.ui.find.view.ICpExclusivePageView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.cp.CpEditSignEvent;
import com.tongdaxing.xchat_core.find.bean.CpUserPageVo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Function:
 * Author: Edward on 2019/3/28
 */
public class CpExclusivePagePresenter extends AbstractMvpPresenter<ICpExclusivePageView> {
    private CpExclusivePageModel cpExclusivePageModel;

    public CpExclusivePagePresenter() {
        cpExclusivePageModel = new CpExclusivePageModel();
    }

    @Override
    public void onCreatePresenter(@Nullable Bundle saveState) {
        super.onCreatePresenter(saveState);
        CoreUtils.register(this);
    }

    public void loadData(long uid) {
        cpExclusivePageModel.getCpPageData(uid, new OkHttpManager.MyCallBack<ServiceResult<CpUserPageVo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().loadFailure(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<CpUserPageVo> response) {
                if (getMvpView() == null || response == null || response.getData() == null) {
                    onError(new Exception());
                    return;
                }
                getMvpView().loadSucceed(response.getData());
            }
        });
    }

    public void relieveCp() {
        cpExclusivePageModel.getRelieveCp(new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().relieveCpFailure();
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response == null || getMvpView() == null) {
                    return;
                }
                if (response.num("code") == 200) {
                    CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid());//更新本地登录用户信息
                    getMvpView().relieveCpSucceed();
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setCpSignSucceed(CpEditSignEvent.OnEditSuccess onEditSuccess) {
        getMvpView().onFreshSignFromEdit(onEditSuccess.getSign());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setCpSigNeedCharge(CpEditSignEvent.OnEditNeedCharge onEditNeedCharge) {
        getMvpView().onNeedCharge();
    }


    @Override
    public void onDestroyPresenter() {
        super.onDestroyPresenter();
        CoreUtils.unregister(this);
    }
}
