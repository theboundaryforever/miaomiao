package com.tongdaxing.erban.common.ui.makefriends;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.erban.ui.baseview.BaseDialogFragment;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.util.EasyTimer;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.xchat_core.makefriedns.BroadcastMsgEvent;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.coremanager.IAppInfoCore;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.uuzuche.lib_zxing.DisplayUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 寻友广播弹窗
 * <p>
 * Created by zhangjian on 2019/6/20.
 */
@Route(path = FindFriendsBroadCastRoute.SEND_DIALOG)
public class RoomBroadcastSendDialog extends BaseDialogFragment {
    @BindView(R2.id.dialog_room_broadcast_msg_content)
    EditText dialogRoomBroadcastMsgContent;
    @BindView(R2.id.dialog_room_broadcast_msg_btn)
    TextView dialogRoomBroadcastMsgBtn;
    @BindView(R2.id.dialog_room_broadcast_msg_btn_disable)
    TextView dialogRoomBroadcastMsgBtnDisable;

    private TextWatcher mTextWatcher;

    private EasyTimer easyTimer = new EasyTimer();
    private int waitingTime;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null && getContext() != null) {
                int color = ContextCompat.getColor(getContext(), android.R.color.transparent);
                window.setBackgroundDrawable(new ColorDrawable(color));
                window.setLayout(DisplayUtil.dip2px(getContext(), 301), DisplayUtil.dip2px(getContext(), 265));
            }
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.dialog_room_broadcast_msg_send;
    }

    @Override
    public void initBefore() {

    }

    @Override
    public void findView() {
        ButterKnife.bind(this, mContentView);
        CoreUtils.register(this);
        CoreUtils.send(new BroadcastMsgEvent.OnBroadcastSendDialogOpen());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CoreUtils.unregister(this);
        removeEasyTimer();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveSendCd(BroadcastMsgEvent.OnMsgSendCD onMsgSendCD) {
        waitingTime = onMsgSendCD.getWaitingTime();
        if (waitingTime == 0) {
            enableBtnSend();
        } else if (waitingTime > 0) {
            //显示倒计时
            startTimer();
        }
    }

    private void enableBtnSend() {
        dialogRoomBroadcastMsgBtn.setVisibility(View.VISIBLE);
        dialogRoomBroadcastMsgBtnDisable.setVisibility(View.GONE);
        //主动弹出输入法
        dialogRoomBroadcastMsgContent.setFocusableInTouchMode(true);
        dialogRoomBroadcastMsgContent.requestFocus();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputManager = (InputMethodManager) dialogRoomBroadcastMsgContent.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(dialogRoomBroadcastMsgContent, 0);
            }
        }, 100);
    }

    private void startTimer() {
        easyTimer.setDuration(1000);
        easyTimer.setRunnable(new Runnable() {
            @Override
            public void run() {
                dialogRoomBroadcastMsgBtnDisable.setText(waitingTime-- + "s");
                if (waitingTime < 0) {
                    enableBtnSend();
                    removeEasyTimer();
                }
            }
        });
        easyTimer.start();
    }

    private void removeEasyTimer() {
        if (easyTimer != null) {
            easyTimer.stop();
            easyTimer = null;
        }
    }

    @Override
    public void setView() {
        String broadCastMsgContent = AvRoomDataManager.get().getBroadCastMsgContent();
        if (!TextUtils.isEmpty(broadCastMsgContent)) {
            dialogRoomBroadcastMsgContent.setText(broadCastMsgContent);
            dialogRoomBroadcastMsgContent.setSelection(broadCastMsgContent.length());
        }
        mTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null && s.length() > 20) {
                    SingleToastUtil.showToast("已超过20个字符，请检查输入内容");

                }
            }
        };
        dialogRoomBroadcastMsgContent.addTextChangedListener(mTextWatcher);
    }

    @Override
    public void setListener() {
    }

    @OnClick(R2.id.dialog_room_broadcast_msg_close)
    public void onDialogRoomBroadcastMsgCloseClicked() {
        dismissAllowingStateLoss();
    }

    @OnClick(R2.id.dialog_room_broadcast_msg_btn)
    public void onDialogRoomBroadcastMsgBtnClicked() {
        //长度检查
        String text = dialogRoomBroadcastMsgContent.getText().toString().trim();
        if (text.length() > 20) {
            SingleToastUtil.showToast("发送失败，内容过长");
            return;
        }
        //敏感词检查
        String sensitiveWordData = CoreManager.getCore(IAppInfoCore.class).getSensitiveWord();

        if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(text)) {
            Pattern pattern = Pattern.compile(sensitiveWordData);
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                SingleToastUtil.showToast("发送失败，小喵提醒您文明用语~");
                return;
            }
        }
        // 由MainPresenter发广播消息
        CoreUtils.send(new BroadcastMsgEvent.OnBroadcastSendDialogSend(text));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDialogNeedDismiss(BroadcastMsgEvent.OnBroadcastSendDialogNeedDismiss dismiss) {
        dismissAllowingStateLoss();
    }
}
