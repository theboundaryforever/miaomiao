package com.tongdaxing.erban.common.ui.find.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupImInfo;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

public class VoiceGroupIMAdapter extends BaseQuickAdapter<VoiceGroupImInfo, BaseViewHolder> {
    public VoiceGroupIMAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, VoiceGroupImInfo item) {
        ImageView ivUserHead = helper.getView(R.id.iv_user_head);
        ImageLoadUtils.loadCircleImage(mContext, item.getFromAvatar(), ivUserHead, R.drawable.default_cover);
        helper.setText(R.id.tv_user_name, item.getFromNick());
        helper.setText(R.id.tv_content, item.getContext());
        helper.setText(R.id.tv_time, TimeUtils.getPostTimeString(mContext,
                item.getCreateDate(), true, false));
    }
}
