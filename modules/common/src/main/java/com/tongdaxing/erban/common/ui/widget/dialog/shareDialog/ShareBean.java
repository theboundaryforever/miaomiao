package com.tongdaxing.erban.common.ui.widget.dialog.shareDialog;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * <p>
 * Created by zhangjian on 2019/6/19.
 */
public class ShareBean implements MultiItemEntity {
    public static final int TYPE_NOTICE_FANS = 6;
    public static final String UMENG_TYPE_NOTICE_FANS = "notice_fans";
    public static final int TYPE_BROADCAST = 0;
    public static final String UMENG_TYPE_BROADCAST = "find_friends_broadcast";
    public static final int TYPE_FRIEND = 1;
    public static final String UMENG_TYPE_FRIEND = "invite_friends";
    public static final int TYPE_WECHAT = 2;
    public static final String UMENG_TYPE_WECHAT = "wechat";
    public static final int TYPE_WECHAT_MOMENTS = 3;
    public static final String UMENG_TYPE_WECHAT_MOMENTS = "wechat_moments";
    public static final int TYPE_QQ = 4;
    public static final String UMENG_TYPE_QQ = "qq";
    public static final int TYPE_QZONE = 5;
    public static final String UMENG_TYPE_QZONE = "qzone";

    private int type;
    private int nameResId;
    private int resId;
    private boolean showTag;
    private int tagResId = -1;
    private String countDown;
    private int itemType;

    public ShareBean(int type, int nameResId, int resId) {
        this.type = type;
        this.nameResId = nameResId;
        this.resId = resId;
    }

    public ShareBean(int type, int nameResId, int resId, boolean showTag, int tagResId) {
        this.type = type;
        this.nameResId = nameResId;
        this.resId = resId;
        this.showTag = showTag;
        this.tagResId = tagResId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNameResId() {
        return nameResId;
    }

    public void setNameResId(int nameResId) {
        this.nameResId = nameResId;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public boolean isShowTag() {
        return showTag;
    }

    public void setShowTag(boolean showTag) {
        this.showTag = showTag;
    }

    public int getTagResId() {
        return tagResId;
    }

    public void setTagResId(int tagResId) {
        this.tagResId = tagResId;
    }

    public String getCountDown() {
        return countDown;
    }

    public void setCountDown(String countDown) {
        this.countDown = countDown;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}
