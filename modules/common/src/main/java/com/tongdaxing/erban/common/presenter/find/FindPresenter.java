package com.tongdaxing.erban.common.presenter.find;

import com.tongdaxing.erban.common.ui.find.view.IFindView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;

/**
 * Function:
 * Author: Edward on 2019/3/8
 */
public class FindPresenter extends AbstractMvpPresenter<IFindView> {
}
