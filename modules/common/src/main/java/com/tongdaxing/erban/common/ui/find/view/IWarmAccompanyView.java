package com.tongdaxing.erban.common.ui.find.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.find.bean.WarmAccompanyInfo;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public interface IWarmAccompanyView extends IMvpBaseView {
    void setupFailView(boolean isRefresh);

    void setupSuccessView(List<WarmAccompanyInfo> voiceGroupInfoList, boolean isRefresh);
}
