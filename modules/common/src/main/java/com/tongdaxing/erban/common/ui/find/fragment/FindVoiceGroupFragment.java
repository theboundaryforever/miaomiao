package com.tongdaxing.erban.common.ui.find.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tongdaxing.erban.common.base.adapter.BaseIndicatorAdapter;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.presenter.find.VoiceGroupPresenter;
import com.tongdaxing.erban.common.ui.find.activity.PublishVoiceGroupActivity;
import com.tongdaxing.erban.common.ui.find.view.IVoiceGroupView;
import com.tongdaxing.erban.common.ui.home.adpater.CommonImageIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Function:发现页-声圈
 * Author: Edward on 2019/3/12
 */
@CreatePresenter(VoiceGroupPresenter.class)
public class FindVoiceGroupFragment extends BaseMvpFragment<IVoiceGroupView, VoiceGroupPresenter> implements IVoiceGroupView,
        CommonMagicIndicatorAdapter.OnItemSelectListener {

    public static final String TAG = "FindVoiceGroupFragment";
    private MagicIndicator indicator;
    private ViewPager viewPager;
    private int mTabPosition = 0;
    private ImageView mIvSend;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_voice_group;
    }

    @Override
    public void onFindViews() {
        indicator = (MagicIndicator) mView.findViewById(R.id.voice_indicator);
        viewPager = (ViewPager) mView.findViewById(R.id.voice_viewpager);
        mIvSend = mView.findViewById(R.id.voice_iv_send);
    }

    @Override
    public void onSetListener() {
        mIvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (userInfo == null) {
                    SingleToastUtil.showToast("操作失败，建议您重新登录！");
                    return;
                }
                if (userInfo.getCreateTime() <= 0) {
                    UIHelper.showAddInfoAct(getActivity());
                    return;
                }
                PublishVoiceGroupActivity.start(FindVoiceGroupFragment.this);
            }
        });
    }

    public int getFindVoiceGroupFragmentCurrentPage() {
        return mTabPosition;
    }

    @Override
    public void initiate() {
        List<Fragment> mTabs = new ArrayList<>();
        mTabs.add(new NewestFragment());
        mTabs.add(new RecommendFragment());
        mTabs.add(new LikeFragment());
        initMagicIndicator();
        viewPager.setAdapter(new BaseIndicatorAdapter(getChildFragmentManager(), mTabs));
        viewPager.setOffscreenPageLimit(mTabs.size());
        ViewPagerHelper.bind(indicator, viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                UmengEventUtil.getInstance().onFindMomentsTab(getContext(), UmengEventId.getFindMomentsItem(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initMagicIndicator() {
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(0, "最新"));
        tabInfoList.add(new TabInfo(1, "推荐"));
        tabInfoList.add(new TabInfo(2, "关注"));
        CommonImageIndicatorAdapter mMsgIndicatorAdapter = new CommonImageIndicatorAdapter(mContext, tabInfoList);
        mMsgIndicatorAdapter.setSize(18);
        mMsgIndicatorAdapter.setNormalColorId(R.color.common_color_14_transparent_87);
        mMsgIndicatorAdapter.setSelectColorId(R.color.color_000000);
        mMsgIndicatorAdapter.setImageIndicatorWidth(35.0f);
        mMsgIndicatorAdapter.setImageIndicatorHeight(14.0f);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        indicator.setNavigator(commonNavigator);
        // must after setNavigator
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
    }

    @Override
    public void onItemSelect(int position) {
        if (viewPager != null) {
            viewPager.setCurrentItem(position);
        }
        mTabPosition = position;
    }
}
