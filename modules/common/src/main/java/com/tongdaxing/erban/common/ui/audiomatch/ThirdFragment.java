package com.tongdaxing.erban.common.ui.audiomatch;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.ui.me.MePresenter;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;

@CreatePresenter(MePresenter.class)
public class ThirdFragment extends BaseFragment {

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_audio_match_third;
    }

    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {
    }
}
