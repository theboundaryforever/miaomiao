package com.tongdaxing.erban.common.ui.widget.dialog.shareDialog;

import android.os.CountDownTimer;

import com.tcloud.core.CoreUtils;

/**
 * <p>
 * Created by zhangjian on 2019/7/18.
 */
public class NewShareDialogCountDownTimer extends CountDownTimer {

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public NewShareDialogCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long millisUntilFinished) {
        String time;
        int countDownTime = (int) (millisUntilFinished / 1000);
        int minute = 0;
        int second;
        if (countDownTime >= 60) {
            minute = countDownTime / 60;
        }
        second = countDownTime % 60;
        if (second < 10) {
            time = minute + ":0" + second;
        } else {
            time = minute + ":" + second;
        }
        CoreUtils.send(new CountDownEvent(time));
    }

    @Override
    public void onFinish() {
        cancel();
        CoreUtils.send(new CountDownEvent(null));
    }
}