package com.tongdaxing.erban.common.constant;

public interface Extras {

    String EXTRA_JUMP_P2P = "EXTRA_JUMP_P2P";

    String EXTRA_DATA = "data";

    String EXTRA_FROM = "from";

    String EXTRA_FROM_NOTIFICATION = "from_notification";

    // 参数
    String EXTRA_ACCOUNT = "account";

    String EXTRA_CHANGE_INDEX = "index";//切换页面位置

}
