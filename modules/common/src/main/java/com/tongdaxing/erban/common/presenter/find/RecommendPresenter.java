package com.tongdaxing.erban.common.presenter.find;

import com.tongdaxing.erban.common.model.makefriends.PopularStarModel;
import com.tongdaxing.erban.common.ui.find.model.RecommendModel;
import com.tongdaxing.erban.common.ui.find.view.IRecommendView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.find.bean.RecommendModuleInfo;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public class RecommendPresenter extends AbstractMvpPresenter<IRecommendView> {
    private PopularStarModel popularStarModel;
    private RecommendModel recommendModel;
    private int currPage = Constants.PAGE_START;

    public RecommendPresenter() {
        popularStarModel = new PopularStarModel();
        recommendModel = new RecommendModel();
    }

    public void refreshData(String dataType, String queryUid) {
        loadData(Constants.PAGE_START, dataType, queryUid);
    }

    public void loadMoreData(String dataType, String queryUid) {
        loadData(currPage + 1, dataType, queryUid);
    }

    /**
     * 加载人气之星数据
     */
    private void loadRecommendModuleData() {
        recommendModel.loadRecommendModule(new OkHttpManager.MyCallBack<ServiceResult<List<RecommendModuleInfo>>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<List<RecommendModuleInfo>> response) {
                if (response != null && response.isSuccess() && response.getData() != null) {
                    getMvpView().insertRecommendModuleItemsData(response.getData());
                }
            }
        });
    }

    /**
     * 加载人气之星数据
     */
    private void loadPopularityStarData() {
        //只要8条数据
        popularStarModel.getDressUpData(1, 8, new OkHttpManager.MyCallBack<ServiceResult<List<UserInfo>>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<List<UserInfo>> response) {
                if (response != null && response.isSuccess() && response.getData() != null) {
                    getMvpView().insertPopularityStarItemsData(response.getData());
                }
            }
        });
    }

    public void loadData(int page, String dataType, String queryUid) {
        recommendModel.loadData(page, dataType, queryUid, new OkHttpManager.MyCallBack<ServiceResult<List<VoiceGroupInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(page == Constants.PAGE_START);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<VoiceGroupInfo>> response) {
                currPage = page;
                if (response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().setupSuccessView(response.getData(), page == Constants.PAGE_START);//直接展示数据
                        if (dataType.equals("3")) {//只有推荐页才有人气之星数据
                            loadPopularityStarData();//加载人气之星数据
                        } else if (dataType.equals("4")) {//只有喜欢页才有推荐圈友数据
                            loadRecommendModuleData();//加载推荐圈友
                        }
                    } else {
                        onError(new Exception());
                    }
                } else {
                    onError(new Exception());
                }

            }
        });
    }

    public void voiceGroupLike(int id) {
        recommendModel.voiceGroupLike(String.valueOf(id), new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    getMvpView().refreshAdapterLikeStatus(id);
                } else {
                    SingleToastUtil.showToast("数据错误!");
                }
            }
        });
    }
}
