package com.tongdaxing.erban.common.presenter.audio;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.AudioCardInfo;

public interface IAudioCardView extends IMvpBaseView {
    void getAudioIdentifyCard(AudioCardInfo audioCardInfo, String url, long audioDuration);

    void closeProgressDialog();
}
