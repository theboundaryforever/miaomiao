package com.tongdaxing.erban.common.ui.find.fragment;

import com.tongdaxing.erban.common.presenter.find.RecommendPresenter;
import com.tongdaxing.erban.common.ui.find.activity.VoiceGroupDetailActivity;
import com.tongdaxing.erban.common.ui.find.view.IRecommendView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.find.bean.RecommendModuleInfo;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupInfo;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;

import java.util.List;

import static com.tongdaxing.erban.common.ui.find.adapter.RecommendAdapter.RECOMMEND_MODULE_ITEM;

/**
 * Function:动态圈---喜欢
 * Author: Edward on 2019/3/12
 */
@CreatePresenter(RecommendPresenter.class)
public class LikeFragment extends RecommendFragment implements IRecommendView {
    @Override
    protected void startVoiceGroupDetailPage(VoiceGroupInfo voiceGroupInfo, int index) {
        VoiceGroupDetailActivity.start(getContext(), voiceGroupInfo, index);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onRecommendModulePraise(long likedUid) {
        if (recommendAdapter != null) {
            List<VoiceGroupInfo> list = recommendAdapter.getData();
            for (int i = 0; i < list.size(); i++) {
                VoiceGroupInfo info = list.get(i);
                if (info != null && info.getItemType() == RECOMMEND_MODULE_ITEM) {
                    List<RecommendModuleInfo> infoList = info.getRecommendModuleInfoList();
                    if (infoList != null && infoList.size() > 0) {
                        for (int j = 0; j < infoList.size(); j++) {
                            if (infoList.get(j).getUid() == likedUid) {
                                infoList.remove(j);
                                if (infoList.size() <= 0) {//检查列表中是否已经没有数据，如果没有则隐藏推荐圈友
                                    list.remove(i);
                                }
                                recommendAdapter.notifyDataSetChanged();
                                return;
                            }
                        }
                    } else {
                        list.remove(i);
                        recommendAdapter.notifyDataSetChanged();//推荐圈友没有数据则隐藏
                        return;
                    }
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onRecommendModulePraiseFail(String error) {
        toast(error);
    }

    @Override
    protected void setRefreshData() {
        getMvpPresenter().refreshData("4", "");
    }

    @Override
    protected void setLoadMoreData() {
        getMvpPresenter().loadMoreData("4", "");
    }

}
