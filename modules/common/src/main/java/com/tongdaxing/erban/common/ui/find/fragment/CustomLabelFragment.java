package com.tongdaxing.erban.common.ui.find.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.common.presenter.find.NotCustomLabelPresenter;
import com.tongdaxing.erban.common.ui.find.view.INotCustomLabelView;
import com.tongdaxing.erban.common.ui.find.widget.dialog.CreateRoomCustomLabelDialog;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.find.ICreateRoomClient;
import com.tongdaxing.xchat_core.find.bean.CreateRoomLabelInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;

import java.util.List;

/**
 * Function:自定义标签
 * Author: Edward on 2019/3/18
 */
@CreatePresenter(NotCustomLabelPresenter.class)
public class CustomLabelFragment extends NotCustomLabelFragment implements INotCustomLabelView {
    public static String TAG = "CustomLabelFragment";

    private void getCustomLabelInfo() {
        boolean flag = false;
        for (int i = 0; i < listBeans.size(); i++) {//如果已有自定义标签则不添加
            if (listBeans.get(i).getIsCustomLabel() == -1) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            CreateRoomLabelInfo.TagListBean noDelBean = new CreateRoomLabelInfo.TagListBean();
            noDelBean.setName("+自定义");
            noDelBean.setIsCustomLabel(-1);
            listBeans.add(noDelBean);
        }
    }

    @Override
    public void delLabelSucceed(int tagId) {
        for (int i = 0; i < listBeans.size(); i++) {
            if (listBeans.get(i).getId() == tagId) {
                listBeans.remove(i);
                adapter.notifyDataChanged();
                return;
            }
        }
    }

    @CoreEvent(coreClientClass = ICreateRoomClient.class)
    public void addLabelSucceed(int roomType, List<CreateRoomLabelInfo.TagListBean> listBeans) {
        if (this.roomType == roomType) {
            this.listBeans.clear();
            this.listBeans.addAll(listBeans);
            getCustomLabelInfo();
            if (adapter != null) {
                adapter.notifyDataChanged();
            }
        }
    }

    @Override
    protected TagAdapter<CreateRoomLabelInfo.TagListBean> getAdapter() {
        getCustomLabelInfo();
        TagAdapter<CreateRoomLabelInfo.TagListBean> adapter = new TagAdapter<CreateRoomLabelInfo.TagListBean>(listBeans) {
            @Override
            public View getView(FlowLayout parent, int position, CreateRoomLabelInfo.TagListBean s) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.flowlayout_create_room_custom_label, tflCreateRoomLabel, false);
                TextView textView = view.findViewById(R.id.tv_select);
                ImageView ivClose = view.findViewById(R.id.iv_close);
                if (s.getIsCustomLabel() == -1) {
                    textView.setTextColor(getResources().getColor(R.color.color_1A1A1A));
                    ivClose.setVisibility(View.GONE);
                    view.setBackgroundResource(R.drawable.shape_e0e0e0_radius_10dp);
                } else {
                    if (s.getType() == 0) {
                        ivClose.setVisibility(View.VISIBLE);
                        ivClose.setOnClickListener(v -> getMvpPresenter().delCustomLabel(s.getId()));
                    } else {
                        ivClose.setVisibility(View.GONE);
                    }
                    textView.setTextColor(getResources().getColor(s.isSelected() ? R.color.color_000000 : R.color.color_1A1A1A));
                    view.setBackgroundResource(s.isSelected() ? R.drawable.bg_create_room_tag : R.drawable.shape_e0e0e0_radius_10dp);
                }
                textView.setText(s.getName());
                return view;
            }
        };
        return adapter;
    }

    @Override
    protected void setClickImpl(TagAdapter<CreateRoomLabelInfo.TagListBean> adapter, int position) {
        CreateRoomLabelInfo.TagListBean bean = adapter.getItem(position);
        if (bean != null) {
            if (bean.getIsCustomLabel() == -1) {
                CreateRoomCustomLabelDialog dialog = new CreateRoomCustomLabelDialog();
                Bundle bundle = new Bundle();
                bundle.putInt("type", roomType);
                dialog.setArguments(bundle);
                dialog.show(getFragmentManager(), "");
                for (int i = 0; i < listBeans.size(); i++) {//全部设置为未选中
                    listBeans.get(i).setSelected(false);
                }
                adapter.notifyDataChanged();//刷新列表
            } else {
                super.setClickImpl(adapter, position);
            }
        }
    }
}
