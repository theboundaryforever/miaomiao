package com.tongdaxing.erban.common.room.audio.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import com.netease.nim.uikit.common.util.sys.NetworkUtil
import com.tongdaxing.erban.common.R
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity
import com.tongdaxing.erban.common.room.audio.adapter.MyMusicAdapter
import com.tongdaxing.erban.common.room.audio.adapter.MyMusicAdapter.*
import com.tongdaxing.erban.common.room.audio.adapter.SearchMusicAdapter
import com.tongdaxing.erban.common.room.audio.presenter.SearchMusicPresenter
import com.tongdaxing.erban.common.room.audio.view.ISearchMusicView
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter
import com.tongdaxing.erban.libcommon.utils.ListUtils
import com.tongdaxing.xchat_core.music.IMusicCore
import com.tongdaxing.xchat_core.music.IMusicDownloaderCore
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo
import com.tongdaxing.xchat_framework.coremanager.CoreEvent
import com.tongdaxing.xchat_framework.coremanager.CoreManager
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil
import kotlinx.android.synthetic.main.activity_search_music.*

/**
 * Function:
 * Author: Edward on 2019/2/15
 */
@CreatePresenter(SearchMusicPresenter::class)
class SearchMusicActivity : BaseMvpActivity<ISearchMusicView, SearchMusicPresenter>(), ISearchMusicView {
    private var myMusicAdapter: SearchMusicAdapter? = null

    override fun loadDataFailure(isRefresh: Boolean, msg: String?) {
        if (isRefresh) {
            if (ListUtils.isListEmpty(myMusicAdapter!!.data)) {
                showNetworkErr()
            } else {
                toast(msg)
            }
//            swipe_refresh.isRefreshing = false
        } else {
            myMusicAdapter!!.loadMoreFail()
        }
    }

    override fun refreshMyMusicList(isRefresh: Boolean, myMusicInfoList: MutableList<MyMusicInfo>?) {
        CoreManager.getCore(IMusicCore::class.java).setMusicStatus(myMusicInfoList)
        if (isRefresh) {
            hideStatus()
            if (myMusicInfoList!!.size == 0) {
                SingleToastUtil.showToast("没有搜索到您想要的数据!")
            } else {
                myMusicAdapter!!.setNewData(myMusicInfoList)
            }
        } else {
            if (!ListUtils.isListEmpty(myMusicInfoList)) {
                myMusicAdapter!!.loadMoreComplete()
                if (myMusicInfoList != null) {
                    myMusicAdapter!!.addData(myMusicInfoList)
                }
                myMusicAdapter!!.loadMoreComplete()
            } else {
                myMusicAdapter!!.loadMoreEnd(true)
            }
        }
        hintKeyBoard()
    }

    /**
     * 开始下载
     */
    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient::class)
    fun onHotMusicStartDownload(hotMusicInfo: HotMusicInfo) {
        notifyDataStatus(hotMusicInfo.id, FILE_STATUS_DOWNLOADING)
    }

    private fun notifyDataStatus(id: Long, status: Int) {
        if (myMusicAdapter != null) {
            val list = myMusicAdapter!!.getData()
            for (i in list.indices) {
                if (list[i].id == id) {
                    val myMusicInfo = list[i]
                    myMusicInfo.fileStatus = status
                    myMusicAdapter!!.setData(i, myMusicInfo)
                    myMusicAdapter!!.notifyDataSetChanged()
                    return
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient::class)
    fun onHotMusicDownloadCompleted(hotMusicInfo: HotMusicInfo) {
        notifyDataStatus(hotMusicInfo.id, FILE_STATUS_PLAY)
    }

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, SearchMusicActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_music)

        init()
        initAdapter()
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient::class)
    fun addSongSucceed(hotMusicInfo: HotMusicInfo) {
        notifyDataStatus(hotMusicInfo.id, FILE_STATUS_PLAY)
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient::class)
    fun onHotMusicDownloadError(msg: String, localMusicInfo: MusicLocalInfo, hotMusicInfo: HotMusicInfo) {
        notifyDataStatus(java.lang.Long.parseLong(localMusicInfo.songId), 0)
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient::class)
    fun addSongFailure(hotMusicInfo: HotMusicInfo) {
        notifyDataStatus(hotMusicInfo.id, FILE_STATUS_DOWNLOAD)
    }

    private fun initAdapter() {
        myMusicAdapter = SearchMusicAdapter(R.layout.adapter_my_music)
        myMusicAdapter!!.setOnItemClickListener { adapter, view, position ->
            val temp = adapter as MyMusicAdapter
            val myMusicLocalInfo = temp.getItem(position)
            if (myMusicLocalInfo == null) {
                toast(getString(R.string.data_exception))
                return@setOnItemClickListener
            }
            if (myMusicLocalInfo.fileStatus == FILE_STATUS_DOWNLOAD) {
                CoreManager.getCore(IMusicDownloaderCore::class.java).addHotMusicToDownQueue(convertBean(myMusicLocalInfo))
            }
        }
        setLoadMoreListener()
        recycler_view.adapter = myMusicAdapter
        recycler_view.layoutManager = LinearLayoutManager(this)
    }

    private fun convertBean(myMusicInfo: MyMusicInfo): HotMusicInfo {
        val localMusicInfo = HotMusicInfo()
        localMusicInfo.id = myMusicInfo.id
        localMusicInfo.singerName = myMusicInfo.artist
        localMusicInfo.singName = myMusicInfo.title
        localMusicInfo.singUrl = myMusicInfo.url.toString()
        return localMusicInfo
    }

    private fun init() {
        et_search.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val temp = et_search.text.toString()
                if (TextUtils.isEmpty(temp)) {
                    SingleToastUtil.showToast("请输入关键字")
                    false
                }
                UmengEventUtil.getInstance().onEvent(this, UmengEventId.getMusicSearch())
                mvpPresenter.refreshData(temp)
                true
            }
            false
        }
        tv_search.setOnClickListener {
            val temp = et_search.text.toString()
            if (TextUtils.isEmpty(temp)) {
                SingleToastUtil.showToast("请输入关键字")
                return@setOnClickListener
            }
            UmengEventUtil.getInstance().onEvent(this, UmengEventId.getMusicSearch())
            mvpPresenter.refreshData(temp)
        }
        iv_go_back.setOnClickListener { finish() }
    }

    private fun hintKeyBoard() {
        //拿到InputMethodManager
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        //如果window上view获取焦点 && view不为空
        if (imm.isActive && currentFocus != null) {
            //拿到view的token 不为空
            if (currentFocus.windowToken != null) {
                //表示软键盘窗口总是隐藏，除非开始时以SHOW_FORCED显示。
                imm.hideSoftInputFromWindow(currentFocus.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }
        }
    }

    private fun setLoadMoreListener() {
        myMusicAdapter!!.setOnLoadMoreListener({
            if (NetworkUtil.isNetAvailable(this)) {
                mvpPresenter.loadMoreData(et_search.text.toString())
            } else {
                myMusicAdapter!!.loadMoreEnd(true)
            }
        }, recycler_view)
    }
}