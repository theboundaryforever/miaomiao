package com.tongdaxing.erban.common.room.lover.weekcp.topic;

import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.erban.ui.mvp.MVPBaseDialogFragment;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.adapter.BaseIndicatorAdapter;
import com.tongdaxing.erban.common.room.lover.RoomLoverRoute;
import com.tongdaxing.erban.common.room.lover.weekcp.topic.item.SoulTopicItemFragment;
import com.tongdaxing.erban.common.room.lover.weekcp.topic.view.SoulTopicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chen on 2019/5/7.
 */
@Route(path = RoomLoverRoute.ROOM_LOVER_SOUL_TOPIC_PATH)
public class SoulTopicDialogFragment extends MVPBaseDialogFragment<ISoulTopicDialogView, SoulTopicDialogPresenter> implements ISoulTopicDialogView {
    @BindView(R2.id.room_lover_topic_indicator)
    MagicIndicator roomLoverTopicIndicator;
    @BindView(R2.id.room_lover_rl_soul_topic)
    FrameLayout roomLoverRlSoulTopic;
    @BindView(R2.id.room_lover_topic_random)
    TextView roomLoverTopicRandom;
    @BindView(R2.id.room_lover_view_pager)
    ViewPager roomLoverViewPager;
    private String TAG = "SoulTopicDialogFragment";
    private List<TabInfo> mTabInfoList;

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            WindowManager.LayoutParams params = window.getAttributes();
            params.gravity = Gravity.BOTTOM; // 显示在底部
            params.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度填充满屏
            params.height = (DisplayUtils.dip2px(BaseApp.mBaseContext, 354));
            window.setAttributes(params);
            // 这里用透明颜色替换掉系统自带背景
            int color = ContextCompat.getColor(BaseApp.mBaseContext, android.R.color.transparent);
            window.setBackgroundDrawable(new ColorDrawable(color));
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_lover_soul_topic_view;
    }

    @Override
    public void initBefore() {

    }

    @Override
    public void findView() {
        ButterKnife.bind(this, mContentView);
    }

    @Override
    public void setView() {

    }

    @Override
    public void setListener() {
        mPresenter.requestSoulTopic();
    }

    @Override
    protected SoulTopicDialogPresenter createPresenter() {
        return new SoulTopicDialogPresenter();
    }

    @Override
    public void dismiss() {
        dismissAllowingStateLoss();
    }

    @Override
    public void updateView(List<String> tittles) {
        L.debug(TAG, "updateView soulTopicInfo: %s", tittles);
        if (tittles == null) {
            return;
        }

        initData(tittles);
    }

    private void initMagicIndicator() {
        if (mTabInfoList == null) {
            return;
        }
        SoulTopicIndicatorAdapter mMsgIndicatorAdapter = new SoulTopicIndicatorAdapter(getContext(), mTabInfoList, 0);
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
        mMsgIndicatorAdapter.setOnItemSelectListener(new CommonMagicIndicatorAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                if (roomLoverViewPager != null) {
                    roomLoverViewPager.setCurrentItem(position);
                }
            }
        });

        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        commonNavigator.setAdjustMode(true);
        roomLoverTopicIndicator.setNavigator(commonNavigator);
        // must after setNavigator
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
    }

    private void initData(List<String> tittles) {
        List<Fragment> mTabs = new ArrayList<>();
        int size = tittles.size();
        if (mTabInfoList == null) {
            mTabInfoList = new ArrayList<>();
        } else {
            mTabInfoList.clear();
        }
        for (int i = 0; i < size; i++) {
            mTabs.add(SoulTopicItemFragment.newInstance(i));
            mTabInfoList.add(new TabInfo(i, tittles.get(i)));
        }
        initMagicIndicator();
        roomLoverViewPager.setAdapter(new BaseIndicatorAdapter(getChildFragmentManager(), mTabs));
        roomLoverViewPager.setOffscreenPageLimit(mTabs.size());
        ViewPagerHelper.bind(roomLoverTopicIndicator, roomLoverViewPager);
        roomLoverViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPresenter.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        roomLoverViewPager.setCurrentItem(mPresenter.getCurrentTab());
    }

    @OnClick(R2.id.room_lover_topic_random)
    public void onRandomClicked() {
        List<String> questions = mPresenter.getQuestions();
        if (questions == null || mTabInfoList == null) {
            return;
        }
        Random random = new Random();
        int index = random.nextInt(questions.size());
        mPresenter.requestRoomPlay(questions.get(index));
        dismissAllowingStateLoss();
    }

}
