package com.tongdaxing.erban.common.room.lover.weekcp.tacit;

import com.tongdaxing.erban.common.room.lover.BaseWeekPresenter;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.weekcp.bean.TacitTestBean;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTestEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import static com.tongdaxing.xchat_core.room.weekcp.tacit.ITacitTestCtrl.TACIT_TEST_ANSWER_AWAIT;
import static com.tongdaxing.xchat_core.room.weekcp.tacit.ITacitTestCtrl.TACIT_TEST_TIME;

/**
 * Created by Chen on 2019/5/7.
 */
public class TacitTestPresenter extends BaseWeekPresenter<ITacitTestView> {
    private String TAG = "TacitTestPresenter";

    public List<TacitTestBean.DataBean.QuestionBean> getTacitQuestions() {
        return CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo().getDataBeans();
    }

    public void addAnswer(String question) {
        L.debug(TAG, "addAnswer question: %s", question);
        List<String> questionAnswer = CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo().getQuestionAnswer();
        questionAnswer.add(question);
    }

    public int getCurrentQuestion() {
        List<String> answer = CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo().getQuestionAnswer();
        return answer.size();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTacitTestTimeChange(TacitTestEvent.OnTacitTestTimeChange testTimeChange) {
        if (getView() == null) {
            return;
        }
        int time = testTimeChange.getTime();
        L.debug(TAG, "onTacitTestTimeChange time: %d", time);
        if (time <= 0) {
            CoreUtils.send(new TacitTestAction.OnDismissTacitTestView());
            SingleToastUtil.showToast("对方已退出考验");
        } else {
            int currentTime = time - TACIT_TEST_TIME;
            if (currentTime >= 0) {
                getView().updateTime(currentTime);
            }
        }
    }

    public void submitAnswer() {
        List<String> answer = CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo().getQuestionAnswer();
        String answerStr = StringUtils.join(answer.toArray(), ",");
        L.debug(TAG, "submitAnswer answerStr: %s", answerStr);
        CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo().setCurrentState(TACIT_TEST_ANSWER_AWAIT);
        CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTestCtrl().doTacitAnswerCollect(getCpPlayerId(), answerStr);
    }

    public int getTacitCurrentState() {
        return CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo().getCurrentState();
    }

    public void exitTacit() {
        CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTestCtrl().doTacitAnswerCollect(getCpPlayerId(), "");
    }
}
