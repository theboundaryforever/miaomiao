package com.tongdaxing.erban.common.room.talk.manager;


import com.tongdaxing.erban.common.room.talk.factory.AttentionFactory;
import com.tongdaxing.erban.common.room.talk.factory.CommonFactory;
import com.tongdaxing.erban.common.room.talk.factory.EggFactory;
import com.tongdaxing.erban.common.room.talk.factory.FaceFactory;
import com.tongdaxing.erban.common.room.talk.factory.GiftFactory;
import com.tongdaxing.erban.common.room.talk.factory.NotificationFactory;
import com.tongdaxing.erban.common.room.talk.factory.TipFactory;
import com.tongdaxing.erban.common.room.talk.factory.TalkFactory;
import com.tongdaxing.erban.common.room.talk.factory.WeekCpFactory;
import com.tongdaxing.xchat_core.room.talk.TalkType;

/**
 * Created by chen on 2019/4/18.
 */
public class TalkFactoryInit {

    public static void init() {
        TalkManager.getInstance().registerFactory(TalkType.ROOM_TALK_NETEASE_TIP, new TipFactory());
        TalkManager.getInstance().registerFactory(TalkType.ROOM_TALK_NETEASE_TEXT, new TalkFactory());
        TalkManager.getInstance().registerFactory(TalkType.ROOM_TALK_HEADER_TYPE_GIFT, new GiftFactory());
        TalkManager.getInstance().registerFactory(TalkType.ROOM_TALK_HEADER_COMMON, new CommonFactory());
        TalkManager.getInstance().registerFactory(TalkType.ROOM_TALK_HEADER_TYPE_EGG, new EggFactory());
        TalkManager.getInstance().registerFactory(TalkType.ROOM_TALK_HEADER_TYPE_WEEK_CP, new WeekCpFactory());
        TalkManager.getInstance().registerFactory(TalkType.ROOM_TALK_HEADER_TYPE_ATTENTION, new AttentionFactory());
        TalkManager.getInstance().registerFactory(TalkType.ROOM_TALK_NETEASE_NOTIFICATION, new NotificationFactory());
        TalkManager.getInstance().registerFactory(TalkType.ROOM_TALK_HEADER_TYPE_FACE, new FaceFactory());
    }
}
