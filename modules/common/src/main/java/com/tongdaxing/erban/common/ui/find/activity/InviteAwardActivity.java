package com.tongdaxing.erban.common.ui.find.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.constant.BaseUrl;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.ui.me.wallet.activity.BinderPhoneActivity;
import com.tongdaxing.erban.common.ui.me.withdraw.RedPacketWithdrawActivity;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.ui.widget.dialog.shareDialog.NewShareDialog;
import com.tongdaxing.erban.common.ui.widget.marqueeview.MarqueeView;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.redpacket.bean.RedDrawListInfo;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.framework.Platform;

@Route(path = CommonRoutePath.INVITE_AWARD_ACTIVITY_ROUTE_PATH)
public class InviteAwardActivity extends BaseActivity implements NewShareDialog.OnShareDialogItemClick {

    @BindView(R2.id.title_bar)
    TitleBar titleBar;
    @BindView(R2.id.img_trumpet)
    ImageView imgTrumpet;
    @BindView(R2.id.marqueeView)
    MarqueeView marqueeView;
    @BindView(R2.id.tv_s)
    TextView tvS;
    @BindView(R2.id.tv_red_money)
    TextView tvRedMoney;
    @BindView(R2.id.ll_red_money)
    LinearLayout llRedMoney;
    @BindView(R2.id.invite_award_content)
    RelativeLayout inviteAwardContent;
    @BindView(R2.id.tv_red_rank)
    TextView tvRedRank;
    @BindView(R2.id.rly_red_rank)
    RelativeLayout rlyRedRank;
    @BindView(R2.id.tv_people)
    TextView tvPeople;
    @BindView(R2.id.tv_share_count)
    TextView tvShareCount;
    @BindView(R2.id.rly_people)
    RelativeLayout rlyPeople;
    @BindView(R2.id.tv_share_tv)
    TextView tvShareTv;
    @BindView(R2.id.tv_share)
    TextView tvShare;
    @BindView(R2.id.rly_share_bonus)
    RelativeLayout rlyShareBonus;
    @BindView(R2.id.rly_red_rule)
    RelativeLayout rlyRedRule;
    @BindView(R2.id.img_withdraw)
    TextView imgWithdraw;
    @BindView(R2.id.btn_invite_share)
    Button btnInviteShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_award);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public boolean blackStatusBar() {
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initView() {
        initTitleBarWhite("我的邀请奖励");
    }

    private void initData() {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        ResponseListener listener = new ResponseListener<ServiceResult<RedPacketInfo>>() {

            @Override
            public void onResponse(ServiceResult<RedPacketInfo> data) {
                if (null != data && data.isSuccess()) {
                    RedPacketInfo redPacketInfo = data.getData();
                    if (redPacketInfo != null) {
                        tvRedMoney.setText(String.valueOf(redPacketInfo.getPacketNum()));//奖励金额
                        tvShareCount.setText(redPacketInfo.getRegisterCout() + "人");//邀请人数
                        tvShare.setText(redPacketInfo.getChargeBonus() + "元");//分成奖励
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRedPacket(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);
        HashMap<String, String> requestParam2 = CommonParamUtil.getDefaultParam();
        ResponseListener listener2 = new ResponseListener<ServiceResult<List<RedDrawListInfo>>>() {
            @Override
            public void onResponse(ServiceResult<List<RedDrawListInfo>> data) {
                if (null != data && data.isSuccess()) {
                    onGetRedDrawList(data.getData());
                } else {
                    showDefaultData();
                }
            }
        };
        ResponseErrorListener errorListener2 = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                showDefaultData();
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRedDrawList(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam2, listener2, errorListener2,
                        ServiceResult.class, Request.Method.GET);
    }

    public void onGetRedDrawList(List<RedDrawListInfo> redDrawListInfos) {
        if (redDrawListInfos != null && redDrawListInfos.size() > 0) {
            List<String> info = new ArrayList<>();
            for (int i = 0; i < redDrawListInfos.size(); i++) {
                String content = redDrawListInfos.get(i).getNick() + "刚刚提现了" + redDrawListInfos.get(i).getPacketNum();
                info.add(content);
            }
            marqueeView.startWithList(info);
        } else {
            showDefaultData();
        }
    }

    private void showDefaultData() {
        List<String> info = new ArrayList<>();
        info.add("article一分钟前提现了200");
        info.add("木马一分钟前提现了200");
        info.add("此乳胸险一分钟前提现了100");
        info.add("xiaoshihou一分钟前提现了100");
        info.add("夜袭寡妇一分钟前提现了200");
        info.add("玮哥一分钟前提现了200");
        info.add("xiaoSeSe一分钟前提现了400");
        info.add("一生懵逼一分钟前提现了600");
        info.add("小可爱一分钟前提现了100");
        info.add("ai人一分钟前提现了200");
        info.add("大叔一分钟前提现了400");
        info.add("小凳子一分钟前提现了800");
        info.add("小逍遥一分钟前提现了300");
        info.add("清新一分钟前提现了100");
        info.add("荔枝一分钟前提现了200");
        info.add("小明哥哥一分钟前提现了100");
        info.add("薇薇一分钟前提现了300");
        info.add("红高粱一分钟前提现了400");
        marqueeView.startWithList(info);
    }

    @OnClick({R2.id.rly_red_rank, R2.id.rly_red_rule, R2.id.rly_people, R2.id.rly_share_bonus, R2.id.img_withdraw, R2.id.btn_invite_share})
    public void onClick(View view) {
        super.onClick(view);
        String url = null;
        int i = view.getId();//邀请奖励
//邀请大作战
//邀请人数
//邀请分成
        if (i == R.id.rly_red_rank) {
            url = BaseUrl.INVITE_REWARD_RANK + "?uid=" + CoreManager.getCore(IAuthCore.class).getCurrentUid();

        } else if (i == R.id.rly_red_rule) {
            url = BaseUrl.MY_INVITE_RULES + "?uid=" + CoreManager.getCore(IAuthCore.class).getCurrentUid();

        } else if (i == R.id.rly_people) {
            url = BaseUrl.MY_INVITE_PEOPLE + "?uid=" + CoreManager.getCore(IAuthCore.class).getCurrentUid();

        } else if (i == R.id.rly_share_bonus) {
            url = BaseUrl.MY_INVITE_PERCENTAGE + "?uid=" + CoreManager.getCore(IAuthCore.class).getCurrentUid();

        } else if (i == R.id.img_withdraw) {
            CoreManager.getCore(IAuthCore.class).isPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid());

        } else if (i == R.id.btn_invite_share) {
            NewShareDialog shareDialog = new NewShareDialog(InviteAwardActivity.this);
            shareDialog.setOnShareDialogItemClick(InviteAwardActivity.this);
            shareDialog.show();

        }
        if (!TextUtils.isEmpty(url))
            openWebView(url);
    }

    private void openWebView(String url) {
        if (TextUtils.isEmpty(url)) return;
        Intent intent = new Intent(this, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onIsphoneFail(String error) {

        startActivity(new Intent(this, BinderPhoneActivity.class));
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onIsPhone() {
        startActivity(new Intent(this, RedPacketWithdrawActivity.class));
    }

    @Override
    public void onSharePlatformClick(Platform platform) {
        WebViewInfo webViewInfo = new WebViewInfo();
        webViewInfo.setTitle("喵喵，语音交友神器！");
        webViewInfo.setImgUrl(BaseUrl.SHARE_DEFAULT_LOGO);
        webViewInfo.setDesc("喵喵交友速配处CP，你想要的我们都有。");
        webViewInfo.setShowUrl(BaseUrl.SHARE_DOWNLOAD);
        CoreManager.getCore(IShareCore.class).shareH5(webViewInfo, platform);

    }

    @Override
    public void onSendBroadcastMsg() {

    }
}
