package com.tongdaxing.erban.common.ui.me.wallet.fragment;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.pingplusplus.android.Pingpp;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.ui.me.bills.activity.BillActivity;
import com.tongdaxing.erban.common.ui.me.wallet.adapter.ChargeAdapter;
import com.tongdaxing.erban.common.ui.me.wallet.presenter.ChargePresenter;
import com.tongdaxing.erban.common.ui.me.wallet.view.IChargeView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.count.IChargeClient;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.ChargeBean;
import com.tongdaxing.xchat_core.pay.bean.PaymentResponseBean;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;

/**
 * 新皮：充值页面
 *
 * @author zwk 2018/5/30
 */
@CreatePresenter(ChargePresenter.class)
public class ChargeFragment extends BaseMvpFragment<IChargeView, ChargePresenter> implements IChargeView, View.OnClickListener {
    @BindView(R2.id.civ_head)
    RoundedImageView mCiv_head;
    @BindView(R2.id.tv_name)
    TextView mTv_name;
    @BindView(R2.id.tv_gold)
    TextView mTv_gold;
    @BindView(R2.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindString(R2.string.charge)
    String activityTitle;
    @BindView(R2.id.title_bar)
    TitleBar mTitleBar;
    @BindView(R2.id.tv_charge)
    TextView btnCharge;
    @BindView(R2.id.tv_bill)
    TextView tvBill;
    private ChargeAdapter mChargeAdapter;

    private ChargeBean mSelectChargeBean;

    @Override
    public int getRootLayoutId() {
        return R.layout.activity_charge;
    }

    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
        btnCharge.setOnClickListener(this);
        tvBill.setOnClickListener(this);
    }

    @Override
    public void initiate() {
        mTitleBar.setVisibility(View.GONE);
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3, OrientationHelper.VERTICAL, false));
        mChargeAdapter = new ChargeAdapter();
        mRecyclerView.setAdapter(mChargeAdapter);
        mChargeAdapter.setOnItemClickListener((baseQuickAdapter, view, position) -> {
            List<ChargeBean> list = mChargeAdapter.getData();
            if (ListUtils.isListEmpty(list)) return;
            mSelectChargeBean = list.get(position);
            int size = list.size();
            for (int i = 0; i < size; i++) {
                list.get(i).isSelected = position == i;
            }
            mChargeAdapter.notifyDataSetChanged();
        });
        getMvpPresenter().getChargeList();
//        getMvpPresenter().refreshWalletInfo(true);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            getMvpPresenter().refreshWalletInfo(true);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.tv_charge) {
            getMvpPresenter().showChargeOptionsDialog();

        } else if (i == R.id.tv_bill) {
            startActivity(new Intent(mContext, BillActivity.class));

        }
    }

    @Override
    public void setupUserInfo(UserInfo userInfo) {
//        mTv_name.setText(getString(R.string.charge_user_name, userInfo.getNick()));
//        ImageLoadUtils.loadAvatar(mContext, userInfo.getAvatar(), mCiv_head, false);
    }

    @Override
    public void setupUserWalletBalance(WalletInfo walletInfo) {
        Log.i(TAG, "setupUserWalletBalance: " + walletInfo.goldNum);
        mTv_gold.setText(String.format(Locale.getDefault(), "%.1f", walletInfo.goldNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
    }

    @Override
    public void buildChargeList(List<ChargeBean> chargeBeanList) {
        if (chargeBeanList != null && chargeBeanList.size() > 0) {
            for (int i = 0; i < chargeBeanList.size(); i++) {
                ChargeBean chargeBean = chargeBeanList.get(i);
                chargeBean.isSelected = chargeBean.getMoney() == 48;
                if (48 == chargeBean.getMoney()) {
                    mSelectChargeBean = chargeBean;
                }
            }
            mChargeAdapter.setNewData(chargeBeanList);
        }
    }

    @Override
    public void getChargeListFail(String error) {
        toast(error);
    }

    @Override
    public void displayChargeOptionsDialog() {
        if (mSelectChargeBean == null || mContext == null) return;

        ButtonItem buttonItem = new ButtonItem(getString(R.string.charge_alipay),
                () -> {
                    L.info(TAG, "charge channel: %d", Constants.PAY_ALIPAY_CHANNEL);
                    if (Constants.PAY_ALIPAY_CHANNEL == Constants.PAY_CHANNEL_ECPSS) {
                        //汇潮支付
                        getMvpPresenter().doRequestCharge(mContext, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_ALIPAY, UriProvider.requestEcpssCharge());
                    } else {
                        //ping++支付
                        getMvpPresenter().doRequestCharge(mContext, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_ALIPAY);
                    }
                });
        ButtonItem buttonItem1 = new ButtonItem(getString(R.string.charge_webchat),
                () -> {
                    if (Constants.PAY_CHANNEL == 2) {
                        //汇聚支付
                        getMvpPresenter().doRequestJoinPayCharge(mContext, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_JOIN_PAY_WX);
                    } else {
                        //ping++支付
                        getMvpPresenter().doRequestCharge(mContext, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_WX);
                    }
                });

        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(buttonItem);
        buttonItems.add(buttonItem1);
        ((BaseActivity) mContext).getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel), false);
    }

    /**
     * ping++支付
     *
     * @param data
     */
    @Override
    public void getChargeOrOrderInfo(String data, String payChannel) {
        if (data != null) {
            L.info(TAG, "data: %s, payChannel: %s, Constants.PAY_ALIPAY_CHANNEL: %d", data, payChannel, Constants.PAY_ALIPAY_CHANNEL);
            if (payChannel.equals(Constants.CHARGE_WX)) {
                //ping++ 微信
                Pingpp.createPayment(this, data);
            } else if (Constants.PAY_ALIPAY_CHANNEL == Constants.PAY_CHANNEL_ECPSS) {
                try {
                    String payUrl = new Json(data).str("payUrl");
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("alipays://platformapi/startApp?appId=10000011&url=" + URLEncoder.encode(payUrl, "UTF-8")));
                    startActivity(intent);
                } catch (Exception e) {
                    L.error(TAG, "getChargeOrOrderInfo:", e);
                    toast("发起充值失败，请联系客服人员");
                }
            } else {
                //ping++ 支付宝
                Pingpp.createPayment(this, data);
            }
        }
    }

    @Override
    public void getChargeOrOrderInfoFail(String error) {
        toast("发起充值失败：" + error);
    }

    /**
     * 汇聚支付
     *
     * @param data
     * @param payChannel
     */
    @Override
    public void getJoinPayOrderInfo(PaymentResponseBean data, String payChannel) {
        getMvpPresenter().createJoinPayment(mContext, data);
    }

    @Override
    public void getJoinPayOrderInfoFail(String error) {
        toast("发起充值失败：" + error);
    }


    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null) {
            mTv_gold.setText(getString(R.string.charge_gold, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onJoinPayWxResp(int errCode) {
        CoreManager.notifyClients(IChargeClient.class, IChargeClient.chargeAction, "");
        if (errCode == 0) {
            getMvpPresenter().refreshWalletInfo(true);
            toast("支付成功！");
        } else if (errCode == -2) {
            toast("支付被取消！");
        } else {
            toast("支付失败！");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: requestCode = " + requestCode + ", resultCode = " + resultCode);
        //支付页面返回处理
        if (requestCode == Pingpp.REQUEST_CODE_PAYMENT) {
            if (data != null && data.getExtras() != null) {
                String result = data.getExtras().getString("pay_result");
                if (result != null) {

                    /* 处理返回值
                     * "success" - 支付成功
                     * "fail"    - 支付失败
                     * "cancel"  - 取消支付
                     * "invalid" - 支付插件未安装（一般是微信客户端未安装的情况）
                     * "unknown" - app进程异常被杀死(一般是低内存状态下,app进程被杀死)
                     */
                    // 错误信息
                    String errorMsg = data.getExtras().getString("error_msg");
                    String extraMsg = data.getExtras().getString("extra_msg");
                    MLog.error(TAG, "errorMsg:" + errorMsg + "extraMsg:" + extraMsg);
                    CoreManager.notifyClients(IChargeClient.class, IChargeClient.chargeAction, result);


//                    LogUtil.d(TAG, "errorMsg:" + errorMsg + "extraMsg:" + extraMsg);
                    if ("success".equals(result)) {
                        getMvpPresenter().refreshWalletInfo(true);
                        toast("支付成功！");
                    } else if ("cancel".equals(result)) {
                        toast("支付被取消！");
                    } else {
                        toast("支付失败！");
                    }
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
