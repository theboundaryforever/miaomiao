package com.tongdaxing.erban.common.presenter.audio;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.AudioCardInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.Map;

public class AudioCardPresenter extends AbstractMvpPresenter<IAudioCardView> {
    public AudioCardPresenter() {
    }

    /**
     * 获取声鉴卡
     *
     * @param url
     * @param audioDuration
     */
    public void getAudioCard(String url, long audioDuration) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("url", url);
        params.put("voiceDura", String.valueOf(audioDuration));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getAudioCard(), params, new OkHttpManager.MyCallBack<ServiceResult<AudioCardInfo>>() {
            @Override
            public void onError(Exception e) {
                mMvpView.closeProgressDialog();
                if (e != null) {
                    SingleToastUtil.showToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<AudioCardInfo> response) {
                if (response.isSuccess() && response.getData() != null) {
                    mMvpView.getAudioIdentifyCard(response.getData(), url, audioDuration);
                } else {
                    mMvpView.closeProgressDialog();
                    SingleToastUtil.showToast("数据错误");
                }
            }
        });
    }
}
