package com.tongdaxing.erban.common.room.audio.presenter;

import com.tongdaxing.erban.common.room.audio.view.ILocalMusicLibraryView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.music.IMusicCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public class LocalMusicLibraryPresenter extends AbstractMvpPresenter<ILocalMusicLibraryView> {

    private List<MyMusicInfo> mMusicInfoList = new CopyOnWriteArrayList<>();

    public void addSongToMyLibrary(MyMusicInfo myMusicInfo, MusicLocalInfo musicLocalInfo) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("songId", String.valueOf(myMusicInfo.getId()));
        OkHttpManager.getInstance().doPostRequest(UriProvider.getAddHotSong(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    CoreManager.getCore(IMusicCore.class).copyFileToCacheFolder(musicLocalInfo);//则直接将歌曲复制到缓存
                } else {
                    SingleToastUtil.showToast("数据异常");
                }
                CoreManager.notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.MUSIC_UPLOAD_SUCCESS, musicLocalInfo);
            }
        });
    }

    public void getMySongList() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageSize", String.valueOf(200));//产品要求不刷新，直接拿200条数据
        params.put("pageNum", String.valueOf(1));
        OkHttpManager.getInstance().doGetRequest(UriProvider.getMySongList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<MyMusicInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (e != null) {
                    getMvpView().loadSongFailure();
                }
            }

            @Override
            public void onResponse(ServiceResult<List<MyMusicInfo>> response) {
                if (response != null && response.isSuccess() && response.getData() != null) {
                    mMusicInfoList.clear();
                    mMusicInfoList.addAll(response.getData());
                    getMvpView().loadSongSuccess(mMusicInfoList);
                } else {
                    getMvpView().loadSongFailure();
                }
            }
        });
    }

    /**
     * 检查远程服务器是否有音乐文件
     *
     * @param localMusicInfo
     */
    public void isExistMusicFromService(MusicLocalInfo localMusicInfo) {
        if (localMusicInfo == null) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("artist", localMusicInfo.getSingerName());
        params.put("size", String.valueOf(localMusicInfo.getFileSize()));
        params.put("title", localMusicInfo.getSongName());
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().doPostRequest(UriProvider.getSongExist(), params, new OkHttpManager.MyCallBack<ServiceResult<MyMusicInfo>>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<MyMusicInfo> response) {
                if (response != null && response.isSuccess() && response.getData() != null) {
                    MyMusicInfo myMusicInfo = response.getData();
                    getMvpView().copyDirectly(myMusicInfo, localMusicInfo);
                    if (myMusicInfo.getSongStatus() == 2 || myMusicInfo.getSongStatus() == 3) {
                        SingleToastUtil.showToast("歌曲违规，无法上传!");
                    }
                } else {
                    getMvpView().copyAfterUpload(localMusicInfo);
                }
            }
        });
    }
}
