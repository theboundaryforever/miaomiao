package com.tongdaxing.erban.common.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

import com.jude.rollviewpager.Util;
import com.jude.rollviewpager.hintview.ColorPointHintView;


/**
 * 首页-热门中房间列表中部的banner
 * <p>
 * Created by zhangjian on 2019/6/11.
 */
public class HomeMiddleBannerHintView extends ColorPointHintView {
    private int mFocusColor;
    private int mNormalColor;
    private int mFocusWidth;
    private int mNormalDiameter;//圆点的直径

    public HomeMiddleBannerHintView(Context context, int focusColor, int normalColor, int focusWidth, int normalDiameter) {
        super(context, focusColor, normalColor);
        mFocusColor = focusColor;
        mNormalColor = normalColor;
        mFocusWidth = focusWidth;
        mNormalDiameter = normalDiameter;
    }

    @Override
    public Drawable makeFocusDrawable() {
        GradientDrawable dot_focus = new GradientDrawable();
        dot_focus.setColor(mFocusColor);
        dot_focus.setShape(GradientDrawable.RECTANGLE);
        dot_focus.setCornerRadius(Util.dip2px(getContext(), 4));
        dot_focus.setSize(Util.dip2px(getContext(), mFocusWidth), Util.dip2px(getContext(), mNormalDiameter));
        return dot_focus;
    }

    @Override
    public Drawable makeNormalDrawable() {
        GradientDrawable dot_normal = new GradientDrawable();
        dot_normal.setColor(mNormalColor);
        dot_normal.setShape(GradientDrawable.OVAL);
        dot_normal.setCornerRadius(Util.dip2px(getContext(), 4));
        dot_normal.setSize(Util.dip2px(getContext(), mNormalDiameter), Util.dip2px(getContext(), mNormalDiameter));
        return dot_normal;
    }
}
