package com.tongdaxing.erban.common.ui.home.view;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.home.HomeRoom;

/**
 * <p> 首页推荐item view </p>
 *
 * @author Administrator
 * @date 2017/11/23
 */
public class HomeRecommendItemView extends RelativeLayout {
    private ImageView mIvCover;
    private View mNormalBg;
    private View mLockBg;
    private ImageView mIvSmallCover;
    private TextView mTvTitle;
    private TextView mTvOnlineNumber;
    private Context mContext;

    public HomeRecommendItemView(Context context) {
        this(context, null);
    }

    public HomeRecommendItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeRecommendItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public HomeRecommendItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        inflate(context, R.layout.home_hot_recommend_item_layout, this);
        mIvCover = (ImageView) findViewById(R.id.cover);
        mNormalBg = findViewById(R.id.normal_bg);
        mLockBg = findViewById(R.id.lock_bg);
        mIvSmallCover = (ImageView) findViewById(R.id.iv_small_cover);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvOnlineNumber = (TextView) findViewById(R.id.online_number);
    }

    public void setData(HomeRoom homeRoom, int index) {
        if (homeRoom == null) {
            return;
        }
        mTvOnlineNumber.setText(mContext.getString(R.string.online_number_text, homeRoom.getOnlineNum()));
        mTvTitle.setText(homeRoom.getTitle());
        mLockBg.setVisibility(!TextUtils.isEmpty(homeRoom.getRoomPwd()) ? VISIBLE : GONE);

        int position = index % 5;
        if (position == 0) {
            mNormalBg.setBackgroundResource(R.drawable.bg_home_recommend_1);
        } else if (position == 1) {
            mNormalBg.setBackgroundResource(R.drawable.bg_home_recommend_2);
        } else if (position == 2) {
            mNormalBg.setBackgroundResource(R.drawable.bg_home_recommend_3);
        } else if (position == 3) {
            mNormalBg.setBackgroundResource(R.drawable.bg_home_recommend_4);
        } else if (position == 4) {
            mNormalBg.setBackgroundResource(R.drawable.bg_home_recommend_5);
        }

        if (!TextUtils.isEmpty(homeRoom.getAvatar())) {
            mIvSmallCover.setBackgroundColor(Color.TRANSPARENT);
            GlideApp.with(mContext)
                    .load(homeRoom.getAvatar())
                    .transform(new CircleCrop())
                    .placeholder(R.drawable.ic_no_avatar)
                    .error(R.drawable.ic_no_avatar)
                    .into(mIvSmallCover);
        } else {
            mIvSmallCover.setImageResource(R.drawable.ic_no_avatar);
        }

        if (!TextUtils.isEmpty(homeRoom.getAvatar())) {
            ImageLoadUtils.loadSmallRoundBackground(mContext, homeRoom.getAvatar(), mIvCover);
        } else {
            mIvCover.setImageResource(R.drawable.default_cover);
        }
    }

}
