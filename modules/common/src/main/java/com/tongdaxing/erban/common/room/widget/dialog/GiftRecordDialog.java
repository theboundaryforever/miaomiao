package com.tongdaxing.erban.common.room.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.tongdaxing.erban.common.room.avroom.adapter.RoomGiftRecordAdapter;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.List;

public class GiftRecordDialog extends Dialog implements RoomGiftRecordAdapter.OnGiftRecordListener {
    private RecyclerView mRecyclerView;
    private RoomGiftRecordAdapter mGiftRecordAdapter;
    private View mEmptyView;
    private Context mContext;

    public GiftRecordDialog(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_gift_record_list);
        Window window = getWindow();
        // setup window and width
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);
        mRecyclerView = findViewById(R.id.recycler_view);
        mEmptyView = findViewById(R.id.tv_gift_history_empty);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mGiftRecordAdapter = new RoomGiftRecordAdapter(mContext);
        mRecyclerView.setAdapter(mGiftRecordAdapter);
        mGiftRecordAdapter.setOnGiftRecordListener(this);
        findViewById(R.id.iv_close_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isShowing())
                    dismiss();
            }
        });
    }

    public void loadData(List<ChatRoomMessage> data) {
        if (mGiftRecordAdapter != null) {
            if (data == null || data.isEmpty()) {
                mEmptyView.setVisibility(View.VISIBLE);
            } else {
                mGiftRecordAdapter.setDatas(data);
            }
        }
    }

    @Override
    public void OnGiftRecordItemListener(String account) {
        if (mContext != null && StringUtils.isNotEmpty(account))
            UserInfoDialogManager.showDialogFragment(mContext, JavaUtil.str2long(account));
        try {
            if (isShowing())
                dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
