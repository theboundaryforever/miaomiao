package com.tongdaxing.erban.common.ui.me.shopping.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.user.bean.DressUpBean;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * 装扮商城View 接口
 *
 * @author zwk 2018/10/16
 */
public interface IDressUpFragmentView extends IMvpBaseView {
    //获取装扮列表数据成功
    void getDressUpListSuccess(ServiceResult<List<DressUpBean>> result);

    //获取装扮列表数据失败
    void getDressUpListFail(Exception e);

    //购买装扮/续费装扮成功
    void onPurseDressUpSuccess(int purseType);

    //购买装扮/续费装扮失败
    void onPurseDressUpFail(String error);

    //修改装扮的使用/取消使用 状态成功
    void onChangeDressUpStateSuccess(int dressUpId);

    //修改装扮的使用/取消使用 失败
    void onChangeDressUpStateFail(String error);
}
