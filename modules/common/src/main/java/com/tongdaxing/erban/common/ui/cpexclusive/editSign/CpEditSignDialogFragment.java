package com.tongdaxing.erban.common.ui.cpexclusive.editSign;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.mvp.MVPBaseDialogFragment;
import com.tongdaxing.erban.common.R;

/**
 * Created by zhangjian on 2019/4/30.
 */
@Route(path = CpExclusivePageARouterPath.CP_EXCLUSIVE_PAGE_EDIT_SIGN_PATH)
public class CpEditSignDialogFragment extends MVPBaseDialogFragment<ICpEditSignDialogView, CpEditSignDialogPresenter> implements ICpEditSignDialogView {
    @Override
    public int getContentViewId() {
        return R.layout.room_dialog_fragment_cp_edit_sign;
    }

    @Override
    public void initBefore() {
        ARouter.getInstance().inject(this);
    }

    @Override
    public void findView() {

    }

    @Override
    public void setView() {
        setCancelable(true);
        getDialog().setCanceledOnTouchOutside(true);
        Window window = getDialog().getWindow();
        if (window != null && getContext() != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setCancelable(true);
    }

    @Override
    public void setListener() {
    }

    @Override
    protected CpEditSignDialogPresenter createPresenter() {
        return new CpEditSignDialogPresenter();
    }

    @Override
    public void dismissDialog() {
        dismiss();
    }
}
