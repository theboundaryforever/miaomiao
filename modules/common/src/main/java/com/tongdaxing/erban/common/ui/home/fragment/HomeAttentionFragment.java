package com.tongdaxing.erban.common.ui.home.fragment;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.common.base.fragment.BaseLazyFragment;
import com.tongdaxing.erban.common.model.homeattention.HomeAttentionModel;
import com.tongdaxing.erban.common.model.makefriends.PopularStarModel;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.home.adpater.HomeAttentionListAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.RoomListAdapter;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.message.activity.AttentionListActivity;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.GridSpaceItemDecoration;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomManager;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> 首页 关注 界面 </p>
 */
public class HomeAttentionFragment extends BaseLazyFragment implements SwipeRefreshLayout.OnRefreshListener,
        BaseQuickAdapter.RequestLoadMoreListener, BaseQuickAdapter.OnItemClickListener {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView, peopleRecyclerView;
    private RoomListAdapter mAdapter;
    private HomeAttentionListAdapter attentionListAdapter;
    private View mHeader, mAttentionNoDataView;
    private List<HomeRoom> firstPageList;//保存第一页数据
    private int mCurrentPage = Constants.PAGE_START;

    private HomeAttentionModel dataSource;
    private boolean isWonderfulRoomLoading;
    private PopularStarModel popularStarModel;
    private boolean isFirst = true;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_home_attention;
    }

    public static HomeAttentionFragment newInstance() {
        return new HomeAttentionFragment();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (firstPageList != null) {
            outState.putParcelableArrayList(Constants.KEY_FIRST_PAGE_LIST, (ArrayList<? extends Parcelable>) firstPageList);
        }
    }

    @Override
    protected void restoreState(@Nullable Bundle savedInstanceState) {
        super.restoreState(savedInstanceState);
        if (savedInstanceState != null) {
            firstPageList = savedInstanceState.getParcelableArrayList(Constants.KEY_FIRST_PAGE_LIST);
        }
    }

    @Override
    public void onFindViews() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.mm_theme));
        mSwipeRefreshLayout.setRefreshing(true);


//        int itemNum = 2;//每行显示多少个
//        int marginByRow = DisplayUtils.dip2px(getContext(), 12);//每行的行左间距和右间距
//        int marginItem = DisplayUtils.dip2px(getContext(), 6);//每行非第一个view的左边距
        GridLayoutManager manager = new GridLayoutManager(getContext(), HotTagFragment.ITEM_COUNT_BY_ROW);
        mRecyclerView.setLayoutManager(manager);

        mAdapter = new RoomListAdapter(getContext());
//        int itemViewHeight = Math.round((ScreenUtil.getDisplayWidth() - marginItem * (itemNum - 1) - marginByRow * 2) / itemNum);
//        mAdapter.setHeightByGrid(itemViewHeight);//等宽高，这里不能用SquareImageView，底部会丢失圆角，后续再分析原理，先自己计算

//        if (roomItemDecoration == null) {
//        roomItemDecoration = new RecyclerView.ItemDecoration() {
//            @Override
//            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//                super.getItemOffsets(outRect, view, parent, state);
//                int itemPosition = parent.getChildLayoutPosition(view) - mAdapter.getHeaderLayoutCount();
//                if (itemPosition >= 0) {
//                    if (itemPosition % itemNum == 0) {  //每行的第一个
//                        outRect.left = marginByRow;
//                        outRect.right = 0;
//                        outRect.bottom = marginByRow;
//                    } else if (itemPosition % itemNum == (itemNum - 1)) {//每行的最后一个
//                        outRect.left = marginItem;
//                        outRect.right = marginByRow;
//                        outRect.bottom = marginByRow;
//                    } else {
//                        outRect.left = marginItem;
//                        outRect.right = 0;
//                        outRect.bottom = marginByRow;
//                    }
//                }
//            }
//        };

        int itemMargin = getResources().getDimensionPixelSize(R.dimen.home_room_margin);//每一行间距
        GridSpaceItemDecoration decoration = new GridSpaceItemDecoration(0, itemMargin);
        decoration.setupBindAdapter(mAdapter);
        mRecyclerView.addItemDecoration(decoration, 0);
//        }

        mAdapter.setOnLoadMoreListener(this, mRecyclerView);
        mAdapter.setOnItemClickListener(this);

        mHeader = LayoutInflater.from(getContext()).inflate(R.layout.layout_home_attention_header, null);
        mAttentionNoDataView = mHeader.findViewById(R.id.home_attention_no_data_layout);
        initWonderfulRoomViewLp(mHeader.findViewById(R.id.wonderful_room_image_layout_one));
        initWonderfulRoomViewLp(mHeader.findViewById(R.id.wonderful_room_image_layout_two));
        initWonderfulRoomViewLp(mHeader.findViewById(R.id.wonderful_room_image_layout_three));

        peopleRecyclerView = mHeader.findViewById(R.id.attention_people_list);
        peopleRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
//        if (peopleItemDecoration == null) {
        RecyclerView.ItemDecoration peopleItemDecoration = new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int position = parent.getChildLayoutPosition(view);
                if (position == 0) {//第一个做间距15dp
                    outRect.left = ScreenUtil.dip2px(15);
                } else {//其他的左间距20dp
                    outRect.left = ScreenUtil.dip2px(20);
                }
                if (position == attentionListAdapter.getItemCount() - 1) {//最后一个右间距15dp
                    outRect.right = ScreenUtil.dip2px(15);
                } else {
                    outRect.right = 0;
                }
            }
        };
        peopleRecyclerView.addItemDecoration(peopleItemDecoration, 0);
//        }

        attentionListAdapter = new HomeAttentionListAdapter(R.layout.item_home_attention_people, new ArrayList<>());
        attentionListAdapter.setOnItemClickListener(this);
        peopleRecyclerView.setAdapter(attentionListAdapter);

        mAdapter.addHeaderView(mHeader);

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onSetListener() {
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void initiate() {
        dataSource = new HomeAttentionModel();
    }

    /**
     * 动态设置推荐房间控件的大小（保证正方形）
     */
    private void initWonderfulRoomViewLp(View view) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        layoutParams.height = Math.round((ScreenUtil.getDisplayWidth() - getResources().getDimensionPixelOffset(R.dimen.wonderful_room_list_margin) * 4) / 3f);
        view.setLayoutParams(layoutParams);
    }

    @Override
    protected void onLazyLoadData() {
        if (!ListUtils.isListEmpty(firstPageList)) {
            mAdapter.setNewData(firstPageList);
            if (firstPageList.size() < Constants.PAGE_SIZE) {
                mAdapter.setEnableLoadMore(false);
            }
        } else {
            if (isFirst) {
                isFirst = false;
                onRefresh();
            }
        }
    }

    /**
     * 加载关注的人
     */
    private void loadAttentionPeopleListData() {
        dataSource.getAttentionPeopleList(Constants.PAGE_START, 40, new OkHttpManager.MyCallBack<ServiceResult<List<AttentionInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (ListUtils.isListEmpty(attentionListAdapter.getData())) {
                    peopleRecyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<AttentionInfo>> response) {
                if ((response == null || ListUtils.isListEmpty(response.getData()))) {
                    attentionListAdapter.setNewData(null);
                    peopleRecyclerView.setVisibility(View.GONE);
                } else {
                    List<AttentionInfo> attentionList = response.getData();
                    AttentionInfo firstAttentionItem = new AttentionInfo();
                    firstAttentionItem.setUid(-1);
                    attentionList.add(0, firstAttentionItem);//加第一栏：关注的人
                    attentionListAdapter.setNewData(attentionList);
                    peopleRecyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /**
     * 加载关注的房间
     */
    private void loadAttentionListData(final int page) {
        dataSource.getAttentionListRoom(page, Constants.PAGE_SIZE, new OkHttpManager.MyCallBack<ServiceResult<List<HomeRoom>>>() {
            @Override
            public void onError(Exception e) {
                if (page == Constants.PAGE_START) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    showNetworkErr();
                } else {
                    hideStatus();
                    mAdapter.loadMoreFail();
                }
            }

            @Override
            public void onResponse(ServiceResult<List<HomeRoom>> response) {
                if (response != null && response.isSuccess()) {
                    hideStatus();
                    setCurrentPage(page);
                    setupAttentionListRoom(response.getData(), page);
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    /**
     * 加载精彩房间推荐
     */
    private void loadWonderfulRoom() {
        if (isWonderfulRoomLoading) {
            return;
        }
        isWonderfulRoomLoading = true;
        dataSource.randomHotRoom(new OkHttpManager.MyCallBack<ServiceResult<List<HomeRoom>>>() {
            @Override
            public void onError(Exception e) {
                isWonderfulRoomLoading = false;
            }

            @Override
            public void onResponse(ServiceResult<List<HomeRoom>> response) {
                isWonderfulRoomLoading = false;
                if (response != null && response.isSuccess()) {
                    setupWonderfulRoomList(response.getData());
                }
            }
        });
    }

    /**
     * 设置关注房间列表显示
     */
    public void setupAttentionListRoom(List<HomeRoom> listData, int page) {
//        listData = new ArrayList<>();
        if (listData == null || (ListUtils.isListEmpty(listData))) {//没关注的房间/关注的房间未开启
            if (page == Constants.PAGE_START) {
                firstPageList = null;
                mAdapter.setNewData(null);
                mSwipeRefreshLayout.setRefreshing(false);
                //显示精彩房间推荐
                mAttentionNoDataView.setVisibility(View.VISIBLE);
                loadWonderfulRoom();
            } else {
                mAdapter.loadMoreEnd(true);
            }
        } else {
            if (page == Constants.PAGE_START) {
                mSwipeRefreshLayout.setRefreshing(false);
                hideStatus();
                mAttentionNoDataView.setVisibility(View.GONE);//隐藏精彩房间推荐
                if (!ListUtils.isListEmpty(firstPageList)) {
                    firstPageList.clear();
                }
                firstPageList = listData;

                mAdapter.setNewData(listData);
                //不够10个的话，不显示加载更多
                if (listData.size() < Constants.PAGE_SIZE) {
                    mAdapter.setEnableLoadMore(false);
                }
            } else {
                mAdapter.addData(listData);
                mAdapter.loadMoreComplete();
            }
        }
    }

    /**
     * 设置显示精彩房间推荐列表
     */
    private void setupWonderfulRoomList(List<HomeRoom> listData) {
        View wonderfulListView = mHeader.findViewById(R.id.wonderful_room_list);
        if (ListUtils.isListEmpty(listData)) {
            wonderfulListView.setVisibility(View.GONE);
        } else {
            mHeader.findViewById(R.id.random_next).setOnClickListener(view -> loadWonderfulRoom());
            wonderfulListView.setVisibility(View.VISIBLE);
            int size = listData.size();
            setupWonderfulRoom(size > 0 ? listData.get(0) : null,
                    R.id.wonderful_room_one, R.id.iv_cover_one, R.id.tv_title_one, R.id.tv_custom_label_one, R.id.tv_online_num_one, R.id.iv_play_one, R.id.btn_attention_one);
            setupWonderfulRoom(size > 1 ? listData.get(1) : null,
                    R.id.wonderful_room_two, R.id.iv_cover_two, R.id.tv_title_two, R.id.tv_custom_label_two, R.id.tv_online_num_two, R.id.iv_play_two, R.id.btn_attention_two);
            setupWonderfulRoom(size > 2 ? listData.get(2) : null,
                    R.id.wonderful_room_three, R.id.iv_cover_three, R.id.tv_title_three, R.id.tv_custom_label_three, R.id.tv_online_num_three, R.id.iv_play_three, R.id.btn_attention_three);
        }
    }

    /**
     * 设置显示精彩房间
     */
    private void setupWonderfulRoom(HomeRoom roomInfo, int wonderfulRoomLayoutId, int coverId, int titleId, int tabLabelId, int onlineNumId, int playId, int attentionBtnResId) {
        if (roomInfo == null) {
            mHeader.findViewById(wonderfulRoomLayoutId).setVisibility(View.INVISIBLE);
            mHeader.findViewById(wonderfulRoomLayoutId).setOnClickListener(null);
            return;
        }
        mHeader.findViewById(wonderfulRoomLayoutId).setVisibility(View.VISIBLE);
        mHeader.findViewById(wonderfulRoomLayoutId).setOnClickListener(view -> AVRoomActivity.start(getContext(), (roomInfo.getUid())));
        GlideApp.with(getContext())
                .load(roomInfo.getAvatar())
                .placeholder(R.drawable.default_cover)
                .error(R.drawable.default_cover)
                .into((ImageView) mHeader.findViewById(coverId));
        ((TextView) mHeader.findViewById(titleId)).setText(roomInfo.getTitle());
        ((TextView) mHeader.findViewById(tabLabelId)).setText(roomInfo.getRoomTag());
        ((TextView) mHeader.findViewById(onlineNumId)).setText(String.valueOf(roomInfo.getOnlineNum()));
        ImageView ivPlay = mHeader.findViewById(playId);
        GlideApp.with(getContext())
                .load(R.drawable.icon_music_play_white)
                .into(ivPlay);

//        SVGAImageView svgaPlay = mHeader.findViewById(R.id.svga_play);
//
//        SvgaUtils.getInstance(getContext()).cyclePlayAssetsAnim(svgaPlay, SvgaUtils.HALL_ROOM_ITEM_PLAY_SVGA_ASSETS);

        TextView attentionBtn = mHeader.findViewById(attentionBtnResId);
        attentionBtn.setSelected(false);
        attentionBtn.setText("关注");
        attentionBtn.setOnClickListener(view -> {
            if (!view.isSelected()) {
                getDialogManager().showProgressDialog(mContext, getResources().getString(R.string.please_wait));
                dataSource.praise(roomInfo.getUid(), new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
                    @Override
                    public void onError(Exception e) {
                        getDialogManager().dismissDialog();
                        toast("关注失败");
                    }

                    @Override
                    public void onResponse(ServiceResult<Object> response) {
                        getDialogManager().dismissDialog();
                        ((TextView) view).setText("已关注");
                        view.setSelected(true);
                    }
                });
            }
        });
    }

    /**
     * 去找TA
     */
    private void toFindHer(final AttentionInfo info) {
        getDialogManager().showProgressDialog(mContext, getResources().getString(R.string.please_wait));
        //检查是否在房间
        if (popularStarModel == null) {
            popularStarModel = new PopularStarModel();
        }
        popularStarModel.getUserRoomInfo(info.getUid(), new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
            @Override
            public void onError(Exception e) {
                getDialogManager().dismissDialog();
                toast(getResources().getString(R.string.state_load_error));
            }

            @Override
            public void onResponse(ServiceResult<RoomInfo> response) {
                getDialogManager().dismissDialog();
                long formalRoomId = BasicConfig.isDebug ? PublicChatRoomManager.devRoomId : PublicChatRoomManager.formalRoomId;
                if (response != null && response.getData() != null &&
                        response.getData().getRoomId() != formalRoomId && response.getData().getRoomId() > 0) {
                    getDialogManager().dismissDialog();
                    AVRoomActivity.start(mContext, response.getData().getUid(), new HerUserInfo(info.getUid(), info.getNick()));
                } else {
                    NewUserInfoActivity.start(mContext, info.getUid());
                }
            }
        });
    }

    private int getCurrentPage() {
        return mCurrentPage;
    }

    private void setCurrentPage(int page) {
        mCurrentPage = page;
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        if (isFirst) {
            isFirst = false;
            onRefresh();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
            if (attentionListAdapter != null) {
                attentionListAdapter.notifyDataSetChanged();//刷新关注人数
            }
        }
    }

    @Override
    public void onRefresh() {
        mAdapter.setEnableLoadMore(true);
        loadAttentionListData(Constants.PAGE_START);
        loadAttentionPeopleListData();
        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());//刷新关注人数
    }

    @Override
    public void onLoadMoreRequested() {
        if (NetworkUtil.isNetAvailable(mContext)) {
            if (firstPageList != null && firstPageList.size() >= Constants.PAGE_SIZE) {
                loadAttentionListData(getCurrentPage() + 1);
            } else {
                mAdapter.setEnableLoadMore(false);
            }
        } else {
            mAdapter.loadMoreEnd(true);
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (baseQuickAdapter == mAdapter) {//关注的房间
            if (ListUtils.isListEmpty(mAdapter.getData())) {
                return;
            }
            if (mAdapter.getData().size() <= i)
                return;
            HomeRoom homeRoom = mAdapter.getItem(i);
            if (homeRoom == null) {
                return;
            }
            AVRoomActivity.start(getContext(), homeRoom.getUid());
        } else {//关注的人
            if (ListUtils.isListEmpty(attentionListAdapter.getData())) {
                return;
            }
            if (attentionListAdapter.getData().size() <= i)
                return;
            AttentionInfo info = attentionListAdapter.getItem(i);
            if (info == null) {
                return;
            }
            if (info.getUid() == -1) {//第一个按钮：关注人
                startActivity(new Intent(mContext, AttentionListActivity.class));
            } else {
                toFindHer(info);
            }
        }
    }


    @Override
    public void onReloadData() {
        super.onReloadData();
        showLoading();
        onRefresh();
    }
}