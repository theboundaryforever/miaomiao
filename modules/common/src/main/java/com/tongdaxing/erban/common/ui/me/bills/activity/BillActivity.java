package com.tongdaxing.erban.common.ui.me.bills.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.adapter.BaseIndicatorAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.tongdaxing.erban.common.ui.me.bills.fragment.BillChargeFragment;
import com.tongdaxing.erban.common.ui.me.bills.fragment.BillExpenseFragment;
import com.tongdaxing.erban.common.ui.me.bills.fragment.BillIncomeFragment;
import com.tongdaxing.erban.common.ui.me.bills.fragment.BillRedFragment;
import com.tongdaxing.erban.common.ui.widget.magicindicator.MagicIndicator;
import com.tongdaxing.erban.common.ui.widget.magicindicator.ViewPagerHelper;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 新皮：账单页面
 *
 * @author zwk 2018/5/30
 */
public class BillActivity extends BaseActivity implements CommonMagicIndicatorAdapter.OnItemSelectListener {
    private ViewPager vpBill;
    private MagicIndicator mIndicator;
    private CommonMagicIndicatorAdapter mMsgIndicatorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);
        setSwipeBackEnable(false);
        initTitleBar("账单");
        initView();
        initListener();
    }

    private void initView() {
        mIndicator = (MagicIndicator) findViewById(R.id.mi_bill_indicator);
        vpBill = (ViewPager) findViewById(R.id.vp_bill);
        List<Fragment> mTabs = new ArrayList<>(4);
        mTabs.add(new BillIncomeFragment());
        mTabs.add(new BillExpenseFragment());
        mTabs.add(new BillChargeFragment());
        mTabs.add(new BillRedFragment());
        // mTabs.add(FansListFragment.newInstance(Constants.FAN_MAIN_PAGE_TYPE));
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(1, "收入礼物"));
        tabInfoList.add(new TabInfo(2, "支出礼物"));
        tabInfoList.add(new TabInfo(3, "充值记录"));
        tabInfoList.add(new TabInfo(4, "邀请记录"));
        mMsgIndicatorAdapter = new CommonMagicIndicatorAdapter(this, tabInfoList, 0);
        mMsgIndicatorAdapter.setSize(15);

        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        mIndicator.setNavigator(commonNavigator);
        // must after setNavigator
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        vpBill.setAdapter(new BaseIndicatorAdapter(getSupportFragmentManager(), mTabs));
        vpBill.setOffscreenPageLimit(5);
        ViewPagerHelper.bind(mIndicator, vpBill);
    }

    private void initListener() {
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
    }

    @Override
    public void onItemSelect(int position) {
        vpBill.setCurrentItem(position);
    }

}
