package com.tongdaxing.erban.common.room.audio.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
public interface IMyMusicView extends IMvpBaseView {
    void refreshMyMusicListFailure(String error);

    void refreshMyMusicListSucceed(List<MyMusicInfo> myMusicInfoList);

//    void delOperation(int position);
}
