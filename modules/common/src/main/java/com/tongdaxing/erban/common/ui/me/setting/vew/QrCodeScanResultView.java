package com.tongdaxing.erban.common.ui.me.setting.vew;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface QrCodeScanResultView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onQrCodeLogin(boolean isSuccess, String msg);

}
