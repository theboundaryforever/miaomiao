package com.tongdaxing.erban.common.ui.me;

import android.content.Context;
import android.content.Intent;

import com.tongdaxing.erban.common.constant.BaseUrl;
import com.tongdaxing.erban.common.ui.me.setting.activity.FeedbackActivity;
import com.tongdaxing.erban.common.ui.me.shopping.activity.DressUpMallActivity;
import com.tongdaxing.erban.common.ui.me.task.activity.MyTaskActivity;
import com.tongdaxing.erban.common.ui.me.task.view.IMeView;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.message.activity.AttentionListActivity;
import com.tongdaxing.erban.common.ui.message.activity.FansListActivity;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

/**
 * Created by MadisonRong on 04/01/2018.
 */

public class MePresenter extends AbstractMvpPresenter<IMeView> {

    private UserMvpModel userMvpModel;
    private UserInfo mUserInfo;

    public MePresenter() {
        userMvpModel = new UserMvpModel();
    }

    public void initUserData() {
        mUserInfo = userMvpModel.getUserDate();
        setupUserData();
    }

    public void setupUserData() {
        if (mUserInfo == null) {
            MLog.error(this, "用户信息不存在！");
        }
        getMvpView().updateUserInfoUI(mUserInfo);
    }

    public void clickBy(int viewId, Context context) {
        if (viewId == R.id.me_tv_user_name || viewId == R.id.me_iv_user_head) {
            if (mUserInfo != null) {
                NewUserInfoActivity.start(context, mUserInfo.getUid());
            }

        } else if (viewId == R.id.me_tv_user_attentions || viewId == R.id.tv_user_attention_text) {
            context.startActivity(new Intent(context, AttentionListActivity.class));

        } else if (viewId == R.id.me_tv_user_fans || viewId == R.id.tv_user_fan_text) {
            context.startActivity(new Intent(context, FansListActivity.class));

        } else if (viewId == R.id.me_tv_friends || viewId == R.id.me_tv_friends_text) {//todo 好友列表activity

        } else if (viewId == R.id.me_item_task) {
            context.startActivity(new Intent(context, MyTaskActivity.class));
//                MobclickAgent.onEvent(context, UmengEventId.getDailyTask());

        } else if (viewId == R.id.me_item_wallet) {
            UIHelper.showWalletAct(context);

        } else if (viewId == R.id.me_item_income) {//打开我的收益 H5
            CommonWebViewActivity.start(context, UriProvider.getMyIncomeH5());

        } else if (viewId == R.id.me_item_feedback) {//反馈
            context.startActivity(new Intent(context, FeedbackActivity.class));

        } else if (viewId == R.id.me_item_auth) {
            CommonWebViewActivity.start(context, UriProvider.getRealNameAuthUrl());

        } else if (viewId == R.id.me_item_charge) {
            context.startActivity(new Intent(context, MyWalletNewActivity.class));

        } else if (viewId == R.id.me_item_level) {//                String url = UriProvider.JAVA_WEB_URL + "/ttyy/level/index.html";

            CommonWebViewActivity.start(context, BaseUrl.MY_LEVEL);

        } else if (viewId == R.id.me_car) {//                context.startActivity(new Intent(context, ShopActivity.class));
            DressUpMallActivity.start(context, false);

        } else {
        }
    }
}
