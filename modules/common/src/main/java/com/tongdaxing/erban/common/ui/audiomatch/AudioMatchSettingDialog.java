package com.tongdaxing.erban.common.ui.audiomatch;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class AudioMatchSettingDialog extends BaseCommonDialog {
    private RadioGroup rgGender, rgHide;

    //0 不限，1男，2女
    private int gender = -1;

    private boolean isHideOpen = false;
    private View btnSure;
    //    private SharedPreferences.Editor editor;
//    private SharedPreferences sharedPreferences;
    private RadioButton rbNoLimit, rbMan, rbWoman;
    private OnSettingCompleteListener onSettingCompleteListener;

//    private final String AUDIO_MATCH_KEY = "audio_match_key";

    @Override
    void initView(View view) {
        rbNoLimit = view.findViewById(R.id.rb_no_limit);
        rbMan = view.findViewById(R.id.rb_man);
        rbWoman = view.findViewById(R.id.rb_woman);
        rgGender = view.findViewById(R.id.rg_gender);
        rgHide = view.findViewById(R.id.rg_hide);
        ImageView ivClose = view.findViewById(R.id.iv_close);
        ivClose.setOnClickListener(v -> dismiss());
        btnSure = view.findViewById(R.id.btn_sure);

//        long curUserId = CoreManager.getCore(IAuthCore.class).getCurrentUid();
//        final String audioMatchFilePath = "audio_match_setting_file_" + curUserId;
//        sharedPreferences = getContext().getSharedPreferences(audioMatchFilePath, Activity.MODE_PRIVATE);
//        gender = sharedPreferences.getInt(AUDIO_MATCH_KEY, -1);
//
        if (gender == -1) {
            UserInfo currUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (currUserInfo != null) {
                if (currUserInfo.getGender() == 2) {//如果当前用户是女性，则默认选中男性
                    gender = 1;
                } else {
                    gender = 2;
                }
            }
        }
        setupConfig(isHideOpen, gender);
        initListener();
    }

    private void initListener() {
        btnSure.setOnClickListener(v -> {
            if (onSettingCompleteListener != null) {
//                editor = sharedPreferences.edit();
//                editor.putInt(AUDIO_MATCH_KEY, gender);
//                editor.apply();
                onSettingCompleteListener.onComplete(isHideOpen, gender);
            }
        });
        rgHide.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rb_close) {
                isHideOpen = false;

            } else if (checkedId == R.id.rb_open) {
                isHideOpen = true;

            }
        });

        rbNoLimit.setOnClickListener(v -> gender = 0);

        rbMan.setOnClickListener(v -> gender = 1);

        rbWoman.setOnClickListener(v -> gender = 2);

    }

    public void setOnSettingCompleteListener(OnSettingCompleteListener onSettingCompleteListener) {
        this.onSettingCompleteListener = onSettingCompleteListener;
    }

    public void setupConfig(boolean isHideOpen, int gender) {
        this.gender = gender;
        this.isHideOpen = isHideOpen;
        if (rgGender != null) {
            switch (gender) {
                case 0:
                    rgGender.check(R.id.rb_no_limit);
                    break;
                case 1:
                    rgGender.check(R.id.rb_man);
                    break;
                case 2:
                    rgGender.check(R.id.rb_woman);
                    break;
            }
        }
        if (rgHide != null) {
            rgHide.check(isHideOpen ? R.id.rb_open : R.id.rb_close);
        }
    }

    @Override
    int getContentView() {
        return R.layout.dialog_audio_match_setting;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            Class c = Class.forName("android.support.v4.app.DialogFragment");
            Constructor con = c.getConstructor();
            Object obj = con.newInstance();
            Field dismissed = c.getDeclaredField("mDismissed");
            dismissed.setAccessible(true);
            dismissed.set(obj, false);
            Field shownByMe = c.getDeclaredField("mShownByMe");
            shownByMe.setAccessible(true);
            shownByMe.set(obj, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    public interface OnSettingCompleteListener {
        void onComplete(boolean isHideOpen, int gender);
    }
}
