package com.tongdaxing.erban.common.ui.audiomatch;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;

public class SecondFragment extends BaseFragment {

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_audio_match_second;
    }

    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {

    }

}
