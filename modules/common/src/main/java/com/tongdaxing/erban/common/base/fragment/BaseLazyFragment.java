package com.tongdaxing.erban.common.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * <p> 具备懒加载的fragment </p>
 *
 * @author Administrator
 * @date 2017/11/23
 */
public abstract class BaseLazyFragment extends BaseFragment {

    private boolean mIsInitView;
    private boolean mIsLoaded;
    /**
     * 当前fragment是否还活着
     */
    private boolean mIsDestroyView = false;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIsInitView = true;
    }

    /**
     * 在onResume()中进行数据懒加载 做法参考自{@link com.tongdaxing.erban.common.base.fragment.BaseLazyFragment}
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mIsInitView && !mIsLoaded && !mIsDestroyView) {
            mIsLoaded = true;
            onLazyLoadData();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIsDestroyView = true;
    }

    /**
     * 在onResume()中进行数据懒加载，防止getMavView()为null的情况
     * <p>
     * 如果子类在启动时要做网络请求，请重写该方法
     */
    protected void onLazyLoadData() {
    }

    public void setmIsLoaded(boolean mIsLoaded) {
        this.mIsLoaded = mIsLoaded;
    }
}
