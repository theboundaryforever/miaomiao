package com.tongdaxing.erban.common.ui.home.fragment;

import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.presenter.home.HotTagPresenter;
import com.tongdaxing.erban.common.presenter.home.IHotTagView;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.home.activity.RecommendColumnActivity;
import com.tongdaxing.erban.common.ui.home.adpater.BannerAdapter;
import com.tongdaxing.erban.common.ui.home.adpater.RoomListAdapter;
import com.tongdaxing.erban.common.ui.home.dialog.NewUserRoomGuideDialogFragment;
import com.tongdaxing.erban.common.ui.widget.Banner;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.GridSpaceItemDecoration;
import com.tongdaxing.erban.common.view.BannerIndicatorView;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.banner.BannerClickModel;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.uuzuche.lib_zxing.DisplayUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 热门 标签fragment
 */
@CreatePresenter(HotTagPresenter.class)
public class HotTagFragment extends BaseMvpFragment<IHotTagView, HotTagPresenter> implements IHotTagView {
    public static final int ITEM_COUNT_BY_ROW = 2;//每行显示多少个
    public static final String TAG = "HotTagFragment";
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private RoomListAdapter mAdapter;
    private RoomListAdapter mPopularAdapter;
    private Banner mBanner;
    private BannerIndicatorView mBannerIndicatorView;
    private View mBannerIndicatorPlaceHolderView;
    private RecyclerView mPopularRv;
    private HomeFragment.OnScrollChangedListener onScrollChangedListener;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_home_hot;
    }

    @Override
    public void onFindViews() {
        recyclerView = mView.findViewById(R.id.recycler_view);
        GridLayoutManager manager = new GridLayoutManager(mContext, ITEM_COUNT_BY_ROW);
        recyclerView.setLayoutManager(manager);
        mAdapter = new RoomListAdapter(mContext, BannerClickModel.HOME_ADVERT_BANNER);
        recyclerView.setAdapter(mAdapter);
        int itemMargin = getResources().getDimensionPixelSize(R.dimen.home_room_item_margin);//每一行间距
        GridSpaceItemDecoration decoration = new GridSpaceItemDecoration(0, itemMargin);
        decoration.setupBindAdapter(mAdapter);
        recyclerView.addItemDecoration(decoration, 0);
        recyclerView.setFocusableInTouchMode(true);
        recyclerView.requestFocus();

        initHeader();

        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(mContext, R.color.mm_theme));
    }

    private void initHeader() {
        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.layout_hot_tag_header, null);
        mBanner = headerView.findViewById(R.id.banner);
        mBannerIndicatorView = headerView.findViewById(R.id.hot_tag_header_indicator_view);
        mBannerIndicatorPlaceHolderView = headerView.findViewById(R.id.hot_tag_header_indicator_placeholder_view);
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mBanner.getLayoutParams();
        layoutParams.height = Math.round((DisplayUtils.getScreenWidth(mContext) - DisplayUtil.dip2px(mContext, 15) * 2) * 160 / 345);
        mBanner.setLayoutParams(layoutParams);
        mBanner.setPlayDelay(5000);
        mBanner.setAnimationDurtion(500);
        mBanner.setHintView(null);

        mPopularRv = headerView.findViewById(R.id.popular_rv);
        mPopularAdapter = new RoomListAdapter(mContext);
        mPopularAdapter.setOnItemClickListener((adapter, view, position) -> onRoomClick(mPopularAdapter.getData().get(position)));
        int itemMargin = getResources().getDimensionPixelSize(R.dimen.home_room_item_margin);//每一行间距
        GridLayoutManager manager = new GridLayoutManager(mContext, ITEM_COUNT_BY_ROW);
        GridSpaceItemDecoration decoration = new GridSpaceItemDecoration(0, itemMargin);
        decoration.setupBindAdapter(mPopularAdapter);
        mPopularRv.addItemDecoration(decoration, 0);
        mPopularRv.setLayoutManager(manager);
        mPopularRv.setFocusableInTouchMode(false);
        mPopularRv.requestFocus();
        mPopularRv.setAdapter(mPopularAdapter);
        mAdapter.addHeaderView(headerView);
    }

    @Override
    public void onSetListener() {
        recyclerView.getViewTreeObserver().addOnScrollChangedListener(() -> {
            if (onScrollChangedListener != null) {
                onScrollChangedListener.onScrollChanged(recyclerView);
            }
        });

        mAdapter.setOnLoadMoreListener(() -> {
            if (NetworkUtil.isNetAvailable(mContext)) {
                getMvpPresenter().loadMoreData();
            } else {
                mAdapter.loadMoreEnd(true);
            }
        }, recyclerView);
        mAdapter.setOnItemClickListener((baseQuickAdapter, view, i) -> onRoomClick(mAdapter.getData().get(i)));

        swipeRefreshLayout.setOnRefreshListener(() -> getMvpPresenter().refreshData());
    }

    private void onRoomClick(HomeRoom homeRoom) {
        if (homeRoom.getTargetType() == 1) {//房间
            AVRoomActivity.start(getActivity(), homeRoom.getUid());
            UmengEventUtil.getInstance().onHomeCategoryList(mContext, homeRoom.getRoomTag());
        } else {//推荐栏目
            RecommendColumnActivity.start(mContext, homeRoom.getUid());
        }
    }

    @Override
    public void initiate() {
    }

    @Override
    public void onReloadData() {
        showLoading();
        getMvpPresenter().refreshData();
    }

    @Override
    public void setViewLoading() {
        showLoading();
    }

    /**
     * 设置banner（顶部的）
     */
    @Override
    public void setupBanner(List<BannerInfo> bannerInfoList) {
        if (!ListUtils.isListEmpty(bannerInfoList)) {
            mBanner.setVisibility(View.VISIBLE);
            BannerAdapter bannerAdapter = new BannerAdapter(bannerInfoList, getActivity(), BannerClickModel.HOME_TOP_BANNER);
            bannerAdapter.setImageRoundRadiusId(R.dimen.common_cover_round_size_10);
            mBanner.setAdapter(bannerAdapter);
            if (bannerInfoList.size() <= 1) {
                mBannerIndicatorView.setVisibility(View.GONE);
                mBannerIndicatorPlaceHolderView.setVisibility(View.INVISIBLE);
            } else {
                mBannerIndicatorView.setVisibility(View.VISIBLE);
                mBannerIndicatorPlaceHolderView.setVisibility(View.GONE);
                mBannerIndicatorView.bindWithViewPager(mBanner.getViewPager(), bannerInfoList.size());
            }
            bannerAdapter.notifyDataSetChanged();
        } else {
            mBanner.setVisibility(View.GONE);
            mBannerIndicatorView.setVisibility(View.GONE);
            mBannerIndicatorPlaceHolderView.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 设置人气list
     */
    @Override
    public void setupPopularList(List<HomeRoom> popularInfoList) {
        if (ListUtils.isListEmpty(popularInfoList)) {
            mPopularRv.setVisibility(View.GONE);
        } else {
            mPopularRv.setVisibility(View.VISIBLE);
            mPopularAdapter.replaceData(popularInfoList);
            mAdapter.setPopularInfoListSize(popularInfoList.size());
        }
    }

    /**
     * 显示引导新手期用户进入推荐房间的弹窗
     *
     * @param homeRoom
     */
    @Override
    public void showNewUserRoomGuideDialog(HomeRoom homeRoom) {
        L.debug(TAG, "showNewUserRoomGuideDialog homeRoom: %s", homeRoom);
        if (homeRoom == null || homeRoom.getUid() == 0) {
            L.debug(TAG, "showNewUserRoomGuideDialog homeRoom return");
            return;
        }
        //用户处于新手期，且没有充值过
        UserInfo user = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        L.debug(TAG, "showNewUserRoomGuideDialog user == null : %b", user == null);
        if (user != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
            L.info(TAG, "showNewUserRoomGuideDialog user CreateTime: %s, ChargeNum: %d", sdf.format(new Date(user.getCreateTime())), user.getChargeNum());
        }
        if (user == null || (CoreManager.getCore(IUserCore.class).isNewUser() && user.getChargeNum() <= 0)) {//用户处于新手期，且没有充值过
            if (getActivity() != null) {
                L.debug(TAG, "showNewUserRoomGuideDialog getActivity() != null");
                NewUserRoomGuideDialogFragment fragment = (NewUserRoomGuideDialogFragment) getActivity()
                        .getSupportFragmentManager().findFragmentByTag("NewUserRoomGuideDialogFragment");
                if (fragment == null) {
                    fragment = NewUserRoomGuideDialogFragment.newInstance(homeRoom.getAvatar(), homeRoom.getNick(), homeRoom.getUid());
                }
                if (fragment.isAdded()) {
                    L.debug(TAG, "showNewUserRoomGuideDialog fragment.isAdded");
                    if (fragment.isStateSaved()) {
                        fragment.dismissAllowingStateLoss();
                    }
                } else {
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
                    String rt = sdf.format(calendar.getTime());
                    if (SpUtils.contains(mContext, rt) && ((boolean) SpUtils.get(mContext, rt, false))) {
                        L.info(TAG, "showNewUserRoomGuideDialog SpUtils contains %s, then dialog has showed ", rt);
                        return;
                    }
                    L.info(TAG, "showNewUserRoomGuideDialog SpUtils don't contains %s, then dialog is showing ", rt);
                    fragment.show(getFragmentManager(), "NewUserRoomGuideDialogFragment");
                    SpUtils.put(mContext, rt, true);
                }
            }
        } else {
            L.debug(TAG, "showNewUserRoomGuideDialog user return");
        }
    }

    @Override
    public void setupListView(boolean isRefresh, List<HomeRoom> homeRooms, int bannerPosition) {
        if (isRefresh) {
            LogUtil.d(HotTagFragment.TAG, "setupListView() hideStatus()");
            hideStatus();
            mAdapter.setBannerPosition(bannerPosition);
            swipeRefreshLayout.setRefreshing(false);
            mAdapter.setNewData(homeRooms);
        } else {
            if (!ListUtils.isListEmpty(homeRooms)) {
                LogUtil.d(HotTagFragment.TAG, "setupListView() ListUtils.isListEmpty(homeRooms) = false");
                mAdapter.addData(homeRooms);
                mAdapter.loadMoreComplete();
            } else {
                LogUtil.d(HotTagFragment.TAG, "setupListView() ListUtils.isListEmpty(homeRooms) = true");
                mAdapter.loadMoreEnd(true);
            }
        }
    }

    @Override
    public void setupFailView(boolean isRefresh, String msg) {
        if (isRefresh) {
            if (ListUtils.isListEmpty(mAdapter.getData())) {
                LogUtil.d(HotTagFragment.TAG, "setupFailView() showNetworkErr()");
                showNetworkErr();
            } else {
                LogUtil.d(HotTagFragment.TAG, "setupFailView() toast()");
                toast(msg);
            }
            swipeRefreshLayout.setRefreshing(false);
        } else {
            LogUtil.d(HotTagFragment.TAG, "setupFailView() loadMoreFail()");
            mAdapter.loadMoreFail();
        }
    }

    public void setOnScrollChangedListener(HomeFragment.OnScrollChangedListener onScrollChangedListener) {
        this.onScrollChangedListener = onScrollChangedListener;
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        LogUtil.d(TAG, "onCurrentUserInfoUpdate");
        if (isActivityCreated && getMvpPresenter().isFirstLoad()) {
//            swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(true));
            if (getMvpPresenter().getMvpView() == null) {
                return;
            }
            setViewLoading();
            getMvpPresenter().setIsFirstLoad(false);
            LogUtil.d(TAG, "onCurrentUserInfoUpdate getMvpPresenter().refreshData()");
            getMvpPresenter().refreshData();
        }
    }
}
