package com.tongdaxing.erban.common.ui.audiomatch;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.fragment.BaseFragment;

public class FourthFragment extends BaseFragment {

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_audio_match_four;
    }

    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {

    }

}
