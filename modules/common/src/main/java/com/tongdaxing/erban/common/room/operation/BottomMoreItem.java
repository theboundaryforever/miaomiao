package com.tongdaxing.erban.common.room.operation;

/**
 * Created by Chen on 2019/6/11.
 */
public class BottomMoreItem {
    private int mItemType;
    private String mItemName;
    private int mItemResId;
    private boolean mIsShowScreen;
    private boolean mIsShowGift;

    public int getItemType() {
        return mItemType;
    }

    public void setItemType(int itemType) {
        mItemType = itemType;
    }

    public String getItemName() {
        return mItemName;
    }

    public void setItemName(String itemName) {
        mItemName = itemName;
    }

    public int getItemResId() {
        return mItemResId;
    }

    public void setItemResId(int itemResId) {
        mItemResId = itemResId;
    }

    public boolean isShowScreen() {
        return mIsShowScreen;
    }

    public void setIsShowScreen(boolean isShowScreen) {
        mIsShowScreen = isShowScreen;
    }

    public boolean isShowGift() {
        return mIsShowGift;
    }

    public void setIsShowGift(boolean isShowGift) {
        mIsShowGift = isShowGift;
    }
}
