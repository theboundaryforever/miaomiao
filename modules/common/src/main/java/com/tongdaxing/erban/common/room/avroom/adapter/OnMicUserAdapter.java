package com.tongdaxing.erban.common.room.avroom.adapter;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.uinfo.constant.GenderEnum;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;

/**
 * 麦上用户adapter
 */
public class OnMicUserAdapter extends BaseQuickAdapter<ChatRoomMember, BaseViewHolder> {

    public OnMicUserAdapter() {
        super(R.layout.list_item_online_user);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, ChatRoomMember chatRoomMember) {

        if (chatRoomMember != null) {
            final ImageView sexImage = baseViewHolder.getView(R.id.sex);

            NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(chatRoomMember.getAccount());
            if (nimUserInfo == null) {
                NimUserInfoCache.getInstance().getUserInfoFromRemote(chatRoomMember.getAccount(),
                        new RequestCallbackWrapper<NimUserInfo>() {
                            @Override
                            public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                                if (nimUserInfo != null) {
                                    if (nimUserInfo.getGenderEnum() == GenderEnum.MALE) {
                                        sexImage.setVisibility(View.VISIBLE);
                                        sexImage.setImageResource(R.drawable.icon_man);
                                    } else if (nimUserInfo.getGenderEnum() == GenderEnum.FEMALE) {
                                        sexImage.setVisibility(View.VISIBLE);
                                        sexImage.setImageResource(R.drawable.icon_female);
                                    } else {
                                        sexImage.setVisibility(View.GONE);
                                    }
                                }
                            }
                        });
            } else {
                if (nimUserInfo.getGenderEnum() == GenderEnum.MALE) {
                    sexImage.setVisibility(View.VISIBLE);
                    sexImage.setImageResource(R.drawable.icon_man);
                } else if (nimUserInfo.getGenderEnum() == GenderEnum.FEMALE) {
                    sexImage.setVisibility(View.VISIBLE);
                    sexImage.setImageResource(R.drawable.icon_female);
                } else {
                    sexImage.setVisibility(View.GONE);
                }
            }

            ImageView roomOwnnerTag = baseViewHolder.getView(R.id.room_owner_logo);
            ImageView managerLogo = baseViewHolder.getView(R.id.manager_logo);
            //注意这里不能用MemberType，MemberType为null
            boolean isAdmin = AvRoomDataManager.get().isRoomAdmin(chatRoomMember.getAccount());
            boolean isOwner = AvRoomDataManager.get().isRoomOwner(chatRoomMember.getAccount());
            baseViewHolder.setGone(R.id.manager_logo, isAdmin || isOwner);
            managerLogo.setImageResource(isAdmin ? R.drawable.icon_admin_logo
                    : R.drawable.icon_user_list_room_owner);
            if (!isOwner) {
                roomOwnnerTag.setImageResource(R.drawable.icon_user_list_up_mic);
                roomOwnnerTag.setVisibility(View.VISIBLE);
            } else {
                roomOwnnerTag.setVisibility(View.GONE);
            }

            baseViewHolder.setText(R.id.nick, chatRoomMember.getNick());

            ImageView avatar = baseViewHolder.getView(R.id.avatar);
            ImageLoadUtils.loadAvatar(mContext, chatRoomMember.getAvatar(), avatar);

            TextView nick = baseViewHolder.getView(R.id.nick);
            nick.setTextColor(ContextCompat.getColor(mContext, R.color.color_1A1A1A));
        }
    }
}
