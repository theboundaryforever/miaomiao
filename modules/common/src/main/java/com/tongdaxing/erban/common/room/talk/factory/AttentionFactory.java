package com.tongdaxing.erban.common.room.talk.factory;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.personal.profile.RoomProfileAction;
import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;
import com.tongdaxing.xchat_core.im.custom.bean.PersonalAttentionAttachment;
import com.tongdaxing.xchat_core.room.talk.message.AttentionMessage;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;


/**
 * Created by Administrator on 5/31 0031.
 */

public class AttentionFactory implements AbsBaseViewHolder.ViewHolderFactory {
    @NonNull
    @Override
    public AttentionViewHolder create(@NonNull ViewGroup parent) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_talk_attention_item, parent, false);
        return new AttentionViewHolder(v);
    }

    private static class AttentionViewHolder extends AbsBaseViewHolder<AttentionMessage> {
        private TextView mTvContent;
        private TextView mTvTitle;

        private AttentionViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvContent = itemView.findViewById(R.id.room_personal_attention_content);
            mTvTitle = itemView.findViewById(R.id.room_personal_attention_title);
        }

        @Override
        public void bind(AttentionMessage message) {
            super.bind(message);
            PersonalAttentionAttachment attachment = message.getAttentionAttachment();
            mTvContent.setText(attachment.getContent());
            mTvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UmengEventUtil.getInstance().onEvent(v.getContext(), UmengEventId.getRoomFollow());
                    CoreUtils.send(new RoomProfileAction.OnAttentionClicked());
                }
            });
            if (attachment.isAttention()) {
                mTvTitle.setText("已关注");
            } else {
                mTvTitle.setText(attachment.getTitle());
            }
        }
    }
}

