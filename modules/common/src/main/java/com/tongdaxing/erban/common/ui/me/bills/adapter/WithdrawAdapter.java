package com.tongdaxing.erban.common.ui.me.bills.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.erban.common.ui.me.bills.fragment.WithdrawBillsFragment;
import com.tongdaxing.erban.common.ui.me.bills.fragment.WithdrawRedFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>提现记录adapter  </p>
 * Created by Administrator on 2017/11/7.
 */
public class WithdrawAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> mFragmentList;

    public WithdrawAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (mFragmentList == null) mFragmentList = new ArrayList<>();
        int size = mFragmentList.size();
        BaseFragment fragment = null;
        if (size > 0 && size > position) {
            fragment = mFragmentList.get(position);
        }
        if (position == 0) {
            if (fragment == null) {
                fragment = new WithdrawBillsFragment();
            }
        } else {
            if (fragment == null)
                fragment = new WithdrawRedFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
