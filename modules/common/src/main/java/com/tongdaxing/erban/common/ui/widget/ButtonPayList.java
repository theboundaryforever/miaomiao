package com.tongdaxing.erban.common.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;

/**
 * Created by Administrator on 2018/3/29.
 */

public class ButtonPayList extends RelativeLayout {

    private View inflate;
    private TextView tabName;
    private View line;

    public ButtonPayList(Context context) {

        this(context, null);
    }

    public ButtonPayList(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate = LayoutInflater.from(context).inflate(R.layout.button_pay_list, this);
        tabName = inflate.findViewById(R.id.tv_button_pay_list);
        line = inflate.findViewById(R.id.line_button_pay_list);
    }

    public TextView getTabName() {
        return tabName;
    }

    public View getLine() {
        return line;
    }
}
