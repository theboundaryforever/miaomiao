package com.tongdaxing.erban.common.ui.find.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.ui.find.fragment.ChatHallFragment;
import com.tongdaxing.erban.common.R;

/**
 * 闲聊广场
 * <p>
 * Created by zhangjian on 2019/6/13.
 */
@Route(path = CommonRoutePath.CHAT_HALL_ACTIVITY_ROUTE_PATH)
public class ChatHallActivity extends BaseActivity {

    @Override
    public boolean blackStatusBar() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_hall);
        initView();
    }

    private void initView() {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        ChatHallFragment chatHallFragment = (ChatHallFragment) supportFragmentManager.findFragmentByTag(ChatHallFragment.TAG);
        if (chatHallFragment == null) {
            chatHallFragment = new ChatHallFragment();
        }
        Bundle bundle = new Bundle();
        bundle.putBoolean(ChatHallFragment.IS_NEED_SHOW_TITLE_BAR, true);
        chatHallFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.chat_hall_container, chatHallFragment, ChatHallFragment.TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }
}
