package com.tongdaxing.erban.common.ui.find.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.erban.ui.adapter.BaseAdapterHelper;
import com.erban.ui.adapter.ListAdapter;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.presenter.find.CreateRoomPresenter;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.find.fragment.CustomLabelFragment;
import com.tongdaxing.erban.common.ui.find.fragment.NotCustomLabelFragment;
import com.tongdaxing.erban.common.ui.find.view.ICreateRoomView;
import com.tongdaxing.erban.common.ui.find.view.INotCustomLabelView;
import com.tongdaxing.erban.common.ui.find.widget.dialog.CreateRoomConfirmDialog;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.common.ui.widget.dialog.CommonDialog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.find.ICreateRoomClient;
import com.tongdaxing.xchat_core.find.bean.CreateRoomLabelInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.photopicker.widget.BGAHeightWrapGridView;

/**
 * Function:创建房间
 * Author: Edward on 2019/3/18
 */
@CreatePresenter(CreateRoomPresenter.class)
public class CreateRoomActivity extends BaseMvpActivity<ICreateRoomView, CreateRoomPresenter> implements ICreateRoomView {
    @BindView(R2.id.app_tool_bar)
    AppToolBar appToolBar;
    @BindView(R2.id.btn_create_room)
    Button btnCreateRoom;
    @BindView(R2.id.create_room_type_gridview)
    BGAHeightWrapGridView createRoomTypeGridView;
    @BindView(R2.id.create_room_title)
    EditText createRoomTitle;
    @BindView(R2.id.create_room_notify_all_iv)
    ImageView createRoomNotifyAllIv;

    private int mCurTabChoose = -1;//当前选中房间类型，-1表示没有选择任何房间类型
    private int mOldSelectedIndex = -1;
    private List<Fragment> mFragmentList;
    private ListAdapter<CreateRoomLabelInfo> mGridViewAdapter;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    private boolean isUsingRandomTitle;
    private Random mTitleRandom;
    private boolean isCheckNoticeFans;//是否勾选了一键通知粉丝
    private boolean isFirstLoad = true;
    private SharedPreferences preferences;

    public static void start(Context context) {
        start(context, -1);
    }

    public static void start(Context context, int defaultIndex) {
        Intent intent = new Intent(context, CreateRoomActivity.class);
        intent.putExtra("data", defaultIndex);
        context.startActivity(intent);
    }

    private void setAppTitleBar() {
        appToolBar.setOnLeftImgBtnClickListener(this::finish);
        TextPaint tp = appToolBar.getTvTitle().getPaint();
        tp.setFakeBoldText(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room_for_miaobo);
        ButterKnife.bind(this);
        setAppTitleBar();
        initGridView();
        Intent intent = getIntent();
        if (intent == null) {
            showNoData();
            return;
        }
        final String fileName = "create_room_type_and_label" + CoreManager.getCore(IAuthCore.class).getCurrentUid();
        preferences = getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        mCurTabChoose = intent.getIntExtra("data", 0);
        mTitleRandom = new Random();
        refreshLabelData();
        setCallback();
        getListRoomTitle();

        createRoomTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && isUsingRandomTitle) {
                    createRoomTitle.setText(null);
                }
            }
        });
    }

    private void initGridView() {
        mGridViewAdapter = new ListAdapter<CreateRoomLabelInfo>(this, R.layout.item_create_room_type) {
            @Override
            public void onUpdate(BaseAdapterHelper helper, CreateRoomLabelInfo item, int position) {
                ImageView imageView = helper.getView(R.id.create_room_type_item_iv);
                ImageView imageViewSelected = helper.getView(R.id.create_room_type_item_selecet_iv);
                if (item != null) {
                    switch (item.getType()) {
                        case 3:
                            imageView.setImageResource(R.drawable.ic_create_room_type3);
                            break;
                        case 4:
                            imageView.setImageResource(R.drawable.ic_create_room_type4);
                            break;
                        case 5:
                            imageView.setImageResource(R.drawable.ic_create_room_type5);
                            break;
                        case 6:
                            imageView.setImageResource(R.drawable.ic_create_room_type6);
                            break;
                    }
                    imageViewSelected.setVisibility(item.isSelected() ? View.VISIBLE : View.INVISIBLE);
                }
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item != null) {
                            createRoomTypeGridView.requestFocus();
                            for (CreateRoomLabelInfo createRoomLabelInfo : getData()) {
                                createRoomLabelInfo.setSelected(createRoomLabelInfo == item);
                            }
                            mGridViewAdapter.notifyDataSetChanged();
                            switchFragment(position);
                        }
                    }
                });
            }
        };
        createRoomTypeGridView.setAdapter(mGridViewAdapter);
    }

    private void refreshLabelData() {
        getMvpPresenter().loadData();
    }

    private void switchFragment(int position) {
        try {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();

            //先隐藏所有的
            for (Fragment fragment : mFragmentList) {
                if (fragment.isVisible()) {
                    fragmentTransaction.hide(fragment);
                }
            }
            Fragment fragment = mFragmentList.get(position);
            if (!fragment.isAdded()) {
                fragmentTransaction.add(R.id.create_room_tag_container, fragment, fragment instanceof CustomLabelFragment ? CustomLabelFragment.TAG : NotCustomLabelFragment.TAG);
            }
            fragmentTransaction.show(fragment);
            //commit() 可能会出现java.lang.IllegalStateException  Can not perform this action after onSaveInstanceState
            //fragmentTransaction.commit();
            fragmentTransaction.commitAllowingStateLoss();

            mCurTabChoose = position;
            if (mOldSelectedIndex != -1 && mOldSelectedIndex == mCurTabChoose) {
                btnCreateRoom.setText("进入房间");
            } else {
                btnCreateRoom.setText("创建房间");
            }
            if (isUsingRandomTitle) {
                onCreateRoomTitleRefreshClicked();
            }
        } catch (Exception e) {
            CoreUtils.crashIfDebug(e, "switchFragment");
        }
    }

    @Override
    public void requestRoomInfoFailView(String errorStr) {
        getDialogManager().dismissDialog();
        onNeedRealNameAuthChecked(this, getResources().getString(R.string.real_name_auth_tips, getResources().getString(R.string.real_name_auth_tips_publish_content)));
    }

    private void onNeedRealNameAuthChecked(Context context, String msg) {
        DialogManager mDialogManager = new DialogManager(context);
        mDialogManager.setCanceledOnClickOutside(false);
        mDialogManager.showOkCancelDialog(msg,
                getResources().getString(R.string.real_name_auth_accept),
                getResources().getString(R.string.cancel),
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onOk() {
                        if (context != null) {
                            CommonWebViewActivity.start(context, UriProvider.getRealNameAuthUrl());
                        }
                    }
                });
    }

    @Override
    public void loadDataFailure(String errorStr) {
        getDialogManager().dismissDialog();
        toast(errorStr);
    }

    @CoreEvent(coreClientClass = ICreateRoomClient.class)
    public void createRoomSucceed(int tagId, int type) {
        if (isCheckNoticeFans) {
            UmengEventUtil.getInstance().onEvent(CreateRoomActivity.this, UmengEventId.getCreateRoomCheckNoticeFans());
        }
        getDialogManager().dismissDialog();
        AVRoomActivity.start(this, CoreManager.getCore(IAuthCore.class).getCurrentUid());
        saveCreateRoomTypeAndLabel(tagId, type);
        finish();
    }

    /**
     * 选回上次成功创建房间类型
     */
    private void readCreateRoomTypeAndLabel(List<CreateRoomLabelInfo> createRoomLabelInfos) {
        int roomType = preferences.getInt("roomType", 6);
        mOldSelectedIndex = getMvpPresenter().getIndexByRoomType(roomType);
        if (mOldSelectedIndex == -1) {
            mCurTabChoose = 3;//默认为交友闲聊
        } else {
            mCurTabChoose = mOldSelectedIndex;
        }
        createRoomLabelInfos.get(mCurTabChoose).setSelected(true);
        switchFragment(mCurTabChoose);

        if (!ListUtils.isListEmpty(mFragmentList) && mOldSelectedIndex >= 0) {
            Fragment fragment = mFragmentList.get(mOldSelectedIndex);
            if (!(fragment instanceof INotCustomLabelView)) {
                return;
            }
            INotCustomLabelView iNotCustomLabelView = (INotCustomLabelView) mFragmentList.get(mOldSelectedIndex);
            int tagId = preferences.getInt("tagId", 0);
            if (tagId != 0) {
                iNotCustomLabelView.setDefaultSelected(tagId);
            }
        }
    }

    private void saveCreateRoomTypeAndLabel(int tagId, int type) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("tagId", tagId);
        editor.putInt("roomType", type);
        editor.putBoolean(SpEvent.createRoomNoticeFans, isCheckNoticeFans);
        editor.apply();
    }

    @Override
    public void showCreateRoomConfirmDialog(int tagId, int type) {
        if (CreateRoomActivity.this.isFinishing() || CreateRoomActivity.this.isDestroyed()) {
            return;
        }
        getDialogManager().dismissDialog();
        CreateRoomConfirmDialog dialog = new CreateRoomConfirmDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("tagId", tagId);
        bundle.putInt("type", type);
        bundle.putBoolean("isCheckNoticeFans", isCheckNoticeFans);
        bundle.putString("title", createRoomTitle.getText().toString());
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "");
    }

    private void setCallback() {
        btnCreateRoom.setOnClickListener(v -> {
            if (mFragmentList == null || mFragmentList.size() <= 0) {
                return;
            }
            if (TextUtils.isEmpty(createRoomTitle.getText().toString())) {
                SingleToastUtil.showToast("请输入房间标题");
                return;
            }
            if (mCurTabChoose >= 0 && mCurTabChoose < mFragmentList.size()) {
                Fragment fragment = mFragmentList.get(mCurTabChoose);
                if (!(fragment instanceof INotCustomLabelView)) {
                    SingleToastUtil.showToast("数据错误!");
                    return;
                }
                INotCustomLabelView iNotCustomLabelView = (INotCustomLabelView) mFragmentList.get(mCurTabChoose);
                if (iNotCustomLabelView != null) {
                    int labelId = iNotCustomLabelView.getLabelId();
                    if (labelId != -1) {
                        getDialogManager().showProgressDialog(this, getString(R.string.waiting_text));
                        getMvpPresenter().getCurrentUserRoomInfo(labelId, getMvpPresenter().getRoomTypeByIndex(mCurTabChoose),
                                isCheckNoticeFans, createRoomTitle.getText().toString());
                    } else {
                        SingleToastUtil.showToast("请选择标签类型");
                    }
                }
            } else {
                SingleToastUtil.showToast("数据错误!");
            }
        });
    }

    @Override
    public void setupSuccessView(List<CreateRoomLabelInfo> createRoomLabelInfos) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        int size = createRoomLabelInfos.size();
        mFragmentList = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            Fragment fragment;
            if (createRoomLabelInfos.get(i).isCustomTag()) {
                fragment = new CustomLabelFragment();
            } else {
                fragment = new NotCustomLabelFragment();
            }
            Bundle bundle = new Bundle();
            bundle.putInt("roomType", createRoomLabelInfos.get(i).getType());
            bundle.putParcelableArrayList("data", (ArrayList<CreateRoomLabelInfo.TagListBean>) createRoomLabelInfos.get(i).getTagList());
            fragment.setArguments(bundle);
            mFragmentList.add(fragment);
        }
        fragmentTransaction.commitAllowingStateLoss();
        mGridViewAdapter.replaceAll(createRoomLabelInfos);

        if (mCurTabChoose < 0) {
            readCreateRoomTypeAndLabel(createRoomLabelInfos);
        } else {
            createRoomLabelInfos.get(mCurTabChoose).setSelected(true);
            switchFragment(mCurTabChoose);
        }
    }

    @Override
    public void setupFailView() {
        showNoData();
    }

    @Override
    public void requestOwnRoomInfoSuccessView(RoomInfo data) {
        String title = data.getTitle();
        if (!TextUtils.isEmpty(title)) {
            createRoomTitle.setText(title);
            createRoomTitle.setSelection(title.length());
        }
    }

    @Override
    public void requestNoticeFansCdSuccess(Long countDown) {
        if (countDown == 0) {
            if (isFirstLoad) {//根据sp初始化勾选状态
                if (((Boolean) SpUtils.get(CreateRoomActivity.this, SpEvent.createRoomNoticeFans, false))) {
                    //初始化 勾选
                    createRoomNotifyAllIv.setImageResource(R.drawable.log_in_btn_check_selected);
                    isCheckNoticeFans = true;
                } else {
                    //初始化 不勾选
                    createRoomNotifyAllIv.setImageResource(R.drawable.log_in_btn_check_nor);
                    isCheckNoticeFans = false;
                }
            } else {
                //点击 勾选
                createRoomNotifyAllIv.setImageResource(R.drawable.log_in_btn_check_selected);
                isCheckNoticeFans = true;
            }
        } else {
            if (!isFirstLoad) {
                SingleToastUtil.showToast("该功能正在冷却中，暂不可用");
            }
            //还在倒计时，不可勾选
            createRoomNotifyAllIv.setImageResource(R.drawable.create_room_ic_disable);
            isCheckNoticeFans = false;
        }
        if (isFirstLoad) {
            isFirstLoad = false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getListRoomTitle() {
        getMvpPresenter().enterRoomFromService();
        getMvpPresenter().getRoomNoticeFansCd();
    }

    @OnClick(R2.id.create_room_title_refresh)
    public void onCreateRoomTitleRefreshClicked() {
        UmengEventUtil.getInstance().onEvent(CreateRoomActivity.this, UmengEventId.getCreateRoomRefreshTitle());
        // 使输入法退出
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(createRoomTitle.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        createRoomTypeGridView.requestFocus();
        if (!isUsingRandomTitle) {
            isUsingRandomTitle = true;
        }
        List<String> titleList = mGridViewAdapter.getData().get(mCurTabChoose).getTitleList();
        if (titleList == null || titleList.size() <= 0) {
            return;
        }
        String text = titleList.get(mTitleRandom.nextInt(titleList.size()));
        if (!TextUtils.isEmpty(text)) {
            createRoomTitle.setText(text);
            createRoomTitle.setSelection(text.length());
        }
    }

    @OnClick(R2.id.create_room_notify_all_desc)
    public void onCreateRoomNotifyAllDescClicked() {
        createRoomTypeGridView.requestFocus();
        CommonDialog commonDialog = new CommonDialog();
        commonDialog.setSingleOkMode();
        Bundle bundle = new Bundle();
        bundle.putString("title", getString(R.string.notify_all_fans));
        commonDialog.setArguments(bundle);
        commonDialog.show(getSupportFragmentManager(), "");
        commonDialog.setmOnClickListener(new CommonDialog.OnClickListener() {
            @Override
            public void onOKClick() {
            }

            @Override
            public void onCancelClick() {
            }
        });
    }

    @OnClick(R2.id.create_room_notify_all)
    public void onCreateRoomNotifyAllClicked() {
        createRoomTypeGridView.requestFocus();
        if (isCheckNoticeFans) {
            //点击 取消勾选
            createRoomNotifyAllIv.setImageResource(R.drawable.log_in_btn_check_nor);
            isCheckNoticeFans = false;
            return;
        }
        getMvpPresenter().getRoomNoticeFansCd();
    }
}















