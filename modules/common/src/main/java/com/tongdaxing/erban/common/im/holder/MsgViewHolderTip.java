package com.tongdaxing.erban.common.im.holder;

import android.widget.TextView;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.ui.cpexclusive.CpExclusivePageActivity;
import com.tongdaxing.xchat_core.im.custom.bean.TipAttachment;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

/**
 * tip消息的MsgViewHolder
 * <p>
 * Created by zhangjian on 2019/3/29.
 */
public class MsgViewHolderTip extends MsgViewHolderBase {

    private TextView tv;

    public MsgViewHolderTip(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.layout_msg_view_holder_tip;
    }

    @Override
    protected void inflateContentView() {
        tv = findViewById(R.id.tv);
    }

    // 返回该消息是不是居中显示
    @Override
    protected boolean isMiddleItem() {
        return true;
    }

    // 是否显示头像，默认为显示
    @Override
    protected boolean isShowHeadImage() {
        return false;
    }

    // 是否显示气泡背景，默认为显示
    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected void onItemClick() {
        TipAttachment attachment = (TipAttachment) message.getAttachment();
        if (attachment.isCpInviteSuccess() && CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getCpUser() != null) {
            CpExclusivePageActivity.start(tv.getContext(), CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid(), true);
        } else {
            SingleToastUtil.showToast("你们的CP关系已解除");
        }
    }

    @Override
    protected void bindContentView() {
        TipAttachment attachment = (TipAttachment) message.getAttachment();
        tv.setText(attachment.getTip());
        if (attachment.isCpInviteSuccess()) {
            tv.setBackgroundResource(R.drawable.bg_msg_view_holder_tip_cp_sussess);
        } else {
            tv.setBackgroundResource(R.drawable.bg_msg_view_holder_tip_normal);
        }

//        Map<String, Object> remoteExtension = message.getRemoteExtension();
//        if (remoteExtension != null) {
//            if (remoteExtension.get("isCpInviteSuccess") != null && ((Boolean) remoteExtension.get("isCpInviteSuccess"))) {
//                tv.setBackgroundResource(R.drawable.bg_msg_view_holder_tip_cp_sussess);
//            } else {
//                tv.setBackgroundResource(R.drawable.bg_msg_view_holder_tip_normal);
//            }
//            if (remoteExtension.get("content") != null && !TextUtils.isEmpty((String) remoteExtension.get("content"))) {
//                tv.setText((String) remoteExtension.get("content"));
//            }
//        } else {
//            tv.setText("系统错误");
//        }
    }
}
