package com.tongdaxing.erban.common.ui.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tongdaxing.erban.common.R;
import com.uuzuche.lib_zxing.DisplayUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 标签 单选切换器
 */
public class TabRadioGroup extends RadioGroup {

    private Context context;

    private OnCheckedChangePositionListener onCheckedChangePositionListener;
    private List<Integer> ids;

    public TabRadioGroup(Context context) {
        super(context);
        init(context);
    }

    public TabRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        ids = new ArrayList<>();
        setOrientation(HORIZONTAL);
        setBackgroundResource(R.drawable.ranking_list_whole_tab_bg);
        int padding = DisplayUtil.dip2px(getContext(), 3);
        setPadding(padding, padding, padding, padding);

        setOnCheckedChangeListener((radioGroup, id) -> {
            if (onCheckedChangePositionListener != null) {
                int position = ids.indexOf(id);
                onCheckedChangePositionListener.onCheckedChanged((RadioButton) getChildAt(position), position);
            }
        });
    }

    /**
     * 设置标签
     *
     * @param tabTexts     标签文字
     * @param tabTextColor 标签文字颜色
     */
    public void setupTabList(List<String> tabTexts, ColorStateList tabTextColor) {
        removeAllViews();
        for (String tabText : tabTexts) {
            RadioButton tabRb = (RadioButton) LayoutInflater.from(context).inflate(R.layout.layout_tab_radio_btn, this, false);
            int id = View.generateViewId();
            tabRb.setId(id);
            tabRb.setTextColor(tabTextColor);
            tabRb.setText(tabText);
            addView(tabRb);
        }
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        ids.add(child.getId());
    }

    @Override
    public void removeAllViews() {
        super.removeAllViews();
        ids.clear();
    }

    /**
     * 选中
     */
    public void checkPosition(int position) {
        RadioButton tabTb = ((RadioButton) getChildAt(position));
        if (tabTb != null) {
            tabTb.setChecked(true);
        }
    }

    public void setOnCheckedChangePositionListener(OnCheckedChangePositionListener onCheckedChangePositionListener) {
        this.onCheckedChangePositionListener = onCheckedChangePositionListener;
    }

    public interface OnCheckedChangePositionListener {
        void onCheckedChanged(RadioButton radioButton, int position);
    }
}
