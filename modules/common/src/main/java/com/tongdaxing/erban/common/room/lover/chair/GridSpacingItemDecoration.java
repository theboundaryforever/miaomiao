package com.tongdaxing.erban.common.room.lover.chair;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

/**
 * User: wukai
 * Date: 2017/03/26
 * Description: 设置RecyclerView布局垂直和水平间隔
 */
public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int mVerSpacing;
    private int mHorSpacing;
    private boolean mIncludeEdge;

    public GridSpacingItemDecoration(int spacing, boolean includeEdge) {
        this(spacing, spacing, includeEdge);
    }

    public GridSpacingItemDecoration(int verSpacing, int horSpacing, boolean includeEdge) {
        mVerSpacing = verSpacing;
        mHorSpacing = horSpacing;
        mIncludeEdge = includeEdge;
    }

    private int getSpanCount(RecyclerView parent) {
        int spanCount = -1;
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            spanCount = ((GridLayoutManager) layoutManager).getSpanCount();
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            spanCount = ((StaggeredGridLayoutManager) layoutManager).getSpanCount();
        }
        return spanCount;
    }

    private int getSpanIndex(View view, RecyclerView parent) {
        int spanIndex = parent.getChildAdapterPosition(view);
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            spanIndex = layoutParams.getSpanIndex();
        }
        return spanIndex;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int spanCount = getSpanCount(parent);
        int position = parent.getChildAdapterPosition(view); // item position
        int spanIndex = getSpanIndex(view, parent);
        int column = spanIndex % spanCount; // item column

        if (mIncludeEdge) {
            outRect.left = mHorSpacing - column * mHorSpacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * mHorSpacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

            if (position < spanCount) { // top edge 第一行设置top
                outRect.top = mVerSpacing;
            }
            outRect.bottom = mVerSpacing; // item bottom 每次都是设置bottom
        } else {
            outRect.left = column * mHorSpacing / spanCount; // column * ((1f / spanCount) * spacing)
            outRect.right = mHorSpacing - (column + 1) * mHorSpacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= spanCount) { // 第二行开始设置top
                outRect.top = mVerSpacing; // item top
            }
        }
    }

}
