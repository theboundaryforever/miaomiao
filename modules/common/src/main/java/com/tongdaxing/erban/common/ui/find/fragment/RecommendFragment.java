package com.tongdaxing.erban.common.ui.find.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.juxiao.library_ui.widget.NestedScrollSwipeRefreshLayout;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.presenter.find.RecommendPresenter;
import com.tongdaxing.erban.common.ui.find.activity.VoiceGroupDetailActivity;
import com.tongdaxing.erban.common.ui.find.adapter.RecommendAdapter;
import com.tongdaxing.erban.common.ui.find.event.VoiceGroupDeletedEvent;
import com.tongdaxing.erban.common.ui.find.view.IRecommendView;
import com.tongdaxing.erban.common.utils.NewAndOldUserEventUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.find.IVoiceGroupClient;
import com.tongdaxing.xchat_core.find.bean.RecommendModuleInfo;
import com.tongdaxing.xchat_core.find.bean.VoiceGroupInfo;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.PraiseEvent;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.uuzuche.lib_zxing.DisplayUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import static com.tongdaxing.erban.common.ui.find.adapter.RecommendAdapter.VOICE_GROUP_ITEM;

/**
 * Function:发现-声圈-推荐页
 * Author: Edward on 2019/3/12
 */
@CreatePresenter(RecommendPresenter.class)
public class RecommendFragment extends BaseMvpFragment<IRecommendView, RecommendPresenter> implements IRecommendView {
    private final int INSERT_DATA_POSITION = 3;
    protected RecommendAdapter recommendAdapter;
    private NestedScrollSwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView recyclerView;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_recommend;
    }

    @Override
    public void onFindViews() {
        recyclerView = mView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        recommendAdapter = new RecommendAdapter(null);
        recommendAdapter.setVoiceGroupLikeCallback(id -> {
            NewAndOldUserEventUtils.onCommonTypeEvent(mContext, UmengEventId.getMomentLike());
            getMvpPresenter().voiceGroupLike(id);
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recommendAdapter);
        recyclerView.setFocusable(false);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.getBoolean("isFromUserInfoActivity")) {
            recyclerView.setPadding(0, 0, 0, DisplayUtil.dip2px(mContext, 70));
        }
    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        onReloadData();//进来加载一次
    }

    @Override
    public void insertRecommendModuleItemsData(List<RecommendModuleInfo> moduleInfoList) {
        if (recommendAdapter != null && moduleInfoList != null && moduleInfoList.size() > 0) {
            VoiceGroupInfo voiceGroupInfo = new VoiceGroupInfo();
            voiceGroupInfo.setItemType(RecommendAdapter.RECOMMEND_MODULE_ITEM);
            voiceGroupInfo.setRecommendModuleInfoList(moduleInfoList);
            List<VoiceGroupInfo> list = recommendAdapter.getData();
            boolean existData = checkData(list, RecommendAdapter.RECOMMEND_MODULE_ITEM, voiceGroupInfo);
            if (!existData) {
                if (list.size() < 4) {
                    recommendAdapter.addData(voiceGroupInfo);//不足（包括）3条，直接添加到尾部
                } else {
                    recommendAdapter.addData(INSERT_DATA_POSITION, voiceGroupInfo);//添加到第四条
                }
            }
        }
    }

    @Override
    public void insertPopularityStarItemsData(List<UserInfo> userInfoList) {
        if (recommendAdapter != null && userInfoList != null && userInfoList.size() > 0) {
            VoiceGroupInfo voiceGroupInfo = new VoiceGroupInfo();
            voiceGroupInfo.setItemType(RecommendAdapter.POPULARITY_STAR_ITEM);
            voiceGroupInfo.setUserInfoList(userInfoList);
            List<VoiceGroupInfo> list = recommendAdapter.getData();
            boolean existData = checkData(list, RecommendAdapter.POPULARITY_STAR_ITEM, voiceGroupInfo);
            if (!existData) {
                if (list.size() < 4) {
                    recommendAdapter.addData(voiceGroupInfo);//不足（包括）3条，直接添加到尾部
                } else {
                    recommendAdapter.addData(INSERT_DATA_POSITION, voiceGroupInfo);//添加到第四条
                }
            }
        }
    }

    /**
     * 此方法是修复线上偶然出现两条人气之星bug
     *
     * @param list
     * @param itemType
     * @param insertData
     * @return
     */
    private boolean checkData(List<VoiceGroupInfo> list, int itemType, VoiceGroupInfo insertData) {
        if (!ListUtils.isListEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {//检查列表是否存在此数据
                VoiceGroupInfo voiceGroupInfo = list.get(i);
                if (voiceGroupInfo.getItemType() == itemType) {//如果已有数据则更新数据
                    list.set(i, insertData);
                    return true;
                }
            }
        }
        return false;
    }

    protected void startVoiceGroupDetailPage(VoiceGroupInfo voiceGroupInfo, int index) {
        VoiceGroupDetailActivity.start(getContext(), voiceGroupInfo, index);
    }

    @Override
    public void onSetListener() {
        recommendAdapter.setOnItemClickListener((adapter, view, position) -> {
            VoiceGroupInfo voiceGroupInfo = ((RecommendAdapter) adapter).getItem(position);
            if (voiceGroupInfo == null || voiceGroupInfo.getItemType() != VOICE_GROUP_ITEM) {//只有动态item才能点击
                return;
            }
            if (voiceGroupInfo.getId() > 0) {
                startVoiceGroupDetailPage(voiceGroupInfo, position);
            } else {
                toast(getResources().getString(R.string.data_exception));
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(this::setRefreshData);
        recommendAdapter.setOnLoadMoreListener(this::setLoadMoreData, recyclerView);
    }

    protected void setLoadMoreData() {
        getMvpPresenter().loadMoreData("3", "");
    }

    protected void setRefreshData() {
        getMvpPresenter().refreshData("3", "");
    }

    @CoreEvent(coreClientClass = IVoiceGroupClient.class)
    public void publishVoiceGroupRefreshList() {//发布动态之后就刷新
        setRefreshData();
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        if (recommendAdapter != null) {
            if (likedUid == recommendAdapter.getCurClickUserId()) {
                List<VoiceGroupInfo> list = recommendAdapter.getData();
                for (int i = 0; i < list.size(); i++) {
                    VoiceGroupInfo voiceGroupInfo = list.get(i);
                    if (voiceGroupInfo.getUid() == likedUid) {
                        voiceGroupInfo.setFans(true);
                        list.set(i, voiceGroupInfo);
                    }
                }
                recommendAdapter.notifyDataSetChanged();
            }
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFail(String error) {
        toast(error);
    }

    @Override
    public void initiate() {
    }

    @Override
    public void setupFailView(boolean isRefresh) {
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            showNetworkErr();
        } else {
            recommendAdapter.loadMoreFail();
        }
    }

    @Override
    public void refreshAdapterLikeStatus(int commentId) {
        if (recommendAdapter != null) {
            List<VoiceGroupInfo> list = recommendAdapter.getData();
            for (int i = 0; i < list.size(); i++) {
                VoiceGroupInfo voiceGroupInfo = list.get(i);
                if (voiceGroupInfo.getId() == commentId) {
                    voiceGroupInfo.setIsLike(!voiceGroupInfo.isIsLike());
                    if (voiceGroupInfo.isIsLike()) {
                        voiceGroupInfo.setLikeNum(voiceGroupInfo.getLikeNum() + 1);
                    } else {
                        if (voiceGroupInfo.getLikeNum() < 0) {
                            voiceGroupInfo.setLikeNum(0);
                        } else {
                            voiceGroupInfo.setLikeNum(voiceGroupInfo.getLikeNum() - 1);
                        }
                    }
                    list.set(i, voiceGroupInfo);
                    recommendAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    @Override
    public void setupSuccessView(List<VoiceGroupInfo> voiceGroupInfoList, boolean isRefresh) {
        hideStatus();
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            if (ListUtils.isListEmpty(voiceGroupInfoList)) {
                showNoData();
                voiceGroupInfoList = new ArrayList<>();
            }
            recommendAdapter.setNewData(voiceGroupInfoList);
        } else {
            recommendAdapter.loadMoreComplete();
            if (!ListUtils.isListEmpty(voiceGroupInfoList)) {
                recommendAdapter.addData(voiceGroupInfoList);
            }
        }
        //不够10个的话，不显示加载更多
        if (voiceGroupInfoList.size() < Constants.PAGE_SIZE) {
            recommendAdapter.setEnableLoadMore(false);
        }
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        showLoading();
        setRefreshData();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVoiceGroupDeletedEvent(VoiceGroupDeletedEvent event) {
        LogUtil.d("RecommendFragment onVoiceGroupDeletedEvent：" + event);
        if (event != null && event.id != 0 && event.index != -1) {
            if (recommendAdapter.getData().get(event.index) != null &&
                    recommendAdapter.getData().get(event.index).getId() == event.id) {
                recommendAdapter.remove(event.index);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionSuccess(PraiseEvent.OnAttentionSuccess success) {
        long uid = success.getUid();
        L.debug(TAG, "onAttentionSuccess uid: %d", uid);
        refreshAdapterAttentionStatus(uid, success.isAttention());
//        if (!success.isCheckAttention()) {  // 主动关注就提示，如果是检测是否存在关注关系的，就不提示
//            if (success.isAttention()) {
//                toast("关注成功");
//            }
//        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionCancel(PraiseEvent.OnAttentionCancel cancel) {
        refreshAdapterAttentionStatus(cancel.getUid(), false);
//        toast("已取消关注");
    }

    public void refreshAdapterAttentionStatus(long commentId, boolean isAttention) {
        if (recommendAdapter != null) {
            List<VoiceGroupInfo> list = recommendAdapter.getData();
            for (int i = 0; i < list.size(); i++) {
                VoiceGroupInfo voiceGroupInfo = list.get(i);
                if (voiceGroupInfo.getUid() == commentId) {
                    voiceGroupInfo.setFans(isAttention);
                    list.set(i, voiceGroupInfo);
                    recommendAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

}
