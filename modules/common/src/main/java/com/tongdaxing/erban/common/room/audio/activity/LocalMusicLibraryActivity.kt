package com.tongdaxing.erban.common.room.audio.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.netease.nim.uikit.common.util.sys.NetworkUtil
import com.tongdaxing.erban.common.R
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity
import com.tongdaxing.erban.common.room.audio.adapter.LocalMusicLibraryAdapter
import com.tongdaxing.erban.common.room.audio.adapter.LocalMusicLibraryAdapter.*
import com.tongdaxing.erban.common.room.audio.presenter.LocalMusicLibraryPresenter
import com.tongdaxing.erban.common.room.audio.view.ILocalMusicLibraryView
import com.tongdaxing.erban.common.room.audio.widget.CloudUploadDialog
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter
import com.tongdaxing.xchat_core.music.IMusicCore
import com.tongdaxing.xchat_core.music.IMusicUploadCore
import com.tongdaxing.xchat_core.music.IMusicUploadCoreClient
import com.tongdaxing.xchat_core.player.IPlayerCore
import com.tongdaxing.xchat_core.player.IPlayerCoreClient
import com.tongdaxing.xchat_core.player.IPlayerDbCore
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo
import com.tongdaxing.xchat_framework.coremanager.CoreEvent
import com.tongdaxing.xchat_framework.coremanager.CoreManager
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil
import kotlinx.android.synthetic.main.activity_local_music_library.*
import java.io.File

/**
 * Function:
 * Author: Edward on 2019/2/14
 */
@CreatePresenter(LocalMusicLibraryPresenter::class)
class LocalMusicLibraryActivity : BaseMvpActivity<ILocalMusicLibraryView, LocalMusicLibraryPresenter>(), ILocalMusicLibraryView {
    private var myMusicInfoList: List<MyMusicInfo>? = null
    override fun loadSongSuccess(myMusicInfoList: List<MyMusicInfo>) {
        this.myMusicInfoList = myMusicInfoList
        CoreManager.getCore(IPlayerCore::class.java).refreshLocalMusic(null)
    }

    override fun loadSongFailure() {
        CoreManager.getCore(IPlayerCore::class.java).refreshLocalMusic(null)
    }

    private var localMusicLibraryAdapter: LocalMusicLibraryAdapter? = null

    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, LocalMusicLibraryActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_local_music_library)

        init()
        localMusicScan()
    }

    /**
     * 本地音乐扫描
     */
    private fun localMusicScan() {
        //开始扫描
        rl_local_scan.visibility = View.VISIBLE
        ll_data.visibility = View.GONE
        mvpPresenter.getMySongList()
    }

    @CoreEvent(coreClientClass = IMusicUploadCoreClient::class)
    fun onMusicUploadError(msg: String, musicLocalInfo: MusicLocalInfo) {
        if (localMusicLibraryAdapter != null) {
            val list = localMusicLibraryAdapter!!.data
            for (i in list.indices) {
                if (list[i].localUri == musicLocalInfo.localUri) {
                    var temp = MusicLocalInfo()
                    temp.copy(list[i])
                    temp.fileStatus = FILE_STATUS_UPLOAD
                    localMusicLibraryAdapter!!.setData(i, temp)
                    localMusicLibraryAdapter!!.notifyDataSetChanged()
                    return
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient::class)
    fun onRefreshLocalMusic(localMusicInfoList: MutableList<MusicLocalInfo>) {
        fun1(localMusicInfoList)
        localMusicLibraryAdapter!!.setNewData(localMusicInfoList)

        rl_local_scan.visibility = View.GONE
        ll_data.visibility = View.VISIBLE
    }

    private fun fun1(localMusicInfoList: MutableList<MusicLocalInfo>) {
        for (i in 0 until localMusicInfoList.size) {
            if (myMusicInfoList != null && myMusicInfoList!!.isNotEmpty()) {
                var flag = false
                for (j in 0 until myMusicInfoList!!.size) {
                    if (localMusicInfoList[i].songName == myMusicInfoList!![j].title &&
                            localMusicInfoList[i].singerName == myMusicInfoList!![j].artist &&
                            localMusicInfoList[i].fileSize == myMusicInfoList!![j].size) {
                        val musicLocalInfo = CoreManager.getCore(IMusicCore::class.java).getCacheMusicInfo(localMusicInfoList[i].songName, localMusicInfoList[i].singerName, localMusicInfoList[i].fileSize)
                        if (musicLocalInfo != null) {//缓存存在显示为空
                            localMusicInfoList[i] = musicLocalInfo
                            if (musicLocalInfo.localUri.contains(CoreManager.getCore(IMusicCore::class.java).currentUserFolderName)) {
                                CoreManager.getCore(IPlayerDbCore::class.java).updateFromLocalMusic(localMusicInfoList[i],
                                        localMusicInfoList[i].songId, FILE_STATUS_NO)
                            } else {
                                CoreManager.getCore(IPlayerDbCore::class.java).updateFromLocalMusic(localMusicInfoList[i],
                                        localMusicInfoList[i].songId, FILE_STATUS_UPLOAD)
                            }
                        } else {//缓存不存存在显示上传
                            CoreManager.getCore(IPlayerDbCore::class.java).updateFromLocalMusic(localMusicInfoList[i],
                                    localMusicInfoList[i].songId, FILE_STATUS_UPLOAD)
                        }
                        flag = true
                        break
                    }
                }

                if (!flag) {//直接显示上传状态
                    CoreManager.getCore(IPlayerDbCore::class.java).updateFromLocalMusic(localMusicInfoList[i],
                            localMusicInfoList[i].songId, FILE_STATUS_UPLOAD)
                }
            } else {
                val musicLocalInfo = CoreManager.getCore(IMusicCore::class.java).getCacheMusicInfo(localMusicInfoList[i].songName, localMusicInfoList[i].singerName, localMusicInfoList[i].fileSize)
                if (musicLocalInfo != null) {
                    localMusicInfoList[i] = musicLocalInfo
                    if (musicLocalInfo.localUri.contains(CoreManager.getCore(IMusicCore::class.java).currentUserFolderName)) {
                        CoreManager.getCore(IPlayerDbCore::class.java).updateFromLocalMusic(localMusicInfoList[i],
                                localMusicInfoList[i].songId, FILE_STATUS_NO)
                    } else {
                        CoreManager.getCore(IPlayerDbCore::class.java).updateFromLocalMusic(localMusicInfoList[i],
                                localMusicInfoList[i].songId, FILE_STATUS_UPLOAD)
                    }
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient::class)
    fun onLoadCacheSuccess(localMusicInfoList: List<MusicLocalInfo>) {

    }

    /**
     * 直接复制文件
     */
    override fun copyDirectly(myMusicInfo: MyMusicInfo, localMusicInfo: MusicLocalInfo) {
        if (myMusicInfo.artist == localMusicInfo.singerName && myMusicInfo.title == localMusicInfo.songName &&
                myMusicInfo.size == localMusicInfo.fileSize) {//如果歌曲名，歌手名，大小三者信息跟云端一样
            mvpPresenter.addSongToMyLibrary(myMusicInfo, localMusicInfo)
        }
    }

    override fun refreshAdapter() {
        localMusicLibraryAdapter!!.notifyDataSetChanged()
    }

    /**
     * 上传之后复制
     */
    override fun copyAfterUpload(localMusicInfo: MusicLocalInfo) {
        CoreManager.getCore(IMusicUploadCore::class.java).addUploadQueue(localMusicInfo)
    }

    @CoreEvent(coreClientClass = IMusicUploadCoreClient::class)
    fun onHotMusicStartUpload(localMusicInfo: MusicLocalInfo) {
        if (localMusicLibraryAdapter != null) {
            val list = localMusicLibraryAdapter!!.data
            for (i in list.indices) {
                if (list[i] == localMusicInfo) {
                    CoreManager.getCore(IPlayerDbCore::class.java).updateFromLocalMusic(localMusicInfo,
                            localMusicInfo.songId, FILE_STATUS_UPLOADING)
                    localMusicLibraryAdapter!!.setData(i, localMusicInfo)
                    localMusicLibraryAdapter!!.notifyDataSetChanged()
                    return
                }
            }
        }
    }

    private fun init() {
        app_tool_bar.setOnLeftImgBtnClickListener {
            finish()
        }
        app_tool_bar!!.setOnRightTitleClickListener {
            var cloudUploadDialog = CloudUploadDialog()
            cloudUploadDialog.show(supportFragmentManager, "")
        }
        //点击扫描
        ll_repeat_scan.setOnClickListener {
            localMusicScan()
        }

        localMusicLibraryAdapter = LocalMusicLibraryAdapter(this)
        localMusicLibraryAdapter!!.setOnItemClickListener { adapter, view, position ->
            var tempInfo = localMusicLibraryAdapter!!.getItem(position)
            if (tempInfo!!.fileStatus == FILE_STATUS_UPLOAD) {
                if (NetworkUtil.isNetAvailable(this)) {
                    val file = File(tempInfo.localUri)
                    if (file.isFile) {
                        CoreManager.notifyClients(IMusicUploadCoreClient::class.java, IMusicUploadCoreClient.METHOD_ON_MUSIC_START_UPLOAD, tempInfo)//用于刷新视图
                        mvpPresenter.isExistMusicFromService(tempInfo)
                    }
                } else {
                    SingleToastUtil.showToast("网络不可用!")
                }
            }
        }
        recycler_view.adapter = localMusicLibraryAdapter
        recycler_view.layoutManager = LinearLayoutManager(this)
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient::class)
    fun musicUploadSuccess(musicLocalInfo: MusicLocalInfo) {
        notifyData(musicLocalInfo, FILE_STATUS_NO)
    }

    private fun notifyData(musicLocalInfo: MusicLocalInfo, status: Int) {
        if (localMusicLibraryAdapter != null) {
            val list = localMusicLibraryAdapter!!.data
            for (i in list.indices) {
                if (list[i] == musicLocalInfo) {
                    CoreManager.getCore(IPlayerDbCore::class.java).updateFromLocalMusic(musicLocalInfo,
                            musicLocalInfo.songId, status)
                    localMusicLibraryAdapter!!.setData(i, musicLocalInfo)
                    localMusicLibraryAdapter!!.notifyDataSetChanged()
                    return
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient::class)
    fun musicUploadFailure(musicLocalInfo: MusicLocalInfo) {
        notifyData(musicLocalInfo, FILE_STATUS_UPLOAD)
    }
}
