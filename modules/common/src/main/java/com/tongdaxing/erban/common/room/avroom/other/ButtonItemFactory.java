package com.tongdaxing.erban.common.room.avroom.other;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.orhanobut.logger.Logger;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.im.actions.GiftAction;
import com.tongdaxing.erban.common.model.ReportModel;
import com.tongdaxing.erban.common.room.avroom.activity.RoomBlackListActivity;
import com.tongdaxing.erban.common.room.avroom.activity.RoomManagerListActivity;
import com.tongdaxing.erban.common.room.gift.GiftDialog;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.erban.common.room.widget.dialog.AuctionDialog;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AuctionModel;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.SingleSource;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Function;


/**
 * @author chenran
 * @date 2017/10/11
 */
public class ButtonItemFactory {
    private static final String TAG = "ButtonItemFactory";
    private static ReportModel reportModel;
    private static HttpRequestCallBack<Object> reportCallBack = new HttpRequestCallBack<Object>() {
        @Override
        public void onFinish() {

        }

        @Override
        public void onSuccess(String message, Object response) {
            SingleToastUtil.showToast("举报成功，我们会尽快为您处理");
        }

        @Override
        public void onFailure(int code, String msg) {
            SingleToastUtil.showToast(msg);
        }
    };

    /**
     * 公屏点击的所有items
     *
     * @return -
     */
    public static List<ButtonItem> createAllRoomPublicScreenButtonItems(Context context, String account) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null
                || TextUtils.isEmpty(account)) return null;
        List<ButtonItem> buttonItems = new ArrayList<>();
        String roomId = String.valueOf(roomInfo.getRoomId());

        ChatRoomMember chatRoomMember = AvRoomDataManager.get().getChatRoomMember(account);
        L.debug(TAG, "account:" + account + "   " + "chatRoomMember:" + chatRoomMember);
        boolean isMyself = AvRoomDataManager.get().isSelf(account);
        boolean isTargetGuess = AvRoomDataManager.get().isGuess(account);
        boolean isTargetRoomAdmin = AvRoomDataManager.get().isRoomAdmin(account);
        boolean isTargetRoomOwner = AvRoomDataManager.get().isRoomOwner(account);
        boolean isTargetOnMic = AvRoomDataManager.get().isOnMic(account);
        boolean isInAuctionNow = AuctionModel.get().isInAuctionNow();
        boolean isTypeAuction = roomInfo.getType() == RoomInfo.ROOMTYPE_AUCTION;
        boolean isPersonal = roomInfo.getTagType() == RoomInfo.ROOMTYPE_PERSONAL;
        // 房主点击
        if (AvRoomDataManager.get().isRoomOwner()) {
            if (isTargetRoomOwner) {
                // 非竞拍房
                if (!isTypeAuction) {
                    UserInfoDialogManager.showDialogFragment(context, JavaUtil.str2long(account));
                    return null;
                }
                if (!isPersonal) {
                    //送礼物
                    buttonItems.add(createSendGiftItem(context, account));
                }
                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account));
                // 发起竞拍
                buttonItems.add(createStartAuctionItem(context, JavaUtil.str2long(roomId), account));
            } else if (isTargetRoomAdmin || isTargetGuess) {
                if (!isPersonal) {
                    //送礼物
                    buttonItems.add(createSendGiftItem(context, account));
                }
                // 增加/移除管理员
                buttonItems.add(createMarkManagerListItem(roomId, account, isTargetGuess));
                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account));
                // 发起竞拍
                if (isTypeAuction && !isInAuctionNow)
                    buttonItems.add(createStartAuctionItem(context, JavaUtil.str2long(roomId), account));
                // 抱Ta上麦
                if (!isTargetOnMic) {
                    int type = roomInfo.getTagType();
                    if (type != RoomInfo.ROOMTYPE_PERSONAL) {
                        buttonItems.add(createInviteOnMicItem(account));
                    }
                }
                // 踢出房间
                buttonItems.add(createKickOutRoomItem(context, chatRoomMember, roomId, account));

                // 加入黑名单
                buttonItems.add(createMarkBlackListItem(context, chatRoomMember, roomId));
            }
        } else if (AvRoomDataManager.get().isRoomAdmin()) {
            if (isMyself) {
                // 非竞拍房
                if (!isTypeAuction) {
                    UserInfoDialogManager.showDialogFragment(context, JavaUtil.str2long(account));
                    return null;
                }
                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account));
                // 发起竞拍
                buttonItems.add(createStartAuctionItem(context, JavaUtil.str2long(roomId), account));
            } else if (isTargetRoomAdmin || isTargetRoomOwner) {
                if (isTargetOnMic) {
                    UserInfoDialogManager.showDialogFragment(context, JavaUtil.str2long(account));
                    return null;
                }
                if (!isPersonal) {
                    //送礼物
                    buttonItems.add(createSendGiftItem(context, account));
                    // 抱Ta上麦
                    buttonItems.add(createInviteOnMicItem(account));
                }
                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account));
                // 发起竞拍
                if (isTypeAuction && !isInAuctionNow)
                    buttonItems.add(createStartAuctionItem(context, JavaUtil.str2long(roomId), account));

            } else if (isTargetGuess) {
                if (!isPersonal) {
                    //送礼物
                    buttonItems.add(createSendGiftItem(context, account));
                    // 抱Ta上麦
                    if (!isTargetOnMic)
                        buttonItems.add(createInviteOnMicItem(account));
                }
                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account));
                // 发起竞拍
                if (isTypeAuction && !isInAuctionNow)
                    buttonItems.add(createStartAuctionItem(context, JavaUtil.str2long(roomId), account));
                // 踢出房间
                buttonItems.add(createKickOutRoomItem(context, chatRoomMember, roomId, account));
                // 加入黑名单
                buttonItems.add(createMarkBlackListItem(context, chatRoomMember, roomId));
            }
        } else if (AvRoomDataManager.get().isGuess()) {
            if (AvRoomDataManager.get().isSelf(account)) {
                UserInfoDialogManager.showDialogFragment(context, JavaUtil.str2long(account));
            } else {
//                if (AvRoomDataManager.get().isOnMic(account)) {
//                    showGiftDialog(context, account);
//                } else {
//                }
                UserInfoDialogManager.showDialogFragment(context, JavaUtil.str2long(account));
            }
            return null;
        }
        return buttonItems;
    }

    /**
     * 出现礼物弹框
     *
     * @param context
     * @param account
     */
    private static void showGiftDialog(Context context, String account) {
        GiftDialog giftDialog = new GiftDialog(context, JavaUtil.str2long(account), "", "");

        giftDialog.setGiftDialogBtnClickListener(new GiftDialog.OnGiftDialogBtnClickListener() {
            @Override
            public void onRechargeBtnClick() {
                MyWalletNewActivity.start(context);
            }

            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number, int sendGiftType) {
                RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (currentRoomInfo == null) return;
                List<Long> uids = new ArrayList<>();
                uids.add(uid);
                CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), uids,
                        currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
            }

            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number, int sendGiftType) {
                RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (currentRoomInfo == null) return;
                List<Long> targetUids = new ArrayList<>();
                for (int i = 0; i < micMemberInfos.size(); i++) {
                    targetUids.add(micMemberInfos.get(i).getUid());
                }
                CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), targetUids,
                        currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
            }

            /**
             * 赠送按钮点击
             * CP判断
             *
             * @param giftInfo
             * @param uid
             */
            @Override
            public void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid) {
                RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (currentRoomInfo == null) return;
                if (giftInfo != null) {
                    L.debug(GiftAction.TAG, "ButtonItemFactory onSendCpGiftBtnClick giftInfo=%s, uid=%d", giftInfo, uid);
                    CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftInfo.getGiftId(), uid,
                            1, currentRoomInfo.getUid(), new ChargeListener() {
                                @Override
                                public void onNeedCharge() {
                                    ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
                                        @Override
                                        public void onClick(View view, ChargeDialogFragment fragment) {
                                            if (view.getId() == R.id.btn_cancel) {
                                                fragment.dismiss();
                                            } else if (view.getId() == R.id.btn_ok) {
                                                MyWalletNewActivity.start(context);
                                                fragment.dismiss();
                                            }
                                        }
                                    }).show(((FragmentActivity) context).getSupportFragmentManager(), "charge");
                                }
                            });
                }
            }
        });
        giftDialog.show();
    }

    /**
     * 发起竞拍
     */
    private static ButtonItem createStartAuctionItem(final Context context, final long roomUid, final String account) {
        return new ButtonItem("发起竞拍", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                AuctionDialog dialog = new AuctionDialog(context, JavaUtil.str2long(account));
                dialog.setOnClickItemListener(new AuctionDialog.OnClickItemListener() {
                    @Override
                    public void onClickHead() {
                        UIHelper.showUserInfoAct(context, JavaUtil.str2long(account));
                    }

                    @Override
                    public void onClickBegin(int price) {
                        AuctionModel.get().startAuction(roomUid, JavaUtil.str2long(account), price,
                                30, 10, "暂无竞拍描述");
                    }
                });
                dialog.show();
            }
        });
    }

    public static ButtonItem createMsgBlackListItem(String s, OnItemClick onItemClick) {


        return new ButtonItem(s, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {

                onItemClick.itemClick();


            }
        });
    }

    /**
     * 踢Ta下麦
     */
    public static ButtonItem createKickDownMicItem(final String account) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.embrace_down_mic),
                new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        if (AvRoomDataManager.get().isOnMic(JavaUtil.str2long(account))) {
                            int micPosition = AvRoomDataManager.get().getMicPosition(JavaUtil.str2long(account));
                            IMNetEaseManager.get().downMicroPhoneBySdk(micPosition, null);

                            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                            if (roomInfo != null) {
                                IMNetEaseManager.get().kickMicroPhoneBySdk(JavaUtil.str2long(account),
                                        roomInfo.getRoomId()).subscribe();
                            }
                        }


                    }
                });
    }

    /**
     * 拉他上麦
     */
    public static ButtonItem createInviteOnMicItem(final String account) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.embrace_up_mic),
                new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        int freePosition = AvRoomDataManager.get().findFreePosition();
                        L.info(TAG, "createInviteOnMicItem freePosition: %d", freePosition);
                        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                        if (currentRoomInfo == null) {
                            return;
                        }
                        if (currentRoomInfo.getTagType() == RoomInfo.ROOMTYPE_LOVERS) {
                            if (freePosition != 0) {
                                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
                                return;
                            }
                        }
                        if (freePosition == Integer.MIN_VALUE) {
                            SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
                            return;
                        }
                        IMNetEaseManager.get().inviteMicroPhoneBySdk(JavaUtil.str2long(account),
                                freePosition).subscribe();
                    }
                });
    }

    /**
     * 踢出房间:  先强制下麦，再踢出房间
     */
    public static ButtonItem createKickOutRoomItem(final Context context, final ChatRoomMember chatRoomMember, final String roomId, final String account) {
        return new ButtonItem("踢出房间", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (!TextUtils.isEmpty(account)) {
                    String chatRoomMemberNick;
                    if (chatRoomMember == null) {
                        chatRoomMemberNick = "TA";
                    } else {
                        chatRoomMemberNick = chatRoomMember.getNick();
                    }
                    ((BaseMvpActivity) context).getDialogManager().showOkCancelDialog(
                            "是否将" + (chatRoomMemberNick == null ? "TA" : chatRoomMemberNick) + "踢出房间？", true,
                            new DialogManager.AbsOkDialogListener() {
                                @Override
                                public void onOk() {
                                    final Map<String, Object> reason = new HashMap<>(2);
                                    reason.put("reason", "kick");
                                    long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                                    reason.put("administratorsUid", currentUid + "");
                                    if (AvRoomDataManager.get().isOnMic(JavaUtil.str2long(account))) {
                                        int micPosition = AvRoomDataManager.get().getMicPosition(JavaUtil.str2long(account));
                                        reason.put("micPosition", micPosition);
                                        reason.put("account", account);
                                        IMNetEaseManager.get().downMicroPhoneBySdk(micPosition, null);

                                    }
                                    //判断是否在排麦
                                    Json json = AvRoomDataManager.get().mMicInListMap.get(Integer.parseInt(account));
                                    if (json != null)
                                        IMNetEaseManager.get().removeMicInList(account, roomId, null);

                                    IMNetEaseManager.get().kickMemberFromRoomBySdk(JavaUtil.str2long(roomId),
                                            JavaUtil.str2long(account), reason).subscribe(new BiConsumer<String, Throwable>() {
                                        @Override
                                        public void accept(String s, Throwable throwable) throws Exception {
                                            if (throwable != null)
                                                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), throwable.getMessage());
                                            else {
                                                Logger.e("kick out mic and room: " + s);
                                                IMNetEaseManager.get().noticeKickOutChatMember(null, account);
                                            }
                                        }
                                    });
                                }
                            });
                }


            }
        });
    }

    /**
     * 查看资料弹窗
     */
    public static ButtonItem createCheckUserInfoDialogItem(final Context context, final String account) {
        return new ButtonItem("查看资料", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                UserInfoDialogManager.showDialogFragment(context, JavaUtil.str2long(account));
            }
        });
    }

    /**
     * 下麦
     */
    public static ButtonItem createDownMicItem() {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.down_mic_text), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                IMNetEaseManager.get().downMicroPhoneBySdk(
                        AvRoomDataManager.get().getMicPosition(currentUid), null);
            }
        });
    }

    //设置管理员
    public static ButtonItem createMarkManagerListItem(final String roomId, final String account, final boolean mark) {
        String title = BasicConfig.INSTANCE.getAppContext().getString(mark ? R.string.set_manager : R.string.remove_manager);
        return new ButtonItem(title, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                IMNetEaseManager.get().markManagerListBySdk(roomId, account, mark, null);
            }
        });
    }

    //发送礼物

    //加入黑名单
    public static ButtonItem createMarkBlackListItem(final Context context, final ChatRoomMember chatRoomMember, final String roomId) {
        return new ButtonItem("加入黑名单", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {

                if (chatRoomMember != null) {
                    ((BaseMvpActivity) context).getDialogManager().showOkCancelDialog(
                            "是否将" + chatRoomMember.getNick() + "加入黑名单？加入后他将无法进入此房间", true,
                            new DialogManager.AbsOkDialogListener() {
                                @Override
                                public void onOk() {
                                    IMNetEaseManager.get().markBlackListBySdk(roomId, chatRoomMember.getAccount(), true)
                                            .flatMap(new Function<String, SingleSource<String>>() {
                                                @Override
                                                public SingleSource<String> apply(String s) throws Exception {

                                                    //判断是否在排麦
                                                    Json json = AvRoomDataManager.get().mMicInListMap.get(Integer.parseInt(chatRoomMember.getAccount()));
                                                    if (json != null)
                                                        IMNetEaseManager.get().removeMicInList(chatRoomMember.getAccount(), roomId, null);

                                                    // : 2018/4/27 拉黑下麦
                                                    int micPosition = AvRoomDataManager.get().getMicPosition(chatRoomMember.getAccount());
                                                    return IMNetEaseManager.get().downMicroPhoneBySdk(micPosition);
                                                }
                                            }).subscribe();
                                }
                            });
                } else {
                    LogUtils.d("incomingChatObserver", "无用户信息");
                }
            }
        });
    }

    public static ButtonItem createSendGiftItem(final Context context, final ChatRoomMember chatRoomMember, final GiftDialog.OnGiftDialogBtnClickListener giftDialogBtnClickListener) {
        ButtonItem buttonItem = new ButtonItem("送礼物", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                GiftDialog dialog = new GiftDialog(context, AvRoomDataManager.get().mMicQueueMemberMap, chatRoomMember);
                if (giftDialogBtnClickListener != null) {
                    dialog.setGiftDialogBtnClickListener(giftDialogBtnClickListener);
                }
                dialog.show();

            }
        });
        return buttonItem;
    }

    public static ButtonItem createSendGiftItem(final Context context, String uid) {
        ButtonItem buttonItem = new ButtonItem("送礼物", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                showGiftDialog(context, uid);

            }
        });
        return buttonItem;
    }

    /**
     * 禁麦
     */
    public static ButtonItem createLockMicItem(final int position, ButtonItem.OnClickListener onClickListener) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.forbid_mic), onClickListener);
    }

    /**
     * 取消禁麦
     */
    public static ButtonItem createFreeMicItem(final int position, ButtonItem.OnClickListener onClickListener) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_forbid_mic), onClickListener);
    }

    public static ButtonItem createManagerListItem(final Context context) {
        ButtonItem buttonItem = new ButtonItem("管理员", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                RoomManagerListActivity.start(context);
            }
        });
        return buttonItem;
    }

    public static ButtonItem createBlackListItem(final Context context) {
        ButtonItem buttonItem = new ButtonItem("黑名单", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                RoomBlackListActivity.start(context);
            }
        });
        return buttonItem;
    }

    public static ButtonItem createPublicSwitch(int publicChatSwitch) {
        ButtonItem buttonItem = new ButtonItem(publicChatSwitch == 0 ? "关闭房间内聊天" : "开启房间内聊天", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);

                IAuthCore core = CoreManager.getCore(IAuthCore.class);
                if (core == null || iAuthCore == null)
                    return;
                String ticket = iAuthCore.getTicket();
                long currentUid = core.getCurrentUid();
                CoreManager.getCore(IAVRoomCore.class).changeRoomMsgFilter(AvRoomDataManager.get().isRoomOwner(), publicChatSwitch == 0 ? 1 : 0, ticket, currentUid);
            }
        });
        return buttonItem;
    }

    /**
     * 举报功能按钮
     *
     * @param title 房间内为举报房间  个人为举报
     * @param type  个人 和 房间举报
     */
    public static ButtonItem createReportItem(Context context, String title, int type) {
        if (reportModel == null) {
            reportModel = new ReportModel();
        }
        return new ButtonItem(title, () -> {
            if (context == null)
                return;
            List<ButtonItem> buttons = new ArrayList<>();
            ButtonItem button1 = new ButtonItem("政治敏感", () -> reportModel.reportCommit(type, 1, reportCallBack));
            ButtonItem button2 = new ButtonItem("色情低俗", () -> reportModel.reportCommit(type, 2, reportCallBack));
            ButtonItem button3 = new ButtonItem("广告骚扰", () -> reportModel.reportCommit(type, 3, reportCallBack));
            ButtonItem button4 = new ButtonItem("人身攻击", () -> reportModel.reportCommit(type, 4, reportCallBack));
            buttons.add(button1);
            buttons.add(button2);
            buttons.add(button3);
            buttons.add(button4);
            DialogManager dialogManager = null;
            if (context instanceof BaseMvpActivity) {
                dialogManager = ((BaseMvpActivity) context).getDialogManager();
            } else if (context instanceof BaseActivity) {
                dialogManager = ((BaseActivity) context).getDialogManager();
            }
            if (dialogManager != null) {
                dialogManager.showCommonPopupDialog(buttons, "取消");
            }
        });
    }

    public interface OnItemClick {
        void itemClick();
    }
}
