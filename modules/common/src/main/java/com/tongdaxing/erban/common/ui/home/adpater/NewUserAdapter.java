package com.tongdaxing.erban.common.ui.home.adpater;


import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.user.bean.NewUserBean;
import com.tongdaxing.xchat_core.utils.StarUtils;

import java.util.Date;

public class NewUserAdapter extends BaseQuickAdapter<NewUserBean, BaseViewHolder> {

    public NewUserAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, NewUserBean item) {
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), helper.getView(R.id.iv_new_user_avatar), R.drawable.ic_no_avatar);
        helper.setText(R.id.tv_new_user_name, item.getNick());
        TextView tvInfo = helper.getView(R.id.tv_new_user_info);
        tvInfo.setText(StarUtils.getConstellation(new Date(item.getBirth())));
        tvInfo.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext, item.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_female), null, null, null);
        helper.setOnClickListener(R.id.tv_new_user_call, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(item.getUid() + "");
                if (nimUserInfo != null) {
                    NimUIKit.startP2PSession(mContext, item.getUid() + "");
                } else {
                    NimUserInfoCache.getInstance().getUserInfoFromRemote(item.getUid() + "", new RequestCallbackWrapper<NimUserInfo>() {
                        @Override
                        public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                            if (i == 200) {
                                NimUIKit.startP2PSession(mContext, item.getUid() + "");
                            }
                        }
                    });
                }
            }
        });
    }
}
