package com.tongdaxing.erban.common.ui.me.withdraw;

/**
 * Created by Chen on 2019/7/19.
 */
public class WxBindEvent {
    public static class OnWvBindSuccess {
        private String mCode;
        private String mName;

        public OnWvBindSuccess(String code, String name) {
            this.mCode = code;
            this.mName = name;
        }

        public String getCode() {
            return mCode;
        }

        public String getName() {
            return mName;
        }
    }
}
