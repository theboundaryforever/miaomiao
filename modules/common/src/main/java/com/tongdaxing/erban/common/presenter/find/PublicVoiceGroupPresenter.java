package com.tongdaxing.erban.common.presenter.find;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.tongdaxing.erban.common.ui.find.view.IPublishVoiceGroupView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.find.bean.PublishVoiceGroupInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_core.utils.file_manager.FileBatchUploadManager;
import com.tongdaxing.xchat_core.utils.file_manager.UploadSucceedInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PublicVoiceGroupPresenter extends AbstractMvpPresenter<IPublishVoiceGroupView> {
    public String getJointUrlStr(List<UploadSucceedInfo> urlList) {
        StringBuilder stringBuilder = new StringBuilder();
        if (urlList != null) {
            for (int i = 0; i < urlList.size(); i++) {
                stringBuilder.append(urlList.get(i).remoteUrl);
                if (i != urlList.size() - 1) {
                    stringBuilder.append(",");
                }
            }
        }
        return stringBuilder.toString();
    }

    /**
     * 判断图片是横屏还是竖屏
     *
     * @return 1是竖屏，2横屏
     */
    public String getImgLayoutType(List<UploadSucceedInfo> urlList) {
        if (urlList != null && urlList.size() == 1) {//只有1张图片才计算横屏或竖屏
            try {
                int[] widthAndHeight = getImageWidthHeight(urlList.get(0).localUrl);
                if (widthAndHeight[0] <= widthAndHeight[1]) {
                    return "1";
                } else {
                    return "2";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "1";//默认返回竖屏
            }
        } else {
            return "";
        }
    }

    public int[] getImageWidthHeight(String path) throws Exception {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(path, options); // 此时返回的bitmap为null
        return new int[]{options.outWidth, options.outHeight};
    }

    /**
     * 发布动态
     *
     * @param content
     * @param urlList
     * @param imgType
     */
    public void publicVoiceGroupContent(String content, String urlList, String imgType) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("context", content);
        params.put("photoStr", urlList);
        params.put("layoutType", imgType);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getPublicVoiceGroup(), params, new OkHttpManager.MyCallBack<ServiceResult<PublishVoiceGroupInfo>>() {
            @Override
            public void onError(Exception e) {
                getMvpView().publishFailure(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<PublishVoiceGroupInfo> response) {
                if (response == null) {
                    getMvpView().publishFailure("数据异常!");
                    return;
                }

                if (response.getCode() == 2401) {
                    getMvpView().publishFailureSensitiveWord();
                } else if (response.getCode() == 2402 && response.getData() != null) {
                    getMvpView().publishFailureImgViolations(response.getData());
                } else if (response.getCode() == 200 && response.getData() != null) {
                    getMvpView().publishSucceed(response.getData());
                } else if (response.getCode() == 2501) {//短期内不能重复发送
                    String msg = response.getMessage();
                    getMvpView().repeatSendVoiceGroup(msg);
                } else {
                    onError(new Exception("数据异常!"));
                }
            }
        });
    }


    public ArrayList<String> convertPhotoListToStrList(ArrayList<UserPhoto> models) {
        ArrayList<String> list = new ArrayList<>();
        if (models != null && models.size() > 0) {
            for (int i = 0; i < models.size(); i++) {
                list.add(models.get(i).getPhotoUrl());
            }
        }
        return list;
    }

    public ArrayList<UserPhoto> convertStrListToPhotoList(ArrayList<String> list) {
        ArrayList<UserPhoto> userPhotoArrayList = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                UserPhoto userPhoto = new UserPhoto();
                userPhoto.setPhotoUrl(list.get(i));
                File file = new File(list.get(i));
                if (file.exists() && file.isFile()) {
                    FileBatchUploadManager.newInstance().addUploadQueue(list.get(i));
                }
                userPhotoArrayList.add(userPhoto);
            }
        }
        return userPhotoArrayList;
    }

    public void updateUrlListInfo(List<UploadSucceedInfo> urlList, UploadSucceedInfo uploadSucceedInfo) {
        if (urlList != null) {
            for (int i = 0; i < urlList.size(); i++) {
                if (urlList.get(i).localUrl.equals(uploadSucceedInfo.localUrl)) {
                    urlList.set(i, uploadSucceedInfo);
                }
            }
        }
    }

    public void addUrlList(List<UploadSucceedInfo> urlList, ArrayList<String> list) {
        if (urlList != null) {
            for (int i = 0; i < list.size(); i++) {
                UploadSucceedInfo uploadSucceedInfo = new UploadSucceedInfo();
                uploadSucceedInfo.localUrl = list.get(i);
                urlList.add(uploadSucceedInfo);
            }
        }
    }
}
