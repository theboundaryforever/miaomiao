package com.tongdaxing.erban.common.room.operation;

/**
 * Created by Chen on 2019/6/11.
 */
public interface IRoomBottomMoreView {
    void dismissView();
}
