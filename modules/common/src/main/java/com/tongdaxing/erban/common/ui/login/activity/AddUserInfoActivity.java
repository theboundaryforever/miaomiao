package com.tongdaxing.erban.common.ui.login.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.ui.common.permission.PermissionActivity;
import com.tongdaxing.erban.common.ui.me.user.activity.ModifyInfoActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.ThirdUserInfo;
import com.tongdaxing.xchat_core.common.QiNiuFileUploadProfile;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.linked.ILinkedCore;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.file.JXFileUtils;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author zhouxiangfeng
 * @date 2017/5/12
 */

public class AddUserInfoActivity extends TakePhotoActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    public static final String DEFAULT_AVATAR = QiNiuFileUploadProfile.accessUrl + "default_head_pic.png";

    private static final String CAMERA_PREFIX = "picture_";
    private static final String BMP_TEMP_NAME = "bmp_temp_name";
    private static final String TAG = "AddUserInfoActivityTAG";
    @BindView(R2.id.title_bar)
    TitleBar titleBar;
    @BindView(R2.id.civ_avatar)
    ImageView civAvatar;
    @BindView(R2.id.civ_avatar_right_arrow)
    ImageView civAvatarRightArrow;
    @BindView(R2.id.rl_change_avatar)
    RelativeLayout rlChangeAvatar;
    @BindView(R2.id.et_nick)
    EditText etNick;
    @BindView(R2.id.tv_birth)
    TextView tvBirth;
    @BindView(R2.id.rl_birth)
    RelativeLayout rlBirth;
    @BindView(R2.id.rb_man)
    RadioButton rbMan;
    @BindView(R2.id.rb_female)
    RadioButton rbFemale;
    @BindView(R2.id.ok_btn)
    TextView okBtn;
    @BindView(R2.id.user_add_root)
    LinearLayout rootView;

    private DatePickerDialog datePickerDialog;
    private String avatarUrl = DEFAULT_AVATAR;
    private String avatarUrlWX;

    private String mCameraCapturingName;
    private File cameraOutFile;
    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
        @Override
        public void superPermission() {
            mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
            cameraOutFile = JXFileUtils.getTempFile(AddUserInfoActivity.this, mCameraCapturingName);
            if (!cameraOutFile.getParentFile().exists()) {
                cameraOutFile.getParentFile().mkdirs();
            }
            Uri uri = Uri.fromFile(cameraOutFile);
            getTakePhoto().onEnableCompress(CompressConfig.ofDefaultConfig(), true);
            getTakePhoto().onPickFromCapture(uri);
        }
    };
    private File photoFile;
    private final String DEFAULT_BIRTH = "1997-01-01";
    //    http://www.daxiaomao.com/user/update?ticket=&uid=900001&birth=&nick=&signture=&userVoice=&avatar=&region=&userDesc=
    private int sexCode;
    //    private Date mSelectDate;
    private Calendar mCalendar;

    public static boolean isExitSDCard() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    @Override
    protected void onPause() {
        super.onPause();
        L.debug(TAG, "onPause");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        L.debug(TAG, "onCreate");
        setContentView(R.layout.activity_addinfo);
        ButterKnife.bind(this);
        //添加监听必须在设置内容操作之前不然不起作用
        etNick.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    okBtn.setEnabled(checkNextStep());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        initTitleBar("");
        setSwipeBackEnable(false);
        ImageLoadUtils.loadCircleImage(this, avatarUrl, civAvatar, R.drawable.log_in_default_avatar);
        addWXUserInfo();
        init();

    }

    @Override
    protected boolean needSteepStateBar() {
        return true;
    }

    private void addWXUserInfo() {
        mCalendar = Calendar.getInstance();
        mCalendar.clear();
        mCalendar.set(1997, 0, 1);
        ThirdUserInfo thirdUserInfo = CoreManager.getCore(IAuthCore.class).getThirdUserInfo();
        tvBirth.setText(DEFAULT_BIRTH);
        if (thirdUserInfo != null) {
            avatarUrlWX = thirdUserInfo.getUserIcon();
            if (!StringUtil.isEmpty(thirdUserInfo.getUserGender())) {
                if (thirdUserInfo.getUserGender().equals("m")) {
                    sexCode = 1;
                    rbMan.setChecked(true);
                } else {
                    sexCode = 2;
                    rbFemale.setChecked(true);
                }
            } else {
                sexCode = 1;
                rbMan.setChecked(true);
            }
            String nick = thirdUserInfo.getUserName();
            if (!StringUtil.isEmpty(nick) && nick.length() > 15) {
                etNick.setText(nick.substring(0, 15));
            } else {
                etNick.setText(nick);
            }
            ImageLoadUtils.loadCircleImage(this, avatarUrlWX, civAvatar, R.drawable.log_in_default_avatar);
//            ImageLoadUtils.loadSmallRoundBackground(this, avatarUrlWX, civAvatar);
        }
    }

    private void init() {
        datePickerDialog = DatePickerDialog.newInstance(this, 1997, 0, 1, true);//生日选项默认为1997-01-01
    }

    @OnClick({R2.id.rb_female, R2.id.rb_man, R2.id.rl_birth, R2.id.ok_btn, R2.id.rl_change_avatar})
    public void onClick(View v) {
        Date date = new Date();
        int i = v.getId();
        if (i == R.id.rb_female) {
            sexCode = 2;
//                okBtn.setEnabled(checkNextStep());

        } else if (i == R.id.rb_man) {
            sexCode = 1;
//                okBtn.setEnabled(checkNextStep());

        } else if (i == R.id.rl_birth) {
            if (isFinishing() || isDestroyed()) {
                return;
            }
            if (datePickerDialog.isAdded()) {
                if (datePickerDialog.isStateSaved()) {
                    datePickerDialog.dismissAllowingStateLoss();
                }
            } else {
                datePickerDialog.setVibrate(true);
                datePickerDialog.setYearRange(1945, date.getYear() + 1900);
                datePickerDialog.show(getSupportFragmentManager(), "DATEPICKER_TAG");
            }

        } else if (i == R.id.ok_btn) {
            if (isFinishing() || isDestroyed()) {
                return;
            }
            date.setYear(date.getYear() - 18);
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
            if (mCalendar.getTimeInMillis() - date.getTime() > 0) {
                Snackbar.make(rootView, "仅对年满18岁及以上用户开放！", Snackbar.LENGTH_SHORT).show();
                return;
            }
            String nick = etNick.getText().toString();
            String birth = tvBirth.getText().toString();
            if (nick.trim().isEmpty()) {
                Snackbar.make(rootView, "昵称不能为空！", Snackbar.LENGTH_SHORT).show();
                return;
            } else if (sexCode == 0) {
                Snackbar.make(rootView, "请选择性别", Snackbar.LENGTH_SHORT).show();
                return;
            } else if (birth.trim().isEmpty()) {
                Snackbar.make(rootView, "生日不能为空！", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if (photoFile != null) {
                getDialogManager().showProgressDialog(AddUserInfoActivity.this, "正在上传请稍后...");
                CoreManager.getCore(IFileCore.class).upload(photoFile);
                return;
            }

            //用户如果自己拍照作为头像就上传，如果为空就代表没拍照，直接拿微信头像上传
//                avatarUrl = avatarUrlWX;
            if (avatarUrlWX != null) {
                avatarUrl = avatarUrlWX;
            } else if (StringUtils.isEmpty(avatarUrl)) {
                Snackbar.make(rootView, "请上传头像！", Snackbar.LENGTH_SHORT).show();
                return;
            }
            commit();

        } else if (i == R.id.rl_change_avatar) {
            if (isFinishing() || isDestroyed()) {
                return;
            }
            ButtonItem upItem = new ButtonItem("拍照上传", new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    checkPermissionAndStartCamera();
                }
            });

            ButtonItem localItem = new ButtonItem("本地相册", new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    if (isFinishing() || isDestroyed()) {
                        return;
                    }
                    String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
                    File cameraOutFile = JXFileUtils.getTempFile(AddUserInfoActivity.this, mCameraCapturingName);
                    if (!cameraOutFile.getParentFile().exists()) {
                        cameraOutFile.getParentFile().mkdirs();
                    }
                    Uri uri = Uri.fromFile(cameraOutFile);
                    CompressConfig compressConfig = new CompressConfig.Builder().create();
                    getTakePhoto().onEnableCompress(compressConfig, true);
                    CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
                    getTakePhoto().onPickFromGalleryWithCrop(uri, options);
                }
            });
            List<ButtonItem> buttonItemList = new ArrayList<>();
            buttonItemList.add(upItem);
            buttonItemList.add(localItem);
            getDialogManager().showCommonPopupDialog(buttonItemList, "取消", false);

        } else {
        }
    }

    private void commit() {
        String nick = etNick.getText().toString();
        String birth = tvBirth.getText().toString();

        UserInfo userInfo = new UserInfo();
        userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        userInfo.setBirthStr(birth);
        userInfo.setNick(nick);
        userInfo.setAvatar(avatarUrl);
        userInfo.setGender(sexCode);
        getDialogManager().showProgressDialog(AddUserInfoActivity.this, "请稍后...");
        LinkedInfo linkedInfo = CoreManager.getCore(ILinkedCore.class).getLinkedInfo();

        String channel = "";
        String roomUid = "";
        String uid = "";
        if (linkedInfo != null) {
            channel = linkedInfo.getChannel();
            roomUid = linkedInfo.getRoomUid();
            uid = linkedInfo.getUid();
        }
//        toast("commit" + uid);
        CoreManager.getCore(IUserCore.class).requestCompleteUserInfo(userInfo, channel, uid, roomUid);
        UmengEventUtil.getInstance().onUserInfoCommit(this, channel, uid, roomUid);
    }

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String path = null;
        MLog.debug(this, "PictureTaskerAct.onActivityResult, resultCode = " + resultCode);
        if (resultCode != Activity.RESULT_OK) {
            MLog.info(this, "return is not ok,resultCode=%d", resultCode);
            return;
        }

        if (requestCode == Method.NICK) {
            if (null != data) {
                String nick = data.getStringExtra(ModifyInfoActivity.CONTENTNICK);
                etNick.setText(nick);
                return;
            }
        }
    }

//    BottomSelectDialog.OnSelectActionListener onSelectActionListener = new BottomSelectDialog.OnSelectActionListener() {
//        @Override
//        public void onSelect(String content) {
//            tvSex.setText(content);
//        }
//    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        datePickerDialog = null;
    }

    private String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        String monthstr;
        if ((month + 1) < 10) {
            monthstr = "0" + (month + 1);
        } else {
            monthstr = String.valueOf(month + 1);
        }
        String daystr;
        if (day < 10) {
            daystr = "0" + day;
        } else {
            daystr = String.valueOf(day);
        }
//        mSelectDate = new Date(year - 1900, month, day);
        mCalendar.set(year, month, day);
        tvBirth.setText(String.valueOf(year) + "-" + monthstr + "-" + daystr);
//        okBtn.setEnabled(checkNextStep());
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        if (isFinishing() || isDestroyed()) {
            return;
        }
        Log.d(TAG, "onUpload: 这是添加用户更改上传");
        avatarUrl = url;
        getDialogManager().dismissDialog();
        ImageLoadUtils.loadCircleImage(this, avatarUrl, civAvatar, R.drawable.log_in_default_avatar);
//        okBtn.setEnabled(checkNextStep());
        commit();
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        toast("上传失败");
        getDialogManager().dismissDialog();
//        okBtn.setEnabled(checkNextStep());
    }


    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoComplete(UserInfo userInfo) {
        getDialogManager().dismissDialog();
        CoreManager.getCore(IAuthCore.class).setThirdUserInfo(null);
        finish();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoCompleteFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void takeSuccess(TResult result) {
        photoFile = new File(result.getImage().getCompressPath());
        ImageLoadUtils.loadCircleImage(this, photoFile.getAbsolutePath(), civAvatar, R.drawable.log_in_default_avatar);
    }

    @Override
    public void takeFail(TResult result, String msg) {
//        okBtn.setEnabled(checkNextStep());
        toast(msg);
    }

    @Override
    protected void onLeftClickListener() {
        SingleToastUtil.showToast("请您及时填写基本资料，否则会影响部分功能使用");
        super.onLeftClickListener();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onLeftClickListener();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 检测下一步是否可以点击
     *
     * @return
     */
    public boolean checkNextStep() {
        if ((etNick.getText() != null && StringUtils.isNotEmpty(etNick.getText().toString()))
                && (tvBirth.getText() != null && StringUtils.isNotEmpty(tvBirth.getText().toString()))
                && sexCode != 0) {
            return true;
        }
        return false;
    }

    public static interface Method {
        public static final int NICK = 2;

    }
}
