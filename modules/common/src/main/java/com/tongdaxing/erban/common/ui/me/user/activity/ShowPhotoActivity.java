package com.tongdaxing.erban.common.ui.me.user.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.ui.me.user.adapter.PhotoAdapter;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.ArrayList;
import java.util.List;

public class ShowPhotoActivity extends BaseActivity {
    private ImageView mImageView;
    private TextView imgCount;
    private ViewPager viewPager;
    private UserPhoto userPhoto;
    private PhotoAdapter photoAdapter;
    private ShowPhotoActivity mActivity;
    private int position;
    private ArrayList<UserPhoto> photoUrls;

    public static void start(Context context, int position, List<UserPhoto> remoteUrlList) {
        Json photoDataJson = getPhotoDataJson(remoteUrlList);
        if (photoDataJson == null)
            return;
        Intent intent = new Intent(context, ShowPhotoActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("photoJsonData", photoDataJson.toString());
        context.startActivity(intent);
    }

    private static Json getPhotoDataJson(List<UserPhoto> photos) {
        if (photos == null)
            return null;
        Json json = new Json();
        for (int i = 0; i < photos.size(); i++) {
            UserPhoto userPhoto = photos.get(i);
            Json j = new Json();
            j.set("pid", userPhoto.getPid());
            j.set("photoUrl", userPhoto.getPhotoUrl());
            json.set(i + "", j.toString());
        }
        return json;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_photo);
        mActivity = this;
        initView();
        initData();
        setListener();
    }

    private void setListener() {
//        mImageView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return false;
//            }
//        });
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                viewPager.setCurrentItem(position);
//                imgCount.setText((position + 1) + "/" + photoAdapter.getCount());
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    imgCount.setText((position + 1) + "/" + (photoAdapter == null ? 0 : photoAdapter.getCount()));
                }
                imgCount.setText((position + 1) + "/" + (photoAdapter == null ? 0 : photoAdapter.getCount()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if (photoAdapter != null)
            photoAdapter.setmImageOnclickListener(new PhotoAdapter.imageOnclickListener() {
                @Override
                public void onClick() {
                    finish();
                }
            });
    }

    private void initData() {
        position = getIntent().getIntExtra("position", 1);
        System.out.println("position===" + position);
//        userPhoto = (UserPhoto) getIntent().getSerializableExtra("userPhoto");
//        photoUrls = (ArrayList<UserPhoto>) getIntent().getSerializableExtra("photoList");
//        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
//        RealmList<UserPhoto> userPhotos = userInfo.getPrivatePhoto();
        String photoJsonData = getIntent().getStringExtra("photoJsonData");
        if (!TextUtils.isEmpty(photoJsonData)) {
            photoUrls = json2PhotoList(Json.parse(photoJsonData));
        } else {
            try {
                photoUrls = (ArrayList<UserPhoto>) getIntent().getSerializableExtra("photoList");
            } catch (Exception e) {

            }

        }
        if (photoUrls != null) {
            photoAdapter = new PhotoAdapter(mActivity, photoUrls);
            viewPager.setAdapter(photoAdapter);
            viewPager.setCurrentItem(position);
            imgCount.setText((position + 1) + "/" + photoAdapter.getCount());
        }
    }

    private ArrayList<UserPhoto> json2PhotoList(Json json) {
        if (json == null || json.key_names().length == 0) {
            finish();
            return null;
        }
        ArrayList<UserPhoto> userPhotos = new ArrayList<>();
        String[] keys = json.key_names();

        for (int i = 0; i < keys.length; i++) {
            Json j = json.json_ok(keys[i]);
            UserPhoto userPhoto = new UserPhoto(j.num_l("pid"), j.str("photoUrl"));
            userPhotos.add(userPhoto);
        }

        return userPhotos;
    }

    private void initView() {
//        mImageView = (ImageView) findViewById(R.id.photoview);
        imgCount = (TextView) findViewById(R.id.tv_count);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
    }

    @Override
    protected boolean needSteepStateBar() {
        return true;
    }
}
