package com.tongdaxing.erban.common.room.talk.factory;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.juxiao.library_ui.widget.EmojiTextView;
import com.tongdaxing.erban.common.room.talk.adapter.AbsBaseViewHolder;
import com.tongdaxing.erban.common.ui.cpexclusive.CpExclusivePageActivity;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.WeekCpAttachment;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.talk.message.WeekCpMessage;
import com.tongdaxing.xchat_core.room.weekcp.tacit.ITacitTestCtrl;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTestEvent;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;


/**
 * Created by Administrator on 5/31 0031.
 */

public class WeekCpFactory implements AbsBaseViewHolder.ViewHolderFactory {
    @NonNull
    @Override
    public WeekCpViewHolder create(@NonNull ViewGroup parent) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_talk_week_cp_item, parent, false);
        return new WeekCpViewHolder(v);
    }

    public static class WeekCpViewHolder extends AbsBaseViewHolder<WeekCpMessage> {
        private EmojiTextView mTvContent;
        private ImageView mIvAskTacit;
        private LinearLayout mRoomLover;
        private TextView mLoverMerry;
        private LinearLayout mLlSoul;
        private TextView mTvQuestion;
        private TextView mTvTittle;

        public WeekCpViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvContent = itemView.findViewById(R.id.talk_iv_week_cp_content);
            mIvAskTacit = itemView.findViewById(R.id.talk_iv_ask_tacit);
            mRoomLover = itemView.findViewById(R.id.talk_lover_ll_week_cp);
            mLoverMerry = itemView.findViewById(R.id.talk_lover_is_week_cp_text);
            mLlSoul = itemView.findViewById(R.id.talk_lover_soul_layout);
            mTvQuestion = itemView.findViewById(R.id.talk_lover_soul_question_msg);
            mTvTittle = itemView.findViewById(R.id.talk_lover_soul_question_tittle);
        }

        @Override
        public void bind(WeekCpMessage message) {
            super.bind(message);
            WeekCpAttachment attachment = message.getWeekCpAttachment();
            mRoomLover.setVisibility(View.GONE);
            mIvAskTacit.setVisibility(View.GONE);
            mLlSoul.setVisibility(View.GONE);
            mTvContent.setVisibility(View.GONE);
            // second 1是普通消息，2是需要跳到专属页， 3是默契挑战请求消息 4是走心回答
            if (attachment.getSecond() == WeekCpAttachment.ROOM_WEEK_CP_MERRY) {
                String uid = attachment.getUid();
                String cpId = attachment.getCpId();
                String userId = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                if (userId.equals(uid) || userId.equals(cpId)) {
                    mRoomLover.setVisibility(View.VISIBLE);
                    String content = attachment.getContent();
                    mLoverMerry.setText(content);
                    mRoomLover.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CpExclusivePageActivity.start(mRoomLover.getContext(), CoreManager.getCore(IAuthCore.class).getCurrentUid(), true);
                        }
                    });
                }
            } else if (attachment.getSecond() == WeekCpAttachment.ROOM_WEEK_CP_TIP) {
                //图片：你和我够默契吗？接受考验吧
                final String uid = attachment.getUid();
                final String cpId = attachment.getCpId();
                final String userId = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                L.info("WeekCpFactory", "uid: %s, cpId: %s, userId: %s", uid, cpId, userId);
                if (userId.equals(uid) || userId.equals(cpId)) {
                    CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo().setCurrentState(ITacitTestCtrl.TACIT_TEST_UN_ASK);
                    mIvAskTacit.setVisibility(View.VISIBLE);
                    mIvAskTacit.setImageResource(R.drawable.room_lover_ask_tacit);
                    mIvAskTacit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            L.info("WeekCpFactory", "onClick uid: %s, cpId: %s, userId: %s", uid, cpId, userId);
                            CoreUtils.send(new TacitTestEvent.OnTacitTestAskClick(uid, attachment.getContent()));
                        }
                    });
                }
            } else if (attachment.getSecond() == WeekCpAttachment.ROOM_WEEK_CP_SINCERITY_ASK) {
                mLlSoul.setVisibility(View.VISIBLE);
                mLlSoul.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CoreUtils.send(new WeekCpEvent.OnWeekCpSoulSend());
                    }
                });
                String content = attachment.getContent();
                if (!TextUtils.isEmpty(content)) {
                    String tittle = content.substring(0, content.indexOf(","));
                    String question = content.substring(content.indexOf(",") + 1, content.length());

                    mTvQuestion.setText(question);
                    mTvTittle.setText(tittle);
                }
            } else {
                mTvContent.setVisibility(View.VISIBLE);
                mTvContent.setText(attachment.getContent());
            }
        }
    }
}
