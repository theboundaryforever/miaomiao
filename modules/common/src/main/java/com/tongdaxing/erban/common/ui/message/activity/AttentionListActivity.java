package com.tongdaxing.erban.common.ui.message.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.room.gift.GiftDialog;
import com.tongdaxing.erban.common.ui.me.user.activity.NewUserInfoActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.message.adapter.AttentionListAdapter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.AttentionCore;
import com.tongdaxing.xchat_core.user.AttentionCoreClient;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static com.tongdaxing.erban.common.R.id.swipe_refresh;

/**
 * 关注列表
 */
public class AttentionListActivity extends BaseActivity implements ChargeListener, GiftDialog.OnGiftDialogBtnClickListener {

    private RecyclerView mRecylcerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AttentionListActivity mActivity;
    private AttentionListAdapter adapter;
    private List<AttentionInfo> mAttentionInfoList = new ArrayList<>();

    private int mPage = Constants.PAGE_START;
    SwipeRefreshLayout.OnRefreshListener onRefreshLisetener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mPage = Constants.PAGE_START;
            onRefreshing();
        }
    };

    public static void start(Context context) {
        Intent intent = new Intent(context, AttentionListActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, boolean fromInViteCp) {
        Intent intent = new Intent(context, AttentionListActivity.class);
        intent.putExtra("fromInViteCp", fromInViteCp);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_attention);
        initTitleBar(getString(R.string.my_attention));
        initView();
        setListener();
        initData();
    }

    private void setListener() {
        swipeRefreshLayout.setOnRefreshListener(onRefreshLisetener);
        adapter = new AttentionListAdapter(mAttentionInfoList, getIntent().getBooleanExtra("fromInViteCp", false));
        adapter.setRylListener(new AttentionListAdapter.onClickListener() {
            @Override
            public void rylListeners(AttentionInfo attentionInfo) {
                if (90000000 == attentionInfo.getUid()) {
                    return;
                }
                NewUserInfoActivity.start(mActivity, attentionInfo.getUid());
            }

            @Override
            public void findHimListeners(AttentionInfo attentionInfo) {
                if (90000000 == attentionInfo.getUid()) {
                    return;
                }
                if (attentionInfo.getUserInRoom() != null)
                    AVRoomActivity.start(mActivity, attentionInfo.getUserInRoom().getUid(), new HerUserInfo(attentionInfo.uid, attentionInfo.nick));
            }

            @Override
            public void openCpGiftDialog(AttentionInfo attentionInfo) {
                GiftDialog giftDialog = new GiftDialog(AttentionListActivity.this, attentionInfo.getUid(), GiftDialog.POSITION_CP);
                giftDialog.setGiftDialogBtnClickListener(AttentionListActivity.this);
                giftDialog.show();
            }
        });
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mPage++;
                onRefreshing();
            }
        }, mRecylcerView);
    }

    private void initData() {
        mRecylcerView.setAdapter(adapter);
        showLoading();
        onRefreshing();
    }

    private void initView() {
        mActivity = this;
        mRecylcerView = (RecyclerView) findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(swipe_refresh);
        mRecylcerView.setLayoutManager(new LinearLayoutManager(mActivity));

    }

    private void onRefreshing() {
        CoreManager.getCore(AttentionCore.class)
                .getAttentionList(CoreManager.getCore(IAuthCore.class).getCurrentUid(), mPage, Constants.PAGE_SIZE);
    }

    @CoreEvent(coreClientClass = AttentionCoreClient.class)
    public void onGetAttentionList(List<AttentionInfo> attentionInfoList, int page) {
        mPage = page;
        if (!ListUtils.isListEmpty(attentionInfoList)) {
            if (mPage == Constants.PAGE_START) {
                hideStatus();
                swipeRefreshLayout.setRefreshing(false);
                mAttentionInfoList.clear();
                adapter.setNewData(attentionInfoList);
                if (attentionInfoList.size() < Constants.PAGE_SIZE) {
                    adapter.setEnableLoadMore(false);
                }
            } else {
                adapter.loadMoreComplete();
                adapter.addData(attentionInfoList);
            }
        } else {
            if (mPage == Constants.PAGE_START) {
                showNoData(getString(R.string.no_attention_text));
            } else {
                adapter.loadMoreEnd(true);
            }

        }
    }

    @CoreEvent(coreClientClass = AttentionCoreClient.class)
    public void onGetAttentionListFail(String error, int page) {
        mPage = page;
        if (mPage == Constants.PAGE_START) {
            swipeRefreshLayout.setRefreshing(false);
            showNetworkErr();
        } else {
            adapter.loadMoreFail();
            toast(error);
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long uid) {
        List<AttentionInfo> data = adapter.getData();
        if (!ListUtils.isListEmpty(data)) {
            ListIterator<AttentionInfo> iterator = data.listIterator();
            for (; iterator.hasNext(); ) {
                AttentionInfo attentionInfo = iterator.next();
                if (attentionInfo.isValid() && attentionInfo.getUid() == uid) {
                    iterator.remove();
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPage = Constants.PAGE_START;
                showLoading();
                onRefreshing();
            }
        };
    }

    @Override
    public void onRechargeBtnClick() {
        MyWalletNewActivity.start(getApplicationContext());
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number, int sendGiftType) {
        if (giftInfo != null) {
            List<Long> uids = new ArrayList<>();
            uids.add(uid);
            CoreManager.getCore(IGiftCore.class).doSendPersonalGiftToNIMNew(giftInfo.getGiftId(), uids,
                    number, null, this, sendGiftType);
        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number, int sendGiftType) {

    }

    @Override
    public void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid) {
        if (giftInfo != null) {
            com.tongdaxing.xchat_framework.util.util.LogUtil.d("AttentionListActivity CpGiftAction onSendCpGiftBtnClick");
            CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftInfo.getGiftId(), uid,
                    1, -1, this);
        }
    }

    @Override
    public void onNeedCharge() {
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    MyWalletNewActivity.start(getApplicationContext());
                    fragment.dismiss();
                }
            }
        }).show(AttentionListActivity.this.getSupportFragmentManager(), "charge");
    }
}


