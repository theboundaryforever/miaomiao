package com.tongdaxing.erban.common.ui.me.wallet.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.ui.login.CodeDownTimer;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.text.NumberFormat;

/*
    copy绑定支付宝页面，控件ID没有改变 详情请看布局
 */
public class BinderPhoneActivity extends BaseActivity {
    public static final String hasBand = "hasBand";
    public static final int modifyBand = 1;
    private EditText etAlipayAccount;
    private EditText etSmsCode;
    private Button btnGetCode;
    private Button btnBinder;
    private Button btnBinderRquest;
    private TextWatcher textWatcher;
    private boolean isModifyBand = false;//true-更换绑定手机 false-绑定手机号码
    private TextInputLayout mTilPhone;
    private TextInputLayout mTilSms;
    private int mHasBandType;
    private CodeDownTimer mTimer;
    private String mSubmitPhone;
    private UserInfo mUserInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHasBandType = getIntent().getIntExtra(hasBand, 0);
        if (mHasBandType == modifyBand) {
            isModifyBand = true;
        }
        setContentView(R.layout.activity_binder_phone);
        initTitleBar(isModifyBand ? "更换绑定手机号码" : "绑定手机号码");
        initView();
        initData();
        setListener();
        if (mHasBandType == modifyBand) {
            mUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (mUserInfo != null) {
                etAlipayAccount.setText(mUserInfo.getPhone());
                setMode(isModifyBand);
            }
        }

    }

    private void initView() {

        mTilPhone = (TextInputLayout) findViewById(R.id.til_phone);
        mTilSms = (TextInputLayout) findViewById(R.id.til_sms);
        etAlipayAccount = (EditText) findViewById(R.id.et_phone);
        etSmsCode = (EditText) findViewById(R.id.et_smscode);
        btnGetCode = (Button) findViewById(R.id.btn_get_code);
        btnBinder = (Button) findViewById(R.id.btn_binder);
        btnBinderRquest = (Button) findViewById(R.id.btn_binder_request);
    }

    private void setMode(boolean isModifyBand) {
        mTilPhone.setHint(isModifyBand ? "请输入已绑定的手机号码" : "请输入新绑定的手机号码");
        mTitleBar.setTitle(isModifyBand ? "更换绑定手机号码" : "绑定手机号码");
        btnBinder.setText(isModifyBand ? "确认替换" : "确认绑定");
    }

    private void initData() {

    }

    private void setListener() {
        //获取绑定手机验证码
        btnGetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = etAlipayAccount.getText().toString().trim();
                Number parse = null;
                try {
                    parse = NumberFormat.getIntegerInstance().parse(phoneNumber);
                } catch (Exception e) {
                    Log.e("bind phone", e.getMessage(), e);
                }
                if (parse == null || parse.intValue() == 0 || phoneNumber.length() != 11) {
                    Toast.makeText(BasicConfig.INSTANCE.getAppContext(), "请输入正确的手机号码", Toast.LENGTH_SHORT).show();
                    return;
                }
                mTimer = new CodeDownTimer(btnGetCode, 60000, 1000);
                mTimer.start();

                if (mHasBandType == modifyBand) {
                    if (mUserInfo == null) {
                        return;
                    }
                    if (isModifyBand) {
                        //确认久手机type=3
                        CoreManager.getCore(IAuthCore.class).getModifyPhoneSMSCode(phoneNumber, "3");

                    } else {
                        //确认久手机type=2
                        CoreManager.getCore(IAuthCore.class).getModifyPhoneSMSCode(phoneNumber, "2");
                    }
                } else {
                    CoreManager.getCore(IAuthCore.class).doGetSMSCode(phoneNumber);
                }


            }
        });

        //请求绑定手机号码
        btnBinderRquest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getDialogManager().showProgressDialog(BinderPhoneActivity.this, "正在验证请稍后...");
                if (mHasBandType == modifyBand) {
                    if (mUserInfo == null) {
                        return;
                    }
                    if (isModifyBand) {
                        CoreManager.getCore(IAuthCore.class).ModifyBinderPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                etAlipayAccount.getText().toString(), etSmsCode.getText().toString(), UriProvider.modifyBinderPhone());
                    } else {
                        mSubmitPhone = etAlipayAccount.getText().toString();
                        CoreManager.getCore(IAuthCore.class).ModifyBinderPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                mSubmitPhone, etSmsCode.getText().toString(), UriProvider.modifyBinderNewPhone());
                    }

                } else {
                    CoreManager.getCore(IAuthCore.class).BinderPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                            etAlipayAccount.getText().toString(), etSmsCode.getText().toString()
                    );
                }

            }
        });
        //输入框监听改变
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etAlipayAccount.getText() != null && etAlipayAccount.getText().length() > 0
                        && etSmsCode.getText() != null && etSmsCode.getText().length() > 0) {
                    btnBinder.setVisibility(View.GONE);
                    btnBinderRquest.setVisibility(View.VISIBLE);
                } else {
                    btnBinder.setVisibility(View.VISIBLE);
                    btnBinderRquest.setVisibility(View.GONE);
                }
            }
        };

        etAlipayAccount.addTextChangedListener(textWatcher);
        etSmsCode.addTextChangedListener(textWatcher);

    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onBinderPhone() {
        getDialogManager().dismissDialog();
        toast("绑定成功");
        finish();
    }


    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onBinderPhoneFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onMoidfyOnBiner() {

        getDialogManager().dismissDialog();
        if (isModifyBand) {

            isModifyBand = false;
            setMode(false);
            etSmsCode.setText("");
            etAlipayAccount.setText("");
            etAlipayAccount.setFocusable(true);
            if (mTimer != null) {
                mTimer.cancel();
                mTimer.onFinish();
            }
        } else {
            CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().setPhone(mSubmitPhone);
            toast("绑定成功");
            finish();
        }


    }


    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onMoidfyOnBinerFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onGetSMSCodeFail(String error) {
        toast(error);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }
}
