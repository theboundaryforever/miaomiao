package com.tongdaxing.erban.common.room.lover.chair;

import android.util.SparseArray;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;

import java.util.List;

/**
 * Created by Chen on 2019/4/26.
 */
public interface IRoomLoverChairView {

    void initChairInfo(List<RoomQueueInfo> chairList);

    void updateChairView(SparseArray<RoomQueueInfo> roomQueueInfo);

    void showOwnerClickDialog(RoomMicInfo mRoomMicInfo, int micPosition, long currentUid);

    SparseArray<ButtonItem> getAvatarButtonItemList(int micPosition, ChatRoomMember chatRoomMember, RoomInfo currentRoom);

    void showOwnerSelfInfo(ChatRoomMember chatRoomMember);

    void showMicAvatarClickDialog(List<ButtonItem> buttonItems);

    void showGiftDialog(ChatRoomMember chatRoomMember);

    void updateSpeakState(int position, boolean hasSound);

    void onReceiveFace(List<FaceReceiveInfo> faceReceiveInfos);
}
