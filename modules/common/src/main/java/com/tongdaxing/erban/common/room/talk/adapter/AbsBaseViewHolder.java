package com.tongdaxing.erban.common.room.talk.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Chen on 2019/4/18.
 */
public abstract class AbsBaseViewHolder<T> extends RecyclerView.ViewHolder {

    protected int position;
    private T mItem;

    public AbsBaseViewHolder(View itemView) {
        super(itemView);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @CallSuper
    public void bind(T item) {
        mItem = item;
    }

    public T getItem() {
        return mItem;
    }

    /**
     * ViewHolder接口工厂
     *
     * @param <F>
     */
    public interface ViewHolderFactory<F extends RecyclerView.ViewHolder> {

        @NonNull
        F create(@NonNull ViewGroup parent);
    }
}
