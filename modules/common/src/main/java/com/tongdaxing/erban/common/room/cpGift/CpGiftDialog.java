package com.tongdaxing.erban.common.room.cpGift;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.gcssloop.widget.PagerGridLayoutManager;
import com.gcssloop.widget.PagerGridSnapHelper;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.gift.adapter.GiftAdapter;
import com.tongdaxing.erban.common.room.gift.widget.PageIndicatorView;
import com.tongdaxing.erban.common.room.widget.dialog.CpRingDescriptionDialog;
import com.tongdaxing.erban.common.ui.widget.marqueeview.Utils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftTypeConstant;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.ICongratulationDialogCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.List;

/**
 * 申请CP
 * Created by zhangjian on 2019/3/26.
 * <p>
 * （如果每次都需要最新的数据，请每次new一个实例使用）
 */
@Deprecated
public class CpGiftDialog extends BottomSheetDialog implements View.OnClickListener, GiftAdapter.OnItemClickListener, PagerGridLayoutManager.PageListener {
    public static final int ROWS = 1;
    public static final int COLUMNS = 3;
    private static final String TAG = "giftdialog";
    private Context context;
    private RecyclerView gridView;
    private GiftAdapter adapter;
    private GiftInfo current;//当前礼物
    private List<GiftInfo> giftInfos;//礼物列表
    private PageIndicatorView giftIndicator;
    private OnCpGiftDialogBtnClickListener giftDialogBtnClickListener;
    private TextView goldText;//金币余额
    private long uid;//赠送人
    private boolean darkStyle;
    private ImageView ivQuestionHint;//戒指说明

    /**
     * 设置礼物框默认页面入口
     *
     * @param context
     */
    public CpGiftDialog(Context context, long uid) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;
        this.uid = uid;
    }

    public CpGiftDialog(Context context, long uid, boolean darkStyle) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;
        this.uid = uid;
        this.darkStyle = darkStyle;
    }

    public void setCpGiftDialogBtnClickListener(OnCpGiftDialogBtnClickListener giftDialogBtnClickListener) {
        this.giftDialogBtnClickListener = giftDialogBtnClickListener;
    }

    @CoreEvent(coreClientClass = ICongratulationDialogCoreClient.class)
    public void closeGiftDialogFromCongraDialog() {
        dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
        setCanceledOnTouchOutside(true);
        setContentView(R.layout.dialog_bottom_cp_gift);
        init(findViewById(R.id.ll_dialog_bottom_gift));
        FrameLayout bottomSheet = findViewById(android.support.design.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(false);
            //dialog_gift_height字段设置对话框高度
            BottomSheetBehavior.from(bottomSheet).setPeekHeight(
                    (int) context.getResources().getDimension(R.dimen.dialog_gift_height) +
                            (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0));
        }
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display d = windowManager.getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
    }

    private void init(View root) {
        ivQuestionHint = root.findViewById(R.id.iv_question_hint);
        ivQuestionHint.setOnClickListener(v -> {
            new CpRingDescriptionDialog().show(((FragmentActivity) context).getSupportFragmentManager(), "cp");
        });
        root.findViewById(R.id.btn_recharge).setOnClickListener(this);
        root.findViewById(R.id.btn_send).setOnClickListener(this);
        giftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_CP);
        gridView = root.findViewById(R.id.gridView);
        giftIndicator = root.findViewById(R.id.gift_layout_indicator);
        PagerGridSnapHelper pageSnapHelper = new PagerGridSnapHelper();
        pageSnapHelper.attachToRecyclerView(gridView);
        goldText = root.findViewById(R.id.text_gold);

        adapter = new GiftAdapter(getContext(), R.layout.list_item_cp_gift);
        if (darkStyle) {
            FrameLayout ll_dialog_bottom_gift = root.findViewById(R.id.ll_dialog_bottom_gift);
            TextView tv_cp = root.findViewById(R.id.tv_cp);
            TextView btn_recharge = root.findViewById(R.id.btn_recharge);
            ll_dialog_bottom_gift.setBackgroundResource(R.drawable.bg_cp_gift_dark_dialog);
            tv_cp.setTextColor(Color.WHITE);
            goldText.setTextColor(Color.WHITE);
            btn_recharge.setTextColor(Color.parseColor("#FFCB2D"));
            Drawable drawable = getContext().getResources().getDrawable(R.drawable.ic_right_yellow);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            btn_recharge.setCompoundDrawables(null, null, drawable, null);
            adapter = new GiftAdapter(getContext(), R.layout.list_item_gift);
        }

        gridView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

        setAdapterAndIndicator(giftInfos);

        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        CoreManager.getCore(IPayCore.class).getWalletInfo(uid);

        updateGold();
    }

    private void setAdapterAndIndicator(List<GiftInfo> giftInfos) {
        if (giftInfos != null) {
            adapter.setGiftInfoList(giftInfos);
            adapter.notifyDataSetChanged();
            PagerGridLayoutManager pagerGridLayoutManager = new PagerGridLayoutManager(ROWS, COLUMNS, PagerGridLayoutManager.HORIZONTAL);
            pagerGridLayoutManager.setPageListener(this);
            gridView.setLayoutManager(pagerGridLayoutManager);
            giftIndicator.initIndicator((int) Math.ceil((float) giftInfos.size() / (ROWS * COLUMNS)));
            if (giftInfos.size() > 0) {
                current = giftInfos.get(0);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_recharge) {
            if (giftDialogBtnClickListener != null) {
                giftDialogBtnClickListener.onRechargeBtnClick();
            }
            dismiss();

        } else if (i == R.id.btn_send) {
            if (adapter == null)
                return;
            current = adapter.getIndexGift();
            if (current == null) {
                SingleToastUtil.showToast("请选择您需要赠送的礼物哦！");
                return;
            }
            if (giftDialogBtnClickListener != null) {
                if (uid > 0) {
                    giftDialogBtnClickListener.onSendCpGiftBtnClick(current, uid);
                }
            }

        }
    }


    public void updateGold() {
        if (goldText != null) {
            WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
            if (walletInfo != null) {
                goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
            }
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        LogUtil.d("Cp onWalletInfoUpdate walletInfo.getGoldNum() = " + walletInfo.getGoldNum());
        if (walletInfo != null) {
            goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onGetWalletInfo(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void refreshFreeGift() {
        if (adapter == null)
            return;
        //礼物
        giftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(GiftTypeConstant.GIFT_TYPE_CP);
        adapter.setGiftInfoList(giftInfos);
        adapter.notifyDataSetChanged();
    }

    /**
     * CP礼物赠送成功
     */
    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendCpGiftSuccess(int giftId, long targetUid, int giftNum) {
        LogUtil.d("onSendCpGiftSuccess ");
        SingleToastUtil.showToast("申请请求已发送，请等待回应");
        dismiss();
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftPastDue() {
        SingleToastUtil.showToast("该礼物已过期");
    }

    //10409 失败啦，你已经拥有CP
    //10412 失败啦，对方已有CP
    //10410 失败啦，你有正在处理的CP请求
    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendGiftFail(String message) {
        LogUtil.d("onSendGiftFail" + message);
        SingleToastUtil.showToast(message);
        dismiss();
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendPersonalGiftFail(String message) {
        LogUtil.d("onSendPersonalGiftFail");
        SingleToastUtil.showToast(message);
        dismiss();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo tempInfo) {
        if (tempInfo != null && this.uid > 0) {
            this.uid = tempInfo.getUid();
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        if (adapter == null)
            return;
        adapter.setIndex(position);
        GiftInfo giftInfo = adapter.getIndexGift();
        if (giftInfo != null) {
            if (!TextUtils.isEmpty(giftInfo.getExt())) {
                SingleToastUtil.showToast(giftInfo.getExt());
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPageSizeChanged(int pageSize) {
        Log.d(TAG, "onPageSizeChanged() called with: pageSize = [" + pageSize + "]");
    }

    @Override
    public void onPageSelect(int pageIndex) {
        giftIndicator.setSelectedPage(pageIndex);
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnCpGiftDialogBtnClickListener {
        void onRechargeBtnClick();

        void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid);
    }
}
