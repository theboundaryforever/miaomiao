package com.tongdaxing.erban.common.room.function;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.mvp.MVPBaseFrameLayout;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.room.personal.RoomPersonalRoute;
import com.tongdaxing.erban.common.room.personal.profile.RoomProfileAction;
import com.tongdaxing.erban.common.room.widget.dialog.BigListDataDialog;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chen on 2019/6/6.
 */
public class RoomFunctionView extends MVPBaseFrameLayout<IRoomFunctionView, RoomFunctionPresenter> implements IRoomFunctionView {
    @BindView(R2.id.room_rank_first)
    ImageView mRoomRankFirst;
    @BindView(R2.id.room_rank_first_wear)
    ImageView mRoomRankFirstWear;
    @BindView(R2.id.room_rank_second)
    ImageView mRoomRankSecond;
    @BindView(R2.id.room_function_rl_rank)
    RelativeLayout mRoomFunctionLlRank;
    @BindView(R2.id.room_iv_topic)
    ImageView mRoomIvTopic;
    @BindView(R2.id.room_iv_music)
    ImageView mRoomIvMusic;

    private DialogManager mDialogManager;

    public RoomFunctionView(@NonNull Context context) {
        super(context);
    }

    public RoomFunctionView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RoomFunctionView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @NonNull
    @Override
    protected RoomFunctionPresenter createPresenter() {
        return new RoomFunctionPresenter();
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_function_view;
    }

    @Override
    protected void findView() {
        ButterKnife.bind(this);
    }

    @Override
    protected void setView() {
        // 设置默认值
        ImageLoadUtils.loadImage(getContext(), R.drawable.ic_no_avatar, mRoomRankFirst);
        ImageLoadUtils.loadImage(getContext(), R.drawable.ic_no_avatar, mRoomRankSecond);
    }

    @Override
    protected void setListener() {

    }

    @OnClick(R2.id.room_function_rl_rank)
    public void onRankClicked() {
        if (mDialogManager == null) {
            mDialogManager = new DialogManager(getContext());
        }
        Context context = getContext();
        if (context instanceof FragmentActivity) {
            BigListDataDialog bigListDataDialog = BigListDataDialog.newContributionListInstance(context);
            bigListDataDialog.setSelectOptionAction(new BigListDataDialog.SelectOptionAction() {
                @Override
                public void optionClick() {
                    mDialogManager.showProgressDialog(context, "请稍后");
                }

                @Override
                public void onDataResponse() {
                    //请求结束前退出可能会导致奔溃，直接捕获没关系
                    try {
                        mDialogManager.dismissDialog();
                    } catch (Exception e) {

                    }
                }
            });
            bigListDataDialog.show(((FragmentActivity) context).getSupportFragmentManager());
        }
    }

    @OnClick(R2.id.room_iv_topic)
    public void onTopicClicked() {
        DialogFragment dialogFragment = (DialogFragment) ARouter.getInstance().build(RoomPersonalRoute.ROOM_PERSONAL_DESC_PATH)
                .navigation(getContext());
        if (dialogFragment.isAdded()) {
            if (dialogFragment.isStateSaved()) {
                dialogFragment.dismissAllowingStateLoss();
            }
        } else {
            FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
            dialogFragment.show(fragmentManager, "PersonRoomDesDialogFragment");
        }
    }

    @OnClick(R2.id.room_iv_music)
    public void onMusicClicked() {
        CoreUtils.send(new RoomProfileAction.OnMusicClicked());
    }

    @Override
    public void updateMusicView() {
        if (mPresenter.isSelfOnMic()) {
            mRoomIvMusic.setVisibility(VISIBLE);
        } else {
            mRoomIvMusic.setVisibility(GONE);
        }
    }

    @Override
    public void updateRankView(List<RoomConsumeInfo> data) {
        if (data.size() >= 1) {
            RoomConsumeInfo consumeInfo = data.get(0);
            String avatar = consumeInfo.getAvatar();
            if (!TextUtils.isEmpty(avatar)) {
                ImageLoadUtils.loadCircleImage(getContext(), avatar, mRoomRankFirst, R.drawable.ic_no_avatar);
            } else {
                ImageLoadUtils.loadImage(getContext(), R.drawable.ic_no_avatar, mRoomRankFirst);
            }
        } else {
            ImageLoadUtils.loadImage(getContext(), R.drawable.ic_no_avatar, mRoomRankFirst);
        }
        if (data.size() >= 2) {
            RoomConsumeInfo consumeInfo = data.get(1);
            String avatar = consumeInfo.getAvatar();
            if (!TextUtils.isEmpty(avatar)) {
                ImageLoadUtils.loadCircleImage(getContext(), avatar, mRoomRankSecond, R.drawable.ic_no_avatar);
            } else {
                ImageLoadUtils.loadImage(getContext(), R.drawable.ic_no_avatar, mRoomRankSecond);
            }
        } else {
            ImageLoadUtils.loadImage(getContext(), R.drawable.ic_no_avatar, mRoomRankSecond);
        }
    }
}
