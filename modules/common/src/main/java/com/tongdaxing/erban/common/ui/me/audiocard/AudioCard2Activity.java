package com.tongdaxing.erban.common.ui.me.audiocard;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.presenter.audio.AudioCardPresenter;
import com.tongdaxing.erban.common.presenter.audio.IAudioCardView;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.ui.me.audiocard.view.ArcProgressView;
import com.tongdaxing.erban.common.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.home.AudioCardContent;
import com.tongdaxing.xchat_core.home.AudioCardInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 喵喵声鉴卡 录制声音页面
 * <p>
 * Created by zhangjian on 2019/3/15.
 */
@Route(path = CommonRoutePath.AUDIO_CARD_2_ACTIVITY_ROUTE_PATH)
@CreatePresenter(AudioCardPresenter.class)
public class AudioCard2Activity extends BaseMvpActivity<IAudioCardView, AudioCardPresenter> implements IAudioCardView, View.OnClickListener {

    public static final String AUDIO_FILE_URL = "AUDIO_FILE_URL";
    public static final String AUDIO_LENGTH = "AUDIO_LENGTH";
    public final int LONG_PRESS_MIN_TIME = 5000;//最小录制时间 3s
    public final int ANIM_MAX_TIME = 15000;//进度条动画最大时间 15s
    public final int RECORD_MAX_TIME = 30000;//最大录制时间 30s

    @BindView(R2.id.title_bar)
    TitleBar mTitleBar;
    @BindView(R2.id.tv_content)
    TextView tvContent;
    @BindView(R2.id.constra_next)
    ConstraintLayout constraNext;
    @BindView(R2.id.view_prgRoundArcView)
    ArcProgressView viewPrgRoundArcView;
    @BindView(R2.id.iv_voicecard_recording_stick)
    ImageView ivVoicecardRecordingStick;
    @BindView(R2.id.iv_press_to_record)
    ImageView ivPressToRecord;

    private boolean isUploadLogicExecutedByTouchEvent;//是否是由长按松开导致录音上传 true是 false是达到了最大录制时长RECORD_MAX_TIME导致录音上传
    private MyHandler myHandler;
    private long pressTime = -1;
    private ObjectAnimator stickPlayAnim;
    private ObjectAnimator stickStopAnim;
    private AudioRecorder audioRecorder;

    private int audioLength;//录制时长 秒
    private String audioFileUrl;
    private List<AudioCardContent> list;
    private List<AudioCardContent> randomList;
    private Random random;
    private boolean isLoadingData = false;//是否加载数据
    private IAudioRecordCallback audioRecordCallback = new IAudioRecordCallback() {
        @Override
        public void onRecordReady() {

        }

        @Override
        public void onRecordStart(File audioFile, RecordType recordType) {
        }

        @Override
        public void onRecordSuccess(File file, long audioLength, RecordType recordType) {
            if (audioRecorder != null) {//这里做释放后为null，加判断就好
                audioFileUrl = file.getAbsolutePath();
                if (!TextUtils.isEmpty(audioFileUrl)) {
                    getDialogManager().showProgressDialog(AudioCard2Activity.this, getString(R.string.please_wait));
                    CoreManager.getCore(IFileCore.class).upload(new File(audioFileUrl));
                } else {
                    toast(R.string.record_fail);
                }
            }
        }

        @Override
        public void onRecordFail() {
            if (audioRecorder != null) {//这里做释放后为null，加判断就好
                setupAudioInit();
                toast(R.string.record_fail);
            }
        }

        @Override
        public void onRecordCancel() {
        }

        @Override
        public void onRecordReachedMaxTime(int maxTime) {

        }
    };
    private int current = 1;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_card2);
        ButterKnife.bind(this);
        initTitleBar(getString(R.string.audio_card_title));
        tvContent = (TextView) findViewById(R.id.tv_content);
        constraNext.setOnClickListener(this);

        //调整录音杆的旋转中心点
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        ivVoicecardRecordingStick.measure(w, h);
        int width = ivVoicecardRecordingStick.getMeasuredWidth();
        ivVoicecardRecordingStick.setPivotX(width - 50);
        ivVoicecardRecordingStick.setPivotY(0);

        viewPrgRoundArcView.setMaxProgress(ANIM_MAX_TIME);
        myHandler = new MyHandler();
        ivPressToRecord.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
                    SingleToastUtil.showToast("请您先退出当前房间，再进行声音录制");
                    return true;
                }
                if (pressTime == -1) {
                    LogUtil.d("ProgressRoundArcView", "sendEmptyMessageDelayed");
                    isUploadLogicExecutedByTouchEvent = false;
                    pressTime = System.currentTimeMillis();
                    myHandler.sendEmptyMessageDelayed(200, RECORD_MAX_TIME);
                }
                stickPlayAnim.start();
                viewPrgRoundArcView.startAnim();
                audioRecorder.startRecord();//开始录制
                return true;
            }
        });
        ivPressToRecord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (isOutterUp(event, v)) {
                        event.setAction(MotionEvent.ACTION_CANCEL);
                        return onTouch(v, event);
                    }
                    long currentTimeMillis = System.currentTimeMillis();
                    if (viewPrgRoundArcView.isProgressDrawing()) {
                        viewPrgRoundArcView.stopAnim();
                        if (currentTimeMillis - pressTime < LONG_PRESS_MIN_TIME) {
                            //小于5秒 需要重新录制
                            ivPressToRecord.setPressed(false);
                            setupStopRecord();
                            audioRecorder.completeRecord(true);//false 正常结束录制 true 取消录制
                            toast(R.string.audio_length_tip);
                            return true;
                        }
                    }
                    if (currentTimeMillis - pressTime < RECORD_MAX_TIME) {
                        ivPressToRecord.setPressed(false);
                        audioLength = (int) ((currentTimeMillis - pressTime) / 1000);
                        isUploadLogicExecutedByTouchEvent = true;
                        upload();
                        return true;
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    ivPressToRecord.setPressed(false);
                    setupStopRecord();
                    viewPrgRoundArcView.backToInitState();
                    audioRecorder.completeRecord(true);
                    toast(R.string.audio_cancel);
                    return true;
                }
                return false;
            }
        });

        stickPlayAnim = ObjectAnimator.ofFloat(ivVoicecardRecordingStick, "rotation", 0, -55);
        stickPlayAnim.setDuration(300);
        stickStopAnim = ObjectAnimator.ofFloat(ivVoicecardRecordingStick, "rotation", -55, 0);
        stickStopAnim.setDuration(300);

        audioRecorder = AudioPlayAndRecordManager.getInstance().getAudioRecorder(getApplicationContext(), 0, audioRecordCallback);

        Intent intent = getIntent();
        audioFileUrl = intent.getStringExtra(AUDIO_FILE_URL);
        audioLength = intent.getIntExtra(AUDIO_LENGTH, 0);
        if (!TextUtils.isEmpty(audioFileUrl)) {
        } else {
            setupAudioInit();
        }

        loadAudioCardSignature(Constants.PAGE_START);
    }

    private void setupStopRecord() {
        stickStopAnim.start();
        pressTime = -1;
        myHandler.removeCallbacksAndMessages(null);//清空所有消息，取消计时
    }

    /**
     * 检测ACTION_CANCEL动作 部分机型不会传递ACTION_CANCEL动作
     *
     * @param event
     * @param v
     * @return
     */
    private boolean isOutterUp(MotionEvent event, View v) {
        float touchX = event.getX();
        float touchY = event.getY();
        float maxX = v.getWidth();
        float maxY = v.getHeight();

        return touchX < 0 || touchX > maxX || touchY < 0 || touchY > maxY;
    }

    private void loadAudioCardSignature(int page) {
        if (isLoadingData) {
            return;
        }
        isLoadingData = true;
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        params.put("pageNum", String.valueOf(page));
        params.put("pageSize", "20");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getAudioCardSignature(), params, new OkHttpManager.MyCallBack<ServiceResult<List<AudioCardContent>>>() {
            @Override
            public void onError(Exception e) {
                isLoadingData = false;
            }

            @Override
            public void onResponse(ServiceResult<List<AudioCardContent>> response) {
                isLoadingData = false;
                if (response.isSuccess()) {
                    if (list == null) {
                        list = new ArrayList<>();
                    } else {
                        list.clear();
                    }
                    list = response.getData();
                    if (randomList == null) {
                        randomList = new ArrayList<>();
                    } else {
                        randomList.clear();
                    }
                    randomList.addAll(list);
                    if (list.size() > 0) {
                        current = page;
                        switchContent();
                    } else {
                        current = 1;
                        loadAudioCardSignature(current);
                    }
                }
            }
        });
    }

    /**
     * 随机一个内容
     */
    private AudioCardContent randomContent() {
        if (random == null) {
            random = new Random();
        }
        int randomInt = random.nextInt(randomList.size());
        AudioCardContent randomContent = randomList.get(randomInt);
        randomList.remove(randomInt);
        return randomContent;
    }

    private void switchContent() {
        if (list != null && !ListUtils.isListEmpty(randomList)) {//还没随机完
            AudioCardContent content = randomContent();
            tvContent.setText(content.getContent());
        } else {
            loadAudioCardSignature(++current);
        }
    }

    /**
     * 设置为声卡初始状态
     */
    private void setupAudioInit() {
        audioFileUrl = null;
        audioLength = 0;
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        getMvpPresenter().getAudioCard(url, audioLength);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        getDialogManager().dismissDialog();
        toast(R.string.audio_upload_fail);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        getDialogManager().dismissDialog();
        Intent intent = new Intent();
        intent.putExtra(AUDIO_FILE_URL, audioFileUrl);
        intent.putExtra(AUDIO_LENGTH, audioLength);
        setResult(RESULT_OK, intent);
        toast(R.string.audio_card_upload_success);
        CommonWebViewActivity.start(this, UriProvider.getAudioIdentifyCard(), getString(R.string.audio_card_clear));
        finish();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        toast(error);
        getDialogManager().dismissDialog();
    }

    @Override
    protected void onDestroy() {
        myHandler.removeCallbacksAndMessages(null);
//        if (audioRecorder.isRecording()) {
//            audioRecorder.completeRecord(true);
//        }
        audioRecorder.destroyAudioRecorder();
//        audioRecorder = null;
//        audioRecordCallback = null;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (audioRecorder.isRecording()) {
            audioRecorder.completeRecord(true);
        }
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.constra_next) {
            switchContent();

        }
    }

    @Override
    public void getAudioIdentifyCard(AudioCardInfo audioCardInfo, String url, long audioDuration) {
        audioFileUrl = url;
        UserInfo user = new UserInfo();
        user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        user.setUserVoice(audioFileUrl);
        user.setVoiceDura(audioLength);
        CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user, false);
    }

    @Override
    public void closeProgressDialog() {
        getDialogManager().dismissDialog();
    }

    /**
     * 1. 录制时间超过30秒
     * 2. [5,30)秒区间内长按松手
     */
    private void upload() {
        setupStopRecord();
        audioRecorder.completeRecord(false);//false 正常结束录制 true 取消录制
    }

    private class MyHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 200) {
                if (!isUploadLogicExecutedByTouchEvent) {
                    // 达到最大录制时长 自动停止录制
                    ivPressToRecord.setPressed(false);
                    LogUtil.d("ProgressRoundArcView", "handleMessage upload");
                    audioLength = RECORD_MAX_TIME / 1000;
                    upload();
                }
            }
        }
    }
}
