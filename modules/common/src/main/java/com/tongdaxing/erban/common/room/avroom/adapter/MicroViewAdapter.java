package com.tongdaxing.erban.common.room.avroom.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.widget.WaveView;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tcloud.core.log.L;
import com.tcloud.core.util.DensityUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * @author xiaoyu
 * @date 2017/12/18
 */

public class MicroViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_INVALID = -2;
    private OnMicroItemClickListener onMicroItemClickListener;
    private Context context;
    private int mTagHeight;
    private String avatarPicture = "";
    private View attentionAddBtn;
    private boolean isAttentionAddBtnEnable;
    private boolean showCharm = false;
    private String TAG = "MicroViewAdapter";

    public MicroViewAdapter(Context context) {
        this.context = context;
        mTagHeight = context.getResources().getDimensionPixelOffset(R.dimen.tag_height);
    }

    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        this.onMicroItemClickListener = onMicroItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_micro, parent, false);
        return new NormalMicroViewHolder(item);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(position);
        L.debug(TAG, "onBindViewHolder position: %d", position);
        if (roomQueueInfo == null) return;
        NormalMicroViewHolder holder = (NormalMicroViewHolder) viewHolder;
        holder.bind(roomQueueInfo, position);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_NORMAL;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setAttentionAddBtnEnable(boolean enable) {
        isAttentionAddBtnEnable = enable;
        if (attentionAddBtn != null) {
            attentionAddBtn.setVisibility(enable ? View.VISIBLE : View.GONE);
        }
    }

    public void setShowCharm(boolean showCharm) {
        this.showCharm = showCharm;
    }

    public interface OnMicroItemClickListener {
        void onAvatarBtnClick(int position);

        void onUpMicBtnClick(int position, ChatRoomMember chatRoomMember);

        void onLockBtnClick(int position);

        void onRoomSettingsClick();

        void onContributeListClick();

        void onAvatarSendMsgClick(int position);

        void onAttentionAddBtnClick(View attentionBtn);
    }

    public class NormalMicroViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView tvNick;
        //        TextView tvPosition;
        ImageView ivUpImage;
        ImageView ivLockImage;
        ImageView ivMuteImage;
        TextView ivGender;
        CircleImageView ivAvatar;
        FrameLayout rlMicLayout;
        WaveView waveView;
        View avatarBg;
        ImageView ivHeadwear;
        TextView tvGlamour;
        LinearLayout llGlamour;
        ImageView ivRoomCpLevel;
        RoomQueueInfo info;
        int mChairPosition = TYPE_INVALID;

        NormalMicroViewHolder(View itemView) {
            super(itemView);
            ivRoomCpLevel = itemView.findViewById(R.id.iv_room_cp_level);
            waveView = itemView.findViewById(R.id.waveview);
            rlMicLayout = itemView.findViewById(R.id.micro_layout);
            ivGender = itemView.findViewById(R.id.iv_mic_gender);
//            tvPosition = itemView.findViewById(R.id.tv_position);
            ivUpImage = itemView.findViewById(R.id.up_image);
            ivLockImage = itemView.findViewById(R.id.lock_image);
            ivMuteImage = itemView.findViewById(R.id.mute_image);
            ivAvatar = itemView.findViewById(R.id.avatar);
            tvNick = itemView.findViewById(R.id.nick);
            avatarBg = itemView.findViewById(R.id.avatar_bg);
            ivHeadwear = itemView.findViewById(R.id.iv_headwear);
            tvGlamour = itemView.findViewById(R.id.tv_glamour);
            llGlamour = itemView.findViewById(R.id.ll_mic_glamour);

            ivUpImage.setOnClickListener(this);
            ivLockImage.setOnClickListener(this);
            ivAvatar.setOnClickListener(this);
            ivAvatar.setOnLongClickListener(this);
            initWaveView();
        }

        private void initWaveView() {
            waveView.setInitialRadius(DensityUtil.dip2px(waveView.getContext(), 28.0f));
            waveView.setMaxRadius(DensityUtil.dip2px(waveView.getContext(), 40.0f));
            waveView.setInitialAlpha(0.5f);
            waveView.setSpeed(800);
            waveView.setColor(waveView.getResources().getColor(R.color.white));
        }

        public void clear(int position) {
            info = null;
            mChairPosition = TYPE_INVALID;
            rlMicLayout.setBackground(null);
            rlMicLayout.clearAnimation();
            waveView.stop();
            ivUpImage.setVisibility(View.VISIBLE);
            ivLockImage.setVisibility(View.GONE);
            ivRoomCpLevel.setVisibility(View.GONE);
            ivMuteImage.setVisibility(View.GONE);
            ivAvatar.setVisibility(View.GONE);
            avatarBg.setVisibility(View.GONE);
            ivGender.setVisibility(View.GONE);

            resetTvNick(position);
        }

        private void resetTvNick(int position) {
            String positionText = String.format(tvNick.getContext().getResources().getString(R.string.room_chair_item_position), position + 1);
            tvNick.setText(positionText);
        }

        /**
         * 设置麦位新年背景
         */
        private void setMicNewYearBg(int position) {
            ivUpImage.setImageResource(R.drawable.icon_room_up_micro);
        }

        void bind(RoomQueueInfo info, int position) {
            this.info = info;
            this.mChairPosition = position;
            L.debug(TAG, "bind mChairPosition: %d", position);
            RoomMicInfo roomMicInfo = info.mRoomMicInfo;
            ChatRoomMember chatRoomMember = info.mChatRoomMember;
            // 清除动画
            waveView.stop();
            rlMicLayout.setBackground(null);
            rlMicLayout.clearAnimation();


            setMicNewYearBg(position);
            if (info.isShowCpIcon) {
                ivRoomCpLevel.setVisibility(View.VISIBLE);
                if (info.cpGiftLevel == 2) {
                    ivRoomCpLevel.setImageResource(R.drawable.ic_room_cp_level_2);
                } else if (info.cpGiftLevel == 3) {
                    ivRoomCpLevel.setImageResource(R.drawable.ic_room_cp_level_3);
                } else {//只要isShowCpIcon为true就要显示cp标识，一周cp的cpGiftLevel为0，所以这里其他情况都显示1级标识
                    ivRoomCpLevel.setImageResource(R.drawable.ic_room_cp_level_1);
                }
            } else {
                ivRoomCpLevel.setVisibility(View.GONE);
            }
            if (roomMicInfo == null) {
                ivUpImage.setVisibility(View.VISIBLE);
                ivLockImage.setVisibility(View.GONE);
                ivMuteImage.setVisibility(View.GONE);
                ivAvatar.setVisibility(View.GONE);
                avatarBg.setVisibility(View.GONE);
                resetTvNick(position);
                ivGender.setVisibility(View.GONE);
                ivRoomCpLevel.setVisibility(View.GONE);
//                tvPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
                return;
            }

            //显示，先展示人，无视麦的锁
            if (chatRoomMember != null) {
                //--------魅力值-----------
                if (llGlamour != null) {//主席位没有魅力值
                    if (showCharm) {
                        int charm = 0;
                        JSONObject roomCharmList = IMNetEaseManager.get().roomCharmList;
                        if (roomCharmList != null) {
                            try {
                                if (roomCharmList.has(chatRoomMember.getAccount())) {
                                    charm = roomCharmList.getInt(chatRoomMember.getAccount());
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        llGlamour.setVisibility(View.VISIBLE);
                        tvGlamour.setText(String.valueOf(charm));
                    } else {
                        llGlamour.setVisibility(View.INVISIBLE);
                    }
                }

                ivLockImage.setVisibility(View.GONE);
                ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);

                if (!TextUtils.isEmpty(chatRoomMember.getAccount()) && JavaUtil.str2long(chatRoomMember.getAccount()) > 0) {
                    ivUpImage.setVisibility(View.GONE);
                    ivAvatar.setVisibility(View.VISIBLE);
                    avatarBg.setVisibility(View.VISIBLE);
                    tvNick.setVisibility(View.VISIBLE);
                    ivGender.setVisibility(View.VISIBLE);
                    tvNick.setText(chatRoomMember.getNick());
                    ivGender.setBackgroundResource(info.gender == 1 ? R.drawable.ic_room_male : R.drawable.ic_room_female);
                    ivGender.setText("" + (position + 1));
                    ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), chatRoomMember.getAvatar(), ivAvatar);

                    //--------头饰-----------
                    Map<String, Object> extension = chatRoomMember.getExtension();
                    String headwearUrl = "";
                    if (extension != null) {
                        try {
                            headwearUrl = (String) extension.get("headwearUrl");
                        } catch (Exception e) {
                        }
                    }
                    if (!TextUtils.isEmpty(headwearUrl)) {
                        ImageLoadUtils.loadImage(context, headwearUrl, ivHeadwear);
                        ivHeadwear.setVisibility(View.VISIBLE);
                    } else {
                        ivHeadwear.setVisibility(View.GONE);
                    }
                    //--------头饰-----------

                    GlideApp.with(context)
                            .load(chatRoomMember.getAvatar())
                            .placeholder(R.drawable.ic_no_avatar)
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object o,
                                                            Target<Drawable> target, boolean b) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable drawable, Object o,
                                                               Target<Drawable> target,
                                                               DataSource dataSource, boolean b) {
                                    ivAvatar.setImageDrawable(drawable);
                                    return true;
                                }
                            })
                            .into(ivAvatar);
                } else {
                    ivUpImage.setVisibility(View.VISIBLE);
                    ivAvatar.setVisibility(View.GONE);
                    avatarBg.setVisibility(View.GONE);
                    resetTvNick(position);
                    ivGender.setVisibility(View.GONE);
//                    tvPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
                }
            } else {
                if (llGlamour != null) {
                    llGlamour.setVisibility(View.INVISIBLE);
                }
                //锁麦
                boolean micLock = roomMicInfo.isMicLock();
                boolean micMute = roomMicInfo.isMicMute();
                L.debug(TAG, "micLock: %b, micMute: %b, mChairPosition: %d", micLock, micMute, position);
                if (micLock) {
                    ivUpImage.setVisibility(View.GONE);
                    ivMuteImage.setVisibility(micMute ? View.VISIBLE : View.GONE);
                    ivLockImage.setVisibility(View.VISIBLE);
                    ivAvatar.setVisibility(View.GONE);
                    avatarBg.setVisibility(View.GONE);
                } else {
                    ivMuteImage.setVisibility(micMute ? View.VISIBLE : View.GONE);
                    ivUpImage.setVisibility(View.VISIBLE);
                    ivAvatar.setVisibility(View.GONE);
                    avatarBg.setVisibility(View.GONE);
                    ivLockImage.setVisibility(View.GONE);
                }
                resetTvNick(position);
                ivGender.setVisibility(View.GONE);
                ivRoomCpLevel.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View v) {
            L.debug(TAG, "onClick mChairPosition: %d", mChairPosition);
            if (info == null || mChairPosition == TYPE_INVALID || onMicroItemClickListener == null)
                return;
            if (v.getId() == R.id.up_image || v.getId() == R.id.lock_image) {
                if (mChairPosition == -1) return;
                onMicroItemClickListener.onUpMicBtnClick(mChairPosition, info.mChatRoomMember);
            } else if (v.getId() == R.id.lock_image) {
                if (mChairPosition == -1) return;
                onMicroItemClickListener.onLockBtnClick(mChairPosition);
            } else if (v.getId() == R.id.avatar) {
                onMicroItemClickListener.onAvatarBtnClick(mChairPosition);
            } else if (v.getId() == R.id.tv_room_desc || v.getId() == R.id.iv_room_type || v.getId() == R.id.tv_custom_label) {
                onMicroItemClickListener.onRoomSettingsClick();
            } else if (v.getId() == R.id.contribute_list) {
                onMicroItemClickListener.onContributeListClick();
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (onMicroItemClickListener != null) {
                onMicroItemClickListener.onAvatarSendMsgClick(mChairPosition);
            }
            return true;
        }
    }
}
