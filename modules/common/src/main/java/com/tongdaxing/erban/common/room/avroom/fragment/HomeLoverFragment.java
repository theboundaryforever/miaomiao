package com.tongdaxing.erban.common.room.avroom.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.adapter.BaseAdapterHelper;
import com.erban.ui.adapter.ListAdapter;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.room.avroom.activity.RoomSettingActivity;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.room.avroom.widget.GiftV2View;
import com.tongdaxing.erban.common.room.widget.dialog.CpRingDescriptionDialog;
import com.tongdaxing.erban.common.room.widget.dialog.ListDataDialog;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.makefriends.FindFriendsBroadCastRoute;
import com.tongdaxing.erban.common.ui.widget.dialog.shareDialog.NewShareDialog;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.HomeLoverUserListPresenter;
import com.tongdaxing.xchat_core.room.weekcp.RoomLoverEvent;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DESUtils;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.framework.Platform;
import io.reactivex.functions.Consumer;

import static com.tongdaxing.xchat_framework.util.util.DESUtils.giftCarSecret;

/**
 * Created by Chen on 2019/4/28.
 */
public class HomeLoverFragment extends AbsRoomFragment
        implements NewShareDialog.OnShareDialogItemClick, HomeLoverRoomFragment.UserComeAction {
    @BindView(R2.id.fm_content)
    FrameLayout fmContent;
    @BindView(R2.id.room_lover_online_gridview)
    GridView mRoomLoverOnlineGridview;
    @BindView(R2.id.room_lover_online_number)
    TextView roomLoverOnlineNumber;
    @BindView(R2.id.room_more)
    ImageView roomMore;
    @BindView(R2.id.iv_help)
    ImageView ivHelp;
    @BindView(R2.id.iv_share)
    ImageView ivShare;
    @BindView(R2.id.gift_view)
    GiftV2View giftView;
    boolean isFirstLoad = true;
    private String TAG = "HomePersonalFragment";
    private ListAdapter<OnlineChatMember> mImageAdapter;
    private HomeLoverRoomFragment roomFragment;
    private List<OnlineChatMember> mPlayerImages;
    private HomeLoverUserListPresenter homeLoverUserListPresenter;

    public static HomeLoverFragment newInstance(long roomUid) {
        Log.e("newInstance", "HomePersonalFragment - newInstance");
        HomeLoverFragment homeLoverFragment = new HomeLoverFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.ROOM_UID, roomUid);
        homeLoverFragment.setArguments(bundle);
        return homeLoverFragment;
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // clear views
        Log.e("onNewIntent", "HomePersonalFragment - onNewIntent");
        if (roomFragment != null) {
            roomFragment.onNewIntent(intent);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_chatroom_lover_main;
    }

    @Override
    public void onFindViews() {
        ButterKnife.bind(this, mView);
    }

    @Override
    public void onSetListener() {
        setAdapter();
        registerRoomEvent();
    }

    @Override
    public void initiate() {
        CoreUtils.register(this);
        roomFragment = new HomeLoverRoomFragment();
        roomFragment.setUserComeAction(this);
        getChildFragmentManager().beginTransaction().replace(R.id.fm_content, roomFragment).commitAllowingStateLoss();
        updateGiftInfo();
        homeLoverUserListPresenter = new HomeLoverUserListPresenter();
        loadData();
    }

    private void setAdapter() {
        if (mImageAdapter == null) {
            mPlayerImages = new ArrayList<>();
            mImageAdapter = new ListAdapter<OnlineChatMember>(getContext(), R.layout.room_lover_profile_onlime_item, mPlayerImages) {
                @Override
                public void onUpdate(BaseAdapterHelper helper, OnlineChatMember item, int position) {
                    CircleImageView view = helper.getView(R.id.room_lover_online_photo);

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (item.chatRoomMember != null) {
//                                new UserInfoDialog(getContext(), Long.valueOf(item.chatRoomMember.getAccount()), true, roomFragment).show();
                                if (getActivity() != null) {
                                    ListDataDialog.newOnlineUserListInstance(getActivity()).show(getChildFragmentManager());
                                }
                            }
                        }
                    });

                    int defaultImageId = R.drawable.default_cover;
                    if (item.chatRoomMember != null) {
                        String url = item.chatRoomMember.getAvatar();
                        if (TextUtils.isEmpty(url)) {
                            ImageLoadUtils.loadCircleImage(getContext(), defaultImageId, view, defaultImageId);
                        } else {
                            ImageLoadUtils.loadCircleImage(getContext(), url, view, defaultImageId);
                        }
                    } else {
                        ImageLoadUtils.loadCircleImage(getContext(), defaultImageId, view, defaultImageId);
                    }
                }
            };
            mRoomLoverOnlineGridview.setAdapter(mImageAdapter);
        } else {
            mImageAdapter.notifyDataSetChanged();
        }
    }

    private void loadData() {
        homeLoverUserListPresenter.requestChatMemberByPage(isFirstLoad);
        if (isFirstLoad) {
            isFirstLoad = false;
        }
    }

    /**
     * 更新礼物信息
     */
    private void updateGiftInfo() {
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
    }


    @Override
    public void release() {
        super.release();
        if (roomFragment != null) {
            roomFragment = null;
        }
    }

    @OnClick(R2.id.room_lover_online_number)
    public void showOnlinePlayer() {
        if (getActivity() != null) {
            ListDataDialog.newOnlineUserListInstance(getActivity()).show(getChildFragmentManager());
        }
    }

    @OnClick(R2.id.iv_share)
    public void loverInvite() {
        NewShareDialog shareDialog = new NewShareDialog(getActivity(), true);
        shareDialog.setOnShareDialogItemClick(HomeLoverFragment.this);
        shareDialog.setHasBroadCast(true);
        shareDialog.setHasFriend(true);//邀请好友分享
        if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
            shareDialog.setHasNoticeFans(true);
        }
        shareDialog.show();
    }

    @OnClick(R2.id.iv_help)
    public void loverHelp() {
        if (getContext() == null) {
            return;
        }
//        DialogFragment dialogFragment = (DialogFragment) ARouter.getInstance().build(RoomPersonalRoute.ROOM_LOVER_PLAYING_PATH).navigation();
//        if (dialogFragment.isAdded()) {
//            if (dialogFragment.isStateSaved()) {
//                dialogFragment.dismissAllowingStateLoss();
//            }
//        } else {
//            FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
//            dialogFragment.show(fragmentManager, "LoverPlayingDialogFragment");
//        }

        CpRingDescriptionDialog cpRingDescriptionDialog = CpRingDescriptionDialog.instance("一周CP玩法", UriProvider.getWeekCpDesH5());
        cpRingDescriptionDialog.show(((FragmentActivity) getContext()).getSupportFragmentManager(), CpRingDescriptionDialog.TAG);
    }

    @OnClick(R2.id.room_more)
    public void exitRoom() {
        showMoreItems();
    }

    private void showMoreItems() {
        List<ButtonItem> buttonItems = new ArrayList<>();
        ButtonItem buttonItem1 = new ButtonItem("最小化", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                AvRoomDataManager.get().setMinimize(true);
                getActivity().finish();
            }
        });
        ButtonItem buttonItem2 = new ButtonItem("退出房间", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                FragmentActivity activity = getActivity();
                if (activity != null && activity instanceof AVRoomActivity) {
                    ((AVRoomActivity) activity).exitRoom();
                }
            }
        });

        ButtonItem buttonItem3 = new ButtonItem("房间设置", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                int isPermitRoom = 2;
                if (AvRoomDataManager.get().mCurrentRoomInfo != null)
                    isPermitRoom = AvRoomDataManager.get().mCurrentRoomInfo.getIsPermitRoom();
                RoomSettingActivity.start(getContext(), AvRoomDataManager.get().mCurrentRoomInfo, isPermitRoom);
            }
        });

        String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
            buttonItems.add(buttonItem3);
            final RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (mCurrentRoomInfo != null) {
                int publicChatSwitch = mCurrentRoomInfo.getPublicChatSwitch();
                buttonItems.add(ButtonItemFactory.createPublicSwitch(publicChatSwitch));
            }
        }
        if (!AvRoomDataManager.get().isRoomOwner(currentUid)) {
            buttonItems.add(ButtonItemFactory.createReportItem(mContext, "举报房间", 2));
        }
        buttonItems.add(buttonItem1);
        buttonItems.add(buttonItem2);

        DialogManager dialogManager = ((BaseMvpActivity) getActivity()).getDialogManager();
        if (dialogManager != null) {
            dialogManager.showCommonPopupDialog(buttonItems, "取消");
        }
    }

    @Override
    public void onSharePlatformClick(Platform platform) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo != null) {
            CoreManager.getCore(IShareCore.class).shareRoom(platform, currentRoomInfo.getUid(), currentRoomInfo.getTitle());
        }
    }

    @Override
    public void onSendBroadcastMsg() {
        // FIXME 发送寻友广播
        DialogFragment dialogFragment = (DialogFragment) ARouter.getInstance().build(FindFriendsBroadCastRoute.SEND_DIALOG)
                .navigation(getContext());
        if (dialogFragment.isAdded()) {
            if (dialogFragment.isStateSaved()) {
                dialogFragment.dismissAllowingStateLoss();
            }
        } else {
            FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
            dialogFragment.show(fragmentManager, "RoomBroadcastSendDialog");
        }

    }

    @Override
    public void onShowActivity(List<ActionDialogInfo> dialogInfos) {
//        if (roomFragment != null && roomFragment.isAdded()) {
//            roomFragment.showActivity(dialogInfos);
//        }
    }

    @Override
    public void onDestroy() {
        if (giftView != null) {
            giftView.release();
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        roomFragment.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 定时拉取房间在线人数
     *
     * @param onlineNumber
     */
    @Override
    public void onRoomOnlineNumberSuccess(int onlineNumber) {
//        L.debug(TAG,"onRoomOnlineNumberSuccess() -> setIdOnlineData()");
        super.onRoomOnlineNumberSuccess(onlineNumber);
//        setIdOnlineData();
    }

    @Override
    public void showCar(String carImageUrl) {
        try {
            final String carUrl = DESUtils.DESAndBase64Decrypt(carImageUrl, giftCarSecret);
            LogUtil.d("setUserCarAciton", carUrl);
            if (!TextUtils.isEmpty(carUrl)) {
                giftView.giftEffectView.drawSvgaEffect(carUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRequestChatMemberByPageSuccess(RoomLoverEvent.OnRequestSuccess onRequestSuccess) {
        List<OnlineChatMember> onlineChatMembers = onRequestSuccess.getOnlineChatMembers();
        int onlineUserNum = onlineChatMembers.size();
        L.info(TAG, "RoomLoverEvent.OnRequestSuccess: onlineChatMembers.size() = %d", onlineChatMembers != null ? onlineUserNum : -1);
        mPlayerImages.clear();
        if (onlineChatMembers != null && onlineUserNum < 3) {
            mPlayerImages.addAll(onlineChatMembers);
        } else {
            mPlayerImages.add(onlineChatMembers.get(0));
            mPlayerImages.add(onlineChatMembers.get(1));
            mPlayerImages.add(onlineChatMembers.get(2));
        }
        invokeUserOnlineNum(onlineUserNum);
    }

    private void invokeUserOnlineNum(int onlineUserNum) {
        mImageAdapter.replaceAll(mPlayerImages);
        if (onlineUserNum >= 3) {
            roomLoverOnlineNumber.setVisibility(View.VISIBLE);
            roomLoverOnlineNumber.setText(String.valueOf(onlineUserNum) + "人");
        } else {
            roomLoverOnlineNumber.setVisibility(View.GONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRequestChatMemberByPageFail(RoomLoverEvent.OnRequestFailure onRequestFailure) {
        L.error(TAG, "RoomLoverEvent.OnRequestFailure: %s", onRequestFailure.getMessage());
    }

    public void onMemberIn(String account, List<OnlineChatMember> dataList) {
        homeLoverUserListPresenter.onMemberInRefreshData(account, dataList);
    }

    private void registerRoomEvent() {
        IMNetEaseManager.get()
                .subscribeChatRoomEventObservable(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        if (roomEvent == null) return;
                        int event = roomEvent.getEvent();
                        if (roomEvent.getEvent() == RoomEvent.ADD_BLACK_LIST ||
                                event == RoomEvent.ROOM_MEMBER_EXIT ||
                                roomEvent.getEvent() == RoomEvent.KICK_OUT_ROOM) {
                            L.info(TAG, "registerRoomEvent() -> RoomEvent.getEvent() = %d, account = %s", roomEvent.getEvent(), roomEvent.getAccount());
//                            if (ListUtils.isListEmpty(mPlayerImages)) return;
//                            ListIterator<OnlineChatMember> iterator = mPlayerImages.listIterator();
//                            for (; iterator.hasNext(); ) {
//                                OnlineChatMember onlineChatMember = iterator.next();
//                                if (onlineChatMember.chatRoomMember != null
//                                        && Objects.equals(onlineChatMember.chatRoomMember.getAccount(), roomEvent.getAccount())) {
//                                    iterator.remove();
//                                }
//                            }
//                            mImageAdapter.replaceAll(mPlayerImages);
//                            invokeUserOnlineNum(mPlayerImages.size());
                            loadData();
                        } else if (event == RoomEvent.ROOM_MEMBER_IN) {
                            L.info(TAG, "registerRoomEvent() -> RoomEvent.getEvent() = ROOM_MEMBER_IN account: %s", roomEvent.getAccount());
                            loadData();
//                            onMemberIn(roomEvent.getAccount(), mImageAdapter.getData());
                        }
                    }
                }, this);
    }
}