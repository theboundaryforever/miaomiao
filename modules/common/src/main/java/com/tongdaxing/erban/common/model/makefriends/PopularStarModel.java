package com.tongdaxing.erban.common.model.makefriends;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

public class PopularStarModel extends BaseMvpModel {

    /**
     * 男女推荐列表
     *
     * @param pageNum  页码
     * @param pageSize 一页item数
     */
    public void getDressUpData(int pageNum, int pageSize, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getListCharm(), params, callBack);
    }

    /**
     * 获取用户所在房间信息
     */
    public void getUserRoomInfo(long uid, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid + "");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getUserRoom(), params, callBack);
    }
}
