package com.tongdaxing.erban.common.room.talk;

import com.erban.ui.mvp.BasePresenter;
import com.tcloud.core.log.L;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.praise.PraiseEvent;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by Chen on 2019/4/18.
 */
public class TalkPresenter extends BasePresenter<IRoomTalkView> {
    private String TAG = "TalkPresenter";
    private boolean mIsShowEnterMessage;

    @Override
    public void onCreate() {
        super.onCreate();
        showEnterRoomMessage();
    }

    public List<IRoomTalkMessage> getTalkMessage() {
        return CoreManager.getCore(IRoomCore.class).getTalkCtrl().getHisTalkMessage();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowTalkMessage(RoomTalkEvent.OnRoomShowTalkMessage talkMessage) {
        L.debug(TAG, "onShowTalkMessage");
        if (getView() == null) {
            return;
        }
        getView().showTalkMessage(talkMessage.getRoomTalkMessage());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEnterRoomSuccess(NewRoomEvent.OnEnterRoomSuccess success) {
        L.debug(TAG, "onEnterRoomSuccess");
        showEnterRoomMessage();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowDefaultMessage(RoomTalkEvent.OnAddDefaultMessageFinish addDefaultMessageFinish) {
        L.debug(TAG, "onShowDefaultMessage");
        showEnterRoomMessage();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoomSendMessageFailed(RoomTalkEvent.OnRoomSendMessageFailed failed) {
        L.debug(TAG, "onRoomSendMessageFailed");
        Throwable throwable = failed.getException();
        if (throwable != null) {
            L.info(TAG, "onRoomSendMessageFailed exception: ", throwable);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttentionChange(PraiseEvent.OnAttentionSuccess attentionSuccess) {
        if (getView() == null) {
            return;
        }
        if (attentionSuccess.isAttention()) {
            long uid = attentionSuccess.getUid();
            if (AvRoomDataManager.get().isRoomOwner(uid)) {
                //todo
//                getView().updateTalkView();
            }
        }
    }

    private synchronized void showEnterRoomMessage() {
        if (getView() == null) {
            return;
        }
        if (!mIsShowEnterMessage) {
            List<IRoomTalkMessage> talkMessage = getTalkMessage();
            if (talkMessage != null && talkMessage.size() > 0) {
                mIsShowEnterMessage = true;
                getView().showAllTalkMessage(talkMessage);
            }
        }
    }
}
