package com.tongdaxing.erban.common.room.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.common.base.fragment.BaseDialogFragment;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.MsgStatusEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.CpGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.TipAttachment;
import com.tongdaxing.xchat_core.result.WalletInfoResult;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.HashMap;

/**
 * 房间CP邀请弹窗
 * <p>
 * Created by zhangjian on 2019/3/29.
 */
public class RoomCpInviteDialog extends BaseDialogFragment {
    private static String KEY_CP_RECEIVE_INFO = "CpGiftReceiveInfo";
    private CpGiftReceiveInfo giftRecieveInfo;

    public static RoomCpInviteDialog instance(CpGiftReceiveInfo giftRecieveInfo) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_CP_RECEIVE_INFO, giftRecieveInfo);
        RoomCpInviteDialog roomCpInviteDialog = new RoomCpInviteDialog();
        roomCpInviteDialog.setArguments(bundle);
        return roomCpInviteDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View view = inflater.inflate(R.layout.dialog_room_cp_invite, ((ViewGroup) window.findViewById(android.R.id.content)), false);//需要用android.R.id.content这个view
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        giftRecieveInfo = (CpGiftReceiveInfo) getArguments().getSerializable(KEY_CP_RECEIVE_INFO);
        ImageView iv_avatar = view.findViewById(R.id.iv_avatar);
        TextView tv_cp_invite = view.findViewById(R.id.tv_cp_invite);
        TextView tv_refuse = view.findViewById(R.id.tv_refuse);
        TextView tv_agree = view.findViewById(R.id.tv_agree);

        GlideApp.with(getContext()).load(giftRecieveInfo.getInviteAvatar()).into(iv_avatar);
//        tv_cp_invite.setText(giftRecieveInfo.getInviteName() + "赠送给你一个【" + giftRecieveInfo.getGiftName()
//                + "】邀请你组成CP");
        tv_cp_invite.setText(giftRecieveInfo.getDesc());
        tv_refuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.d("拒绝CP邀请");
                dismiss();
                refuseInvite(giftRecieveInfo);
            }
        });
        tv_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.d("同意CP邀请");
                dismiss();
                agreeInvite(giftRecieveInfo);
            }
        });
    }


    private void agreeInvite(CpGiftReceiveInfo giftRecieveInfo) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("recordId", giftRecieveInfo.getRecordId() + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        ResponseListener Listener = new ResponseListener<ServiceResult>() {

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        LogUtil.d("同意了CP邀请");
                        CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid());//更新本地登录用户信息
                        TipAttachment tipAttachment = new TipAttachment(401, 401);
                        tipAttachment.setCpInviteSuccess(true);
                        tipAttachment.setTip("CP配对成功啦！快前往你的专属页");
                        IMMessage msg = MessageBuilder.createCustomMessage(String.valueOf(giftRecieveInfo.getInviteUid()), SessionTypeEnum.P2P, tipAttachment);
                        msg.setStatus(MsgStatusEnum.unread);
                        NIMClient.getService(MsgService.class).sendMessage(msg, true);
                    } else {
                        SingleToastUtil.showToast(response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
//                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_PERSONAL_GIFT_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.cpAgree(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        WalletInfoResult.class, Request.Method.GET);
    }

    private void refuseInvite(CpGiftReceiveInfo giftRecieveInfo) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("recordId", giftRecieveInfo.getRecordId() + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        ResponseListener Listener = new ResponseListener<ServiceResult>() {

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        LogUtil.d("拒绝了CP邀请");
                    } else {
                        SingleToastUtil.showToast(response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
//                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_PERSONAL_GIFT_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.cpRefuse(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        WalletInfoResult.class, Request.Method.GET);
    }
}
