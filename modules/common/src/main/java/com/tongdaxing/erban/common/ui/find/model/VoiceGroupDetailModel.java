package com.tongdaxing.erban.common.ui.find.model;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public class VoiceGroupDetailModel extends BaseMvpModel {

    /**
     * 获取动态详情数据
     *
     * @param page
     * @param commentId
     * @param myCallBack
     */
    public void getVoiceGroupDetailData(int page, String commentId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("momentId", commentId);
        params.put("pageNum", String.valueOf(page));
        params.put("pageSize", String.valueOf(Constants.PAGE_HOME_HOT_SIZE));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getVoiceGroupDetailList(), params, myCallBack);
    }

    /**
     * 发表评论
     *
     * @param content
     * @param momentId
     * @param playerUid
     * @param myCallBack
     */
    public void publishComment(String content, String momentId, String playerUid, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("context", content);
        params.put("momentId", momentId);
        params.put("playerUid", playerUid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getPublicComment(), params, myCallBack);
    }

    /**
     * 举报动态Items
     *
     * @param momentId   动态id
     * @param playerId   评论id
     * @param reasonStr
     * @param myCallBack
     */
    public void reportVoiceGroupItems(String momentId, String playerId, String reasonStr, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("momentId", momentId);
        params.put("reason", reasonStr);
        params.put("playerId", playerId);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getReportVoiceGroup(), params, myCallBack);
    }

    public void commentLike(String playerId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("playerId", playerId);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getPublicVoiceGroupCommentLike(), params, myCallBack);
    }

    /**
     * 删除动态
     *
     * @param myCallBack
     */
    public void deleteVoiceGroupItem(String momentId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("momentId", momentId);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getDeleteVoiceGroup(), params, myCallBack);
    }
}
