package com.tongdaxing.erban.common.utils;

import android.os.CountDownTimer;

import com.tongdaxing.xchat_core.find.ICreateRoomClient;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomManager;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * Function:
 * Author: Edward on 2019/3/23
 */
public class SquareCountDownTimer extends CountDownTimer {
    private static SquareCountDownTimer countDownTimer;
    public boolean isRunning;

    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    private SquareCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
    }

    public static SquareCountDownTimer getInstance() {
        if (countDownTimer == null) {
            countDownTimer = new SquareCountDownTimer(PublicChatRoomManager.WAITING_TIME * 1000, 1000);
        }
        return countDownTimer;
    }

    public void startCountDownTimer() {
        if (countDownTimer != null) {
            isRunning = true;
            countDownTimer.start();
        }
    }

    @Override
    public void onTick(long millisUntilFinished) {
        isRunning = true;
        int countDownTime = (int) (millisUntilFinished / 1000);
        CoreManager.notifyClients(ICreateRoomClient.class, ICreateRoomClient.REFRESH_SQUARE_NOTIFY, countDownTime);
    }

    @Override
    public void onFinish() {
        isRunning = false;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        CoreManager.notifyClients(ICreateRoomClient.class, ICreateRoomClient.FINISH_SQUARE_NOTIFY);
    }
}
