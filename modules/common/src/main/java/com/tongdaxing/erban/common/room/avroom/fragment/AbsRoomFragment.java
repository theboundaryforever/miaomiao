package com.tongdaxing.erban.common.room.avroom.fragment;

import com.tongdaxing.erban.common.base.fragment.BaseFragment;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;

import java.util.List;

/**
 * @author chenran
 * @date 2017/8/8
 */

public abstract class AbsRoomFragment extends BaseFragment {


    public abstract void onShowActivity(List<ActionDialogInfo> dialogInfo);


    public void onRoomOnlineNumberSuccess(int onlineNumber) {

    }

    public void release() {
    }
}
