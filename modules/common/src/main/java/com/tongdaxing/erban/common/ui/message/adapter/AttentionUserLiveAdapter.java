package com.tongdaxing.erban.common.ui.message.adapter;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;

import java.util.List;


/**
 * 关注用户在线列表
 * Created by chenran on 2017/9/21.
 */
public class AttentionUserLiveAdapter extends RecyclerView.Adapter<AttentionUserLiveAdapter.AttentionUserLiveHolder> implements View.OnClickListener {
    private List<AttentionInfo> attentionInfoList;
    private Activity context;

    public AttentionUserLiveAdapter(Activity context) {
        this.context = context;
    }

    public void setAttentionInfoList(List<AttentionInfo> attentionInfoList) {
        this.attentionInfoList = attentionInfoList;
    }

    @Override
    public AttentionUserLiveAdapter.AttentionUserLiveHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_attention_user_live, parent, false);
        return new AttentionUserLiveAdapter.AttentionUserLiveHolder(item);
    }

    @Override
    public void onBindViewHolder(AttentionUserLiveAdapter.AttentionUserLiveHolder holder, int position) {
        final AttentionInfo attentionInfo = attentionInfoList.get(position);
        holder.liveAnimation.setBackgroundResource(R.drawable.attention_live_anim);
        AnimationDrawable anima = (AnimationDrawable) holder.liveAnimation.getBackground();
        anima.start();
        holder.nick.setText(attentionInfo.getNick());
        if (attentionInfo.getType() == RoomInfo.ROOMTYPE_AUCTION) {
            holder.flagImage.setImageResource(R.drawable.icon_attention_flag_auction);
        } else if (attentionInfo.getType() == RoomInfo.ROOMTYPE_LIGHT_CHAT) {
            holder.flagImage.setImageResource(R.drawable.icon_attention_flag_light_chat);
        } else {
            holder.flagImage.setImageResource(R.drawable.icon_attention_flag_homeparty);
        }

        Drawable drawable;
        if (attentionInfo.getGender() == 1) {
            drawable = context.getResources().getDrawable(R.drawable.icon_man);
        } else {
            drawable = context.getResources().getDrawable(R.drawable.icon_female);
        }

        holder.nick.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        holder.relativeLayout.setTag(position);
        holder.relativeLayout.setOnClickListener(this);

        ImageLoadUtils.loadAvatar(holder.imageBg.getContext(), attentionInfo.getAvatar(), holder.avatar, true);
        ImageLoadUtils.loadImageWithBlurTransformationAndCorner(holder.imageBg.getContext(), attentionInfo.getAvatar(), holder.imageBg);
    }

    @Override
    public int getItemCount() {
        if (attentionInfoList == null) {
            return 0;
        } else {
            return attentionInfoList.size();
        }
    }

    @Override
    public void onClick(View v) {
        Integer position = (Integer) v.getTag();
        AttentionInfo attentionInfo = attentionInfoList.get(position);
//        boolean isImLogin = CoreManager.getCore(IIMLoginCore.class).isImLogin();
//        if (isImLogin) {
        AVRoomActivity.start(v.getContext(), attentionInfo.getUid());
//        }
    }

    public static class AttentionUserLiveHolder extends RecyclerView.ViewHolder {
        private ImageView avatar;
        private ImageView imageBg;
        private ImageView flagImage;
        private ImageView liveAnimation;
        private TextView nick;
        private RelativeLayout relativeLayout;

        public AttentionUserLiveHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            imageBg = (ImageView) itemView.findViewById(R.id.image_bg);
            flagImage = (ImageView) itemView.findViewById(R.id.flag_logo);
            liveAnimation = (ImageView) itemView.findViewById(R.id.live_animation);
            nick = (TextView) itemView.findViewById(R.id.nick);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rly_home);
        }
    }
}
