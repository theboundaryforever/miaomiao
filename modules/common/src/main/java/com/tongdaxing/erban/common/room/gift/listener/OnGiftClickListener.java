package com.tongdaxing.erban.common.room.gift.listener;

import com.tongdaxing.xchat_core.gift.GiftInfo;

public interface OnGiftClickListener {
    void onGiftClick(GiftInfo current);
}
