package com.tongdaxing.erban.common.presenter.rankinglist;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.bean.RankingInfo;

public interface IRankingListView extends IMvpBaseView {
    void setupFailView(String message);

    void setupSuccessView(RankingInfo rankingList);
}

