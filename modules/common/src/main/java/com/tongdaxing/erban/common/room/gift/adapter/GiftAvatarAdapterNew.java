package com.tongdaxing.erban.common.room.gift.adapter;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;

/**
 * Created by chenran on 2017/10/25.
 */
@Deprecated
public class GiftAvatarAdapterNew extends BaseQuickAdapter<MicMemberInfo, BaseViewHolder> {
    private OnCancelAllMicSelectListener onCancelAllMicSelectListener;
    private int selectCount = 1;

    public GiftAvatarAdapterNew() {
        super(R.layout.list_item_gift_avatar);
    }

    public void setAllSelect(boolean isAllMic) {
        this.selectCount = isAllMic ? getItemCount() - 1 : 0;
        if (onCancelAllMicSelectListener != null)
            onCancelAllMicSelectListener.onChange(isAllMic, selectCount);
    }

    @Override
    protected void convert(BaseViewHolder helper, MicMemberInfo item) {
        TextView tvName = helper.getView(R.id.user_name);
        ImageView avatar = (CircleImageView) helper.getView(R.id.avatar);
        RelativeLayout avatarContainer = (RelativeLayout) helper.getView(R.id.avatar_container);
        ImageLoadUtils.loadAvatar(mContext, item.getAvatar(), avatar);
        CheckBox cbSelect = helper.getView(R.id.cb_member_select);
        tvName.setText(item.getNick());
        if (item.isSelect()) {
            cbSelect.setChecked(true);
            tvName.setSelected(true);
        } else {
            cbSelect.setChecked(false);
            tvName.setSelected(false);
        }
        avatarContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.isSelect()) {
                    item.setSelect(false);
                    selectCount--;
                } else {
                    item.setSelect(true);
                    selectCount++;
                }
                if (selectCount == 0) {
                    if (onCancelAllMicSelectListener != null)
                        onCancelAllMicSelectListener.onChange(false, selectCount);
                } else if (selectCount == getItemCount() - 1) {//选中之后如果满人数则全选
                    if (onCancelAllMicSelectListener != null)
                        onCancelAllMicSelectListener.onChange(true, selectCount);
                } else {
                    if (onCancelAllMicSelectListener != null)//选中之前如果满人数则取消全选
                        onCancelAllMicSelectListener.onChange(false, selectCount);
                }
                notifyDataSetChanged();
            }
        });
    }

    public void setOnCancelAllMicSelectListener(OnCancelAllMicSelectListener onCancelAllMicSelectListener) {
        this.onCancelAllMicSelectListener = onCancelAllMicSelectListener;
    }

    public interface OnCancelAllMicSelectListener {
        void onChange(boolean isAllMic, int count);
    }
}
