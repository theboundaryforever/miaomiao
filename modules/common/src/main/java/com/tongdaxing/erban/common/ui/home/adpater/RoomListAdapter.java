package com.tongdaxing.erban.common.ui.home.adpater;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.ui.home.fragment.HotTagFragment;
import com.tongdaxing.erban.common.ui.widget.Banner;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.view.HomeMiddleBannerHintView;
import com.netease.nim.uikit.glide.GlideApp;
import com.ruffian.library.widget.RRelativeLayout;
import com.ruffian.library.widget.RView;
import com.ruffian.library.widget.helper.RBaseHelper;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.uuzuche.lib_zxing.DisplayUtil;

import java.util.List;

/**
 * <p> 热门标签房间列表adapter </p>
 */
public class RoomListAdapter extends BaseMultiItemQuickAdapter<HomeRoom, BaseViewHolder> {
    public static final int POSITION_NOT_BANNER = -1;
    private String TAG = "RoomListAdapter";
    private Context context;
    private int itemMargin, listMargin;
    private int bannerPosition = POSITION_NOT_BANNER;//banner所在的位置

    private int popularInfoListSize;
    private String mBannerLocation;

    public int getPopularInfoListSize() {
        return popularInfoListSize;
    }

    public void setPopularInfoListSize(int popularInfoListSize) {
        this.popularInfoListSize = popularInfoListSize;
    }

    public RoomListAdapter(Context context) {
        super(null);
        this.context = context;
        addItemType(0, R.layout.list_item_home_hot_simple_for_miaobo);
        addItemType(1, R.layout.item_hot_banner);

        listMargin = context.getResources().getDimensionPixelSize(R.dimen.home_room_margin_left_or_right);//recyclerview的左边距和右边距
        itemMargin = context.getResources().getDimensionPixelSize(R.dimen.home_room_item_margin);//item间的边距
    }

    public RoomListAdapter(Context context, String bannerLocation) {
        super(null);
        this.context = context;
        mBannerLocation = bannerLocation;
        addItemType(0, R.layout.list_item_home_hot_simple_for_miaobo);
        addItemType(1, R.layout.item_hot_banner);

        listMargin = context.getResources().getDimensionPixelSize(R.dimen.home_room_margin_left_or_right);//recyclerview的左边距和右边距
        itemMargin = context.getResources().getDimensionPixelSize(R.dimen.home_room_item_margin);//item间的边距
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, HomeRoom homeRoom) {
        if (homeRoom == null) {
            return;
        }
        if (homeRoom.getItemType() == 1) {
            setupBanner(baseViewHolder, homeRoom);
        } else {
            setupNormalView(baseViewHolder, homeRoom);
        }
    }

    public void setBannerPosition(int bannerPosition) {
        this.bannerPosition = bannerPosition;
    }

    /**
     * 获取当前view在所在行中的位置
     */
    private int getPositionByRaw(int position) {
        int dataPosition = position - getHeaderLayoutCount();//数据position，排除header
        if (bannerPosition == POSITION_NOT_BANNER || position <= bannerPosition) {
            return dataPosition % HotTagFragment.ITEM_COUNT_BY_ROW;
        } else {//插了一个banner后，位置就不一样了
            return (dataPosition + 1) % HotTagFragment.ITEM_COUNT_BY_ROW;
        }
    }

    private void setupNormalView(BaseViewHolder baseViewHolder, HomeRoom homeRoom) {
        int positionByRow = getPositionByRaw(baseViewHolder.getAdapterPosition());
        if (positionByRow % HotTagFragment.ITEM_COUNT_BY_ROW == 0) {//当前行的第一个
            baseViewHolder.itemView.setPadding(listMargin, 0, itemMargin / 2, 0);
        } else if (positionByRow % HotTagFragment.ITEM_COUNT_BY_ROW == HotTagFragment.ITEM_COUNT_BY_ROW - 1) {//当前行的最后一个
            baseViewHolder.itemView.setPadding(itemMargin / 2, 0, listMargin, 0);
        } else {
            //如果要支持列数不等于2，这里要考虑
        }

        RRelativeLayout rRelativeLayout = baseViewHolder.getView(R.id.item_home_room_rrl);
        RView rView = baseViewHolder.getView(R.id.item_home_room_rview);
        //获取Helper
        RBaseHelper rRelativeLayoutHelper = rRelativeLayout.getHelper();
        RBaseHelper rViewHelper = rView.getHelper();
        int dataPosition = baseViewHolder.getAdapterPosition();
//        if (!mIsPopular) {
        dataPosition -= getHeaderLayoutCount();//数据position，排除header
//        }
        if (bannerPosition != POSITION_NOT_BANNER && dataPosition > bannerPosition) {
            dataPosition--;
        }
        int rRelativeLayoutBgId = R.color.home_room_bg1;
        int rViewBgId = R.color.home_room_bg_stroke1;

        dataPosition += popularInfoListSize;//要接着人气列表的item来设置热门房间下面的item的颜色
        switch ((dataPosition + 1) % 4) {
            case 1:
                rRelativeLayoutBgId = R.color.home_room_bg1;
                rViewBgId = R.color.home_room_bg_stroke1;
                break;
            case 2:
                rRelativeLayoutBgId = R.color.home_room_bg2;
                rViewBgId = R.color.home_room_bg_stroke2;
                break;
            case 3:
                rRelativeLayoutBgId = R.color.home_room_bg3;
                rViewBgId = R.color.home_room_bg_stroke3;
                break;
            case 0:
                rRelativeLayoutBgId = R.color.home_room_bg4;
                rViewBgId = R.color.home_room_bg_stroke4;
                break;
        }
        rRelativeLayoutHelper.setBackgroundColorNormal(context.getResources().getColor(rRelativeLayoutBgId));
        rViewHelper.setBorderColorNormal(context.getResources().getColor(rViewBgId))
                .setBorderWidthNormal(DisplayUtil.dip2px(context, 8));

        ImageView ivCover = baseViewHolder.getView(R.id.iv_cover);
        ImageLoadUtils.loadCircleImage(mContext, homeRoom.getAvatar(), ivCover, R.drawable.default_cover);

        baseViewHolder.setText(R.id.tv_title, homeRoom.getTitle());
        baseViewHolder.setText(R.id.tv_nick, homeRoom.getNick());
        baseViewHolder.setText(R.id.tv_online_num, String.valueOf(homeRoom.getOnlineNum()));

        TextView tvCustomLabel = baseViewHolder.getView(R.id.tv_custom_label);

        String roomTag = homeRoom.getRoomTag();
        if (TextUtils.isEmpty(roomTag)) {
            tvCustomLabel.setVisibility(View.INVISIBLE);
        } else {
            tvCustomLabel.setText(roomTag);
            tvCustomLabel.setVisibility(View.VISIBLE);
        }
        ImageView ivPlay = baseViewHolder.getView(R.id.iv_play);
        GlideApp.with(context)
                .load(R.drawable.icon_music_play_white)
                .into(ivPlay);

        ImageView ivTabCustom = baseViewHolder.getView(R.id.iv_custom_tag);
        if (!TextUtils.isEmpty(homeRoom.getCustomTag())) {
            ivTabCustom.setImageDrawable(null);
            ivTabCustom.setVisibility(View.VISIBLE);
            GlideApp.with(context)
                    .load(homeRoom.getCustomTag())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Drawable> target, boolean b) {
                            ivTabCustom.setVisibility(View.INVISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable drawable, Object o, Target<Drawable> target, DataSource dataSource, boolean b) {
                            float ratio = (drawable.getIntrinsicHeight() + 0.F) / drawable.getIntrinsicWidth();
                            int width = Math.round(context.getResources().getDimensionPixelOffset(R.dimen.custom_tag_height) / ratio);
                            int height = context.getResources().getDimensionPixelOffset(R.dimen.custom_tag_height);
                            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) ivTabCustom.getLayoutParams();
                            params.width = width;
                            params.height = height;
                            ivTabCustom.setLayoutParams(params);
                            ivTabCustom.setImageDrawable(drawable);
                            return true;
                        }
                    })
                    .into(ivTabCustom);
        } else {
            ivTabCustom.setVisibility(View.INVISIBLE);
        }
    }

    private void setupBanner(BaseViewHolder baseViewHolder, HomeRoom homeRoom) {
        Banner banner = baseViewHolder.getView(R.id.banner);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) banner.getLayoutParams();
        lp.height = Math.round((DisplayUtils.getScreenWidth(context) - itemMargin * 2) * 85.0f / 345.0f);
        banner.setLayoutParams(lp);
        banner.setPlayDelay(5000);
        banner.setAnimationDurtion(500);
        List<BannerInfo> bannerInfoList = homeRoom.getBannerInfos();
        BannerAdapter bannerAdapter = new BannerAdapter(bannerInfoList, context, mBannerLocation);
        bannerAdapter.setIsImageRound(true);
        HomeMiddleBannerHintView homeMiddleBannerHintView = new HomeMiddleBannerHintView(
                context, context.getResources().getColor(R.color.white),
                context.getResources().getColor(R.color.home_room_banner_normal_bg), 12, 4);
        if (bannerInfoList.size() > 1) {
            banner.setHintView(homeMiddleBannerHintView);
        } else {
            banner.setHintView(null);
        }
        banner.setAdapter(bannerAdapter);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
        if (manager instanceof GridLayoutManager) {
            final GridLayoutManager g = (GridLayoutManager) manager;
            g.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return getItemViewType(position) == 0 ? 1 : HotTagFragment.ITEM_COUNT_BY_ROW;
                }
            });
        }
    }
}
