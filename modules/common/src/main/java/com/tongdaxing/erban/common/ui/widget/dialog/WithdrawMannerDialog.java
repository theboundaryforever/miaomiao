package com.tongdaxing.erban.common.ui.widget.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.common.ui.me.withdraw.BinderAlipayActivity;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

/**
 * Function:选择提现方式对话框
 * Author: Edward on 2019/1/9
 */
public class WithdrawMannerDialog extends DialogFragment implements View.OnClickListener {
    private static final String TEMP = "temp";
    private static WithdrawMannerDialog withdrawMannerDialog;
    private TextView tvBindingAliPay, tvBindingWxPay;
    private WithdrawInfo withdrawInfo;
    private ImageView ivAliCurrentManner, ivWxCurrentManner;
    private ImageView ivAliLogo, ivWxLogo;
    private LinearLayout llAliWithdrawaImg, llWxWithdrawImg;

    @SuppressLint("ValidFragment")
    private WithdrawMannerDialog() {
    }

    public static WithdrawMannerDialog newInstance(WithdrawInfo withdrawInfo) {
        Bundle args = new Bundle();
        args.putSerializable(TEMP, withdrawInfo);
        if (withdrawMannerDialog == null) {
            withdrawMannerDialog = new WithdrawMannerDialog();
        }
        withdrawMannerDialog.setArguments(args);
        return withdrawMannerDialog;
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getContentView(), container, false);
        initDialog();
        initView(view);
        initData();
        return view;
    }

    private void initDialog() {
        Dialog dialog = this.getDialog();
        if (null != dialog) {
            Window window = dialog.getWindow();
            if (null != window) {
                window.setBackgroundDrawable(new ColorDrawable(0));
            }
        }
    }

    public void initView(View view) {
        llAliWithdrawaImg = view.findViewById(R.id.ll_ali_withdraw_img);
        llWxWithdrawImg = view.findViewById(R.id.ll_wx_withdraw_img);
        ivAliCurrentManner = view.findViewById(R.id.iv_ali_current_manner);
        ivWxCurrentManner = view.findViewById(R.id.iv_wx_current_manner);
        tvBindingAliPay = view.findViewById(R.id.tv_binding_ali_pay);
        tvBindingWxPay = view.findViewById(R.id.tv_binding_wx_pay);
        ivAliLogo = view.findViewById(R.id.iv_ali_logo);
        ivWxLogo = view.findViewById(R.id.iv_wx_logo);
        tvBindingAliPay.setOnClickListener(this);
        tvBindingWxPay.setOnClickListener(this);
    }

    public void initData() {
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        withdrawInfo = (WithdrawInfo) bundle.getSerializable(TEMP);
        if (withdrawInfo == null) {
            return;
        }

        if (isBindingAliPay()) {
            ivAliLogo.setImageResource(R.drawable.ic_withdraw_ali_logo);
            tvBindingAliPay.setText("修改信息");
            setCurrentManner(withdrawInfo.payment);
            llAliWithdrawaImg.setOnClickListener(v -> {
                setCurrentManner(2);
                dismiss();
                SingleToastUtil.showToast("提现方式更换成功");
                withdrawInfo.payment = 2;
                CoreManager.getCore(IWithdrawCore.class).refreshOnGetWithdrawUserInfo(withdrawInfo);
            });
        }

        if (isBindingWxPay()) {
            ivWxLogo.setImageResource(R.drawable.ic_withdraw_wx_logo);
            tvBindingWxPay.setText("修改信息");
            setCurrentManner(withdrawInfo.payment);
            llWxWithdrawImg.setOnClickListener(v -> {
                setCurrentManner(1);
                dismiss();
                SingleToastUtil.showToast("提现方式更换成功");
                withdrawInfo.payment = 1;
                CoreManager.getCore(IWithdrawCore.class).refreshOnGetWithdrawUserInfo(withdrawInfo);
            });
        }
    }

    private boolean isBindingAliPay() {
        if (withdrawInfo == null) {
            return false;
        }
        return !TextUtils.isEmpty(withdrawInfo.alipayAccount) && !TextUtils.isEmpty(withdrawInfo.alipayAccountName);
    }

    private boolean isBindingWxPay() {
        if (withdrawInfo == null) {
            return false;
        }
        return !TextUtils.isEmpty(withdrawInfo.wxPhone) && !TextUtils.isEmpty(withdrawInfo.wxNickName) && !TextUtils.isEmpty(withdrawInfo.wxRealName);
    }


    private void setCurrentManner(int curentManner) {
        if (curentManner == 1) {
            ivAliCurrentManner.setVisibility(View.GONE);
            ivWxCurrentManner.setVisibility(View.VISIBLE);
        } else if (curentManner == 2) {
            ivAliCurrentManner.setVisibility(View.VISIBLE);
            ivWxCurrentManner.setVisibility(View.GONE);
        } else {
            ivAliCurrentManner.setVisibility(View.GONE);
            ivWxCurrentManner.setVisibility(View.GONE);
        }
    }


    public int getContentView() {
        return R.layout.dialog_withdraw_manner;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();//绑定支付宝
//绑定微信
        if (i == R.id.tv_binding_ali_pay) {
            if (isBindingAliPay()) {//更换支付宝信息
                Intent intent = new Intent(getActivity(), BinderAlipayActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("withdrawInfo", withdrawInfo);
                intent.putExtras(mBundle);
                startActivity(intent);
            } else {
                bindingThird(2);
            }

        } else if (i == R.id.tv_binding_wx_pay) {
            if (isBindingWxPay()) {
                existPhone();
            } else {
                bindingThird(1);
            }

        }
    }

    /**
     * 是否存在手机号码
     */
    private void existPhone() {
        CommonTitleDialog commonTitleDialog = new CommonTitleDialog();
        commonTitleDialog.show(getFragmentManager(), "");
        dismiss();
    }

    /**
     * 绑定第三方
     *
     * @param type
     */
    private void bindingThird(int type) {
        if (type == 1) {//微信
            existPhone();
        } else {//支付宝
            Intent intent = new Intent(getActivity(), BinderAlipayActivity.class);
            Bundle mBundle = new Bundle();
            mBundle.putSerializable("withdrawInfo", withdrawInfo);
            intent.putExtras(mBundle);
            startActivity(intent);
            dismiss();
        }
    }

    /**
     * 解绑第三方
     *
     * @param type
     */
    private void unbindingThird(int type) {

    }
}
