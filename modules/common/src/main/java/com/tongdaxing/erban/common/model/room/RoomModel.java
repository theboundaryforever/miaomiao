package com.tongdaxing.erban.common.model.room;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.AdvertInfo;
import com.tongdaxing.xchat_core.home.HomeInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;

public class RoomModel extends BaseMvpModel {

    /**
     * 获取热门分类数据
     */
    public void getHotTabData(int page, HttpRequestCallBack<HomeInfo> callBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("pageNum", String.valueOf(page));
        requestParam.put("pageSize", String.valueOf(Constants.PAGE_HOME_HOT_SIZE));
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().doGetRequest(UriProvider.getMainHotData(), requestParam, callBack);
    }

    /**
     * 获取其他分类数据
     */
    public void getOtherTabData(int tagId, int page, HttpRequestCallBack<List<HomeRoom>> callBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("pageNum", String.valueOf(page));
        requestParam.put("pageSize", String.valueOf(Constants.PAGE_HOME_HOT_SIZE));
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        requestParam.put("tagId", String.valueOf(tagId));
        OkHttpManager.getInstance().doGetRequest(UriProvider.getMainDataByTab(), requestParam, callBack);
    }

    /**
     * 获取节目预告和活位推荐
     */
    public void getAdvert(HttpRequestCallBack<AdvertInfo> callBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().doGetRequest(UriProvider.getAdvert(), requestParam, callBack);
    }

    /**
     * 获取直播tab下的banner
     */
    public void getLiveBanner(HttpRequestCallBack<HomeRoom> callBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().doGetRequest(UriProvider.getLiveBanner(), requestParam, callBack);
    }
}
