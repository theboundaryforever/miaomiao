package com.tongdaxing.erban.common.ui.web;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.miaomiao.ndklib.JniUtils;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.util.JsonParserUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.route.CommonRoutePath;
import com.tongdaxing.erban.common.ui.cpexclusive.CpExclusivePageActivity;
import com.tongdaxing.erban.common.ui.find.activity.PublishVoiceGroupActivity;
import com.tongdaxing.erban.common.ui.me.audiocard.AudioCard2Activity;
import com.tongdaxing.erban.common.ui.me.user.activity.UserInfoModifyActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.BinderPhoneActivity;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.widget.dialog.shareDialog.NewShareDialog;
import com.tongdaxing.erban.common.utils.NewAndOldUserEventUtils;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.JsResponseInfo;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.http_image.util.DeviceUuidFactory;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SignUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.json.JsonParser;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * <p> html js 与webview 交互接口</p>
 * Created by ${user} on 2017/11/6.
 */
public class JSInterface {
    private static final String TAG = "JSInterface";
    private WebView mWebView;
    private CommonWebViewActivity mActivity;
    private int mPosition;

    public JSInterface(WebView webView, CommonWebViewActivity activity) {
        mWebView = webView;
        mActivity = activity;
    }

    /**
     * 调转个人主页
     *
     * @param uid 用户id
     */
    @JavascriptInterface
    public void openPersonPage(String uid) {
        LogUtil.i(TAG, "openPersonPage：" + uid);
        if (!TextUtils.isEmpty(uid)) {
            try {
                long uidLong = Long.parseLong(uid);
                UIHelper.showUserInfoAct(mActivity, uidLong);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 声鉴卡，“点击匹配超动听的声音”
     */
    @JavascriptInterface
    public void openAudioMatchPage() {
        NewAndOldUserEventUtils.onCommonTypeEvent(mActivity, UmengEventId.getSoundMatch());
        ARouter.getInstance().build(CommonRoutePath.AUDIO_MATCH_ACTIVITY_ROUTE_PATH).navigation();
    }

    /**
     * 声鉴卡，点击重新鉴定
     */
    @JavascriptInterface
    public void openMyAduioPage() {
        UmengEventUtil.getInstance().onEvent(mActivity, UmengEventId.getIdentifyCardClear());
        mActivity.checkPermission(() -> {
                    Intent intent = new Intent(mActivity, AudioCard2Activity.class);
                    intent.putExtra(AudioCard2Activity.AUDIO_FILE_URL, "");
                    intent.putExtra(AudioCard2Activity.AUDIO_LENGTH, 0);
                    mActivity.startActivityForResult(intent, UserInfoModifyActivity.Method.AUDIO);
                }, R.string.ask_again,
                Manifest.permission.RECORD_AUDIO);
        mActivity.finish();//关闭当前页面
    }

    /**
     * 将base64转换为bitmap
     *
     * @param string
     * @return
     */
    private Bitmap stringToBitmap(String string) {
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray = Base64.decode(string.split(",")[1], Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * 保存声鉴卡图片
     *
     * @param imgBase64
     */
    @JavascriptInterface
    public void saveAudioCardImg(String imgBase64) {
        UmengEventUtil.getInstance().onEvent(mActivity, UmengEventId.getIdentifyCardSaveImg());
        Bitmap bitmap = stringToBitmap(imgBase64);
        if (bitmap != null) {
            String fileName = System.currentTimeMillis() + ".jpg";
            String insertFilePath = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), bitmap, fileName, fileName);//插入图片到相册中
            File file1 = new File(getRealPathFromURI(Uri.parse(insertFilePath), mActivity));
            updatePhotoMedia(file1, mActivity);
            SingleToastUtil.showToast(mActivity.getResources().getString(R.string.save_img_succeed));
        }
    }

    //更新图库
    private void updatePhotoMedia(File file, Context context) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(file));
        context.sendBroadcast(intent);
    }

    private String getRealPathFromURI(Uri contentUri, Context context) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String fileStr = cursor.getString(column_index);
            cursor.close();
            return fileStr;
        } else {
            return "";
        }
    }

    /**
     * 将图片保存在本地
     *
     * @param bitmap
     * @return
     */
    private String savePictureToLocal(Bitmap bitmap) {
        try {
            if (bitmap != null) {
                String fileName = System.currentTimeMillis() + ".jpg";
                String saveFilePath = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DCIM + File.separator + "Camera" + File.separator + "/" + fileName;
                File file = new File(saveFilePath);
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
                return file.getAbsolutePath();
            } else {
                return "";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 分享到朋友圈
     *
     * @param imgBase64
     */
    @JavascriptInterface
    public void shareToMonents(String imgBase64) {
        UmengEventUtil.getInstance().onEvent(mActivity, UmengEventId.getIdentifyCardShare());
        mActivity.showShareToMomentsProgress();
        //逻辑先保存图片到本地，接着上传图片给七牛，接着调起分享API
        Bitmap bitmap = stringToBitmap(imgBase64);
        String localPicturePath = savePictureToLocal(bitmap);

        if (!TextUtils.isEmpty(localPicturePath)) {
            File file = new File(localPicturePath);
            if (file.isFile()) {
                CoreManager.getCore(IFileCore.class).upload(file);
            } else {
                SingleToastUtil.showToast("文件错误!");
            }
        } else {
            SingleToastUtil.showToast("图片路径错误!");
        }
    }

    /**
     * 跳转充值页面
     */
    @JavascriptInterface
    public void openChargePage() {
        if (mActivity != null) {
            MyWalletNewActivity.start(mActivity);
        }
    }

    /**
     * 前端页面会调用这个方法来设置右上角分享按钮的显示/隐藏
     *
     * @param isShow
     */
    @JavascriptInterface
    public void showShareButton(boolean isShow) {
        L.debug(TAG, "showShareButton isShow = %b", isShow);
        if (mActivity != null && mActivity.getImgShare() != null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mActivity != null && mActivity.getImgShare() != null) {
                        L.debug(TAG, "showShareButton mActivity != null: %b, mActivity.getImgShare() != null: %b,mActivity.getOption() = %s",
                                mActivity != null, mActivity.getImgShare() != null, mActivity.getOption());
                        if (!TextUtils.isEmpty(mActivity.getOption())) {//文字选项的显示优先级更高
                            mActivity.getImgShare().setVisibility(View.GONE);
                        } else {
                            mActivity.getImgShare().setVisibility(isShow ? View.VISIBLE : View.GONE);
                        }
                    }
                }
            });
        }
    }

    @JavascriptInterface
    public void openSharePage() {
        if (mActivity != null) {
            NewShareDialog shareDialog = new NewShareDialog(mActivity);
            shareDialog.setOnShareDialogItemClick(mActivity);
            shareDialog.show();
        }
    }

    /**
     * 调转钱包页
     */
    @JavascriptInterface
    public void openPurse() {
        LogUtil.i(TAG, "openPurse");
    }

    /**
     * 调转房间
     *
     * @param uid 房主uid
     */
    @JavascriptInterface
    public void openRoom(String uid) {
        LogUtil.i(TAG, "openRoom：" + uid);
        if (!TextUtils.isEmpty(uid)) {
            try {
                long uidLong = Long.parseLong(uid);
                AVRoomActivity.start(mActivity, uidLong);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取用户ticket
     *
     * @return
     */
    @JavascriptInterface
    public String getTicket() {
        return CoreManager.getCore(IAuthCore.class).getTicket();
    }

    /**
     * 获取设备ID
     *
     * @return 设备id
     */
    @JavascriptInterface
    public String getDeviceId() {
        return DeviceUuidFactory.getDeviceId(BasicConfig.INSTANCE.getAppContext());
    }

    /**
     * 获取uid
     *
     * @return uid
     */
    @JavascriptInterface
    public String getUid() {
     /*   if (mActivity != null && mWebView != null) {
            LogUtil.i(TAG, "getUid..........................");
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String args = "'uid'," + String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                    mWebView.loadUrl("javascript:getMessage(" + args + ")");
                }
            });
        }*/
        return String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @JavascriptInterface
    public String getPosition() {
        return String.valueOf(mPosition);
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    /*--------------------图片选择 start--------------------------*/

    /**
     * 请求调起拍照+相册选择器
     */
    @JavascriptInterface
    public void requestImageChooser() {
        if (mActivity != null) {
            mActivity.showImageChooser();
        }
    }

    /**
     * 通知H5图片选择结果
     */
    public void onImageChooserResult(String imageUrl) {
        mWebView.evaluateJavascript("onImageChooserResult('" + imageUrl + "')", value -> {

        });
    }

    /*--------------------图片选择 end--------------------------*/

    /*-----------------网络请求 start----------------------*/

    /**
     * 发起网络请求
     */
    @JavascriptInterface
    public void httpRequest(int requestMethod, String urlController, String headerMapString, String paramMapString) {
        Map<String, String> header;
        Map<String, String> params;
        try {
            header = mapObejctsToMapStrings(JsonParser.toMap(headerMapString));
            params = mapObejctsToMapStrings(JsonParser.toMap(paramMapString));
        } catch (Exception e) {
            e.printStackTrace();
            onHttpResponse(urlController, true, "", e.getMessage());
            return;
        }
        if (params == null) {
            params = new HashMap<>();
        }
        params = CommonParamUtil.getDefaultParam(params);
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.JAVA_WEB_URL.concat(urlController);
        if (requestMethod == 1) {
            OkHttpManager.getInstance().doPostRequest(url, header, params, new OkHttpManager.MyCallBack<String>() {
                @Override
                public void onError(Exception e) {
                    onHttpResponse(urlController, true, "", e.getMessage());
                }

                @Override
                public void onResponse(String response) {
                    onHttpResponse(urlController, false, response, "");
                }
            });
        } else {
            OkHttpManager.getInstance().doGetRequest(url, header, params, new OkHttpManager.MyCallBack<String>() {
                @Override
                public void onError(Exception e) {
                    onHttpResponse(urlController, true, "", e.getMessage());
                }

                @Override
                public void onResponse(String response) {
                    onHttpResponse(urlController, false, response, "");
                }
            });
        }
    }

    private Map<String, String> mapObejctsToMapStrings(Map<String, Object> mapObjects) {
        Map<String, String> mapStrings = new HashMap<>();
        if (mapObjects != null) {
            for (Map.Entry<String, Object> mapObject : mapObjects.entrySet()) {
                mapStrings.put(mapObject.getKey(), mapObject.getValue().toString());
            }
        }
        return mapStrings;
    }

    /**
     * 通知H5请求结果
     */
    public void onHttpResponse(String urlController, boolean isRequestError, String bodyString, String errorMsg) {
        CoreUtils.runInMainThread(new Runnable() {
            @Override
            public void run() {
                String responseStr;
                JsResponseInfo responseInfo = new JsResponseInfo();
                responseInfo.setUrlController(urlController);
                responseInfo.setRequestError(isRequestError);
                responseInfo.setBodyString(bodyString);
                responseInfo.setErrorMsg(errorMsg);
                responseStr = JsonParser.toJson(responseInfo);
                mWebView.evaluateJavascript("onHttpResponse(" + responseStr + ")", value -> {

                });
            }
        });
    }

    /*-----------------网络请求 end----------------------*/

    /*-------------------用户信息 start---------------------------*/

    @JavascriptInterface
    public String getUserPhoneNumber() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null && !TextUtils.isEmpty(userInfo.getPhone()) && !userInfo.getPhone().equals(String.valueOf(userInfo.getErbanNo()))) {//手机号为用户id 也是属于未绑定手机号
            return userInfo.getPhone();
        }
        return "";
    }

    /*-------------------用户信息 end---------------------------*/


    /**
     * 点击双方任意一个头像跳转到CP专属页
     *
     * @param userIdStr 用户id
     */
    @JavascriptInterface
    public void openCpExclusivePage(String userIdStr) {
        long userId = Long.valueOf(userIdStr);
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo.getUid() == userId) {
            if (userInfo.getCpUser() != null) {
                //打开自己和cp的CP专属页 有解除CP功能
                CpExclusivePageActivity.start(mActivity, userId, true);
            }
        } else {
            if (userInfo.getCpUser() != null && userInfo.getCpUser().getUid() == userId) {
                //打开自己和cp的CP专属页 有解除CP功能
                CpExclusivePageActivity.start(mActivity, userId, true);
            } else {
                //打开他人的CP专属页 没有解除CP功能
                CpExclusivePageActivity.start(mActivity, userId);
            }
        }
    }

    /**
     * 支持webview金币数展示
     *
     * @param jsonStr
     * @return
     */
    @JavascriptInterface
    public String getSn(String jsonStr) {
        String sn = "";
        if (TextUtils.isEmpty(jsonStr)) {
            return sn;
        }
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String url = jsonObject.getString("url");
            JSONObject jsonParams = jsonObject.getJSONObject("data");
            Map<String, String> params = new HashMap<>();
            for (Iterator<String> it = jsonParams.keys(); it.hasNext(); ) {
                String key = it.next();
                params.put(key, jsonParams.getString(key));
            }
            String kp = JniUtils.getDk(mActivity);
            sn = SignUtils.signParams(url, params, kp, String.valueOf(System.currentTimeMillis()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sn;
    }

    /**
     * 刷新背包礼物
     */
    @JavascriptInterface
    public void refreshGift(String jsonStr) {
        //刷新背包礼物
        CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
    }

    /**
     * 跳转绑定手机页
     */
    @JavascriptInterface
    public void openBindPhonePage() {
        Intent intent = new Intent(mActivity, BinderPhoneActivity.class);
        mActivity.startActivity(intent);
    }

    @JavascriptInterface
    public void bindWx() {
        L.info(TAG, "bindWx");
        Platform wechat = ShareSDK.getPlatform(Wechat.NAME);
        String url = "/withDraw/boundThird";
        if (!wechat.isClientValid()) {
            L.info(TAG, "bindWx 未安装微信");
            onHttpResponse(url, true, "", "未安装微信");
            return;
        }
        if (wechat.isAuthValid()) {
            wechat.removeAccount(true);
        }

        wechat.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(final Platform platform, int i, HashMap<String, Object> hashMap) {
                if (i == Platform.ACTION_USER_INFOR) {
                    WxJson json = new WxJson();
                    json.setAccessToken(platform.getDb().getToken());
                    json.setNickName(platform.getDb().getUserName());
                    json.setOpenId(platform.getDb().getUserId());
                    json.setUnionId(platform.getDb().get("unionid"));
                    String toJson = JsonParserUtil.toJson(json);
                    L.info(TAG, "onComplete toJson: %s", toJson);
                    onHttpResponse(url, false, JsonParserUtil.toJson(json), "");
                } else {
                    L.info(TAG, "onComplete i: %d", i);
                    onHttpResponse(url, true, "", "[errorCode: " + i + "]");
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                L.info(TAG, "bindWx onError i: %d, throwable: %s", i, throwable);
                if (null != throwable) {
                    onHttpResponse(url, true, "", "绑定失败");
                }
            }

            @Override
            public void onCancel(Platform platform, int i) {
                L.info(TAG, "bindWx onCancel i: %d", i);
                onHttpResponse(url, true, "", "用户取消了");
            }
        });
        wechat.SSOSetting(false);
        wechat.showUser(null);
    }

    /**
     * 跳转发布声圈（动态）页
     */
    @JavascriptInterface
    public void openPublishVoiceGroupPage(String text) {
        PublishVoiceGroupActivity.start(mActivity, text);
    }
}
