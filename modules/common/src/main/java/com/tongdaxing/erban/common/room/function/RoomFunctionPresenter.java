package com.tongdaxing.erban.common.room.function;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.common.room.RoomBasePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.RoomAction;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Map;

import static com.tongdaxing.xchat_core.UriProvider.JAVA_WEB_URL;

/**
 * Created by Chen on 2019/6/6.
 */
public class RoomFunctionPresenter extends RoomBasePresenter<IRoomFunctionView> {
    private String TAG = "RoomFunctionPresenter";

    @Override
    public void onCreate() {
        super.onCreate();
        getRankData();
        //打开房间时 重新更新一下音乐按钮的显示 防止最小化时不显示音乐按钮
        if (getView() == null) {
            return;
        }
        getView().updateMusicView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChairChange(RoomAction.OnChairStateChange stateChange) {
        if (getView() == null) {
            return;
        }
        getView().updateMusicView();
    }

    private void getRankData() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(roomInfo.getUid()));
        requestParam.put("dataType", "2");
        requestParam.put("type", "1");
        String url = JAVA_WEB_URL + "/roomctrb/queryByType";
        OkHttpManager.getInstance().doGetRequest(url, requestParam, new OkHttpManager.MyCallBack<ServiceResult<List<RoomConsumeInfo>>>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "getData, onError： ", e);
            }

            @Override
            public void onResponse(ServiceResult<List<RoomConsumeInfo>> response) {
                L.info(TAG, "getData, onResponse: response: %s", response);
                if (getView() == null) {
                    return;
                }
                if (response != null) {
                    if (response.isSuccess()) {
                        List<RoomConsumeInfo> data = response.getData();
                        if (data != null) {
                            getView().updateRankView(data);
                        }
                    }
                }
            }
        });
    }
}
