package com.tongdaxing.erban.common.room.avroom.other;

/**
 * Created by chenran on 2017/11/21.
 */

public abstract class BottomViewListenerWrapper {
    public BottomViewListenerWrapper() {

    }

    public void onOpenMicBtnClick() {

    }

    public void onSendMsgBtnClick() {

    }

    public void onSendFaceBtnClick() {

    }

    public void onSendGiftBtnClick() {

    }

    public void onSendGiftHis() {

    }

    public void onRemoteMuteBtnClick() {

    }

//    public void onLotteryBoxeBtnClick() {
//    }


    public void onBuShowMicInList() {

    }

    public void onMsgBtnClick() {

    }

    public void onMoreBtnClick() {

    }
}
