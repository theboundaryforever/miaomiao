package com.tongdaxing.erban.common.room.operation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.erban.ui.adapter.BaseAdapterHelper;
import com.erban.ui.adapter.ListAdapter;
import com.erban.ui.mvp.MVPBaseDialogFragment;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.activity.BaseMvpActivity;
import com.tongdaxing.erban.common.room.avroom.activity.PkSettingActivity;
import com.tongdaxing.erban.common.room.avroom.activity.RoomSettingActivity;
import com.tongdaxing.erban.common.room.avroom.widget.dialog.BottomSetAudioDialog;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.setting.activity.FeedbackActivity;
import com.tongdaxing.erban.common.ui.web.LuckyWheelDialog;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RoomBottomMarkManager;
import com.tongdaxing.xchat_core.pk.IPkCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;


/**
 * Created by Chen on 2019/6/11.
 */
public class RoomBottomMoreDialog extends MVPBaseDialogFragment<IRoomBottomMoreView, RoomBottomMorePresenter> implements IRoomBottomMoreView {
    @BindView(R2.id.room_operation_gv_more)
    GridView mRoomOperationGvMore;

    private ListAdapter<BottomMoreItem> mMoreItemAdapter;
    private List<BottomMoreItem> mMoreItems = new ArrayList<>();

    private RoomBottomMarkManager mRoomBottomMarkManager;

    private int[] mItemResId = {
            R.drawable.room_operation_setting
            , R.drawable.room_operation_pk
            , R.drawable.room_operation_close_screen
            , R.drawable.room_operation_close_gift
            , R.drawable.room_operation_magic_wheel
            , R.drawable.room_operation_music
            , R.drawable.room_operation_problem_feedback
    };

    private String[] mItemName = {
            BaseApp.mBaseContext.getString(R.string.room_setting)
            , BaseApp.mBaseContext.getString(R.string.room_pk_open)
            , BaseApp.mBaseContext.getString(R.string.room_close_screen)
            , BaseApp.mBaseContext.getString(R.string.room_close_gift)
            , BaseApp.mBaseContext.getString(R.string.room_wheel_surf)
            , BaseApp.mBaseContext.getString(R.string.room_music)
            , BaseApp.mBaseContext.getString(R.string.room_question_report)
    };

    public static RoomBottomMoreDialog newInstance() {
        Bundle args = new Bundle();
        RoomBottomMoreDialog fragment = new RoomBottomMoreDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected RoomBottomMorePresenter createPresenter() {
        return new RoomBottomMorePresenter();
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            WindowManager.LayoutParams params = window.getAttributes();
            params.gravity = Gravity.BOTTOM; // 显示在底部
            params.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度填充满屏
            params.height = WindowManager.LayoutParams.WRAP_CONTENT; // 自适应
            window.setAttributes(params);
            // 这里用透明颜色替换掉系统自带背景
            int color = ContextCompat.getColor(BaseApp.mBaseContext, android.R.color.transparent);
            window.setBackgroundDrawable(new ColorDrawable(color));
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_operation_more_fragment_dialog;
    }

    @Override
    public void initBefore() {
    }

    @Override
    public void findView() {
        ButterKnife.bind(this, mContentView);
    }

    @Override
    public void setView() {
        mRoomBottomMarkManager = RoomBottomMarkManager.getInstance(getContext());

        if (mPresenter.isRoomOwner() || mPresenter.isRoomAdmin()) {
            initOwnerOrAdminItem();
        } else {
            initGeneralItem();
        }
        setAdapter();
    }

    private void initOwnerOrAdminItem() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        int length = mItemName.length;
        int size = mItemResId.length;
        for (int i = 0; i < length; i++) {
            if (i < size) {     // 防止有人只修改一个数组导致越界崩溃
                int index = i + 1;
                BottomMoreItem item;
                if (index == RoomOperationMoreType.ROOM_OPERATION_MORE_PK) {
                    AccountInfo accountInfo = CoreManager.getCore(IAuthCore.class).getCurrentAccount();
                    if (accountInfo != null) {
                        if (roomInfo.getTagType() == RoomInfo.ROOMTYPE_LOVERS || roomInfo.getTagType() == RoomInfo.ROOMTYPE_PERSONAL) {
                            // 个人房和情侣房不需要PK
                            continue;
                        }
                    }
                    item = getItem(AvRoomDataManager.get().isPkOpen() ? "取消PK" : mItemName[i], mItemResId[i], index);
                } else {
                    item = getItem(mItemName[i], mItemResId[i], index);
                }
                if (index == RoomOperationMoreType.ROOM_OPERATION_MORE_GIFT) {
                    int changeGiftEffect = roomInfo.getGiftEffectSwitch();
                    item.setIsShowGift(changeGiftEffect != 0);
                } else if (index == RoomOperationMoreType.ROOM_OPERATION_MORE_SCREEN) {
                    int publicChatSwitch = roomInfo.getPublicChatSwitch();
                    item.setIsShowScreen(publicChatSwitch != 0);
                }
                mMoreItems.add(item);
            }
        }
    }

    private void initGeneralItem() {
        mMoreItems.add(getItem(getString(R.string.room_wheel_surf), R.drawable.room_operation_magic_wheel, RoomOperationMoreType.ROOM_OPERATION_MORE_WHEEL));
        mMoreItems.add(getItem(getString(R.string.room_music), R.drawable.room_operation_music, RoomOperationMoreType.ROOM_OPERATION_MORE_MUSIC));
        mMoreItems.add(getItem(getString(R.string.room_report), R.drawable.room_operation_report, RoomOperationMoreType.ROOM_OPERATION_MORE_REPORT));
        mMoreItems.add(getItem(getString(R.string.room_question_report), R.drawable.room_operation_problem_feedback, RoomOperationMoreType.ROOM_OPERATION_MORE_QUESTION));
    }

    private BottomMoreItem getItem(String name, int resId, int type) {
        BottomMoreItem item = new BottomMoreItem();
        item.setItemName(name);
        item.setItemResId(resId);
        item.setItemType(type);
        return item;
    }

    private void setAdapter() {
        if (mMoreItemAdapter == null) {
            mMoreItemAdapter = new ListAdapter<BottomMoreItem>(getContext(), R.layout.room_operation_item_layout, mMoreItems) {
                @Override
                public void onUpdate(BaseAdapterHelper helper, BottomMoreItem item, int position) {
                    ImageView itemImage = helper.getView(R.id.room_operation_iv_more_item);
                    TextView itemName = helper.getView(R.id.room_operation_tv_item_name);
                    TextView itemTip = helper.getView(R.id.room_operation_tv_tip);

                    ImageLoadUtils.loadImage(getContext(), item.getItemResId(), itemImage);
                    itemName.setText(item.getItemName());

                    if (item.isShowGift()) {
                        ImageLoadUtils.loadImage(getContext(), R.drawable.room_operation_open_gift, itemImage);
                        itemName.setText(getString(R.string.room_show_gift));
                    }

                    if (item.isShowScreen()) {
                        ImageLoadUtils.loadImage(getContext(), R.drawable.room_operation_open_screen, itemImage);
                        itemName.setText(getString(R.string.room_open_screen));
                    }

                    switch (item.getItemType()) {
                        case RoomOperationMoreType.ROOM_OPERATION_MORE_SETTING:
                            itemTip.setVisibility(mRoomBottomMarkManager.isButtonMark(RoomBottomMarkManager.BUTTON_TYPE_ROOM_SETTING) ? View.VISIBLE : View.GONE);
                            break;
                        case RoomOperationMoreType.ROOM_OPERATION_MORE_PK:
                            itemTip.setVisibility(mRoomBottomMarkManager.isButtonMark(RoomBottomMarkManager.BUTTON_TYPE_OPEN_PK) ? View.VISIBLE : View.GONE);
                            break;
                        case RoomOperationMoreType.ROOM_OPERATION_MORE_WHEEL:
                            itemTip.setVisibility(mRoomBottomMarkManager.isButtonMark(RoomBottomMarkManager.BUTTON_TYPE_WHEEL_SURF) ? View.VISIBLE : View.GONE);
                            break;
                        case RoomOperationMoreType.ROOM_OPERATION_MORE_QUESTION:
                            itemTip.setVisibility(mRoomBottomMarkManager.isButtonMark(RoomBottomMarkManager.BUTTON_TYPE_QUESTION_REPORT) ? View.VISIBLE : View.GONE);
                            break;
                        default:
                            itemTip.setVisibility(View.GONE);
                            break;
                    }
                }
            };
            mRoomOperationGvMore.setAdapter(mMoreItemAdapter);
        } else {
            mMoreItemAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void setListener() {

    }

    @OnItemClick(R2.id.room_operation_gv_more)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Context context = getContext();
        if (isActivityDestroyed(context)) {
            return;
        }
        int size = mMoreItems.size();
        if (position < size) {
            BottomMoreItem item = mMoreItems.get(position);
            switch (item.getItemType()) {
                case RoomOperationMoreType.ROOM_OPERATION_MORE_SETTING:
                    showSetting();
                    break;
                case RoomOperationMoreType.ROOM_OPERATION_MORE_PK:
                    showPk();
                    break;
                case RoomOperationMoreType.ROOM_OPERATION_MORE_SCREEN:
                    doScreen();
                    break;
                case RoomOperationMoreType.ROOM_OPERATION_MORE_GIFT:
                    doGift();
                    break;
                case RoomOperationMoreType.ROOM_OPERATION_MORE_WHEEL:
                    showWheel(context);
                    break;
                case RoomOperationMoreType.ROOM_OPERATION_MORE_MUSIC:
                    showMusic();
                    break;
                case RoomOperationMoreType.ROOM_OPERATION_MORE_QUESTION:
                    doQuestion();
                    break;
                case RoomOperationMoreType.ROOM_OPERATION_MORE_REPORT:
                    doReport();
                    break;
            }
        }
        dismissView();
    }

    private void showSetting() {
        mRoomBottomMarkManager.clearMark(RoomBottomMarkManager.BUTTON_TYPE_ROOM_SETTING);
        int isPermitRoom = 2;
        if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
            isPermitRoom = AvRoomDataManager.get().mCurrentRoomInfo.getIsPermitRoom();
        }
        RoomSettingActivity.start(getContext(), AvRoomDataManager.get().mCurrentRoomInfo, isPermitRoom);
    }

    private void doReport() {
        List<ButtonItem> buttons = new ArrayList<>();
        ButtonItem button1 = new ButtonItem("政治敏感", () -> mPresenter.reportCommit(1));
        ButtonItem button2 = new ButtonItem("色情低俗", () -> mPresenter.reportCommit(2));
        ButtonItem button3 = new ButtonItem("广告骚扰", () -> mPresenter.reportCommit(3));
        ButtonItem button4 = new ButtonItem("人身攻击", () -> mPresenter.reportCommit(4));
        buttons.add(button1);
        buttons.add(button2);
        buttons.add(button3);
        buttons.add(button4);
        DialogManager dialogManager = null;

        Context context = getContext();
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.isFinishing() || activity.isDestroyed()) {
                return;
            }
            if (context instanceof BaseMvpActivity) {
                dialogManager = ((BaseMvpActivity) context).getDialogManager();
            } else if (context instanceof BaseActivity) {
                dialogManager = ((BaseActivity) context).getDialogManager();
            }
            if (dialogManager != null) {
                dialogManager.showCommonPopupDialog(buttons, "取消");
            }
        }
    }

    private void doQuestion() {
        mRoomBottomMarkManager.clearMark(RoomBottomMarkManager.BUTTON_TYPE_QUESTION_REPORT);
        startActivity(new Intent(getContext(), FeedbackActivity.class));
    }

    private void doGift() {
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo != null) {
            int giftEffectSwitch = mCurrentRoomInfo.getGiftEffectSwitch();
            if (giftEffectSwitch == 0) {
                giftEffectSwitch = 1;
            } else {
                giftEffectSwitch = 0;
            }
            mPresenter.setGiftShow(giftEffectSwitch);
        }
    }

    private void doScreen() {
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo != null) {
            int publicChatSwitch = mCurrentRoomInfo.getPublicChatSwitch();
            mPresenter.setScreenShow(publicChatSwitch == 0);
        }
    }

    private void showMusic() {
        BottomSetAudioDialog setAudioDialog = new BottomSetAudioDialog(getActivity());
        setAudioDialog.show();
    }

    private void showPk() {
        mRoomBottomMarkManager.clearMark(RoomBottomMarkManager.BUTTON_TYPE_OPEN_PK);
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        AccountInfo accountInfo = CoreManager.getCore(IAuthCore.class).getCurrentAccount();
        if (roomInfo != null && accountInfo != null) {
            if (roomInfo.getTagType() != RoomInfo.ROOMTYPE_LOVERS && roomInfo.getTagType() != RoomInfo.ROOMTYPE_PERSONAL) {
                if (AvRoomDataManager.get().isPkOpen()) {
                    CoreManager.getCore(IPkCore.class).cancelPK(roomInfo.getRoomId(), accountInfo.getUid());
                } else {
                    startActivity(new Intent(getContext(), PkSettingActivity.class));
                }
            }
        }
    }

    private void showWheel(Context context) {
        mRoomBottomMarkManager.clearMark(RoomBottomMarkManager.BUTTON_TYPE_WHEEL_SURF);
        DialogManager mDialogManager = new DialogManager(context);
        mDialogManager.setCanceledOnClickOutside(false);
        mDialogManager.showProgressDialog(context, "加载中...");
        new LuckyWheelDialog(context, new LuckyWheelDialog.Callback() {
            @Override
            public void succed(LuckyWheelDialog dialog) {
                if (isActivityDestroyed(context)) {
                    return;
                }
                dialog.show();
                mDialogManager.dismissDialog();
            }

            @Override
            public void failed(LuckyWheelDialog dialog) {
                if (isActivityDestroyed(context)) {
                    return;
                }
                mDialogManager.dismissDialog();
            }
        });
    }

    @Override
    public void dismissView() {
        this.dismissAllowingStateLoss();
    }

    private boolean isActivityDestroyed(Context context) {
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.isFinishing() || activity.isDestroyed()) {
                return true;
            }
        }
        return false;
    }
}
