package com.tongdaxing.erban.common.ui.me.wallet.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.tongdaxing.erban.common.R;

/**
 * Created by chenran on 2017/11/15.
 */

public class ChargeDialog extends Dialog implements View.OnClickListener {
    private int gold;
    private TextView goldText;
    private Button btnIKnow;

    public ChargeDialog(@NonNull Context context, int gold) {
        super(context, R.style.dialog);
        this.gold = gold;
    }

    public ChargeDialog(@NonNull Context context, @StyleRes int themeResId, int gold) {
        super(context, R.style.dialog);
        this.gold = gold;
    }

    protected ChargeDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.charge_dialog);
        initView();
        setListener();
    }

    private void setListener() {
        btnIKnow.setOnClickListener(this);
    }

    private void initView() {
        goldText = (TextView) findViewById(R.id.gold_text);
        btnIKnow = (Button) findViewById(R.id.btn_charge);
        goldText.setText(gold + "金币");
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
