package com.tongdaxing.erban.common.ui.find.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.xchat_core.find.bean.NoticeIMBean;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

public class NoticeIMAdapter extends BaseQuickAdapter<NoticeIMBean, BaseViewHolder> {
    public NoticeIMAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, NoticeIMBean item) {
        ImageView ivUserHead = helper.getView(R.id.iv_user_head);
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), ivUserHead, R.drawable.default_cover);
        helper.setText(R.id.tv_user_name, item.getNick());
        int contentResId = R.string.im_notification_follow;
        switch (item.getType()) {
            case NoticeIMBean.TYPE_FOLLOW:
                contentResId = R.string.im_notification_follow;
                break;
            case NoticeIMBean.TYPE_AUDIO_MATCH_LIKE:
                contentResId = R.string.im_notification_like;
                break;
            case NoticeIMBean.TYPE_NOTICE_FANS:
                contentResId = R.string.im_notification_fans;
                break;
        }
        helper.setText(R.id.tv_content, contentResId);
        helper.setText(R.id.tv_time, TimeUtils.getPostTimeString(mContext,
                item.getCreateTime(), true, false));
    }
}
