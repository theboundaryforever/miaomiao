package com.tongdaxing.erban.common.ui.find.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.juxiao.library_ui.widget.NestedScrollSwipeRefreshLayout;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.base.fragment.BaseMvpFragment;
import com.tongdaxing.erban.common.presenter.find.WarmAccompanyPresenter;
import com.tongdaxing.erban.common.ui.find.adapter.WarmAccompanyAdapter;
import com.tongdaxing.erban.common.ui.find.view.IWarmAccompanyView;
import com.tongdaxing.erban.common.ui.home.fragment.HotTagFragment;
import com.tongdaxing.erban.common.ui.widget.itemdecotion.GridSpaceItemDecoration;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.find.bean.WarmAccompanyInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Function:
 * Author: Edward on 2019/3/17
 */
@CreatePresenter(WarmAccompanyPresenter.class)
public class WarmAccompanyFragment extends BaseMvpFragment<IWarmAccompanyView, WarmAccompanyPresenter> implements IWarmAccompanyView {
    @BindView(R2.id.swipe_refresh)
    NestedScrollSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R2.id.recycler_view)
    RecyclerView recyclerView;
    private WarmAccompanyAdapter warmAccompanyAdapter;
    private int genderId = 0;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_warm_accompany;
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        showLoading();
        getMvpPresenter().refreshData(genderId);
    }

    @Override
    public void setupFailView(boolean isRefresh) {
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            showNetworkErr();
        } else {
            warmAccompanyAdapter.loadMoreFail();
        }
    }

    @Override
    public void onFindViews() {
        recyclerView = mView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        warmAccompanyAdapter = new WarmAccompanyAdapter(R.layout.warm_accompany_room_list_item, genderId);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), HotTagFragment.ITEM_COUNT_BY_ROW));
        recyclerView.setAdapter(warmAccompanyAdapter);
        int itemMargin = getResources().getDimensionPixelSize(R.dimen.find_warm_item_margin);//每一行间距
        GridSpaceItemDecoration decoration = new GridSpaceItemDecoration(0, itemMargin);
        decoration.setupBindAdapter(warmAccompanyAdapter);
        recyclerView.addItemDecoration(decoration, 0);
    }

    @Override
    public void setupSuccessView(List<WarmAccompanyInfo> voiceGroupInfoList, boolean isRefresh) {
        hideStatus();
        if (isRefresh) {
            mSwipeRefreshLayout.setRefreshing(false);
            if (ListUtils.isListEmpty(voiceGroupInfoList)) {
                showNoData();
                voiceGroupInfoList = new ArrayList<>();
            }
            warmAccompanyAdapter.setNewData(voiceGroupInfoList);
        } else {
            warmAccompanyAdapter.loadMoreComplete();
            if (!ListUtils.isListEmpty(voiceGroupInfoList)) {
                warmAccompanyAdapter.addData(voiceGroupInfoList);
            }
        }
        //不够10个的话，不显示加载更多
        if (voiceGroupInfoList.size() < Constants.PAGE_SIZE) {
            warmAccompanyAdapter.setEnableLoadMore(false);
        }
    }

    @Override
    public void onSetListener() {
        mSwipeRefreshLayout.setOnRefreshListener(() -> getMvpPresenter().refreshData(genderId));
        warmAccompanyAdapter.setOnLoadMoreListener(() -> {
            getMvpPresenter().loadMoreData(genderId);
        }, recyclerView);
    }

    @Override
    public void initiate() {
        Bundle bundle = getArguments();
        if (bundle == null) {
            showPageError(R.string.data_exception);
            return;
        }

        genderId = bundle.getInt("data");
        onReloadData();//进来加载一次
    }

}
