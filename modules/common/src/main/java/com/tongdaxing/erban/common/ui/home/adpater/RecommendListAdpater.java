package com.tongdaxing.erban.common.ui.home.adpater;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.blur.BlurTransformation;
import com.tongdaxing.xchat_core.home.HomeRoom;

public class RecommendListAdpater extends BaseQuickAdapter<HomeRoom, BaseViewHolder> {
    private Context context;

    public RecommendListAdpater(Context context) {
        super(R.layout.item_hot_room_list_more);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, HomeRoom homeRoom) {
        if (homeRoom == null) {
            return;
        }
//        baseViewHolder.setGone(R.id.iv_popular_room_state,homeRoom.getSeqNo() > 0 ? true : false);
        baseViewHolder.setText(R.id.tv_hot_room_name, homeRoom.title);
        baseViewHolder.setText(R.id.tv_hot_room_count, homeRoom.onlineNum + "人");
        ImageView ivBg = baseViewHolder.getView(R.id.iv_hot_room_bg);
        ImageView ivIcon = baseViewHolder.getView(R.id.iv_hot_room_avatar);
        GlideApp.with(context)
                .load(homeRoom.getAvatar())
                .transform(new BlurTransformation(context, 23, 4))
                .into(ivBg);
        ImageLoadUtils.loadCircleImage(context, homeRoom.getAvatar(), ivIcon, R.drawable.ic_no_avatar);
        baseViewHolder.getView(R.id.rl_popular_room).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AVRoomActivity.start(context, homeRoom.getUid());
            }
        });
    }
}
