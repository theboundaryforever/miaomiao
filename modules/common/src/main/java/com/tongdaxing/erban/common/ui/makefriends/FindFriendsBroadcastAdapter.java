package com.tongdaxing.erban.common.ui.makefriends;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.common.utils.NewAndOldUserEventUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.xchat_core.im.custom.bean.FriendsBroadcastAttachment;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;

import java.util.List;

/**
 * <p> 寻友广播adapter </p>
 */
public class FindFriendsBroadcastAdapter extends BaseQuickAdapter<FriendsBroadcastAttachment, BaseViewHolder> {

    public FindFriendsBroadcastAdapter(int layoutRes, List<FriendsBroadcastAttachment> beans) {
        super(layoutRes, beans);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, FriendsBroadcastAttachment broadcastBean) {
        if (broadcastBean == null) {
            return;
        }
        ImageView itemBg = viewHolder.getView(R.id.find_friends_broadcast_item_bg);
        ImageView itemBottomBg = viewHolder.getView(R.id.find_friends_broadcast_item_bottom_bg);
        TextView onlineNumTv = viewHolder.getView(R.id.find_friends_broadcast_item_online_num);
        TextView roomTagTv = viewHolder.getView(R.id.find_friends_broadcast_item_tag);
        TextView roomUserNickTv = viewHolder.getView(R.id.find_friends_broadcast_item_room_title);
        if (broadcastBean.isBlueBg()) {
            itemBg.setImageResource(R.drawable.find_new_make_friends_bg_blue);
            itemBottomBg.setImageResource(R.drawable.find_bg_blue_bottom);
            onlineNumTv.setBackgroundResource(R.drawable.find_friends_broadcast_item_online_blue_bg);
            roomTagTv.setBackgroundResource(R.drawable.item_broadcast_room_tag_bg);
            roomUserNickTv.setTextColor(mContext.getResources().getColor(R.color.item_find_friends_broadcast_blue_text));
        } else {
            itemBg.setImageResource(R.drawable.find_bg_yellow);
            itemBottomBg.setImageResource(R.drawable.find_bg_yellow_bottom);
            onlineNumTv.setBackgroundResource(R.drawable.find_friends_broadcast_item_online_yellow_bg);
            roomTagTv.setBackgroundResource(R.drawable.item_broadcast_room_tag_yellow_bg);
            roomUserNickTv.setTextColor(mContext.getResources().getColor(R.color.item_find_friends_broadcast_yellow_text));
        }
        onlineNumTv.setText(String.valueOf(broadcastBean.getOnlineNum()).concat(" 人在玩"));
        viewHolder.setText(R.id.find_friends_broadcast_item_content, broadcastBean.getContent());
        roomTagTv.setText(broadcastBean.getRoomTag());
        roomUserNickTv.setText(broadcastBean.getUserNick());
        ImageLoadUtils.loadAvatar(mContext, broadcastBean.getAvatar(), viewHolder.getView(R.id.find_friends_broadcast_item_avatar), true);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (broadcastBean.getRoomUid() > 0) {
                    NewAndOldUserEventUtils.onCommonTypeEvent(mContext, UmengEventId.getFindBroadcast());
                    AVRoomActivity.start(mContext, broadcastBean.getRoomUid());
                }
            }
        });
    }
}
