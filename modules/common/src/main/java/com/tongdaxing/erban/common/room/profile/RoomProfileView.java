package com.tongdaxing.erban.common.room.profile;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.mvp.MVPBaseFrameLayout;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tcloud.core.log.L;
import com.tcloud.core.util.DensityUtil;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.R2;
import com.tongdaxing.erban.common.im.actions.ChargeDialogFragment;
import com.tongdaxing.erban.common.room.AVRoomActivity;
import com.tongdaxing.erban.common.room.avroom.activity.RoomSettingActivity;
import com.tongdaxing.erban.common.room.avroom.other.ButtonItemFactory;
import com.tongdaxing.erban.common.room.face.anim.AnimFaceFactory;
import com.tongdaxing.erban.common.room.gift.GiftDialog;
import com.tongdaxing.erban.common.room.user.UserInfoDialogManager;
import com.tongdaxing.erban.common.room.widget.dialog.ListDataDialog;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.makefriends.FindFriendsBroadCastRoute;
import com.tongdaxing.erban.common.ui.me.wallet.activity.MyWalletNewActivity;
import com.tongdaxing.erban.common.ui.widget.WaveView;
import com.tongdaxing.erban.common.ui.widget.dialog.shareDialog.NewShareDialog;
import com.tongdaxing.erban.common.ui.widget.magicindicator.buildins.UIUtil;
import com.tongdaxing.erban.common.utils.ImageLoadUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.framework.Platform;

/**
 * Created by Chen on 2019/6/5.
 */
public class RoomProfileView extends MVPBaseFrameLayout<IRoomProfileView, RoomProfilePresenter> implements IRoomProfileView, GiftDialog.OnGiftDialogBtnClickListener, ChargeListener {
    private String TAG = "RoomProfileView";
    @BindView(R2.id.room_profile_iv_owner)
    ImageView mRoomProfileIvOwner;
    @BindView(R2.id.room_profile_iv_head)
    ImageView mRoomProfileIHead;
    @BindView(R2.id.room_profile_tv_state)
    ImageView mRoomProfileTvState;
    @BindView(R2.id.room_profile_fl_head)
    FrameLayout mRoomProfileFlOwnerLayout;
    @BindView(R2.id.room_profile_tv_name)
    TextView mRoomProfileTvName;
    @BindView(R2.id.room_profile_tv_id_and_online)
    TextView mRoomProfileTvIdAndOnline;
    @BindView(R2.id.room_profile_iv_attention)
    ImageView mRoomProfileIvAttention;
    @BindView(R2.id.room_profile_iv_close)
    ImageView mRoomProfileIvClose;
    @BindView(R2.id.room_profile_iv_share)
    ImageView roomProfileIvShare;
    @BindView(R2.id.room_profile_iv_lock)
    ImageView mRoomProfileIvLock;
    @BindView(R2.id.room_profile_iv_cp_level)
    ImageView mRoomProfileIvCpLevel;
    @BindView(R2.id.room_profile_wv_speak)
    WaveView mRoomProfileWvSpeak;

    private DialogManager mDialogManager;

    private SparseArray<ImageView> faceImageViews;

    private int giftWidth;
    private int giftHeight;
    private int faceWidth;
    private int faceHeight;
    private long mRoomId = 0;
    private long mUserId = 0;

    public RoomProfileView(@NonNull Context context) {
        super(context);
    }

    public RoomProfileView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RoomProfileView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_profile_view;
    }

    @NonNull
    @Override
    protected RoomProfilePresenter createPresenter() {
        return new RoomProfilePresenter();
    }

    @Override
    protected void findView() {
        ButterKnife.bind(this);
    }

    @Override
    protected void setView() {
        if (mDialogManager == null) {
            mDialogManager = new DialogManager(getContext());
        }
        // 礼物位置，表情位置
        giftWidth = UIUtil.dip2px(getContext(), 80);
        giftHeight = UIUtil.dip2px(getContext(), 80);
        faceWidth = UIUtil.dip2px(getContext(), 60);
        faceHeight = UIUtil.dip2px(getContext(), 60);

        faceImageViews = new SparseArray<>();

        mRoomProfileIvOwner.postDelayed(new Runnable() {
            @Override
            public void run() {
                setMicCenterPoint();
            }
        }, 500);

        setRoomOwnerView();
        initWaveView();
    }

    private void initWaveView() {
        mRoomProfileWvSpeak.setInitialRadius(DensityUtil.dip2px(getContext(), 24.0f));
        mRoomProfileWvSpeak.setMaxRadius(DensityUtil.dip2px(getContext(), 32.0f));
        mRoomProfileWvSpeak.setInitialAlpha(0.7f);
        mRoomProfileWvSpeak.setSpeed(800);
        mRoomProfileWvSpeak.setColor(getResources().getColor(R.color.white));
    }

    private void setRoomOwnerView() {
        boolean roomOwner = mPresenter.isRoomOwner();
        if (roomOwner) {
            mRoomProfileIvAttention.setVisibility(GONE);
        } else {
            mRoomProfileIvAttention.setVisibility(VISIBLE);
        }
        // 设置房间信息

        UserInfo ownerInfo = mPresenter.getRoomOwner();
        RoomInfo roomInfo = mPresenter.getRoomInfo();
        L.debug(TAG, "setView ownerInfo: %s", ownerInfo);
        if (ownerInfo != null) {
            // 房间标题
            String name = ownerInfo.getNick();
            if (!TextUtils.isEmpty(name)) {
                mRoomProfileTvName.setText(name);
            } else {
                mRoomProfileTvName.setText("");
            }
        }

        L.debug(TAG, "setView roomInfo: %s", roomInfo);
        if (roomInfo != null) {
            // 房间id
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(roomInfo.getUid());
            if (userInfo != null) {
                mRoomId = userInfo.getErbanNo();
                mUserId = userInfo.getUid();
            }
            // 房间密码
            String roomPwd = roomInfo.getRoomPwd();
            if (TextUtils.isEmpty(roomPwd)) {
                mRoomProfileIvLock.setVisibility(GONE);
            } else {
                mRoomProfileIvLock.setVisibility(VISIBLE);
            }
            // 在线人数
            int number = roomInfo.onlineNum + roomInfo.getFactor();
            setOnlineText(number);
        }
    }

    private void setOnlineText(int number) {
        String online = String.format(getResources().getString(R.string.room_profile_online), mRoomId, number);
        mRoomProfileTvIdAndOnline.setText(online);
    }

    public void setMicCenterPoint() {
        SparseArray<Point> centerPoints = new SparseArray<>();

        centerPoints.put(-1, getPoint(-1));
        AvRoomDataManager.get().mMicPointMap = centerPoints;
    }

    private Point getPoint(int position) {
        int[] location = new int[2];
        int[] nameLocation = new int[2];
        // 找到头像
        mRoomProfileFlOwnerLayout.getLocationInWindow(location);
        mRoomProfileFlOwnerLayout.getLocationInWindow(nameLocation);
        int x = (location[0] + mRoomProfileFlOwnerLayout.getWidth() / 2) - giftWidth / 2;
        int y = location[1] >= nameLocation[1] ?
                ((location[1] + mRoomProfileFlOwnerLayout.getHeight() * 7 / 8) - giftHeight / 2) :
                ((nameLocation[1] + mRoomProfileFlOwnerLayout.getHeight() / 2) - giftHeight / 2);

        // 放置表情占位image view
        if (faceImageViews.get(position) == null) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(faceWidth, faceHeight);
            mRoomProfileFlOwnerLayout.getLocationInWindow(location);
            int[] containerLocation = new int[2];
            this.getLocationInWindow(containerLocation);
            params.leftMargin = ((location[0] - containerLocation[0] + mRoomProfileFlOwnerLayout.getWidth() / 2) - faceWidth / 2);
            params.topMargin = ((location[1] - containerLocation[1] + mRoomProfileFlOwnerLayout.getHeight() / 2) - faceHeight / 2);
            L.debug(TAG, "function:sendFace position = %d, params.leftMargin = %d, params.topMargin = %d", position, params.leftMargin, params.topMargin);
            ImageView imageView = new ImageView(getContext());
            imageView.setLayoutParams(params);
            faceImageViews.put(position, imageView);
            addView(imageView);
        }
        return new Point(x, y);
    }

    @Override
    protected void setListener() {

    }

    @OnClick(R2.id.room_profile_fl_head)
    public void onOwnerHeadClicked() {
        mPresenter.microPhonePositionClick();
    }

    @OnClick(R2.id.room_profile_tv_id_and_online)
    public void onOnlinePlayerClicked() {
        Context context = getContext();
        if (context instanceof FragmentActivity) {
            FragmentActivity activity = (FragmentActivity) context;
            ListDataDialog.newOnlineUserListInstance(context).show(activity.getSupportFragmentManager());
        }
    }

    @OnClick(R2.id.room_profile_iv_attention)
    public void onAttentionClicked() {
        mPresenter.doAttention();
    }

    @OnClick(R2.id.room_profile_iv_close)
    public void onCloseClicked() {
        List<ButtonItem> buttonItems = new ArrayList<>();
        ButtonItem buttonItem1 = new ButtonItem("最小化", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                AvRoomDataManager.get().setMinimize(true);
                Context context = getContext();
                ((Activity) context).finish();
            }
        });
        ButtonItem buttonItem2 = new ButtonItem("退出房间", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                Context context = getContext();
                if (context instanceof AVRoomActivity) {
                    ((AVRoomActivity) context).exitRoom();
                }
            }
        });

        ButtonItem buttonItem3 = new ButtonItem("房间设置", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                int isPermitRoom = 2;
                if (AvRoomDataManager.get().mCurrentRoomInfo != null)
                    isPermitRoom = AvRoomDataManager.get().mCurrentRoomInfo.getIsPermitRoom();
                RoomSettingActivity.start(getContext(), AvRoomDataManager.get().mCurrentRoomInfo, isPermitRoom);
            }
        });

        if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
            buttonItems.add(buttonItem3);
            final RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (mCurrentRoomInfo != null) {
                int publicChatSwitch = mCurrentRoomInfo.getPublicChatSwitch();
                buttonItems.add(ButtonItemFactory.createPublicSwitch(publicChatSwitch));
            }
        }
        if (!AvRoomDataManager.get().isRoomOwner()) {
            buttonItems.add(ButtonItemFactory.createReportItem(getContext(), "举报房间", 2));
        }
        buttonItems.add(buttonItem1);
        buttonItems.add(buttonItem2);

        DialogManager dialogManager = new DialogManager(getContext());
        dialogManager.showCommonPopupDialog(buttonItems, "取消");
    }

    @OnClick(R2.id.room_profile_iv_share)
    public void onShareClicked() {
        if (isActivityDestroyed(getContext())) {
            return;
        }
        NewShareDialog shareDialog = new NewShareDialog(getContext(), true);
        shareDialog.setHasBroadCast(true);
        shareDialog.setHasFriend(true);//邀请好友分享
        if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
            shareDialog.setHasNoticeFans(true);
        }
        shareDialog.setOnShareDialogItemClick(new NewShareDialog.OnShareDialogItemClick() {
            @Override
            public void onSharePlatformClick(Platform platform) {
                RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (currentRoomInfo != null) {
                    CoreManager.getCore(IShareCore.class).shareRoom(platform, currentRoomInfo.getUid(), currentRoomInfo.getTitle());
                }
            }

            @Override
            public void onSendBroadcastMsg() {
                //发送寻友广播
                DialogFragment dialogFragment = (DialogFragment) ARouter.getInstance().build(FindFriendsBroadCastRoute.SEND_DIALOG)
                        .navigation(getContext());
                if (dialogFragment.isAdded()) {
                    if (dialogFragment.isStateSaved()) {
                        dialogFragment.dismissAllowingStateLoss();
                    }
                } else {
                    FragmentManager fragmentManager = ((FragmentActivity) getContext()).getSupportFragmentManager();
                    dialogFragment.show(fragmentManager, "RoomBroadcastSendDialog");
                }

            }
        });
        shareDialog.show();
    }

    @Override
    public void onRechargeBtnClick() {
        MyWalletNewActivity.start(getContext());
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number, int sendGiftType) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) return;
        if (giftInfo == null)
            return;
        List<Long> uids = new ArrayList<>();
        uids.add(uid);
        CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), uids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number, int sendGiftType) {
        if (giftInfo == null) return;
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) return;
        List<Long> targetUids = new ArrayList<>();
        for (int i = 0; i < micMemberInfos.size(); i++) {
            targetUids.add(micMemberInfos.get(i).getUid());
        }
        CoreManager.getCore(IGiftCore.class).sendGiftWithGold(giftInfo.getGiftId(), targetUids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice(), sendGiftType);
    }

    @Override
    public void onSendCpGiftBtnClick(GiftInfo giftInfo, long uid) {
        if (giftInfo != null) {
            RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (currentRoomInfo == null) return;
            CoreManager.getCore(IGiftCore.class).sendPersonalCpGift(giftInfo.getGiftId(), uid,
                    1, currentRoomInfo.getUid(), this);
        }
    }

    @Override
    public void onNeedCharge() {
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    MyWalletNewActivity.start(getContext());
                    fragment.dismiss();
                }
            }
        }).show(((FragmentActivity) getContext()).getSupportFragmentManager(), "charge");
    }

    @Override
    public void updateChairView(SparseArray<RoomQueueInfo> roomQueueInfo) {
        int size = roomQueueInfo.size();
        if (size > 0) {
            RoomQueueInfo queueInfo = roomQueueInfo.valueAt(0);
            ChatRoomMember chatRoomMember = queueInfo.mChatRoomMember;
            if (chatRoomMember != null) {
                mRoomProfileTvState.setVisibility(View.GONE);
            } else {
                mRoomProfileTvState.setVisibility(View.VISIBLE);
            }

            if (queueInfo.isShowCpIcon) {
                mRoomProfileIvCpLevel.setVisibility(View.VISIBLE);
                if (queueInfo.cpGiftLevel == 2) {
                    mRoomProfileIvCpLevel.setImageResource(R.drawable.ic_room_cp_level_2);
                } else if (queueInfo.cpGiftLevel == 3) {
                    mRoomProfileIvCpLevel.setImageResource(R.drawable.ic_room_cp_level_3);
                } else {//只要isShowCpIcon为true就要显示cp标识，一周cp的cpGiftLevel为0，所以这里其他情况都显示1级标识
                    mRoomProfileIvCpLevel.setImageResource(R.drawable.ic_room_cp_level_1);
                }
            } else {
                mRoomProfileIvCpLevel.setVisibility(View.GONE);
            }

            //--------头饰-----------
            if (chatRoomMember != null) {
                Map<String, Object> extension = chatRoomMember.getExtension();
                String headwearUrl = "";
                if (extension != null && extension.get("headwearUrl") != null) {
                    try {
                        headwearUrl = (String) extension.get("headwearUrl");
                    } catch (Exception e) {
                        L.error(TAG, "updateChairView exception:", e);
                    }
                }
                if (!TextUtils.isEmpty(headwearUrl)) {
                    ImageLoadUtils.loadImage(getContext(), headwearUrl, mRoomProfileIHead);
                    mRoomProfileIHead.setVisibility(VISIBLE);
                } else {
                    mRoomProfileIHead.setVisibility(GONE);
                }
            } else {
                mRoomProfileIHead.setVisibility(GONE);
            }
            //--------头像-----------
            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            ImageLoadUtils.loadImage(getContext(), R.drawable.ic_no_avatar, mRoomProfileIvOwner);
            if (roomInfo != null) {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(roomInfo.getUid());
                if (userInfo != null) {
                    String avatar = userInfo.getAvatar();
                    L.debug(TAG, "avatar: %s", avatar);
                    if (!TextUtils.isEmpty(avatar)) {
                        ImageLoadUtils.loadCircleImage(getContext(), avatar, mRoomProfileIvOwner, R.drawable.ic_no_avatar);
                    }
                }
            }
        }
    }

    @Override
    public void showOwnerSelfInfo(ChatRoomMember chatRoomMember) {
        if (isActivityDestroyed(getContext()) || chatRoomMember == null) {
            return;
        }
        UserInfoDialogManager.showDialogFragment(getContext(), JavaUtil.str2long(chatRoomMember.getAccount()));
    }

    @Override
    public void showGiftDialog(ChatRoomMember chatRoomMember) {
        if (isActivityDestroyed(getContext())) {
            return;
        }
        GiftDialog giftDialog = new GiftDialog(getContext(), AvRoomDataManager.get().mMicQueueMemberMap, chatRoomMember);
        giftDialog.setGiftDialogBtnClickListener(this);
        giftDialog.show();
    }

    @Override
    public void updateSpeakState(boolean state) {
        if (mRoomProfileWvSpeak != null) {
            if (state) {
                mRoomProfileWvSpeak.start();
            } else {
                mRoomProfileWvSpeak.stop();
            }
        }
    }

    @Override
    public void onReceiveFace(List<FaceReceiveInfo> faceReceiveInfos) {
        L.debug(TAG, "function:sendFace RoomLoverChairView onReceiveFace faceReceiveInfos: %s", faceReceiveInfos);
        if (faceReceiveInfos == null || faceReceiveInfos.size() <= 0) return;
        for (FaceReceiveInfo faceReceiveInfo : faceReceiveInfos) {
            int position = AvRoomDataManager.get().getMicPosition(faceReceiveInfo.getUid());
            if (position < -1) continue;
            ImageView imageView = faceImageViews.get(position);
            if (imageView == null) {
                continue;
            }
            L.debug(TAG, "function:sendFace position = %d, imageView.getX() = %f, imageView.getY() = %f, imageView.getWidth() = %d, imageView.getHeight() = %d",
                    position, imageView.getX(), imageView.getY(), imageView.getWidth(), imageView.getHeight());
            AnimationDrawable drawable = AnimFaceFactory.get(faceReceiveInfo, getContext(), imageView.getWidth(), imageView.getHeight());
            if (drawable == null) {
                continue;
            }
            imageView.setImageDrawable(drawable);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            drawable.setOneShot(true);
            drawable.start();
        }
    }

    @Override
    public void attentionSuccess(long uid, boolean isAttention) {
        if (isAttention) {
            if (uid > 0 && uid == mUserId) {
                mRoomProfileIvAttention.setVisibility(GONE);
            }
        } else {
            mRoomProfileIvAttention.setVisibility(VISIBLE);
        }

    }

    @Override
    public void attentionFailed(String message) {
        SingleToastUtil.showToast(getContext(), message);
    }

    @Override
    public void attentionCancel(long uid) {
        ChatRoomMember createMember = AvRoomDataManager.get().mRoomCreateMember;
        if (createMember != null) {
            String account = createMember.getAccount();
            if (Objects.equals(String.valueOf(uid), account)) {
                mRoomProfileIvAttention.setVisibility(VISIBLE);
            }
        }
    }

    @Override
    public void updateOnlineUserNum(int onlineUserNum) {
        setOnlineText(onlineUserNum);
    }

    @Override
    public void updateProfileView() {
        setRoomOwnerView();
    }

    private static boolean isActivityDestroyed(Context context) {
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.isFinishing() || activity.isDestroyed()) {
                return true;
            }
        }
        return false;
    }
}
