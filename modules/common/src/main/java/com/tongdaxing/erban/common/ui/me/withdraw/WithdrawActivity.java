package com.tongdaxing.erban.common.ui.me.withdraw;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.common.R;
import com.tongdaxing.erban.common.base.activity.BaseActivity;
import com.tongdaxing.erban.common.base.view.TitleBar;
import com.tongdaxing.erban.common.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.common.ui.me.wallet.adapter.WithdrawJewelAdapter;
import com.tongdaxing.erban.common.ui.widget.dialog.CommonTitleDialog;
import com.tongdaxing.erban.common.ui.widget.dialog.WithdrawMannerDialog;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCoreClient;
import com.tongdaxing.xchat_core.withdraw.bean.ExchangerInfo;
import com.tongdaxing.xchat_core.withdraw.bean.RefreshInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrwaListInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WxVerifyInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * 钻石提现
 */
public class WithdrawActivity extends BaseActivity {
    public WithdrwaListInfo checkedPosition;
    private TitleBar mTitleBar;
    private TextView alipayAccount;
    private TextView alipayName;
    private TextView diamondNumWithdraw;
    private FrameLayout binder;
    private RelativeLayout binderSucceed;
    private RecyclerView mRecyclerView;
    private Button btnWithdraw;
    private Button unBtnWithdraw;
    private WithdrawJewelAdapter mJewelAdapter;
    private WithdrawInfo withdrawInfos = new WithdrawInfo();
    private boolean hasAlipay = false;
    private boolean hasWxPay = false;
    private TextView tvChooseWithdrawManner;
    private ImageView ivWidthdrawRule;
    private RelativeLayout rlBindingWxSuccess;
    private TextView tvWxAccount, tvWxRealName;
    private WxVerifyInfo tempWxVerifyInfo;
    private String openId, unionId, token, wxNickname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);
        CoreUtils.register(this);
        initTitleBar(getString(R.string.withdraw));
        initView();
        setListener();
        initData();
    }

    private void initData() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mJewelAdapter = new WithdrawJewelAdapter();
        mJewelAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            checkedPosition = mJewelAdapter.getData().get(position);
            if (!isWithdraw()) {
                return;
            }
            //发起兑换
            getDialogManager().showOkCancelDialog("您将要兑换" + checkedPosition.getCashProdName(),
                    true, new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {
                            getDialogManager().dismissDialog();
                        }

                        @Override
                        public void onOk() {
                            getDialogManager().dismissDialog();
                            if (checkedPosition != null && withdrawInfos != null) {
                                if (withdrawInfos.payment == 0) {
                                    toast("请选择支付方式");
                                } else {
                                    CoreManager.getCore(IWithdrawCore.class).requestExchangeV2(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                            checkedPosition.cashProdId, withdrawInfos.payment);
                                }
                            } else {
                                toast("兑换失败");
                            }
                        }
                    });
        });
        mRecyclerView.setAdapter(mJewelAdapter);
        loadAlipayInfo();
        loadRecyclerViewData();
    }

    private void loadAlipayInfo() {
        CoreManager.getCore(IWithdrawCore.class).getWithdrawUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
//        CoreManager.getCore(IWithdrawCore.class).getWithdrawBindingInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfo(WithdrawInfo withdrawInfo) {
        if (withdrawInfo == null) {
            return;
        }
        withdrawInfos = withdrawInfo;
        binder.setVisibility(View.VISIBLE);
        binderSucceed.setVisibility(View.GONE);
        rlBindingWxSuccess.setVisibility(View.GONE);
        hasAlipay = false;
        hasWxPay = false;

        if (withdrawInfos.payment == 2 && !TextUtils.isEmpty(withdrawInfo.alipayAccount) &&
                !withdrawInfo.alipayAccount.equals("null") && !TextUtils.isEmpty(withdrawInfo.alipayAccount)) {
            hasAlipay = true;
            binder.setVisibility(View.GONE);
            notifyListState(mJewelAdapter.getData());
            mJewelAdapter.notifyDataSetChanged();
            binderSucceed.setVisibility(View.VISIBLE);
            rlBindingWxSuccess.setVisibility(View.GONE);
            alipayAccount.setText(withdrawInfo.alipayAccount);
            alipayName.setText(withdrawInfo.alipayAccountName);
        } else if (withdrawInfos.payment == 1 && !TextUtils.isEmpty(withdrawInfo.wxPhone) || !TextUtils.isEmpty(withdrawInfo.wxRealName)) {
            hasWxPay = true;
            binder.setVisibility(View.GONE);
            notifyListState(mJewelAdapter.getData());
            mJewelAdapter.notifyDataSetChanged();
            rlBindingWxSuccess.setVisibility(View.VISIBLE);
            binderSucceed.setVisibility(View.GONE);
            tvWxAccount.setText("真实姓名：" + withdrawInfo.wxRealName);
            tvWxRealName.setText("微信昵称：" + withdrawInfo.wxNickName);
        }
        diamondNumWithdraw.setText(String.valueOf(withdrawInfos.diamondNum));
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfoFail(String error) {
        toast(error);
    }

    private void loadRecyclerViewData() {
        CoreManager.getCore(IWithdrawCore.class).getWithdrawList();
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawList(List<WithdrwaListInfo> withdrwaListInfo) {
        if (withdrwaListInfo != null && withdrwaListInfo.size() > 0) {
            notifyListState(withdrwaListInfo);
            mJewelAdapter.setNewData(withdrwaListInfo);
        }
    }

    private void notifyListState(List<WithdrwaListInfo> withdrwaListInfo) {
        if (withdrwaListInfo != null && !withdrwaListInfo.isEmpty()) {
            if (hasAlipay || hasWxPay) {
                for (WithdrwaListInfo info : withdrwaListInfo) {
                    info.setWd(true);
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawListFail(String error) {
        toast("获取提现列表失败");
    }

    private void setListener() {
        rlBindingWxSuccess.setOnClickListener(v -> {
            WithdrawMannerDialog withdrawMannerDialog = WithdrawMannerDialog.newInstance(withdrawInfos);
            if (!withdrawMannerDialog.isAdded()) {
                withdrawMannerDialog.show(getFragmentManager(), "");
            }
        });
        //用户点击绑定支付宝
        binder.setOnClickListener(v -> {
            WithdrawMannerDialog withdrawMannerDialog = WithdrawMannerDialog.newInstance(withdrawInfos);
            if (!withdrawMannerDialog.isAdded()) {
                withdrawMannerDialog.show(getFragmentManager(), "");
            }
        });
        //用户点击修改支付宝信息
        binderSucceed.setOnClickListener(v -> {
            WithdrawMannerDialog withdrawMannerDialog = WithdrawMannerDialog.newInstance(withdrawInfos);
            if (!withdrawMannerDialog.isAdded()) {
                withdrawMannerDialog.show(getFragmentManager(), "");
            }
        });
    }

    private boolean isWithdraw() {
        if (withdrawInfos != null && !withdrawInfos.isNotBoundPhone) {
            if (checkedPosition != null) {
                //用户的钻石余额 > 选中金额的钻石数时
                if (withdrawInfos.diamondNum >= checkedPosition.diamondNum) {
//                    unBtnWithdraw.setVisibility(View.GONE);
//                    btnWithdraw.setVisibility(View.VISIBLE);
                    return true;
                } else {
                    toast("您的钻石不足以进行此次提现！");
//                    unBtnWithdraw.setVisibility(View.VISIBLE);
//                    btnWithdraw.setVisibility(View.GONE);
                    return false;
                }
            }
        } else {
            return false;
        }
        //如果选中position不为空的时候
        return false;
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onRequestExchange(ExchangerInfo exchangerInfo) {
        if (exchangerInfo != null) {
            withdrawInfos.diamondNum = exchangerInfo.diamondNum;
            diamondNumWithdraw.setText(String.valueOf(exchangerInfo.diamondNum));
            toast("提现成功");
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onRequestExchangeFail(String error) {
        toast(error);
    }

    public void getThirdPlatformAccessToken(WxVerifyInfo wxVerifyInfo) {
        Platform wechat = ShareSDK.getPlatform(Wechat.NAME);
        if (!wechat.isClientValid()) {
            toast("未安装微信");
            return;
        }
        if (wechat.isAuthValid()) {
            wechat.removeAccount(true);
        }

        wechat.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(final Platform platform, int i, HashMap<String, Object> hashMap) {
                if (i == Platform.ACTION_USER_INFOR) {
                    tempWxVerifyInfo = wxVerifyInfo;
                    token = platform.getDb().getToken();
                    openId = platform.getDb().getUserId();
                    unionId = platform.getDb().get("unionid");
                    wxNickname = platform.getDb().getUserName();
                    bindingWx(tempWxVerifyInfo, token, openId, unionId, wxNickname);
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                if (null != throwable) {
                    toast("绑定失败");
                }
            }

            @Override
            public void onCancel(Platform platform, int i) {
            }
        });
        wechat.SSOSetting(false);
        wechat.showUser(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonTitleDialog.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                WxVerifyInfo wxVerifyInfo = bundle.getParcelable("data");
                if (wxVerifyInfo != null) {
                    getThirdPlatformAccessToken(wxVerifyInfo);
                }
            }
        }
    }

    private void unbindingWx() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("type", String.valueOf(1));
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.unbindingThird(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                toast(e.getMessage());
            }

            @Override
            public void onResponse(Json response) {
                if (response == null) {
                    return;
                }

                if (response.num("code") == 200) {
                    bindingWx(tempWxVerifyInfo, token, openId, unionId, wxNickname);
                }
            }
        });
    }

    private void bindingWx(WxVerifyInfo wxVerifyInfo, String accessToken, String openId, String unionId, String wxNickname) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("accessToken", accessToken);
        params.put("code", wxVerifyInfo.getVerifyCode());
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo == null) {
            return;
        }
        params.put("nickName", wxNickname);
        params.put("openId", openId);
        params.put("phone", wxVerifyInfo.getPhone());
        params.put("realName", wxVerifyInfo.getRealName());
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("type", String.valueOf(1));
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("unionId", unionId);

        OkHttpManager.getInstance().doPostRequest(UriProvider.bindingThird(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                toast(e.getMessage());
            }

            @Override
            public void onResponse(Json response) {
                if (response == null) {
                    return;
                }

                if (response.num("code") == 200) {
                    loadAlipayInfo();//重新加载数据
                } else if (response.num("code") == 500) {
                    unbindingWx();
                } else {
                    toast("数据错误");

                }
            }
        });
    }

    private void initView() {
        rlBindingWxSuccess = (RelativeLayout) findViewById(R.id.rl_binding_wx_success);
        tvWxAccount = (TextView) findViewById(R.id.tv_wx_account);
        tvWxRealName = (TextView) findViewById(R.id.tv_wx_real_name);
        tvChooseWithdrawManner = (TextView) findViewById(R.id.tv_choose_withdraw_manner);
        tvChooseWithdrawManner.setOnClickListener(v -> {
            WithdrawMannerDialog withdrawMannerDialog = WithdrawMannerDialog.newInstance(withdrawInfos);
            if (!withdrawMannerDialog.isAdded()) {
                withdrawMannerDialog.show(getFragmentManager(), "");
            }
        });
        diamondNumWithdraw = (TextView) findViewById(R.id.tv_diamondNums);
        binder = (FrameLayout) findViewById(R.id.rly_binder);
        binderSucceed = (RelativeLayout) findViewById(R.id.rly_binder_succeed);
        alipayAccount = (TextView) findViewById(R.id.tv_user_zhifubao);
        alipayName = (TextView) findViewById(R.id.tv_user_zhifubao_name);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        btnWithdraw = (Button) findViewById(R.id.btn_withdraw);
        unBtnWithdraw = (Button) findViewById(R.id.btn_withdraw_un);

    }


    public void initTitleBar(String title) {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            mTitleBar.setTitle(title);
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.black));
            mTitleBar.setDividerColor(ContextCompat.getColor(this, R.color.color_f7f7f7));
            mTitleBar.setSubTitleColor(getResources().getColor(R.color.text_1A1A1A));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        mTitleBar.setActionTextColor(getResources().getColor(R.color.text_1A1A1A));
        ivWidthdrawRule = (ImageView) findViewById(R.id.iv_withdraw_rule);
        ivWidthdrawRule.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), WithdrawRuleActivity.class)));
//        mTitleBar.addAction(new TitleBar.TextAction("提现规则") {
//            @Override
//            public void performAction(View view) {
//                startActivity(new Intent(getApplicationContext(), WithdrawRuleActivity.class));
//            }
//        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshEvent(RefreshInfo refreshInfo) {
        loadAlipayInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CoreUtils.unregister(this);
    }

}
