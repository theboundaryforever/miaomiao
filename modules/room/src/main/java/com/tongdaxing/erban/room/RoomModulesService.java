package com.tongdaxing.erban.room;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.room.api.IRoomModulesService;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;

/**
 * Created by Chen on 2019/7/10.
 */
public class RoomModulesService extends AbstractBaseCore implements IRoomModulesService {
    private String TAG = "RoomModulesService";

    @Override
    public void enterRoom(long roomId) {
        L.debug(TAG, "enterRoom roomId: %d", roomId);
        enterRoom(roomId, 0);
    }

    @Override
    public void enterRoom(long roomId, long followId) {
        //        ARouter.getInstance().build(RoomConstant.ROOM_ACTIVITY_PATH)
//                .withLong(RoomConstant.ENTER_ROOM_ID, roomId)
//                .withLong(RoomConstant.ENTER_ROOM_FOLLOW_ID, followId)
//                .withBoolean(RoomConstant.SHOW_EGG_ID, openEgg)
//                .navigation(BaseApp.gStack.getTopContext());
    }

    @Override
    public void exitRoom() {

    }

    @Override
    public void registerTalkFactory(int type, Object factory) {

    }

    @Override
    public void showMessage(IRoomTalkMessage talkMessage) {

    }

    @Override
    public void sendMessage(IRoomTalkMessage talkMessage) {

    }

    @Override
    public void moveChair(long playerId) {

    }

    @Override
    public void muteSpeak(long playerId, boolean state) {

    }

    @Override
    public void muteTalk(long playerId, boolean state) {

    }

    @Override
    public void tickoutPlayer(long playerId) {

    }

    @Override
    public void addBlackList(long playerId) {

    }

    @Override
    public void reportPlayer(long playerId) {

    }

    @Override
    public void showPlayerInfo(long playerId) {

    }
}
