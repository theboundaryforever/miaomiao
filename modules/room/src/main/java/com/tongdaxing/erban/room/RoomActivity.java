package com.tongdaxing.erban.room;

import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.erban.ui.mvp.MVPBaseActivity;
import com.tongdaxing.erban.common.room.avroom.widget.BottomView;
import com.tongdaxing.erban.common.room.talk.RoomTalkView;
import com.tongdaxing.erban.room.top.RoomTopView;

import butterknife.BindView;
import butterknife.ButterKnife;

@Route(path = RoomARouterPath.AROUTER_ROOM_PATH)
public class RoomActivity extends MVPBaseActivity<IRoomView, RoomPresenter> implements IRoomView {
    @BindView(R2.id.room_theme_view)
    ImageView mRoomThemeView;
    @BindView(R2.id.room_rtv_top_view)
    RoomTopView mRoomRtvTopView;
    @BindView(R2.id.room_rtv_talk_view)
    RoomTalkView mRoomRtvTalkView;
    @BindView(R2.id.room_bv_bottom_view)
    BottomView mRoomBvBottomView;

    @NonNull
    @Override
    protected RoomPresenter createPresenter() {
        return new RoomPresenter();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.room_activity;
    }

    @Override
    public void findView() {
        ButterKnife.bind(this);
        ARouter.getInstance().inject(this);
    }

    @Override
    public void setView() {

    }

    @Override
    public void setListener() {

    }
}
