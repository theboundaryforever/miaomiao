package com.tongdaxing.erban.room;

import android.text.TextUtils;

import com.erban.ui.mvp.BasePresenter;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.api.room.IRoomService;
import com.tongdaxing.erban.api.room.info.MasterInfo;
import com.tongdaxing.erban.api.room.manager.IRoomBasicMgr;
import com.tongdaxing.erban.api.room.session.IRoomSession;
import com.tongdaxing.erban.api.user.IUserService;
import com.tongdaxing.xchat_core.manager.RoomEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


/**
 * Created by Chen on 2018/11/23.
 */
public class RoomBasePresenter<UIInterface> extends BasePresenter<UIInterface> {

    protected String TAG = "RoomBasePresenter";

    protected IRoomBasicMgr mRoomBasicMgr;
    protected IRoomSession mRoomSession;

    public RoomBasePresenter() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        doJoinRoomSuccess();
    }

    private void doJoinRoomSuccess() {
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onRoomJoinSuccess(RoomEvent.RoomJoinSuccess roomJoinSuccess) {
//        L.debug(TAG, "joinRoomSuccess");
//        doJoinRoomSuccess();
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onRoomJoinFail(RoomEvent.RoomJoinFail event) {
//        L.debug(TAG, "onRoomJoinFail");
//        if (event == null) {
//            return;
//        }
//        joinRoomFail(event.getResult(), event.getMessege());
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void openRoomSettingView(RoomProfileAction.OnExitRoom onExitRoom) {
//        leaveRoom();
//    }
}
