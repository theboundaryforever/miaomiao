package com.tongdaxing.erban.room.top;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.erban.ui.mvp.MVPBaseFrameLayout;
import com.tongdaxing.erban.room.R;

/**
 * Created by Chen on 2019/7/11.
 */
public class RoomTopView extends MVPBaseFrameLayout<IRoomTopView, RoomTopPresenter> implements IRoomTopView {
    public RoomTopView(@NonNull Context context) {
        super(context);
    }

    public RoomTopView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RoomTopView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getContentViewId() {
        return R.layout.room_top_view;
    }

    @NonNull
    @Override
    protected RoomTopPresenter createPresenter() {
        return new RoomTopPresenter();
    }

    @Override
    protected void findView() {

    }

    @Override
    protected void setView() {

    }

    @Override
    protected void setListener() {

    }

}
