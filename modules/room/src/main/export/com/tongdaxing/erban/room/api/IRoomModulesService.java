package com.tongdaxing.erban.room.api;

import com.tongdaxing.xchat_core.room.talk.TalkType;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * Created by Chen on 2019/7/12.
 */
public interface IRoomModulesService extends IBaseCore {
    void enterRoom(long roomId, long followId);

    void enterRoom(long roomId);

    void exitRoom();

    void registerTalkFactory(@TalkType.Type int type, Object factory);

    void showMessage(IRoomTalkMessage talkMessage);

    void sendMessage(IRoomTalkMessage talkMessage);

    void moveChair(long playerId);

    void muteSpeak(long playerId, boolean state);

    void muteTalk(long playerId, boolean state);

    void tickoutPlayer(long playerId);

    void addBlackList(long playerId);

    void reportPlayer(long playerId);

    void showPlayerInfo(long playerId);
}
