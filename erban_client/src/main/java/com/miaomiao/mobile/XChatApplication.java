package com.miaomiao.mobile;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Color;
import android.net.http.HttpResponseCache;
import android.os.Bundle;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.request.target.ViewTarget;
import com.github.moduth.blockcanary.BlockCanary;
import com.github.moduth.blockcanary.BlockCanaryContext;
import com.juxiao.library_utils.log.LogUtil;
import com.juxiao.library_utils.storage.StorageType;
import com.juxiao.library_utils.storage.StorageUtil;
import com.microquation.linkedme.android.LinkedME;
import com.mob.MobApplication;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.SDKOptions;
import com.netease.nimlib.sdk.StatusBarNotificationConfig;
import com.netease.nimlib.sdk.msg.MessageNotifierCustomization;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.pingplusplus.android.Pingpp;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreater;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreater;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.tcloud.core.CoreLog;
import com.tcloud.core.log.L;
import com.tcloud.core.log.LogProxy;
import com.tcloud.core.thread.ExecutorCenter;
import com.tcloud.core.thread.WorkRunnable;
import com.tcloud.core.util.Config;
import com.tcloud.core.util.Utils;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.crashreport.CrashReport;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.common.BaseApp;
import com.tongdaxing.erban.common.ModulesInit;
import com.tongdaxing.erban.common.jpush.JPushHelper;
import com.tongdaxing.erban.common.reciever.ConnectiveChangedReceiver;
import com.tongdaxing.erban.common.room.talk.manager.TalkFactoryInit;
import com.tongdaxing.erban.common.ui.MainActivity;
import com.tongdaxing.erban.common.ui.launch.activity.MiddleActivity;
import com.tongdaxing.erban.common.ui.launch.activity.NimMiddleActivity;
import com.tongdaxing.erban.common.utils.UIHelper;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.ErBanAllHostnameVerifier;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RxNet;
import com.tongdaxing.erban.libcommon.net.rxnet.model.HttpParams;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.CoreContact;
import com.tongdaxing.xchat_core.CoreRegisterCenter;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.Env;
import com.tongdaxing.xchat_core.LoginSyncDataStatusObserver;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.activity.IActivityCore;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.login.IIMLoginCore;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.im.notification.INotificationCore;
import com.tongdaxing.xchat_core.im.room.IIMRoomCore;
import com.tongdaxing.xchat_core.im.sysmsg.ISysMsgCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.realm.IRealmCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCore;
import com.tongdaxing.xchat_core.rom.RomBugHander;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.BuildConfig;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.image.ImageManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.constant.AppKey;
import com.tongdaxing.xchat_framework.util.util.ChannelUtil;
import com.umeng.commonsdk.UMConfigure;
import com.zhy.autolayout.utils.ScreenUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;

import cn.jpush.android.api.JPushInterface;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.plugins.RxJavaPlugins;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI;

/**
 * @author chenran
 * @date 2017/2/11
 */

public class XChatApplication extends MobApplication {
    public static final String TAG = "XChatApplication";
    private static final String LAST_CRASH_KEY = "ark_crashproxy_last_crash";
    public static XChatApplication gContext;
    private static String sChannel = "";

    //static 代码段可以防止内存泄露
    static {
        SmartRefreshLayout.setDefaultRefreshHeaderCreater(new DefaultRefreshHeaderCreater() {
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
//                layout.setPrimaryColorsId(R.color.colorPrimary, android.R.color.white);//全局设置主题颜色
//                layout.setLoadmoreFinished(false);
                layout.setEnableHeaderTranslationContent(false);
                MaterialHeader materialHeader = new MaterialHeader(context);
                materialHeader.setShowBezierWave(false);
                return materialHeader;
            }
        });
        SmartRefreshLayout.setDefaultRefreshFooterCreater(new DefaultRefreshFooterCreater() {
            @NonNull
            @Override
            public RefreshFooter createRefreshFooter(Context context, RefreshLayout layout) {
                return new ClassicsFooter(context).setDrawableSize(20);
            }
        });
    }

    private MessageNotifierCustomization messageNotifierCustomization = new MessageNotifierCustomization() {
        @Override
        public String makeNotifyContent(String nick, IMMessage message) {
            if (message.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment customAttachment = (CustomAttachment) message.getAttachment();
                if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                    return message.getFromNick();
                }
            }
            // 采用SDK默认文案
            return "收到一条消息";
        }

        @Override
        public String makeTicker(String nick, IMMessage message) {
            if (message.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment customAttachment = (CustomAttachment) message.getAttachment();
                if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                    return message.getFromNick();
                }
            }
            // 采用SDK默认文案
            return "收到一条消息";
        }

        @Override
        public String makeRevokeMsgTip(String revokeAccount, IMMessage item) {
            return null;
        }
    };

    public static boolean inMainProcess(Context context) {
        String packageName = context.getPackageName();
        String processName = getProcessName(context);
        return packageName.equals(processName);
    }

    /**
     * 获取当前进程名
     *
     * @param context
     * @return 进程名
     */
    public static String getProcessName(Context context) {
        String processName = null;

        // ActivityManager
        ActivityManager am = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE));

        while (true) {
            for (ActivityManager.RunningAppProcessInfo info : am.getRunningAppProcesses()) {
                if (info.pid == android.os.Process.myPid()) {
                    processName = info.processName;
                    break;
                }
            }

            // go home
            if (!TextUtils.isEmpty(processName)) {
                return processName;
            }

            // take a rest and again
            try {
                Thread.sleep(100L);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void initRxNet(Context context, String url) {
        HttpParams httpParams = new HttpParams();
//        httpParams.put("os", "android");
//        httpParams.put("osVersion", Build.VERSION.RELEASE);
//        httpParams.put("app", "xchat");
//        httpParams.put("ispType", String.valueOf(SystemUtils.getIspType(context)));
//        httpParams.put("netType", String.valueOf(SystemUtils.getNetworkType(context)));
//        httpParams.put("model", SystemUtils.getPhoneModel());
//        httpParams.put("appVersion", VersionUtil.getLocalName(context));
//        httpParams.put("appCode", VersionUtil.getVersionCode(context) + "");
//        httpParams.put("deviceId", DeviceUuidFactory.getDeviceId(context));
//        httpParams.put("channel", AppMetaDataUtil.getChannelID(context));
        RxNet.init(context)
                .debug(BasicConfig.isDebug)
                .setBaseUrl(url)
                .setHttpParams(httpParams)
                .certificates()
                .hostnameVerifier(new ErBanAllHostnameVerifier())
                .build();
    }

    public static void markCrash(boolean isCrash) {
        Config.getInstance(gContext).setBooleanSync(LAST_CRASH_KEY, isCrash);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        gContext = this;
        OkHttpManager.init(this);
        NIMClient.init(this, null, options());

        if (inMainProcess(this)) {
            initMainThread();

            if (BasicConfig.isDebug) {           // 这两行必须写在init之前，否则这些配置在init过程中将无效
                ARouter.openLog();     // 打印日志
                ARouter.openDebug();   // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
            }
            ARouter.init(this);

            ExecutorCenter.getInstance().post(new WorkRunnable() {
                @android.support.annotation.NonNull
                @Override
                public String getTag() {
                    return TAG;
                }

                @Override
                public void run() {
                    onDelayInit();
                }
            });
        }

        //应用退出时，退出聊天室，释放必要资源
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {
                L.debug(TAG, "onActivityResumed: activity = %s", activity.getClass().getSimpleName());
                if (activity instanceof MainActivity) {
                    BasicConfig.INSTANCE.setMainActivityLive(true);
                }
                BasicConfig.INSTANCE.setAppRunningForeground(true);
                if (BasicConfig.INSTANCE.isAppRunningForeground()) {
                    //如果应用在前台，收到消息不应该有通知栏提醒（但会有未读数变更通知）
                    NIMClient.getService(MsgService.class).setChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_ALL, SessionTypeEnum.None);
                }
                HashSet<String> jpushTags = BasicConfig.INSTANCE.getJpushTags();
                if (jpushTags != null) {
                    if (jpushTags.contains(JPushHelper.TAG_BACKGROUND)) {
                        jpushTags.remove(JPushHelper.TAG_BACKGROUND);
                        JPushHelper.getInstance().onTagsAction(gContext, jpushTags, JPushHelper.ACTION_SET_TAG);
                    }
                }
            }

            @Override
            public void onActivityPaused(Activity activity) {
                L.debug(TAG, "onActivityPaused: activity = %s, !UIHelper.isRunForeground(gContext) = %b", activity.getLocalClassName(), !UIHelper.isRunForeground(gContext));
                if (!UIHelper.isRunForeground(gContext)) {
                    //如果应用在后台，收到消息应该有通知栏提醒
                    NIMClient.getService(MsgService.class).setChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_NONE, SessionTypeEnum.None);
                    BasicConfig.INSTANCE.setAppRunningForeground(false);

                    HashSet<String> jpushTags = BasicConfig.INSTANCE.getJpushTags();
                    if (jpushTags != null) {
                        jpushTags.add(JPushHelper.TAG_BACKGROUND);
                        JPushHelper.getInstance().onTagsAction(gContext, jpushTags, JPushHelper.ACTION_SET_TAG);
                    }
                }
            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                if (activity instanceof MainActivity) {
                    BasicConfig.INSTANCE.setMainActivityLive(false);

                    HashSet<String> jpushTags = BasicConfig.INSTANCE.getJpushTags();
                    if (jpushTags != null && !jpushTags.contains(JPushHelper.TAG_BACKGROUND)) {
                        jpushTags.add(JPushHelper.TAG_BACKGROUND);
                        JPushHelper.getInstance().onTagsAction(gContext, jpushTags, JPushHelper.ACTION_SET_TAG);
                    }

                    AvRoomDataManager.get().release();
                    PublicChatRoomManager publicChatRoomManager = CoreManager.getCore(IRoomCore.class).getPublicChatRoomManager();
                    L.info(TAG, "MainActivity : destroyed, PublicChatRoomManager.mIsEnterRoom = %b", PublicChatRoomManager.mIsEnterRoom);
                    publicChatRoomManager.leaveRoom();
                    new AvRoomModel().exitRoom(new CallBack<String>() {
                        @Override
                        public void onSuccess(String data) {

                        }

                        @Override
                        public void onFail(int code, String error) {

                        }
                    }, 0);
                }
            }
        });

        CoreContact.initCore(getApplicationContext());
    }

    private void initMainThread() {
        sChannel = getChannel();
        //友盟统计
        UMConfigure.init(this, AppKey.getUmengAppkey(), sChannel, UMConfigure.DEVICE_TYPE_PHONE, AppKey.getUmengAppSecret());

        init();
        BaseApp.init(this);
        //bugly初始化
        initBugly();

        ModulesInit.getInstance().init();
    }

    private void onDelayInit() {
        RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                throwable.printStackTrace();
                // print it
                Log.e(TAG, "the subscribe() method default error handler", throwable);
            }
        });
        //fixed: Glide Exception:"You must not call setTag() on a view Glide is targeting"
        ViewTarget.setTagId(R.id.tag_glide);

        libInit();

        if (BasicConfig.isDebug) {
            //设置debug模式下打印LinkedME日志
            LinkedME.getInstance(this).setDebug();
        } else {
            LinkedME.getInstance(this);
        }
        LinkedME.getInstance().setImmediate(false);
        LinkedME.getInstance().setHandleActivity(MiddleActivity.class.getName());

        //JPush初始化
        JPushInterface.setDebugMode(BasicConfig.isDebug);
        JPushInterface.init(this);

        //修复系统的bug
        RomBugHander.initHuaweiVerifier(this);
        RomBugHander.initVivoAbsListViewCrashHander();
        // xlog日志初始化
        CoreLog.setgIsTest(BasicConfig.isDebug);
        CoreLog.init(this);

        TalkFactoryInit.init();

        //
//        ModelInit.init();
    }

    /**
     * maven类库初始化
     */
    private void libInit() {
        StorageUtil.init(this, null);
        String path = StorageUtil.getDirectoryByDirType(StorageType.TYPE_LOG);
        LogUtil.init(path, Log.DEBUG);
        LogUtil.setConsoleFileLog(true);//是否关闭日志输出到控制台，true开启，false关闭
        LogUtil.setConsoleLog(BuildConfig.isDebug);//是否关闭日志输出到文件，true开启，false关闭
    }

    /**
     * 获取渠道号
     */
    private String getChannel() {
        try {
            return ChannelUtil.getChannel(this);
        } catch (Exception e) {
            e.printStackTrace();
            return ChannelUtil.DEFUALT_CHANNEL;
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);
        // 安装tinker
//        Beta.installTinker();
    }

    /**
     * @return
     */
    public SDKOptions options() {
        SDKOptions options = new SDKOptions();
        options.asyncInitSDK = true;
        if (BasicConfig.isDebug)
            options.checkManifestConfig = true;
        // 如果将新消息通知提醒托管给 SDK 完成，需要添加以下配置。否则无需设置。
        StatusBarNotificationConfig config = new StatusBarNotificationConfig();
        // 点击通知栏跳转到该Activity
        config.notificationEntrance = NimMiddleActivity.class;
//        config.notificationSmallIconId = R.drawable.icon_msg_normal;
        // 呼吸灯配置
        config.ledARGB = Color.GREEN;
        config.ledOnMs = 1000;
        config.ledOffMs = 1500;
        // 通知铃声的uri字符串
        config.notificationSound = "android.resource://com.netease.nim.demo/raw/msg";
        options.statusBarNotificationConfig = config;
        // 定制通知栏提醒文案（可选，如果不定制将采用SDK默认文案）
        options.messageNotifierCustomization = messageNotifierCustomization;

        options.appKey = Constants.nimAppKey;

        // 配置保存图片，文件，log 等数据的目录
        // 如果 options 中没有设置这个值，SDK 会使用下面代码示例中的位置作为 SDK 的数据目录。
        // 该目录目前包含 log, file, image, audio, video, thumb 这6个目录。
        // 如果第三方 APP 需要缓存清理功能， 清理这个目录下面个子目录的内容即可。
        String sdkPath = null;
        try {
            sdkPath = Environment.getExternalStorageDirectory() + "/" + this.getPackageName() + "/nim";
        } catch (ArrayIndexOutOfBoundsException e) {

        }
        options.sdkStorageRootPath = sdkPath;

        // 配置是否需要预下载附件缩略图，默认为 true
        options.preloadAttach = true;

        // 配置附件缩略图的尺寸大小。表示向服务器请求缩略图文件的大小
        // 该值一般应根据屏幕尺寸来确定， 默认值为 Screen.width / 2
        int[] size = ScreenUtils.getScreenSize(this, true);
        options.thumbnailSize = size[0] / 2;
//        // save cache，留做切换账号备用
        DemoCache.setNotificationConfig(config);
        return options;
    }

    private void init() {
        initNimUIKit();
        BasicConfig.INSTANCE.setAppContext(getApplicationContext());

        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BasicConfig.isDebug;
            }
        });


        //切换生产坏境和测试环境 true/测试环境 false/生产环境
        BasicConfig.INSTANCE.setDebuggable(BasicConfig.isDebug);
        BasicConfig.INSTANCE.setChannel(sChannel);

        Pingpp.DEBUG = BasicConfig.isDebug;
        Env.instance().init();
        BasicConfig.INSTANCE.setRootDir(Constants.ERBAN_DIR_NAME);
        BasicConfig.INSTANCE.setLogDir(Constants.LOG_DIR);
        BasicConfig.INSTANCE.setConfigDir(Constants.CONFIG_DIR);
        BasicConfig.INSTANCE.setVoiceDir(Constants.VOICE_DIR);
        BasicConfig.INSTANCE.setCacheDir(Constants.CACHE_DIR);

        try {
            File cacheDir = new File(getApplicationContext().getCacheDir(), "http");
            HttpResponseCache.install(cacheDir, 1024 * 1024 * 128);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // url
        UriProvider.initDevUri(UriProvider.JAVA_WEB_URL);

        initRxNet(BasicConfig.INSTANCE.getAppContext(), UriProvider.JAVA_WEB_URL);

        //UI卡顿监控
        setBlockCanary();
        RequestManager.instance().init(getApplicationContext(), Constants.HTTP_CACHE_DIR);
        ImageManager.instance().init(getApplicationContext(), Constants.IMAGE_CACHE_DIR);

        ConnectiveChangedReceiver.getInstance().init(getApplicationContext());
        //corexxxw
        CoreManager.init(BasicConfig.INSTANCE.getLogDir().getAbsolutePath());
        CoreRegisterCenter.registerCore();

        CoreManager.getCore(IRealmCore.class);
        CoreManager.getCore(IUserCore.class);
        CoreManager.getCore(IRedPacketCore.class);
        CoreManager.getCore(IActivityCore.class);
        CoreManager.getCore(IIMLoginCore.class);
        CoreManager.getCore(IIMFriendCore.class);
        CoreManager.getCore(IGiftCore.class);
        CoreManager.getCore(IFaceCore.class);
        CoreManager.getCore(IPayCore.class);
        CoreManager.getCore(IIMMessageCore.class);
        CoreManager.getCore(IIMRoomCore.class);
        CoreManager.getCore(IAVRoomCore.class);
        CoreManager.getCore(IRoomCore.class);

        LogUtil.i(TAG, sChannel);
//        initBaiduStatistic(channel);

        // 监听登录同步数据完成通知
        LoginSyncDataStatusObserver.getInstance().registerLoginSyncDataStatus(true);
        //系统通知的到达和已读
        CoreManager.getCore(ISysMsgCore.class).registSystemMessageObserver(true);
        //注册im用户状态的系统通知
        CoreManager.getCore(IIMLoginCore.class).registAuthServiceObserver(true);
        //其他端登录观察者
//        CoreManager.getCore(IIMLoginCore.class).registerOtherClientsObserver(true);
        //全局自定义通知
        CoreManager.getCore(INotificationCore.class).observeCustomNotification(true);
    }

    private void initNimUIKit() {
        NimUIKit.init(this);
    }

    private void setBlockCanary() {
        if (!BasicConfig.INSTANCE.isDebuggable()) {
            return;
        }

        BlockCanary.install(this, new BlockCanaryContext()).start();
    }

    private void initBugly() {
        /***** Beta高级设置 *****/
        /**
         * true表示app启动自动初始化升级模块; false不会自动初始化;
         * 开发者如果担心sdk初始化影响app启动速度，可以设置为false，
         * 在后面某个时刻手动调用Beta.init(getApplicationContext(),false);
         */

        /**
         * true表示初始化时自动检查升级，默认true; false表示不会自动检查升级,需要手动调用Beta.checkUpgrade()方法;
         */
        Beta.autoCheckUpgrade = true;

        /**
         * 设置升级检查周期为60s(默认检查周期为0s)，60s内SDK不重复向后台请求策略);
         */
        Beta.upgradeCheckPeriod = 60 * 1000;
        /**
         * 设置启动延时为1s（默认延时3s），APP启动1s后初始化SDK，避免影响APP启动速度;
         */
        Beta.initDelay = 3000;

        /**
         * 设置sd卡的Download为更新资源保存目录;
         * 后续更新资源会保存在此目录，需要在manifest中添加WRITE_EXTERNAL_STORAGE权限;
         */
        Beta.storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        /**
         * 已经确认过的弹窗在APP下次启动自动检查更新时会再次显示;
         */
        Beta.showInterruptedStrategy = true;
        /**
         * 只允许在MainActivity上显示更新弹窗，其他activity上不显示弹窗; 不设置会默认所有activity都可以显示弹窗;
         */
        Beta.canShowUpgradeActs.add(MainActivity.class);
        /**
         * 显示通知栏
         */
        Beta.enableNotification = true;

        BuglyStrategy strategy = new BuglyStrategy();
        strategy.setAppChannel(getChannel());

        String packageName = this.getPackageName();
        String processName = Utils.getProcessName(this);

        strategy.setUploadProcess(processName.equals("null_name") || processName.equals(packageName));
        strategy.setCrashHandleCallback(new CrashReport.CrashHandleCallback() {
            public Map<String, String> onCrashHandleStart(int crashType, String errorType,
                                                          String errorMessage, String errorStack) {
                if (CrashReport.CrashHandleCallback.CRASHTYPE_NATIVE == crashType) {
                    L.info(TAG, "native crash occur");
                } else {
                    L.info(TAG, "crash occur");
                }
                L.error(TAG, "%s \n %s", errorMessage, errorStack);
                LogProxy.flushToDisk();
                markCrash(true);
                return null;
            }

            @Override
            public byte[] onCrashHandleStart2GetExtraDatas(int crashType, String errorType,
                                                           String errorMessage, String errorStack) {
                return null;
            }

        });

        if (!BasicConfig.isDebug) {
            Bugly.init(getApplicationContext(), "ffa4282259", false, strategy);
        } else {
            Bugly.init(getApplicationContext(), "c120af35e2", true, strategy);
        }
    }

}
