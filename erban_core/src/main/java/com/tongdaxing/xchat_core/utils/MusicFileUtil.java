package com.tongdaxing.xchat_core.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.tongdaxing.xchat_core.music.IMusicCore;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MusicFileUtil {

    public static final String tiantianMusicDownloadUri = "/tiantian/music/";
    public static final String AUDIO_SUFFIX_MP3 = ".mp3";
//    public static final String AUDIO_SUFFIX_AAC = ".aac";
//    public static final String AUDIO_SUFFIX_3GP = ".3gp";
//    public static final String AUDIO_SUFFIX_WAV = ".wav";
//    public static final String AUDIO_SUFFIX_FLAC = ".flac";
//    public static final String AUDIO_SUFFIX_M4A = ".m4a";

    public static final String[] mimeTypes = new String[]{
            AUDIO_SUFFIX_MP3
    };

    private static List<String> downloadPaths;

    public static List<String> getDownloadPaths() {
        String rootPath = Environment.getExternalStorageDirectory() + "/";
        if (null == downloadPaths) {
            downloadPaths = new ArrayList<>();
            downloadPaths.add(rootPath + "kgmusic/download/");//酷狗目录
            downloadPaths.add(rootPath + "qqmusic/song/");//qq音乐
            downloadPaths.add(rootPath + "netease/cloudmusic/Music/");//网易云音乐
            downloadPaths.add(rootPath + "KuwoMusic/music/");//酷我音乐
            downloadPaths.add(rootPath + "xiami/audios/");//虾米音乐
            downloadPaths.add(rootPath + "Baidu_music/download/");//百度音乐
            downloadPaths.add(rootPath + "Music/");//媒体库
            downloadPaths.add(rootPath + "MIUI/music/mp3/");//媒体库
            downloadPaths.add(rootPath + "Smartisan/music/cloud/");//媒体库
            downloadPaths.add(rootPath + "Music/Download/");//媒体库
            downloadPaths.add(rootPath + "Samsung/Music/Download/");//媒体库
            downloadPaths.add(rootPath + "i音乐/歌曲/");//媒体库
            downloadPaths.add(rootPath + "tiantian/music/");//甜甜语音
        }

        createMusicDownloadDir();

        return downloadPaths;
    }

    public static final Uri audioUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

    public static void createMusicDownloadDir() {
        File file = new File(getMusicDownloadDirByApp());
        if (null != file && !file.exists()) {
            file.mkdirs();
        }
    }

    public static String getMusicDownloadDirByApp() {
        return CoreManager.getCore(IMusicCore.class).getUserStoragePath();
    }

    public static String getMusicDownloadFilePath(String singName, long serverId) {
        StringBuilder sb = new StringBuilder(getMusicDownloadDirByApp() + "/");
        sb.append(singName);
//        sb.append(serverId);
        sb.append(AUDIO_SUFFIX_MP3);
        return sb.toString();
    }

    /**
     * 获取本地扫描音乐信息
     *
     * @param audioIdUri 音频ID URI
     */
    public static MusicLocalInfo getLocalSongFromUri(Context context, Uri audioIdUri) {
        MusicLocalInfo localSongInfo = null;
        if (isAudioUri(audioIdUri)) {
            ContentResolver contentResolver = context.getContentResolver();
            Cursor mCursor = contentResolver.query(audioIdUri, new String[]{
                    MediaStore.Audio.Media.TITLE,
                    MediaStore.Audio.Media.ARTIST,
                    MediaStore.Audio.Media.ALBUM,
                    MediaStore.Audio.Media.DURATION,
                    MediaStore.Audio.Media.YEAR,
                    MediaStore.Audio.Media.SIZE,
                    MediaStore.Audio.Media.DATA,
                    MediaStore.Audio.Media._ID}, null, null, null);
            if (mCursor != null && mCursor.moveToFirst()) {
                localSongInfo = getLocalSong(mCursor);
            }
            if (mCursor != null && !mCursor.isClosed()) {
                mCursor.close();
            }
        }
        return localSongInfo;
    }

    public static boolean isAudioUri(Uri audioIdUri) {
        return audioIdUri != null && audioIdUri.getPath().contains(audioUri.getPath());
    }

    /**
     * 获取本地音乐
     */
    public static MusicLocalInfo getLocalSong(Cursor cursor) {
        MusicLocalInfo song = new MusicLocalInfo();
        song.setLocalUri(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)));
        song.setSongName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)));
        song.setYear(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.YEAR)));
        song.setAlbumName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)));
        song.setDuration(cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)));//与服务端统一精度 精确到毫秒

        List<String> artistNames = new ArrayList<>();
        artistNames.add(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
        song.setArtistName(artistNames);
        song.setSingerName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
        song.setFileSize(cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.SIZE)));
        song.setSongId(SongUtils.generateThirdPartyId());
        return song;
    }
}
