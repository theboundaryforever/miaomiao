package com.tongdaxing.xchat_core.utils.file_manager;

import android.content.Context;

import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * Function:
 * Author: Edward on 2019/2/20
 */
public interface IFileBatchUploadCore extends IBaseCore {
    /**
     * 添加到上传队列
     *
     * @param url
     */
    void addUploadQueue(String url);

    /**
     * 检测指定文件是否处于下载队列中
     *
     * @param url
     */
    boolean isUploading(String url);

    /**
     * 注册回调事件
     *
     * @param context
     * @param fileManagerCallback
     */
    void registerFileManagerCallback(Context context, FileManagerCallback fileManagerCallback);

    /**
     * 反注册回调事件
     *
     * @param context
     */
    void unregisterFileManagerCallback(Context context);

}
