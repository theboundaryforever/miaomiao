package com.tongdaxing.xchat_core.utils.file_manager;

public class UploadSucceedInfo {
    public String localUrl;
    public String remoteUrl;

    @Override
    public String toString() {
        return "UploadSucceedInfo{" +
                "localUrl='" + localUrl + '\'' +
                ", remoteUrl='" + remoteUrl + '\'' +
                '}';
    }
}
