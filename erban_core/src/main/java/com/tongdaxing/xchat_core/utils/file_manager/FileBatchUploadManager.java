package com.tongdaxing.xchat_core.utils.file_manager;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.utils.UrlSafeBase64;
import com.tongdaxing.xchat_core.common.QiNiuFileUploadProfile;

import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Function:文件批量上传管理类
 * Author: Edward on 2019/2/20
 */
public class FileBatchUploadManager implements IFileBatchUploadCore {
    //正在下载url队列（并发集合）
    private List<String> localUrlQueue = new CopyOnWriteArrayList<>();
    private Map<String, Runnable> runnableTaskQueue = new ConcurrentHashMap<>();

    private ExecutorService uploadExService;
    private static IFileBatchUploadCore fileBatchUploadCore;
    private Map<Context, FileManagerCallback> callbackMap = new HashMap<>();

    @Override
    public void registerFileManagerCallback(Context context, FileManagerCallback fileManagerCallback) {
        if (context != null && callbackMap != null) {
            callbackMap.put(context, fileManagerCallback);
        }
    }

    @Override
    public void unregisterFileManagerCallback(Context context) {
        if (context != null && callbackMap != null) {
            callbackMap.remove(context);
        }
    }

    public static IFileBatchUploadCore newInstance() {
        return newInstance(10);
    }

    public static IFileBatchUploadCore newInstance(int threadCount) {
        if (fileBatchUploadCore == null) {
            synchronized (FileBatchUploadManager.class) {
                if (fileBatchUploadCore == null) {
                    fileBatchUploadCore = new FileBatchUploadManager(threadCount);
                }
            }
        }
        return fileBatchUploadCore;
    }


    private FileBatchUploadManager(int threadCount) {
        if (threadCount <= 0 || threadCount > 100) {//只能在1-100的线程数之内
            threadCount = 10;
        }
        uploadExService = Executors.newFixedThreadPool(threadCount);
    }

    /**
     * 添加上传队列
     */
    @Override
    public void addUploadQueue(String url) {
        localUrlQueue.add(url);//将文件加入队列中
        Runnable runnable = new UploadRunnable(url);
        runnableTaskQueue.put(url, runnable);//上传任务队列
        uploadExService.execute(runnable);//开启线程开始上传
    }

    /**
     * 广播成功事件给所有人
     *
     * @param uploadSucceedInfo
     */
    private void broadcastSuccess(UploadSucceedInfo uploadSucceedInfo) {
        if (callbackMap != null) {
            for (Map.Entry<Context, FileManagerCallback> entry : callbackMap.entrySet()) {
                entry.getValue().success(uploadSucceedInfo);
            }
        }
    }

    /**
     * 广播失败事件给所有人
     */
    private void broadcastFailure(UploadFailureInfo uploadFailureInfo) {
        if (callbackMap != null) {
            for (Map.Entry<Context, FileManagerCallback> entry : callbackMap.entrySet()) {
                entry.getValue().failure(uploadFailureInfo);
            }
        }
    }

    private class UploadRunnable implements Runnable {
        private String localUrl;

        UploadRunnable(String localUrl) {
            this.localUrl = localUrl;
        }

        @Override
        public void run() {
//            int a = (int) (Math.random() * 3);
//            try {
//                Log.e("输出", "开始上传" + localUrl);
//                Thread.sleep(a * 1000);
//                UploadSucceedInfo uploadSucceedInfo = new UploadSucceedInfo();
//                uploadSucceedInfo.localUrl = localUrl;
//                uploadSucceedInfo.remoteUrl = "远程路径" + this.localUrl;
//                onMusicDownloadResponse(uploadSucceedInfo);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//                UploadFailureInfo uploadFailureInfo = new UploadFailureInfo();
//                uploadFailureInfo.errorStr = e.getMessage();
//                uploadFailureInfo.localUrl = localUrl;
//                onMusicDownloadError(uploadFailureInfo);
//            }

            try {
                // 1 构造上传策略
                JSONObject _json = new JSONObject();
                long _deadline = System.currentTimeMillis() / 1000 + 3600;
                _json.put("deadline", _deadline);// 有效时间为一个小时
                _json.put("scope", QiNiuFileUploadProfile.bucket);
                String _encodedPutPolicy = UrlSafeBase64.encodeToString(_json
                        .toString().getBytes());
                byte[] _sign = HmacSHA1Encrypt(_encodedPutPolicy, QiNiuFileUploadProfile.secretKey);
                String _encodedSign = UrlSafeBase64.encodeToString(_sign);
                String _uploadToken = QiNiuFileUploadProfile.accessKey + ':' + _encodedSign + ':'
                        + _encodedPutPolicy;
                UploadManager uploadManager = new UploadManager();
                File musicFile = new File(localUrl);
                uploadManager.put(musicFile, null, _uploadToken,
                        new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info,
                                                 final JSONObject response) {
                                try {
                                    UploadSucceedInfo uploadSucceedInfo = new UploadSucceedInfo();
                                    uploadSucceedInfo.localUrl = localUrl;
                                    String imgName = response.getString("key");
                                    uploadSucceedInfo.remoteUrl = QiNiuFileUploadProfile.accessUrl + imgName + QiNiuFileUploadProfile.picprocessing;
                                    onMusicDownloadResponse(uploadSucceedInfo);
                                } catch (Exception e) {
                                    UploadFailureInfo uploadFailureInfo = new UploadFailureInfo(e.getMessage(), localUrl);
                                    onMusicDownloadError(uploadFailureInfo);
                                }
                            }
                        }, null);
            } catch (Exception e) {
                e.printStackTrace();
                UploadFailureInfo uploadFailureInfo = new UploadFailureInfo(e.getMessage(), localUrl);
                onMusicDownloadError(uploadFailureInfo);
            }
        }
    }

    /**
     * 使用 HMAC-SHA1 签名方法对encryptText进行签名
     *
     * @param encryptText 被签名的字符串
     * @param encryptKey  密钥
     * @return
     * @throws Exception
     */
    public static byte[] HmacSHA1Encrypt(String encryptText, String encryptKey)
            throws Exception {
        byte[] data = encryptKey.getBytes("UTF-8");
        // 根据给定的字节数组构造一个密钥,第二参数指定一个密钥算法的名称
        SecretKey secretKey = new SecretKeySpec(data, "HmacSHA1");
        // 生成一个指定 Mac 算法 的 Mac 对象
        Mac mac = Mac.getInstance("HmacSHA1");
        // 用给定密钥初始化 Mac 对象
        mac.init(secretKey);
        byte[] text = encryptText.getBytes("UTF-8");
        // 完成 Mac 操作
        return mac.doFinal(text);
    }

    /**
     * 上传出错
     */
    private void onMusicDownloadError(UploadFailureInfo uploadFailureInfo) {
        Message msg = Message.obtain();
        msg.what = UPLOAD_FAILURE;
        msg.obj = uploadFailureInfo;
        uploadFileHandler.sendMessage(msg);
    }

    /**
     * 上传完成
     */
    private void onMusicDownloadResponse(UploadSucceedInfo uploadSucceedInfo) {
        Message msg = Message.obtain();
        msg.what = UPLOAD_SUCCEED;
        msg.obj = uploadSucceedInfo;
        uploadFileHandler.sendMessage(msg);
    }

    private static final int UPLOAD_SUCCEED = 1;
    private static final int UPLOAD_FAILURE = 2;

    private UploadFileHandler uploadFileHandler = new UploadFileHandler(this);

    private static class UploadFileHandler extends Handler {
        private WeakReference<FileBatchUploadManager> mWeakReference;

        UploadFileHandler(FileBatchUploadManager playerCore) {
            mWeakReference = new WeakReference<>(playerCore);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            FileBatchUploadManager playerCore = mWeakReference.get();
            if (playerCore == null) {
                return;
            }
            switch (msg.what) {
                case UPLOAD_SUCCEED:
                    UploadSucceedInfo uploadSucceedInfo = (UploadSucceedInfo) msg.obj;
                    playerCore.localUrlQueue.remove(uploadSucceedInfo.localUrl);
                    playerCore.runnableTaskQueue.remove(uploadSucceedInfo.localUrl);
                    playerCore.broadcastSuccess(uploadSucceedInfo);
                    break;
                case UPLOAD_FAILURE:
                    UploadFailureInfo uploadFailureInfo = (UploadFailureInfo) msg.obj;
                    playerCore.localUrlQueue.remove(uploadFailureInfo.localUrl);
                    playerCore.runnableTaskQueue.remove(uploadFailureInfo.localUrl);
                    playerCore.broadcastFailure(uploadFailureInfo);
                    break;
                default:
            }
        }
    }

    /**
     * 检测路径是否处于下载队列中
     *
     * @param url
     */
    @Override
    public boolean isUploading(String url) {
        if (localUrlQueue != null && !TextUtils.isEmpty(url)) {
            for (int i = 0; i < localUrlQueue.size(); i++) {
                if (localUrlQueue.get(i).equals(url)) {
                    return true;
                }
            }
        }
        return false;
    }

}
