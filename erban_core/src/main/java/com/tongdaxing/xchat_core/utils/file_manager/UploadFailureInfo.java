package com.tongdaxing.xchat_core.utils.file_manager;

public class UploadFailureInfo {
    public UploadFailureInfo() {
    }

    public UploadFailureInfo(String errorStr, String localUrl) {
        this.errorStr = errorStr;
        this.localUrl = localUrl;
    }

    public String errorStr;
    public String localUrl;

    @Override
    public String toString() {
        return "UploadFailureInfo{" +
                "errorStr='" + errorStr + '\'' +
                ", localUrl='" + localUrl + '\'' +
                '}';
    }
}
