package com.tongdaxing.xchat_core.utils.file_manager;

public interface FileManagerCallback {
    void success(UploadSucceedInfo uploadSucceedInfo);

    void failure(UploadFailureInfo uploadFailureInfo);
}
