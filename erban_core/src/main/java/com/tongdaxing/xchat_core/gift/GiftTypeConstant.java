package com.tongdaxing.xchat_core.gift;

/**
 * 礼物类型
 * <p>
 * Created by zhangjian on 2019/4/26.
 */
public class GiftTypeConstant {
    /**
     * 本地背包礼物 是本地的礼物类型
     */
    public static final int GIFT_TYPE_LOCAL_BACKPACK = -2;

    /**
     * 本地普通礼物 是本地的礼物类型
     */
    public static final int GIFT_TYPE_LOCAL_NORMAL = -1;

    /**
     * 普通礼物
     */
    public static final int GIFT_TYPE_NORMAL = 2;

    /**
     * 神秘礼物
     */
    public static final int GIFT_TYPE_MYSTERY = 3;

    /**
     * 福星礼物
     */
    public static final int GIFT_TYPE_BLESSING_STAR = 5;

    /**
     * 等级礼物
     */
    public static final int GIFT_TYPE_LEVEL = 7;

    /**
     * CP礼物
     */
    public static final int GIFT_TYPE_CP = 8;

    /**
     * 撤销上一个CP邀请 重新发CP礼物
     */
    public static final int GIFT_TYPE_RESEND_CP = 9;

    /**
     * 专属礼物 只有pids中的用户才能发
     */
    public static final int GIFT_TYPE_EXCLUSIVE = 10;
}
