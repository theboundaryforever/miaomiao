package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;

/**
 * CP礼物实体
 * <p>
 * Created by zhangjian on 2019/3/26.
 */
public class CpGiftReceiveInfo implements Serializable {

    /**
     * title : cp邀请请求
     * desc : 有一天你说你不在爱做梦了，于是我也醒了。想将【冰镇西瓜】赠予你，邀你共度余生。
     * picUrl : https://pic.miaomiaofm.com/FkDOzwS9vyZ3aMMjShqJu0WjdDCZ?imageslim
     * inviteUid : 1
     * giftId : 46
     * giftName : 玫瑰花
     * status : 0
     * recordId : 8
     */

    private String title;
    private String desc;
    private String picUrl;//礼物图片url
    private Long inviteUid;//邀请方的uid
    private String inviteName;//邀请方的name
    private String inviteAvatar;//邀请方的avatar
    private Integer giftId;
    private String giftName;
    private Integer status;//0-4 与CpGiftAttachment中的常量定义一致 0邀请 1同意 2拒绝 3超时 4解除
    private Integer recordId;//每一轮CP邀请该值固定 邀请成为cp的聊天消息里面的recordId
    private Long recUid;//接收方的uid
    private Integer cpGiftLevel;//礼物等级1 2 3
    private String vggUrl;//装扮特效动画

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public long getInviteUid() {
        return inviteUid;
    }

    public void setInviteUid(long inviteUid) {
        this.inviteUid = inviteUid;
    }

    public Integer getGiftId() {
        return giftId;
    }

    public void setGiftId(Integer giftId) {
        this.giftId = giftId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public long getRecUid() {
        return recUid;
    }

    public void setRecUid(long recUid) {
        this.recUid = recUid;
    }

    public String getInviteName() {
        return inviteName;
    }

    public void setInviteName(String inviteName) {
        this.inviteName = inviteName;
    }

    public String getInviteAvatar() {
        return inviteAvatar;
    }

    public void setInviteAvatar(String inviteAvatar) {
        this.inviteAvatar = inviteAvatar;
    }

    public Integer getCpGiftLevel() {
        return cpGiftLevel;
    }

    public void setCpGiftLevel(Integer cpGiftLevel) {
        this.cpGiftLevel = cpGiftLevel;
    }

    public String getVggUrl() {
        return vggUrl;
    }

    public void setVggUrl(String vggUrl) {
        this.vggUrl = vggUrl;
    }

    @Override
    public String toString() {
        return "CpGiftReceiveInfo{" +
                "title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", inviteUid=" + inviteUid +
                ", inviteName='" + inviteName + '\'' +
                ", inviteAvatar='" + inviteAvatar + '\'' +
                ", giftId=" + giftId +
                ", giftName='" + giftName + '\'' +
                ", status=" + status +
                ", recordId=" + recordId +
                ", recUid=" + recUid +
                ", cpGiftLevel=" + cpGiftLevel +
                ", vggUrl='" + vggUrl + '\'' +
                '}';
    }
}
