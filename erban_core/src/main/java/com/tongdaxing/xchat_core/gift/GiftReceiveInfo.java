package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;
import java.util.List;

/**
 * Created by chenran on 2017/7/28.
 */

public class GiftReceiveInfo implements Serializable {
    private long uid;
    private long targetUid;
    private int giftId;
    private int giftNum;
    private String targetNick;
    private String targetAvatar;
    private String nick;
    private String avatar;
    private int personCount;
    private List<Integer> roomIdList;
    private String roomId;
    private String userNo;
    private int userGiftPurseNum;
    private int useGiftPurseGold;

    //cp
    private boolean isCpGift;//是否是展示cp动效
    private boolean isCpTopNotify;//是否是展示顶部cp动效
    private String picUrl;//cp礼物的图片地址
    private String inviteUserName;
    private String recUserName;
    private String cpVggUrl;//cp礼物的svga动效
    private int cpNum;//第几对cp

    public int getUseGiftPurseGold() {
        return useGiftPurseGold;
    }

    public void setUseGiftPurseGold(int useGiftPurseGold) {
        this.useGiftPurseGold = useGiftPurseGold;
    }

    public int getUserGiftPurseNum() {
        return userGiftPurseNum;
    }

    public void setUserGiftPurseNum(int userGiftPurseNum) {
        this.userGiftPurseNum = userGiftPurseNum;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public List<Integer> getRoomIdList() {
        return roomIdList;
    }

    public void setRoomIdList(List<Integer> roomIdList) {
        this.roomIdList = roomIdList;
    }

    public int getPersonCount() {
        return personCount;
    }

    public void setPersonCount(int personCount) {
        this.personCount = personCount;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(long targetUid) {
        this.targetUid = targetUid;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(int giftNum) {
        this.giftNum = giftNum;
    }

    public String getTargetNick() {
        return targetNick;
    }

    public void setTargetNick(String targetNick) {
        this.targetNick = targetNick;
    }

    public String getTargetAvatar() {
        return targetAvatar;
    }

    public void setTargetAvatar(String targetAvatar) {
        this.targetAvatar = targetAvatar;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public boolean isCpGift() {
        return isCpGift;
    }

    public void setCpGift(boolean cpGift) {
        isCpGift = cpGift;
    }

    public String getInviteUserName() {
        return inviteUserName;
    }

    public void setInviteUserName(String inviteUserName) {
        this.inviteUserName = inviteUserName;
    }

    public String getRecUserName() {
        return recUserName;
    }

    public void setRecUserName(String recUserName) {
        this.recUserName = recUserName;
    }

    public boolean isCpTopNotify() {
        return isCpTopNotify;
    }

    public void setCpTopNotify(boolean cpTopNotify) {
        isCpTopNotify = cpTopNotify;
    }

    public String getCpVggUrl() {
        return cpVggUrl;
    }

    public void setCpVggUrl(String cpVggUrl) {
        this.cpVggUrl = cpVggUrl;
    }

    public int getCpNum() {
        return cpNum;
    }

    public void setCpNum(int cpNum) {
        this.cpNum = cpNum;
    }

    @Override
    public String toString() {
        return "GiftReceiveInfo{" +
                "uid=" + uid +
                ", targetUid=" + targetUid +
                ", giftId=" + giftId +
                ", giftNum=" + giftNum +
                ", targetNick='" + targetNick + '\'' +
                ", targetAvatar='" + targetAvatar + '\'' +
                ", nick='" + nick + '\'' +
                ", avatar='" + avatar + '\'' +
                ", personCount=" + personCount +
                ", roomIdList=" + roomIdList +
                ", roomId='" + roomId + '\'' +
                ", userNo='" + userNo + '\'' +
                ", userGiftPurseNum=" + userGiftPurseNum +
                ", useGiftPurseGold=" + useGiftPurseGold +
                ", isCpGift=" + isCpGift +
                ", isCpTopNotify=" + isCpTopNotify +
                ", picUrl='" + picUrl + '\'' +
                ", inviteUserName='" + inviteUserName + '\'' +
                ", recUserName='" + recUserName + '\'' +
                ", cpVggUrl='" + cpVggUrl + '\'' +
                ", cpNum=" + cpNum +
                '}';
    }
}
