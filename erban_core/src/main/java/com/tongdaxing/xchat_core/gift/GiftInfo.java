package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;

/**
 * Created by chenran on 2017/7/27.
 */

public class GiftInfo implements Serializable {
    //本地的礼物类型，用在GiftAdapter类中，标记礼物是否应该显示数量、标签等，-1是普通礼物，-2是背包礼物。
    private int localGiftType;
    private int giftId;
    private int giftType;//2是普通礼物，3是神秘礼物
    private String giftName;
    private int goldPrice;
    private String giftUrl;
    private int seqNo;
    private boolean hasGifPic;
    private String gifUrl;
    private String gifFile;
    private boolean hasVggPic;
    private String vggUrl;
    private String ext;
    /**
     * 礼物自定义标签
     */
    private String giftTag;
    private boolean hasLatest;//是否是最新礼物
    private boolean hasTimeLimit;//是否是限时礼物
    private boolean hasEffect;//是否有特效
    private int userGiftPurseNum;//抽奖礼物数量
    private int giftNum;//砸蛋砸中礼物数

    private Long[] pids;//可赠送此礼物的uid 专属礼物特有

    public String getGiftTag() {
        return giftTag;
    }

    public void setGiftTag(String giftTag) {
        this.giftTag = giftTag;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public int getUserGiftPurseNum() {
        return userGiftPurseNum;
    }

    public void setUserGiftPurseNum(int userGiftPurseNum) {
        this.userGiftPurseNum = userGiftPurseNum;
    }

    public int getLocalGiftType() {
        return localGiftType;
    }

    public void setLocalGiftType(int localGiftType) {
        this.localGiftType = localGiftType;
    }

    /**
     * 是否背包礼物
     *
     * @return true表示背包礼物，false普通礼物
     */
    public boolean isBackpackGift() {
        return localGiftType == GiftTypeConstant.GIFT_TYPE_LOCAL_BACKPACK;
    }

    /**
     * 是否是贵族礼物
     */
    @Deprecated
    private boolean isNobleGift;

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public int getGiftType() {
        return giftType;
    }

    public void setGiftType(int giftType) {
        this.giftType = giftType;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(int goldPrice) {
        this.goldPrice = goldPrice;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public boolean isHasGifPic() {
        return hasGifPic;
    }

    public void setHasGifPic(boolean hasGifPic) {
        this.hasGifPic = hasGifPic;
    }

    public String getGifUrl() {
        return gifUrl;
    }

    public void setGifUrl(String gifUrl) {
        this.gifUrl = gifUrl;
    }

    public String getGifFile() {
        return gifFile;
    }

    public void setGifFile(String gifFile) {
        this.gifFile = gifFile;
    }

    public boolean isHasVggPic() {
        return hasVggPic;
    }

    public void setHasVggPic(boolean hasVggPic) {
        this.hasVggPic = hasVggPic;
    }

    public String getVggUrl() {
        return vggUrl;
    }

    public void setVggUrl(String vggUrl) {
        this.vggUrl = vggUrl;
    }

    public boolean isHasLatest() {
        return hasLatest;
    }

    public void setHasLatest(boolean hasLatest) {
        this.hasLatest = hasLatest;
    }

    public boolean isHasTimeLimit() {
        return hasTimeLimit;
    }

    public void setHasTimeLimit(boolean hasTimeLimit) {
        this.hasTimeLimit = hasTimeLimit;
    }

    public boolean isHasEffect() {
        return hasEffect;
    }

    public void setHasEffect(boolean hasEffect) {
        this.hasEffect = hasEffect;
    }

    @Deprecated
    public boolean isNobleGift() {
        return isNobleGift;
    }

    @Deprecated
    public void setNobleGift(boolean nobleGift) {
        isNobleGift = nobleGift;
    }

    public int getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(int giftNum) {
        this.giftNum = giftNum;
    }

    public Long[] getPids() {
        return pids;
    }

    public void setPids(Long[] pids) {
        this.pids = pids;
    }

    @Override
    public String toString() {
        return "GiftInfo{" +
                "localGiftType=" + localGiftType +
                ", giftId=" + giftId +
                ", giftType=" + giftType +
                ", giftName='" + giftName + '\'' +
                ", goldPrice=" + goldPrice +
                ", giftUrl='" + giftUrl + '\'' +
                ", seqNo=" + seqNo +
                ", hasGifPic=" + hasGifPic +
                ", gifUrl='" + gifUrl + '\'' +
                ", gifFile='" + gifFile + '\'' +
                ", hasVggPic=" + hasVggPic +
                ", vggUrl='" + vggUrl + '\'' +
                ", ext='" + ext + '\'' +
                ", giftTag='" + giftTag + '\'' +
                ", hasLatest=" + hasLatest +
                ", hasTimeLimit=" + hasTimeLimit +
                ", hasEffect=" + hasEffect +
                ", userGiftPurseNum=" + userGiftPurseNum +
                ", giftNum=" + giftNum +
                ", isNobleGift=" + isNobleGift +
                ", pids=" + pids +
                '}';
    }
}
