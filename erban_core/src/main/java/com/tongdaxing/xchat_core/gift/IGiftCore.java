package com.tongdaxing.xchat_core.gift;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by chenran on 2017/7/27.
 */

public interface IGiftCore extends IBaseCore {
//    public void requestSynGiftInfos(final GiftCoreImpl.GiftCallback giftCallback);

    List<GiftInfo> getGiftBackpackInfo();

    void requestBackpackGiftInfos();

    List<GiftInfo> getBackpackGiftInfos();

//    List<GiftInfo> requestBackpackGiftInfos(long uid);

    List<GiftInfo> getGiftInfosByType(int type);

//    List<GiftInfo> getGiftInfosByType2And3();

    List<GiftInfo> getBackpackGiftInfosByType2And3And8();

    void sendLotteryMeg(GiftInfo giftInfo, int count);

//    void doSendRoomGift(int giftId, long targetUid, long roomUid, int giftNum, int goldPrice);

//    void doSendRoomMultiGift(int giftId, List<Long> targetUids, long roomUid, int giftNum, int goldPrice);

    /**
     * 送礼物
     *
     * @param giftId
     * @param targetUids
     * @param roomUid
     * @param giftNum
     * @param goldPrice
     * @param sendGiftType 1使用背包礼物接口，2使用普通礼物接口，
     */
    void sendGiftWithGold(int giftId, List<Long> targetUids, long roomUid, int giftNum, int goldPrice, int sendGiftType);

    /**
     * 云信发P2P礼物专用
     *
     * @param giftId
     * @param targetUid
     * @param giftNum
     * @param reference
     * @param chargeListener
     */
    void doSendPersonalGiftToNIMNew(int giftId, List<Long> targetUid, int giftNum, WeakReference<Object> reference, ChargeListener chargeListener, int sendGiftType);

    void sendLotteryMegNew(EggGiftInfo giftInfo, int count);

    void sendPersonalCpGift(int giftId, long targetUid, int giftNum, long roomUid, int type, ChargeListener chargeListener);

    void sendPersonalCpGift(int giftId, long targetUid, int giftNum, long roomUid, ChargeListener chargeListener);

    GiftInfo findGiftInfoById(int giftId);

    void requestGiftInfos();

    void onReceiveChatRoomMessages(List<ChatRoomMessage> messages);
}
