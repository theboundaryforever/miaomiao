package com.tongdaxing.xchat_core.gift;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;

import com.netease.nim.uikit.session.event.SendGiftSuccessEvent;
import com.netease.nim.uikit.session.fragment.MessageFragment;
import com.netease.nim.uikit.session.module.Container;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomMessageConfig;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.common.ICommonClient;
import com.tongdaxing.xchat_core.find.bean.CpUserBean;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NewPushAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.SquareCpMsgAttachment;
import com.tongdaxing.xchat_core.im.room.IIMRoomCore;
import com.tongdaxing.xchat_core.im.room.IIMRoomCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.CpWalletInfo;
import com.tongdaxing.xchat_core.pk.IPKCoreClient;
import com.tongdaxing.xchat_core.result.CpWalletInfoResult;
import com.tongdaxing.xchat_core.result.GiftResult;
import com.tongdaxing.xchat_core.result.WalletInfoResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author chenran
 * @date 2017/7/27
 */

public class GiftCoreImpl extends AbstractBaseCore implements IGiftCore {
    private GiftListInfo giftListInfo;
    //    private GiftListInfo blessingStarInfo;
    private List<GiftInfo> giftBackpackInfo;
    private static List<CustomAttachment> giftQueue;

    public GiftCoreImpl() {
        CoreManager.addClient(this);
        giftListInfo = DemoCache.readGiftList();
        giftBackpackInfo = DemoCache.readGiftBackpackList();
//        blessingStarInfo = DemoCache.readBlessingStarGiftList();
        giftQueue = new ArrayList<>();
        requestGiftInfos();
        requestBackpackGiftInfos();
    }

    private GiftCoreHandler handler = new GiftCoreHandler(Looper.getMainLooper());

    private static void parseChatRoomAttachMent(CustomAttachment attachMent) {
        if (attachMent.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
            GiftAttachment giftAttachment = (GiftAttachment) attachMent;
            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_RECIEVE_GIFT_MSG, giftAttachment.getGiftRecieveInfo());
        } else if (attachMent.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
            MultiGiftAttachment multiGiftAttachment = (MultiGiftAttachment) attachMent;
            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_MULTI_GIFT_MSG, multiGiftAttachment.getMultiGiftRecieveInfo());
        } else if (attachMent.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT) {
            GiftAttachment giftAttachment = (GiftAttachment) attachMent;
            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SUPER_GIFT_MSG, giftAttachment.getGiftRecieveInfo());
        } else if (attachMent.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_CP_TOP) {
            //CP消息 房间顶部横幅 展示CP表白 second: 0表示表白（动效只在2人房间展示） 45-0全服表白横幅 -> 表白所在房间全屏动效
            SquareCpMsgAttachment cpAttachment = (SquareCpMsgAttachment) attachMent;
            CpUserBean inviteUser = cpAttachment.getInviteUser();//cp邀请方信息
            CpUserBean recUser = cpAttachment.getRecUser();//cp接收方信息
            String giftPicUrl = cpAttachment.getPicUrl();
            if (inviteUser != null && recUser != null && !TextUtils.isEmpty(giftPicUrl)) {
                GiftReceiveInfo giftReceiveInfo = new GiftReceiveInfo();
                if (cpAttachment.getDataSecond() == CustomAttachment.CUSTOM_MSG_ROOM_CP_TOP_SECOND_INVITE) {
                    if (cpAttachment.getRoomUid() == AvRoomDataManager.get().mCurrentRoomInfo.getUid()) {
                        giftReceiveInfo.setCpGift(true);//表白动效只在cp2人的房间展示
                    } else {
                        giftReceiveInfo.setCpTopNotify(true);//其他房间只展示表白横幅
                    }
                }
                giftReceiveInfo.setAvatar(inviteUser.getAvatar());
                giftReceiveInfo.setTargetAvatar(recUser.getAvatar());
                giftReceiveInfo.setPicUrl(giftPicUrl);
                giftReceiveInfo.setTargetUid(recUser.getUid());
                giftReceiveInfo.setInviteUserName(inviteUser.getNick());
                giftReceiveInfo.setRecUserName(recUser.getNick());
                giftReceiveInfo.setCpVggUrl(cpAttachment.getVggUrl());
                giftReceiveInfo.setCpNum(cpAttachment.getCpNum());
                CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SUPER_GIFT_MSG, giftReceiveInfo);
            }
        }
    }

    private void addGiftMessage(CustomAttachment attachment) {
    }

    @Override
    public void requestGiftInfos() {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        ResponseListener Listener = new ResponseListener<GiftResult>() {
            @Override
            public void onResponse(GiftResult response) {
                if (response.isSuccess()) {
                    GiftListInfo listInfo = response.getData();
                    if (listInfo != null && listInfo.getGift() != null) {
                        for (int i = 0; i < listInfo.getGift().size(); i++) {
                            listInfo.getGift().get(i).setLocalGiftType(GiftTypeConstant.GIFT_TYPE_LOCAL_NORMAL);//将获取的礼物全部设置为普通礼物
                        }
                        giftListInfo = listInfo;
                        DemoCache.saveGiftList(giftListInfo);
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {

            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getGiftList(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        GiftResult.class, Request.Method.GET);


    }

    private List<GiftInfo> filterNobleGifts(List<GiftInfo> originals) {
        return originals;
    }


    @Override
    public void requestBackpackGiftInfos() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getBackpackGiftList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<GiftInfo>>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<List<GiftInfo>> response) {
                if (response.isSuccess()) {
                    giftBackpackInfo = response.getData();
                    if (giftBackpackInfo != null) {
                        for (int i = 0; i < giftBackpackInfo.size(); i++) {
                            giftBackpackInfo.get(i).setLocalGiftType(GiftTypeConstant.GIFT_TYPE_LOCAL_BACKPACK);//将获取的礼物全部设置为背包礼物类型
                        }
                        DemoCache.saveGiftBackpackList(giftBackpackInfo);
                    }
                }
            }
        });
    }

    @Override
    public List<GiftInfo> getGiftBackpackInfo() {
        return giftBackpackInfo;
    }

    @Override
    public List<GiftInfo> getBackpackGiftInfos() {
        if (giftBackpackInfo != null && giftBackpackInfo.size() > 0) {
            return filterNobleGifts(giftBackpackInfo);
        } else {
            requestBackpackGiftInfos();
        }
        return new ArrayList<>();
    }

    /**
     * 通过类型获取礼物数据
     *
     * @param type 2表示普通礼物，5表示福星礼物,7等级礼物，8表示cp礼物
     * @return
     */
    @Override
    public List<GiftInfo> getGiftInfosByType(int type) {
        if (giftListInfo != null && giftListInfo.getGift() != null && giftListInfo.getGift().size() > 0) {
            List<GiftInfo> giftInfos = new ArrayList<>();
            for (int i = 0; i < giftListInfo.getGift().size(); i++) {
                GiftInfo giftInfo = giftListInfo.getGift().get(i);
                if (giftInfo.getGiftType() == type) {
                    giftInfos.add(giftInfo);
                }
            }
            return filterNobleGifts(giftInfos);
        } else {
            requestGiftInfos();
        }
        return new ArrayList<>();
    }

    @Override
    public List<GiftInfo> getBackpackGiftInfosByType2And3And8() {
        if (giftBackpackInfo != null && giftBackpackInfo.size() > 0) {
            for (int i = giftBackpackInfo.size() - 1; i >= 0; i--) {
                if (giftBackpackInfo.get(i).getGiftType() != GiftTypeConstant.GIFT_TYPE_NORMAL
                        && giftBackpackInfo.get(i).getGiftType() != GiftTypeConstant.GIFT_TYPE_MYSTERY
                        && giftBackpackInfo.get(i).getGiftType() != GiftTypeConstant.GIFT_TYPE_CP) {
                    giftBackpackInfo.remove(i);
                }
            }
            return filterNobleGifts(giftBackpackInfo);
        } else {
            requestBackpackGiftInfos();
        }
        return new ArrayList<>();
    }

    @Override
    public void onReceiveChatRoomMessages(List<ChatRoomMessage> messages) {
        for (ChatRoomMessage msg : messages) {
            giftQueue.add((CustomAttachment) msg.getAttachment());
            if (giftQueue.size() == 1) {//原本已经没有队列在跑了
                handler.removeCallbacksAndMessages(null);
                handler.sendEmptyMessageDelayed(0, 150);
            }
        }
    }

    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void onSendRoomMessageSuccess(ChatRoomMessage msg) {
        if (msg.getMsgType() == MsgTypeEnum.custom) {
            CustomAttachment attachment = (CustomAttachment) msg.getAttachment();
            if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                    || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT)
                addGiftMessage(attachment);
        }
    }

    /**
     * CP赠礼请求
     *
     * @param giftId
     * @param targetUid
     * @param giftNum
     * @param chargeListener
     */
    @Override
    public void sendPersonalCpGift(final int giftId, final long targetUid, final int giftNum, final long roomUid, final int type, final ChargeListener chargeListener) {
        LogUtils.d("sendPersonalCpGift", 2 + "");
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("giftId", giftId + "");
        requestParam.put("targetUid", targetUid + "");
        requestParam.put("uid", uid + "");
        if (roomUid > 0) {
            requestParam.put("roomUid", roomUid + "");
        }
        requestParam.put("giftNum", giftNum + "");
        requestParam.put("ticket", ticket);
        requestParam.put("type", String.valueOf(type));//cp礼物类型为8或9
        ResponseListener Listener = new ResponseListener<CpWalletInfoResult>() {
            @Override
            public void onResponse(CpWalletInfoResult response) {
                if (response.isSuccess()) {
                    CpWalletInfo data = response.getData();
                    CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());

                    if (updateRepertoryCount(giftId, data.getUserGiftPurseNum(), type)) {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift);
                    }

                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_CP_GIFT_SUCCESS);
                } else {
                    if (response.getCode() == 2103) {
                        chargeListener.onNeedCharge();
                    } else if (response.getCode() == 8000) {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_PAST_DUE);
                        requestGiftInfos();
                        requestBackpackGiftInfos();
                    } else if (response.getCode() == 8001) {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_MYSTERY_NOT_ENOUGH);
                    } else {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_CP_GIFT_FAIL, response.getCode(), response.getMessage(),
                                giftId, targetUid, giftNum, roomUid, chargeListener);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_PERSONAL_GIFT_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.sendGiftV3(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        WalletInfoResult.class, Request.Method.POST);
    }

    private void parsePersonalAttachMent(CustomAttachment attachMent) {
        if (attachMent.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
            GiftAttachment giftAttachment = (GiftAttachment) attachMent;
            notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_RECEIVE_PERSONAL_GIFT, giftAttachment.getGiftRecieveInfo());
        }
    }

    private void sendGiftMessage(GiftReceiveInfo giftReceiveInfo, int price) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null || giftReceiveInfo == null) return;
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

        GiftAttachment giftAttachment = new GiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
        giftAttachment.setUid(myUid + "");
        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            giftAttachment.setCharmLevel(userInfo.getCharmLevel());
            giftAttachment.setExperLevel(userInfo.getExperLevel());
        }

        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomInfo.getRoomId() + "",
                giftAttachment
        );

        CoreUtils.send(new RoomTalkEvent.OnRoomMsg(message));
        CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
    }


    private void sendMultiGiftMessage(MultiGiftReceiveInfo giftReceiveInfo, int price) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null && giftReceiveInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            MultiGiftAttachment giftAttachment = new MultiGiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT);
            giftAttachment.setUid(myUid + "");
            giftAttachment.setMultiGiftAttachment(giftReceiveInfo);
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo != null) {
                giftAttachment.setCharmLevel(userInfo.getCharmLevel());
                giftAttachment.setExperLevel(userInfo.getExperLevel());
            }
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    giftAttachment
            );
            CoreUtils.send(new RoomTalkEvent.OnRoomMsg(message));
            // TODO 后期改为服务器发送
            notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_MULTI_GIFT_MSG, giftAttachment.getMultiGiftRecieveInfo());

            CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
        }
    }


    @Override
    public void sendLotteryMeg(GiftInfo giftInfo, int count) {
        if (giftInfo == null)
            return;
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        LotteryBoxAttachment lotteryBoxAttachment = new LotteryBoxAttachment(CustomAttachment.CUSTOM_MSG_LOTTERY_BOX, CustomAttachment.CUSTOM_MSG_LOTTERY_BOX);
        Json json = new Json();
        json.set("giftName", giftInfo.getGiftName());
        json.set("goldPrice", giftInfo.getGoldPrice());
        json.set("count", count);
        json.set("nick", CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getNick());
        json.set("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid());
        lotteryBoxAttachment.setParams(json + "");
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomInfo.getRoomId() + "",
                lotteryBoxAttachment
        );
        CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
    }

    @Override
    public void sendLotteryMegNew(EggGiftInfo giftInfo, int count) {
        if (giftInfo == null)
            return;
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        NewPushAttachment newPushAttachment = new NewPushAttachment(CustomAttachment.CUSTOM_MSG_TYPE_NEW_PUSH_FIRST,
                CustomAttachment.CUSTOM_MSG_TYPE_NEW_PUSH_FIRST);
        Json json = new Json();
        json.set("giftName", giftInfo.getGiftName());
        json.set("goldPrice", giftInfo.getGoldPrice());
        json.set("count", count);
        json.set("nick", CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getNick());
        json.set("isLocal", true);
        newPushAttachment.setParams(json + "");
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomInfo.getRoomId() + "",
                newPushAttachment
        );
        CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
    }

    /**
     * 私聊送礼
     *
     * @param giftId
     * @param targetUids
     * @param giftNum
     * @param reference
     * @param chargeListener
     * @param sendGiftType
     */
    @Override
    public void doSendPersonalGiftToNIMNew(final int giftId, final List<Long> targetUids, final int giftNum,
                                           final WeakReference<Object> reference,
                                           final ChargeListener chargeListener, final int sendGiftType) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("giftId", giftId + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("giftNum", giftNum + "");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < targetUids.size(); i++) {
            long targetUid = targetUids.get(i);
            sb.append(targetUid + "");
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        requestParam.put("recvUids", sb.toString());
        requestParam.put("type", "2");//送给其他人

        if (sendGiftType == GiftTypeConstant.GIFT_TYPE_CP) {//sendGiftType为GiftTypeConstant.GIFT_TYPE_CP或GIFT_TYPE_RESEND_CP时注意区分
            requestParam.put("type", String.valueOf(GiftTypeConstant.GIFT_TYPE_CP));//便于服务端知道这是发起背包的cp礼物邀请
        } else if (sendGiftType == GiftTypeConstant.GIFT_TYPE_RESEND_CP) {
            requestParam.put("type", String.valueOf(GiftTypeConstant.GIFT_TYPE_RESEND_CP));//便于服务端知道这是【重新】发起背包的cp礼物邀请
        }

        String url;
        if (sendGiftType == 0 || sendGiftType == 2 || sendGiftType == 3) {//0是普通礼物，2是福星礼物，3是等级礼物，它们都是扣金币
            url = UriProvider.sendToUserWithGold();
        } else {
            url = UriProvider.sendToUserWithRepertory();//背包礼物，它们都是扣库存
        }
        OkHttpManager.getInstance().doPostRequest(url,
                requestParam, new OkHttpManager.MyCallBack<ServiceResult<MultiGiftReceiveInfo>>() {
                    @Override
                    public void onError(Exception e) {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_PERSONAL_GIFT_FAIL, 0);
                    }

                    @Override
                    public void onResponse(ServiceResult<MultiGiftReceiveInfo> response) {
                        if (response == null) {
                            return;
                        }
                        if (response.isSuccess()) {
                            UserInfo myUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                            if (myUserInfo == null) {
                                return;
                            }

                            MultiGiftReceiveInfo data = response.getData();
                            if (sendGiftType == GiftTypeConstant.GIFT_TYPE_CP || sendGiftType == GiftTypeConstant.GIFT_TYPE_RESEND_CP) {
                                //用背包cp发起cp邀请
                                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_CP_GIFT_SUCCESS);
                            } else {
                                GiftReceiveInfo giftRecieveInfo = new GiftReceiveInfo();
                                giftRecieveInfo.setNick(myUserInfo.getNick());
                                giftRecieveInfo.setTargetUid(targetUids.get(0));
                                giftRecieveInfo.setAvatar(myUserInfo.getAvatar());
                                giftRecieveInfo.setGiftId(giftId);
                                giftRecieveInfo.setUid(myUserInfo.getUid());
                                giftRecieveInfo.setGiftNum(giftNum);
                                LogUtils.d("doSendRoomGift", data + "");
                                GiftAttachment giftAttachment = new GiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
                                giftAttachment.setUid(myUserInfo.getUid() + "");
                                giftAttachment.setGiftRecieveInfo(giftRecieveInfo);
                                giftAttachment.setCharmLevel(myUserInfo.getCharmLevel());
                                giftAttachment.setExperLevel(myUserInfo.getExperLevel());
                                CustomMessageConfig customMessageConfig = new CustomMessageConfig();
                                customMessageConfig.enablePush = false;
                                IMMessage imMessage = MessageBuilder.createCustomMessage(targetUids.get(0) + "", SessionTypeEnum.P2P,
                                        "", giftAttachment, customMessageConfig);
                                if (reference == null || reference.get() == null) {
                                    NIMClient.getService(MsgService.class).sendMessage(imMessage, false);
                                } else {
                                    if (reference.get() instanceof Container) {
                                        Container container = (Container) reference.get();
                                        container.proxy.sendMessage(imMessage);
                                    } else if (reference.get() instanceof MessageFragment) {
                                        ((MessageFragment) reference.get()).sendMessage(imMessage);
                                    }
                                }
                                RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                                if (roomInfo != null) {
                                    ChatRoomMessage chatRoomMessage = ChatRoomMessageBuilder.createChatRoomCustomMessage(String.valueOf(roomInfo.getRoomId()), giftAttachment);
                                    CoreUtils.send(new RoomTalkEvent.OnRoomMsg(chatRoomMessage));
                                }

                                parsePersonalAttachMent((CustomAttachment) imMessage.getAttachment());
                                CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
                            }

                            if (updateRepertoryCount(giftId, data.getUserGiftPurseNum(), sendGiftType)) {
                                notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift);
                            }

                            notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_GIFT_SUCCESS, giftId, targetUids.get(0), giftNum);
                            CoreUtils.send(new SendGiftSuccessEvent(data.getUseGiftPurseGold()));//用于更新私聊页的亲密数值
                        } else {
                            if (response.getCode() == 2103) {
                                chargeListener.onNeedCharge();
//                        notifyClients(ICommonClient.class, ICommonClient.METHOD_ON_RECIEVE_NEED_RECHARGE);
                            } else if (response.getCode() == 8000) {
                                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_PAST_DUE);
                                requestGiftInfos();
                                requestBackpackGiftInfos();
                            } else if (response.getCode() == 8001) {
                                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_MYSTERY_NOT_ENOUGH);
                            } else if (response.getCode() == 10410) {
                                //从背包礼物中重新发起cp邀请
                                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_BACKPACK_CP_GIFT_FAIL, response.getCode(), response.getMessage(),
                                        giftId, targetUids.get(0));
                            } else {
                                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_GIFT_FAIL, response.getMessage());
                            }
                        }
                    }
                });
    }

    @Override
    public void sendPersonalCpGift(final int giftId, final long targetUid, final int giftNum, final long roomUid, final ChargeListener chargeListener) {
        sendPersonalCpGift(giftId, targetUid, giftNum, roomUid, GiftTypeConstant.GIFT_TYPE_CP, chargeListener);
    }

    @Override
    public void sendGiftWithGold(final int giftId, final List<Long> targetUids, final long roomUid, final int giftNum, final int goldPrice, final int sendGiftType) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("giftId", giftId + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("roomUid", roomUid + "");
        requestParam.put("giftNum", giftNum + "");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < targetUids.size(); i++) {
            long targetUid = targetUids.get(i);
            sb.append(targetUid + "");
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        requestParam.put("recvUids", sb.toString());
        if (targetUids.size() > 1) {
            requestParam.put("type", "5");//全麦
        } else {
            if (targetUids.get(0) == roomUid) {
                requestParam.put("type", "1");//送给房主
            } else {
                requestParam.put("type", "3");//送给其他人
            }
        }
        if (sendGiftType == GiftTypeConstant.GIFT_TYPE_CP) {//sendGiftType为GiftTypeConstant.GIFT_TYPE_CP或GIFT_TYPE_RESEND_CP时注意区分
            requestParam.put("type", String.valueOf(GiftTypeConstant.GIFT_TYPE_CP));//便于服务端知道这是发起背包的cp礼物邀请
        } else if (sendGiftType == GiftTypeConstant.GIFT_TYPE_RESEND_CP) {
            requestParam.put("type", String.valueOf(GiftTypeConstant.GIFT_TYPE_RESEND_CP));//便于服务端知道这是【重新】发起背包的cp礼物邀请
        }

        String url;
        if (sendGiftType == 0 || sendGiftType == 2 || sendGiftType == 3) {//0是普通礼物，2是福星礼物，3是等级礼物，它们都是扣金币
            url = UriProvider.sendToUserWithGold();
        } else {
            url = UriProvider.sendToUserWithRepertory();//背包礼物，它们都是扣库存
        }
        OkHttpManager.getInstance().doPostRequest(url, requestParam, new OkHttpManager.MyCallBack<ServiceResult<MultiGiftReceiveInfo>>() {
            @Override
            public void onError(Exception e) {
                if (targetUids.size() == 1) {
                    notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, e.getMessage());
                    notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_GIFT_FAIL, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<MultiGiftReceiveInfo> response) {
                if (response == null) {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_GIFT_FAIL, "赠送失败");
                    return;
                }
                MultiGiftReceiveInfo data = response.getData();
                if (response.isSuccess() && data != null) {
                    if (sendGiftType == GiftTypeConstant.GIFT_TYPE_CP || sendGiftType == GiftTypeConstant.GIFT_TYPE_RESEND_CP) {
                        //用背包cp发起cp邀请
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_CP_GIFT_SUCCESS);
                    } else {
                        // : 2018/3/1 送礼物成功
                        int price = goldPrice * giftNum * targetUids.size();
                        CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
                        if (targetUids.size() > 1) {
                            sendMultiGiftMessage(data, price);//发送多人礼物
                        } else {
                            GiftReceiveInfo giftReceiveInfo = convertData(data);
                            sendGiftMessage(giftReceiveInfo, price);
                            // TODO 特效动画，自己发的那个房间服务器没有广播，客户端自己发，后期要改为服务器去发
                            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_RECIEVE_GIFT_MSG, giftReceiveInfo);
                        }
                        notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_MULTI_GIFT, targetUids);
                    }

                    if (updateRepertoryCount(giftId, data.getUserGiftPurseNum(), sendGiftType)) {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift);
                    }
                } else if (response.getCode() == 2103) {
                    notifyClients(ICommonClient.class, ICommonClient.METHOD_ON_RECIEVE_NEED_RECHARGE);
                } else if (response.getCode() == 8000) {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_PAST_DUE);
                    requestGiftInfos();
                    requestBackpackGiftInfos();
                } else if (response.getCode() == 8001) {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_MYSTERY_NOT_ENOUGH);
                } else if (response.getCode() == 10410) {
                    //从背包礼物中重新发起cp邀请
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_BACKPACK_CP_GIFT_FAIL, response.getCode(), response.getMessage(),
                            giftId, targetUids.get(0));
                } else {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_GIFT_FAIL, response.getMessage());
                }
            }
        });
    }

    private GiftReceiveInfo convertData(MultiGiftReceiveInfo data) {
        GiftReceiveInfo giftReceiveInfo = null;
        if (!ListUtils.isListEmpty(data.getAvatarList()) ||
                !ListUtils.isListEmpty(data.getNickList()) ||
                !ListUtils.isListEmpty(data.getTargetUids())) {
            giftReceiveInfo = new GiftReceiveInfo();
            giftReceiveInfo.setAvatar(data.getAvatar());
            giftReceiveInfo.setGiftId(data.getGiftId());
            giftReceiveInfo.setGiftNum(data.getGiftNum());
            giftReceiveInfo.setNick(data.getNick());
            giftReceiveInfo.setUid(data.getUid());
            giftReceiveInfo.setTargetAvatar(data.getAvatarList().get(0));
            giftReceiveInfo.setTargetUid(data.getTargetUids().get(0));
            giftReceiveInfo.setTargetNick(data.getNickList().get(0));
            giftReceiveInfo.setUseGiftPurseGold(data.getUseGiftPurseGold());
            giftReceiveInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
        }
        return giftReceiveInfo;
    }

    /**
     * 更新库存数量
     *
     * @param giftId
     * @param userGiftPurseNum
     * @param sendGiftType
     * @return
     */
    private boolean updateRepertoryCount(int giftId, int userGiftPurseNum, int sendGiftType) {
        if (sendGiftType == 0 || sendGiftType == 2 || sendGiftType == 3) {
            return true;
        }
        boolean needRefresh = false;
        List<GiftInfo> giftInfos = CoreManager.getCore(IGiftCore.class).getBackpackGiftInfosByType2And3And8();
        if (giftInfos != null) {
            for (int i = 0; i < giftInfos.size(); i++) {
                GiftInfo giftInfo = giftInfos.get(i);
                if (giftInfo == null) {
                    continue;
                }
                if (giftInfo.getGiftId() == giftId) {
                    if (userGiftPurseNum < 1) {
                        giftInfos.remove(i);
                    } else {
                        giftInfo.setUserGiftPurseNum(userGiftPurseNum);
                    }
                    needRefresh = true;
                }
            }
        }
        return needRefresh;
    }

    static class GiftCoreHandler extends Handler {
        public GiftCoreHandler() {
        }

        public GiftCoreHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (giftQueue.size() > 0) {
                CustomAttachment attachment = giftQueue.remove(0);
                if (attachment != null) {
                    parseChatRoomAttachMent(attachment);
                }
            }

            if (giftQueue.size() > 0) {
                sendEmptyMessageDelayed(0, 150);
            }
        }
    }

    @Override
    public GiftInfo findGiftInfoById(int giftId) {
        GiftInfo giftInfo = null;
        if (giftListInfo != null && !ListUtils.isListEmpty(giftListInfo.getGift())) {
            for (int i = 0; i < giftListInfo.getGift().size(); i++) {
                if (giftListInfo.getGift().get(i).getGiftId() == giftId) {
                    giftInfo = giftListInfo.getGift().get(i);
                    return giftInfo;
                }
            }
        }

        if (giftInfo == null) {
            if (giftBackpackInfo != null && !ListUtils.isListEmpty(giftBackpackInfo)) {
                for (int i = 0; i < giftBackpackInfo.size(); i++) {
                    if (giftBackpackInfo.get(i).getGiftId() == giftId) {
                        giftInfo = giftBackpackInfo.get(i);
                        return giftInfo;
                    }
                }
            }
        }
        return giftInfo;
    }

}
