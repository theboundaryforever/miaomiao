package com.tongdaxing.xchat_core.withdraw;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.ExchangeInfoResult;
import com.tongdaxing.xchat_core.result.WithdrawListResult;
import com.tongdaxing.xchat_core.result.WithdrawUserInfoResult;
import com.tongdaxing.xchat_core.withdraw.bean.ExchangerInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/24.
 */

public class WithdrawCoreImpl extends AbstractBaseCore implements IWithdrawCore {
    //获取提现列表
    @Override
    public void getWithdrawList() {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        ResponseListener responseListener = new ResponseListener<WithdrawListResult>() {
            @Override
            public void onResponse(WithdrawListResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_LIST, response.getData());
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_LIST_FAIL, response.getMessage());
                    }
                } else {
                    notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_LIST_FAIL);
                }
            }
        };

        ResponseErrorListener responseErrorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_LIST_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getWithdrawList(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param,
                responseListener,
                responseErrorListener,
                WithdrawListResult.class,
                Request.Method.GET
        );
    }

    //获取提现页用户信息
    @Override
    public void getWithdrawUserInfo(long uid) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));
        ResponseListener responseListener = new ResponseListener<WithdrawUserInfoResult>() {
            @Override
            public void onResponse(WithdrawUserInfoResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_USER_INFO, response.getData());
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_USER_INFO_FAIL, response.getMessage());
                    }
                } else {
                    notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_USER_INFO_FAIL);
                }
            }
        };

        ResponseErrorListener responseErrorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {

                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_USER_INFO_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getWithdrawInfo(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param,
                responseListener,
                responseErrorListener,
                WithdrawUserInfoResult.class,
                Request.Method.GET
        );
    }

    @Override
    public void getWithdrawBindingInfo(long uid) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));
        OkHttpManager.getInstance().doPostRequest(UriProvider.getBindingInfo(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(Json response) {

            }
        });
    }

    //发起兑换
    @Override
    public void requestExchange(long uid, int pid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("pid", String.valueOf(pid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener responseListener = new ResponseListener<ExchangeInfoResult>() {
            @Override
            public void onResponse(ExchangeInfoResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE, response.getData());
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE_FAIL, response.getMessage());
                    }
                } else {
                    notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE_FAIL);
                }
            }
        };

        ResponseErrorListener responseErrorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.requestExchange(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                responseListener,
                responseErrorListener,
                ExchangeInfoResult.class,
                Request.Method.POST
        );
    }

    @Override
    public void requestExchangeV2(long uid, int pid, int type) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("type", String.valueOf(type));
        params.put("pid", String.valueOf(pid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.withdrawV2(), params, new OkHttpManager.MyCallBack<ServiceResult<ExchangerInfo>>() {
            @Override
            public void onError(Exception e) {
                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<ExchangerInfo> response) {
                if (response != null && response.isSuccess()) {
                    notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE, response.getData());
                } else {
                    if (response != null) {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE_FAIL, response.getMessage());
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE_FAIL);
                    }
                }
            }
        });
    }

    //获取绑定支付宝验证码
    @Override
    public void getSmsCode(long uid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_SMS_CODE_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_SMS_CODE_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getSms(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);
    }

    //绑定支付宝
    @Override
    public void binderAlipay(String aliPayAccount, String aliPayAccountName, String code) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("aliPayAccount", aliPayAccount);
        params.put("aliPayAccountName", aliPayAccountName);
        params.put("code", code);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener responseListener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_BINDER_ALIPAY);
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_BINDER_ALIPAY_FAIL, response.getMessage());
                    }
                }
            }
        };

        ResponseErrorListener responseErrorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_BINDER_ALIPAY_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.binder(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                responseListener,
                responseErrorListener,
                ServiceResult.class,
                Request.Method.POST
        );
    }

    @Override
    public void refreshOnGetWithdrawUserInfo(WithdrawInfo withdrawInfo) {
        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_USER_INFO, withdrawInfo);
    }


}
