package com.tongdaxing.xchat_core.withdraw.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/7/25.
 */

public class WithdrawInfo implements Serializable {

    /*
        uid:90000
		alipayAccount:xxx //支付宝账号
		alipayAccountName:xxx //支付宝姓名
		diamondNum:钻石余额
		isNotBoundPhone:true//没有绑定手机，绑定了手机不返回该字段
     */
    public long uid;
    public String alipayAccount;
    public double diamondNum;
    public String alipayAccountName;

    public boolean isNotBoundPhone;
    public String wxPhone; //微信-手机号
    public String wxRealName;  //微信-真实姓名
    public String wxNickName; //微信-昵称
    public int payment;  //支付方式-0未选择, 1 微信, 2 支付宝

    @Override
    public String toString() {
        return "WithdrawInfo{" +
                "uid=" + uid +
                ", alipayAccount='" + alipayAccount + '\'' +
                ", diamondNum=" + diamondNum +
                ", alipayAccountName='" + alipayAccountName + '\'' +
                ", isNotBoundPhone=" + isNotBoundPhone +
                ", wxPhone='" + wxPhone + '\'' +
                ", wxRealName='" + wxRealName + '\'' +
                ", wxNickName='" + wxNickName + '\'' +
                ", payment=" + payment +
                '}';
    }
}
