package com.tongdaxing.xchat_core.withdraw.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Function:
 * Author: Edward on 2019/1/9
 */
public class WxVerifyInfo implements Parcelable {

    /**
     * phone : 13427561170
     * realName : 李志辉
     * uid : 107607
     */

    private String phone;
    private String realName;
    private int uid;
    private String verifyCode;


    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "WxVerifyInfo{" +
                "phone='" + phone + '\'' +
                ", realName='" + realName + '\'' +
                ", uid=" + uid +
                ", verifyCode='" + verifyCode + '\'' +
                '}';
    }

    public WxVerifyInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.phone);
        dest.writeString(this.realName);
        dest.writeInt(this.uid);
        dest.writeString(this.verifyCode);
    }

    protected WxVerifyInfo(Parcel in) {
        this.phone = in.readString();
        this.realName = in.readString();
        this.uid = in.readInt();
        this.verifyCode = in.readString();
    }

    public static final Creator<WxVerifyInfo> CREATOR = new Creator<WxVerifyInfo>() {
        @Override
        public WxVerifyInfo createFromParcel(Parcel source) {
            return new WxVerifyInfo(source);
        }

        @Override
        public WxVerifyInfo[] newArray(int size) {
            return new WxVerifyInfo[size];
        }
    };
}
