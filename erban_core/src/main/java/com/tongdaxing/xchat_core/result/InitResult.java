package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.initial.InitInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * @author xiaoyu
 * @date 2017/12/8
 */

public class InitResult extends ServiceResult<InitInfo> {
}
