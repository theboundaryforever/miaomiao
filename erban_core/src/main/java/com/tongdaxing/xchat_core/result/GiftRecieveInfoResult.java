package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * Created by chenran on 2017/11/25.
 */

public class GiftRecieveInfoResult extends ServiceResult<GiftReceiveInfo> {
}
