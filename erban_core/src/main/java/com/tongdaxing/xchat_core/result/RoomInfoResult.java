package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * Created by chenran on 2017/10/18.
 */

public class RoomInfoResult extends ServiceResult<RoomInfo> {
}
