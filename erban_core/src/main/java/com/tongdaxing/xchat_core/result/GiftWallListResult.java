package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * Created by chenran on 2017/10/17.
 */

public class GiftWallListResult extends ServiceResult<List<GiftWallInfo>> {
}
