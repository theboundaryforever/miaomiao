package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.pay.bean.CpWalletInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * Created by zhangjian on 2019/3/28.
 */

public class CpWalletInfoResult extends ServiceResult<CpWalletInfo> {
}
