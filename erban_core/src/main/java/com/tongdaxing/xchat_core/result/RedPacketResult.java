package com.tongdaxing.xchat_core.result;


import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * Created by zhouxiangfeng on 2017/5/28.
 */

public class RedPacketResult extends ServiceResult<RedPacketInfo> {
}
