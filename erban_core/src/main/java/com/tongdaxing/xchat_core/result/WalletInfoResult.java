package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * Created by zhouxiangfeng on 2017/5/4.
 */

public class WalletInfoResult extends ServiceResult<WalletInfo> {

}
