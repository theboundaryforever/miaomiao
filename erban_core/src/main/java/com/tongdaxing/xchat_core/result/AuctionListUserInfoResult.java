package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.room.auction.bean.AuctionListUserInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * Created by chenran on 2017/8/9.
 */

public class AuctionListUserInfoResult extends ServiceResult<List<AuctionListUserInfo>> {
}
