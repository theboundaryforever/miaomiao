package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.redpacket.bean.RedDrawListInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * Created by chenran on 2017/12/4.
 */

public class RedDrawListInfoResult extends ServiceResult<List<RedDrawListInfo>> {
}
