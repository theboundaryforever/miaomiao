package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.home.RankingInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * <p> 首页排行信息 </p>
 * Created by Administrator on 2017/11/8.
 */
public class RankingResult extends ServiceResult<RankingInfo> {
}
