package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * Created by zhouxiangfeng on 2017/5/4.
 */

public class UserListResult extends ServiceResult<List<UserInfo>> {

}
