package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * Created by Administrator on 2017/7/8 0008.
 */

public class RoomListResult extends ServiceResult<List<RoomInfo>> {
}
