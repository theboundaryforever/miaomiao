package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.user.bean.AttentionInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * Created by Administrator on 2017/7/5 0005.
 */

public class AttentionListResult extends ServiceResult<List<AttentionInfo>> {
}
