package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.room.auction.bean.AuctionInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * Created by zhouxiangfeng on 2017/5/28.
 */

public class AuctionInfoResult extends ServiceResult<AuctionInfo> {
}
