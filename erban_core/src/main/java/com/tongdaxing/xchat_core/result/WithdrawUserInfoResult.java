package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * Created by zhouxiangfeng on 2017/5/4.
 */

public class WithdrawUserInfoResult extends ServiceResult<WithdrawInfo> {

}
