package com.tongdaxing.xchat_core.result;


import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * Created by zhouxiangfeng on 2017/5/28.
 */

public class ActionDialogResult extends ServiceResult<List<ActionDialogInfo>> {
}
