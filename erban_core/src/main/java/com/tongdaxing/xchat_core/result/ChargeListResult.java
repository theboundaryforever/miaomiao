package com.tongdaxing.xchat_core.result;

import com.tongdaxing.xchat_core.pay.bean.ChargeBean;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * Created by zhouxiangfeng on 2017/5/4.
 */

public class ChargeListResult extends ServiceResult<List<ChargeBean>> {

}
