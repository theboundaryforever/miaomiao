package com.tongdaxing.xchat_core.room.weekcp.tacit;

import com.tongdaxing.xchat_core.room.weekcp.bean.TacitAnswerBean;
import com.tongdaxing.xchat_core.room.weekcp.bean.TacitTestBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2019/5/9.
 */
public class TacitTextInfo {
    private List<TacitTestBean.DataBean.QuestionBean> mDataBeans = new ArrayList<>();    // 没有开始回答的默契考验的题目
    private List<String> mQuestionAnswer = new ArrayList<>();   // 回答的问题答案
    private TacitAnswerBean.DataBean mTacitAnswerBean;  // 推送回来的双方题目答案
    private int mCurrentState;          // 当前回答问题的状态
    private long mQuestionId;

    public List<TacitTestBean.DataBean.QuestionBean> getDataBeans() {
        return mDataBeans;
    }

    public void setDataBeans(List<TacitTestBean.DataBean.QuestionBean> dataBeans) {
        reset();
        mDataBeans.addAll(dataBeans);
    }

    private void reset() {
        mDataBeans.clear();
        mQuestionAnswer.clear();
        mTacitAnswerBean = null;
    }

    public List<String> getQuestionAnswer() {
        return mQuestionAnswer;
    }

    public void setQuestionAnswer(List<String> questionAnswer) {
        mQuestionAnswer = questionAnswer;
    }

    public TacitAnswerBean.DataBean getTacitAnswerBean() {
        return mTacitAnswerBean;
    }

    public void setTacitAnswerBean(TacitAnswerBean.DataBean tacitAnswerBean) {
        mTacitAnswerBean = tacitAnswerBean;
    }

    public int getCurrentState() {
        return mCurrentState;
    }

    public void setCurrentState(int currentState) {
        mCurrentState = currentState;
    }

    public long getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(long questionId) {
        mQuestionId = questionId;
    }
}
