package com.tongdaxing.xchat_core.room.bean;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/1/17
 */
public class RoomStatus {

    /**
     * abChannelType : 1
     * alarmEnable : true
     * audioLevel : 0
     * backPic :
     * calcSumDataIndex : 0
     * charmOpen : 0
     * charmSwitch : 0
     * count : 0
     * exceptionClose : false
     * factor : 0
     * giftEffectSwitch : 0
     * hideFace : []
     * isExceptionClose : false
     * isPermitRoom : 2
     * isRecom : 0
     * meetingName : abc9e44c7e354dbfb88258154bb99a3e
     * officeUser : 1
     * openTime : 1546436826000
     * operatorStatus : 1
     * publicChatSwitch : 0
     * roomDesc :
     * roomId : 60505648
     * roomPwd :
     * roomTag : 听歌
     * tagId : 4
     * tagPict : https://pic.miaomiaofm.com/room_tag_music.png
     * timeInterval : 5000
     * title : 小可乐的房间
     * type : 3
     * uid : 109614
     * valid : true
     */
    private String avatar;
    private int abChannelType;
    private boolean alarmEnable;
    private int audioLevel;
    private String backPic;
    private int calcSumDataIndex;
    private int charmOpen;
    private int charmSwitch;
    private int count;
    private boolean exceptionClose;
    private int factor;
    private int giftEffectSwitch;
    private boolean isExceptionClose;
    private int isPermitRoom;
    private int isRecom;
    private String meetingName;
    private int officeUser;
    private long openTime;
    private int operatorStatus;
    private int onlineNum = 0;
    private int publicChatSwitch;
    private String roomDesc;
    private int roomId;
    private String roomPwd;
    private String roomTag;
    private int tagId;
    private String tagPict;
    private int timeInterval;
    private String title;
    private int type;
    private int uid;
    private boolean valid;
//    private List<Integer> hideFace;//没有用到这个字段，后台也没返回此数据，先暂时隐藏

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getAbChannelType() {
        return abChannelType;
    }

    public void setAbChannelType(int abChannelType) {
        this.abChannelType = abChannelType;
    }

    public boolean isAlarmEnable() {
        return alarmEnable;
    }

    public void setAlarmEnable(boolean alarmEnable) {
        this.alarmEnable = alarmEnable;
    }

    public int getAudioLevel() {
        return audioLevel;
    }

    public void setAudioLevel(int audioLevel) {
        this.audioLevel = audioLevel;
    }

    public String getBackPic() {
        return backPic;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }

    public int getCalcSumDataIndex() {
        return calcSumDataIndex;
    }

    public void setCalcSumDataIndex(int calcSumDataIndex) {
        this.calcSumDataIndex = calcSumDataIndex;
    }

    public int getCharmOpen() {
        return charmOpen;
    }

    public void setCharmOpen(int charmOpen) {
        this.charmOpen = charmOpen;
    }

    public int getCharmSwitch() {
        return charmSwitch;
    }

    public void setCharmSwitch(int charmSwitch) {
        this.charmSwitch = charmSwitch;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isExceptionClose() {
        return exceptionClose;
    }

    public void setExceptionClose(boolean exceptionClose) {
        this.exceptionClose = exceptionClose;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public int getGiftEffectSwitch() {
        return giftEffectSwitch;
    }

    public void setGiftEffectSwitch(int giftEffectSwitch) {
        this.giftEffectSwitch = giftEffectSwitch;
    }

    public boolean isIsExceptionClose() {
        return isExceptionClose;
    }

    public void setIsExceptionClose(boolean isExceptionClose) {
        this.isExceptionClose = isExceptionClose;
    }

    public int getIsPermitRoom() {
        return isPermitRoom;
    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }

    public int getIsRecom() {
        return isRecom;
    }

    public void setIsRecom(int isRecom) {
        this.isRecom = isRecom;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public int getOfficeUser() {
        return officeUser;
    }

    public void setOfficeUser(int officeUser) {
        this.officeUser = officeUser;
    }

    public long getOpenTime() {
        return openTime;
    }

    public void setOpenTime(long openTime) {
        this.openTime = openTime;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public int getPublicChatSwitch() {
        return publicChatSwitch;
    }

    public void setPublicChatSwitch(int publicChatSwitch) {
        this.publicChatSwitch = publicChatSwitch;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getRoomPwd() {
        return roomPwd;
    }

    public void setRoomPwd(String roomPwd) {
        this.roomPwd = roomPwd;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public int getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

//    public List<Integer> getHideFace() {
//        return hideFace;
//    }
//
//    public void setHideFace(List<Integer> hideFace) {
//        this.hideFace = hideFace;
//    }
}
