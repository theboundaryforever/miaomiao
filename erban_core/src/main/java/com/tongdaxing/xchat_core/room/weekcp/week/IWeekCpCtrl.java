package com.tongdaxing.xchat_core.room.weekcp.week;


import com.tongdaxing.xchat_core.room.bean.RoomCpTicket;

/**
 * Created by Chen on 2019/4/30.
 */
public interface IWeekCpCtrl {
    int ROOM_CP_MAX_VALUE = 60;

    void requestRoomCpHeart(RoomCpTicket cpTicket);

    // 增加心动值
    void requestAddCpHeart(RoomCpTicket cpTicket);

    // 心动值校验
    void requestCheckCpHeart();

    // 一周CP
    void requestWeekCp(RoomCpTicket cpTicket);

    // 解除一周CP
    void unbandWeekCp(RoomCpTicket cpTicket);

    // 一周CP排行榜
    void requestWeekRank(RoomCpTicket cpTicket);

    void checkCpInfo(String queryId);

}
