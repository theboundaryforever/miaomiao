package com.tongdaxing.xchat_core.room.talk.message;

import com.tongdaxing.xchat_core.im.custom.bean.PersonalAttentionAttachment;
import com.tongdaxing.xchat_core.room.talk.TalkType;

/**
 * Created by Chen on 2019/6/27.
 */
public class AttentionMessage implements IRoomTalkMessage {
    private String mLocalId;
    private PersonalAttentionAttachment mAttentionAttachment;

    public AttentionMessage() {
        setLocalId(System.currentTimeMillis() + "");
    }

    @Override
    public int getRoomTalkType() {
        return TalkType.ROOM_TALK_HEADER_TYPE_ATTENTION;
    }

    public PersonalAttentionAttachment getAttentionAttachment() {
        return mAttentionAttachment;
    }

    public void setAttentionAttachment(PersonalAttentionAttachment attentionAttachment) {
        this.mAttentionAttachment = attentionAttachment;
    }

    @Override
    public String getLocalId() {
        return mLocalId;
    }

    @Override
    public void setLocalId(String id) {
        mLocalId = id;
    }
}
