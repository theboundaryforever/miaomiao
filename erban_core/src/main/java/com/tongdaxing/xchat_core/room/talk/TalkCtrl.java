package com.tongdaxing.xchat_core.room.talk;

import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.base.BaseCtrl;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.BurstGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRuleAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.msg.Message;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.talk.message.CommonMessage;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;
import com.tongdaxing.xchat_core.room.talk.message.TipMessage;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_TYPE_BURST_GIFT;

/**
 * Created by Chen on 2019/4/19.
 */
public class TalkCtrl extends BaseCtrl implements ITalkCtrl {
    private String TAG = "TalkCtrl";
    private List<IRoomTalkMessage> mTalkMessageList = new CopyOnWriteArrayList<>();
    private final int MAX_MESSAGE_COUNT = 500;
    private boolean mIsEnterRoom;
    private List<ChatRoomMessage> mGiftMsgList = new CopyOnWriteArrayList<>();

    public TalkCtrl() {
        CoreUtils.register(this);
    }

    public TipMessage getFirstMessageContent() {
        String content = "封面、背景及内容含低俗、引导、暴露等都会被屏蔽处理，泄露用户隐私、导流第三方平台、欺诈用户等将被封号处理。";
        TipMessage message = new TipMessage();
        message.setContent(content);
        return message;
    }

    @Override
    public void onEnterRoom(Object response) {
        super.onEnterRoom(response);
    }

    @Override
    public void onLeaveRoom() {
        super.onLeaveRoom();
        reset();
    }

    @Override
    public void reset() {
        mIsEnterRoom = false;
        mTalkMessageList.clear();
        mGiftMsgList.clear();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onEnterRoomSuccess(NewRoomEvent.OnEnterRoomSuccess roomSuccess) {
        L.info(TAG, "onEnterRoomSuccess");
        if (!mIsEnterRoom) {
            addDefaultMessage();
        }
        mIsEnterRoom = true;
    }

    private void addDefaultMessage() {
        mTalkMessageList.clear();
        mGiftMsgList.clear();
        // 房间第一条提示
        TipMessage messageContent = getFirstMessageContent();
        mTalkMessageList.add(messageContent);
        // 房间规则
        sendRoomRulesMessage();
        CoreUtils.send(new RoomTalkEvent.OnAddDefaultMessageFinish());
    }

    public void sendRoomRulesMessage() {
        RoomInfo roomInfo = AvRoomDataManager.get().mServiceRoominfo;
        if (roomInfo == null) {
            return;
        }
        if (StringUtils.isEmpty(roomInfo.getPlayInfo())){
            return;
        }
        RoomRuleAttachment rules = new RoomRuleAttachment(CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST, CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST);
        rules.setRule(roomInfo.getPlayInfo());
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomInfo.getRoomId() + "",
                // 自定义消息
                rules
        );
        CommonMessage commonMessage = new CommonMessage();
        commonMessage.setChatRoomMessage(message);
        mTalkMessageList.add(commonMessage);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onExitRoom(NewRoomEvent.OnExitRoom exitRoom) {
        String account = exitRoom.getAccount();
        L.info(TAG, "onExitRoom account: %s", account);
        if (AvRoomDataManager.get().isSelf(account)) {
            reset();
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onTalkMessageAccept(RoomTalkEvent.OnTalkMessageAccept accept) {
        L.info(TAG, "onTalkMessageAccept");
        IRoomTalkMessage message = accept.getMessage();
        if (message != null) {
            if (mTalkMessageList.size() >= MAX_MESSAGE_COUNT) {
                mTalkMessageList.remove(0);
            }
            mTalkMessageList.add(message);
            CoreUtils.send(new RoomTalkEvent.OnRoomShowTalkMessage(message));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoomMsgCome(RoomTalkEvent.OnRoomMsg roomMsg) {
        ChatRoomMessage message = roomMsg.getMessage();
        CustomAttachment attachment = (CustomAttachment) message.getAttachment();
        if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT ||
                attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
            L.info(TAG, "onRoomMsgCome");
            mGiftMsgList.add(0, message);
        }
    }

    @Override
    public void forbidSpeak(long playerId, int forbidTime) {
        L.info(TAG, "forbidSpeak playerId: %d, forbidTime: %d", playerId, forbidTime);
    }

    @Override
    public void sendChat(Message message) {

    }

    @Override
    public void playDice() {

    }

    @Override
    public List<Message> getTalkMessage() {
        return null;
    }

    @Override
    public void addRoomTalkLocalMessage(Message message) {

    }

    @Override
    public List<IRoomTalkMessage> getHisTalkMessage() {
        return mTalkMessageList;
    }

    @Override
    public List<ChatRoomMessage> getHisGiftMessage() {
        return mGiftMsgList;
    }
}
