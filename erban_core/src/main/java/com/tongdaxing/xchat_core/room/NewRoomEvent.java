package com.tongdaxing.xchat_core.room;

/**
 * Created by Chen on 2019/4/19.
 */
public class NewRoomEvent {

    public static class OnEnterRoomSuccess {
    }

    public static class OnRoomOnlineNumberChange {
        private int mCount;

        public OnRoomOnlineNumberChange(int count) {
            this.mCount = count;
        }

        public int getCount() {
            return mCount;
        }
    }

    public static class OnExitRoom {
        private String mAccount;

        public OnExitRoom(String account) {
            this.mAccount = account;
        }

        public String getAccount() {
            return mAccount;
        }
    }

    public static class OnGetRoomOwnerInfoSuccess {
    }

    public static class OnCheckEnterRoomStatus{
        private boolean mIsSuccess;
        private long mRoomId;
        private String mMessage;

        public OnCheckEnterRoomStatus(boolean isSuccess,long roomId, String message) {
            this.mIsSuccess = isSuccess;
            this.mRoomId = roomId;
            this.mMessage = message;
        }

        public boolean isSuccess() {
            return mIsSuccess;
        }

        public long getRoomId() {
            return mRoomId;
        }

        public String getMessage() {
            return mMessage;
        }
    }

    public static class OnNetworkChange{}

}
