package com.tongdaxing.xchat_core.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.NewTabInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.List;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/15
 */
public interface IRoomSettingView extends IMvpBaseView {

    void onResultRequestTagAllSuccess(List<NewTabInfo> tabInfoList);

    void onResultRequestTagAllFail(String error);

    void updateRoomInfoSuccess(RoomInfo roomInfo);

    void updateRoomInfoFail(String error);
}
