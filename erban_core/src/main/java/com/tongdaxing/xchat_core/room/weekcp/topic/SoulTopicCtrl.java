package com.tongdaxing.xchat_core.room.weekcp.topic;

import android.text.TextUtils;
import android.util.SparseArray;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.weekcp.BaseWeekCtrl;
import com.tongdaxing.xchat_core.room.weekcp.bean.SoulTopicBean;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by Chen on 2019/5/7.
 */
public class SoulTopicCtrl extends BaseWeekCtrl implements ISoulTopicCtrl {
    private String TAG = "SoulTopicCtrl";
    private SoulTopicInfo mSoulTopicInfo;

    public void setSoulTopicInfo(SoulTopicInfo soulTopicInfo) {
        mSoulTopicInfo = soulTopicInfo;
    }

    // 获取默契考验题目
    public void requestAskQuestion() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        String uid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        params.put("uid", uid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.getAskQuestion();
        L.info(TAG, "requestAskQuestion url: %s, uid: %s", url, uid);
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<SoulTopicBean>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "requestAskQuestion onError: ", e);
            }

            @Override
            public void onResponse(SoulTopicBean response) {
                L.info(TAG, "requestAskQuestion onResponse: %s", response);
                if (response != null && response.getCode() == OkHttpManager.HTTP_RESPONSE_CODE_SUCCESS) {
                    List<SoulTopicBean.DataBean> dataBeans = response.getData();
                    if (dataBeans != null) {
                        int size = dataBeans.size();
                        List<String> soulTittle = mSoulTopicInfo.getSoulTittle();
                        soulTittle.clear();

                        SparseArray<List<String>> soulTopicQuestions = mSoulTopicInfo.getSoulTopicQuestions();
                        soulTopicQuestions.clear();
                        for (int i = 0; i < size; i++) {
                            SoulTopicBean.DataBean dataBean = dataBeans.get(i);
                            String className = dataBean.getClassName();
                            soulTopicQuestions.put(i, dataBean.getClassContent());
                            soulTittle.add(className);
                        }
                        mSoulTopicInfo.setSoulTittle(soulTittle);
                        mSoulTopicInfo.setSoulTopicQuestions(soulTopicQuestions);
                    }
                    CoreUtils.send(new SoulTopicEvent.OnSoulTopicSuccess());
                }
            }
        });
    }

    // 发起玩法增加心动值
    @Override
    public void doRoomPlay(String msg) {
        List<String> soulTittle = mSoulTopicInfo.getSoulTittle();
        int questionTab = mSoulTopicInfo.getQuestionTab();
        String tittle = null;
        if (questionTab < soulTittle.size()) {
            tittle = soulTittle.get(questionTab);
            if (!TextUtils.isEmpty(tittle)) {
                msg = tittle + "," + msg;
            }
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        String uid = getUid();
        String roomId = getRoomUid();
        params.put("uid", uid);
        params.put("roomUid", roomId);
        params.put("text", msg);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.getAddForRoomPlay();
        L.info(TAG, "doRoomPlay url: %s, uid: %s, roomId: %s, msg: %s", url, uid, roomId, msg);
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "doRoomPlay onError: ", e);
            }

            @Override
            public void onResponse(String response) {
                L.info(TAG, "doRoomPlay onResponse: %s", response);
                showWeekCpFailedMsg(response);
            }
        });
    }
}
