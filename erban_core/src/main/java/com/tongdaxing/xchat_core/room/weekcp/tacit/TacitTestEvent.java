package com.tongdaxing.xchat_core.room.weekcp.tacit;

/**
 * Created by Chen on 2019/5/9.
 */
public class TacitTestEvent {
    public static class OnTacitTestAsk {
        private long mQuestionId;

        public OnTacitTestAsk(long id) {
            this.mQuestionId = id;
        }

        public long getQuestionId() {
            return mQuestionId;
        }
    }

    public static class OnTacitTestAskFinish {
        private int mHeart;

        public OnTacitTestAskFinish(int heart) {
            this.mHeart = heart;
        }

        public int getHeart() {
            return mHeart;
        }
    }

    public static class OnTacitTestAskFinishShowView {
        private String mQuestionId;

        public OnTacitTestAskFinishShowView(String questionId) {
            this.mQuestionId = questionId;
        }

        public String getQuestionId() {
            return mQuestionId;
        }
    }

    public static class OnTacitTestAskExit {
        private boolean mInitiative;

        public OnTacitTestAskExit(boolean initiative) {
            this.mInitiative = initiative;
        }

        public boolean isInitiative() {
            return mInitiative;
        }
    }

    public static class OnTacitTestTimeChange {
        private int mTime;

        public OnTacitTestTimeChange(int time) {
            this.mTime = time;
        }

        public int getTime() {
            return mTime;
        }
    }

    public static class OnTacitTestAskClick {
        private String mUid;
        private String mCurrentTime;

        public OnTacitTestAskClick(String uid, String currentTime) {
            this.mUid = uid;
            this.mCurrentTime = currentTime;
        }

        public String getUid() {
            return mUid;
        }

        public String getCurrentTime() {
            return mCurrentTime;
        }
    }

    public static class OnTacitTestFailed {
        private String mMessage;

        public OnTacitTestFailed(String msg) {
            this.mMessage = msg;
        }

        public String getMessage() {
            return mMessage;
        }
    }
}
