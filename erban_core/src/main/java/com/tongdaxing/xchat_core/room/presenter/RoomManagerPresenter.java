package com.tongdaxing.xchat_core.room.presenter;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.PresenterEvent;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.model.RoomBaseModel;
import com.tongdaxing.xchat_core.room.view.IRoomManagerView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class RoomManagerPresenter extends AbstractMvpPresenter<IRoomManagerView> {

    private final RoomBaseModel mRoomBaseModel;

    public RoomManagerPresenter() {
        mRoomBaseModel = new RoomBaseModel();
    }

    public void queryManagerList(int limit) {
        mRoomBaseModel.queryManagerList(limit)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<List<ChatRoomMember>>bindUntilEvent(PresenterEvent.DESTROY))
                .subscribe(new Consumer<List<ChatRoomMember>>() {
                    @Override
                    public void accept(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                        AvRoomDataManager.get().mRoomManagerList = chatRoomMemberList;
                        if (getMvpView() != null)
                            getMvpView().queryManagerListSuccess(chatRoomMemberList);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (getMvpView() != null) {
                            getMvpView().queryManagerListFail();
                        }
                    }
                });
    }

    /**
     * 设置管理员
     *
     * @param roomId
     * @param account
     * @param mark    true:设置管理员 ,false：移除管理员
     */
    public void markManagerList(long roomId, String account, boolean mark) {
        mRoomBaseModel.markManagerList(roomId, account, mark, new CallBack<ChatRoomMember>() {
            @Override
            public void onSuccess(ChatRoomMember data) {
                if (getMvpView() != null) {
                    getMvpView().markManagerListSuccess(data);
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().markManagerListFail(code, error);
                }
            }
        });
    }
}
