package com.tongdaxing.xchat_core.room.weekcp.topic;

import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2019/5/7.
 */
public class SoulTopicInfo {

    private List<String> mSoulTittle = new ArrayList<>();

    private SparseArray<List<String>> mSoulTopicQuestions = new SparseArray<>();

    private int mQuestionTab;


    public List<String> getSoulTittle() {
        return mSoulTittle;
    }

    public void setSoulTittle(List<String> soulTittle) {
        mSoulTittle = soulTittle;
    }

    public SparseArray<List<String>> getSoulTopicQuestions() {
        return mSoulTopicQuestions;
    }

    public void setSoulTopicQuestions(SparseArray<List<String>> soulTopicQuestions) {
        mSoulTopicQuestions = soulTopicQuestions;
    }

    public int getQuestionTab() {
        return mQuestionTab;
    }

    public void setQuestionTab(int questionTab) {
        mQuestionTab = questionTab;
    }

    public List<String> getSoulTabQuestions(int index) {
        if (index >= mSoulTopicQuestions.size()) {
            return null;
        }
        return mSoulTopicQuestions.valueAt(index);
    }

    public List<String> getSoulTabQuestions() {
        if (mQuestionTab >= mSoulTopicQuestions.size()) {
            return null;
        }
        return mSoulTopicQuestions.valueAt(mQuestionTab);
    }
}
