package com.tongdaxing.xchat_core.room.event;

/**
 * <p>
 * Created by zhangjian on 2019/5/8.
 */
public class SendQuickMsgEvent {
    private String mMsg;

    public SendQuickMsgEvent(String msg) {
        mMsg = msg;
    }

    public String getMsg() {
        return mMsg;
    }
}
