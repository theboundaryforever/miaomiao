package com.tongdaxing.xchat_core.room.talk.message;

import com.tongdaxing.xchat_core.room.talk.TalkType;

/**
 * Created by Chen on 2019/6/26.
 */
public class EggMessage extends CommonMessage {

    @Override
    public int getRoomTalkType() {
        return TalkType.ROOM_TALK_HEADER_TYPE_EGG;
    }
}
