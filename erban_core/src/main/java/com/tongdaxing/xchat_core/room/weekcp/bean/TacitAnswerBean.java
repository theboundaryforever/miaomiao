package com.tongdaxing.xchat_core.room.weekcp.bean;

import java.util.List;

/**
 * Created by Chen on 2019/5/9.
 */
public class TacitAnswerBean {

    /**
     * first : 56
     * second : 2
     * data : {"score":40,"sameNum":2,"uid":100723,"cpUid":109832,"roomUid":100723,"uidNick":"ccc","cpNick":"船长","uidAvatar":"https://pic.miaomiaofm.com/Fu-UE3FWEq9EhOSi1a_EKczR7xA2?imageslim","cpAvatar":"https://pic.miaomiaofm.com/FtUzsyqLpeyG0ehz1-wbxRSSl7Lu?imageslim","heartValue":60,"answer":[{"question":"你去过漫展吗","uidAnswer":"朋友","cpAnswer":"爱人"},{"question":"你喜欢朋友和你一起过生日还是喜欢和喜欢和爱人过生日","uidAnswer":"悬念电影","cpAnswer":"悬念电影"},{"question":"你喜欢看悬念电影还是科幻电影","uidAnswer":"植物黄油","cpAnswer":"植物黄油"},{"question":"动物黄油和植物黄油哪个更健康","uidAnswer":"拔TA鼻毛","cpAnswer":"偷亲一口"},{"question":"你会趁对象睡着了，偷偷做什么","uidAnswer":"","cpAnswer":""}]}
     */

    private int first;
    private int second;
    private DataBean data;

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * score : 40
         * sameNum : 2
         * uid : 100723
         * cpUid : 109832
         * roomUid : 100723
         * uidNick : ccc
         * cpNick : 船长
         * uidAvatar : https://pic.miaomiaofm.com/Fu-UE3FWEq9EhOSi1a_EKczR7xA2?imageslim
         * cpAvatar : https://pic.miaomiaofm.com/FtUzsyqLpeyG0ehz1-wbxRSSl7Lu?imageslim
         * heartValue : 60
         * answer : [{"question":"你去过漫展吗","uidAnswer":"朋友","cpAnswer":"爱人"},{"question":"你喜欢朋友和你一起过生日还是喜欢和喜欢和爱人过生日","uidAnswer":"悬念电影","cpAnswer":"悬念电影"},{"question":"你喜欢看悬念电影还是科幻电影","uidAnswer":"植物黄油","cpAnswer":"植物黄油"},{"question":"动物黄油和植物黄油哪个更健康","uidAnswer":"拔TA鼻毛","cpAnswer":"偷亲一口"},{"question":"你会趁对象睡着了，偷偷做什么","uidAnswer":"","cpAnswer":""}]
         */

        private int score;
        private int sameNum;
        private int uid;
        private int cpUid;
        private int roomUid;
        private String uidNick;
        private String cpNick;
        private String uidAvatar;
        private String cpAvatar;
        private int heartValue;
        private List<AnswerBean> answer;

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public int getSameNum() {
            return sameNum;
        }

        public void setSameNum(int sameNum) {
            this.sameNum = sameNum;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public int getCpUid() {
            return cpUid;
        }

        public void setCpUid(int cpUid) {
            this.cpUid = cpUid;
        }

        public int getRoomUid() {
            return roomUid;
        }

        public void setRoomUid(int roomUid) {
            this.roomUid = roomUid;
        }

        public String getUidNick() {
            return uidNick;
        }

        public void setUidNick(String uidNick) {
            this.uidNick = uidNick;
        }

        public String getCpNick() {
            return cpNick;
        }

        public void setCpNick(String cpNick) {
            this.cpNick = cpNick;
        }

        public String getUidAvatar() {
            return uidAvatar;
        }

        public void setUidAvatar(String uidAvatar) {
            this.uidAvatar = uidAvatar;
        }

        public String getCpAvatar() {
            return cpAvatar;
        }

        public void setCpAvatar(String cpAvatar) {
            this.cpAvatar = cpAvatar;
        }

        public int getHeartValue() {
            return heartValue;
        }

        public void setHeartValue(int heartValue) {
            this.heartValue = heartValue;
        }

        public List<AnswerBean> getAnswer() {
            return answer;
        }

        public void setAnswer(List<AnswerBean> answer) {
            this.answer = answer;
        }

        public static class AnswerBean {
            /**
             * question : 你去过漫展吗
             * uidAnswer : 朋友
             * cpAnswer : 爱人
             */

            private String question;
            private String uidAnswer;
            private String cpAnswer;

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public String getUidAnswer() {
                return uidAnswer;
            }

            public void setUidAnswer(String uidAnswer) {
                this.uidAnswer = uidAnswer;
            }

            public String getCpAnswer() {
                return cpAnswer;
            }

            public void setCpAnswer(String cpAnswer) {
                this.cpAnswer = cpAnswer;
            }
        }
    }
}
