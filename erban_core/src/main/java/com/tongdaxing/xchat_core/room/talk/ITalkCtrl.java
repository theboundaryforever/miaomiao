package com.tongdaxing.xchat_core.room.talk;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.xchat_core.msg.Message;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;

import java.util.List;

/**
 * Created by Chen on 2019/4/18.
 */
public interface ITalkCtrl {

    void forbidSpeak(long playerId, int forbidTime);

    void sendChat(Message message); // 聊天

    void playDice();//举牌

    List<Message> getTalkMessage();

    void addRoomTalkLocalMessage(Message message);

    List<IRoomTalkMessage> getHisTalkMessage();

    List<ChatRoomMessage> getHisGiftMessage();

    void reset();
}
