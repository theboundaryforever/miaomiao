package com.tongdaxing.xchat_core.room.turnTable.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;

/**
 * 夺宝大转盘碎片兑换消息
 * <p>
 * Created by zhangjian on 2019/4/16.
 */
public class TurnTablePiecesExchangeAttachment extends CustomAttachment {

    private String message;
    private long uid;

    public TurnTablePiecesExchangeAttachment(int first, int second) {
        super(first, second);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    @Override
    protected void parseData(JSONObject data) {
        message = data.getString("message");
        uid = data.getLong("uid");
    }

    @Override
    protected JSONObject packData() {


        JSONObject jsonObject = new JSONObject();

        jsonObject.put("message", message);
        jsonObject.put("uid", uid);

        return jsonObject;
    }
}
