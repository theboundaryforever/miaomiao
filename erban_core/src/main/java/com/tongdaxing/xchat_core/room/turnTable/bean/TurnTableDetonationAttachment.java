package com.tongdaxing.xchat_core.room.turnTable.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;

/**
 * 魔力转圈圈 暴走消息
 * <p>
 * Created by zhangjian on 2019/4/9.
 */
public class TurnTableDetonationAttachment extends CustomAttachment {

    private String params;

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public TurnTableDetonationAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        params =JSONArray.parseArray(data.getString("content")).getString(0);
    }

    @Override
    protected JSONObject packData() {


        JSONObject jsonObject = new JSONObject();

        jsonObject.put("params", params);

        return jsonObject;
    }
}
