package com.tongdaxing.xchat_core.room.talk.message;

/**
 * Created by Chen on 2019/4/18.
 */
public interface IRoomTalkMessage {

    int getRoomTalkType();

    void setLocalId(String id);

    String getLocalId();
}
