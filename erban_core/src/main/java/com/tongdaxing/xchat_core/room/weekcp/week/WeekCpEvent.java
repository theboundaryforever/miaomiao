package com.tongdaxing.xchat_core.room.weekcp.week;

import com.tongdaxing.xchat_core.im.custom.bean.WeekCpAttachment;

/**
 * Created by Chen on 2019/5/5.
 */
public class WeekCpEvent {
    public static class OnChangeHeartValue {
        int mHeartValue;

        public OnChangeHeartValue(int value) {
            this.mHeartValue = value;
        }

        public int getHeartValue() {
            return mHeartValue;
        }
    }

    public static class OnFromCpChange {
        private WeekCpAttachment mWeekCpAttachment;

        public OnFromCpChange(WeekCpAttachment weekCpAttachment) {
            this.mWeekCpAttachment = weekCpAttachment;
        }

        public WeekCpAttachment getWeekCpAttachment() {
            return mWeekCpAttachment;
        }
    }

    public static class OnWeekCpMerry {
    }

    public static class OnWeekCpOffice {
        private int mSecond;
        private String mCpId;

        public OnWeekCpOffice(int second, String cpId) {
            this.mSecond = second;
            this.mCpId = cpId;
        }

        public String getCpId() {
            return mCpId;
        }

        public int getSecond() {
            return mSecond;
        }
    }

    public static class OnCpUnband {
    }

    public static class OnWeekCpFailed {
        private String mMessage;

        public OnWeekCpFailed(String msg) {
            this.mMessage = msg;
        }

        public String getMessage() {
            return mMessage;
        }
    }

    public static class OnWeekCpSoulSend {
    }

    public static class OnWeekCpSoulAddHeart {
        int mHeart;

        public OnWeekCpSoulAddHeart(int heart) {
            this.mHeart = heart;
        }

        public int getHeart() {
            return mHeart;
        }
    }

    public static class OnRemoveCurrentCpForNewWeekCp {

    }
}
