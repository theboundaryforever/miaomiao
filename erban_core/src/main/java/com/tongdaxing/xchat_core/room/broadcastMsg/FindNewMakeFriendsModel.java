package com.tongdaxing.xchat_core.room.broadcastMsg;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * <p>
 * Created by zhangjian on 2019/6/20.
 */
public class FindNewMakeFriendsModel extends BaseMvpModel {

    public void requestMsgList(int pageNum, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(Constants.PAGE_SIZE));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getFindFriendsBroadcastList(), params, myCallBack);
    }

    /**
     * 发布广场消息
     *
     * @param roomId     房间ID
     * @param roomUid    房主ID
     * @param content    发布的内容
     * @param myCallBack
     */
    public void pushMsg(long roomId, long roomUid, String content, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", String.valueOf(roomId));
        params.put("roomUid", String.valueOf(roomUid));
        params.put("content", content);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getFindFriendsBroadcastPushMsg(), params, myCallBack);
    }

    /**
     * 获取发布消息限制的过期时间
     *
     * @param myCallBack
     */
    public void requestCD(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getFindFriendsBroadcastLimitExpired(), params, myCallBack);
    }
}
