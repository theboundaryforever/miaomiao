package com.tongdaxing.xchat_core.room.presenter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.util.Entry;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.PresenterEvent;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.OnLoginCompletionListener;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.praise.PraiseEvent;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.RoomAction;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.view.IAvRoomView;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.NetworkUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/11
 */
public class AvRoomPresenter extends AbstractMvpPresenter<IAvRoomView> {
    private final String TAG = "AvRoomPresenter";
    private final AvRoomModel mAvRoomModel;

    public AvRoomModel getmAvRoomModel() {
        return mAvRoomModel;
    }

    private final Gson mGson;
    private Disposable mGetOnlineNumberDisposable;
    private static final String GET_ROOM_FROM_IMNET_ERROR = "-1101";

    public AvRoomPresenter() {
        mAvRoomModel = new AvRoomModel();
        mGson = new Gson();
    }

    /**
     * 进入云信聊天室回调
     */
    public void enterRoom(final RoomInfo roomInfo, final Context context, final HerUserInfo herUserInfo) {
        L.debug(TAG, "enterRoom herUserInfo: %s", herUserInfo);
        if (roomInfo == null) {
            if (getMvpView() != null)
                getMvpView().showFinishRoomView();
            return;
        }
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        L.debug(TAG, "enterRoom currentRoom: %s", currentRoom);
        //如果已经进入的房间要先退出房间
        if (currentRoom != null) {
            if (currentRoom.getUid() == roomInfo.getUid()) {
                return;
            }
            //进入新的房间需要先退出房间
            mAvRoomModel.exitRoom(new CallBack<String>() {
                @Override
                public void onSuccess(String data) {
                    L.info(TAG, "enterRoom, exitRoom onSuccess: %s", data);
                    if (getMvpView() != null) {
                        getMvpView().exitRoom(AvRoomDataManager.get().mCurrentRoomInfo);
                    }
                    enterRoomAction(roomInfo, context, herUserInfo);
                }

                @Override
                public void onFail(int code, String error) {
                    L.error(TAG, "enterRoom, exitRoom coed: %d, error: %s", code, error);
                    if (getMvpView() != null)
                        getMvpView().showFinishRoomView();
                }
            });
        } else {
            enterRoomAction(roomInfo, context, herUserInfo);
        }
    }

    private void enterRoomAction(RoomInfo roomInfo, final Context context, final HerUserInfo herUserInfo) {
        //我们自己服务端信息
        AvRoomDataManager.get().mCurrentRoomInfo = roomInfo;
        L.debug(TAG, "enterRoomAction mCurrentRoomInfo = %s", AvRoomDataManager.get().mCurrentRoomInfo);
        AvRoomDataManager.get().mServiceRoominfo = roomInfo;
        LogUtil.d("enterRoom", AvRoomDataManager.get().mCurrentRoomInfo + "");
        final long roomId = roomInfo.getRoomId();

        L.debug(TAG, "enterRoomAction roomId: %d", roomId);
        Observable<EnterChatRoomResultData> enterRoomObservable = mAvRoomModel.enterRoom(roomInfo.getRoomId(), 3, herUserInfo);

        enterRoomObservable.flatMap(new Function<EnterChatRoomResultData, ObservableSource<List<Entry<String, String>>>>() {
            @Override
            public ObservableSource<List<Entry<String, String>>> apply(EnterChatRoomResultData enterChatRoomResultData) throws Exception {
                return dealServerMicInfo(enterChatRoomResultData);
            }
        }).map(new Function<List<Entry<String, String>>, SparseArray<RoomQueueInfo>>() {
            @Override
            public SparseArray<RoomQueueInfo> apply(List<Entry<String, String>> entries) throws Exception {
                return dealMicMemberFromIMNet(entries);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<SparseArray<RoomQueueInfo>>() {
                    @Override
                    public void accept(SparseArray<RoomQueueInfo> roomQueueInfoSparseArray) throws Exception {
                        L.info(TAG, "enterRoomAction accept: %d", roomId);
                        initRtcEngine(roomId, context);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LogUtils.d("AVRoomMsgError", roomId + "");
                        L.error(TAG, "enterRoomAction Exception: ", throwable);
                        dealEnterRoomError(throwable);
                    }
                });
    }

    /**
     * 声网appid动态获取
     *
     * @param roomId
     */
    private void initRtcEngine(final long roomId, Context context) {
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        final Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", uid + "");
        long roomid = AvRoomDataManager.get().mCurrentRoomInfo.getRoomId();
        requestParam.put("roomId", roomid + "");
//        requestParam.put("_method","post");
        L.info(TAG, "initRtcEngine roomId: %d, uid: %d", roomid, uid);
        if (!NetworkUtils.isNetworkAvailable(context))
            dealEnterRoomError(new Throwable("414"));
        OkHttpManager.getInstance().doPostRequest(UriProvider.getRoomAgoraKey(), requestParam, new OkHttpManager.MyCallBack<ServiceResult<String>>() {

            @Override
            public void onError(Exception e) {
                L.info(TAG, "initRtcEngine onError:", e);
                dealEnterRoomError(new Throwable("414"));
            }

            @Override
            public void onResponse(ServiceResult<String> response) {
                L.info(TAG, "initRtcEngine onResponse: %s", response);
                if (response != null && response.isSuccess() && response.getData() != null) {
                    String token = response.getData();
                    RoomInfo curRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                    if (curRoomInfo != null && getMvpView() != null) {
                        RtcEngineManager.get().setAudioOrganization(curRoomInfo.getAudioChannel());
                        L.info(TAG, "initRtcEngine startRtcEngine + " + "audioChannel ---> " + curRoomInfo.getAudioChannel());
                        RtcEngineManager.get().setOnLoginCompletionListener(new OnLoginCompletionListener() {
                            @Override
                            public void onLoginCompletionFail(String msg) {
                                dealEnterRoomError(msg);
                            }
                        });
                        RtcEngineManager.get().startRtcEngine(uid, token, curRoomInfo);
                        getMvpView().enterRoomSuccess();
                        CoreUtils.send(new RoomAction.OnEnterRoom(true));
                    } else {
                        dealEnterRoomError(new Throwable("414"));
                    }
                } else {
                    dealEnterRoomError(new Throwable("414"));
                }
            }
        });
    }

    /**
     * 即构登录房间错误回调
     *
     * @param errorMsg 错误信息
     */
    private void dealEnterRoomError(String errorMsg) {
        exitRoom();
        if (getMvpView() != null) {
            getMvpView().enterRoomFail(-1, errorMsg);

        }
    }


    private void dealEnterRoomError(Throwable throwable) {
        CoreUtils.send(new RoomAction.OnEnterRoom(false));
        L.error(TAG, "dealEnterRoomError: ", throwable);
        throwable.printStackTrace();
        LogUtils.d("dealEnterRoomError", "" + throwable.getMessage());
        String error;
        switch (throwable.getMessage()) {
            case "414":
                error = "参数错误";
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, throwable.getMessage() + ":" + error);
                }
                break;
            case "404":
            case "13002":
                if (getMvpView() != null)
                    getMvpView().showFinishRoomView();
                error = "聊天室不存在";
                break;
            case "403":
                error = "无权限";
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, throwable.getMessage() + ":" + error);
                }
                break;
            case "500":
                error = "服务器内部错误";
                break;
            case "13001":
                error = "IM主连接状态异常";
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, throwable.getMessage() + ":" + error);
                }
                break;
            case "13003":
                error = "黑名单用户禁止进入聊天室";
                if (getMvpView() != null)
                    getMvpView().showBlackEnterRoomView();
                break;
            case GET_ROOM_FROM_IMNET_ERROR:
                exitRoom();
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, throwable.getMessage() + ":" + "网络异常");

                }
                break;
            default:
                error = "网络异常";
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, throwable.getMessage() + ":" + error);
                }
                break;

        }

    }


    private void addInfoToMicInList(Entry<String, String> entry, JsonParser jsonParser) {

        JsonObject valueJsonObj = jsonParser.parse(entry.value).getAsJsonObject();


        Set<String> strings = valueJsonObj.keySet();
        if (valueJsonObj == null)
            return;
        Json json = new Json();
        for (String key : strings) {
            json.set(key, valueJsonObj.get(key).getAsString());
        }
        AvRoomDataManager.get().addMicInListInfo(entry.key, json);

    }

    /**
     * 处理网易云信坑位信息
     */
    private SparseArray<RoomQueueInfo> dealMicMemberFromIMNet(List<Entry<String, String>> entries) {

        if (!ListUtils.isListEmpty(entries)) {
            JsonParser jsonParser = new JsonParser();
            ChatRoomMember chatRoomMember;
            for (Entry<String, String> entry : entries) {
                LogUtils.d("micInListLogFetchQueue", "key:" + entry.key + "   content:" + entry.value);
                if (entry.key != null && entry.key.length() > 2) {
                    // : 2018/4/26 01
                    addInfoToMicInList(entry, jsonParser);
                    continue;

                }


                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(Integer.parseInt(entry.key));
                if (roomQueueInfo != null) {
                    JsonObject valueJsonObj = jsonParser.parse(entry.value).getAsJsonObject();
                    if (valueJsonObj != null) {
                        chatRoomMember = new ChatRoomMember();
                        if (valueJsonObj.has("uid")) {
                            int uid = valueJsonObj.get("uid").getAsInt();
                            chatRoomMember.setAccount(String.valueOf(uid));
                        }
                        if (valueJsonObj.has("nick")) {
                            chatRoomMember.setNick(valueJsonObj.get("nick").getAsString());
                        }
                        if (valueJsonObj.has("avatar")) {
                            chatRoomMember.setAvatar(valueJsonObj.get("avatar").getAsString());
                        }
                        if (valueJsonObj.has("gender")) {
                            roomQueueInfo.gender = valueJsonObj.get("gender").getAsInt();
                        }
                        if (valueJsonObj.has(SpEvent.headwearUrl)) {

                            Map<String, Object> stringStringMap = new HashMap<>();
                            stringStringMap.put(SpEvent.headwearUrl, valueJsonObj.get(SpEvent.headwearUrl).getAsString());
                            chatRoomMember.setExtension(stringStringMap);
                        }

                        roomQueueInfo.mChatRoomMember = chatRoomMember;

                        //同时开始接收此用户的推流
                        try {
                            Integer account = Integer.valueOf(chatRoomMember.getAccount());
                            L.debug(TAG, "dealMicMemberFromIMNet account: %d", account);
                            RtcEngineManager.get().muteRemoteAudioStream(account, false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    AvRoomDataManager.get().mMicQueueMemberMap.put(Integer.valueOf(entry.key), roomQueueInfo);
                }
            }
        }
        return AvRoomDataManager.get().mMicQueueMemberMap;
    }


    /**
     * 处理服务端坑位信息
     */
    @Nullable
    private ObservableSource<List<Entry<String, String>>> dealServerMicInfo(EnterChatRoomResultData enterChatRoomResultData) {
        AvRoomDataManager.get().mEnterChatRoomResultData = enterChatRoomResultData;
        if (enterChatRoomResultData == null)
            return Observable.error(new Throwable(GET_ROOM_FROM_IMNET_ERROR));
        else {
            final ChatRoomInfo roomInfo = enterChatRoomResultData.getRoomInfo();
            if (roomInfo == null) {
                return Observable.error(new Throwable(GET_ROOM_FROM_IMNET_ERROR));
            }
            AvRoomDataManager.get().mCurrentRoomInfo.onlineNum = roomInfo.getOnlineUserCount();
            Map<String, Object> extension = roomInfo.getExtension();
            if (extension != null) {
                String roomInfoStr = (String) extension.get(Constants.KEY_CHAT_ROOM_INFO_ROOM);
                if (!TextUtils.isEmpty(roomInfoStr)) {
                    RoomInfo extRoomInfo = mGson.fromJson(roomInfoStr, RoomInfo.class);
                    extRoomInfo.setRoomId(Long.valueOf(roomInfo.getRoomId()));
                    extRoomInfo.onlineNum = AvRoomDataManager.get().mCurrentRoomInfo.onlineNum;
                    //云信服务端信息
                    AvRoomDataManager.get().mCurrentRoomInfo = extRoomInfo;

//                    LogUtil.d("enterRoom1", AvRoomDataManager.get().mCurrentRoomInfo + "");
                }
                //获取云信麦序相关信息
                String roomMicStr = (String) extension.get(Constants.KEY_CHAT_ROOM_INFO_MIC);
                if (!TextUtils.isEmpty(roomMicStr)) {
                    //初始化所有坑位
                    Map<String, String> micMapStr = mGson.fromJson(roomMicStr, new TypeToken<Map<String, String>>() {
                    }.getType());
                    for (Map.Entry<String, String> entry : micMapStr.entrySet()) {
                        AvRoomDataManager.get().mMicQueueMemberMap.put(Integer.valueOf(entry.getKey()),
                                new RoomQueueInfo(mGson.fromJson(entry.getValue(), RoomMicInfo.class), null));
                    }
                    return mAvRoomModel.queryRoomMicInfo(roomInfo.getRoomId());
                }
            }
            return Observable.error(new Throwable(GET_ROOM_FROM_IMNET_ERROR));
        }
    }

    public void exitRoom() {
        RtcEngineManager.get().setRole(io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE);
        mAvRoomModel.exitRoom(new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
                if (getMvpView() != null) {
                    getMvpView().exitRoom(AvRoomDataManager.get().mCurrentRoomInfo);
                }
            }

            @Override
            public void onFail(int code, String error) {

            }
        });
    }


    public void enterRoomFromService(String uId) {
        L.debug(TAG, "enterRoomFromService uId: %s", uId);
        if (TextUtils.isEmpty(uId)) {
            return;
        }
        ResponseListener listener = new ResponseListener<ServiceResult<RoomInfo>>() {
            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                L.debug(TAG, "enterRoomFromService uId: %s", data);
                if (getMvpView() == null) {
                    return;
                }
                if (null != data) {
                    if (data.isSuccess()) {
                        if (data.getData() == null || data.getData().getRoomId() == 0) {
                            getMvpView().showFinishRoomView();
                        } else {
                            getMvpView().requestRoomInfoSuccessView(data.getData());
                        }
                    } else {
                        getMvpView().requestRoomInfoFailView(data.getCode(), data.getErrorMessage());
                    }
                } else {
                    getMvpView().requestRoomInfoFailView(-1, "进入房间失败");
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                L.error(TAG, "onErrorResponse error:", error);
                if (getMvpView() != null) {
                    getMvpView().requestRoomInfoFailView(-1, error.getErrorStr());
                }
            }
        };
        mAvRoomModel.requestRoomInfoFromService(uId, listener, errorListener);
    }

    /**
     * 获取活动信息
     */
    public void getActionDialog(int type) {
        ResponseListener listener = new ResponseListener<ServiceResult<List<ActionDialogInfo>>>() {
            @Override
            public void onResponse(ServiceResult<List<ActionDialogInfo>> data) {
                if (null != data && data.isSuccess()) {
                    if (getMvpView() != null && data.getData() != null) {
                        getMvpView().onGetActionDialog(data.getData());
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onGetActionDialogError(data.getErrorMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if (getMvpView() != null) {
                    getMvpView().onGetActionDialogError(error.getErrorStr());
                }
            }
        };
        mAvRoomModel.getActionDialog(type, listener, errorListener);
    }

    /**
     * 获取房间内固定成员列表
     */
    public void getNormalChatMember() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        mAvRoomModel.getNormalChatMember(String.valueOf(roomInfo.getRoomId()), currentUid);
    }

    private void startGetOnlineMemberNumberJob() {
        Observable.interval(10, 10, TimeUnit.SECONDS, Schedulers.io())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mGetOnlineNumberDisposable = d;
                    }

                    @Override
                    public void onNext(Long aLong) {
                        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                        if (roomInfo == null) return;
                        mAvRoomModel.startGetOnlineMemberNumberJob(roomInfo.getRoomId())
                                .observeOn(AndroidSchedulers.mainThread())
                                .compose(AvRoomPresenter.this.<ChatRoomInfo>bindUntilEvent(PresenterEvent.DESTROY))
                                .subscribe(new Consumer<ChatRoomInfo>() {
                                    @Override
                                    public void accept(ChatRoomInfo chatRoomInfo) throws Exception {
                                        if (chatRoomInfo == null) return;
                                        int onlineUserCount = chatRoomInfo.getOnlineUserCount();
                                        AvRoomDataManager.get().mCurrentRoomInfo.onlineNum = onlineUserCount;
                                        CoreUtils.send(new NewRoomEvent.OnRoomOnlineNumberChange(onlineUserCount));
//                                        Logger.i("定时拉取房间在线人数完成...." + onlineUserCount);

                                        LogUtils.d("roomInfo.onlineNum", "roomInfo.onlineUserCount:" + onlineUserCount);
                                        if (getMvpView() != null)
                                            getMvpView().onRoomOnlineNumberSuccess(onlineUserCount);
                                    }
                                });
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    private void toast(String message) {
        SingleToastUtil.showToast(message);
    }

    @Override
    public void onDestroyPresenter() {
        super.onDestroyPresenter();
        if (mGetOnlineNumberDisposable != null) {
            mGetOnlineNumberDisposable.dispose();
            mGetOnlineNumberDisposable = null;
        }
        CoreUtils.unregister(this);
    }

    @Override
    public void onCreatePresenter(@Nullable Bundle saveState) {
        super.onCreatePresenter(saveState);
        startGetOnlineMemberNumberJob();
        CoreUtils.register(this);
    }
}
