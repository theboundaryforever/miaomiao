package com.tongdaxing.xchat_core.room.face;

import java.util.List;

/**
 * Created by Chen on 2019/5/7.
 */
public class RoomFaceEvent {
    public static class OnReceiveFace {
        private List<FaceReceiveInfo> mReceiveInfos;

        public OnReceiveFace(List<FaceReceiveInfo> receiveInfos) {
            this.mReceiveInfos = receiveInfos;
        }

        public List<FaceReceiveInfo> getReceiveInfos() {
            return mReceiveInfos;
        }
    }

}
