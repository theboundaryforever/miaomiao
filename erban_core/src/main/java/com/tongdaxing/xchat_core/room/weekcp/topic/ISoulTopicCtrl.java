package com.tongdaxing.xchat_core.room.weekcp.topic;

/**
 * Created by Chen on 2019/5/7.
 */
public interface ISoulTopicCtrl {
    // 获取默契考验题目
    void requestAskQuestion();

    // 发起玩法增加心动值
    void doRoomPlay(String msg);
}
