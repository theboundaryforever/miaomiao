package com.tongdaxing.xchat_core.room.weekcp.bean;

/**
 * Created by Chen on 2019/5/5.
 */
public class CheckCpBean {
    /**
     * code : 200
     * data : {"avatar":"https://pic.miaomiaofm.com/FtUzsyqLpeyG0ehz1-wbxRSSl7Lu?imageslim","closeLevel":0,"cpType":2,"erbanNo":6930484,"nick":"船长","uid":109832}
     * message : 200:success
     */

    private int code;
    private DataBean data;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "CheckCpBean{" +
                "code=" + code +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }

    public static class DataBean {
        /**
         * avatar : https://pic.miaomiaofm.com/FtUzsyqLpeyG0ehz1-wbxRSSl7Lu?imageslim
         * closeLevel : 0
         * cpType : 2
         * erbanNo : 6930484
         * nick : 船长
         * uid : 109832
         */

        private String avatar;
        private int closeLevel;
        private int cpType;
        private int erbanNo;
        private String nick;
        private String uid;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getCloseLevel() {
            return closeLevel;
        }

        public void setCloseLevel(int closeLevel) {
            this.closeLevel = closeLevel;
        }

        public int getCpType() {
            return cpType;
        }

        public void setCpType(int cpType) {
            this.cpType = cpType;
        }

        public int getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(int erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "avatar='" + avatar + '\'' +
                    ", closeLevel=" + closeLevel +
                    ", cpType=" + cpType +
                    ", erbanNo=" + erbanNo +
                    ", nick='" + nick + '\'' +
                    ", uid=" + uid +
                    '}';
        }
    }
}
