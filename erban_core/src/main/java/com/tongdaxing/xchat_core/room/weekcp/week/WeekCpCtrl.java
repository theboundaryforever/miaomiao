package com.tongdaxing.xchat_core.room.weekcp.week;

import android.text.TextUtils;
import android.util.SparseArray;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.util.EasyTimer;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.custom.bean.WeekCpAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.WeekCpMsgAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.RoomAction;
import com.tongdaxing.xchat_core.room.bean.RoomCpTicket;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.weekcp.BaseWeekCtrl;
import com.tongdaxing.xchat_core.room.weekcp.RoomLoverEvent;
import com.tongdaxing.xchat_core.room.weekcp.bean.CheckCpBean;
import com.tongdaxing.xchat_core.room.weekcp.bean.HeartValueBean;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTestEvent;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Chen on 2019/4/30.
 */
public class WeekCpCtrl extends BaseWeekCtrl implements IWeekCpCtrl {
    private String TAG = "WeekCpCtrl";
    private int mCpMemberSize = 2;  // 情侣房间座位的数量

    private WeekCpInfo mWeekCpInfo;
    private int mStartHeartType = 1;     // 开始计算心动值
    private int mStopHeartType = 2;     // 停止计算心动值
    private EasyTimer mEasyTimer;
    private int mTimerDuration = 60 * 1000; // 一分钟计算一次

    private Lock mTimerLock = new ReentrantLock();

    public WeekCpCtrl() {
        L.info(TAG, "WeekCpCtrl");
        CoreUtils.register(this);
    }

    public void setWeekCpInfo(WeekCpInfo weekCpInfo) {
        this.mWeekCpInfo = weekCpInfo;
        initCp();
    }

    private void initCp() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(getUserId(), true);
        if (userInfo != null && userInfo.getCpUser() != null) {
            int uid = userInfo.getCpUser().getUid();
            mWeekCpInfo.setCpId(String.valueOf(uid));
        }
    }

    @Subscribe
    public void onChairChange(RoomAction.OnChairStateChange stateChange) {
        boolean sitDown = stateChange.isSitDown();
        L.info(TAG, "onChairChange sitDown: %b", sitDown);
        checkCp(stateChange.getPlayerId(), sitDown);
    }

    @Subscribe
    public void onCpUnband(WeekCpEvent.OnCpUnband cpUnband) {
        checkCp("", true);
    }

    private void checkCp(String playerId, boolean sitDown) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null && roomInfo.getTagType() == RoomInfo.ROOMTYPE_LOVERS) {
            if (sitDown) {
                String userId = getUid();
                if (AvRoomDataManager.get().isOnMic(userId)) {      // 自己上麦
                    SparseArray<RoomQueueInfo> queueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
                    if (queueMemberMap != null) {
                        int size = queueMemberMap.size();
                        if (size >= mCpMemberSize) {    // 座位上人数大于等于情侣房的座位数
                            RoomQueueInfo ownerInfo = queueMemberMap.valueAt(0);
                            RoomQueueInfo cpInfo = queueMemberMap.valueAt(1);
                            if (ownerInfo != null && cpInfo != null) {
                                ChatRoomMember ownerMember = ownerInfo.mChatRoomMember;
                                ChatRoomMember cpMember = cpInfo.mChatRoomMember;
                                if (ownerMember != null && cpMember != null) {
                                    RoomCpTicket cpTicket = new RoomCpTicket();
                                    cpTicket.setRoomId(getRoomUid());
                                    cpTicket.setChairChangeType(mStartHeartType);
                                    cpTicket.setUserId(getUid());
                                    String account;
                                    if (isOwner()) {
                                        account = cpMember.getAccount();
                                    } else {
                                        account = ownerMember.getAccount();
                                    }
                                    cpTicket.setCpId(account);
                                    checkCpInfo(account);
                                    requestRoomCpHeart(cpTicket);
                                }
                            }
                        }
                    }
                }
            } else {
                L.info(TAG, "onChairChange reset playerId: %s", playerId);
                reset(playerId);
            }
        }
    }

    @Subscribe
    public void onExitRoom(NewRoomEvent.OnExitRoom exitRoom) {
        String account = exitRoom.getAccount();
        L.info(TAG, "onExitRoom account: %s", account);
        if (!TextUtils.isEmpty(account)) {
            if (AvRoomDataManager.get().isSelf(account)) {
                mWeekCpInfo = new WeekCpInfo();
                stopHeartTimer();
            }
        }
    }

    // 上麦计算心动值
    public void requestRoomCpHeart(final RoomCpTicket cpTicket) {
        if (cpTicket == null) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", cpTicket.getUserId());
        params.put("cpUid", cpTicket.getCpId());
        params.put("roomUid", cpTicket.getRoomId());
        params.put("type", cpTicket.getChairChangeType() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.getCpHeart();
        L.info(TAG, "requestRoomCpHeart url: %s, RoomCpTicket: %s", url, cpTicket.toString());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getCpHeart(), null, params, new OkHttpManager.MyCallBack<HeartValueBean>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "requestRoomCpHeart onError: ", e);
            }

            @Override
            public void onResponse(HeartValueBean response) {
                L.info(TAG, "requestRoomCpHeart onResponse: %s", response);
                if (response == null) {
                    return;
                }
                if (cpTicket.getChairChangeType() == mStartHeartType) {
                    if (response.getCode() == OkHttpManager.HTTP_RESPONSE_CODE_SUCCESS) {
                        mTimerLock.lock();
                        try {
                            int data = response.getData();
                            L.info(TAG, "requestRoomCpHeart data: %d", data);
                            mWeekCpInfo.setHeartValue(data);
                            int heartValue = CoreManager.getCore(IRoomCore.class).getWeekManager().getWeekCpInfo().getHeartValue();
                            L.info(TAG, "requestRoomCpHeart heartValue: %d", heartValue);
                            CoreUtils.send(new WeekCpEvent.OnChangeHeartValue(data));
                            if (data < ROOM_CP_MAX_VALUE) {
                                starHeartTimer();
                            }
                        } finally {
                            mTimerLock.unlock();
                        }
                    } else {
                        CoreUtils.send(new WeekCpEvent.OnWeekCpFailed(response.getMessage()));
                    }
                }
            }
        });
    }

    // 增加心动值
    public void requestAddCpHeart(RoomCpTicket cpTicket) {
        if (cpTicket == null) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", cpTicket.getUserId());
        params.put("cpUid", cpTicket.getCpId());
        params.put("roomUid", cpTicket.getRoomId());
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.getCpValue();
        L.info(TAG, "requestAddCpHeart url: %s, RoomCpTicket: %s", url, cpTicket.toString());
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<HeartValueBean>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "requestAddCpHeart onError: ", e);
            }

            @Override
            public void onResponse(HeartValueBean response) {
                L.info(TAG, "requestAddCpHeart onResponse: %s", response);
                if (response != null) {
                    if (response.getCode() == OkHttpManager.HTTP_RESPONSE_CODE_SUCCESS) {
                        mTimerLock.lock();
                        try {
                            mWeekCpInfo.setIntimacyValue(response.getData());
                            mWeekCpInfo.setHeartValue(0);
                            starHeartTimer();
                            CoreUtils.send(new WeekCpEvent.OnChangeHeartValue(mWeekCpInfo.getHeartValue()));
                        } finally {
                            mTimerLock.unlock();
                        }
                    } else {
                        CoreUtils.send(new WeekCpEvent.OnWeekCpFailed(response.getMessage()));
                    }
                }
            }
        });
    }

    // 心动值校验
    public void requestCheckCpHeart() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", getUid());
        params.put("roomUid", getRoomUid());
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.getCpHeartCheck();
        L.info(TAG, "requestCheckCpHeart url: %s, uid: %s, roomUid: %s", url, getUid(), getRoomUid());
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "requestCheckCpHeart onError: ", e);
            }

            @Override
            public void onResponse(String response) {
                L.info(TAG, "requestCheckCpHeart onResponse: %s", response);
                CoreUtils.send(new RoomWeekCpEvent.OnCheckCpHeart(response));
                showWeekCpFailedMsg(response);
            }
        });
    }

    // 一周CP
    public void requestWeekCp(RoomCpTicket cpTicket) {
        if (cpTicket == null) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", cpTicket.getUserId());
        params.put("cpUid", cpTicket.getCpId());
        params.put("roomUid", cpTicket.getRoomId());
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.getWeekCp();
        L.info(TAG, "requestWeekCp url: %s, RoomCpTicket: %s", url, cpTicket.toString());
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "requestWeekCp onError: ", e);
            }

            @Override
            public void onResponse(String response) {
                L.info(TAG, "requestWeekCp onResponse: %s", response);
                showWeekCpFailedMsg(response);
            }
        });
    }

    // 解除一周CP
    public void unbandWeekCp(RoomCpTicket cpTicket) {
        if (cpTicket == null) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", cpTicket.getUserId());
        params.put("cpUid", cpTicket.getCpId());
        params.put("roomUid", cpTicket.getRoomId());
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.getUnbandWeekCp();
        L.info(TAG, "unbandWeekCp url: %s, RoomCpTicket: %s", url, cpTicket.toString());
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "unbandWeekCp onError: ", e);
            }

            @Override
            public void onResponse(String response) {
                L.info(TAG, "unbandWeekCp onResponse: %s", response);
                showWeekCpFailedMsg(response);
            }
        });
    }

    // 一周CP排行榜
    public void requestWeekRank(RoomCpTicket cpTicket) {
        if (cpTicket == null) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", cpTicket.getUserId());
        params.put("cpUid", cpTicket.getCpId());
        params.put("roomUid", cpTicket.getRoomId());
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.getWeekCpRank();
        L.info(TAG, "requestWeekRank url: %s, RoomCpTicket: %s", url, cpTicket.toString());
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "requestWeekRank onError: ", e);
            }

            @Override
            public void onResponse(String response) {
                L.info(TAG, "requestWeekRank onResponse: %s", response);
                showWeekCpFailedMsg(response);
                //
            }
        });
    }

    // 检查是否为自己的CP
    @Override
    public void checkCpInfo(final String queryId) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        String url = UriProvider.cpCheck();
        String uid = getUid();
        params.put("uid", uid);
        params.put("cid", queryId);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        L.info(TAG, "checkCpInfo uid: %s, cid: %s, url: %s", uid, queryId, url);
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<CheckCpBean>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "checkCpInfo onError: ", e);
            }

            @Override
            public void onResponse(CheckCpBean response) {
                L.info(TAG, "checkCpInfo onResponse: %s", response);
                if (response == null) {
                    return;
                }
                if (response.getCode() == OkHttpManager.HTTP_RESPONSE_CODE_SUCCESS) {
                    CheckCpBean.DataBean data = response.getData();
                    if (data != null && !TextUtils.isEmpty(data.getUid()) && data.getUid().equals(getUid())) {
                        mWeekCpInfo.setCpId(queryId);
                    }
                } else {
                    CoreUtils.send(new WeekCpEvent.OnWeekCpFailed(response.getMessage()));
                }
            }
        });
    }

    @Subscribe
    public void onWeekCpOffice(WeekCpEvent.OnWeekCpOffice cpOffice) {
        L.info(TAG, "onWeekCpOffice cpId: %s", cpOffice.getCpId());
        if (cpOffice.getSecond() == WeekCpMsgAttachment.ROOM_WEEK_CP_OFFICE_MSG_MERRY) {
            mWeekCpInfo.setCpId(cpOffice.getCpId());
        } else if (cpOffice.getSecond() == WeekCpMsgAttachment.ROOM_WEEK_CP_OFFICE_MSG_UNBAN) {
            mWeekCpInfo.setCpId("");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWeekCpOffice(RoomLoverEvent.OnCpStateChange stateChange) {
        L.debug(TAG, "onWeekCpOffice: %s", stateChange.getCpId());
        mWeekCpInfo.setCpId(stateChange.getCpId());
        CoreUtils.send(new WeekCpEvent.OnChangeHeartValue(mWeekCpInfo.getHeartValue()));
    }

    @Subscribe
    public void onWeekCpMerry(WeekCpEvent.OnFromCpChange fromCpChange) {
        L.info(TAG, "onWeekCpMerry");
        WeekCpAttachment weekCpAttachment = fromCpChange.getWeekCpAttachment();
        if (weekCpAttachment != null) {
            int second = weekCpAttachment.getSecond();
            L.info(TAG, "onWeekCpMerry second: %d", second);
            if (second == WeekCpAttachment.ROOM_WEEK_CP_MERRY || second == WeekCpAttachment.ROOM_WEEK_CP_UPDATE || second == WeekCpAttachment.ROOM_WEEK_CP_BY_SOUL) {
                mTimerLock.lock();
                try {
                    int heartValue = weekCpAttachment.getHeartValue();
                    L.info(TAG, "onWeekCpMerry heartValue: %d", heartValue);
                    if (second == WeekCpAttachment.ROOM_WEEK_CP_MERRY) {
                        String cpId = weekCpAttachment.getCpId();
                        String uid = weekCpAttachment.getUid();
                        String userId = getUid();
                        if (Objects.equals(cpId, userId)) {
                            cpId = uid;
                        }
                        mWeekCpInfo.setCpId(cpId);
                        mWeekCpInfo.setHeartValue(0);
                    } else {
                        mWeekCpInfo.setHeartValue(heartValue);
                    }

                    mWeekCpInfo.setIntimacyValue(weekCpAttachment.getCloseLevel());
                    CoreUtils.send(new WeekCpEvent.OnChangeHeartValue(mWeekCpInfo.getHeartValue()));
                    starHeartTimer();
                } finally {
                    mTimerLock.unlock();
                }
            }
        }
    }

    @Subscribe
    public void onWeekCpSoulAddHeart(WeekCpEvent.OnWeekCpSoulAddHeart heart) {
        mTimerLock.lock();
        try {
            L.info(TAG, "onWeekCpSoulAddHeart heart: %d", heart.getHeart());
            mWeekCpInfo.setHeartValue(heart.getHeart());
            CoreUtils.send(new WeekCpEvent.OnChangeHeartValue(mWeekCpInfo.getHeartValue()));
        } finally {
            mTimerLock.unlock();
        }
    }

    @Subscribe
    public void OnTacitTestAskFinishAddHeart(TacitTestEvent.OnTacitTestAskFinish heart) {
        mTimerLock.lock();
        try {
            L.info(TAG, "OnTacitTestAskFinishAddHeart heart: %d", heart.getHeart());
            mWeekCpInfo.setHeartValue(heart.getHeart());
            CoreUtils.send(new WeekCpEvent.OnChangeHeartValue(mWeekCpInfo.getHeartValue()));
        } finally {
            mTimerLock.unlock();
        }
    }

    private boolean isOwner() {
        return AvRoomDataManager.get().isRoomOwner();
    }

    private void starHeartTimer() {
        if (mEasyTimer == null) {
            mEasyTimer = new EasyTimer();
            mEasyTimer.setDuration(mTimerDuration);
            mEasyTimer.setRunnable(new Runnable() {
                @Override
                public void run() {
                    mTimerLock.lock();
                    try {
                        int heartValue = mWeekCpInfo.getHeartValue();
                        L.info(TAG, "starHeartTimer heartValue: %d", heartValue);
                        heartValue++;
                        mWeekCpInfo.setHeartValue(heartValue);
                        if (heartValue >= ROOM_CP_MAX_VALUE) {
                            mEasyTimer.stop();
                            requestCheckCpHeart();
                        }
                        CoreUtils.send(new WeekCpEvent.OnChangeHeartValue(mWeekCpInfo.getHeartValue()));
                    } finally {
                        mTimerLock.unlock();
                    }
                }
            });
        }
        L.info(TAG, "starHeartTimer running: %b", mEasyTimer.isRuning());
        if (!mEasyTimer.isRuning()) {
            mEasyTimer.delayedStart(mTimerDuration);
        }
    }

    private void stopHeartTimer() {
        L.info(TAG, "stopHeartTimer");
        if (mEasyTimer != null) {
            mEasyTimer.stop();
        }
    }

    private void reset(String account) {
        L.info(TAG, "reset account: %s", account);
        RoomCpTicket cpTicket = new RoomCpTicket();
        cpTicket.setRoomId(getRoomUid());
        cpTicket.setChairChangeType(mStopHeartType);
        cpTicket.setUserId(getUid());
        cpTicket.setCpId(account);
        requestRoomCpHeart(cpTicket);
        stopHeartTimer();
    }

}
