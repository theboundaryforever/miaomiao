package com.tongdaxing.xchat_core.room.weekcp.bean;

import java.util.List;

/**
 * Created by Chen on 2019/5/9.
 */
public class TacitTestBean {

    /**
     * first : 56
     * second : 1
     * data : {"question":[{"question":"你喝咖啡会加奶吗","items":["会","不会"]},{"question":"你喜欢听什么类型的歌","items":["民谣","rap"]},{"question":"你喜欢cosplay吗","items":["喜欢","不喜欢"]},{"question":"你喜欢小公主还是小王子","items":["小公主","小王子"]},{"question":"上班空闲时，你会聊天还是玩手机","items":["聊天","玩手机"]}],"qid":1557478770850}
     */
    private int first;
    private int second;
    private DataBean data;

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * question : [{"question":"你喝咖啡会加奶吗","items":["会","不会"]},{"question":"你喜欢听什么类型的歌","items":["民谣","rap"]},{"question":"你喜欢cosplay吗","items":["喜欢","不喜欢"]},{"question":"你喜欢小公主还是小王子","items":["小公主","小王子"]},{"question":"上班空闲时，你会聊天还是玩手机","items":["聊天","玩手机"]}]
         * qid : 1557478770850
         */

        private long qid;
        private List<QuestionBean> question;

        public long getQid() {
            return qid;
        }

        public void setQid(long qid) {
            this.qid = qid;
        }

        public List<QuestionBean> getQuestion() {
            return question;
        }

        public void setQuestion(List<QuestionBean> question) {
            this.question = question;
        }

        public static class QuestionBean {
            /**
             * question : 你喝咖啡会加奶吗
             * items : ["会","不会"]
             */

            private String question;
            private List<String> items;

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public List<String> getItems() {
                return items;
            }

            public void setItems(List<String> items) {
                this.items = items;
            }
        }
    }
}
