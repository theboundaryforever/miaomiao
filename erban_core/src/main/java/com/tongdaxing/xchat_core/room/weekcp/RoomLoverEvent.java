package com.tongdaxing.xchat_core.room.weekcp;

import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;

import java.util.List;

/**
 * <p>
 * Created by zhangjian on 2019/5/7.
 */
public class RoomLoverEvent {

    public static class OnRequestSuccess {
        private List<OnlineChatMember> onlineChatMembers;

        public OnRequestSuccess(List<OnlineChatMember> onlineChatMembers) {
            this.onlineChatMembers = onlineChatMembers;
        }

        public List<OnlineChatMember> getOnlineChatMembers() {
            return onlineChatMembers;
        }
    }

    public static class OnRequestFailure {
        private String message;

        public OnRequestFailure(String message) {

            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class OnCpStateChange {
        private String mCpId;

        public OnCpStateChange(String cpId) {
            this.mCpId = cpId;
        }

        public String getCpId() {
            return mCpId;
        }
    }
}
