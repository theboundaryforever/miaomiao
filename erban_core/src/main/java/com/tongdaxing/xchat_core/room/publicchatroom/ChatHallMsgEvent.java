package com.tongdaxing.xchat_core.room.publicchatroom;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;

import java.util.List;

/**
 * <p>
 * Created by zhangjian on 2019/6/26.
 */
public class ChatHallMsgEvent {
    public static class OnReceiveNewMsg {
        private ChatRoomMessage chatRoomMessage;
        private List<ChatRoomMessage> chatRoomMessages;

        public OnReceiveNewMsg(List<ChatRoomMessage> chatRoomMessages) {
            this.chatRoomMessages = chatRoomMessages;
        }

        public OnReceiveNewMsg(ChatRoomMessage object) {
            this.chatRoomMessage = object;
        }

        public ChatRoomMessage getChatRoomMessage() {
            return chatRoomMessage;
        }

        public void setChatRoomMessage(ChatRoomMessage chatRoomMessage) {
            this.chatRoomMessage = chatRoomMessage;
        }

        public List<ChatRoomMessage> getChatRoomMessages() {
            return chatRoomMessages;
        }

        @Override
        public String toString() {
            return "OnReceiveNewMsg{" +
                    "chatRoomMessage=" + chatRoomMessage +
                    '}';
        }
    }

    public static class OnMsgDisposableNeedInit {

    }

    public static class OnSendMsgFailure {

    }
}
