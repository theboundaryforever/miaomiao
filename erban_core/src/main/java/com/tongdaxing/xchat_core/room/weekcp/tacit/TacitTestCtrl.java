package com.tongdaxing.xchat_core.room.weekcp.tacit;

import android.text.TextUtils;
import android.util.LongSparseArray;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.util.EasyTimer;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.im.custom.bean.WeekCpAttachment;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.weekcp.BaseWeekCtrl;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import org.greenrobot.eventbus.Subscribe;

import java.util.Map;

/**
 * Created by Chen on 2019/5/9.
 */
public class TacitTestCtrl extends BaseWeekCtrl implements ITacitTestCtrl {
    private String TAG = "TacitTestCtrl";

    private EasyTimer mEasyTimer;
    private int mTimerDuration = 1000;
    private int mCurrentTime;

    private LongSparseArray<TacitTextInfo> mRoomTalkTacitMsgs;

    public TacitTestCtrl() {
        CoreUtils.register(this);
        mRoomTalkTacitMsgs = new LongSparseArray<>();
    }

    // 应答默契考验
    @Override
    public void doAnswerTacit(String cpId) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", getUid());
        params.put("cpUid", cpId);
        params.put("roomUid", getRoomUid());
        params.put("ticket", getTicket());
        String url = UriProvider.getAnswerAsk();
        L.info(TAG, "doAnswerTacit url: %s, cpUid: %s", url, cpId);
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "doAnswerTacit onError: ", e);
            }

            @Override
            public void onResponse(String response) {
                showWeekCpFailedMsg(response);
                L.info(TAG, "doAnswerTacit onResponse: %s", response);
            }
        });
    }

    // 发起默契考验
    @Override
    public void doAskTacit(String cpId) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", getUid());
        params.put("cpUid", cpId);
        params.put("roomUid", getRoomUid());
        params.put("ticket", getTicket());
        String url = UriProvider.getAskTacit();
        L.info(TAG, "doAskTacit url: %s", url);
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "doAskTacit onError: ", e);
            }

            @Override
            public void onResponse(String response) {
                L.info(TAG, "doAskTacit onResponse: %s", response);
                showWeekCpFailedMsg(response);
            }
        });
    }

    // 默契考验提交
    @Override
    public void doTacitAnswerCollect(String cpId, String answer) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("answer", answer);
        params.put("uid", getUid());
        params.put("cpUid", cpId);
        String qid = String.valueOf(getInfo().getQuestionId());
        params.put("qid", qid);
        params.put("roomUid", getRoomUid());
        params.put("ticket", getTicket());
        String url = UriProvider.getTacitAnswerCollect();
        if (TextUtils.isEmpty(answer)) {
            exit();
        }
        L.info(TAG, "doTacitAnswerCollect url: %s, cpId: %s, answer: %s, qid: %s", url, cpId, answer, qid);
        OkHttpManager.getInstance().doGetRequest(url, null, params, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "doTacitAnswerCollect onError: ", e);
            }

            @Override
            public void onResponse(String response) {
                L.info(TAG, "doTacitAnswerCollect onResponse: %s", response);
                showWeekCpFailedMsg(response);
            }
        });
    }

    private void reset() {
        L.info(TAG, "reset");
        TacitTextInfo info = new TacitTextInfo();
        CoreManager.getCore(IRoomCore.class).getWeekManager().setTacitTextInfo(info);
    }

    private void starAnswerTimer() {
        L.info(TAG, "starAnswerTimer");
        if (mEasyTimer == null) {
            mCurrentTime = TACIT_TEST_TIME * (getInfo().getDataBeans().size() + 1);
            L.info(TAG, "starAnswerTimer mCurrentTime: %d", mCurrentTime);
            mEasyTimer = new EasyTimer();
            mEasyTimer.setDuration(mTimerDuration);
            mEasyTimer.setRunnable(new Runnable() {
                @Override
                public void run() {
                    mCurrentTime--;
                    L.info(TAG, "run mCurrentTime: %d", mCurrentTime);
                    if (mCurrentTime <= 0) {
                        TacitTextInfo info = getInfo();
                        info.setCurrentState(TACIT_TEST_PAST_DUE);
                        mRoomTalkTacitMsgs.put(info.getQuestionId(), getTacitTextInfo());
                        stopAnswerTimer();
                    }
                    CoreUtils.send(new TacitTestEvent.OnTacitTestTimeChange(mCurrentTime));
                }
            });
        }

        if (!mEasyTimer.isRuning()) {
            mEasyTimer.delayedStart(mTimerDuration);
        }
    }

    @Override
    public LongSparseArray<TacitTextInfo> getRoomTalkTacitMsgs() {
        return mRoomTalkTacitMsgs;
    }

    @Subscribe
    public void onFromCpChange(WeekCpEvent.OnFromCpChange fromCpChange) {
        WeekCpAttachment weekCpAttachment = fromCpChange.getWeekCpAttachment();
        if (weekCpAttachment != null) {
            int second = weekCpAttachment.getSecond();
            L.info(TAG, "onFromCpChange second: %d", second);
            if (second == WeekCpAttachment.ROOM_WEEK_CP_TIP) {
                String content = weekCpAttachment.getContent();
                if (!TextUtils.isEmpty(content)) {
                    try {
                        Long time = Long.valueOf(content);
                        TacitTextInfo tacitTextInfo = new TacitTextInfo();
                        mRoomTalkTacitMsgs.put(time, tacitTextInfo);
                    } catch (Exception e) {
                        CoreUtils.crashIfDebug(e, "onWeekCpMerry exception");
                    }
                }
            }
        }
    }

    private void stopAnswerTimer() {
        L.info(TAG, "stopAnswerTimer");
        if (mEasyTimer != null) {
            mEasyTimer.stop();
            mEasyTimer = null;
        }
    }

    @Subscribe
    public void onTacitTestAskExit(TacitTestEvent.OnTacitTestAskExit askExit) {
        L.info(TAG, "onTacitTestAskExit");
        exit();
    }

    private void exit() {
        TacitTextInfo info = getInfo();
        info.setCurrentState(TACIT_TEST_CANCEL);
        long questionId = info.getQuestionId();
        int currentState = info.getCurrentState();
        L.info(TAG, "onTacitTestAskExit questionId: %s, currentState: %d", String.valueOf(questionId), currentState);
        mRoomTalkTacitMsgs.put(questionId, getTacitTextInfo());
        reset();
        stopAnswerTimer();
    }

    private TacitTextInfo getTacitTextInfo() {
        TacitTextInfo textInfo = new TacitTextInfo();
        TacitTextInfo info = getInfo();

        long questionId = info.getQuestionId();
        int currentState = info.getCurrentState();
        L.info(TAG, "getTacitTextInfo questionId: %s, currentState: %d", String.valueOf(questionId), currentState);
        textInfo.setCurrentState(info.getCurrentState());
        textInfo.setQuestionId(info.getQuestionId());
        textInfo.setDataBeans(info.getDataBeans());
        textInfo.setQuestionAnswer(info.getQuestionAnswer());
        textInfo.setTacitAnswerBean(info.getTacitAnswerBean());
        return textInfo;
    }

    public void setTacitTextInfo(TacitTextInfo tacitTextInfo) {

    }

    @Subscribe
    public void onTacitTestAskFinish(TacitTestEvent.OnTacitTestAskFinish finish) {
        TacitTextInfo info = getInfo();
        long questionId = info.getQuestionId();
        int currentState = info.getCurrentState();
        L.info(TAG, "onTacitTestAskFinish questionId: %s, currentState: %d", String.valueOf(questionId), currentState);
        mRoomTalkTacitMsgs.put(questionId, getTacitTextInfo());
        CoreUtils.send(new TacitTestEvent.OnTacitTestAskFinishShowView(String.valueOf(questionId)));
        reset();
        stopAnswerTimer();
    }

    @Subscribe
    public void onTacitTestAsk(TacitTestEvent.OnTacitTestAsk tacitTestAsk) {
        mRoomTalkTacitMsgs.put(tacitTestAsk.getQuestionId(), getTacitTextInfo());
        starAnswerTimer();
    }

    private TacitTextInfo getInfo() {
        return CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo();
    }

}
