package com.tongdaxing.xchat_core.room.presenter;

import android.annotation.SuppressLint;

import com.orhanobut.logger.Logger;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.model.HomeLoverOnlineUserModel;
import com.tongdaxing.xchat_core.room.view.IHomeLoverUserListView;
import com.tongdaxing.xchat_core.room.weekcp.RoomLoverEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.functions.Consumer;

/**
 * <p>
 * Created by zhangjian on 2019/5/7.
 */
public class HomeLoverUserListPresenter extends AbstractMvpPresenter<IHomeLoverUserListView> {

    public static final String TAG = "HomeLoverUserListPresenter";
    private final HomeLoverOnlineUserModel homeLoverOnlineUserModel;

    public HomeLoverUserListPresenter() {
        homeLoverOnlineUserModel = new HomeLoverOnlineUserModel();
    }

    /**
     * 分页获取房间成员：第一页包含队列成员，固定成员，游客50人，之后每一页获取游客50人
     */
    @SuppressLint("CheckResult")
    public void requestChatMemberByPage(boolean isFirstLoad) {
        L.info(TAG, "loadData() -> requestChatMemberByPage()");
        homeLoverOnlineUserModel.getPageMembers()
                .delay(isFirstLoad ? 0 : 2, TimeUnit.SECONDS)
                .retry(1)
                .subscribe(new Consumer<List<OnlineChatMember>>() {
                    @Override
                    public void accept(List<OnlineChatMember> onlineChatMembers) throws Exception {
                        L.info(TAG, "requestChatMemberByPage() getPageMembers() Success onlineChatMembers.size = %d", onlineChatMembers.size());
                        Logger.i("第%1d页成员人数:%2d", Constants.PAGE_START, onlineChatMembers.size());
                        CoreUtils.send(new RoomLoverEvent.OnRequestSuccess(onlineChatMembers));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        L.error(TAG, "requestChatMemberByPage() getPageMembers() Failure: ", throwable);
                        throwable.printStackTrace();
                        Logger.i("第%d页成员人数失败:%s", Constants.PAGE_START, throwable.getMessage());
                        CoreUtils.send(new RoomLoverEvent.OnRequestFailure(throwable.getMessage()));
                    }
                });
    }


    /**
     * 成员进来刷新在线列表
     *
     * @param account
     * @param onlineChatMembers
     */
    @SuppressLint("CheckResult")
    public void onMemberInRefreshData(String account, List<OnlineChatMember> onlineChatMembers) {
        homeLoverOnlineUserModel.onMemberInRefreshData(account, Constants.PAGE_START, onlineChatMembers)
//                .compose(this.<List<OnlineChatMember>>bindUntilEvent(PresenterEvent.DESTROY))
                .subscribe(new Consumer<List<OnlineChatMember>>() {
                    @Override
                    public void accept(List<OnlineChatMember> onlineChatMembers) throws Exception {
                        CoreUtils.send(new RoomLoverEvent.OnRequestSuccess(onlineChatMembers));
                    }
                });

    }
}
