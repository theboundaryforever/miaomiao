package com.tongdaxing.xchat_core.room.weekcp;

import com.tongdaxing.xchat_core.room.weekcp.tacit.ITacitTestCtrl;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTextInfo;
import com.tongdaxing.xchat_core.room.weekcp.topic.ISoulTopicCtrl;
import com.tongdaxing.xchat_core.room.weekcp.topic.SoulTopicInfo;
import com.tongdaxing.xchat_core.room.weekcp.week.IWeekCpCtrl;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpInfo;

/**
 * Created by Chen on 2019/5/7.
 */
public interface IWeekManager {

    IWeekCpCtrl getWeekCpCtrl();

    WeekCpInfo getWeekCpInfo();

    ISoulTopicCtrl getSoulTopicCtrl();

    SoulTopicInfo getSoulTopicInfo();

    ITacitTestCtrl getTacitTestCtrl();

    TacitTextInfo getTacitTextInfo();

    void setTacitTextInfo(TacitTextInfo tacitTextInfo);
}
