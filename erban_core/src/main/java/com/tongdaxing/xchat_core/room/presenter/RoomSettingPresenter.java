package com.tongdaxing.xchat_core.room.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.NewTabInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.RoomSettingModel;
import com.tongdaxing.xchat_core.room.view.IRoomSettingView;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/15
 */
public class RoomSettingPresenter extends AbstractMvpPresenter<IRoomSettingView> {
    private final RoomSettingModel model;

    public RoomSettingPresenter() {
        model = new RoomSettingModel();
    }

    public void requestTagAll(final int roomType) {
        model.requestTagAll(roomType, new OkHttpManager.MyCallBack<ServiceResult<List<NewTabInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null && e != null) {
                    getMvpView().onResultRequestTagAllFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<NewTabInfo>> response) {
                IRoomSettingView roomSettingView = getMvpView();
                if (roomSettingView == null) return;
                if (null != response && response.isSuccess() && response.getData() != null) {
                    roomSettingView.onResultRequestTagAllSuccess(response.getData());
                } else {
                    onError(new Exception());
                }
            }
        });
    }

    boolean changeGiftEffect = false;//判断开关 避免后台发送更新信息太快而出现无法判断状态是否改变的情况

    /**
     * 更新房间设置信息
     *
     * @param title
     * @param pwd
     * @param label   标签名字
     * @param tagId   标签id
     * @param backPic
     */
    public void updateRoomInfo(String title, String pwd, String label, int tagId, String backPic, final int giftEffect, int drawMsgOption) {
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if ("".equals(title)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                title = userInfo.getNick() + "的房间";
            }
        }

        if (AvRoomDataManager.get().mCurrentRoomInfo.getGiftEffectSwitch() != giftEffect)
            changeGiftEffect = true;
        ResponseListener listener = new ResponseListener<ServiceResult<RoomInfo>>() {
            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                IRoomSettingView roomSettingView = getMvpView();
                if (roomSettingView == null) return;
                if (null != data && data.isSuccess()) {
                    roomSettingView.updateRoomInfoSuccess(data.getData());
                    //判断是否发送屏蔽小礼物特效消息
                    if (AvRoomDataManager.get().mCurrentRoomInfo != null && changeGiftEffect) {
                        IMNetEaseManager.get().systemNotificationBySdk(uid, giftEffect == 1 ?
                                CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN :
                                CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE, -1);
                        changeGiftEffect = false;
                    }
                } else {
                    roomSettingView.updateRoomInfoFail(data.getErrorMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if (getMvpView() != null)
                    getMvpView().updateRoomInfoFail(error.getErrorStr());
            }
        };
        model.doUpdateRoomInfo(title, pwd, label, tagId, uid,
                CoreManager.getCore(IAuthCore.class).getTicket(), backPic, giftEffect, drawMsgOption, listener, errorListener);
    }


    boolean giftEffectAdmin = false;

    public void updateByAdmin(long roomUid, String title, String pwd, String label, int tagId, String backPic, final int giftEffect, int drawMsgOption) {
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if ("".equals(title)) {
            title = null;
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                title = userInfo.getNick() + "的房间";
            }
        }
        if (AvRoomDataManager.get().mCurrentRoomInfo.getGiftEffectSwitch() != giftEffect)
            giftEffectAdmin = true;
        ResponseListener listener = new ResponseListener<ServiceResult<RoomInfo>>() {
            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                IRoomSettingView roomSettingView = getMvpView();
                if (roomSettingView == null) return;
                if (null != data && data.isSuccess()) {
                    //判断是否发送屏蔽小礼物特效消息
                    if (AvRoomDataManager.get().mCurrentRoomInfo != null && giftEffectAdmin) {
                        IMNetEaseManager.get().systemNotificationBySdk(uid, giftEffect == 1 ? CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN : CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE, -1);
                        giftEffectAdmin = false;
                    }
                    roomSettingView.updateRoomInfoSuccess(data.getData());
                } else {
                    roomSettingView.updateRoomInfoFail(data.getErrorMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if (getMvpView() != null)
                    getMvpView().updateRoomInfoFail(error.getErrorStr());
            }
        };
        model.updateByAdmin(roomUid, title, pwd, label, tagId, uid,
                CoreManager.getCore(IAuthCore.class).getTicket(), backPic, giftEffect, drawMsgOption, listener, errorListener);
    }
}
