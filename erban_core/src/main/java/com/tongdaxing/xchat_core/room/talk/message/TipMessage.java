package com.tongdaxing.xchat_core.room.talk.message;

import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.room.talk.TalkType;

/**
 * Created by Chen on 2019/6/25.
 */
public class TipMessage extends CustomAttachment  {
    private String mContent;

    @Override
    public int getRoomTalkType() {
        return TalkType.ROOM_TALK_NETEASE_TIP;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }
}
