package com.tongdaxing.xchat_core.room.talk;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Chen on 2019/4/18.
 */
public class TalkType {

    // 公屏聊天的item类型
    public static final int ROOM_TALK_NETEASE_TIP = 9;              // 云信提示消息: MsgTypeEnum.tip
    public static final int ROOM_TALK_NETEASE_TEXT = 2;             // 云信文本消息: MsgTypeEnum.text
    public static final int ROOM_TALK_NETEASE_NOTIFICATION = 8;     // 云信通知消息: MsgTypeEnum.notification
    public static final int ROOM_TALK_NETEASE_CUSTOM = 11;        // 云信自定义消息: MsgTypeEnum.custom
    /**
     * 上面4种是云信提供的消息类型；
     * 下面是我们根据云信第四种自定义消息类型来定义的
     */
    public static final int ROOM_TALK_HEADER_TYPE_AUCTION = 1001;       // 公屏关注
    public static final int ROOM_TALK_HEADER_TYPE_TIP = 1002;            // 房间提示消息
    public static final int ROOM_TALK_HEADER_TYPE_GIFT = 1003;          // 礼物
    public static final int ROOM_TALK_HEADER_TYPE_FACE = 1005; // 麦上表情标签
    public static final int ROOM_TALK_HEADER_TYPE_EGG = 1006;   // 砸蛋
    public static final int ROOM_TALK_HEADER_COMMON = 1007;    // 多种消息类型的
    public static final int ROOM_TALK_HEADER_TYPE_WEEK_CP = 1008;    // 一周夫妻
    public static final int ROOM_TALK_HEADER_TYPE_ATTENTION = 1009;    // 关注

    @IntDef({ROOM_TALK_NETEASE_TEXT,
            ROOM_TALK_NETEASE_TIP,
            ROOM_TALK_NETEASE_NOTIFICATION,
            ROOM_TALK_NETEASE_CUSTOM,
            ROOM_TALK_HEADER_TYPE_GIFT,
            ROOM_TALK_HEADER_TYPE_AUCTION,
            ROOM_TALK_HEADER_TYPE_TIP,
            ROOM_TALK_HEADER_TYPE_FACE,
            ROOM_TALK_HEADER_TYPE_EGG,
            ROOM_TALK_HEADER_COMMON,
            ROOM_TALK_HEADER_TYPE_WEEK_CP,
            ROOM_TALK_HEADER_TYPE_ATTENTION
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }

}
