package com.tongdaxing.xchat_core.room.charm;

import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * 魅力值 core
 */
public interface ICharmCore extends IBaseCore {

    /**
     * 请求获取房间魅力值
     *
     * @param roomId  房间id
     * @param roomUid 房主uid
     */
    void requestRoomCharm(long roomId, long roomUid);

}
