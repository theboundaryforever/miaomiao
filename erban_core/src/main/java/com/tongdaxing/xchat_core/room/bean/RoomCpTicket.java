package com.tongdaxing.xchat_core.room.bean;

/**
 * Created by Chen on 2019/4/29.
 */
public class RoomCpTicket {
    private String mUserId;
    private String mCpId;
    private String mRoomId;
    private int mChairChangeType;

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getCpId() {
        return mCpId;
    }

    public void setCpId(String cpId) {
        mCpId = cpId;
    }

    public String getRoomId() {
        return mRoomId;
    }

    public void setRoomId(String roomId) {
        mRoomId = roomId;
    }

    public int getChairChangeType() {
        return mChairChangeType;
    }

    public void setChairChangeType(int chairChangeType) {
        mChairChangeType = chairChangeType;
    }

    @Override
    public String toString() {
        return "RoomCpTicket{" +
                "mUserId='" + mUserId + '\'' +
                ", mCpId='" + mCpId + '\'' +
                ", mRoomId='" + mRoomId + '\'' +
                ", mChairChangeType=" + mChairChangeType +
                '}';
    }
}
