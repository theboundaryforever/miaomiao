package com.tongdaxing.xchat_core.room.charm;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * 魅力值core实现
 */
public class CharmCoreImpl extends AbstractBaseCore implements ICharmCore {

    @Override
    public void requestRoomCharm(long roomId, long roomUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("roomId", String.valueOf(roomId));
        params.put("roomUid", String.valueOf(roomUid));
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getRoomCharm(), null, params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                CoreManager.notifyClients(ICharmClient.class, ICharmClient.METHOD_ON_REQUEST_CHARM_FAIL, "网络不稳定，请稍后再试");
            }

            @Override
            public void onResponse(Json response) {
                try {
                    int code = response.num("code");
                    if (code == 200) {
                        CoreManager.notifyClients(ICharmClient.class, ICharmClient.METHOD_ON_REQUEST_CHARM_SUCCESS, (JSONObject) response.obj("data"));
                    } else {
                        CoreManager.notifyClients(ICharmClient.class, ICharmClient.METHOD_ON_REQUEST_CHARM_FAIL, response.get("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    onError(e);
                }
            }
        });
    }
}
