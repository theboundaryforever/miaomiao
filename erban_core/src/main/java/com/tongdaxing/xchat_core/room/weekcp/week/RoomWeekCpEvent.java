package com.tongdaxing.xchat_core.room.weekcp.week;

/**
 * Created by Chen on 2019/4/30.
 */
public class RoomWeekCpEvent {
    public static class OnCheckCpHeart {
        String mValue;

        public OnCheckCpHeart(String value) {
            this.mValue = value;
        }

        public String getValue() {
            return mValue;
        }
    }
}
