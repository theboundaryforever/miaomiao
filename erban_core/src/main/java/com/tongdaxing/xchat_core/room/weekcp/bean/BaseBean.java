package com.tongdaxing.xchat_core.room.weekcp.bean;

/**
 * Created by Chen on 2019/5/5.
 */
public class BaseBean {
    int code;
    String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "HeartValueBean{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
