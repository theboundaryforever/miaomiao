package com.tongdaxing.xchat_core.room.presenter;

import com.orhanobut.logger.Logger;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.HomePartyUserListModel;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.room.view.IHomePartyUserListView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class HomePartyUserListPresenter extends AbstractMvpPresenter<IHomePartyUserListView> {

    private final HomePartyUserListModel mHomePartyUserListMode;

    public HomePartyUserListPresenter() {
        mHomePartyUserListMode = new HomePartyUserListModel();
    }

    /**
     * 分页获取房间成员：第一页包含队列成员，固定成员，游客50人，之后每一页获取游客50人
     *
     * @param page 页数
     * @param time 固定成员列表用updateTime,
     *             游客列表用进入enterTime，
     *             填0会使用当前服务器最新时间开始查询，即第一页，单位毫秒
     */
    public void requestChatMemberByPage(final int page, long time, List<OnlineChatMember> oldList) {
//        DeferredScalarSubscription
        mHomePartyUserListMode.getPageMembers(page, time, oldList)
                .subscribe(new Consumer<List<OnlineChatMember>>() {
                    @Override
                    public void accept(List<OnlineChatMember> onlineChatMembers) throws Exception {
                        Logger.i("第%1d页成员人数:%2d", page, onlineChatMembers.size());
                        if (getMvpView() != null) {
                            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        Logger.i("第%d页成员人数失败:%s", page, throwable.getMessage());
                        if (getMvpView() != null) {
                            getMvpView().onRequestChatMemberByPageFail(throwable.getMessage(), page);
                        }
                    }
                });
    }

    /**
     * 房间发送礼物
     *
     * @param giftId    礼物id
     * @param targetUid 接收礼物的用户对象id
     * @param giftNum   礼物数量
     * @param goldPrice 礼物金币数量
     */
    @Deprecated //如果要发送礼物情调用CoreManager.getCore(IGiftCore.class).sendGiftWithGold
    public void sendRoomGift(int giftId, long targetUid, int giftNum, int goldPrice) {
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom != null) {
            mHomePartyUserListMode.sendRoomGift(giftId, targetUid, currentRoom.getUid(), giftNum, goldPrice);
        }
    }

    /**
     * 全麦送礼物
     *
     * @param giftId         礼物id
     * @param micMemberInfos 麦上的所有成员
     * @param giftNum        礼物数量
     * @param goldPrice      礼物金币数量
     */
    @Deprecated //如果要发送礼物情调用CoreManager.getCore(IGiftCore.class).sendGiftWithGold
    public void sendRoomMultiGift(int giftId, List<MicMemberInfo> micMemberInfos, int giftNum, int goldPrice) {
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom != null) {
            List<Long> targetUids = new ArrayList<>();
            for (int i = 0; i < micMemberInfos.size(); i++) {
                targetUids.add(micMemberInfos.get(i).getUid());
            }
            mHomePartyUserListMode.sendRoomMultiGift(giftId, targetUids, currentRoom.getUid(), giftNum, goldPrice);
        }
    }

    /**
     * 成员进来刷新在线列表
     *
     * @param account
     * @param onlineChatMembers
     */
    public void onMemberInRefreshData(String account, List<OnlineChatMember> onlineChatMembers, final int page) {
        mHomePartyUserListMode.onMemberInRefreshData(account,page, onlineChatMembers)
//                .compose(this.<List<OnlineChatMember>>bindUntilEvent(PresenterEvent.DESTROY))
                .subscribe(new Consumer<List<OnlineChatMember>>() {
                    @Override
                    public void accept(List<OnlineChatMember> onlineChatMembers) throws Exception {
                        if (getMvpView() != null)
                            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                    }
                });

    }

    public void onMemberDownUpMic(String account, boolean isUpMic, List<OnlineChatMember> dataList,
                                  final int page) {
        mHomePartyUserListMode.onMemberDownUpMic(account, isUpMic, dataList)
                .subscribe(new Consumer<List<OnlineChatMember>>() {
                    @Override
                    public void accept(List<OnlineChatMember> onlineChatMembers) throws Exception {
                        if (getMvpView() != null)
                            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                    }
                });
    }


    public void onUpdateMemberManager(String account, List<OnlineChatMember> dataList,
                                      boolean isRemoveManager, final int page) {
        mHomePartyUserListMode.onUpdateMemberManager(account, isRemoveManager, dataList)
                .subscribe(new Consumer<List<OnlineChatMember>>() {
                    @Override
                    public void accept(List<OnlineChatMember> onlineChatMembers) throws Exception {
                        if (getMvpView() != null)
                            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                    }
                });
    }
}
