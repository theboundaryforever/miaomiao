package com.tongdaxing.xchat_core.room.model;

import android.os.Handler;
import android.text.TextUtils;
import android.util.SparseArray;

import com.netease.nimlib.sdk.AbortableFuture;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.util.api.RequestResult;
import com.orhanobut.logger.Logger;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RxNet;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.HerUserInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.json.JsonParser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.agora.rtc.Constants;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * <p> 房间网络数据操作 </p>
 *
 * @author jiahui
 * @date 2017/12/11
 */
public class AvRoomModel extends RoomBaseModel {

    public static final String CP_LEVEL = "cp_level";//cp尾灯级别

    private final AvRoomService mRoomService;
    public static final String EXPER_LEVEL = "experLevel";
    public static final String EXPER_CHARM = "charmLevel";
    public static final String USER_NICK = "user_nick";
    public static final String USER_CAR = "user_car";
    public static final String USER_CAR_NAME = "user_car_name";
    public static final String IS_NEW_USER = "is_new_user";
    private static final String ROOM_ID = "room_id";
    public static final String HER_USER_INFO = "her_user_info";
    public static final String PUBLIC_CHAT_ROOM = "PUBLIC_CHAT_ROOM";
    public static final String CP_SIGN = "cp_sign";//cp尾灯
    public static final String CUSTOM_LABEL_URL = "customSign";//自定义尾灯
    private String TAG = "AvRoomModel";

    public AvRoomModel() {
        mRoomService = RxNet.create(AvRoomService.class);
    }

    public static final int PUBLIC_CHAT_ROOM_TYPE = 1;


    /**
     * 进入云信聊天室
     *
     * @param roomId     房间ID
     * @param retryCount 重试次数
     */
    public Observable<EnterChatRoomResultData> enterRoom(final long roomId, final int retryCount) {
        return enterRoom(roomId, retryCount, 0, null);
    }

    /**
     * 进入云信聊天室
     *
     * @param roomId      房间ID
     * @param retryCount  重试次数
     * @param herUserInfo 去找TA时，TA的资料
     */
    public Observable<EnterChatRoomResultData> enterRoom(final long roomId, final int retryCount, HerUserInfo herUserInfo) {
        return enterRoom(roomId, retryCount, 0, herUserInfo);
    }

    public Observable<EnterChatRoomResultData> enterRoom(final long roomId, final int retryCount, int type, HerUserInfo herUserInfo) {
        final EnterChatRoomData enterChatRoomData = new EnterChatRoomData(String.valueOf(roomId));


        Map<String, Object> stringIntegerMap = new HashMap<>();
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();

        if (type == PUBLIC_CHAT_ROOM_TYPE) {
            stringIntegerMap.put(PUBLIC_CHAT_ROOM, PUBLIC_CHAT_ROOM);
        }
        if (userInfo != null) {
            //FIXME 退房时没有下麦 然后重进同一个房间 很大概率会出现experLevel=0 CarUrl=null CarName=null的情况 需要检查AvRoomModel.exitRoom(CallBack<java.lang.String>, int)
            L.info(TAG, "enterRoom setEnterChatRoomData setNotifyExtension setExtension userInfo.getExperLevel() = %d", userInfo.getExperLevel());
            stringIntegerMap.put(EXPER_LEVEL, userInfo.getExperLevel());
            stringIntegerMap.put(EXPER_CHARM, userInfo.getCharmLevel());
            stringIntegerMap.put(USER_NICK, userInfo.getNick());
            stringIntegerMap.put(USER_CAR, userInfo.getCarUrl());
            stringIntegerMap.put(USER_CAR_NAME, userInfo.getCarName());
            stringIntegerMap.put(ROOM_ID, roomId + "");
            stringIntegerMap.put(CP_SIGN, (userInfo.getCpUser() == null || userInfo.getSign() == null) ? "" : userInfo.getSign());
            stringIntegerMap.put(CP_LEVEL, userInfo.getCpUser() == null ? 0 : userInfo.getCpUser().getCpGiftLevel());
            stringIntegerMap.put(CUSTOM_LABEL_URL, userInfo.getCustomSign());
            if (herUserInfo != null) {
                stringIntegerMap.put(HER_USER_INFO, JsonParser.toJson(herUserInfo));
            }
            //少于3天都是新用户
            LogUtils.d("enterRoom", System.currentTimeMillis() + "   " + userInfo.getCreateTime());
            if (System.currentTimeMillis() - userInfo.getCreateTime() < (86400 * 3000)) {
                UserInfo userInfo1 = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (userInfo1 != null && !TextUtils.isEmpty(userInfo1.getUserLabel())) {
                    stringIntegerMap.put(IS_NEW_USER, userInfo1.getUserLabel());
                } else {
                    stringIntegerMap.put(IS_NEW_USER, "new");
                }
                LogUtils.d("enterRoom", System.currentTimeMillis() - userInfo.getCreateTime());
            }
        }

        enterChatRoomData.setNotifyExtension(stringIntegerMap);//此字段没有应用
        enterChatRoomData.setExtension(stringIntegerMap);
        Observable<EnterChatRoomResultData> enterChatRoomResultDataObservable = Observable.create(new ObservableOnSubscribe<EnterChatRoomResultData>() {
            @Override
            public void subscribe(ObservableEmitter<EnterChatRoomResultData> e) throws Exception {
                AbortableFuture<EnterChatRoomResultData> enterChatRoomEx = NIMChatRoomSDK.getChatRoomService().enterChatRoomEx(enterChatRoomData, retryCount);
                RequestResult<EnterChatRoomResultData> requestResult = NIMClient.syncRequest(enterChatRoomEx);
                executeNIMClient(requestResult, e);
            }
        }).subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());
        return enterChatRoomResultDataObservable;
    }


    /**
     * 离开聊天室（退出房间）
     */
    public void quiteRoom(String roomId) {
        L.info(TAG, "quiteRoom roomId: %s", roomId);
        if (!TextUtils.isEmpty(roomId)) {
//            LogUtil.d("quiteRoom111", "1111");
            AvRoomDataManager.get().release();
            //云信的房间退出
            LogUtils.d("nim_sdk", "exitChatRoom_1");
            NIMClient.getService(ChatRoomService.class).exitChatRoom(roomId);
        }
    }

    /**
     * 退出房间
     * 2.4.5后添加退出麦序的逻辑
     */
    public void exitRoom(final CallBack<String> callBack) {
        exitRoom(callBack, 200);
    }

    // : 2018/4/28 逻辑好乱记得改
    public void exitRoom(final CallBack<String> callBack, final int time) {

        L.info(TAG, "exitRoom time: %d", time);
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;

        if (currentRoom == null) {
            return;
        }


        String account = CoreManager.getCore(IAuthCore.class).getCurrentUid() + "";
        L.info(TAG, "exitRoom account: %s", account);

        CoreUtils.send(new NewRoomEvent.OnExitRoom(account));

        boolean onMic = AvRoomDataManager.get().isOnMic(Long.valueOf(account));
        //如果在排麦队列又不在麦上则先移除队列在离开房间
        if (AvRoomDataManager.get().checkInMicInlist() && !onMic) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            LogUtils.d("AvExitRoom", userInfo.getUid() + "     " + roomInfo.getRoomId());
            IMNetEaseManager.get().removeMicInList(userInfo.getUid() + "", roomInfo.getRoomId() + "", new RequestCallback() {
                @Override
                public void onSuccess(Object param) {
                    LogUtils.d("AvExitRoom", "onSuccess");
                    L.info(TAG, "exitRoom onSuccess: %s", param);
                    exitUserRoom(time, callBack, currentRoom);
                }

                @Override
                public void onFailed(int code) {
                    LogUtils.d("AvExitRoom", "onFailed" + code);
                    L.info(TAG, "exitRoom onFailed: %d", code);
                    exitUserRoom(time, callBack, currentRoom);
                }

                @Override
                public void onException(Throwable exception) {
                    LogUtils.d("AvExitRoom", "onException");
                    L.error(TAG, "exitRoom onException: ", exception);
                    exitUserRoom(time, callBack, currentRoom);
                }
            });
        } else {
            if (onMic) {
                //在麦上的要退出麦
//            LogUtils.d("nim_sdk", "chatRoomMemberExit     " + account);
                //延迟发送退出，
                IMNetEaseManager.get().downMicroPhoneBySdk(AvRoomDataManager.get().getMicPosition(Long.valueOf(account)), new CallBack<String>() {
                    @Override
                    public void onSuccess(String data) {

                        exitUserRoom(time, callBack, currentRoom);
                    }

                    @Override
                    public void onFail(int code, String error) {
                        exitUserRoom(time, callBack, currentRoom);
                    }
                });
                SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
                int size = mMicQueueMemberMap.size();
                for (int i = 0; i < size; i++) {
                    RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                    //房主的话不移除
                    if (roomQueueInfo.mChatRoomMember != null
                            && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), account)) {

                        LogUtils.d("remove  mChatRoomMember", 1 + "");

                        RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                        roomQueueInfo.mChatRoomMember = null;

                        IMNetEaseManager.get().noticeDownMic(String.valueOf(mMicQueueMemberMap.keyAt(i)), account);
                        break;
                    }
                }

            } else {
                quitUserRoom(callBack, currentRoom);

                Logger.i("quitUserRoom", 1 + "1111");
            }
        }
    }

    private void exitUserRoom(int time, final CallBack<String> callBack, final RoomInfo currentRoom) {
        L.info(TAG, "exitUserRoom time: %d, currentRoom: %s", time, currentRoom);
        if (time == 0) {
            quitUserRoom(callBack, currentRoom);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Logger.i("quitUserRoom", 3 + "3333");
                    L.info(TAG, "exitUserRoom postDelayed");
                    quitUserRoom(callBack, currentRoom);
                }
            }, time);
        }
    }

    private void quitUserRoom(CallBack<String> callBack, RoomInfo currentRoom) {
        String roomId = String.valueOf(currentRoom.getRoomId());
        LogUtils.d("AVRoomMsgQuit", roomId + "");
        quiteRoom(roomId);
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        L.info(TAG, "quitUserRoom roomId: %s, uid: %d, currentUid", roomId, uid);
        if (currentRoom.getUid() == uid) {
            if (currentRoom.getType() != RoomInfo.ROOMTYPE_HOME_PARTY) {
                L.info(TAG, "quitUserRoom roomType: %d, currentUid", currentRoom.getType());
                ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
                    @Override
                    public void onResponse(ServiceResult<String> data) {
                        L.info(TAG, "quitUserRoom onResponse, data: %s", data);
                        if (data != null && data.isSuccess()) {
                            Logger.i("通知服务端退出房间成功:" + data);
                        } else {
                            Logger.i("通知服务端退出房间失败:" + data.getErrorMessage() + "  " + "code:" + data.getCode());
                        }
                    }
                };

                ResponseErrorListener errorListener = new ResponseErrorListener() {
                    @Override
                    public void onErrorResponse(RequestError error) {
                        L.info(TAG, "quitUserRoom onErrorResponse: ", error);
                        Logger.i("通知服务端退出房间失败:" + error + "  " + "code:" + error.getErrorStr());
                    }
                };
                quiteRoomForOurService(String.valueOf(uid), CoreManager.getCore(IAuthCore.class).getTicket(), listener, errorListener);
            }
        }
        IMNetEaseManager.get().getChatRoomEventObservable()
                .onNext(new RoomEvent().setEvent(RoomEvent.ROOM_EXIT));
        if (callBack != null) {
            callBack.onSuccess("成功");
        }

        Logger.i("quitUserRoom 离开房间id:" + String.valueOf(uid));
//        LogUtil.d("exitRoomquitUserRoom", String.valueOf(uid) + "      " + CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
            @Override
            public void onResponse(ServiceResult<String> data) {
                if (data != null) {
                    if (data.isSuccess()) {
                        Logger.i("quitUserRoom 通知服务端退出房间成功:" + data);
                    } else {
                        Logger.i("quitUserRoom 通知服务端退出房间失败:" + data);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.i("quitUserRoom 通知服务端退出房间失败:" + error.getMessage());
            }
        };
        quitUserRoom(String.valueOf(uid), CoreManager.getCore(IAuthCore.class).getTicket(), listener, errorListener);
    }

    /**
     * 通知服务端房间退出
     *
     * @param uId
     * @param ticket
     */
    public void quiteRoomForOurService(String uId, String ticket, ResponseListener listener, ResponseErrorListener errorListener) {
//        execute(mRoomService.quiteRoomForOurService(uId, ticket)
//                .subscribeOn(Schedulers.io())
//                .unsubscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread()), callBack);
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", uId);
        requestParam.put("ticket", ticket);
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.closeRoom(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    public void quitUserRoom(String uId, String ticket, ResponseListener listener, ResponseErrorListener errorListener) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", uId);
        requestParam.put("ticket", ticket);
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.userRoomOut(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    public void requestRoomInfoFromService(String uid, ResponseListener listener, ResponseErrorListener errorListener) {
        L.debug(TAG, "enterRoomFromService uId: %s", uid);
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", uid);
        requestParam.put("visitorUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRoomInfo(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);
    }

    public void getRoomInfoNoRealNameAuth(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("visitorUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getRoomInfoInCreateRoom(), param, myCallBack);
    }

    /**
     * 获取活动信息
     */
    public void getActionDialog(int type, ResponseListener listener, ResponseErrorListener errorListener) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("type", type + "");
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRedBagDialogType(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);
//        execute(mRoomService.getActionDialog(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", String.valueOf(type))
//                .subscribeOn(Schedulers.io())
//                .unsubscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread()), callBack);
    }

    public void userRoomIn(String uid, long roomId, ResponseListener listener, ResponseErrorListener errorListener) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", uid);
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        requestParam.put("roomUid", roomId + "");
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.userRoomIn(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
//        return mRoomService.userRoomIn(uid, CoreManager.getCore(IAuthCore.class).getTicket(),
//                String.valueOf(roomId))
//                .subscribeOn(Schedulers.io())
//                .unsubscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread());
    }

    public void getNormalChatMember(String roomId, final long currentUid) {
        NIMChatRoomSDK.getChatRoomService()
                .fetchRoomMembers(roomId, MemberQueryType.NORMAL, 0, 500)
                .setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
                    @Override
                    public void onResult(int i, List<ChatRoomMember> chatRoomMemberList, Throwable throwable) {
                        if (ListUtils.isListEmpty(chatRoomMemberList)) {
                            return;
                        }
                        AvRoomDataManager.get().clearMembers();

                        for (ChatRoomMember chatRoomMember : chatRoomMemberList) {
                            if (Objects.equals(chatRoomMember.getAccount(), String.valueOf(currentUid))) {
                                //自己
                                AvRoomDataManager.get().mOwnerMember = chatRoomMember;
                            }
                            if (chatRoomMember.getMemberType() == MemberType.ADMIN) {
                                AvRoomDataManager.get().addAdminMember(chatRoomMember);
                            }
                            if (chatRoomMember.getMemberType() == MemberType.CREATOR) {
                                AvRoomDataManager.get().mRoomCreateMember = chatRoomMember;
                            }
                        }
                        AvRoomDataManager.get().mRoomFixedMemberList.addAll(chatRoomMemberList);
                        AvRoomDataManager.get().mRoomAllMemberList.addAll(chatRoomMemberList);
                        Logger.i("进入房间获取固定成员成功,人数:" + chatRoomMemberList.size());
                    }
                });
    }


    public interface AvRoomService {

        /**
         * @param uid
         * @param ticket
         * @param roomUid
         * @return
         */
        @FormUrlEncoded
        @POST("userroom/in")
        Single<ServiceResult<UserInfo>> userRoomIn(@Field("uid") String uid, @Field("ticket") String ticket,
                                                   @Field("roomUid") String roomUid);

        @GET("activity/query")
        Observable<ServiceResult<List<ActionDialogInfo>>> getActionDialog(@Query("uid") String uid, @Query("type") String type);


        @GET("activity/query")
        Observable<ServiceResult<List<ActionDialogInfo>>> getActionDialog(@Query("type") String type);

        /**
         * 通知我们自己服务器房间退出接口
         *
         * @param uId
         * @param ticket
         * @return
         */
        @FormUrlEncoded
        @POST("room/close")
        Observable<ServiceResult<String>> quiteRoomForOurService(@Field("uid") String uId, @Field("ticket") String ticket);

        /**
         * 退出房间
         *
         * @param uId
         * @param ticket
         * @return
         */
        @FormUrlEncoded
        @POST("userroom/out")
        Observable<ServiceResult<String>> quitUserRoom(@Field("uid") String uId, @Field("ticket") String ticket);

        /**
         * 请求服务端信息接口
         *
         * @param uId
         * @return
         */
        @GET("room/get")
        Observable<ServiceResult<RoomInfo>> requestRoomInfoFromService(@Query("uid") String uId);

        /**
         * 开房间
         * uid：必填
         * type：房间类型，1竞拍房，2悬赏房，必填
         * title：房间标题
         * roomDesc：房间描述
         * backPic：房间背景图
         * rewardId：当type为2时，必填如rewardId
         */
        @FormUrlEncoded
        @POST("room/open")
        Observable<ServiceResult<RoomInfo>> openRoom(@Field("uid") long uid,
                                                     @Field("ticket") String ticket,
                                                     @Field("roomPwd") String roomPwd,
                                                     @Field("type") int type,
                                                     @Field("title") String title,
                                                     @Field("roomDesc") String roomDesc,
                                                     @Field("backPic") String backPic,
                                                     @Field("rewardId") String rewardId);


    }
}
