package com.tongdaxing.xchat_core.room.model;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RxNet;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * <p> 房间设置 </p>
 *
 * @author jiahui
 * @date 2017/12/15
 */
public class RoomSettingModel extends BaseMvpModel {
    private final RoomSettingService mRoomSettingService;

    public RoomSettingModel() {
        mRoomSettingService = RxNet.create(RoomSettingService.class);
    }

    public void requestTagAll(int tagType, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("tagType", String.valueOf(tagType));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getNewRoomTagList(), params, myCallBack);
    }

    /**
     * 更新房间设置信息
     *
     * @param title
     * @param desc
     * @param pwd
     * @param label 标签名字
     * @param tagId 标签id
     */
    public void doUpdateRoomInfo(String title, String pwd, String label,
                                 int tagId, long uid, String ticket, String backPic, int giftEffect, int drawMsgOption, ResponseListener listener, ResponseErrorListener errorListener) {
//        return mRoomSettingService.doUpdateRoomInfo(title, desc, pwd, label, tagId, uid, ticket,backPic)
//                .subscribeOn(Schedulers.io())
//                .unsubscribeOn(Schedulers.io())
//                .onErrorResumeNext(this.<RoomInfo>getSingleCommonExceptionFunction())
//                .flatMap(this.<RoomInfo>getSingleFunction())
//                .observeOn(AndroidSchedulers.mainThread());
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("title", title);
        requestParam.put("roomPwd", pwd);
        requestParam.put("roomTag", label);
        requestParam.put("tagId", tagId + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("backPic", backPic);
        requestParam.put("giftEffectSwitch", giftEffect + "");
        requestParam.put("drawMsgOption", drawMsgOption + "");
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.updateRoomInfo(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    /**
     * 更新房间设置信息
     *
     * @param title
     * @param desc
     * @param pwd
     * @param label 标签名字
     * @param tagId 标签id
     */
    public void updateByAdmin(long roomUid, String title, String pwd, String label,
                              int tagId, long uid, String ticket, String backPic, int giftEffect, int drawMsgOption, ResponseListener listener, ResponseErrorListener errorListener) {
//        return mRoomSettingService.updateByAdmin(roomUid, title, desc, pwd, label, tagId, uid, ticket, backPic)
//                .subscribeOn(Schedulers.io())
//                .unsubscribeOn(Schedulers.io())
//                .onErrorResumeNext(this.<RoomInfo>getSingleCommonExceptionFunction())
//                .flatMap(this.<RoomInfo>getSingleFunction())
//                .observeOn(AndroidSchedulers.mainThread());
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("roomUid", roomUid + "");
        requestParam.put("title", title);
        requestParam.put("roomPwd", pwd);
        requestParam.put("roomTag", label);
        requestParam.put("tagId", tagId + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("backPic", backPic);
        requestParam.put("giftEffectSwitch", giftEffect + "");
        requestParam.put("drawMsgOption", drawMsgOption + "");
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.updateByAdimin(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    public interface RoomSettingService {

        /**
         * 请求所有的标签列表数据
         *
         * @param ticket
         * @return
         */
        @FormUrlEncoded
        @POST("room/tag/all")
        Single<ServiceResult<List<TabInfo>>> requestTagAll(@Field("ticket") String ticket);

        /**
         * 更新房间设置信息
         *
         * @param title
         * @param desc
         * @param pwd
         * @param label  标签名字
         * @param tagId  标签id
         * @param uid    用户id
         * @param ticket
         */
        @FormUrlEncoded
        @POST("room/update")
        Single<ServiceResult<RoomInfo>> updateRoomInfo(@Field("title") String title, @Field("roomDesc") String desc,
                                                       @Field("roomPwd") String pwd, @Field("roomTag") String label,
                                                       @Field("tagId") int tagId, @Field("uid") long uid,
                                                       @Field("ticket") String ticket, @Field("backPic") String backPic);

        /**
         * 更新房间设置信息
         *
         * @param title
         * @param desc
         * @param pwd
         * @param label  标签名字
         * @param tagId  标签id
         * @param uid    用户id
         * @param ticket
         */
        @FormUrlEncoded
        @POST("room/updateByAdmin")
        Single<ServiceResult<RoomInfo>> updateByAdmin(@Field("roomUid") long roomUid, @Field("title") String title,
                                                      @Field("roomDesc") String desc, @Field("roomPwd") String pwd,
                                                      @Field("roomTag") String label, @Field("tagId") int tagId,
                                                      @Field("uid") long uid, @Field("ticket") String ticket, @Field("backPic") String backPic);
    }
}
