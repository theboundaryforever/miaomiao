package com.tongdaxing.xchat_core.room;

/**
 * Created by Chen on 2019/4/29.
 */
public class RoomAction {
    public static class OnEnterRoom {
        boolean mIsSuccess;

        public OnEnterRoom(boolean isSuccess) {
            this.mIsSuccess = isSuccess;
        }

        public boolean isSuccess() {
            return mIsSuccess;
        }
    }

    public static class OnUpdateChairView {
    }

    public static class OnChairStateChange {
        boolean mIsSitDown;
        String mPlayerId;

        public OnChairStateChange(boolean state, String playerId) {
            mIsSitDown = state;
            mPlayerId = playerId;
        }

        public boolean isSitDown() {
            return mIsSitDown;
        }

        public String getPlayerId() {
            return mPlayerId;
        }
    }
}
