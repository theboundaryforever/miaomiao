package com.tongdaxing.xchat_core.room.publicchatroom;

import android.text.TextUtils;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.NIMAntiSpamOption;
import com.netease.nimlib.sdk.msg.model.QueryDirectionEnum;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PublicChatRoomAttachment;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM;


/**
 * Created by Administrator on 2018/3/20.
 */

public class PublicChatRoomManager extends AbstractBaseCore {
    public static final int MAX_MSG_SIZE = 100;
    public static final int WAITING_TIME = 10;//成功发送消息后，需要等待一定时间才可以再次发送
    public static long devRoomId = 25501704;//测试公聊
    public static long formalRoomId = 25561748;//线上
    public static long checkDevRoomId = 57746367;//线下审核
    public static long checkRoomId = 57718418;//线上审核
    public static boolean mIsEnterRoom;//是否进入了广场聊天室
    public static boolean mIsListening;//是否正在监听云信消息
    public long cacheTime = 0;
    private String TAG = "PublicChatRoomManager";
    private long roomId = formalRoomId;
    private String cacheNameKey = "cacheNameKey";

    private List<ChatRoomMessage> mChatRoomMessages;
    private CompositeDisposable compositeDisposable;

    public PublicChatRoomManager() {
        setRoomId();
        compositeDisposable = new CompositeDisposable();
        mChatRoomMessages = new ArrayList<>();
    }

    public List<ChatRoomMessage> getmChatRoomMessages() {
        return mChatRoomMessages;
    }

    public void enterRoom(final ActionCallBack actionCallBack) {
        if (mIsEnterRoom) {
            //拿缓存数据
            L.debug(TAG, "mIsEnterRoom = true, ");
            onEnterRoomSuccess(actionCallBack);
            return;
        }
        //适配云信乱发下麦消息的坑
        final EnterChatRoomData data = new EnterChatRoomData(String.valueOf(roomId));
        Map<String, Object> stringIntegerMap = new HashMap<>();
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            stringIntegerMap.put(AvRoomModel.PUBLIC_CHAT_ROOM, AvRoomModel.PUBLIC_CHAT_ROOM);
            stringIntegerMap.put(AvRoomModel.EXPER_LEVEL, userInfo.getExperLevel());
            stringIntegerMap.put(AvRoomModel.EXPER_CHARM, userInfo.getCharmLevel());
            stringIntegerMap.put(AvRoomModel.USER_NICK, userInfo.getNick());
            stringIntegerMap.put(AvRoomModel.USER_CAR, userInfo.getCarUrl());
            stringIntegerMap.put(AvRoomModel.USER_CAR_NAME, userInfo.getCarName());
            stringIntegerMap.put(AvRoomModel.CP_SIGN, (userInfo.getCpUser() == null || userInfo.getSign() == null) ? "" : userInfo.getSign());
            stringIntegerMap.put(AvRoomModel.CP_LEVEL, userInfo.getCpUser() == null ? 0 : userInfo.getCpUser().getCpGiftLevel());
            stringIntegerMap.put(AvRoomModel.CUSTOM_LABEL_URL, userInfo.getCustomSign());
            data.setNotifyExtension(stringIntegerMap);
            data.setExtension(stringIntegerMap);
        }
        NIMClient.getService(ChatRoomService.class).enterChatRoomEx(data, 3).setCallback(new RequestCallback<EnterChatRoomResultData>() {
            @Override
            public void onSuccess(EnterChatRoomResultData result) {
                mIsEnterRoom = true;
                onEnterRoomSuccess(actionCallBack);
            }

            @Override
            public void onFailed(int code) {
                // 登录失败
                actionCallBack.error(code);
                mIsEnterRoom = false;
            }

            @Override
            public void onException(Throwable exception) {
                // 错误
                actionCallBack.error(exception);
                mIsEnterRoom = false;
            }
        });
    }

    private void onEnterRoomSuccess(ActionCallBack actionCallBack) {
        actionCallBack.success();
        // mChatRoomMessages缓存为空时，拿云信数据
        if (ListUtils.isListEmpty(mChatRoomMessages)) {
            L.debug(TAG, "mChatRoomMessages is blank, so get it from NIMClient");
            MsgTypeEnum[] typeEnums = new MsgTypeEnum[]{MsgTypeEnum.custom};
            NIMClient.getService(ChatRoomService.class).pullMessageHistoryExType(roomId + "", System.currentTimeMillis(), MAX_MSG_SIZE,
                    QueryDirectionEnum.QUERY_OLD, typeEnums).setCallback(new RequestCallback<List<ChatRoomMessage>>() {
                @Override
                public void onSuccess(List<ChatRoomMessage> param) {
                    L.info(TAG, "enterChatRoomEx() onSuccess() param.size() = %d", param.size());
                    Collections.reverse(param);
                    for (int i = 0; i < param.size(); i++) {
                        LogUtil.d("房间信息拦截", "sessionId----- " + param.get(i).getSessionId());
                        ChatRoomMessage chatRoomMessage = param.get(i);
                        MsgAttachment attachment = chatRoomMessage.getAttachment();
                        if (attachment instanceof CustomAttachment &&
                                ((CustomAttachment) attachment).getFirst() == CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM) {
                            mChatRoomMessages.add(chatRoomMessage);
                        }
                    }
                    L.debug(TAG, "pullMessageHistoryExType : %d", mChatRoomMessages.size());
                    onGetRoomMessageSuccess();
                    initMsgDisposable();//开始监听
                }

                @Override
                public void onFailed(int code) {
                    L.error(TAG, "enterChatRoomEx() error() code = %d", code);
                    initMsgDisposable();
                }

                @Override
                public void onException(Throwable exception) {
                    L.error(TAG, "enterNeteaseRoom() error() exception: ", exception);
                    initMsgDisposable();
                }
            });
        } else {
            // 缓存不为空时，拿缓存数据
            L.debug(TAG, "mChatRoomMessages is not blank, so get it from cache");
            onGetRoomMessageSuccess();
            initMsgDisposable();//开始监听
        }
    }

    /**
     * 拿到闲聊广场历史消息
     */
    private void onGetRoomMessageSuccess() {
        CoreUtils.send(new ChatHallMsgEvent.OnReceiveNewMsg(mChatRoomMessages));
    }

    public void initMsgDisposable() {
        if (!mIsListening) {
            if (compositeDisposable == null)
                return;
            compositeDisposable.clear();
            Disposable crMsgDisposable = IMNetEaseManager.get().getChatRoomMsgFlowable()
                    .subscribe(new Consumer<List<ChatRoomMessage>>() {
                        @Override
                        public void accept(List<ChatRoomMessage> messages) throws Exception {
                            if (messages.size() == 0) return;
                            List<ChatRoomMessage> temp = new ArrayList<>();
                            for (int i = 0; i < messages.size(); i++) {
                                ChatRoomMessage chatRoomMessage = messages.get(i);
                                if (PublicChatRoomManager.this.checkNeedMsg(chatRoomMessage)) {
                                    temp.add(chatRoomMessage);
                                }
                            }
                            L.debug(TAG, "getChatRoomMsgFlowable onCurrentRoomReceiveNewMsg: messages.size() = %d, temp.size() = %d", messages.size(), temp.size());
                            onCurrentRoomReceiveNewMsg(temp);
                        }
                    });
            compositeDisposable.add(crMsgDisposable);
            L.debug(TAG, "compositeDisposable inited");
            mIsListening = true;
        }
    }

    /**
     * 检查是否是广场需要的消息
     */
    private boolean checkNeedMsg(ChatRoomMessage chatRoomMessage) {
        if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
            CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
            return attachment.getFirst() == CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM;//广场消息
        }
        return false;
    }

    public void onCurrentRoomReceiveNewMsg(ChatRoomMessage message) {
        if (message == null) {
            return;
        }
        addMessage(message);
    }

    public void onCurrentRoomReceiveNewMsg(List<ChatRoomMessage> messages) {
        if (ListUtils.isListEmpty(messages)) {
            return;
        }
        ChatRoomMessage message = messages.get(0);
        if (messages.size() == 1 && message != null) {//单个消息走单个的添加方法
            onCurrentRoomReceiveNewMsg(message);
            return;
        }
        addMessageList(messages);
    }

    private void addMessage(ChatRoomMessage message) {
        mChatRoomMessages.add(message);
        if (mChatRoomMessages.size() > PublicChatRoomManager.MAX_MSG_SIZE) {
            mChatRoomMessages.remove(0);
        }
        //发通知 更新数据
        CoreUtils.send(new ChatHallMsgEvent.OnReceiveNewMsg(message));
    }

    private void addMessageList(List<ChatRoomMessage> messages) {
        mChatRoomMessages.addAll(messages);
        while (mChatRoomMessages.size() > PublicChatRoomManager.MAX_MSG_SIZE) {
            mChatRoomMessages.remove(0);
        }
        //发通知 更新数据
        CoreUtils.send(new ChatHallMsgEvent.OnReceiveNewMsg(messages));
    }

    public void leaveRoom() {
        if (mIsEnterRoom) {
            L.info(TAG, "leaveRoom");
            NIMChatRoomSDK.getChatRoomService().exitChatRoom(roomId + "");
            mIsEnterRoom = false;
            refreshTime();

            //停止消息监听
            if (compositeDisposable != null) {
                compositeDisposable.dispose();
                compositeDisposable = null;
                mIsListening = false;
            }
        }
    }

    public void initCacheTime() {
        cacheTime = (long) SpUtils.get(getContext(), cacheNameKey, 0L);
    }

    public void refreshTime() {
        cacheTime = System.currentTimeMillis();
        SpUtils.put(getContext(), cacheNameKey, cacheTime);
    }

    public void sendMsg(final String content) {
        if (TextUtils.isEmpty(content)) return;

        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo == null) {
            return;
        }

        PublicChatRoomAttachment publicChatRoomAttachment = new PublicChatRoomAttachment(CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM, CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM);
        publicChatRoomAttachment.setMsg(content);
        String avatar = userInfo.getAvatar();
        if (TextUtils.isEmpty(avatar)) {
            avatar = "";
        }
        final int charmLevel = userInfo.getCharmLevel();
        Json json = new Json();
        json.set("avatar", avatar);
        json.set("uid", userInfo.getUid());
        json.set("charmLevel", charmLevel);
        json.set("nick", userInfo.getNick());
        L.info(TAG, "sendMsg, json: %s", json);

        publicChatRoomAttachment.setParams(json + "");
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomId + "",
                // 自定义消息
                publicChatRoomAttachment
        );
        message.setContent(content);

        // 构造反垃圾对象
        NIMAntiSpamOption antiSpamOption = new NIMAntiSpamOption();
        Json jsonContent = new Json();
        jsonContent.set("type", 1);
        jsonContent.set("data", content);
        antiSpamOption.content = jsonContent + "";
        antiSpamOption.enable = true;
        message.setNIMAntiSpamOption(antiSpamOption);

        IMNetEaseManager.get().sendChatRoomMessage(message, false, true)
                .subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
                    @Override
                    public void accept(ChatRoomMessage chatRoomMessage,
                                       Throwable throwable) throws Exception {
                        if (chatRoomMessage != null) {
                            MobclickAgent.onEvent(getContext(), UmengEventId.getFindHallSendSuc());
                            L.debug(TAG, "sendMsg content = %s success", content);
                            addMessage(chatRoomMessage);
                        } else {
                            CoreUtils.send(new ChatHallMsgEvent.OnSendMsgFailure());
                            L.error(TAG, "sendChatRoomMessage chatRoomMessage == null");
                        }
                        if (throwable != null) {
                            CoreUtils.send(new ChatHallMsgEvent.OnSendMsgFailure());
                            L.error(TAG, "sendChatRoomMessage throwable: ", throwable);
                        }
                    }
                });
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId() {
        if (BasicConfig.isDebug) {
            roomId = devRoomId;
        } else {
            roomId = formalRoomId;
        }
    }

    public interface ActionCallBack {
        void success();

        void error(int code);

        void error(Throwable exception);
    }
}
