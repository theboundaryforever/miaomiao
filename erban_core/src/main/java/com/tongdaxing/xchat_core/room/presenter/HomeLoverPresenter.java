package com.tongdaxing.xchat_core.room.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.util.Entry;
import com.orhanobut.logger.Logger;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.BlessingBeastInfo;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.HomePartyModel;
import com.tongdaxing.xchat_core.room.view.IHomePartyView;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.Constants;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class HomeLoverPresenter extends AbstractMvpPresenter<IHomePartyView> {
    private static final String TAG = "HomeLoverPresenter";
    private final HomePartyModel mHomePartyMode;
    /**
     * 判断所坑服务端是否响应回来了
     */
    private boolean mIsLockMicPosResultSuccess = true;
    private boolean mIsUnLockMicPosResultSuccess = true;

    public HomeLoverPresenter() {
        mHomePartyMode = new HomePartyModel();
    }

    /**
     * 麦坑点击处理，麦上没人的时候
     *
     * @param micPosition    麦序位置
     * @param chatRoomMember 坑上的用户
     */
    public void microPhonePositionClick(final int micPosition, ChatRoomMember chatRoomMember) {
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        final String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (AvRoomDataManager.get().isRoomOwner(currentUid)) {
            onOwnerUpMicroClick(micPosition, currentRoom.getUid(), true);
        } else if (AvRoomDataManager.get().isRoomAdmin(currentUid)) {
            onOwnerUpMicroClick(micPosition, currentRoom.getUid(), false);
        } else {
            upMicroPhone(micPosition, currentUid, false);
        }
    }

    private void onOwnerUpMicroClick(final int micPosition, final long currentUid, boolean isOwner) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null) return;
        if (getMvpView() != null)
            getMvpView().showOwnerClickDialog(roomQueueInfo.mRoomMicInfo, micPosition, currentUid, isOwner);
    }

    /**
     * 排麦加入队列
     */
    public void addMicInList() {
        if (AvRoomDataManager.get().isSelfOnMic())
            return;

        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        int index = AvRoomDataManager.get().checkHasEmpteyMic();
        if (index == AvRoomDataManager.MIC_FULL) {
            // : 2018/4/26 3
            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (roomInfo != null && userInfo != null)
                mHomePartyMode.updateQueueEx((int) userInfo.getUid(), roomInfo.getRoomId() + "", null, userInfo, 1);
        } else {
            if (userInfo != null) {
                upMicroPhone(index, userInfo.getUid() + "", false);
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.micInListDismiss, "");
            }
        }
    }


    public void lockMicroPhone(int micPosition, final long currentUid) {
        if (!mIsLockMicPosResultSuccess) {
            return;
        }
        mIsLockMicPosResultSuccess = false;
        ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
            @Override
            public void onResponse(ServiceResult<String> data) {
                if (null != data && data.isSuccess()) {//解麦成功
                    Logger.i("用户%1$s锁坑成功: %2$s", String.valueOf(currentUid), data);
                    mIsLockMicPosResultSuccess = true;
                } else {//解麦失败
                    Logger.i("用户%1$s锁坑失败: %2$s", String.valueOf(currentUid), data.getError());
                    mIsLockMicPosResultSuccess = true;
                }
            }

        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {//解麦失败
                Logger.i("用户%1$s锁坑失败: %2$s", String.valueOf(currentUid), error);
                mIsLockMicPosResultSuccess = true;
            }
        };
        mHomePartyMode.lockMicroPhone(micPosition, String.valueOf(currentUid),
                CoreManager.getCore(IAuthCore.class).getTicket(), listener, errorListener);
    }

    /**
     * 坑位释放锁
     */
    public void unLockMicroPhone(int micPosition) {
        if (!mIsUnLockMicPosResultSuccess) {
            return;
        }
        mIsUnLockMicPosResultSuccess = false;
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        final String currentUid = String.valueOf(currentRoom.getUid());
        if (AvRoomDataManager.get().isRoomAdmin() || AvRoomDataManager.get().isRoomOwner(currentUid)) {
            ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
                @Override
                public void onResponse(ServiceResult<String> data) {
                    if (null != data && data.isSuccess()) {//解麦成功
                        mIsUnLockMicPosResultSuccess = true;
                    } else {//解麦失败
                        mIsUnLockMicPosResultSuccess = true;
                    }
                }

            };
            ResponseErrorListener errorListener = new ResponseErrorListener() {
                @Override
                public void onErrorResponse(RequestError error) {//解麦失败
                    mIsUnLockMicPosResultSuccess = true;
                }
            };
            mHomePartyMode.unLockMicroPhone(micPosition, currentUid, CoreManager.getCore(IAuthCore.class).getTicket(), listener, errorListener);
        }
    }

    /**
     * 下麦
     *
     * @param micPosition
     * @param isKick      是否是主动的
     */
    public void downMicroPhone(int micPosition, final boolean isKick) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        final String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        mHomePartyMode.downMicroPhone(micPosition, new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
                Logger.i("用户%1$s下麦成功：%2$s", currentUid, data);
                if (!isKick) {
                    //被踢了
                    if (getMvpView() != null) {
                        getMvpView().kickDownMicroPhoneSuccess();
                    }
                }
            }

            @Override
            public void onFail(int code, String error) {
                Logger.i("用户%1$s下麦失败：%2$s----", currentUid, error);
            }
        });
    }


    public void upMicroPhone(final int micPosition, final String uId, boolean isInviteUpMic) {
        upMicroPhone(micPosition, uId, isInviteUpMic, false);
    }

    /**
     * 上麦
     *
     * @param micPosition
     * @param uId
     * @param isInviteUpMic 是否是主动的，true：主动
     */
    public void upMicroPhone(final int micPosition, final String uId, boolean isInviteUpMic, boolean needCloseMic) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        mHomePartyMode.upMicroPhone(micPosition, uId, String.valueOf(roomInfo.getRoomId()),
                isInviteUpMic, new CallBack<String>() {
                    @Override
                    public void onSuccess(String data) {
                        if (micPosition == -1) {
                            if (getMvpView() != null) {
                                getMvpView().notifyRefresh();
                            }
                        }
                        Logger.i("用户%1$s上麦成功：%2$s", uId, data);
                    }

                    @Override
                    public void onFail(int code, String error) {
                        Logger.i("用户%1$s上麦失败：%2$s----", uId, error);
                    }
                }, needCloseMic);
    }

    /**
     * 邀请用户上麦
     *
     * @param micUid   要邀请的用户id
     * @param position 坑位
     */
    public void inviteMicroPhone(final long micUid, final int position) {
        if (AvRoomDataManager.get().isOnMic(micUid)) {
            return;
        }
        //如果点击的就是自己,那这里就是自己上麦
        if (AvRoomDataManager.get().isSelf(micUid)) {
            upMicroPhone(position, String.valueOf(micUid), true);
            return;
        }
        mHomePartyMode.inviteMicroPhone(micUid, position)
                .subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
                    @Override
                    public void accept(ChatRoomMessage chatRoomMessage,
                                       Throwable throwable) throws Exception {
                        if (throwable != null) {
                            Logger.i("邀请用户%d上麦失败!!!" + micUid);
                        } else
                            Logger.i("邀请用户%d上麦成功!!!" + micUid);
                    }
                });
    }

    /**
     * 头像点击，人在麦上
     *
     * @param micPosition
     */
    public void avatarClick(int micPosition) {
//        LogUtils.d("avatarClick", "micPosition:" + micPosition);
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) return;
        // 判断在不在麦上
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null) return;
        // 麦上的人员信息,麦上的坑位信息

        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
        RoomMicInfo roomMicInfo = roomQueueInfo.mRoomMicInfo;


        if (chatRoomMember == null && micPosition == -1) {
            chatRoomMember = new ChatRoomMember();
            RoomInfo info = AvRoomDataManager.get().mCurrentRoomInfo;
            chatRoomMember.setNick("房主");
            chatRoomMember.setAvatar("");
            chatRoomMember.setAccount(info.getUid() + "");


        }
        if (chatRoomMember == null || roomMicInfo == null) return;

        String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        boolean isMySelf = Objects.equals(currentUid, chatRoomMember.getAccount());
        boolean isTargetRoomAdmin = AvRoomDataManager.get().isRoomAdmin(chatRoomMember.getAccount());
        boolean isTargetRoomOwner = AvRoomDataManager.get().isRoomOwner(chatRoomMember.getAccount());


        List<ButtonItem> buttonItems = new ArrayList<>();
        if (getMvpView() == null) return;
        SparseArray<ButtonItem> avatarButtonItemMap = getMvpView().getAvatarButtonItemList(micPosition,
                chatRoomMember, currentRoom);
        if (AvRoomDataManager.get().isRoomOwner()) {
            //房主操作
            if (!isMySelf) {
                //点击不是自己
                buttonItems.add(avatarButtonItemMap.get(0));
                buttonItems.add(avatarButtonItemMap.get(4));
                buttonItems.add(avatarButtonItemMap.get(2));
                if (roomMicInfo.isMicMute()) {
                    buttonItems.add(avatarButtonItemMap.get(6));
                } else {
                    buttonItems.add(avatarButtonItemMap.get(1));
                }
                buttonItems.add(avatarButtonItemMap.get(3));

                if (!isTargetRoomAdmin) {
                    buttonItems.add(avatarButtonItemMap.get(7));
                } else {
                    buttonItems.add(avatarButtonItemMap.get(8));
                }
                buttonItems.add(avatarButtonItemMap.get(9));
                if (getMvpView() != null) {
                    getMvpView().showMicAvatarClickDialog(buttonItems);
                }
            } else {
                // 查看资料
                // 下麦旁听
                // 解禁此座位
//                buttonItems.add(avatarButtonItemMap.get(4));
//                buttonItems.add(avatarButtonItemMap.get(5));
//                if (roomMicInfo.isMicMute()) {
//                    buttonItems.add(avatarButtonItemMap.get(6));
//                }
                if (getMvpView() != null) {
                    getMvpView().showOwnerSelfInfo(chatRoomMember);
                }
            }

        } else if (AvRoomDataManager.get().isRoomAdmin()) {
            //管理员操作
            if (isMySelf) {
                //点击自己
                //查看资料
                //下麦旁听
                // 禁麦此座位/解禁此座位
                buttonItems.add(avatarButtonItemMap.get(4));
                buttonItems.add(avatarButtonItemMap.get(5));
                if (roomMicInfo.isMicMute()) {
                    buttonItems.add(avatarButtonItemMap.get(6));
                } else {
                    buttonItems.add(avatarButtonItemMap.get(1));
                }
            } else {


                //送礼物
                buttonItems.add(avatarButtonItemMap.get(0));
                //查看资料
                buttonItems.add(avatarButtonItemMap.get(4));


                if (!isTargetRoomAdmin && !isTargetRoomOwner) {
                    //非房主或管理员
                    //抱他下麦
                    buttonItems.add(avatarButtonItemMap.get(2));
                    //禁麦/解麦操作
                    buttonItems.add(roomMicInfo.isMicMute() ?
                            avatarButtonItemMap.get(6) :
                            avatarButtonItemMap.get(1));
                    //踢出房间
                    buttonItems.add(avatarButtonItemMap.get(3));
                    //加入黑名单
                    buttonItems.add(avatarButtonItemMap.get(9));
                } else {


                    if (!roomMicInfo.isMicMute() && !isTargetRoomOwner) {
                        buttonItems.add(avatarButtonItemMap.get(1));
                    }

//                    if (roomMicInfo.isMicMute() && !isTargetRoomOwner) {
//                        buttonItems.add(avatarButtonItemMap.get(6));
//                    }


                    if (!isTargetRoomOwner)
                        buttonItems.add(avatarButtonItemMap.get(2));
                    // 对于管理员和房主,只有解麦功能
                    if (roomMicInfo.isMicMute())
                        buttonItems.add(avatarButtonItemMap.get(6));
                }
            }
            if (getMvpView() != null) {
                getMvpView().showMicAvatarClickDialog(buttonItems);
            }
        } else {
            //游客操作
            if (isMySelf) {
                //查看资料
                //下麦旁听
                buttonItems.add(avatarButtonItemMap.get(4));
                buttonItems.add(avatarButtonItemMap.get(5));
                if (getMvpView() != null) {
                    getMvpView().showMicAvatarClickDialog(buttonItems);
                }
            } else {
                buttonItems.add(avatarButtonItemMap.get(0));
                buttonItems.add(avatarButtonItemMap.get(4));

//                LogUtils.d("showGiftDialog", 1 + "");
                if (getMvpView() != null) {
//                    LogUtils.d("showGiftDialog", 2 + "");

                    if (micPosition == -1 && roomQueueInfo.mChatRoomMember == null) {
                        getMvpView().showGiftDialog(chatRoomMember);
                    } else {
                        getMvpView().showGiftDialog(chatRoomMember);
                    }

                }
            }
        }
    }


    /**
     * 获取固定成员（创建者,管理员,普通用户,受限用户）
     */
    public void loadNormalMembers() {
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        mHomePartyMode.loadNormalMembers(String.valueOf(currentRoom.getRoomId()), 0, 500,
                new CallBack<List<ChatRoomMember>>() {
                    @Override
                    public void onSuccess(List<ChatRoomMember> data) {
                        if (getMvpView() != null) {
                            getMvpView().resultLoadNormalMembers(data);
                        }
                    }

                    @Override
                    public void onFail(int code, String error) {

                    }
                });
    }

    /**
     * 开麦
     *
     * @param micPosition
     */
    public void openMicroPhone(int micPosition) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }

        ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
            @Override
            public void onResponse(ServiceResult<String> data) {
                if (null != data && data.isSuccess()) {//解麦成功
                    Logger.i("用户%1$s开麦成功: %2$s", String.valueOf(roomInfo.getUid()), data);
                } else {//解麦失败
                    Logger.i("用户%1$s开麦失败: %2$s", String.valueOf(roomInfo.getUid()), data.getError());
                }
            }

        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {//解麦失败
                Logger.i("用户%1$s开麦失败: %2$s", String.valueOf(roomInfo.getUid()), error);
            }
        };
        mHomePartyMode.openMicroPhone(micPosition, roomInfo.getUid(), listener, errorListener);
    }

    /**
     * 闭麦
     *
     * @param micPosition
     */
    public void closeMicroPhone(int micPosition) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        ResponseListener listener = new ResponseListener<ServiceResult<String>>() {
            @Override
            public void onResponse(ServiceResult<String> data) {
                if (null != data && data.isSuccess()) {//解麦成功
                    Logger.i("用户%1$s闭麦成功: %2$s", String.valueOf(roomInfo.getUid()), data);
                } else {//解麦失败
                    Logger.i("用户%1$s闭麦失败: %2$s", String.valueOf(roomInfo.getUid()), data.getError());
                }
            }

        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {//解麦失败
                Logger.i("用户%1$s闭麦失败: %2$s", String.valueOf(roomInfo.getUid()), error);
            }
        };
        mHomePartyMode.closeMicroPhone(micPosition, roomInfo.getUid(), listener, errorListener);
    }

    /***
     * 发送房间消息
     * @param message
     */
    public void sendTextMsg(String message) {
        if (TextUtils.isEmpty(message)) {
            return;
        }

        CoreManager.getCore(IRoomCore.class).sendRoomTextMsg(message);
    }

    public void chatRoomReConnect(final RoomQueueInfo queueInfo) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) return;

        mHomePartyMode.queryRoomMicInfo(String.valueOf(roomInfo.getRoomId()))
                .delay(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Entry<String, String>>>() {
                    @Override
                    public void accept(final List<Entry<String, String>> entries) throws Exception {
                        boolean needUpMic = true;
                        if (!ListUtils.isListEmpty(entries)) {
                            JsonParser jsonParser = new JsonParser();
                            ChatRoomMember chatRoomMember;
                            AvRoomDataManager.get().mMicInListMap.clear();
                            int roomMicInfoPosition = -100;
                            if (queueInfo != null && queueInfo.mRoomMicInfo != null) {
                                roomMicInfoPosition = queueInfo.mRoomMicInfo.getPosition();
                            }
                            for (Entry<String, String> entry : entries) {

                                LogUtils.d("micInListLogFetchQueue", "key:" + entry.key + "   content:" + entry.value);

                                if (entry.key != null && entry.key.length() > 2) {
                                    // : 2018/4/26 01
                                    addInfoToMicInList(entry, jsonParser);

                                    continue;
//                                    AvRoomDataManager.get().addMicInListInfo(entry.key, roomQueueInfo);
                                }
                                //判断是否有别人在麦位上
                                if ((roomMicInfoPosition + "").equals(entry.key)) {
                                    String oldQueueUid = queueInfo.mChatRoomMember.getAccount() + "";
                                    String queueUid = Json.parse(entry.value).str("uid");
                                    if (!oldQueueUid.equals(queueUid)) {
                                        needUpMic = false;
                                    }
                                }


                                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(Integer.parseInt(entry.key));
                                if (roomQueueInfo != null) {
                                    JsonObject valueJsonObj = jsonParser.parse(entry.value).getAsJsonObject();
                                    if (valueJsonObj != null) {
                                        chatRoomMember = new ChatRoomMember();
                                        if (valueJsonObj.has("uid")) {
                                            int uid = valueJsonObj.get("uid").getAsInt();
                                            chatRoomMember.setAccount(String.valueOf(uid));
                                        }
                                        if (valueJsonObj.has("nick")) {
                                            chatRoomMember.setNick(valueJsonObj.get("nick").getAsString());
                                        }
                                        if (valueJsonObj.has("avatar")) {
                                            chatRoomMember.setAvatar(valueJsonObj.get("avatar").getAsString());
                                        }
                                        if (valueJsonObj.has("gender")) {
                                            roomQueueInfo.gender = valueJsonObj.get("gender").getAsInt();
                                        }
                                        if (valueJsonObj.has(SpEvent.headwearUrl)) {

                                            Map<String, Object> stringStringMap = new HashMap<>();
                                            stringStringMap.put(SpEvent.headwearUrl, valueJsonObj.get(SpEvent.headwearUrl).getAsString());
                                            chatRoomMember.setExtension(stringStringMap);
                                        }
                                        roomQueueInfo.mChatRoomMember = chatRoomMember;
                                    }


                                    AvRoomDataManager.get().addRoomQueueInfo(entry.key, roomQueueInfo);


                                }
                            }
                        } else {
                            //麦上都没有人
                            AvRoomDataManager.get().resetMicMembers();
                        }
                        if (getMvpView() != null)
                            getMvpView().chatRoomReConnectView();
                        //之前在麦上
                        if (queueInfo != null && queueInfo.mChatRoomMember != null && queueInfo.mRoomMicInfo != null) {
                            RoomQueueInfo roomQueueInfo = AvRoomDataManager.get()
                                    .getRoomQueueMemberInfoByMicPosition(queueInfo.mRoomMicInfo.getPosition());
                            //麦上没人
                            String account = queueInfo.mChatRoomMember.getAccount();
                            if (needUpMic && roomQueueInfo != null && (roomQueueInfo.mChatRoomMember == null ||
                                    Objects.equals(account, roomQueueInfo.mChatRoomMember.getAccount()))) {


                                roomQueueInfo.mChatRoomMember = null;

                                AvRoomDataManager.get().mIsNeedOpenMic = false;
                                upMicroPhone(queueInfo.mRoomMicInfo.getPosition(), account, true);
//                                downMicroPhone(queueInfo.mRoomMicInfo.getPosition(), true);//卡麦炸房用
//                                new Handler().postDelayed(new Runnable() {//卡麦炸房用
//                                    @Override
//                                    public void run() {
//                                        RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_BROADCASTER);
//                                    }
//                                }, 1000);
                            }
                            if (!needUpMic) {
                                if (AvRoomDataManager.get().isSelf(account)) {
                                    // 更新声网闭麦 ,发送状态信息
                                    RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                                    AvRoomDataManager.get().mIsNeedOpenMic = false;
                                    if (getMvpView() != null)
                                        getMvpView().notifyBottomBtnState();
                                }
                            }
                        }
                        Logger.i("断网重连获取队列信息成功...." + entries);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        Logger.i("断网重连获取队列信息失败....");
                    }
                });
    }

    private void addInfoToMicInList(Entry<String, String> entry, JsonParser jsonParser) {

        JsonObject valueJsonObj = jsonParser.parse(entry.value).getAsJsonObject();


        Set<String> strings = valueJsonObj.keySet();
        if (valueJsonObj == null)
            return;
        Json json = new Json();
        for (String key : strings) {
            json.set(key, valueJsonObj.get(key).getAsString());
        }
        AvRoomDataManager.get().addMicInListInfo(entry.key, json);

    }

    public void removeMicInList() {


        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;

        IMNetEaseManager.get().removeMicInList(userInfo.getUid() + "", roomInfo.getRoomId() + "", new RequestCallback() {
            @Override
            public void onSuccess(Object param) {

            }

            @Override
            public void onFailed(int code) {

            }

            @Override
            public void onException(Throwable exception) {

            }
        });


    }

    public void updataQueueExBySdk(int micPosition, String roomId, String value) {
        mHomePartyMode.updataQueueExBySdk(micPosition, roomId, null, value
                , false);
    }

    /**
     * 麦上用户校验
     */
    public void checkSpeakerException(Context context, long roomId, long roomUid, String exceptionUserId) {
        if (AvRoomDataManager.get().isRoomCheckSpeakerExceptionEnable(roomId)) {//这里控制上报频率
            mHomePartyMode.kickIllegal(context, roomUid, exceptionUserId, new OkHttpManager.MyCallBack<Object>() {
                @Override
                public void onError(Exception e) {

                }

                @Override
                public void onResponse(Object response) {

                }
            });
        }
    }

    public void queryRoomBlessingBeast(String roomUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("roomUid", roomUid);
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        OkHttpManager.getInstance().doGetRequest(UriProvider.queryRoomBlessingBeast(), params, new HttpRequestCallBack<BlessingBeastInfo>() {
            @Override
            public void onFinish() {

            }

            @Override
            public void onSuccess(String message, BlessingBeastInfo response) {
                if (response != null) {
                    getMvpView().triggerBlessingBeast(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {

            }
        });
    }

    /**
     * 刷新麦上用户的cp标识
     */
    public void refreshMicQueueUsersInfo() {
        SparseArray<RoomQueueInfo> sparseArray = AvRoomDataManager.get().mMicQueueMemberMap;
        ArrayList<Long> userIds = new ArrayList<>();
        for (int i = 0; i < sparseArray.size(); i++) {
            ChatRoomMember c = sparseArray.valueAt(i).mChatRoomMember;
            if (c != null) {
                userIds.add(Long.valueOf(c.getAccount()));
            }
        }
        L.debug(TAG, "refreshMicQueueUsersInfo() userIds.size() = %d -> requestUserInfoMapByUidList()", userIds.size());
        if (userIds.size() < 2) {
            return;
        }
        LinkedHashMap<Long, UserInfo> rstMap = new LinkedHashMap<>();
        CoreManager.getCore(IUserCore.class).requestUserInfoMapByUidList(userIds, rstMap);
    }
}
