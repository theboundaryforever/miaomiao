package com.tongdaxing.xchat_core.room;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomNotificationAttachment;
import com.netease.nimlib.sdk.msg.model.NIMAntiSpamOption;
import com.netease.nimlib.sdk.util.api.RequestResult;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.thread.ExecutorCenter;
import com.tcloud.core.thread.WorkRunnable;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.room.IIMRoomCore;
import com.tongdaxing.xchat_core.im.room.IIMRoomCoreClient;
import com.tongdaxing.xchat_core.im.state.IPhoneCallStateClient;
import com.tongdaxing.xchat_core.im.state.PhoneCallStateCoreImpl;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.result.HomeTabResult;
import com.tongdaxing.xchat_core.result.RoomConsumeInfoListResult;
import com.tongdaxing.xchat_core.result.RoomInfoResult;
import com.tongdaxing.xchat_core.result.RoomResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomManager;
import com.tongdaxing.xchat_core.room.talk.ITalkCtrl;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.talk.TalkCtrl;
import com.tongdaxing.xchat_core.room.talk.message.TalkMessage;
import com.tongdaxing.xchat_core.room.weekcp.IWeekManager;
import com.tongdaxing.xchat_core.room.weekcp.WeekManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.utils.Logger;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.agora.rtc.Constants;

import static com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH;

/**
 * Created by zhouxiangfeng on 2017/5/27.
 */

public class RoomCoreImpl extends AbstractBaseCore implements IRoomCore {
    private static final String TAG = "RoomCoreImpl";
    private List<ChatRoomMessage> messages;
    private List<ChatRoomMessage> cacheMessages;
    private List<ActionDialogInfo> dialogInfo;
    private RoomInfo curRoomInfo;
    private boolean modifyMuteState;
    private IWeekManager mWeekManager;
    private ITalkCtrl mTalkCtrl;
    private PublicChatRoomManager publicChatRoomManager;


    private RoomCoreHandler handler = new RoomCoreHandler(this);

    public RoomCoreImpl() {
        CoreManager.addClient(this);
        messages = new ArrayList<>();

        mWeekManager = new WeekManager();
        mTalkCtrl = new TalkCtrl();
        publicChatRoomManager = new PublicChatRoomManager();
    }

    public void setDialogInfo(List<ActionDialogInfo> dialogInfo) {
        this.dialogInfo = dialogInfo;
    }

    public List<ActionDialogInfo> getDialogInfo() {
        return dialogInfo;
    }

    private void sendStatistics() {
        if (curRoomInfo != null) {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            long roomUid = curRoomInfo.getUid();
            String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
            HashMap<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("uid", String.valueOf(uid));
            params.put("roomUid", String.valueOf(roomUid));
            params.put("ticket", ticket);
            ResponseListener listener = new ResponseListener<ServiceResult>() {
                @Override
                public void onResponse(ServiceResult response) {
                    if (response != null) {

                    }
                }
            };

            ResponseErrorListener errorListener = new ResponseErrorListener() {
                @Override
                public void onErrorResponse(RequestError error) {
                    Logger.error(TAG, error.getErrorStr());
                }
            };

            RequestManager.instance()
                    .submitJsonResultQueryRequest(UriProvider.roomStatistics(),
                            CommonParamUtil.getDefaultHeaders(getContext()),
                            params, listener, errorListener,
                            RoomResult.class, Request.Method.POST);
        }
    }

    @Override
    public List<ChatRoomMessage> getMessages() {
        return messages;
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        userRoomOut();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {
//        exitRoom();
    }

    @Override
    public IWeekManager getWeekManager() {
        return mWeekManager;
    }

    @Override
    public PublicChatRoomManager getPublicChatRoomManager() {
        return publicChatRoomManager;
    }

    public void addMessages(ChatRoomMessage msg) {
        boolean needClear = false;

        if (messages == null) {
            messages = new ArrayList<>();
        }

        if (cacheMessages == null) {
            cacheMessages = new ArrayList<>();
        }

        if (messages != null && messages.size() >= 400) {
            messages.removeAll(cacheMessages);
            cacheMessages.clear();
            cacheMessages.addAll(messages);
            needClear = true;
        }

        messages.add(msg);

        if (cacheMessages.size() <= 200) {
            cacheMessages.add(msg);
        }

        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_CURRENT_ROOM_RECEIVE_NEW_MSG, msg, needClear);
    }

    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void onEnterRoom() {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (curRoomInfo.getType() == RoomInfo.ROOMTYPE_LIGHT_CHAT) {
            if (BasicConfig.INSTANCE.isDebuggable()) {
                CoreManager.getCore(IAVRoomCore.class).joinHighQualityChannel(curRoomInfo.getRoomId() + "", (int) uid, true);
            } else {
                CoreManager.getCore(IAVRoomCore.class).joinHighQualityChannel(curRoomInfo.getRoomId() + "", (int) uid, false);
            }
        } else {
            CoreManager.getCore(IAVRoomCore.class).joinChannel(curRoomInfo.getRoomId() + "", (int) uid);
        }
        if (curRoomInfo.getUid() == uid) {
            CoreManager.getCore(IAVRoomCore.class).setRole(Constants.CLIENT_ROLE_BROADCASTER);
        } else {
            CoreManager.getCore(IAVRoomCore.class).setRole(Constants.CLIENT_ROLE_AUDIENCE);
        }
    }

    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void onRoomInfoUpdate(ChatRoomNotificationAttachment attachment) {


        if (curRoomInfo == null) {
            return;
        }

        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(curRoomInfo.getUid()));
        params.put("visitorUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        ResponseListener listener = new ResponseListener<RoomResult>() {
            @Override
            public void onResponse(RoomResult response) {

                if (response != null) {
                    if (response.isSuccess()) {
                        curRoomInfo = response.getData();
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_CURRENT_ROOM_INFO_UPDATE, response.getData());
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRoomInfo(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RoomResult.class, Request.Method.GET);
    }

    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void onEnterRoomFail(int code, String error) {
        LogUtil.i(TAG, "onEnterRoomFail--->code:" + code);
        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_ENTER_FAIL, code, error);
        clearRoomData();
    }

    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void onKickOut(ChatRoomKickOutEvent.ChatRoomKickOutReason reason) {
        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_BE_KICK_OUT, reason);
        CoreManager.getCore(IAVRoomCore.class).leaveChannel();
        clearRoomData();
    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onJoinAVRoom() {
        LogUtil.i(TAG, "onJoinAVRoom--->");
        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_ENTER, curRoomInfo);

        com.tongdaxing.xchat_framework.util.util.LogUtil.d("onJoinAVRoom 背景及内容含低俗");
        String content = "封面、背景及内容含低俗、引导、暴露等都会被屏蔽处理，泄露用户隐私、导流第三方平台、欺诈用户等将被封号处理。";
        ChatRoomMessage message = ChatRoomMessageBuilder.createTipMessage(content);
        message.setContent(content);
        addMessages(message);

//        CoreManager.getCore(IIMRoomCore.class).queryChatRoomMembers(curRoomInfo.getRoomId()+"");

        handler.removeMessages(0);
        handler.sendEmptyMessageDelayed(0, 60000);

        userRoomIn(curRoomInfo.getUid());
//
//        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
//        if (curRoomInfo.getUid() == uid) {
//            CoreManager.getCore(IStatisticsCore.class).onEventStart(getContext(), IStatisticsCore.EVENT_OPENROOM, "开房");
//        }
//        CoreManager.getCore(IStatisticsCore.class).onEventStart(getContext(), IStatisticsCore.EVENT_ENTERROOM, "进入房间");
    }

/*    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        if (curRoomInfo != null) {
            exitRoom();
            handler.removeMessages(0);
        }
    }*/

    @CoreEvent(coreClientClass = IPhoneCallStateClient.class)
    public void onPhoneStateChanged(PhoneCallStateCoreImpl.PhoneCallStateEnum phoneCallStateEnum) {
        if (curRoomInfo != null) {
            if (phoneCallStateEnum != PhoneCallStateCoreImpl.PhoneCallStateEnum.IDLE) {
                boolean isAudience = CoreManager.getCore(IAVRoomCore.class).isAudienceRole();
                boolean isMute = CoreManager.getCore(IAVRoomCore.class).isMute();
                if (!isAudience && !isMute) {
                    CoreManager.getCore(IAVRoomCore.class).setMute(true);
                    modifyMuteState = true;
                } else {
                    modifyMuteState = false;
                }
            } else {
                if (modifyMuteState == true) {
                    CoreManager.getCore(IAVRoomCore.class).setMute(false);
                }
//                CoreManager.getCore(IAVRoomCore.class).setSpeeker(true);
                modifyMuteState = false;
            }
        } else {
            modifyMuteState = false;
        }
    }

    private void clearRoomData() {
        LogUtil.i(TAG, "clearRoomData");
        messages.clear();
        curRoomInfo = null;
        if (dialogInfo != null) {
            dialogInfo.clear();
            dialogInfo = null;
        }
    }

    @Override
    public boolean isRoomOwner() {
        if (null != curRoomInfo) {
            return curRoomInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid();
        } else {
            return false;
        }
    }

    @Override
    public RoomInfo getCurRoomInfo() {
        return curRoomInfo;
    }

    @Override
    public void updateByAdmin(long roomUid, String title, String desc, String pwd, String label, int tagId) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("roomUid", String.valueOf(roomUid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("tagId", String.valueOf(tagId));

        if (title != null) {
            params.put("title", title);
            if (title.equals("")) {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
                if (userInfo != null) {
                    params.put("title", userInfo.getNick() + "的房间");
                }
            }
        }

        if (desc != null) {
            params.put("roomDesc", desc);
        }
        if (pwd != null) {
            params.put("roomPwd", pwd);
        }
        if (!StringUtil.isEmpty(label)) {
            params.put("roomTag", label);
        }

        ResponseListener listener = new ResponseListener<RoomResult>() {
            @Override
            public void onResponse(RoomResult response) {
                if (response.isSuccess()) {
                    notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_UPDATE_ROOM_INFO, response.getData());
                } else {
                    notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_UPDATE_ROOM_INFO_FAIL, response.getMessage());
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_UPDATE_ROOM_INFO_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.updateByAdimin(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RoomResult.class, Request.Method.POST);
    }

    @Override
    public void updateRoomInfo(String title, String desc, String pwd, String label, int tagId) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("tagId", String.valueOf(tagId));

        if (title != null) {
            params.put("title", title);
            if (title.equals("")) {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
                if (userInfo != null) {
                    params.put("title", userInfo.getNick() + "的房间");
                }
            }
        }

        if (desc != null) {
            params.put("roomDesc", desc);
        }
        if (pwd != null) {
            params.put("roomPwd", pwd);
        }
        if (!StringUtil.isEmpty(label)) {
            params.put("roomTag", label);
        }

        ResponseListener listener = new ResponseListener<RoomResult>() {
            @Override
            public void onResponse(RoomResult response) {
                if (response.isSuccess()) {
                    notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_UPDATE_ROOM_INFO, response.getData());
                } else {
                    notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_UPDATE_ROOM_INFO_FAIL, response.getMessage());
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_UPDATE_ROOM_INFO_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.updateRoomInfo(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RoomResult.class, Request.Method.POST);
    }

    @Override
    public void sendRoomTextMsg(final String str) {
        ExecutorCenter.getInstance().post(new WorkRunnable() {
            @NonNull
            @Override
            public String getTag() {
                return "sendRoomTextMsg";
            }

            @Override
            public void run() {
                RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (roomInfo != null && !StringUtils.isEmpty(str)) {
                    final ChatRoomMessage chatRoomMessage = ChatRoomMessageBuilder.createChatRoomTextMessage(roomInfo.getRoomId() + "",str);
                    // 构造反垃圾对象
                    NIMAntiSpamOption antiSpamOption = chatRoomMessage.getNIMAntiSpamOption();
                    if (antiSpamOption == null) {
                        antiSpamOption = new NIMAntiSpamOption();
                    }
                    antiSpamOption.enable = false;
                    chatRoomMessage.setNIMAntiSpamOption(antiSpamOption);

                    RequestResult<Void> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                            .sendMessage(chatRoomMessage, false));
                    int code = result.code;
                    L.info(TAG, "sendRoomTextMsg str: %s, code: %d, threadName: %s", str, result.code, Thread.currentThread().getName());
                    if (result.exception != null) {
                        CoreUtils.send(new RoomTalkEvent.OnRoomSendMessageFailed(result.exception));
                    } else if (code != BaseMvpModel.RESULT_OK) {
                        CoreUtils.send(new RoomTalkEvent.OnRoomSendMessageFailed(new Exception("错误码: " + code)));
                    } else {
                        TalkMessage message = new TalkMessage();
                        message.setChatRoomMessage(chatRoomMessage);
                        CoreUtils.send(new RoomTalkEvent.OnTalkMessageAccept<TalkMessage>(message));
                    }
                }
            }
        });
    }


    @Override
    public void enterRoom(RoomInfo roomInfo) {
        if (roomInfo == null) {
            return;
        }

        if (curRoomInfo != null) {
            if (curRoomInfo.getUid() == roomInfo.getUid()) {
                return;
            }
//            exitRoom();
        }
        this.curRoomInfo = roomInfo;
        CoreManager.getCore(IIMRoomCore.class).enterRoom(roomInfo.getRoomId() + "");
    }

    @Override
    public void openRoom(final long uid, final int type, final String title, final String roomDesc,
                         final String backPic, final String rewardId) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("tagType", String.valueOf(type));
        params.put("roomPwd", "");
//        params.put("roomTag", "聊天");

        if (!StringUtil.isEmpty(title)) {
            params.put("title", title);
        } else {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                params.put("title", userInfo.getNick() + "的房间");
            }
        }
        if (!StringUtil.isEmpty(roomDesc)) {
            params.put("roomDesc", roomDesc);
        } else {
            params.put("roomDesc", "");
        }
        if (!StringUtil.isEmpty(backPic)) {
            params.put("backPic", backPic);
        } else {
            params.put("backPic", "");
        }

        ResponseListener listener = new ResponseListener<RoomResult>() {
            @Override
            public void onResponse(RoomResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_OPEN_ROOM, response.getData());
                    } else {
                        if (response.getCode() == 1500) {
                            notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_ALREADY_OPENED_ROOM);
                        } else if (response.getCode() == RESULT_FAILED_NEED_REAL_NAME_AUTH) {
                            notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_OPENED_ROOM_AUTH_FAIL);
                        } else {
                            notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_OPEN_ROOM_FAIL, response.getMessage());
                        }
                    }
                } else {
                    notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_OPEN_ROOM_FAIL, "网络异常，请稍后");

                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_OPEN_ROOM_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.openRoom(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RoomResult.class, Request.Method.POST);
    }

    @Override
    public void requestRoomInfo(long uid, final int pageType) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("visitorUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        ResponseListener listener = new ResponseListener<RoomResult>() {
            @Override
            public void onResponse(RoomResult response) {

                if (response != null) {
                    LogUtil.i(TAG, "onJoinAVRoom--->response:" + response.getCode());
                    if (response.isSuccess()) {
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_ROOM_INFO, response.getData(), pageType);
                    } else {
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_ROOM_INFO_FAIL, response.getCode(), response.getMessage(), pageType);
                    }
                } else {
                    notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_ROOM_INFO_FAIL, response.getMessage(), pageType);
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                LogUtil.i(TAG, "onJoinAVRoom--->response:" + error.getErrorStr());
                notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_ROOM_INFO_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRoomInfo(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RoomResult.class, Request.Method.GET);
    }

    @Override
    public void closeRoomInfo(String uid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_CLOSE_ROOM_INFO);
                    } else {
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_CLOSE_ROOM_INFO_FAIL, response.getMessage());
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_CLOSE_ROOM_INFO_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.closeRoom(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    @Override
    public void getRoomConsumeList(long roomUid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(roomUid));
        ResponseListener listener = new ResponseListener<RoomConsumeInfoListResult>() {
            @Override
            public void onResponse(RoomConsumeInfoListResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_ROOM_CONSUME_LIST, response.getData());
                    } else {
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_ROOM_CONSUME_LIST_FAIL, response.getMessage());
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_ROOM_CONSUME_LIST_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRoomConsumeList(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RoomConsumeInfoListResult.class, Request.Method.GET);
    }

    @Override
    public void roomSearch(String key) {
        if (!StringUtil.isEmpty(key)) {
            HashMap<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("key", key);

            ResponseListener listener = new ResponseListener<HomeTabResult>() {
                @Override
                public void onResponse(HomeTabResult response) {
                    if (null != response) {
                        if (response.isSuccess()) {
                            notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_SEARCH_ROOM, response.getData());
                        } else {
                            notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_SEARCH_ROOM_FAIL, response.getMessage());
                        }
                    }
                }
            };

            ResponseErrorListener errorListener = new ResponseErrorListener() {
                @Override
                public void onErrorResponse(RequestError error) {
                    Logger.error(TAG, error.getErrorStr());
                    notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_SEARCH_ROOM_FAIL, error.getErrorStr());
                }
            };

            RequestManager.instance()
                    .submitJsonResultQueryRequest(UriProvider.roomSearch(),
                            CommonParamUtil.getDefaultHeaders(getContext()),
                            params, listener, errorListener,
                            HomeTabResult.class, Request.Method.GET);
        }
    }

    @Override
    public void getUserRoom(long uid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        ResponseListener listener = new ResponseListener<RoomInfoResult>() {
            @Override
            public void onResponse(RoomInfoResult response) {
                if (response.isSuccess()) {
                    notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_USER_ROOM, response.getData());
                    if (response.getData() != null) {
                        CoreUtils.send(new NewRoomEvent.OnCheckEnterRoomStatus(true, response.getData().getUid(), ""));
                    } else {
                        CoreUtils.send(new NewRoomEvent.OnCheckEnterRoomStatus(false, 0, "对方不在房间内"));
                    }
                } else {
                    notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_USER_ROOM_FAIL, response.getMessage());
                    CoreUtils.send(new NewRoomEvent.OnCheckEnterRoomStatus(false, 0, response.getMessage()));
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_GET_USER_ROOM_FAIL, error.getErrorStr());
                CoreUtils.send(new NewRoomEvent.OnCheckEnterRoomStatus(false, 0, error.getErrorStr()));
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getUserRoom(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RoomInfoResult.class, Request.Method.GET);
    }

    @Override
    public void userRoomIn(long roomUid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomUid", String.valueOf(roomUid));
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_USER_ROOM_IN);
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.userRoomIn(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    @Override
    public void userRoomOut() {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_USER_ROOM_OUT);
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.userRoomOut(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    static class RoomCoreHandler extends Handler {
        WeakReference<RoomCoreImpl> roomCoreImpl;

        public RoomCoreHandler(RoomCoreImpl roomCoreImpl) {
            this.roomCoreImpl = new WeakReference<>(roomCoreImpl);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (roomCoreImpl == null || roomCoreImpl.get() == null)
                return;
            roomCoreImpl.get().sendStatistics();
            sendEmptyMessageDelayed(0, 60000);
        }
    }

    @Override
    public ITalkCtrl getTalkCtrl() {
        return mTalkCtrl;
    }
}
