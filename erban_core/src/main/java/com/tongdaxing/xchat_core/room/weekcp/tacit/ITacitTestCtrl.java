package com.tongdaxing.xchat_core.room.weekcp.tacit;

import android.util.LongSparseArray;

/**
 * Created by Chen on 2019/5/9.
 */
public interface ITacitTestCtrl {
    int TACIT_TEST_UN_ASK = 0;      // 没有回答
    int TACIT_TEST_UNFINISHED = 1;    // 回答了，没有完成，退出了
    int TACIT_TEST_ANSWER_AWAIT = 2;    // 回答完了，等待对方
    int TACIT_TEST_FINISHED = 3;        // 回答完了，显示默契指数
    int TACIT_TEST_PAST_DUE = 4;        // 过期了
    int TACIT_TEST_CANCEL = 5;        // 对方或者己方取消

    int TACIT_TEST_TIME = 15;   // 15秒一道题

    // 应答默契考验
    void doAnswerTacit(String cpId);

    // 发起默契考验
    void doAskTacit(String cpId);

    // 默契考验提交
    void doTacitAnswerCollect(String cpId, String answer);

    LongSparseArray<TacitTextInfo> getRoomTalkTacitMsgs();
}
