package com.tongdaxing.xchat_core.room.bean;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


/**
 * @author zhouxiangfeng
 * @date 2017/5/24
 */

public class RoomInfo implements Parcelable {

    @Deprecated
    public static final int ROOMTYPE_AUCTION = 1;
    @Deprecated
    public static final int ROOMTYPE_LIGHT_CHAT = 2;
    public static final int ROOMTYPE_HOME_PARTY = 3;//(娱乐)默认
    public static final int ROOMTYPE_PERSONAL = 4;//个人
    public static final int ROOMTYPE_LOVERS = 5;//情侣
    public static final int ROOMTYPE_GAME = 6;//游戏

    public int getTagType() {
        return tagType;
    }

    public void setTagType(int tagType) {
        this.tagType = tagType;
    }

    private long uid;//咋们系统的房间id，房主ID
    private int tagType;//新的房间类型
    /**
     * 官方账号与非官方账号
     */
    private int officeUser;

    private long roomId;//房间ID

    public String title;//房间名称

    private int type;
    public String tagPict;//图片标签的url
    private String roomNotice;//房间话题的内容
    private String backPic;
    private String backPicUrl;
    private int factor;
    private String roomDesc;//房间话题的主题（标题）
    //小礼物特效,0表示有特效，1表示无特效
    private int giftEffectSwitch;
    //公屏开关
    private int publicChatSwitch;
    //音效质量等级 0为AUDIO_PROFILE_DEFAULT、1为AUDIO_PROFILE_MUSIC_STANDARD_STEREO、2为AUDIO_PROFILE_MUSIC_HIGH_QUALITY、3为AUDIO_PROFILE_MUSIC_HIGH_QUALITY_STEREO
    private int audioLevel;

    //是否开启炸房检测
    private boolean alarmEnable;
    //炸房检测判断的延时时间（毫秒）
    private int timeInterval;

    /**
     * 房间是否开启，是否正在直播
     */
    private boolean valid;

    /**
     * 1:房主在房间，2 :房主不在房间
     */
    private int operatorStatus;
    private String meetingName;

    private int isPermitRoom;

    /**
     * 魅力值开关--是否允许使用这个功能
     **/
    private int charmOpen;

    public String roomPwd;
    //房间玩法
    private String playInfo;//进房提示
    public int tagId;
    private String roomTag;//比如“一周CP”
    /**
     * 房间在线人数
     */
    public int onlineNum;

    /**
     * 房间内快捷文案
     */
    private List<String> fastReply;

    private int[] hideFace;


    /**
     * 是使用即构，还是声网  0.声网 1.即构
     */
    private int audioChannel = 0;

    /**
     * 砸蛋消息开关 0 关 1 开
     * 个播、情侣房、交友房，默认不展示砸蛋消息
     * 若某用户在不展示砸蛋消息的房间内砸蛋时，此时触发中奖消息，则仅自己可见
     */
    private int drawMsgOption;

    protected RoomInfo(Parcel in) {
        this.uid = in.readLong();
        this.tagType = in.readInt();
        this.officeUser = in.readInt();
        this.roomId = in.readLong();
        this.title = in.readString();
        this.type = in.readInt();
        this.roomNotice = in.readString();
        this.roomDesc = in.readString();
        this.backPic = in.readString();
        this.backPicUrl = in.readString();
        this.factor = in.readInt();
        this.playInfo = in.readString();
        this.giftEffectSwitch = in.readInt();
        this.publicChatSwitch = in.readInt();
        this.audioLevel = in.readInt();
        this.alarmEnable = in.readByte() != 0;
        this.timeInterval = in.readInt();
        this.valid = in.readByte() != 0;
        this.operatorStatus = in.readInt();
        this.meetingName = in.readString();
        this.isPermitRoom = in.readInt();
        this.charmOpen = in.readInt();
        this.roomPwd = in.readString();
        this.roomTag = in.readString();
        this.tagId = in.readInt();
        this.tagPict = in.readString();
        this.onlineNum = in.readInt();
        if (this.fastReply == null) {
            this.fastReply = new ArrayList<>();
        }
        in.readStringList(this.fastReply);
        this.audioChannel = in.readInt();
        int length = in.readInt();
        if (length > 0) {
            this.hideFace = new int[length];
            in.readIntArray(hideFace);
        }
        this.drawMsgOption = in.readInt();
    }

    public RoomInfo(int type) {
        this.type = type;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getOfficeUser() {
        return officeUser;
    }

    public void setOfficeUser(int officeUser) {
        this.officeUser = officeUser;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public String getBackPic() {
        return backPic;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

//    public long getOpenTime() {
//        return openTime;
//    }
//
//    public void setOpenTime(long openTime) {
//        this.openTime = openTime;
//    }


    public int[] getHideFace() {
        return hideFace;
    }

    public void setHideFace(int[] hideFace) {
        this.hideFace = hideFace;
    }

    public String getRoomNotice() {
        return roomNotice;
    }

    public void setRoomNotice(String roomNotice) {
        this.roomNotice = roomNotice;
    }

    public String getRoomPwd() {
        return roomPwd;
    }

    public void setRoomPwd(String roomPwd) {
        this.roomPwd = roomPwd;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public String getPlayInfo() {
        return playInfo;
    }

    public void setPlayInfo(String playInfo) {
        this.playInfo = playInfo;
    }

    public int getGiftEffectSwitch() {
        return giftEffectSwitch;
    }

    public void setGiftEffectSwitch(int giftEffectSwitch) {
        this.giftEffectSwitch = giftEffectSwitch;
    }

    public int getPublicChatSwitch() {
        return publicChatSwitch;
    }

    public void setPublicChatSwitch(int publicChatSwitch) {
        this.publicChatSwitch = publicChatSwitch;
    }

    public String getBackPicUrl() {
        return backPicUrl;
    }

    public void setBackPicUrl(String backPicUrl) {
        this.backPicUrl = backPicUrl;
    }

    public int getAudioLevel() {
        return audioLevel;
    }

    public void setAudioLevel(int audioLevel) {
        this.audioLevel = audioLevel;
    }

    public int getAudioChannel() {
        return audioChannel;
    }

    public void setAudioChannel(int audioChannel) {
        this.audioChannel = audioChannel;
    }

    public int getDrawMsgOption() {
        return drawMsgOption;
    }

    public void setDrawMsgOption(int drawMsgOption) {
        this.drawMsgOption = drawMsgOption;
    }

    @Override
    public String toString() {
        return "RoomInfo{" +
                "uid=" + uid +
                ", tagType=" + tagType +
                ", officeUser=" + officeUser +
                ", roomId=" + roomId +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", roomNotice='" + roomNotice + '\'' +
                ", roomDesc='" + roomDesc + '\'' +
                ", backPic='" + backPic + '\'' +
                ", backPicUrl='" + backPicUrl + '\'' +
                ", factor=" + factor +
                ", playInfo='" + playInfo + '\'' +
                ", giftEffectSwitch=" + giftEffectSwitch +
                ", publicChatSwitch=" + publicChatSwitch +
                ", audioLevel=" + audioLevel +
                ", alarmEnable=" + alarmEnable +
                ", timeInterval=" + timeInterval +
                ", valid=" + valid +
                ", operatorStatus=" + operatorStatus +
                ", meetingName='" + meetingName + '\'' +
                ", isPermitRoom=" + isPermitRoom +
                ", charmOpen=" + charmOpen +
                ", roomPwd='" + roomPwd + '\'' +
                ", roomTag='" + roomTag + '\'' +
                ", tagId=" + tagId +
                ", tagPict='" + tagPict + '\'' +
                ", onlineNum=" + onlineNum +
                ", fastReply=" + fastReply +
                ", audioChannel=" + audioChannel +
                ", hideFace=" + hideFace +
                ", drawMsgOption=" + drawMsgOption +
                '}';
    }

    public int getCharmOpen() {
        return charmOpen;
    }

    public void setCharmOpen(int charmOpen) {
        this.charmOpen = charmOpen;
    }

    public int getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    public int getIsPermitRoom() {
        return isPermitRoom;

    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public boolean isAlarmEnable() {
        return alarmEnable;
    }

    public void setAlarmEnable(boolean alarmEnable) {
        this.alarmEnable = alarmEnable;
    }

    public List<String> getFastReply() {
        return fastReply;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void setFastReply(List<String> fastReply) {
        this.fastReply = fastReply;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.uid);
        dest.writeInt(this.tagType);
        dest.writeInt(this.officeUser);
        dest.writeLong(this.roomId);
        dest.writeString(this.title);
        dest.writeInt(this.type);
        dest.writeString(this.roomNotice);
        dest.writeString(this.roomDesc);
        dest.writeString(this.backPic);
        dest.writeString(this.backPicUrl);
        dest.writeInt(this.factor);
        dest.writeString(this.playInfo);
        dest.writeInt(this.giftEffectSwitch);
        dest.writeInt(this.publicChatSwitch);
        dest.writeInt(this.audioLevel);
        dest.writeByte(this.alarmEnable ? (byte) 1 : (byte) 0);
        dest.writeInt(this.timeInterval);
        dest.writeByte(this.valid ? (byte) 1 : (byte) 0);
        dest.writeInt(this.operatorStatus);
        dest.writeString(this.meetingName);
        dest.writeInt(this.isPermitRoom);
        dest.writeInt(this.charmOpen);
        dest.writeString(this.roomPwd);
        dest.writeString(this.roomTag);
        dest.writeInt(this.tagId);
        dest.writeString(this.tagPict);
        dest.writeInt(this.onlineNum);
        if (this.fastReply == null) {
            this.fastReply = new ArrayList<>();
        }
        dest.writeStringList(this.fastReply);
        dest.writeInt(this.audioChannel);
        if (this.hideFace == null) {
            dest.writeInt(0);
        } else {
            dest.writeInt(this.hideFace.length);
            dest.writeIntArray(this.hideFace);
        }
        dest.writeInt(this.drawMsgOption);
    }

    public static final Creator<RoomInfo> CREATOR = new Creator<RoomInfo>() {
        @Override
        public RoomInfo createFromParcel(Parcel source) {
            return new RoomInfo(source);
        }

        @Override
        public RoomInfo[] newArray(int size) {
            return new RoomInfo[size];
        }
    };
}
