package com.tongdaxing.xchat_core.room.charm;

import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

import org.json.JSONObject;

public interface ICharmClient extends ICoreClient {

    String METHOD_ON_REQUEST_CHARM_SUCCESS = "onRequestCharmSuccess";

    String METHOD_ON_REQUEST_CHARM_FAIL = "onRequestCharmFail";

    void onRequestCharmSuccess(JSONObject charmJson);

    void onRequestCharmFail(String message);
}
