package com.tongdaxing.xchat_core.room.model;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.MemberOption;
import com.netease.nimlib.sdk.util.Entry;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.exception.ErrorThrowable;
import com.tongdaxing.erban.libcommon.net.rxnet.RxNet;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.CoreContact;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * <p>房间相关操作model </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class RoomBaseModel extends BaseMvpModel {
    /**
     * 一页房间人数
     */
    protected static final int ROOM_MEMBER_SIZE = 50;
    private static final String TAG = "RoomBaseModel";


    /**
     * 获取固定成员列表(操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 请求数量
     */
    public Single<List<ChatRoomMember>> queryNormalList(int limit, long time) {
        return fetchRoomMembers(MemberQueryType.NORMAL, time, limit);
    }

    /**
     * 获取最新固定在线人数(操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 请求数量
     */
    public Single<List<ChatRoomMember>> queryOnlineList(int limit) {
        return fetchRoomMembers(MemberQueryType.ONLINE_NORMAL, 0, limit);
    }


    /**
     * 获取最新在线游客列表 (操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 请求数量
     */
    public Single<List<ChatRoomMember>> queryGuestList(int limit) {
        return queryGuestList(limit, 0);
    }

    /**
     * 获取在线游客列表 (操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 获取数量大小
     * @param time  从当前时间开始查找
     */
    public Single<List<ChatRoomMember>> queryGuestList(int limit, long time) {
        return fetchRoomMembers(MemberQueryType.GUEST, time, limit);
    }

    private Single<List<ChatRoomMember>> fetchRoomMembers(final MemberQueryType memberType,
                                                          final long time, final int limit) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null)
            return Single.error(new ErrorThrowable(ErrorThrowable.ROOM_INFO_NULL_ERROR));
        return Single.create(
                new SingleOnSubscribe<List<ChatRoomMember>>() {
                    @Override
                    public void subscribe(SingleEmitter<List<ChatRoomMember>> e) throws Exception {
                        executeNIMClient(NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                                .fetchRoomMembers(String.valueOf(roomInfo.getRoomId()), memberType, time, limit)), e);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());
    }

    /**
     * 获取管理员列表 (操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit -
     * @return --
     */
    public Single<List<ChatRoomMember>> queryManagerList(int limit) {
        return queryNormalList(limit, 0)
                .map(new Function<List<ChatRoomMember>, List<ChatRoomMember>>() {
                    @Override
                    public List<ChatRoomMember> apply(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                        List<ChatRoomMember> managerList = null;
                        if (!ListUtils.isListEmpty(chatRoomMemberList)) {
                            managerList = new ArrayList<>();
                            for (ChatRoomMember chatRoomMember : chatRoomMemberList) {
                                if (chatRoomMember.getMemberType() == MemberType.ADMIN) {
                                    managerList.add(chatRoomMember);
                                }
                            }
                        }
                        return managerList;
                    }
                });
    }


    /**
     * 获取最新黑名单列表 (操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 数量
     * @return
     */
    public Single<List<ChatRoomMember>> queryBlackList(int limit) {
        return queryNormalList(limit, 0)
                .map(new Function<List<ChatRoomMember>, List<ChatRoomMember>>() {
                    @Override
                    public List<ChatRoomMember> apply(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                        List<ChatRoomMember> blackList = null;
                        if (!ListUtils.isListEmpty(chatRoomMemberList)) {
                            blackList = new ArrayList<>();
                            for (ChatRoomMember chatRoomMember : chatRoomMemberList) {
                                if (chatRoomMember.isInBlackList()) {
                                    blackList.add(chatRoomMember);
                                }
                            }
                        }
                        return blackList;
                    }
                });
    }


    /**
     * 设置管理员
     *
     * @param roomId
     * @param account
     * @param mark    true:设置管理员 ,false：移除管理员
     */
    public void markManagerList(long roomId, String account, boolean mark, CallBack<ChatRoomMember> callBack) {
        IMNetEaseManager.get().markManagerListBySdk(String.valueOf(roomId), account, mark, callBack);
    }

    /**
     * 拉黑房间成员
     *
     * @param roomId
     * @param account
     * @param mark     true:拉黑 ,false：取消拉黑
     * @param callBack
     */
    public void markBlackList(long roomId, String account, boolean mark, CallBack<ChatRoomMember> callBack) {
        IMNetEaseManager.get().markBlackListBySdk(String.valueOf(roomId), account, mark, callBack);
    }

    public Single<ChatRoomMember> markMemmberBlack(final String account, final boolean mark) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null)
            return Single.error(new ErrorThrowable(ErrorThrowable.ROOM_INFO_NULL_ERROR));
        return Single.create(
                new SingleOnSubscribe<ChatRoomMember>() {
                    @Override
                    public void subscribe(SingleEmitter<ChatRoomMember> e) throws Exception {
                        executeNIMClient(NIMClient.syncRequest(NIMClient.getService(ChatRoomService.class)
                                .markChatRoomBlackList(mark,
                                        new MemberOption(String.valueOf(roomInfo), account))), e);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());

    }

    private Single<RoomInfo> checkRoomInfoNull() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null)
            return Single.error(new ErrorThrowable(ErrorThrowable.ROOM_INFO_NULL_ERROR));
        return Single.just(roomInfo);
    }


    /**
     * 邀请上麦
     *
     * @param micUid   上麦用户uid
     * @param position
     * @return Single<ChatRoomMessage>
     */
    public Single<ChatRoomMessage> inviteMicroPhone(long micUid, int position) {
        return IMNetEaseManager.get().inviteMicroPhoneBySdk(micUid, position);
    }


    public void upMicroPhone(final int micPosition, final String uId, final String roomId,
                             boolean isInviteUpMic, final CallBack<String> callBack) {
        upMicroPhone(micPosition, uId, roomId, isInviteUpMic, callBack, false);
    }


    /**
     * 上麦
     *
     * @param micPosition
     * @param uId           要上麦的用户id
     * @param roomId
     * @param isInviteUpMic 是否是主动的
     * @param callBack
     */
    public void upMicroPhone(final int micPosition, final String uId, final String roomId,
                             boolean isInviteUpMic, final CallBack<String> callBack, boolean needCloseMic) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(micPosition);
        if (roomQueueInfo == null) {
            return;
        }
        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;//为空表示坑上没人
        final RoomMicInfo roomMicInfo = roomQueueInfo.mRoomMicInfo;

        final UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        //房主只能上-1麦位,其他只能上非-1麦位
        if (AvRoomDataManager.get().isRoomOwner(uId)) {
            if (userInfo != null && micPosition == -1) {
                updateQueueEx(micPosition, roomId, callBack, userInfo);
            }
        } else if (roomMicInfo != null && micPosition != -1 &&
                ((!roomMicInfo.isMicLock() || AvRoomDataManager.get().isRoomOwner(uId) || AvRoomDataManager.get().isRoomAdmin(uId)) || isInviteUpMic) && chatRoomMember == null) {//坑上没人且没锁
            if (userInfo != null) {
                //先看下这个用户是否在麦上
                if (AvRoomDataManager.get().isOnMic(userInfo.getUid())) {
                    int position = AvRoomDataManager.get().getMicPosition(userInfo.getUid());
                    if (position == -1)
                        return;
                    //先下麦，再上麦
                    downMicroPhone(position, new CallBack<String>() {
                        @Override
                        public void onSuccess(String data) {
                            updateQueueEx(micPosition, roomId, callBack, userInfo);
                        }

                        @Override
                        public void onFail(int code, String error) {
                            if (callBack != null) {
                                callBack.onFail(-1, "上麦导致下麦失败");
                            }
                        }
                    });
                } else {
                    updateQueueEx(micPosition, roomId, callBack, userInfo);
                }
            }
        }
    }

    public void downMicroPhone(int micPosition, final CallBack<String> callBack) {
        IMNetEaseManager.get().downMicroPhoneBySdk(micPosition, callBack);

    }

    public void updateQueueEx(int micPosition, String roomId, final CallBack<String> callBack, UserInfo userInfo) {
        updateQueueEx(micPosition, roomId, callBack, userInfo, -1);
    }


    public void updateQueueEx(int micPosition, String roomId, final CallBack<String> callBack, UserInfo userInfo, int type) {
        if (userInfo == null) {
            return;
        }
        JSONObject contentJsonObj = new JSONObject();
        //大于0的是排麦的
        if (type > 0) {
            contentJsonObj.put("time", String.valueOf(System.currentTimeMillis()));
            contentJsonObj.put("experLevel", userInfo.getExperLevel() + "");
            contentJsonObj.put("erbanNo", userInfo.getErbanNo() + "");
        }

        if (!TextUtils.isEmpty(userInfo.getHeadwearUrl())) {
            contentJsonObj.put("headwearUrl", userInfo.getHeadwearUrl());
        } else {
            contentJsonObj.put(SpEvent.headwearUrl, SpUtils.get(CoreContact.mContext, SpEvent.headwearUrl, ""));
        }
        L.debug(TAG, "headwearUrlFunc userInfo.getHeadwearUrl() = %s, sp.headwearUrl = %s", userInfo.getHeadwearUrl(), SpUtils.get(CoreContact.mContext, SpEvent.headwearUrl, ""));

        if (userInfo.getCpUser() != null) {
            contentJsonObj.put(SpEvent.cpUserId, userInfo.getCpUser().getUid());
            contentJsonObj.put(SpEvent.cpGiftLevel, userInfo.getCpUser().getCpGiftLevel());
        } else {
            contentJsonObj.put(SpEvent.cpUserId, 0L);
            contentJsonObj.put(SpEvent.cpGiftLevel, 0);
        }

        contentJsonObj.put("uid", String.valueOf(userInfo.getUid()));
        contentJsonObj.put("nick", userInfo.getNick());
        contentJsonObj.put("avatar", userInfo.getAvatar());
        contentJsonObj.put("gender", userInfo.getGender());
        String value = contentJsonObj.toJSONString();
        L.debug(TAG, "updataQueueExBySdk(): value = %s", value);
        updataQueueExBySdk(micPosition, roomId, callBack, value);
    }


    public void updataQueueExBySdk(int micPosition, String roomId, final CallBack<String> callBack, String value) {
        updataQueueExBySdk(micPosition, roomId, callBack, value, true);
    }

    public void updataQueueExBySdk(int micPosition, String roomId, final CallBack<String> callBack, String value, boolean isTransient) {
        //防止房主掉麦
        if (micPosition == -1) {
            String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            if (!AvRoomDataManager.get().isRoomOwner(currentUid)) {
                return;
            }
        }
        LogUtils.d("micInListLogDpdataQueue", "getRoomId:" + roomId + "   key:" + micPosition);
        NIMChatRoomSDK.getChatRoomService()
                .updateQueueEx(roomId, String.valueOf(micPosition), value, isTransient)
                .setCallback(new RequestCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if (callBack != null) {
                            callBack.onSuccess("上麦成功");
                        }
                        LogUtils.d("micInListLogDpdataQueue", 1 + "");
                    }

                    @Override
                    public void onFailed(int i) {
                        if (callBack != null) {
                            callBack.onFail(-1, "上麦失败");
                        }
                        LogUtils.d("micInListLogDpdataQueue", 2 + "");
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        if (callBack != null) {
                            callBack.onFail(-1, "上麦异常");
                        }
                        LogUtils.d("micInListLogDpdataQueue", 3 + "");
                    }
                });
    }

    /**
     * 获取房间队列信息
     *
     * @param roomId
     */
    public Observable<List<Entry<String, String>>> queryRoomMicInfo(final String roomId) {
        return Observable.create(new ObservableOnSubscribe<List<Entry<String, String>>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Entry<String, String>>> e) throws Exception {
                executeNIMClient(NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .fetchQueue(roomId)), e);
            }
        }).subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());
    }

    /**
     * 获取聊天室信息
     *
     * @param roomId 聊天室id
     * @return ChatRoomInfo 房间信息
     */
    public Observable<ChatRoomInfo> startGetOnlineMemberNumberJob(final long roomId) {
        return Observable.create(new ObservableOnSubscribe<ChatRoomInfo>() {
            @Override
            public void subscribe(ObservableEmitter<ChatRoomInfo> e) throws Exception {
                executeNIMClient(NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .fetchRoomInfo(String.valueOf(roomId))), e);
            }
        }).subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());
    }

    /**
     * 开房间
     * uid：必填
     * type：房间类型，1竞拍房，2悬赏房，必填
     * title：房间标题
     * roomDesc：房间描述
     * backPic：房间背景图
     * rewardId：当type为2时，必填如rewardId
     */
    public Observable<RoomInfo> openRoom(long uid, int type, String title, String roomDesc,
                                         String backPic, String rewardId) {
        if (TextUtils.isEmpty(title)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                title = userInfo.getNick() + "的房间";
            }
        }
        return RxNet.create(AvRoomModel.AvRoomService.class)
                .openRoom(uid, CoreManager.getCore(IAuthCore.class).getTicket(),
                        "", type, title, roomDesc, backPic, rewardId)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .flatMap(this.<RoomInfo>getFunction())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
