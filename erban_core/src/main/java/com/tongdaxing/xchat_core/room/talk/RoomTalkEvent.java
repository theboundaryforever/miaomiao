package com.tongdaxing.xchat_core.room.talk;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;

/**
 * Created by Chen on 2019/6/26.
 */
public class RoomTalkEvent {
    public static class OnAddDefaultMessageFinish {
    }

    public static class OnRoomShowTalkMessage {
        private IRoomTalkMessage mRoomTalkMessage;

        public OnRoomShowTalkMessage(IRoomTalkMessage roomTalkMessage) {
            this.mRoomTalkMessage = roomTalkMessage;
        }

        public IRoomTalkMessage getRoomTalkMessage() {
            return mRoomTalkMessage;
        }
    }

    public static class OnTalkMessageAccept<T extends IRoomTalkMessage> {
        private T mMessage;

        public OnTalkMessageAccept(T message) {
            this.mMessage = message;
        }

        public T getMessage() {
            return mMessage;
        }
    }

    public static class OnRoomSendMessageFailed {
        private Throwable mException;

        public OnRoomSendMessageFailed(Throwable e) {
            this.mException = e;
        }

        public Throwable getException() {
            return mException;
        }
    }

    public static class OnRoomPkMsg {
        private PkCustomAttachment mAttachment;

        public OnRoomPkMsg(PkCustomAttachment attachment) {
            this.mAttachment = attachment;
        }

        public PkCustomAttachment getAttachment() {
            return mAttachment;
        }
    }

    public static class OnRoomMsg {
        private ChatRoomMessage mMessage;

        public OnRoomMsg(ChatRoomMessage message) {
            this.mMessage = message;
        }

        public ChatRoomMessage getMessage() {
            return mMessage;
        }
    }

    public static class OnRoomMsgItemClicked{
        private ChatRoomMessage mMessage;

        public OnRoomMsgItemClicked(ChatRoomMessage message) {
            this.mMessage = message;
        }

        public ChatRoomMessage getMessage() {
            return mMessage;
        }
    }
}
