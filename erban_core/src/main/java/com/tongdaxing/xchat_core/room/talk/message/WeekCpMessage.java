package com.tongdaxing.xchat_core.room.talk.message;

import com.tongdaxing.xchat_core.im.custom.bean.WeekCpAttachment;
import com.tongdaxing.xchat_core.room.talk.TalkType;

/**
 * Created by Chen on 2019/6/27.
 */
public class WeekCpMessage implements IRoomTalkMessage {

    private WeekCpAttachment weekCpAttachment;

    private String mLocalId;

    public WeekCpMessage() {
        setLocalId(System.currentTimeMillis() + "");
    }

    @Override
    public String getLocalId() {
        return mLocalId;
    }

    @Override
    public void setLocalId(String id) {
        mLocalId = id;
    }

    @Override
    public int getRoomTalkType() {
        return TalkType.ROOM_TALK_HEADER_TYPE_WEEK_CP;
    }

    public WeekCpAttachment getWeekCpAttachment() {
        return weekCpAttachment;
    }

    public void setWeekCpAttachment(WeekCpAttachment weekCpAttachment) {
        this.weekCpAttachment = weekCpAttachment;
    }
}
