package com.tongdaxing.xchat_core.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;

import java.util.List;

/**
 * <p>
 * Created by zhangjian on 2019/5/7.
 */
public interface IHomeLoverUserListView extends IMvpBaseView {
    void onRequestChatMemberByPageSuccess(List<OnlineChatMember> onlineChatMembers);

    void onRequestChatMemberByPageFail(String message);
}
