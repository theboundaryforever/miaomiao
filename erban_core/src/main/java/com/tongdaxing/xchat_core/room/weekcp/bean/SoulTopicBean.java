package com.tongdaxing.xchat_core.room.weekcp.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2019/5/7.
 */
public class SoulTopicBean {
    /**
     * code : 200
     * data : [{"classContent":["你喜欢做什么运动？","你不喜欢吃什么东西？","你喜欢在家做饭还是下馆子？","你喜欢什么花？","你喜欢吃什么口味的糖？","你喜欢什么动漫？","你平时看一些什么剧？","你平时爱看什么类型的电影？","你平时打什么游戏？","你的拿手菜是？"],"className":"兴趣爱好"},{"classContent":["你生命中最早的记忆是什么？","有因为着急而进入异性厕所吗？","有多少个异性追过你？","有没有在公共场合做了一个很大的动作，所有的人都看着你？","你觉得你好不好看？","你现在心里最想谁？","洗澡时会不会在热水里傻傻的站着？","你做运动的频率是？","平时会在家里尝试做好吃的东西吗？","你用过哪些不堪回首的网名？","你干过最邪恶的事情是什么？","你会和陌生人去旅行吗？"],"className":"私密交流"},{"classContent":["你早上会不会吃早餐？","将来想生活在哪个城市？","如果你中了彩票还会继续上班吗？","你现在想吃什么？","你对什么东西过敏？","你旅游去过哪些地方？","你有没有健身的习惯？","拿到工资的第一件事是什么？","你作息规律吗？"],"className":"生活习惯"},{"classContent":["你相信一见钟情吗？","身边的人陆续结婚，自己很孤独？","有没有人说你唱歌很好听？","你总是这样油嘴滑舌吗？","心灵美和外表美哪个更重要？","你吃醋是什么样子的？","你心中完美的婚礼是什么样子的？","结婚前会看生辰八字吗？"],"className":"恋爱情感"}]
     * message : 200:success
     */

    private int code;
    private String message;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SoulTopicBean{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public static class DataBean {
        /**
         * classContent : ["你喜欢做什么运动？","你不喜欢吃什么东西？","你喜欢在家做饭还是下馆子？","你喜欢什么花？","你喜欢吃什么口味的糖？","你喜欢什么动漫？","你平时看一些什么剧？","你平时爱看什么类型的电影？","你平时打什么游戏？","你的拿手菜是？"]
         * className : 兴趣爱好
         */

        private String className;
        private ArrayList<String> classContent;

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public ArrayList<String> getClassContent() {
            return classContent;
        }

        public void setClassContent(ArrayList<String> classContent) {
            this.classContent = classContent;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "className='" + className + '\'' +
                    ", classContent=" + classContent +
                    '}';
        }
    }
}
