package com.tongdaxing.xchat_core.room.weekcp;

import com.alibaba.fastjson.JSONObject;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * Created by Chen on 2019/5/9.
 */
public class BaseWeekCtrl {

    protected String getRoomUid() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null) {
            return String.valueOf(roomInfo.getUid());
        }
        return "";
    }

    protected String getUid() {
        return String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    protected long getUserId() {
        return CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }

    protected String getTicket() {
        return CoreManager.getCore(IAuthCore.class).getTicket();
    }

    public void showWeekCpFailedMsg(String response) {
        try {
            JSONObject object = JSONObject.parseObject(response);
            int code = object.getIntValue("code");
            String message = object.getString("message");
            if (code != OkHttpManager.HTTP_RESPONSE_CODE_SUCCESS) {
                CoreUtils.send(new WeekCpEvent.OnWeekCpFailed(message));
            }
        } catch (Exception e) {
            CoreUtils.crashIfDebug(e, "showWeekCpFailedMsg");
        }
    }
}
