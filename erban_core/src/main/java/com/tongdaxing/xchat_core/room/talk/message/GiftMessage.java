package com.tongdaxing.xchat_core.room.talk.message;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.xchat_core.room.talk.TalkType;

/**
 * Created by Chen on 2019/6/26.
 */
public class GiftMessage implements IRoomTalkMessage {
    private String mLocalId;
    private ChatRoomMessage mChatRoomMessage;


    public GiftMessage() {
        setLocalId(System.currentTimeMillis() + "");
    }

    @Override
    public String getLocalId() {
        return mLocalId;
    }

    @Override
    public void setLocalId(String id) {
        mLocalId = id;
    }

    @Override
    public int getRoomTalkType() {
        return TalkType.ROOM_TALK_HEADER_TYPE_GIFT;
    }

    public ChatRoomMessage getChatRoomMessage() {
        return mChatRoomMessage;
    }

    public void setChatRoomMessage(ChatRoomMessage chatRoomMessage) {
        mChatRoomMessage = chatRoomMessage;
    }


}
