package com.tongdaxing.xchat_core.room.presenter;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.room.model.RoomBaseModel;
import com.tongdaxing.xchat_core.room.view.IRoomBlackView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class RoomBlackPresenter extends AbstractMvpPresenter<IRoomBlackView> {
    private final RoomBaseModel mRoomBaseModel;

    public RoomBlackPresenter() {
        mRoomBaseModel = new RoomBaseModel();
    }

    public void queryNormalListFromIm(int limit, long time) {
        mRoomBaseModel.queryNormalList(limit, time)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<ChatRoomMember>>() {
                    @Override
                    public void accept(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                        if (getMvpView() != null)
                            getMvpView().queryNormalListSuccess(chatRoomMemberList);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (getMvpView() != null)
                            getMvpView().queryNormalListFail();
                    }
                });
    }

    /**
     * 拉黑操作
     *
     * @param roomId
     * @param account
     * @param mark    true，拉黑，false：移除拉黑
     */
    public void markBlackList(long roomId, String account, final boolean mark) {
        mRoomBaseModel.markBlackList(roomId, String.valueOf(account), mark, new CallBack<ChatRoomMember>() {
            @Override
            public void onSuccess(ChatRoomMember data) {
                if (getMvpView() != null)
                    getMvpView().makeBlackListSuccess(data, mark);
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null)
                    getMvpView().makeBlackListFail(code, error, mark);
            }
        });
    }
}
