package com.tongdaxing.xchat_core.room.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.orhanobut.logger.Logger;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * <p>
 * Created by zhangjian on 2019/5/7.
 */
public class HomeLoverOnlineUserModel extends RoomBaseModel {
    public static final String TAG = "HomeLoverOnlineUserModel";

    /**
     * 分页获取房间成员：第一页包含队列成员，固定成员，游客50人，之后每一页获取游客50人
     * <p>
     * 固定成员列表用updateTime,
     * 游客列表用进入enterTime，
     * 填0会使用当前服务器最新时间开始查询，即第一页，单位毫秒
     */
    public Single<List<OnlineChatMember>> getPageMembers() {
        L.info(TAG, "getPageMembers");
        Single<List<ChatRoomMember>> onlineUserObservable = queryOnlineList(500);
        Single<List<ChatRoomMember>> firstGuestObservable = queryGuestList(ROOM_MEMBER_SIZE, 0);

        return Single.zip(onlineUserObservable, firstGuestObservable,
                new BiFunction<List<ChatRoomMember>, List<ChatRoomMember>, List<OnlineChatMember>>() {
                    @Override
                    public List<OnlineChatMember> apply(List<ChatRoomMember> onlineChatRoomMemberList,
                                                        List<ChatRoomMember> guestChatRoomMemberList) throws Exception {
                        return getChatRoomMemberList(onlineChatRoomMemberList, guestChatRoomMemberList);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    private List<OnlineChatMember> getChatRoomMemberList(List<ChatRoomMember> oldList,
                                                         List<ChatRoomMember> newList) {
        L.info(TAG, "合并固定成员列表和游客列表 getChatRoomMemberList(): onlineChatRoomMemberList.size = %d, guestChatRoomMemberList.size = %d",
                oldList == null ? -1 : oldList.size(), newList == null ? -1 : newList.size());
        long startTime = System.currentTimeMillis();
       /* try {
            Thread.sleep(12000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        //处理耗时的循环
        List<OnlineChatMember> allMemberList = OnlineChatMember.coverToOnlineChatMember(oldList, newList);

        List<OnlineChatMember> part1MemberList = new ArrayList<>();
        List<OnlineChatMember> limitMemberList = new ArrayList<>();
        List<OnlineChatMember> managerMemberList = new ArrayList<>();
        List<OnlineChatMember> normalMemberList = new ArrayList<>();
        List<OnlineChatMember> onMicMemberList = new ArrayList<>();
        List<OnlineChatMember> guestMemberList = new ArrayList<>();

        int size = AvRoomDataManager.get().mMicQueueMemberMap.size();
        boolean isRoomOwnerOnline = false;
        ChatRoomMember chatRoomMember;
        for (OnlineChatMember temp : allMemberList) {
            chatRoomMember = temp.chatRoomMember;
            if (chatRoomMember == null) continue;
            String account = chatRoomMember.getAccount();
            MemberType memberType = chatRoomMember.getMemberType();
            //自己
            if (AvRoomDataManager.get().isSelf(account)) {
                AvRoomDataManager.get().mOwnerMember = chatRoomMember;
            }
            //在麦上集合处理
            boolean isOnMic = false;
            for (int i = 0; i < size; i++) {
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo.mChatRoomMember != null
                        && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), account)) {
                    temp.isOnMic = true;
                    if (memberType == MemberType.CREATOR) {
                        isRoomOwnerOnline = true;
                        //房主在麦上
                        temp.isRoomOwer = true;
                        onMicMemberList.add(0, temp);
                    } else if (memberType == MemberType.ADMIN) {
                        //管理员在麦上
                        temp.isAdmin = true;
                        onMicMemberList.add(temp);
                    } else {
                        onMicMemberList.add(temp);
                    }
                    isOnMic = true;
                }
            }
            if (isOnMic) continue;

            //处理不再麦上的
            if (memberType == MemberType.ADMIN) {
                temp.isAdmin = true;
                managerMemberList.add(temp);
            } else if (memberType == MemberType.CREATOR) {
                isRoomOwnerOnline = chatRoomMember.isOnline();
                AvRoomDataManager.get().mRoomCreateMember = chatRoomMember;
            } else if (chatRoomMember.isInBlackList() || chatRoomMember.isMuted()) {
                limitMemberList.add(temp);
            } else if (memberType == MemberType.NORMAL) {
                normalMemberList.add(temp);
            } else if (memberType == MemberType.GUEST) {
                guestMemberList.add(temp);
            }
        }

        //添加游客
        if (!ListUtils.isListEmpty(guestMemberList)) {
            L.info(TAG, "游客 guestMemberList.size = %d", guestMemberList.size());
            part1MemberList.addAll(guestMemberList);
        }
        //固定在线普通成员
        if (!ListUtils.isListEmpty(normalMemberList)) {
            L.info(TAG, "固定在线普通成员 guestMemberList.size = %d", normalMemberList.size());
            part1MemberList.addAll(normalMemberList);
        }
        //管理员
        if (!ListUtils.isListEmpty(managerMemberList)) {
            L.info(TAG, "管理员 guestMemberList.size = %d", managerMemberList.size());
            part1MemberList.addAll(managerMemberList);
        }
        //上麦用户
        if (!ListUtils.isListEmpty(onMicMemberList)) {
            L.info(TAG, "上麦用户 onMicMemberList.size = %d", onMicMemberList.size());
            part1MemberList.addAll(onMicMemberList);
        }
        //房主
        L.info(TAG, "房主%s", isRoomOwnerOnline ? "在线" : "离开");
        if (isRoomOwnerOnline) {
            if (AvRoomDataManager.get().mRoomCreateMember != null) {
                if (!ListUtils.isListEmpty(onMicMemberList)
                        && !AvRoomDataManager.get().isRoomOwner(onMicMemberList.get(0).chatRoomMember.getAccount())) {
                    part1MemberList.add(new OnlineChatMember(AvRoomDataManager.get().mRoomCreateMember,
                            false, false, true));
                } else if (ListUtils.isListEmpty(onMicMemberList)) {
                    //处理麦上没有人的情况
                    part1MemberList.add(new OnlineChatMember(AvRoomDataManager.get().mRoomCreateMember,
                            false, false, true));
                }
            }
        }
        L.info(TAG, "合并固定成员列表和游客列表 getChatRoomMemberList: part1MemberList.size = %d", part1MemberList.size());
        Logger.i("循环处理在线顺序列表耗时：" + (System.currentTimeMillis() - startTime));
        return part1MemberList;
    }

    /**
     * 成员进来刷新在线列表
     *
     * @param account           进来成员的账号
     * @param onlineChatMembers 成员列表
     */
    public Single<List<OnlineChatMember>> onMemberInRefreshData(String account, int page,
                                                                final List<OnlineChatMember> onlineChatMembers) {
        if (TextUtils.isEmpty(account)) return Single.error(new Throwable("account 不能为空"));
        List<String> accounts = new ArrayList<>(1);
        accounts.add(account);
        return IMNetEaseManager.get().fetchRoomMembersByIds(accounts)
                .observeOn(Schedulers.io())
                .map(new Function<List<ChatRoomMember>, List<OnlineChatMember>>() {
                    @Override
                    public List<OnlineChatMember> apply(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                        if (ListUtils.isListEmpty(chatRoomMemberList)) return onlineChatMembers;
                        return /*getChatRoomMemberList(chatRoomMemberList, OnlineChatMember.converOnlineToNormal(onlineChatMembers))*/onlineChatMembers;
                    }
                })
                .delay(page == Constants.PAGE_START ? 2 : 0, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread());
    }
}
