package com.tongdaxing.xchat_core.room.weekcp;

import com.tcloud.core.CoreUtils;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.weekcp.tacit.ITacitTestCtrl;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTestCtrl;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTextInfo;
import com.tongdaxing.xchat_core.room.weekcp.topic.ISoulTopicCtrl;
import com.tongdaxing.xchat_core.room.weekcp.topic.SoulTopicCtrl;
import com.tongdaxing.xchat_core.room.weekcp.topic.SoulTopicInfo;
import com.tongdaxing.xchat_core.room.weekcp.week.IWeekCpCtrl;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpCtrl;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpInfo;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Chen on 2019/5/7.
 */
public class WeekManager implements IWeekManager {
    private WeekCpCtrl mWeekCpCtrl;
    private WeekCpInfo mWeekCpInfo;
    private SoulTopicCtrl mSoulTopicCtrl;
    private SoulTopicInfo mSoulTopicInfo;
    private TacitTestCtrl mTacitTestCtrl;
    private TacitTextInfo mTacitTextInfo;

    public WeekManager() {

        CoreUtils.register(this);

        init();
    }

    private void init() {
        mWeekCpCtrl = new WeekCpCtrl();
        mSoulTopicCtrl = new SoulTopicCtrl();
        mTacitTestCtrl = new TacitTestCtrl();

        mWeekCpInfo = new WeekCpInfo();
        mSoulTopicInfo = new SoulTopicInfo();
        mTacitTextInfo = new TacitTextInfo();

        mWeekCpCtrl.setWeekCpInfo(mWeekCpInfo);
        mSoulTopicCtrl.setSoulTopicInfo(mSoulTopicInfo);
        mTacitTestCtrl.setTacitTextInfo(mTacitTextInfo);
    }

    private void reset() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null && roomInfo.getTagType() == RoomInfo.ROOMTYPE_LOVERS) {
            mWeekCpInfo = new WeekCpInfo();
            mSoulTopicInfo = new SoulTopicInfo();
            mTacitTextInfo = new TacitTextInfo();

            mWeekCpCtrl.setWeekCpInfo(mWeekCpInfo);
            mSoulTopicCtrl.setSoulTopicInfo(mSoulTopicInfo);
            mTacitTestCtrl.setTacitTextInfo(mTacitTextInfo);
        }
    }

    @Override
    public IWeekCpCtrl getWeekCpCtrl() {
        return mWeekCpCtrl;
    }

    @Override
    public WeekCpInfo getWeekCpInfo() {
        return mWeekCpInfo;
    }

    @Override
    public ISoulTopicCtrl getSoulTopicCtrl() {
        return mSoulTopicCtrl;
    }

    @Override
    public SoulTopicInfo getSoulTopicInfo() {
        return mSoulTopicInfo;
    }

    @Override
    public ITacitTestCtrl getTacitTestCtrl() {
        return mTacitTestCtrl;
    }

    @Override
    public TacitTextInfo getTacitTextInfo() {
        return mTacitTextInfo;
    }

    @Override
    public void setTacitTextInfo(TacitTextInfo tacitTextInfo) {
        mTacitTextInfo = tacitTextInfo;
    }

    @Subscribe
    public void onEnterRoomSuccess(NewRoomEvent.OnEnterRoomSuccess roomSuccess) {
        reset();
    }
}
