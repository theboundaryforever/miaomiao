package com.tongdaxing.xchat_core.room.weekcp.week;

import com.tcloud.core.log.L;

/**
 * Created by Chen on 2019/4/30.
 */
public class WeekCpInfo {
    private int mUid;

    private volatile int mHeartValue;

    private String mCpId;

    private int mIntimacyValue;
    private String TAG = "WeekCpInfo";

    public int getUid() {
        return mUid;
    }

    public void setUid(int uid) {
        mUid = uid;
    }

    public int getHeartValue() {
        return mHeartValue;
    }

    public void setHeartValue(int heartValue) {
        mHeartValue = heartValue;
    }

    public String getCpId() {
        return mCpId;
    }

    public void setCpId(String cpId) {
        L.info(TAG, "setCpId cpId: %s", cpId);
        mCpId = cpId;
    }

    public int getIntimacyValue() {
        return mIntimacyValue;
    }

    public void setIntimacyValue(int intimacyValue) {
        mIntimacyValue = intimacyValue;
    }
}
