package com.tongdaxing.xchat_core.room.model;

import android.content.Context;

import com.miaomiao.ndklib.JniUtils;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.orhanobut.logger.Logger;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p> 轰趴房model层：数据获取 </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class HomePartyModel extends RoomBaseModel {
    private String TAG = "HomePartyModel";

    public HomePartyModel() {
    }

    public void lockMicroPhone(int micPosition, String roomUid, String ticker, ResponseListener listener, ResponseErrorListener errorListener) {
        operateMicroPhone(micPosition, "1", roomUid, ticker, listener, errorListener);
//        execute(mHomePartyService.lockMicroPhone(micPosition, "1", roomUid, ticker,
//                CoreManager.getCore(IAuthCore.class).getCurrentUid())
//                .subscribeOn(Schedulers.io())
//                .unsubscribeOn(Schedulers.io())
//                .onErrorResumeNext(this.<String>getCommonExceptionFunction())
//                .observeOn(AndroidSchedulers.mainThread()), callBack);
    }

    /**
     * 释放该麦坑的锁
     *
     * @param micPosition
     */
    public void unLockMicroPhone(int micPosition, String roomUid, String ticker, ResponseListener listener, ResponseErrorListener errorListener) {
        operateMicroPhone(micPosition, "0", roomUid, ticker, listener, errorListener);
//        return mHomePartyService.lockMicroPhone(micPosition, "0", roomUid, ticker,
////                CoreManager.getCore(IAuthCore.class).getCurrentUid())
////                .subscribeOn(Schedulers.io())
////                .unsubscribeOn(Schedulers.io())
////                .onErrorResumeNext(this.<String>getCommonExceptionFunction())
////                .flatMap(this.<String>getFunction())
////                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 操作麦坑的锁
     *
     * @param micPosition
     * @param state         1：锁，0：不锁
     * @param roomUid
     * @param ticker
     * @param listener
     * @param errorListener
     */
    private void operateMicroPhone(int micPosition, String state, String roomUid, String ticker, ResponseListener listener, ResponseErrorListener errorListener) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("position", micPosition + "");
        requestParam.put("state", state);
        requestParam.put("roomUid", roomUid);
        requestParam.put("ticket", ticker);
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getlockMicroPhone(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }


    private void updateMicQueue(final String roomId, final String key, String value) {
        NIMChatRoomSDK.getChatRoomService()
                .updateQueue(roomId, key, value)
                .setCallback(new RequestCallbackWrapper<Void>() {
                    @Override
                    public void onResult(int i, Void aVoid, Throwable throwable) {
                        Logger.i("%1$s房间更新麦序%2$s成功", roomId, key);
                    }
                });
    }


    /**
     * 获取固定成员（创建者,管理员,普通用户,受限用户）
     *
     * @param roomId
     * @param time
     * @param limit
     */
    public void loadNormalMembers(String roomId, long time, int limit, final CallBack<List<ChatRoomMember>> callback) {
        NIMChatRoomSDK.getChatRoomService().fetchRoomMembers(roomId, MemberQueryType.NORMAL, time, limit)
                .setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
                    @Override
                    public void onResult(int code, List<ChatRoomMember> result, Throwable exception) {
                        L.info(TAG, "loadNormalMembers onResult, code: %d, result: %s", code, result);
                        if (ListUtils.isListEmpty(result)) {
                            AvRoomDataManager.get().clearMembers();
                            return;
                        }
                        AvRoomDataManager.get().mRoomFixedMemberList.addAll(result);
                        for (ChatRoomMember chatRoomMember : result) {
                            if (chatRoomMember == null) {
                                continue;
                            }
                            MemberType memberType = chatRoomMember.getMemberType();
                            switch (memberType) {
                                case ADMIN:
                                    AvRoomDataManager.get().mRoomManagerList.add(chatRoomMember);
                                    break;
                                case CREATOR:
                                    AvRoomDataManager.get().mRoomCreateMember = chatRoomMember;
                                    break;
                                case NORMAL:
                                    break;
                                case ANONYMOUS:
                                    break;
                                default:
                            }
                        }
                        if (callback != null) {
                            callback.onSuccess(result);
                        }
                    }
                });
    }

    /**
     * 开麦
     *
     * @param micPosition
     * @param roomUid
     */
    public void openMicroPhone(int micPosition, long roomUid, ResponseListener listener, ResponseErrorListener errorListener) {
        openOrCloseMicroPhone(micPosition, 0, roomUid, CoreManager.getCore(IAuthCore.class).getTicket(), listener, errorListener);
    }

    /**
     * 闭麦
     *
     * @param micPosition
     * @param roomUid
     */
    public void closeMicroPhone(int micPosition, long roomUid, ResponseListener listener, ResponseErrorListener errorListener) {
        openOrCloseMicroPhone(micPosition, 1, roomUid, CoreManager.getCore(IAuthCore.class).getTicket(), listener, errorListener);
    }

    /**
     * 开闭麦接口
     *
     * @param micPosition
     * @param state       1:闭麦，0：开麦
     * @param roomUid
     * @param ticket
     */
    private void openOrCloseMicroPhone(int micPosition, int state, long roomUid, String ticket, ResponseListener listener, ResponseErrorListener errorListener) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("position", micPosition + "");
        requestParam.put("state", state + "");
        requestParam.put("roomUid", roomUid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.operateMicroPhone(),
                        CommonParamUtil.getDefaultHeaders(),
                        requestParam, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }

    /**
     * 麦上用户校验接口
     */
    public void kickIllegal(Context context, long roomUid, String exceptionUserId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        try {
            String content = "roomUid".concat("=").concat(String.valueOf(roomUid)).concat("&").concat("kuids").concat("=").concat(exceptionUserId);
            params.put("data", JniUtils.encrypt(context, content));
        } catch (Exception e) {
            e.printStackTrace();
        }
        OkHttpManager.getInstance().doPostRequest(UriProvider.kickIllegal(), null, params, callBack);
    }
}
