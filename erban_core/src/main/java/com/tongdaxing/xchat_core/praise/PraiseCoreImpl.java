package com.tongdaxing.xchat_core.praise;

import android.util.LongSparseArray;

import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.im.room.IIMRoomCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.result.BooleanResult;
import com.tongdaxing.xchat_core.result.UserListResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;


/**
 * Created by zhouxiangfeng on 2017/5/18.
 */

public class PraiseCoreImpl extends AbstractBaseCore implements IPraiseCore {
    private String TAG = "PraiseCoreImpl";

    private LongSparseArray<Boolean> mPraisePlayer = new LongSparseArray<>();

    public PraiseCoreImpl() {
        CoreManager.addClient(this);
    }

    @Override
    public void recommendFriends(final long likedUid) {
        L.info(TAG, "recommendFriends likedUid: %d", likedUid);
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("type", String.valueOf(1));
        params.put("likedUid", likedUid + "");

        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (response.isSuccess()) {
                    RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                    if (roomInfo != null && roomInfo.getUid() == likedUid) {
                        sendAttentionRoomTipMsg(likedUid);
                    }
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_RECOMMEND_MODULE_PRAISE, likedUid);
                } else {
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_RECOMMEND_MODULE_PRAISE_FAITH, response.getMessage());
                }

            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_RECOMMEND_MODULE_PRAISE_FAITH, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.praise(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST);
    }

    @Override
    public void praise(final long likedUid) {
        L.info(TAG, "praise likedUid: %d", likedUid);
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("type", String.valueOf(1));
        params.put("likedUid", likedUid + "");

        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                L.info(TAG, "onResponse response: %s", response);
                if (response.isSuccess()) {
                    RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                    if (roomInfo != null && roomInfo.getUid() == likedUid) {
                        sendAttentionRoomTipMsg(likedUid);
                    }
                    mPraisePlayer.put(likedUid, true);
                    CoreUtils.send(new PraiseEvent.OnAttentionSuccess(likedUid, true, false));
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_PRAISE, likedUid);
                } else {
                    CoreUtils.send(new PraiseEvent.OnAttentionFailed(response.getMessage()));
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_PRAISE_FAITH, response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                L.info(TAG, "onResponse onErrorResponse: %s", error);
                CoreUtils.send(new PraiseEvent.OnAttentionFailed(error.getErrorStr()));
                notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_PRAISE_FAITH, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.praise(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST);

    }

    @Override
    public void cancelPraise(final long canceledUid) {
        L.info(TAG, "cancelPraise canceledUid: %d", canceledUid);
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("type", String.valueOf(2));
        params.put("likedUid", canceledUid + "");

        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (response.isSuccess()) {
                    CoreUtils.send(new PraiseEvent.OnAttentionCancel(canceledUid));
                    mPraisePlayer.put(canceledUid, false);
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_CANCELED_PRAISE, canceledUid);
                } else {
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_CANCELED_PRAISE_FAITH, response.getMessage());
                }

            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_CANCELED_PRAISE_FAITH, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.praise(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST);
    }

    @Override
    public void deleteLike(final long deletedUid) {
        L.info(TAG, "deleteLike deletedUid: %d", deletedUid);
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("likedUid", deletedUid + "");

        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_CANCELED_PRAISE, deletedUid);
                    } else {
                        notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_CANCELED_PRAISE_FAITH, response.getMessage());
                    }
                }

            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_CANCELED_PRAISE_FAITH, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.deleteLike(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST);
    }

    @Override
    public void getAllFans(long uid, int pageSize, int pageNo) {
        L.info(TAG, "getAllFans uid: %d, pageSize: %d, pageNo: %d", uid, pageSize, pageNo);
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("pageSize", String.valueOf(pageSize));
        params.put("pageNo", String.valueOf(pageNo));

        ResponseListener listener = new ResponseListener<UserListResult>() {
            @Override
            public void onResponse(UserListResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_GET_ALL_FANS, response.getData());
                    } else {
                        notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_GET_ALL_FANS_FAITH, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_GET_ALL_FANS_FAITH, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getAllFans(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                UserListResult.class,
                Request.Method.POST);
    }

    @Override
    public void isPraised(long uid, final long isLikeUid) {
        L.info(TAG, "isPraised uid: %d, isLikeUid: %d", uid, isLikeUid);
//        http:// www.daxiaomao.com /fans/islike?uid=90001&isLikeUid=900002
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("isLikeUid", String.valueOf(isLikeUid));

        ResponseListener listener = new ResponseListener<BooleanResult>() {
            @Override
            public void onResponse(BooleanResult response) {
                if (null != response) {
                    String message = response.getMessage();
                    if (response.isSuccess()) {
                        if (null != response.getData()) {
                            mPraisePlayer.put(isLikeUid, response.getData());
                            CoreUtils.send(new PraiseEvent.OnAttentionSuccess(isLikeUid, response.getData(), true));
                            notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_ISLIKED, response.getData(), isLikeUid);
                        } else {
                            notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_ISLIKED_FAITH, message);
                            CoreUtils.send(new PraiseEvent.OnAttentionFailed(message));
                        }
                    } else {
                        notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_ISLIKED_FAITH, message);
                        CoreUtils.send(new PraiseEvent.OnAttentionFailed(message));
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                CoreUtils.send(new PraiseEvent.OnAttentionFailed(error.getErrorStr()));
                notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_ISLIKED_FAITH, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.isLike(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                BooleanResult.class,
                Request.Method.GET);
    }

    @Override
    public boolean checkIsPraised(long likeId) {
        int size = mPraisePlayer.size();
        for (int i = 0; i < size; i++) {
            long playerId = mPraisePlayer.keyAt(i);
            if (playerId == likeId) {
                return mPraisePlayer.valueAt(i);
            }
        }
        return false;
    }

    private void sendAttentionRoomTipMsg(long targetUid) {
        L.info(TAG, "sendAttentionRoomTipMsg targetUid: %d", targetUid);
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(targetUid);
        if (roomInfo != null && userInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            UserInfo myUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(myUid);
            if (myUserInfo == null) {
                return;
            }
            RoomTipAttachment roomTipAttachment = new RoomTipAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP, CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_ATTENTION_ROOM_OWNER);
            roomTipAttachment.setUid(myUid);
            roomTipAttachment.setNick(myUserInfo.getNick());
            roomTipAttachment.setTargetUid(targetUid);
            roomTipAttachment.setTargetNick(userInfo.getNick());

            roomTipAttachment.setCharmLevel(userInfo.getCharmLevel());
            roomTipAttachment.setExperLevel(userInfo.getExperLevel());
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    // 自定义消息
                    roomTipAttachment
            );

            CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
        }
    }
}
