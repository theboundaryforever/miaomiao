package com.tongdaxing.xchat_core.praise;

/**
 * Created by Chen on 2019/5/20.
 */
public class PraiseEvent {
    public static class OnAttentionSuccess {
        boolean mIsAttention;
        private long mUid;
        private boolean mIsCheckAttention;

        public OnAttentionSuccess(long uid, boolean isAttention, boolean isCheckAttention) {
            this.mUid = uid;
            this.mIsAttention = isAttention;
            this.mIsCheckAttention = isCheckAttention;
        }

        public long getUid() {
            return mUid;
        }

        public boolean isAttention() {
            return mIsAttention;
        }

        public boolean isCheckAttention() {
            return mIsCheckAttention;
        }
    }

    public static class OnAttentionFailed {
        private String mMessage;

        public OnAttentionFailed(String msg) {
            this.mMessage = msg;
        }

        public String getMessage() {
            return mMessage;
        }
    }

    public static class OnAttentionCancel {
        private long mUid;

        public OnAttentionCancel(long uid) {
            this.mUid = uid;
        }

        public long getUid() {
            return mUid;
        }
    }
}
