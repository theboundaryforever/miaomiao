package com.tongdaxing.xchat_core.bills;

import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.ChargeResult;
import com.tongdaxing.xchat_core.result.ExpendResult;
import com.tongdaxing.xchat_core.result.IncomedResult;
import com.tongdaxing.xchat_core.result.RedBagResult;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;

/**
 * Created by Seven on 2017/9/9.
 */

public class BillsCoreImpl extends AbstractBaseCore implements IBillsCore {
    private static final String TAG = "BillsCoreImpl";

    /**
     * 账单获取情况
     *
     * @param pageNo
     * @param pageSize
     * @param time
     * @param type     1：礼物支出记录 2：礼物收入记录 3：密聊记录 4：充值记录 5：提现记录
     */
    private void getBillRecode(int pageNo, int pageSize, long time, final int type) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("pageNo", String.valueOf(pageNo));
        param.put("pageSize", String.valueOf(pageSize));
        param.put("date", String.valueOf(time));
        param.put("type", String.valueOf(type));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());


        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                noticeErrorClients(error, type);
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(
                UriProvider.getBillRecord(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param,
                getResponseListener(type),
                errorListener,
                getResponseBeanClass(type),
                Request.Method.GET
        );
    }

    private ResponseListener getResponseListener(int type) {
        switch (type) {
            case 1:
                return giftExpendListener;
            case 2:
                return giftInComeListener;
            case 3:
                return chatListener;
            case 4:
                return getChargeListener;
            case 5:
                return getWithdrawListener;
        }
        return null;
    }

    private Class getResponseBeanClass(int type) {
        switch (type) {
            case 1:
                return ExpendResult.class;
            case 2:
                return IncomedResult.class;
            case 3:
               return IncomedResult.class;
            case 4:
                return ChargeResult.class;
            case 5:
                return IncomedResult.class;
        }
        return null;
    }

    private void noticeErrorClients(RequestError error, int type) {
        String method = null;
        switch (type) {
            case 1:
                method = IBillsCoreClient.METHOD_GET_EXPEND_BILLS_ERROR;
                break;
            case 2:
                method = IBillsCoreClient.METHOD_GET_INCOME_BILLS_ERROR;
                break;
            case 3:
                method = IBillsCoreClient.METHOD_GET_ORDER_INCOME_BILLS_ERROR;
                break;
            case 4:
                method = IBillsCoreClient.METHOD_GET_CHARGE_BILLS_ERROR;
                break;
            case 5:
                method = IBillsCoreClient.METHOD_GET_WIHTDRAW_BILLS_ERROR;
                break;
        }
        if (error != null)
            notifyClients(IBillsCoreClient.class, method, error.getErrorStr());
    }

    //礼物支出监听
    private ResponseListener giftExpendListener = new ResponseListener<ExpendResult>() {
        @Override
        public void onResponse(ExpendResult response) {
            if (response != null) {
                if (response.isSuccess()) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_EXPEND_BILLS, response.getData());
                } else {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_EXPEND_BILLS_ERROR, response.getMessage());
                }
            }
        }
    };

    //礼物收入监听
    private ResponseListener giftInComeListener = new ResponseListener<IncomedResult>() {
        @Override
        public void onResponse(IncomedResult response) {
            if (response != null) {
                if (response.isSuccess()) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_INCOME_BILLS, response.getData());
                } else {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_INCOME_BILLS_ERROR, response.getMessage());
                }
            }
        }
    };

    //提现监听
    private ResponseListener getWithdrawListener = new ResponseListener<IncomedResult>() {
        @Override
        public void onResponse(IncomedResult response) {
            if (response != null) {
                if (response.isSuccess()) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WIHTDRAW_BILLS, response.getData());
                } else {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WIHTDRAW_BILLS_ERROR, response.getMessage());
                }
            }
        }
    };

    //充值记录监听
    private ResponseListener getChargeListener = new ResponseListener<ChargeResult>() {
        @Override
        public void onResponse(ChargeResult response) {
            if (response != null) {
                if (response.isSuccess()) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_CHARGE_BILLS, response.getData());
                } else {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_CHARGE_BILLS_ERROR, response.getMessage());
                }
            }
        }
    };

    //密聊监听
   private ResponseListener chatListener = new ResponseListener<IncomedResult>() {
        @Override
        public void onResponse(IncomedResult response) {
            if (response != null) {
                if (response.isSuccess()) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_ORDER_INCOME_BILLS, response.getData());
                } else {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_ORDER_INCOME_BILLS_ERROR, response.getMessage());
                }
            }
        }
    };


    //礼物收入2
    @Override
    public void getGiftIncomeBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 2);
    }

    //礼物支出1
    @Override
    public void getGiftExpendBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 1);
    }

    //提现记录5
    @Override
    public void getWithdrawBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 5);
    }

    @Override
    public void getWithdrawRedBills(int pageNo, int pageSize, long time) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("pageNo", String.valueOf(pageNo));
        param.put("pageSize", String.valueOf(pageSize));
        param.put("date", String.valueOf(time));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<RedBagResult>() {
            @Override
            public void onResponse(RedBagResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WITHDRAW_RED_BILLS, response.getData());
                    } else {
                        notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WITHDRAW_RED_BILLS_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WITHDRAW_RED_BILLS_ERROR, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getPacketRecordDeposit(),
                CommonParamUtil.getDefaultHeaders(getContext()), param,
                listener, errorListener, RedBagResult.class, Request.Method.GET
        );
    }

    //密聊记录：3
    @Override
    public void getChatBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 3);
    }


    //充值记录4
    @Override
    public void getChargeBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 4);
    }

    @Override
    public void getRedBagBills(int pageNo, int pageSize, long time) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("pageNo", String.valueOf(pageNo));
        param.put("pageSize", String.valueOf(pageSize));
        param.put("date", String.valueOf(time));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<RedBagResult>() {
            @Override
            public void onResponse(RedBagResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_RED_BAG_BILLS, response.getData());
                    } else {
                        notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_RED_BAG_BILLS_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_RED_BAG_BILLS_ERROR, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getPacketRecord(),
                CommonParamUtil.getDefaultHeaders(getContext()), param,
                listener, errorListener, RedBagResult.class, Request.Method.GET
        );
    }
}
