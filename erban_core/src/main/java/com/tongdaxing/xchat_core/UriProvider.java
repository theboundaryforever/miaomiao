package com.tongdaxing.xchat_core;

import android.text.TextUtils;

import com.tongdaxing.xchat_framework.BuildConfig;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.pref.CommonPref;

/**
 * 请求接口接口地址
 */
public class UriProvider {

    public static void init(Env.UriSetting uriSetting) {
        if (uriSetting == Env.UriSetting.Product) {
            //生产环境地址
            initProductUri();
        } else if (uriSetting == Env.UriSetting.Test) {
            //测试环境地址
            initTestUri();
        }
    }

    public static void initDevUri() {
        JAVA_WEB_URL = "http://beta.miaomiaofm.com";
        IM_SERVER_URL = "http://beta.miaomiaofm.com";
    }

    public static void initProductUri() {
        if (BuildConfig.IS_RELEASE) {//product
            JAVA_WEB_URL = "https://www.miaomiaofm.com";
            IM_SERVER_URL = "https://www.miaomiaofm.com";
        } else {//product18
            JAVA_WEB_URL = "http://alpha.miaomiaofm.com";
            IM_SERVER_URL = "http://alpha.miaomiaofm.com";
        }
    }

    public static String checkUpdata() {
        return IM_SERVER_URL.concat("/version/get");
    }

    private static String DEBUG_URL;

    public static void initDevUri(String url) {
        DEBUG_URL = url;
        JAVA_WEB_URL = url;
        IM_SERVER_URL = url;
    }

    private static void initTestUri() {
        int environment = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("environment");
        if (environment == 0) {
            initProductUri();//其他使用DEV的配置，就神曲这个跟新一下
        } else if (environment == 2) {
            JAVA_WEB_URL = "http://alpha.miaomiaofm.com";
            IM_SERVER_URL = "http://alpha.miaomiaofm.com";
        } else {
            if (!TextUtils.isEmpty(DEBUG_URL))
                initDevUri(DEBUG_URL);
            else
                initDevUri();
        }
    }

    /**
     * 默认以下生产环境地址
     */
    public static String JAVA_WEB_URL = "https://www.miaomiaofm.com";
    public static String IM_SERVER_URL = "https://www.miaomiaofm.com";
    public static String HOME_RANKING_URL = "/mm/rank/index.html";

    public static String getGroupResourceBaseUri() {
        return JAVA_WEB_URL.concat("/app/service/resource/list");
    }

    /**
     * 注册接口
     *
     * @return
     */
    public static String getRegisterResourceUrl() {
        return IM_SERVER_URL.concat("/acc/signup");
    }

    /**************************************CP接口********************************************/
    /**
     * 查询广场CP广播
     *
     * @return
     */
    public static String getQueryBroadcastMsg() {
        return IM_SERVER_URL.concat("/user/cp/hallmsg");
    }

    /**
     * CP专属页
     *
     * @return
     */
    public static String getCPPage() {
        return IM_SERVER_URL.concat("/user/cp/page");
    }

    /**
     * 解除CP
     *
     * @return
     */
    public static String getRelieveCp() {
        return IM_SERVER_URL.concat("/user/cp/unband");
    }

    /**
     * 设置cp称号牌
     *
     * @return
     */
    public static String getSetCpSign() {
        return IM_SERVER_URL.concat("/user/cp/setSign");
    }

    /**************************************CP接口********************************************/


    /**************************************发现页---交友接口********************************************/

    /**
     * 获取声圈IM通知列表
     *
     * @return
     */
    public static String getIMVoiceGroupMsgList() {
        return IM_SERVER_URL.concat("/user/moment/listRecord");
    }

    /**
     * 创建房间---房间类型
     *
     * @return
     */
    public static String getCreateRoomType() {
        return IM_SERVER_URL.concat("/room/listType");
    }

    /**
     * 删除自定义标签
     *
     * @return
     */
    public static String delCustomLabel() {
        return IM_SERVER_URL.concat("/room/tag/user/delete");
    }

    /**
     * 添加自定义标签
     *
     * @return
     */
    public static String addCustomLabel() {
        return IM_SERVER_URL.concat("/room/tag/save");
    }

    /**
     * 交友自定义栏目房间列表
     *
     * @return
     */
    public static String getCustomColumn() {
        return IM_SERVER_URL.concat("/home/friendColumn/listRoom");
    }

    /**
     * 推荐栏目
     *
     * @return
     */
    public static String getRecommendColumn() {
        return IM_SERVER_URL.concat("/home/homeColumn/listRoom");
    }

    /**
     * 组队开黑房间列表
     *
     * @return
     */
    public static String getPlayGameList() {
        return IM_SERVER_URL.concat("/home/game/listRoom");
    }

    /**
     * 暖心陪伴房间列表
     *
     * @return
     */
    public static String getWarmRoomList() {
        return IM_SERVER_URL.concat("/home/warm/listRoom");
    }

    /**
     * 情侣速配
     *
     * @return
     */
    public static String getLoverMatch() {
        return IM_SERVER_URL.concat("/home/matchCouple");
    }

    /**
     * 重开房间
     *
     * @return
     */
    public static String getReopenRoom() {
        return IM_SERVER_URL.concat("/room/reopen");
    }

    /**
     * 交友栏目列表
     *
     * @return
     */
    public static String getFriendsColumnList() {
        return IM_SERVER_URL.concat("/home/friendColumn/listRoom");
    }

    /**************************************发现页---交友接口********************************************/


    /**************************************声圈接口********************************************/

    /**
     * 获取推荐圈友数据
     *
     * @return
     */
    public static String getRecommendModule() {
        return IM_SERVER_URL.concat("/user/moment/listActive");
    }

    /**
     * 举报动态信息
     *
     * @return
     */
    public static String getReportVoiceGroup() {
        return IM_SERVER_URL.concat("/user/moment/report");
    }

    /**
     * 对动态点赞
     *
     * @return
     */
    public static String getPublicVoiceGroupLike() {
        return IM_SERVER_URL.concat("/user/moment/like");
    }

    /**
     * 发布动态
     *
     * @return
     */
    public static String getPublicVoiceGroup() {
        return IM_SERVER_URL.concat("/user/moment/push");
    }

    /**
     * 动态圈列表
     *
     * @return
     */
    public static String getPublicVoiceGroupList() {
        return IM_SERVER_URL.concat("/user/moment/list");
    }

    /**
     * 动态详细信息
     *
     * @return
     */
    public static String getVoiceGroupDetailList() {
        return IM_SERVER_URL.concat("/user/moment/info");
    }

    /**
     * 对评论点赞
     *
     * @return
     */
    public static String getPublicVoiceGroupCommentLike() {
        return IM_SERVER_URL.concat("/user/moment/player/like");
    }

    /**
     * 发布评论
     *
     * @return
     */
    public static String getPublicComment() {
        return IM_SERVER_URL.concat("/user/moment/player/push");
    }

    /**
     * 发布评论
     *
     * @return
     */
    public static String getDeleteVoiceGroup() {
        return IM_SERVER_URL.concat("/user/moment/delMoment");
    }

    /**************************************声圈接口********************************************/

    /**
     * 登录接口
     *
     * @return
     */
    public static String getLoginResourceUrl() {
        return IM_SERVER_URL.concat("/oauth/token");
    }

    /**
     * 获取ticket
     *
     * @return
     */
    public static String getAuthTicket() {
        return IM_SERVER_URL.concat("/oauth/ticket");
    }

    /**
     * 获取短信验证码
     *
     * @return
     */
    public static String getSMSCode() {
        return IM_SERVER_URL.concat("/acc/sms");
    }

    /**
     * 获取榜单礼物总价值
     *
     * @return
     */
    public static String getGiftTotalValue() {
        return IM_SERVER_URL.concat("/roomctrb/roomMonthTotal");
    }

    /**
     * 获取指定用户当前所在房间（在P2PMessageActivity类中用到这个接口，后期再更正）
     *
     * @return
     */
    public static String getTheRoomStatus() {
        return IM_SERVER_URL.concat("/userroom/get");
    }

    /**
     * 获取砸蛋不对外展示状态
     *
     * @return
     */
    public static String getPoundEggHideStatus() {
        return IM_SERVER_URL.concat("/user/setting/v1/get");
    }

//    /**
//     * 获取房间轮播图
//     *
//     * @return
//     */
//    public static String getRoomCarousel() {
//        return IM_SERVER_URL.concat("/activity/query");
//    }

    /**
     * 获取房间轮播图
     *
     * @return
     */
    public static String getRoomCarousel() {
        return IM_SERVER_URL.concat("/room/activity/listBanner");
    }

    /**
     * banner点击统计
     *
     * @return
     */
    public static String getBannerStat() {
        return IM_SERVER_URL.concat("/user/behavior/stat/banner");
    }

    /**
     * 隐藏砸蛋
     *
     * @return
     */
    public static String hidePoundEgg() {
        return IM_SERVER_URL.concat("/user/giftPurse/hideDrawMsg");
    }

    /**
     * 清除异常声音
     *
     * @return
     */
    public static String clearExceptionAudio() {
        return IM_SERVER_URL.concat("/room/mic/checkMicInfo");
    }

    /**
     * 获取声鉴卡
     *
     * @return
     */
    public static String getAudioIdentifyCard() {
        return IM_SERVER_URL.concat("/mm/sound/index.html");
    }

    /**
     * 找回，修改密码
     *
     * @return
     */
    public static String modifyPsw() {
        return IM_SERVER_URL.concat("/acc/pwd/reset");
    }

    /**
     * 登出
     *
     * @return
     */
    public static String logout() {
        return IM_SERVER_URL.concat("/acc/logout");
    }

    public static String getUserInfo() {
        return IM_SERVER_URL.concat("/user/v3/get");
    }

    public static String getUserInfoListUrl() {
        return IM_SERVER_URL.concat("/user/list");
    }


    public static String updateUserInfo() {
        return IM_SERVER_URL.concat("/user/update");
    }

    public static String addPhoto() {
        return IM_SERVER_URL.concat("/photo/upload");
    }

    public static String deletePhoto() {
        return IM_SERVER_URL.concat("/photo/delPhoto");
    }

    public static String giftWall() {
        return IM_SERVER_URL.concat("/giftwall/get");
    }

    //获取用户收到的神秘礼物列表
    public static String getMysteryGiftWall() {
        return IM_SERVER_URL.concat("/giftwall/listMystic");
    }

    public static String praise() {
        return IM_SERVER_URL.concat("/fans/like");
    }

    public static String deleteLike() {
        return IM_SERVER_URL.concat("/fans/fdelete");
    }

    public static String isLike() {
        return IM_SERVER_URL.concat("/fans/islike");
    }

    public static String searchUserInfo() {
        return IM_SERVER_URL.concat("/search/user");
    }

    public static String getAllFans() {
        return IM_SERVER_URL.concat("/fans/following");

    }

    /**
     * 在线的关注人员
     */
    public static String getAllFansOnLine() {
        return IM_SERVER_URL.concat("/fans/v3/following");

    }

    public static String getFansList() {
        return IM_SERVER_URL.concat("/fans/fanslist");

    }

    public static String getUserRoom() {
        return IM_SERVER_URL.concat("/userroom/get");
    }

    public static String userRoomIn() {
        return IM_SERVER_URL.concat("/userroom/in");
    }

    public static String userRoomOut() {
        return IM_SERVER_URL.concat("/userroom/out");
    }

    /**
     * 房间标签列表
     */
    public static String getRoomTagList() {
        return IM_SERVER_URL.concat("/room/tag/all");
    }

    /**
     * 获取新标签列表
     *
     * @return
     */
    public static String getNewRoomTagList() {
        return IM_SERVER_URL.concat("/room/tag/listTag");
    }

    public static String openRoom() {
        return IM_SERVER_URL.concat("/room/open");
    }

    public static String getRoomInfo() {
        return IM_SERVER_URL.concat("/room/get");
    }

    /**
     * 只在创建房间页面调用，此接口只是拿房间信息，不会返回实名认证状态码
     *
     * @return
     */
    public static String getRoomInfoInCreateRoom() {
        return IM_SERVER_URL.concat("/room/roomInfo");
    }

    /**
     * 定时发送自己的房间数据 退出房间后不会发送 在房间和最小化都会发送
     *
     * @return
     */
    public static String roomStatistics() {
        return JAVA_WEB_URL.concat("/basicusers/record");
    }

    public static String updateRoomInfo() {
        return IM_SERVER_URL.concat("/room/update");
    }

    public static String updateByAdimin() {
        return IM_SERVER_URL.concat("/room/updateByAdmin");
    }

    public static String closeRoom() {
        return IM_SERVER_URL.concat("/room/close");
    }

    public static String getRoomConsumeList() {
        return IM_SERVER_URL.concat("/roomctrb/query");
    }

    public static String roomSearch() {
        return IM_SERVER_URL.concat("/search/room");
    }

    public static String getAuctionInfo() {
        return IM_SERVER_URL.concat("/auction/get");
    }

    public static String auctionStart() {
        return IM_SERVER_URL.concat("/auction/start");
    }

    //声网动态的appkey获取
    public static String getRoomAgoraKey() {
        return IM_SERVER_URL.concat("/agora/getKey");
    }

    /**
     * 用户参与竞拍报价
     *
     * @return
     */
    public static String auctionUp() {
        return IM_SERVER_URL.concat("/auctrival/up");
    }

    /**
     * 房主结束竞拍
     */
    public static String finishAuction() {
        return IM_SERVER_URL.concat("/auction/finish");
    }

    public static String weekAucionList() {
        return IM_SERVER_URL.concat("/weeklist/query");
    }

    public static String totalAuctionList() {
        return IM_SERVER_URL.concat("/sumlist/query");
    }

    public static String likeUser() {
        return IM_SERVER_URL.concat("/user/soundMatch/likeUser");
    }

    public static String unLikeUser() {
        return IM_SERVER_URL.concat("/user/soundMatch/unlikeUser");
    }

    public static String getListGift() {
        return IM_SERVER_URL.concat("/user/soundMatch/listGift");
    }

    public static String getRandomUser() {
        return IM_SERVER_URL.concat("/user/soundMatch/randomUser");
    }

    public static String audioMatchGetConfig() {
        return IM_SERVER_URL.concat("/user/soundMatch/getConfig");
    }

    public static String audioMatchSaveConfig() {
        return IM_SERVER_URL.concat("/user/soundMatch/saveConfig");
    }

    /**
     * 获取声鉴卡
     *
     * @return
     */
    public static String getAudioCard() {
        return IM_SERVER_URL.concat("/user/voiceCard/genCard");
    }

    public static String getRoomCharm() {
        return IM_SERVER_URL.concat("/userroom/getRoomCharm");
    }

    /**
     * 获取订单列表
     */
    public static String getOrderList() {
        return IM_SERVER_URL.concat("/order/list");
    }

    /**
     * 完成订单
     */
    public static String finishOrder() {
        return IM_SERVER_URL.concat("/order/finish");
    }

    /**
     * 修改相册顺序位置
     */
    public static String modifyAlbumPosition() {
        return IM_SERVER_URL.concat("/photo/sortPhoto");
    }

    /**
     * 获取指定订单
     */
    public static String getOrder() {
        return IM_SERVER_URL.concat("/order/get");
    }

    /**
     * 获取房间列表
     */
    public static String getRoomList() {
        return IM_SERVER_URL.concat("/home/get");

    }

    /**
     * 获取房间列表
     */
    public static String getRoomListV2() {
        return IM_SERVER_URL.concat("/home/getV2");

    }

    /**
     * 获取banner列表
     */
    public static String getBannerList() {
        return IM_SERVER_URL.concat("/banner/list");

    }

    /**
     * 获取声卡签名
     *
     * @return
     */
    public static String getAudioCardSignature() {
        return IM_SERVER_URL.concat("/user/voice/listDocs");

    }

    /**
     * 获取礼物列表
     */
    public static String getGiftList() {
        return IM_SERVER_URL.concat("/gift/listV3");
    }

    /**
     * 获取神秘礼物列表
     */
    public static String getMysteryGiftList() {
        return IM_SERVER_URL.concat("/gift/listMystic");
    }

    /**
     * 给用户送礼物--只扣金币
     *
     * @return
     */
    public static String sendToUserWithGold() {
        return IM_SERVER_URL.concat("/gift/gold/send");
    }

    /**
     * 给用户送礼物--只扣库存
     *
     * @return
     */
    public static String sendToUserWithRepertory() {
        return IM_SERVER_URL.concat("/gift/stock/send");
    }

    /**
     * 获取礼物背包(用户拥有的所有礼物)
     *
     * @return
     */
    public static String getBackpackGiftList() {
        return IM_SERVER_URL.concat("/gift/stock/list");
    }

    /**
     * 送礼物新接口
     *
     * @return
     */
    public static String sendGiftV3() {
        return IM_SERVER_URL.concat("/gift/sendV3");
    }

    public static String requestGiftCar() {
        return IM_SERVER_URL.concat("/giftCar/give");
    }

    public static String requestHeadWear() {
        return IM_SERVER_URL.concat("/headwear/give");
    }

    /**
     * 全麦送
     *
     * @return
     */
    public static String sendWholeGiftV3() {
        return IM_SERVER_URL.concat("/gift/sendWholeMicroV3");
    }

    /**
     * 获取表情列表
     */
    public static String getFaceList() {
        return IM_SERVER_URL.concat("/client/init");
    }

    /**
     * 客户端初始化
     *
     * @return --
     */
    public static String getInit() {
        return IM_SERVER_URL.concat("/client/init");
    }

    /**
     * 保存客户端设备激活
     *
     * @return
     */
    public static String getActivatingSave() {
        return IM_SERVER_URL.concat("/client/activating/save");
    }

    /**
     * 跟快手imei激活有关
     *
     * @return
     */
    public static String getKsActive() {
        return IM_SERVER_URL.concat("/channel/ad/ks/active");
    }

    /**
     * 获取版本号
     */
    public static String getVersions() {
        return IM_SERVER_URL.concat("/appstore/check");
    }

    /**
     * 获取钱包信息
     */
    public static String getWalletInfos() {
        return IM_SERVER_URL.concat("/purse/query");
    }

    /**
     * 获取充值产品列表
     */
    public static String getChargeList() {
        return IM_SERVER_URL.concat("/chargeprod/list");
    }

    /**
     * 发起充值
     */
    public static String requestCharge() {
        return IM_SERVER_URL.concat("/charge/apply");
    }

    /**
     * 发起充值 汇潮支付
     */
    public static String requestEcpssCharge() {
        return IM_SERVER_URL.concat("/charge/ecpss/alipay/apply");
    }

    /**
     * 发起充值 汇聚支付
     */
    public static String requestJoinPayCharge() {
        return IM_SERVER_URL.concat("/charge/joinpay/apply");
    }

    public static String requestCDKeyCharge() {
        return IM_SERVER_URL.concat("/redeemcode/use");
    }

    /**
     * 钻石兑换
     */
    public static String changeGold() {
        return IM_SERVER_URL.concat("/change/gold");
    }

    /**
     * 获取提现列表
     */
    public static String getWithdrawList() {
        return IM_SERVER_URL.concat("/withDraw/findList");
    }

    /**
     * 获取提现页用户信息
     */
    public static String getWithdrawInfo() {
        return IM_SERVER_URL.concat("/withDraw/exchange");
    }

    /**
     * 发起兑换
     */
    public static String requestExchange() {
        return IM_SERVER_URL.concat("/withDraw/withDrawCash");
    }

    /**
     * 用户绑定信息列表
     *
     * @return
     */
    public static String getBindingInfo() {
        return IM_SERVER_URL.concat("/withDraw/bound/list");
    }

    /**
     * 绑定第三方
     *
     * @return
     */
    public static String bindingThird() {
        return IM_SERVER_URL.concat("/withDraw/boundThird");
    }

    /**
     * 验证绑定第三方短信
     *
     * @return
     */
    public static String checkCode() {
        return IM_SERVER_URL.concat("/withDraw/checkCode");
    }

    /**
     * 解绑第三方
     *
     * @return
     */
    public static String unbindingThird() {
        return IM_SERVER_URL.concat("/withDraw/unBoundThird");
    }

    /**
     * 提前v2
     *
     * @return
     */
    public static String withdrawV2() {
        return IM_SERVER_URL.concat("/withDraw/v2/withDrawCash");
    }

    /**
     * 获取绑定支付宝验证码
     */
    public static String getSms() {
        return IM_SERVER_URL.concat("/withDraw/getSms");
    }

    /**
     * 获取绑定手机验证码
     */
    public static String getSmS() {
        return IM_SERVER_URL.concat("/withDraw/phoneCode");
    }


    /**
     * 获取绑定手机验证码
     */
    public static String getModifyPhoneSMS() {
        return IM_SERVER_URL.concat("/acc/sms");
    }

    /**
     * 绑定支付宝
     */

    public static String binder() {
        return IM_SERVER_URL.concat("/withDraw/bound");
    }

    /**
     * 绑定手机
     */
    public static String binderPhone() {
        return IM_SERVER_URL.concat("/withDraw/phone");
    }

    public static String modifyBinderPhone() {
        return IM_SERVER_URL.concat("/user/confirm");
    }

    public static String modifyBinderNewPhone() {
        return IM_SERVER_URL.concat("/user/replace");
    }


    /**
     * 提交反馈
     */
    public static String commitFeedback() {
        return IM_SERVER_URL.concat("/feedback");
    }

    /**
     * 微信登陆接口
     */
    public static String requestWXLogin() {
        return IM_SERVER_URL.concat("/acc/third/login");
    }

    /**
     * 获取order平均时长
     */
    public static String getAvgChattime() {
        return IM_SERVER_URL.concat("/basicorder/avgchattime");
    }

    /**
     * 是否绑定手机
     */
    public static String isPhones() {
        return IM_SERVER_URL.concat("/user/isBindPhone");
    }

    /**
     * 获取个人支出账单，包含礼物 充值 订单支出
     */
    public static String getAllBills() {
        return IM_SERVER_URL.concat("/personbill/list");
    }

    /**
     * 礼物，密聊，充值，提现账单查询（不包括红包）
     */
    public static String getBillRecord() {
        return IM_SERVER_URL.concat("/billrecord/get");
    }

    /**
     * 红包账单查询
     */
    public static String getPacketRecord() {
        return IM_SERVER_URL.concat("/packetrecord/get");
    }

    /**
     * 账单提现查询(红包提现)
     */
    public static String getPacketRecordDeposit() {
        return IM_SERVER_URL.concat("/packetrecord/deposit");
    }

    public static String getRedPacket() {
        return IM_SERVER_URL.concat("/statpacket/get");
    }


    public static String getShareRedPacket() {
        return IM_SERVER_URL.concat("/usershare/save");
    }

    /**
     * 是否第一次进入，获取红包弹窗
     */
    public static String getRedBagDialog() {
        return IM_SERVER_URL.concat("/packet/first");
    }

    /**
     * 获取红包弹窗活动类型
     */
    public static String getRedBagDialogType() {
        return IM_SERVER_URL.concat("/activity/query");
    }

    /**
     * 获取红包提现列表
     */
    public static String getRedBagList() {
        return IM_SERVER_URL.concat("/redpacket/list");
    }

    /**
     * 发起红包提现
     */
    public static String getRedWithdraw() {
        return IM_SERVER_URL.concat("/redpacket/withdraw");
    }

    /**
     * 红包提现列表
     */
    public static String getRedDrawList() {
        return IM_SERVER_URL.concat("/redpacket/drawlist");
    }

    /**
     * 首页排行列表
     */
    public static String getHomeRanking() {
        return IM_SERVER_URL.concat("/allrank/homeV2");
    }

    /**
     * 首页首页人气厅列表模块
     */
    public static String getHomePopularRoom() {
        return IM_SERVER_URL.concat("/home/v2/getindex");
    }

    /**
     * 获取首页tab数据
     */
    public static String getMainTabList() {
        return IM_SERVER_URL.concat("/room/tag/top");
//        return IM_SERVER_URL.concat("/mb/home/tag/list");
    }

    /**
     * 获取首页顶部标签
     *
     * @return
     */
    public static String getMainDataByMenu() {
        return IM_SERVER_URL.concat("/mb/home/tag/list");
    }

    /**
     * 获取首页热门数据
     */
    public static String getMainHotData() {
        return IM_SERVER_URL.concat("/mb/home/index");
    }

    /**
     * 通过tag获取首页各个tag数据
     */
    public static String getMainDataByTab() {
        return IM_SERVER_URL.concat("/mb/home/tag/listRoom");
    }

    /**
     * 获取节目预告和活位推荐
     */
    public static String getAdvert() {
        return IM_SERVER_URL.concat("/home/advert");
    }

    /**
     * 直播tab下banner接口
     */
    public static String getLiveBanner() {
        return IM_SERVER_URL.concat("/home/anchor/banner");
    }

    /**
     * 交友页信息
     */
    public static String getMakeFriendsList() {
        return IM_SERVER_URL.concat("/home/makeFriends");
    }

    /**
     * 男女推荐列表
     */
    public static String getListCharm() {
        return IM_SERVER_URL.concat("/home/listCharm");
    }

    /**
     * 麦上用户校验
     */
    public static String kickIllegal() {
        return IM_SERVER_URL.concat("/room/mic/v1/kickIllegal");
    }

    /**
     * 关注房间列表接口
     */
    public static String getAttentionListRoom() {
        return IM_SERVER_URL.concat("/fans/following/listRoom");
    }

    /**
     * 排行榜
     */
    public static String getRankingList() {
        return IM_SERVER_URL.concat("/allrank/geth5");
    }

    /**
     * 随机热门房间
     */
    public static String randomHotRoom() {
        return IM_SERVER_URL.concat("/home/random/hotRoom");
    }

    public static String chatHallQuestion() {
        return IM_SERVER_URL.concat("/mm/chatHallTips/index.html");
    }

    public static String getLotteryActivityPage() {
        return IM_SERVER_URL.concat("/mm/luckdraw/index.html");
    }

    /**
     * 查看新年福兽规则
     *
     * @return
     */
    public static String lookBlessingBeastRule() {
        return IM_SERVER_URL.concat("/mm/mascot/rule.html");
    }

    /**
     * 查询房间福兽信息
     *
     * @return
     */
    public static String queryRoomBlessingBeast() {
        return IM_SERVER_URL.concat("/activity/getMascot");
    }

    /**
     * 客户端二维码接口
     *
     * @return
     */
    public static String qrCodeScan() {
        return IM_SERVER_URL.concat("/acc/qrCode/scan");
    }

    public static String getConfigUrl() {
        return IM_SERVER_URL.concat("/client/configure");
    }

    /**
     * 查询歌曲是否存在
     *
     * @return
     */
    public static String getSongExist() {
        return IM_SERVER_URL.concat("/item/song/findSong");
    }

    /**
     * 上传歌曲
     *
     * @return
     */
    public static String getUploadSong() {
        return IM_SERVER_URL.concat("/item/song/v3/upload");
    }

    /**
     * 我的曲库列表
     *
     * @return
     */
    public static String getMySongList() {
        return IM_SERVER_URL.concat("/item/song/songList");
    }

    /**
     * 歌曲举报
     *
     * @return
     */
    public static String getSongReport() {
        return IM_SERVER_URL.concat("/item/song/report");
    }

    /**
     * 热门歌曲列表
     *
     * @return
     */
    public static String getHotSongList() {
        return IM_SERVER_URL.concat("/item/song/hotList");
    }

    /**
     * 删除歌曲
     *
     * @return
     */
    public static String getDelSong() {
        return IM_SERVER_URL.concat("/item/song/del");
    }

    /**
     * 添加热门歌曲
     *
     * @return
     */
    public static String getAddHotSong() {
        return IM_SERVER_URL.concat("/item/song/add");
    }


    /**
     * 举报接口
     *
     * @return
     */
    public static String reportUserUrl() {
        return IM_SERVER_URL.concat("/user/report/save");
    }

    /**
     * 敏感词
     *
     * @return
     */
    public static String getSensitiveWord() {
        return IM_SERVER_URL.concat("/sensitiveWord/regex");
    }

    /**
     * 禁言
     *
     * @return
     */
    public static String getBannedType() {
        return IM_SERVER_URL.concat("/banned/checkBanned");
    }

    /**
     * 获取砸蛋排行榜列表
     *
     * @return
     */
    public static String getPoundEggRank() {
        return IM_SERVER_URL.concat("/user/giftPurse/getRank");
    }

    /**
     * 获取砸蛋中奖记录
     *
     * @return
     */
    public static String getPoundEggRewordRecord() {
        return IM_SERVER_URL.concat("/user/giftPurse/record");
    }

    //发现 -- 活动列表
    public static String getFindInfo() {
        return JAVA_WEB_URL.concat("/advertise/getList");
    }

    /**
     * 锁麦，开麦操作
     *
     * @return
     */
    public static String operateMicroPhone() {
        return IM_SERVER_URL.concat("/room/mic/lockmic");
    }

    /**
     * 锁坑，开坑操作
     *
     * @return
     */
    public static String getlockMicroPhone() {
        return IM_SERVER_URL.concat("/room/mic/lockpos");
    }

    /**
     * 任务列表
     *
     * @return
     */
    public static String getTaskList() {
        return IM_SERVER_URL.concat("/duty/list");
    }

    /**
     * 领取任务奖励
     *
     * @return
     */
    public static String getTaskReward() {
        return IM_SERVER_URL.concat("/duty/achieve");
    }

    public static String reportPublic() {
        return IM_SERVER_URL.concat("/duty/fresh/public");
    }

    /**
     * 获取房间速配活动状态
     *
     * @return
     */
    public static String getRoomMatch() {
        return IM_SERVER_URL.concat("/room/game/getState");
    }

    /**
     * 提交房间速配活动确认选择结果
     *
     * @return
     */
    public static String postRoomMatchChoose() {
        return IM_SERVER_URL.concat("/room/game/choose");
    }

    /**
     * 提交房间速配活动显示的结果
     *
     * @return
     */
    public static String postRoomMatchConfirm() {
        return IM_SERVER_URL.concat("/room/game/confirm");
    }

    /**
     * 新手推荐弹框
     *
     * @return
     */
    public static String getNewUserRecommend() {
        return IM_SERVER_URL.concat("/room/rcmd/get");
    }

    /***
     * 萌新列表
     * @return
     */
    public static String getMengXin() {
        return IM_SERVER_URL.concat("/user/newUserList");
    }


    /**
     * PK活动接口模块
     *
     * @return
     */
    //发起保存一个PK
    public static String savePk() {
        return JAVA_WEB_URL.concat("/room/pkvote/save");
    }

    //取消一个PK
    public static String cancelPk() {
        return JAVA_WEB_URL.concat("/room/pkvote/cancel");
    }

    //获取一个PK结果
    public static String getPkResult() {
        return JAVA_WEB_URL.concat("/room/pkvote/get");
    }

    //获取PK历史
    public static String getPkHistoryList() {
        return JAVA_WEB_URL.concat("/room/pkvote/list");
    }

    //取消一个PK
    public static String sendPkVote() {
        return JAVA_WEB_URL.concat("/room/pkvote/vote");
    }

    // ---------------------------------------------------房间相关接口 ---------------------------------------

    //房间背景图片列表
    public static String getRoomBackList() {
        return JAVA_WEB_URL.concat("/room/bg/list");
    }


    // --------------------------------------------------- 消息相关接口 --------------------------------------

    //获取免打扰状态
    public static String getFocusMsgSwitch() {
        return JAVA_WEB_URL.concat("/user/setting/v1/get");
    }

    //保存免打扰状态
    public static String saveFocusMsgSwitch() {
        return JAVA_WEB_URL.concat("/user/setting/v1/save");
    }


    //----------------------------------------------------- 装扮商城相关接口 -----------------------------------
    //头饰接口
    public static String getHeadWearList() {
        return JAVA_WEB_URL.concat("/headwear/listMall");
    }

    //我的头饰接口
    public static String getMyHeadWearList() {
        return JAVA_WEB_URL.concat("/headwear/user/list");
    }

    //购买头饰接口
    public static String purseHeadWear() {
        return JAVA_WEB_URL.concat("/headwear/purse");
    }

    //改变头饰使用状态
    public static String changeHeadWearState() {
        return JAVA_WEB_URL.concat("/headwear/use");
    }

    //座驾接口
    public static String getCarList() {
        return JAVA_WEB_URL.concat("/giftCar/listMall");
    }

    //我的座驾接口
    public static String getMyCarList() {
        return JAVA_WEB_URL.concat("/giftCar/user/list");
    }

    //购买座驾
    public static String purseCar() {
        return JAVA_WEB_URL.concat("/giftCar/purse");
    }

    //改变座驾使用状态
    public static String changeCarState() {
        return JAVA_WEB_URL.concat("/giftCar/use");
    }


    //-------------------------------------------------- 客户端配置相关接口 ---------------------------------
    //获取审核模式状态
    public static String getClientChannel() {
        return JAVA_WEB_URL.concat("/client/channel");
    }

    //获取日志地址
    public static String getClientLogFile() {
        return JAVA_WEB_URL.concat("/client/log/save");
    }

    //上报安全检测的结果
    public static String reportSafetyCheckResult() {
        return JAVA_WEB_URL.concat("/client/security/saveInfo");
    }

    public static String getRealNameAuthUrl() {
        return JAVA_WEB_URL.concat("/mm/real_name/index.html");
    }

    /**
     * 暴走大转盘
     *
     * @return
     */
    public static String getSlyderUrl() {
        return JAVA_WEB_URL.concat("/mm/newluckdraw/index.html");
    }

    /**
     * CP榜单页H5
     *
     * @return
     */
    public static String getCpRankingList() {
        return JAVA_WEB_URL.concat("/mm/rank_cp/index.html");
    }

    /**
     * CP规则H5
     *
     * @return
     */
    public static String getCpRule() {
        return JAVA_WEB_URL.concat("/mm/rank_cp/rule.html");
    }

    /**
     * cp戒指说明H5
     *
     * @return
     */
    public static String getCpRingDesH5() {
        return JAVA_WEB_URL.concat("/mm/cp_rule/index.html");
    }

    /**
     * 一周cp玩法说明H5
     * FIXME 因为线下http://beta.miaomiaofm.com/mm/rank_cp/room_rule.html无法访问 以后可以访问了改为JAVA_WEB_URL.concat地址
     *
     * @return
     */
    public static String getWeekCpDesH5() {
//        return "https://www.miaomiaofm.com/mm/rank_cp/room_rule.html";
        return JAVA_WEB_URL.concat("/mm/rank_cp/room_rule.html");
    }

    public static String getMyIncomeH5() {
        return JAVA_WEB_URL.concat("/mm/cash/withdrawal.html");
    }


    //-------------------------------------------------- Cp相关接口 ---------------------------------
    //同意成为cp
    public static String cpAgree() {
        return JAVA_WEB_URL.concat("/user/cp/agree");
    }

    //查询某个玩家cp信息
    public static String cpCheck() {
        return JAVA_WEB_URL.concat("/user/cp/check");
    }

    //拒绝成为cp
    public static String cpRefuse() {
        return JAVA_WEB_URL.concat("/user/cp/refuse");
    }

    //CP请求的状态查询
    public static String getCpStatus() {
        return JAVA_WEB_URL.concat("/user/cp/status");
    }

    //解除cp
    public static String cpUnband() {
        return JAVA_WEB_URL.concat("/user/cp/unband");
    }

    // 上麦获取心动值
    public static String getCpHeart() {
        return JAVA_WEB_URL.concat("/user/cp/heart");
    }

    // 心动值校验
    public static String getCpHeartCheck() {
        return JAVA_WEB_URL.concat("/user/cp/heartCheck");
    }

    // 增加心动值
    public static String getCpValue() {
        return JAVA_WEB_URL.concat("/user/cp/addCpValue");
    }

    // 一周CP
    public static String getWeekCp() {
        return JAVA_WEB_URL.concat("/user/cp/weekCp");
    }

    // 解除一周CP
    public static String getUnbandWeekCp() {
        return JAVA_WEB_URL.concat("/user/cp/unband");
    }

    // 一周CP排行榜
    public static String getWeekCpRank() {
        return JAVA_WEB_URL.concat("/user/cp/weekrank");
    }

    // 应答默契考验
    public static String getAnswerAsk() {
        return JAVA_WEB_URL.concat("/user/cp/answerTacit");
    }

    // 趣味问答题目获取
    public static String getAskQuestion() {
        return JAVA_WEB_URL.concat("/user/cp/ask");
    }

    // 发起默契考验
    public static String getAskTacit() {
        return JAVA_WEB_URL.concat("/user/cp/askTacit");
    }

    // 默契考验提交
    public static String getTacitAnswerCollect() {
        return JAVA_WEB_URL.concat("/user/cp/tacitAnswerCollect");
    }

    // 发起玩法增加心动值
    public static String getAddForRoomPlay() {
        return JAVA_WEB_URL.concat("/user/cp/roomPlay");
    }

    public static String getChatHallH5() {
        return JAVA_WEB_URL.concat("/mm/chatHallTips/index.html");
    }

    // 微信绑定
    public static String getWithdrawal() {
        return JAVA_WEB_URL.concat("/mm/cash/withdrawal.html");
    }

    //-------------------------------------------------- 寻友广播相关接口 ---------------------------------
    //寻友广播列表
    public static String getFindFriendsBroadcastList() {
        return JAVA_WEB_URL.concat("/room/chat/square/listMsg");
    }

    //发布广场消息
    public static String getFindFriendsBroadcastPushMsg() {
        return JAVA_WEB_URL.concat("/room/chat/square/pushMsg");
    }

    //获取发布消息限制的过期时间
    public static String getFindFriendsBroadcastLimitExpired() {
        return JAVA_WEB_URL.concat("/room/chat/square/getLimitExpired");
    }
    //-------------------------------------------------- 寻友广播相关接口 ---------------------------------

    //通知消息接口
    public static String getNoticeList() {
        return JAVA_WEB_URL.concat("/notice/get");
    }

    //一键通知粉丝
    public static String getRoomNoticeFans() {
        return JAVA_WEB_URL.concat("/room/openRoomNoticeFans");
    }

    //房间标题列表
    public static String getListRoomTitle() {
        return JAVA_WEB_URL.concat("/room/listRoomTitle");
    }

    //一键通知粉丝CD
    public static String getRoomNoticeFansCd() {
        return JAVA_WEB_URL.concat("/room/noticeFansCd");
    }

    /**
     * 消息设置页面 通知过滤
     *
     * @return
     */
    public static String getSetNoticeOption() {
        return JAVA_WEB_URL.concat("/user/setNoticeOption");
    }
}
