package com.tongdaxing.xchat_core.order;

import android.util.Log;

import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.OrderListResult;
import com.tongdaxing.xchat_core.result.OrderResult;
import com.tongdaxing.xchat_core.utils.Logger;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;

/**
 * Created by zhouxiangfeng on 2017/6/15.
 */

public class OrderCoreImpl extends AbstractBaseCore implements IOrderCore {

    private static final String TAG = "OrderCoreImpl";

    @Override
    //
    public void getOrderList(long uid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("type", String.valueOf(1));


        ResponseListener listener = new ResponseListener<OrderListResult>() {
            @Override
            public void onResponse(OrderListResult response) {
                if (null != response) {
                    Log.i(TAG, "onResponse: !null");
                    if (response.isSuccess()) {
                        Log.i(TAG, "onResponse:走了成功 ");
                        notifyClients(IOrderCoreClient.class,IOrderCoreClient.METHOD_GET_ORDER_LIST,response.getData());
                        //Log.i("zsh", "onResponse:"+response.getData().toString());
                    } else {
                        notifyClients(IOrderCoreClient.class, IOrderCoreClient.METHOD_GET_ORDER_LIST_ERROR, response.getMessage());
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IOrderCoreClient.class, IOrderCoreClient.METHOD_GET_ORDER_LIST_ERROR, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getOrderList(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        OrderListResult.class, Request.Method.GET);
    }

    @Override
    //获取指定订单
    public void getOrderById(int orderId) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("orderId", String.valueOf(orderId));
        ResponseListener listener = new ResponseListener<OrderResult>() {
            @Override
            public void onResponse(OrderResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IOrderCoreClient.class,IOrderCoreClient.METHOD_GET_ORDER,response.getData());
                    } else {
                        notifyClients(IOrderCoreClient.class, IOrderCoreClient.METHOD_GET_ORDER_ERROR, response.getMessage());
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IOrderCoreClient.class, IOrderCoreClient.METHOD_GET_ORDER_ERROR, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getOrder(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        OrderResult.class, Request.Method.GET);
    }



    //完成订单
    @Override
    public void finishOrder(int orderId, long uid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(uid));
        params.put("orderId", String.valueOf(orderId));
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IOrderCoreClient.class,IOrderCoreClient.METHOD_FINISH_ORDER);
                    } else {
                        notifyClients(IOrderCoreClient.class, IOrderCoreClient.METHOD_FINISH_ORDER_ERROR, response.getMessage());
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IOrderCoreClient.class, IOrderCoreClient.METHOD_FINISH_ORDER_ERROR, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.finishOrder(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }
}
