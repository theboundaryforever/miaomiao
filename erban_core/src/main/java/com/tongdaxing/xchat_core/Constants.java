package com.tongdaxing.xchat_core;

import java.io.File;

/**
 * <p> 常量集合 </p>
 * Created by Administrator on 2017/11/9.
 */
public class Constants {
    /**
     * 商城菜单标签
     */
    public static final String MALL_MENU_LABEL = "mallMenuLabelPic";
    public static boolean IS_SHOW_WINNING_MSG = false;//默认显示中奖消息
    public static final int PUSH_DEFAULT = 500;
    public static final int PAY_CHANNEL_DEFAULT = 2;
    public static final int PAY_CHANNEL_ECPSS = 3;  // 汇潮支付
    public static int FULL_PUSH_MAX = PUSH_DEFAULT;//从/client/configure接口获取
    public static final String CHARGE_JOIN_PAY_WX = "WEIXIN_APP";
    public static final String ERBAN_DIR_NAME = "com.miaomiao.mobile";
    public static final String nimAppKey = "801631bce2ff7bb299f7edde56bbbd1a";
    public static final String nimAppSecret = "3ba6bccf4e2d";

    public static int PAY_ALIPAY_CHANNEL = 0;//从/client/configure 支付宝渠道: 默认是p++ 3是汇潮


    public static String WX_APPID = "wxaf8731ed73d6bf3d";//微信appid
    /**
     * 百度统计
     */
    public static final String BAIDU_APPKEY = "e1afec7406";

    public static final String LOG_DIR = ERBAN_DIR_NAME + File.separator + "logs";
    public static final String CONFIG_DIR = ERBAN_DIR_NAME + File.separator + "config";
    public static final String VOICE_DIR = ERBAN_DIR_NAME + File.separator + "voice";
    public static final String CACHE_DIR = ERBAN_DIR_NAME + File.separator + "cache";
    public static final String HTTP_CACHE_DIR = ERBAN_DIR_NAME + File.separator + "http";
    public static final String IMAGE_CACHE_DIR = ERBAN_DIR_NAME + File.separator + "image";


    public static final String KEY_MAIN_POSITION = "key_main_position";

    public static final int RESULT_OK = 200;

    public static final int PAGE_START = 1;
    public static final int PAGE_SIZE = 10;
    public static final int PAGE_HOME_HOT_SIZE = 20;
    public static final int BILL_PAGE_SIZE = 50;


    public static final String HOME_TAB_INFO = "home_tab_info";
    public static final String KEY_USER_INFO = "key_user_info";

    public static final String KEY_HOME_HOT_LIST = "key_home_hot_list";
    public static final String KEY_HOME_NO_HOT_LIST = "key_home_no_hot_list";

    public static final String KEY_MAKE_FRIENDS_LIST = "key_make_friends_list";

    public static final String KEY_FIRST_PAGE_LIST = "key_first_page_list";

    public static final int FAN_MAIN_PAGE_TYPE = 100;
    public static final int FAN_NO_MAIN_PAGE_TYPE = 101;
    public static final String KEY_PAGE_TYPE = "page_type";
    public static final String KEY_MAIN_TAB_LIST = "main_tab_list";

    public static final String KEY_POSITION = "position";


    public static final String CHARGE_WX = "wx";
    public static final String CHARGE_ALIPAY = "alipay";
    public static int PAY_CHANNEL = PAY_CHANNEL_DEFAULT;//从/client/configure接口获取 1 是ping++ 2 是汇聚

    public static final int PAGE_TYPE_AV_ROOM_ACTIVITY = 100;
    public static final int PAGE_TYPE_USER_INFO_ACTIVITY = 101;
    public static final java.lang.String KEY_ROOM_IS_SHOW_ONLINE = "is_show_online";
    public static final String KEY_ROOM_INFO = "key_room_info";

    /**
     * 房间相关Key设置
     */
    public static final String ROOM_UPDATE_KEY_POSTION = "micPosition";
    public static final String ROOM_UPDATE_KEY_UID = "micUid";
    public static final String ROOM_UPDATE_KEY_GENDER = "gender";

    public static final String KEY_CHAT_ROOM_INFO_ROOM = "roomInfo";
    public static final String KEY_CHAT_ROOM_INFO_MIC = "micQueue";

    public static final String ROOM_UID = "ROOM_UID";
    public static final String ROOM_TYPE = "ROOM_TYPE";
    public static final String ROOM_HER_USER_INFO = "roomHerUserInfo";//去找TA时，TA的资料

    public static final String TYPE = "TYPE";

    //大礼物
    public static final int SUPER_GIFT_SIZE = 999;

    public static boolean isNeedJoin = false;

    //push data
    public static final String PUSH_SKIP_EXTRA = "PUSH_SKIP_EXTRA";

}
