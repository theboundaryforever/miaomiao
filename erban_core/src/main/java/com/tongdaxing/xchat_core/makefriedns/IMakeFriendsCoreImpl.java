package com.tongdaxing.xchat_core.makefriedns;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.MakeFriendsListInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

public class IMakeFriendsCoreImpl extends AbstractBaseCore implements IMakeFriendsCore {

    @Override
    public void getMakeFriendsList(final int pageNum, int pageSize) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("pageNum", String.valueOf(pageNum));
        requestParam.put("pageSize", String.valueOf(pageSize));

        OkHttpManager.getInstance().doGetRequest(UriProvider.getMakeFriendsList(), requestParam, new OkHttpManager.MyCallBack<ServiceResult<MakeFriendsListInfo>>() {
            @Override
            public void onError(Exception e) {
                notifyClients(IMakeFriendsCoreClient.class, IMakeFriendsCoreClient.METHOD_ON_GET_MAKEFRIEDNS_LIST_FAIL, "加载失败", pageNum);
            }

            @Override
            public void onResponse(ServiceResult<MakeFriendsListInfo> response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IMakeFriendsCoreClient.class, IMakeFriendsCoreClient.METHOD_ON_GET_MAKEFRIEDNS_LIST, response.getData(), pageNum);
                    } else {
                        notifyClients(IMakeFriendsCoreClient.class, IMakeFriendsCoreClient.METHOD_ON_GET_MAKEFRIEDNS_LIST_FAIL, response.getMessage(), pageNum);
                    }
                }
            }
        });
    }
}
