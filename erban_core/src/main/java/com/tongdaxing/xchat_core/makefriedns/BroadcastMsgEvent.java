package com.tongdaxing.xchat_core.makefriedns;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.xchat_core.im.custom.bean.FriendsBroadcastAttachment;

import java.util.List;

/**
 * <p>
 * Created by zhangjian on 2019/6/20.
 */
public class BroadcastMsgEvent {
    public static class OnReceiveNewMsg {
        private ChatRoomMessage chatRoomMessage;
        private List<FriendsBroadcastAttachment> attachments;

        public OnReceiveNewMsg(List<FriendsBroadcastAttachment> attachments) {
            this.attachments = attachments;
        }

        public OnReceiveNewMsg(ChatRoomMessage object) {
            this.chatRoomMessage = object;
        }

        public ChatRoomMessage getChatRoomMessage() {
            return chatRoomMessage;
        }

        public void setChatRoomMessage(ChatRoomMessage chatRoomMessage) {
            this.chatRoomMessage = chatRoomMessage;
        }

        public List<FriendsBroadcastAttachment> getAttachments() {
            return attachments;
        }

        @Override
        public String toString() {
            return "OnReceiveNewMsg{" +
                    "chatRoomMessage=" + chatRoomMessage +
                    '}';
        }
    }

    /**
     * 发送寻友广播等待时间
     */
    public static class OnMsgSendCD {
        private int waitingTime;

        public OnMsgSendCD(int waitingTime) {
            this.waitingTime = waitingTime;
        }

        public int getWaitingTime() {
            return waitingTime;
        }
    }

    public static class OnBroadcastSendDialogOpen {

    }

    public static class OnBroadcastSendDialogNeedDismiss {

    }

    public static class OnBroadcastSendDialogSend {
        private String content;

        public OnBroadcastSendDialogSend(String content) {

            this.content = content;
        }

        public String getContent() {
            return content;
        }
    }
}
