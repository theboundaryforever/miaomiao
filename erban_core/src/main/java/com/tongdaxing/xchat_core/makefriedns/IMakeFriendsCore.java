package com.tongdaxing.xchat_core.makefriedns;

import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

public interface IMakeFriendsCore extends IBaseCore {

    void getMakeFriendsList(int pageNum, int pageSize);
}
