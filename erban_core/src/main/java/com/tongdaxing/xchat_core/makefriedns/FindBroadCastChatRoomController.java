package com.tongdaxing.xchat_core.makefriedns;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.QueryDirectionEnum;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.FriendsBroadcastAttachment;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.LogUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2018/3/20.
 */

public class FindBroadCastChatRoomController extends AbstractBaseCore {
    public static final int MAX_MSG_SIZE = 50;

    public static long devRoomId = 109213253;//线下
    public static long formalRoomId = 110062050;//线上
    public static boolean mIsEnterRoom;
    private String TAG = "FindBroadCastChatRoomController";
    private long roomId = formalRoomId;

    public FindBroadCastChatRoomController() {
        if (BasicConfig.isDebug) {
            roomId = devRoomId;
        }
    }

    public void enterRoom(final ActionCallBack actionCallBack) {
        //适配云信乱发下麦消息的坑
        final EnterChatRoomData data = new EnterChatRoomData(String.valueOf(roomId));
        NIMClient.getService(ChatRoomService.class).enterChatRoomEx(data, 3).setCallback(new RequestCallback<EnterChatRoomResultData>() {
            @Override
            public void onSuccess(EnterChatRoomResultData result) {
                actionCallBack.success();
                // 登录成功
                MsgTypeEnum[] typeEnums = new MsgTypeEnum[]{MsgTypeEnum.custom};
                NIMClient.getService(ChatRoomService.class).pullMessageHistoryExType(roomId + "", System.currentTimeMillis(), MAX_MSG_SIZE, QueryDirectionEnum.QUERY_OLD, typeEnums).setCallback(new RequestCallback<List<ChatRoomMessage>>() {
                    @Override
                    public void onSuccess(List<ChatRoomMessage> param) {
                        int size = param.size();
                        L.info(TAG, "enterChatRoomEx() onSuccess() param.size() = %d", size);
                        mIsEnterRoom = true;
                        List<FriendsBroadcastAttachment> temp = new ArrayList<>();
                        if (size >= MAX_MSG_SIZE) {
                            size = MAX_MSG_SIZE;//最多只取50条消息
                        }
                        for (int i = 0; i < size; i++) {

                            LogUtil.d("房间信息拦截", "sessionId----- " + param.get(i).getSessionId());
                            ChatRoomMessage chatRoomMessage = param.get(i);
                            MsgAttachment attachment = chatRoomMessage.getAttachment();

                            if (attachment instanceof CustomAttachment) {
                                if (((CustomAttachment) attachment).getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_FIND_FRIENDS_BROADCAST_MSG) {
                                    //寻友广播消息
                                    temp.add((FriendsBroadcastAttachment) attachment);
                                }
                            }
                        }
                        onGetRoomMessageSuccess(temp);
                    }

                    @Override
                    public void onFailed(int code) {
                        L.error(TAG, "enterChatRoomEx() error() code = %d", code);
                        mIsEnterRoom = false;
                    }

                    @Override
                    public void onException(Throwable exception) {
                        L.error(TAG, "enterNeteaseRoom() error() exception: ", exception);
                        mIsEnterRoom = false;
                    }
                });
            }

            @Override
            public void onFailed(int code) {
                // 登录失败
                actionCallBack.error(code);
                mIsEnterRoom = false;
            }

            @Override
            public void onException(Throwable exception) {
                // 错误
                actionCallBack.error(exception);
                mIsEnterRoom = false;
            }
        });
    }


    public void leaveRoom() {
        LogUtils.d("nim_sdk", "exitChatRoom_3");
        NIMChatRoomSDK.getChatRoomService().exitChatRoom(roomId + "");
        mIsEnterRoom = false;
    }

    private void onGetRoomMessageSuccess(List<FriendsBroadcastAttachment> chatRoomMessages) {
        CoreUtils.send(new BroadcastMsgEvent.OnReceiveNewMsg(chatRoomMessages));
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId() {
        if (BasicConfig.isDebug) {
            roomId = devRoomId;
        } else {
            roomId = formalRoomId;
        }
    }

    public interface ActionCallBack {
        void success();

        void error(int code);

        void error(Throwable exception);
    }
}
