package com.tongdaxing.xchat_core.makefriedns;

import com.tongdaxing.xchat_core.bean.MakeFriendsListInfo;
import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

public interface IMakeFriendsCoreClient extends ICoreClient {

    String METHOD_ON_GET_MAKEFRIEDNS_LIST = "onGetMakeFriendsList";
    String METHOD_ON_GET_MAKEFRIEDNS_LIST_FAIL = "onGetMakeFriendsListFail";

    void onGetMakeFriendsList(MakeFriendsListInfo makeFriendsList, int pageNum);

    void onGetMakeFriendsListFail(String msg, int pageNum);
}
