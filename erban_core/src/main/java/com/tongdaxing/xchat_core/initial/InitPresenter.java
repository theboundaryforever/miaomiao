package com.tongdaxing.xchat_core.initial;


import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * @author xiaoyu
 * @date 2017/12/29
 */
public class InitPresenter extends AbstractMvpPresenter<IInitView> {

    private InitModel model;

    public InitPresenter() {
        model = InitModel.get();
    }

    public InitInfo getLocalSplashVo() {
        return model.getCacheInitInfo();
    }

    public void init() {
        model.init();
    }

    public void activatingSave() {
        model.activatingSave(new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response.isSuccess() && getMvpView() != null) {
                    getMvpView().activatingSaveSuccess();
                }
            }
        });
    }
}
