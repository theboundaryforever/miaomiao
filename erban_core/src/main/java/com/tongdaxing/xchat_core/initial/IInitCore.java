package com.tongdaxing.xchat_core.initial;

import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * @author xiaoyu
 * @date 2017/12/8
 */

public interface IInitCore extends IBaseCore {
//    /**
//     * app全局初始化
//     * 暂时,初始化的时候,请求的只有表情,,,
//     *
//     * @param force 是否强制重新请求
//     */
//    void init(boolean force);
}
