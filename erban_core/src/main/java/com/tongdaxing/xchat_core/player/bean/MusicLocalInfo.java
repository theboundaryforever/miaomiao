package com.tongdaxing.xchat_core.player.bean;

import com.tongdaxing.xchat_core.utils.StringUtils;
import com.tongdaxing.xchat_framework.util.util.json.JsonParser;
import com.tongdaxing.xchat_framework.util.util.valid.BlankUtil;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by chenran on 2017/10/28.
 */

public class MusicLocalInfo extends RealmObject implements Serializable {
    private String songId;

    private String songName;

    private String albumId;

    private String albumIndex;

    private String albumName;

    private String artistIdsJson;

    private String artistIndex;

    private String artistNamesJson;

    private String remoteUri;

    @PrimaryKey
    private String localUri;

    private String quality;

    private String year;

    private long duration;

    private boolean deleted;

    private boolean isInPlayerList;

    private long fileSize;

    private String lyricUrl;

    private String songAlbumCover;

    private String singerName;//歌手名称

    //是否是在缓存中
    private boolean isOnCache;

    public boolean isOnCache() {
        return isOnCache;
    }

    private int fileStatus;//0是可上传，1可播放，2是暂停,3是正在上传，4无状态。此字段与服务器无关。

    public int getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(int fileStatus) {
        this.fileStatus = fileStatus;
    }

    public void setOnCache(boolean onCache) {
        isOnCache = onCache;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public String getAlbumIndex() {
        return albumIndex;
    }

    public void setAlbumIndex(String albumIndex) {
        this.albumIndex = albumIndex;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getArtistIdsJson() {
        return artistIdsJson;
    }

    public void setArtistIdsJson(String artistIdsJson) {
        this.artistIdsJson = artistIdsJson;
    }

    public String getArtistIndex() {
        return artistIndex;
    }

    public void setArtistIndex(String artistIndex) {
        this.artistIndex = artistIndex;
    }

    public String getArtistNamesJson() {
        return artistNamesJson;
    }

    public void setArtistNamesJson(String artistNamesJson) {
        this.artistNamesJson = artistNamesJson;
    }

    public String getRemoteUri() {
        return remoteUri;
    }

    public void setRemoteUri(String remoteUri) {
        this.remoteUri = remoteUri;
    }

    public String getLocalUri() {
        return localUri;
    }

    public void setLocalUri(String localUri) {
        this.localUri = localUri;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInPlayerList() {
        return isInPlayerList;
    }

    public void setInPlayerList(boolean inPlayerList) {
        isInPlayerList = inPlayerList;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getLyricUrl() {
        return lyricUrl;
    }

    public void setLyricUrl(String lyricUrl) {
        this.lyricUrl = lyricUrl;
    }

    public String getSongAlbumCover() {
        return songAlbumCover;
    }

    public void setSongAlbumCover(String songAlbumCover) {
        this.songAlbumCover = songAlbumCover;
    }

    public List<String> getArtistNames() {
        if (null != artistNamesJson) {
            return JsonParser.parseJsonList(artistNamesJson, String.class);
        }

        return null;
    }

    public void setArtistName(List<String> artistNames) {
        if (!BlankUtil.isBlank(artistNames)) {
            this.artistNamesJson = JsonParser.toJson(artistNames);
        }
    }

    @Override
    public String toString() {
        return "MusicLocalInfo{" +
                "songId='" + songId + '\'' +
                ", songName='" + songName + '\'' +
                ", albumId='" + albumId + '\'' +
                ", albumIndex='" + albumIndex + '\'' +
                ", albumName='" + albumName + '\'' +
                ", artistIdsJson='" + artistIdsJson + '\'' +
                ", artistIndex='" + artistIndex + '\'' +
                ", artistNamesJson='" + artistNamesJson + '\'' +
                ", remoteUri='" + remoteUri + '\'' +
                ", localUri='" + localUri + '\'' +
                ", quality='" + quality + '\'' +
                ", year='" + year + '\'' +
                ", duration=" + duration +
                ", deleted=" + deleted +
                ", isInPlayerList=" + isInPlayerList +
                ", fileSize=" + fileSize +
                ", lyricUrl='" + lyricUrl + '\'' +
                ", songAlbumCover='" + songAlbumCover + '\'' +
                ", singerName='" + singerName + '\'' +
                ", isOnCache=" + isOnCache +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MusicLocalInfo) {
            MusicLocalInfo musicInfo = (MusicLocalInfo) obj;
            //匹配三个规则：歌曲名，歌手名，文件大小
            return StringUtils.equals(musicInfo.getSongName(), getSongName()) &&
                    StringUtils.equals(musicInfo.getSingerName(), getSingerName()) && musicInfo.getFileSize() == getFileSize();
        }
        return super.equals(obj);
    }

    public void copy(MusicLocalInfo musicLocalInfo){
        setLocalUri(musicLocalInfo.getLocalUri());
        setOnCache(musicLocalInfo.isOnCache);
        setFileSize(musicLocalInfo.getFileSize());
        setSingerName(musicLocalInfo.getSingerName());
        setAlbumId(musicLocalInfo.getAlbumId());
        setAlbumIndex(musicLocalInfo.getAlbumIndex());
        setAlbumName(musicLocalInfo.getAlbumName());
        setArtistIdsJson(musicLocalInfo.getArtistIdsJson());
        setArtistIndex(musicLocalInfo.getArtistIndex());
        setArtistName(musicLocalInfo.getArtistNames());
        setArtistNamesJson(musicLocalInfo.getArtistNamesJson());
        setDeleted(musicLocalInfo.isDeleted());
        setDuration(musicLocalInfo.getDuration());
        setInPlayerList(musicLocalInfo.isInPlayerList());
        setLyricUrl(musicLocalInfo.getLyricUrl());
        setQuality(musicLocalInfo.getQuality());
        setRemoteUri(musicLocalInfo.getRemoteUri());
        setSongAlbumCover(musicLocalInfo.getSongAlbumCover());
        setSongId(musicLocalInfo.getSongId());
        setSongName(musicLocalInfo.getSongName());
        setYear(musicLocalInfo.getYear());
    }
}

