package com.tongdaxing.xchat_core.player;

import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

import java.util.List;

/**
 * Created by chenran on 2017/10/28.
 */

public interface IPlayerCoreClient extends ICoreClient {
    public static final String METHOD_ON_REFRESH_PLAY_MODE = "onRefreshPlayMode";
    public static final String METHOD_ON_REFRESH_LOCAL_MUSIC = "onRefreshLocalMusic";
    public static final String METHOD_ON_REFRESH_LOCAL_MUSIC_PROGRESS = "onRefreshLocalMusicProgress";
    public static final String METHOD_ON_REFRESH_PLAYER_LIST = "onRefreshPlayerList";
    public static final String METHOD_ON_MUSIC_PLAYING = "onMusicPlaying";
    public static final String METHOD_ON_MUSIC_PAUSE = "onMusicPause";
    public static final String METHOD_ON_MUSIC_STOP = "onMusicStop";
    public static final String METHOD_ON_CURRENT_MUSIC_UPDATE = "onCurrentMusicUpdate";
    public static final String METHOD_ON_MUSIC_PROGRESS_UPDATE = "onMusicProgressUpdate";
    String MUSIC_UPLOAD_SUCCESS = "musicUploadSuccess";
    String MUSIC_UPLOAD_FAILURE = "musicUploadFailure";

    void onRefreshPlayMode(int playMode);

    void musicUploadSuccess(MusicLocalInfo musicLocalInfo);

    void musicUploadFailure(MusicLocalInfo musicLocalInfo);

    void onRefreshLocalMusicProgress(int progress);

    void onRefreshLocalMusic(List<MusicLocalInfo> localMusicInfoList);

    void onRefreshPlayerList(List<MusicLocalInfo> playerListMusicInfoList);

    void onMusicPlaying(MusicLocalInfo localMusicInfo);

    void onMusicPause(MusicLocalInfo localMusicInfo);

    void onMusicStop(MusicLocalInfo localMusicInfo);

    void onCurrentMusicUpdate(MusicLocalInfo localMusicInfo);

    void onMusicProgressUpdate(long total, long current);
}
