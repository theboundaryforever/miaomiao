package com.tongdaxing.xchat_core.player;

import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by chenran on 2017/10/31.
 */

public class PlayerDbCoreImpl extends AbstractBaseCore implements IPlayerDbCore {
    private Realm mRealm;

    public PlayerDbCoreImpl() {
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public MusicLocalInfo requestLocalMusicInfoByLocalUri(String localUri) {
        return mRealm.where(MusicLocalInfo.class).equalTo("localUri", localUri).findFirst();
    }

    @Override
    public MusicLocalInfo requestLocalMusicInfo(String songName, String singerName, long fileSize, boolean isOnCache) {
        return mRealm.where(MusicLocalInfo.class)
                .equalTo("songName", songName).equalTo("singerName", singerName).equalTo("fileSize", fileSize).equalTo("isOnCache", isOnCache)
                .findFirst();
    }

    @Override
    public RealmResults<MusicLocalInfo> requestLocalMusicInfoAll(String songName, String singerName, long fileSize, boolean isOnCache) {
        return mRealm.where(MusicLocalInfo.class)
                .equalTo("songName", songName).equalTo("singerName", singerName).equalTo("fileSize", fileSize).equalTo("isOnCache", isOnCache)
                .findAll();
    }

    @Override
    public void replaceAllLocalMusics(List<MusicLocalInfo> localMusicInfoList) {
        if (localMusicInfoList != null) {
            for (int i = 0; i < localMusicInfoList.size(); i++) {
                MusicLocalInfo localMusicInfo = localMusicInfoList.get(i);
                MusicLocalInfo local = mRealm.where(MusicLocalInfo.class).equalTo("localUri", localMusicInfo.getLocalUri()).findFirst();
                if (local != null) {
                    localMusicInfo.setInPlayerList(local.isInPlayerList());
                }
            }
            try {//java.lang.IllegalArgumentException Illegal Argument: Failure when converting to UTF-8; error_code = 5; 0xbb50 0xdc74
                mRealm.beginTransaction();
                RealmResults<MusicLocalInfo> realmResults = mRealm.where(MusicLocalInfo.class).equalTo("isOnCache", false).findAll();
                if (realmResults != null) {
                    realmResults.deleteAllFromRealm();
                }
                mRealm.copyToRealmOrUpdate(localMusicInfoList);
                mRealm.commitTransaction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public RealmResults<MusicLocalInfo> queryAllLocalMusicInfos() {
        return mRealm.where(MusicLocalInfo.class).equalTo("isOnCache", false).findAll();
    }

    @Override
    public RealmResults<MusicLocalInfo> queryPlayerListLocalMusicInfos() {
        return mRealm.where(MusicLocalInfo.class).equalTo("isInPlayerList", true).findAll();
    }

    @Override
    public MusicLocalInfo queryPlayerListLocalMusicInfoByLocalUri(String localUri) {
        return mRealm.where(MusicLocalInfo.class).equalTo("isInPlayerList", true).equalTo("localUri", localUri).findFirst();
    }

    @Override
    public void addToLocalPlayerList(final MusicLocalInfo musicLocalInfo) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                musicLocalInfo.setInPlayerList(true);
                mRealm.copyToRealmOrUpdate(musicLocalInfo);
            }
        });
    }

    @Override
    public void addToLocalPlayerList(String localUri) {
        final MusicLocalInfo localMusicInfo = mRealm.where(MusicLocalInfo.class).equalTo("localUri", localUri).findFirst();
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                localMusicInfo.setInPlayerList(true);
                mRealm.copyToRealmOrUpdate(localMusicInfo);
            }
        });
    }

    @Override
    public void deleteFromLocalPlayerListByRemoteUri(String remoteUri) {
        final MusicLocalInfo localMusicInfo = mRealm.where(MusicLocalInfo.class).equalTo("remoteUri", remoteUri).findFirst();
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                localMusicInfo.setInPlayerList(false);
                mRealm.copyToRealmOrUpdate(localMusicInfo);
            }
        });
    }

    @Override
    public void deleteFromLocalPlayerList(String localUri) {
        final MusicLocalInfo localMusicInfo = mRealm.where(MusicLocalInfo.class).equalTo("localUri", localUri).findFirst();
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                localMusicInfo.setInPlayerList(false);
                mRealm.copyToRealmOrUpdate(localMusicInfo);
            }
        });
    }

    @Override
    public void deleteFromLocalMusics(String localUri) {
        final MusicLocalInfo localMusicInfo = mRealm.where(MusicLocalInfo.class).equalTo("localUri", localUri).findFirst();
        if (localMusicInfo == null) {
            return;
        }
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                localMusicInfo.deleteFromRealm();
            }
        });
    }

    @Override
    public void updateFromLocalMusics(final MusicLocalInfo musicLocalInfo) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                mRealm.copyToRealmOrUpdate(musicLocalInfo);
            }
        });
    }

    @Override
    public void updateFromLocalMusic(final MusicLocalInfo musicLocalInfo, final String songId, final int fileStatus) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                musicLocalInfo.setSongId(songId);
                musicLocalInfo.setFileStatus(fileStatus);
                mRealm.copyToRealmOrUpdate(musicLocalInfo);
            }
        });
    }
}
