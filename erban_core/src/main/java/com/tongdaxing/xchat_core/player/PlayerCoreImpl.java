package com.tongdaxing.xchat_core.player;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.music.IMusicCore;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.utils.AsyncTaskScanMusicFile;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.pref.CommonPref;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.realm.RealmResults;

/**
 * Created by chenran on 2017/10/28.
 */

public class PlayerCoreImpl extends AbstractBaseCore implements IPlayerCore {
    private AsyncTaskScanMusicFile scanMediaTask;
    private boolean isRefresh;
    private List<MusicLocalInfo> playerListMusicInfos;
    private MusicLocalInfo current;
    private String currentLocalUri;
    private int state;
    private int volume;
    private int recordingVolume;
    private Disposable mDisposable;

    public PlayerCoreImpl() {
        CoreManager.addClient(this);
        playerListMusicInfos = requestPlayerListLocalMusicInfos();
        if (playerListMusicInfos == null) {
            playerListMusicInfos = new CopyOnWriteArrayList<>();
        }
        state = STATE_STOP;
        volume = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("volume", 50);
        recordingVolume = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("recordingVolume", 50);

        mDisposable = IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        if (roomEvent == null) return;
                        int event = roomEvent.getEvent();
                        if (event == RoomEvent.ROOM_EXIT
                                || event == RoomEvent.KICK_OUT_ROOM
                                || event == RoomEvent.ADD_BLACK_LIST) {
                            state = STATE_STOP;
                            current = null;
                            currentLocalUri = "";
                        } else if (event == RoomEvent.DOWN_MIC
                                || event == RoomEvent.KICK_DOWN_MIC) {
                            if (AvRoomDataManager.get().isSelf(roomEvent.getAccount())) {
                                if (state != STATE_STOP) {//这样写的原因见stop方法
                                    current = null;
                                }
                                stop();
                            }
                        } else if (event == RoomEvent.METHOD_ON_AUDIO_MIXING_FINISHED) {//歌曲播放结束回调
                            handler.sendEmptyMessage(Play_Msg_What_Next);
                        }
                    }
                });
    }

    @Override
    public void setAudioMixCurrPosition(int position) {
        RtcEngineManager.get().setAudioMixingPosition(position);
    }


    @Override
    public boolean isRefresh() {
        return isRefresh;
    }

    @Override
    public int getState() {
        return state;
    }

    @Override
    public MusicLocalInfo getCurrent() {
        return current;
    }

    private PlayHandler handler = new PlayHandler(this);

    private static final int Play_Msg_What_Next = 0;//播放下一首
    private static final int Play_Msg_What_Progress = 1;//更新播放进度
    private static final long intervalOfMusicUpdateTimerExe = 250l;

    static class PlayHandler extends Handler {
        private WeakReference<PlayerCoreImpl> mWeakReference;

        PlayHandler(PlayerCoreImpl playerCore) {
            mWeakReference = new WeakReference<>(playerCore);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            PlayerCoreImpl playerCore = mWeakReference.get();
            switch (msg.what) {
                case Play_Msg_What_Next:
                    if (playerCore != null) {
                        playerCore.stop();//暂停当前正在播放的状态
                        playerCore.switchPlayNext();
                    }
                    break;
                case Play_Msg_What_Progress:
                    if (playerCore != null) {
                        playerCore.updateMusicPlayProgress();
                    }
                    break;
                default:
            }
        }
    }

    @Override
    public long getTotalDuration() {
        return RtcEngineManager.get().getAudioMixingDuration();
    }

    @Override
    public long getCurrentDuration() {
        return RtcEngineManager.get().getAudioMixingCurrentPosition();
    }

    @Override
    public void updateMusicPlayProgress() {
        long totalDur = RtcEngineManager.get().getAudioMixingDuration();
        long currPosition = RtcEngineManager.get().getAudioMixingCurrentPosition();
        if (totalDur > 0 && currPosition > 0) {
            notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_PROGRESS_UPDATE, totalDur, currPosition);
            if (null != handler) {
                handler.sendEmptyMessageDelayed(Play_Msg_What_Progress, intervalOfMusicUpdateTimerExe);
            }
        }
    }

    @Override
    public List<MusicLocalInfo> getPlayerListMusicInfos() {
        return playerListMusicInfos;
    }

    /**
     * 清空播放列表
     */
    @Override
    public void clearPlayerListMusicInfos() {
        if (playerListMusicInfos != null && playerListMusicInfos.size() != 0) {
            for (int i = 0; i < playerListMusicInfos.size(); i++) {
                if (TextUtils.isEmpty(playerListMusicInfos.get(i).getRemoteUri())) {
                    CoreManager.getCore(IPlayerDbCore.class).deleteFromLocalPlayerListByRemoteUri(playerListMusicInfos.get(i).getRemoteUri());
                }
            }
        }
        playerListMusicInfos = new ArrayList<>();
    }

    @Override
    public void refreshLocalMusic(List<MusicLocalInfo> lastScannedSongs) {
        if (isRefresh) {
            return;
        }

        isRefresh = true;
        scanMediaTask = new AsyncTaskScanMusicFile(BasicConfig.INSTANCE.getAppContext(), 0, new AsyncTaskScanMusicFile.ScanMediaCallback() {
            @Override
            public void onProgress(int progress, String message, int total) {
                notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_REFRESH_LOCAL_MUSIC_PROGRESS, progress);
            }

            @Override
            public void onComplete(boolean result) {
                isRefresh = false;
                List<MusicLocalInfo> localMusicInfoList = requestLocalMusicInfos();
                notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_REFRESH_LOCAL_MUSIC, localMusicInfoList);
                List<MusicLocalInfo> playerList = requestPlayerListLocalMusicInfos();
                playerListMusicInfos = playerList;
                notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_REFRESH_PLAYER_LIST, playerList);
            }
        }, lastScannedSongs);
        scanMediaTask.execute(BasicConfig.INSTANCE.getAppContext());
    }


    @Override
    public void addMusicToPlayerList(MusicLocalInfo localMusicInfo) {
        CoreManager.getCore(IPlayerDbCore.class).addToLocalPlayerList(localMusicInfo.getLocalUri());
        List<MusicLocalInfo> localMusicInfoList = requestLocalMusicInfos();
        notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_REFRESH_LOCAL_MUSIC, localMusicInfoList);
        List<MusicLocalInfo> playerList = requestPlayerListLocalMusicInfos();
        playerListMusicInfos = playerList;
        notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_REFRESH_PLAYER_LIST, playerList);
    }

    @Override
    public void deleteMusicFromPlayerList(MusicLocalInfo localMusicInfo) {
        if (current != null && localMusicInfo.getLocalUri().equals(current.getLocalUri())) {
            stop();
        }
        CoreManager.getCore(IPlayerDbCore.class).deleteFromLocalPlayerList(localMusicInfo.getLocalUri());
        List<MusicLocalInfo> localMusicInfoList = requestLocalMusicInfos();
        notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_REFRESH_LOCAL_MUSIC, localMusicInfoList);
        List<MusicLocalInfo> playerList = requestPlayerListLocalMusicInfos();
        playerListMusicInfos = playerList;
        notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_REFRESH_PLAYER_LIST, playerList);
    }

    @Override
    public List<MusicLocalInfo> requestLocalMusicInfos() {
        RealmResults<MusicLocalInfo> localMusicInfos = CoreManager.getCore(IPlayerDbCore.class).queryAllLocalMusicInfos();
        List<MusicLocalInfo> localMusicInfoList = new CopyOnWriteArrayList<>();
        if (localMusicInfos != null && localMusicInfos.size() > 0) {
            localMusicInfoList.addAll(localMusicInfos);
        }
        return localMusicInfoList;
    }

    @Override
    public List<MusicLocalInfo> requestPlayerListLocalMusicInfos() {
        RealmResults<MusicLocalInfo> localMusicInfos = CoreManager.getCore(IPlayerDbCore.class).queryPlayerListLocalMusicInfos();
        List<MusicLocalInfo> localMusicInfoList = new CopyOnWriteArrayList<>();
        if (localMusicInfos != null && localMusicInfos.size() > 0) {
            for (int i = 0; i < localMusicInfos.size(); i++) {//将不属于这个用户的音乐文件过滤
                if (localMusicInfos.get(i).getLocalUri().contains(CoreManager.getCore(IMusicCore.class).getCurrentUserFolderName())) {
                    localMusicInfoList.add(localMusicInfos.get(i));
                }
            }
        }
        return localMusicInfoList;
    }

    @Override
    public int play(String localUri) {
        return play(CoreManager.getCore(IPlayerDbCore.class).queryPlayerListLocalMusicInfoByLocalUri(localUri));
    }

    @Override
    public int play(MusicLocalInfo localMusicInfo) {
        if (localMusicInfo == null) {
            localMusicInfo = current;
        }

        if (current != null && state == STATE_PAUSE && current.getLocalUri().equals(localMusicInfo.getLocalUri())) {
            state = STATE_PLAY;
            RtcEngineManager.get().resumeAudioMixing();
            notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_PLAYING, current);
            if (null != handler) {
                handler.sendEmptyMessageDelayed(Play_Msg_What_Progress, intervalOfMusicUpdateTimerExe);
            }
            return PLAY_STATUS_SUCCESS;
        }

        if (localMusicInfo == null) {
            return PLAY_STATUS_MUSIC_INFO_INVALID;
        }

        File file = new File(localMusicInfo.getLocalUri());
        if (!file.exists()) {
            return PLAY_STATUS_FILE_NOT_FOUND;
        }

        RtcEngineManager.get().adjustAudioMixingVolume(volume);
        RtcEngineManager.get().adjustRecordingSignalVolume(recordingVolume);
        int result = RtcEngineManager.get().startAudioMixing(localMusicInfo.getLocalUri(), false, 1);
        if (result == PLAY_STATUS_FILE_NOT_FOUND) {
            return PLAY_STATUS_FILE_NOT_FOUND;
        }
        current = localMusicInfo;
        currentLocalUri = current.getLocalUri();
        state = STATE_PLAY;
        notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_PLAYING, current);
        if (null != handler) {
            handler.sendEmptyMessageDelayed(Play_Msg_What_Progress, intervalOfMusicUpdateTimerExe);
        }
        return PLAY_STATUS_SUCCESS;
    }

    @Override
    public void pause() {
        if (state == STATE_PLAY) {
            RtcEngineManager.get().pauseAudioMixing();
            state = STATE_PAUSE;
            notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_PAUSE, current);
            if (null != handler) {
                handler.removeMessages(Play_Msg_What_Progress);
            }
        }
    }

    @Override
    public void stop() {
        if (state != STATE_STOP) {
            RtcEngineManager.get().stopAudioMixing();
            state = STATE_STOP;
            currentLocalUri = "";
//            current = null;停止后如果置空会导致列表播放无法下一首 -- 而如果下麦后如果不置空会导致重新上麦直接播放下一首
            notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_STOP, current);
            if (null != handler) {
                handler.removeMessages(Play_Msg_What_Progress);
            }
        }
    }

    /**
     * 手动切换下一首歌曲
     *
     * @return
     */
    @Override
    public int switchPlayNext() {
        if (current == null || !current.isValid()) {
            return PLAY_STATUS_FILE_NOT_FOUND;
        }
        if (playerListMusicInfos != null && playerListMusicInfos.size() > 0) {
            switch (playMode) {
                case 0://列表循环
                    index = playerListMusicInfos.indexOf(current);
                    if (index >= 0 && index < playerListMusicInfos.size()) {
                        index += 1;
                        if (index >= 0 && index < playerListMusicInfos.size()) {
                            if (null != handler) {
                                handler.removeMessages(Play_Msg_What_Progress);
                            }
                            return play(playerListMusicInfos.get(index));
                        }
                    }
                    index = 0;
                    if (null != handler) {
                        handler.removeMessages(Play_Msg_What_Progress);
                    }
                    return play(playerListMusicInfos.get(index));
                case 1://单曲循环
                    index = playerListMusicInfos.indexOf(current);
                    if (index >= 0 && index < playerListMusicInfos.size()) {
                        if (null != handler) {
                            handler.removeMessages(Play_Msg_What_Progress);
                        }
                        return play(playerListMusicInfos.get(index));
                    }
                    index = 0;
                    if (null != handler) {
                        handler.removeMessages(Play_Msg_What_Progress);
                    }
                    return play(playerListMusicInfos.get(index));
                case 2://随机播放
                    index = playerListMusicInfos.indexOf(current);
                    if (index >= 0 && index < playerListMusicInfos.size()) {
                        Random random = new Random();
                        index = random.nextInt(playerListMusicInfos.size());
                        if (index >= 0 && index < playerListMusicInfos.size()) {
                            if (null != handler) {
                                handler.removeMessages(Play_Msg_What_Progress);
                            }
                            return play(playerListMusicInfos.get(index));
                        }
                    }
                    index = 0;
                    if (null != handler) {
                        handler.removeMessages(Play_Msg_What_Progress);
                    }
                    return play(playerListMusicInfos.get(index));
                default://跟列表循环一样
                    index = playerListMusicInfos.indexOf(current);
                    if (index >= 0 && index < playerListMusicInfos.size()) {
                        index += 1;
                        if (index >= 0 && index < playerListMusicInfos.size()) {
                            if (null != handler) {
                                handler.removeMessages(Play_Msg_What_Progress);
                            }
                            return play(playerListMusicInfos.get(index));
                        }
                    }
                    index = 0;
                    if (null != handler) {
                        handler.removeMessages(Play_Msg_What_Progress);
                    }
                    return play(playerListMusicInfos.get(index));
            }
        } else {
            return PLAY_STATUS_MUSIC_LIST_EMPTY;
        }
    }

    /**
     * 手动切换上一首歌曲
     *
     * @return
     */
    @Override
    public int switchPlayLast() {
        if (playerListMusicInfos != null && playerListMusicInfos.size() > 0) {
            switch (playMode) {
                case 0://列表循环
                    index = playerListMusicInfos.indexOf(current);
                    if (index >= 1 && index < playerListMusicInfos.size()) {
                        index -= 1;
                        if (index >= 0 && index < playerListMusicInfos.size()) {
                            if (null != handler) {
                                handler.removeMessages(Play_Msg_What_Progress);
                            }
                            return play(playerListMusicInfos.get(index));
                        }
                    }
                    index = playerListMusicInfos.size() - 1;
                    if (null != handler) {
                        handler.removeMessages(Play_Msg_What_Progress);
                    }
                    return play(playerListMusicInfos.get(index));
                case 1://单曲循环
                    index = playerListMusicInfos.indexOf(current);
                    if (index >= 0 && index < playerListMusicInfos.size()) {
                        if (null != handler) {
                            handler.removeMessages(Play_Msg_What_Progress);
                        }
                        return play(playerListMusicInfos.get(index));
                    }
                    index = 0;
                    if (null != handler) {
                        handler.removeMessages(Play_Msg_What_Progress);
                    }
                    return play(playerListMusicInfos.get(index));
                case 2://随机播放 todo 要用一个列表存放所有播放过的音乐 这样上一首才能播放 而不是又随机一次
                    index = playerListMusicInfos.indexOf(current);
                    if (index >= 0 && index < playerListMusicInfos.size()) {
                        Random random = new Random();
                        index = random.nextInt(playerListMusicInfos.size());
                        if (index >= 0 && index < playerListMusicInfos.size()) {
                            if (null != handler) {
                                handler.removeMessages(Play_Msg_What_Progress);
                            }
                            return play(playerListMusicInfos.get(index));
                        }
                    }
                    index = 0;
                    if (null != handler) {
                        handler.removeMessages(Play_Msg_What_Progress);
                    }
                    return play(playerListMusicInfos.get(index));
                default://跟列表循环一样
                    index = playerListMusicInfos.indexOf(current);
                    if (index >= 1 && index < playerListMusicInfos.size()) {
                        index -= 1;
                        if (index >= 0 && index < playerListMusicInfos.size()) {
                            if (null != handler) {
                                handler.removeMessages(Play_Msg_What_Progress);
                            }
                            return play(playerListMusicInfos.get(index));
                        }
                    }
                    index = playerListMusicInfos.size() - 1;
                    if (null != handler) {
                        handler.removeMessages(Play_Msg_What_Progress);
                    }
                    return play(playerListMusicInfos.get(index));
            }
        } else {
            return PLAY_STATUS_MUSIC_LIST_EMPTY;
        }
    }

    private int index = 0;

    /**
     * 0:"列表循环", 1:"单曲循环", 2:"随机播放"
     */
    private int playMode;

    @Override
    public int getPlayMode() {
        return playMode;
    }

    @Override
    public void setPlayMode(int playMode) {
        this.playMode = playMode;
    }

    @Override
    public void seekVolume(int volume) {
        this.volume = volume;
        CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("volume", volume);
        RtcEngineManager.get().adjustAudioMixingVolume(volume);
    }

    @Override
    public void seekRecordingVolume(int volume) {
        this.recordingVolume = volume;
        CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("recordingVolume", volume);
        RtcEngineManager.get().adjustRecordingSignalVolume(volume);
    }

    @Override
    public int getCurrentVolume() {
        return volume;
    }

    @Override
    public int getCurrentRecordingVolume() {
        return recordingVolume;
    }
}
