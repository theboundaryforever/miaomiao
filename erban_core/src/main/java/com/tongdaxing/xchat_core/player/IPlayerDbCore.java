package com.tongdaxing.xchat_core.player;

import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by chenran on 2017/10/31.
 */

public interface IPlayerDbCore extends IBaseCore {

    /**
     * 根据本地路径查找播放列表中的音乐文件信息
     *
     * @param localUri 音乐的本地路径
     */
    MusicLocalInfo queryPlayerListLocalMusicInfoByLocalUri(String localUri);

    void addToLocalPlayerList(MusicLocalInfo musicLocalInfo);

    /**
     * 添加到本地播放列表
     */
    public void addToLocalPlayerList(String localUri);

    /**
     * 查询本地歌曲
     */
    public MusicLocalInfo requestLocalMusicInfoByLocalUri(String localUri);

    void deleteFromLocalPlayerListByRemoteUri(String remoteUri);

    /**
     * 从本地播放列表中删除
     */
    public void deleteFromLocalPlayerList(String localUri);

    MusicLocalInfo requestLocalMusicInfo(String songName, String singerName, long fileSize, boolean isOnCache);

    RealmResults<MusicLocalInfo> requestLocalMusicInfoAll(String songName, String singerName, long fileSize, boolean isOnCache);

    /**
     * 保存本地音乐列表到数据库
     */
    public void replaceAllLocalMusics(List<MusicLocalInfo> localMusicInfoList);

    /**
     * 查询本地所有音乐
     */
    public RealmResults<MusicLocalInfo> queryAllLocalMusicInfos();

    /**
     * 查询本地音乐播放列表音乐
     */
    public RealmResults<MusicLocalInfo> queryPlayerListLocalMusicInfos();

    /**
     * 删除歌曲
     */
    void deleteFromLocalMusics(String localUri);

    /**
     * 更新歌曲
     */
    void updateFromLocalMusics(MusicLocalInfo musicLocalInfo);

    void updateFromLocalMusic(MusicLocalInfo musicLocalInfo, String songId, int fileStatus);
}
