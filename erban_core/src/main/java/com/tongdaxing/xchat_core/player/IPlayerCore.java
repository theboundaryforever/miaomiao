package com.tongdaxing.xchat_core.player;

import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

import java.util.List;

/**
 * Created by chenran on 2017/10/28.
 */

public interface IPlayerCore extends IBaseCore {
    public static final int STATE_STOP = 0;
    public static final int STATE_PLAY = 1;
    public static final int STATE_PAUSE = 2;

    public static final int PLAY_STATUS_SUCCESS = 0;//成功加载并播放
    public static final int PLAY_STATUS_MUSIC_INFO_INVALID = -2;//媒体资源信息为空
    public static final int PLAY_STATUS_FILE_NOT_FOUND = -1;//媒体资源文件未能找到/不存在
    public static final int PLAY_STATUS_MUSIC_LIST_EMPTY = -3;//播放列表为空

    boolean closeMicToStopMusic = false;

    /**
     * 获取歌曲的总进度
     *
     * @return
     */
    long getTotalDuration();

    /**
     * 获取歌曲的当前进度
     *
     * @return
     */
    long getCurrentDuration();

    /**
     * 刷新本地音乐库
     */
    void refreshLocalMusic(List<MusicLocalInfo> lastScannedSongs);

    void addMusicToPlayerList(MusicLocalInfo localMusicInfo);

    void deleteMusicFromPlayerList(MusicLocalInfo localMusicInfo);

    /**
     * 查询本地音乐列表
     */
    List<MusicLocalInfo> requestLocalMusicInfos();

    /**
     * 查询本地播放器列表
     *
     * @return
     */
    List<MusicLocalInfo> requestPlayerListLocalMusicInfos();

    void setAudioMixCurrPosition(int position);

    boolean isRefresh();

    /**
     * 获取当前播放器播放状态
     *
     * @return
     */
    int getState();

    /**
     * 当前正在播放的音乐
     *
     * @return
     */
    MusicLocalInfo getCurrent();

    List<MusicLocalInfo> getPlayerListMusicInfos();

    void clearPlayerListMusicInfos();

    int play(MusicLocalInfo localMusicInfo);

    int play(String localUri);

    void pause();

    void stop();

    void seekVolume(int volume);

    void seekRecordingVolume(int volume);

    int getCurrentVolume();

    int getCurrentRecordingVolume();

    void setPlayMode(int playMode);

    int getPlayMode();

    int switchPlayNext();

    int switchPlayLast();

    void updateMusicPlayProgress();
}
