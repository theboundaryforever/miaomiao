package com.tongdaxing.xchat_core.player.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Function:
 * Author: Edward on 2019/2/15
 */
public class MyMusicInfo implements Parcelable {

    /**
     * album : 自定义
     * artist : 许嵩
     * duration : 220
     * id : 16
     * singCount : 0
     * size : 5399374
     * songStatus : 0
     * title : 清明雨上
     * upUid : 109731
     * url : https://pic.miaomiaofm.com/lj8q9C1RgZbuAYrGP9pD1G-aO0MP
     * userNick : img
     */
    private String album;
    private String artist;
    private int duration;
    private long id;
    private int singCount;
    private long size;
    private int songStatus;
    private String title;
    private boolean isUserSong;//是否是用户歌单中的歌曲
    private int upUid;
    private String url;
    private String userNick;
    private String localUri;//本地音乐文件缓存路径，有可能为空。此字段与服务器无关。
    private boolean isHasCache = false;//判断这首歌是否有本地缓存，true表示有，false表示没有。此字段与服务器无关。
    private int fileStatus;//0是可下载，1可播放，2是暂停,3是正在下载。此字段与服务器无关。

    public boolean isUserSong() {
        return isUserSong;
    }

    public void setUserSong(boolean userSong) {
        isUserSong = userSong;
    }

    public int getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(int fileStatus) {
        this.fileStatus = fileStatus;
    }

    public String getLocalUri() {
        return localUri;
    }

    public void setLocalUri(String localUri) {
        this.localUri = localUri;
    }

    public boolean isHasCache() {
        return isHasCache;
    }

    public void setHasCache(boolean hasCache) {
        isHasCache = hasCache;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSingCount() {
        return singCount;
    }

    public void setSingCount(int singCount) {
        this.singCount = singCount;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public int getSongStatus() {
        return songStatus;
    }

    public void setSongStatus(int songStatus) {
        this.songStatus = songStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUpUid() {
        return upUid;
    }

    public void setUpUid(int upUid) {
        this.upUid = upUid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    @Override
    public String toString() {
        return "MyMusicInfo{" +
                "album='" + album + '\'' +
                ", artist='" + artist + '\'' +
                ", duration=" + duration +
                ", id=" + id +
                ", singCount=" + singCount +
                ", size=" + size +
                ", songStatus=" + songStatus +
                ", title='" + title + '\'' +
                ", isUserSong=" + isUserSong +
                ", upUid=" + upUid +
                ", url='" + url + '\'' +
                ", userNick='" + userNick + '\'' +
                ", localUri='" + localUri + '\'' +
                ", isHasCache=" + isHasCache +
                ", fileStatus=" + fileStatus +
                '}';
    }

    public MyMusicInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.album);
        dest.writeString(this.artist);
        dest.writeInt(this.duration);
        dest.writeLong(this.id);
        dest.writeInt(this.singCount);
        dest.writeLong(this.size);
        dest.writeInt(this.songStatus);
        dest.writeString(this.title);
        dest.writeByte(this.isUserSong ? (byte) 1 : (byte) 0);
        dest.writeInt(this.upUid);
        dest.writeString(this.url);
        dest.writeString(this.userNick);
        dest.writeString(this.localUri);
        dest.writeByte(this.isHasCache ? (byte) 1 : (byte) 0);
        dest.writeInt(this.fileStatus);
    }

    protected MyMusicInfo(Parcel in) {
        this.album = in.readString();
        this.artist = in.readString();
        this.duration = in.readInt();
        this.id = in.readLong();
        this.singCount = in.readInt();
        this.size = in.readLong();
        this.songStatus = in.readInt();
        this.title = in.readString();
        this.isUserSong = in.readByte() != 0;
        this.upUid = in.readInt();
        this.url = in.readString();
        this.userNick = in.readString();
        this.localUri = in.readString();
        this.isHasCache = in.readByte() != 0;
        this.fileStatus = in.readInt();
    }

    public static final Creator<MyMusicInfo> CREATOR = new Creator<MyMusicInfo>() {
        @Override
        public MyMusicInfo createFromParcel(Parcel source) {
            return new MyMusicInfo(source);
        }

        @Override
        public MyMusicInfo[] newArray(int size) {
            return new MyMusicInfo[size];
        }
    };
}
