package com.tongdaxing.xchat_core.file;

/**
 * Created by Chen on 2019/4/25.
 */
public class UpLoadFileEvent {
    public static class OnUploadFile {
        boolean mIsSuccess;
        String mFilePath;

        public OnUploadFile(boolean isSuccess, String filePath) {
            this.mIsSuccess = isSuccess;
            this.mFilePath = filePath;
        }

        public boolean isSuccess() {
            return mIsSuccess;
        }

        public String getFilePath() {
            return mFilePath;
        }
    }

    public static class OnSilentUploadLogFile {
    }
}
