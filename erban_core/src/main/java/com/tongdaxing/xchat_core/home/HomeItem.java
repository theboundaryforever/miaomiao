package com.tongdaxing.xchat_core.home;


import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * <p> 首页热门实体u </p>
 *
 * @author Administrator
 * @date 2017/11/16
 */
public class HomeItem implements MultiItemEntity, Parcelable {

    public static final int BANNER = 1;
    public static final int RANKING = 2;
    public static final int RECOMMEND = 3;
    public static final int NORMAL = 4;


    private int itemType;

    //正常数据
    public List<HomeRoom> recomList;
    public List<HomeRoom> homeItemRoomList;
    public List<BannerInfo> bannerInfoList;
    public RankingInfo mRankingInfo;


    public HomeItem(int itemType) {
        this.itemType = itemType;
    }


    public HomeItem(int itemType, RankingInfo rankingInfo) {
        this.itemType = itemType;
        mRankingInfo = rankingInfo;
    }

    protected HomeItem(Parcel in) {
        itemType = in.readInt();
        recomList = in.createTypedArrayList(HomeRoom.CREATOR);
        homeItemRoomList = in.createTypedArrayList(HomeRoom.CREATOR);
        bannerInfoList = in.createTypedArrayList(BannerInfo.CREATOR);
        mRankingInfo = in.readParcelable(RankingInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(itemType);
        dest.writeTypedList(recomList);
        dest.writeTypedList(homeItemRoomList);
        dest.writeTypedList(bannerInfoList);
        dest.writeParcelable(mRankingInfo, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeItem> CREATOR = new Creator<HomeItem>() {
        @Override
        public HomeItem createFromParcel(Parcel in) {
            return new HomeItem(in);
        }

        @Override
        public HomeItem[] newArray(int size) {
            return new HomeItem[size];
        }
    };

    @Override
    public int getItemType() {
        return itemType;
    }
}
