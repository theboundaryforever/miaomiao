package com.tongdaxing.xchat_core.home;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/19
 */
public class NewTabInfo {

    /**
     * name : 娱乐房间
     * tagList : [{"children":"2","createTime":1511155709000,"defPic":"https://pic.miaomiaofm.com/home_nanshen_moren@3x.png","description":"","id":2,"istop":false,"leftLevel":0,"name":"男神","optPic":"https://pic.miaomiaofm.com/home_nanshen_dianji@3x.png","pict":"https://pic.miaomiaofm.com/room_tag_new_god.png","seq":6,"status":true,"tmpint":1,"tmpstr":"","type":1},{"children":"3","createTime":1511155717000,"defPic":"https://pic.miaomiaofm.com/home_nvshen_moren@3x.png","description":"","id":3,"istop":false,"leftLevel":0,"name":"女神","optPic":"https://pic.miaomiaofm.com/home_nvshen_dianji@3x.png","pict":"https://pic.miaomiaofm.com/room_tag_new_goddess.png","seq":5,"status":true,"tmpint":1,"tmpstr":"","type":1},{"children":"4,6","createTime":1511155717000,"defPic":"https://pic.miaomiaofm.com/home_song_moren@3x.png","description":"","id":4,"istop":false,"leftLevel":0,"name":"听歌","optPic":"https://pic.miaomiaofm.com/home_song_dianji@3x.png","pict":"https://pic.miaomiaofm.com/room_tag_new_music.png","seq":9,"status":true,"tmpint":1,"tmpstr":"","type":1},{"children":"6","createTime":1511155717000,"defPic":"https://pic.miaomiaofm.com/home_diantai_moren@3x.png","description":"","id":6,"istop":false,"leftLevel":0,"name":"电台","optPic":"https://pic.miaomiaofm.com/home_diantai_dianji@3x.png","pict":"https://pic.miaomiaofm.com/room_tag_new_radio.png","seq":3,"status":true,"tmpstr":"","type":1}]
     * type : 3
     * typeEnum : recreation
     */

    private String name;
    private int type;
    private int id;
    private String typeEnum;
    private List<TagListBean> tagList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeEnum() {
        return typeEnum;
    }

    public void setTypeEnum(String typeEnum) {
        this.typeEnum = typeEnum;
    }

    public List<TagListBean> getTagList() {
        return tagList;
    }

    public void setTagList(List<TagListBean> tagList) {
        this.tagList = tagList;
    }

    public static class TagListBean {
        /**
         * children : 2
         * createTime : 1511155709000
         * defPic : https://pic.miaomiaofm.com/home_nanshen_moren@3x.png
         * description :
         * id : 2
         * istop : false
         * leftLevel : 0
         * name : 男神
         * optPic : https://pic.miaomiaofm.com/home_nanshen_dianji@3x.png
         * pict : https://pic.miaomiaofm.com/room_tag_new_god.png
         * seq : 6
         * status : true
         * tmpint : 1
         * tmpstr :
         * type : 1
         */

        private String children;
        private long createTime;
        private String defPic;
        private String description;
        private int id;
        private boolean istop;
        private int leftLevel;
        private String name;
        private String optPic;
        private String pict;
        private int seq;
        private boolean status;
        private int tmpint;
        private String tmpstr;
        private int type;

        public String getChildren() {
            return children;
        }

        public void setChildren(String children) {
            this.children = children;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getDefPic() {
            return defPic;
        }

        public void setDefPic(String defPic) {
            this.defPic = defPic;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isIstop() {
            return istop;
        }

        public void setIstop(boolean istop) {
            this.istop = istop;
        }

        public int getLeftLevel() {
            return leftLevel;
        }

        public void setLeftLevel(int leftLevel) {
            this.leftLevel = leftLevel;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOptPic() {
            return optPic;
        }

        public void setOptPic(String optPic) {
            this.optPic = optPic;
        }

        public String getPict() {
            return pict;
        }

        public void setPict(String pict) {
            this.pict = pict;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public int getTmpint() {
            return tmpint;
        }

        public void setTmpint(int tmpint) {
            this.tmpint = tmpint;
        }

        public String getTmpstr() {
            return tmpstr;
        }

        public void setTmpstr(String tmpstr) {
            this.tmpstr = tmpstr;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}
