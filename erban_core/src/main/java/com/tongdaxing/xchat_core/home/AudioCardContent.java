package com.tongdaxing.xchat_core.home;

/**
 * Function:
 * Author: Edward on 2018/12/26
 */
public class AudioCardContent {

    /**
     * id : 4
     * title : 土味情话
     * content : 莫文蔚的阴天，周杰伦的晴天，孙燕姿的雨天，统统都不如跟你聊天。
     */

    private int id;
    private String title;
    private String content;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "AudioCardContent{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
