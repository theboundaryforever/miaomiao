package com.tongdaxing.xchat_core.home;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author zhouxiangfeng
 * @date 2017/5/17
 */

public class HomeIcon implements Parcelable {


    private String pic;

    private String activity;

    private String params;

    private String title;

    private String url;

    private String subtitle;

    private int skipType;//4排行榜

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSkipType() {
        return skipType;
    }

    public void setSkipType(int skipType) {
        this.skipType = skipType;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }


    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.pic);
        dest.writeString(this.activity);
        dest.writeString(this.params);
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeString(this.subtitle);
        dest.writeInt(this.skipType);
    }

    public HomeIcon() {
    }

    protected HomeIcon(Parcel in) {
        this.pic = in.readString();
        this.activity = in.readString();
        this.params = in.readString();
        this.title = in.readString();
        this.url = in.readString();
        this.subtitle = in.readString();
        this.skipType = in.readInt();
    }

    public static final Creator<HomeIcon> CREATOR = new Creator<HomeIcon>() {
        @Override
        public HomeIcon createFromParcel(Parcel source) {
            return new HomeIcon(source);
        }

        @Override
        public HomeIcon[] newArray(int size) {
            return new HomeIcon[size];
        }
    };
}
