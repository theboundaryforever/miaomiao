package com.tongdaxing.xchat_core.home;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/4
 */
public class AudioCardInfo {

    /**
     * auxiliaryTone : [{"toneName":"低沉撩人音","tonePro":8},{"toneName":"古风公子音","tonePro":3},{"toneName":"温婉电台音","tonePro":5}]
     * auxiliaryToneList : [{"toneName":"低沉撩人音","tonePro":8},{"toneName":"古风公子音","tonePro":3},{"toneName":"温婉电台音","tonePro":5}]
     * avatar : https://pic.miaomiaofm.com/FiTJZq8espv-mOkBMBB4fypAULDi?imageslim
     * bestMatch : 元气少女音
     * charmLevel : 5
     * createDate : 1551670627225
     * masterTone : 傲娇学长音
     * masterTonePro : 84
     * nick : img
     * similarPerson : 胡歌
     * uid : 109731
     * url : https://pic.miaomiaofm.com/FlMJfqT6p0-cdNjjtTg4COzrJot2?imageslim
     * voiceScore : 98
     */

    private String auxiliaryTone;
    private String avatar;
    private String bestMatch;
    private int charmLevel;
    private long createDate;
    private String masterTone;
    private int masterTonePro;
    private String nick;
    private String similarPerson;
    private long uid;
    private String url;
    private int voiceScore;
    private List<AuxiliaryToneListBean> auxiliaryToneList;

    public String getAuxiliaryTone() {
        return auxiliaryTone;
    }

    public void setAuxiliaryTone(String auxiliaryTone) {
        this.auxiliaryTone = auxiliaryTone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBestMatch() {
        return bestMatch;
    }

    public void setBestMatch(String bestMatch) {
        this.bestMatch = bestMatch;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getMasterTone() {
        return masterTone;
    }

    public void setMasterTone(String masterTone) {
        this.masterTone = masterTone;
    }

    public int getMasterTonePro() {
        return masterTonePro;
    }

    public void setMasterTonePro(int masterTonePro) {
        this.masterTonePro = masterTonePro;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getSimilarPerson() {
        return similarPerson;
    }

    public void setSimilarPerson(String similarPerson) {
        this.similarPerson = similarPerson;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getVoiceScore() {
        return voiceScore;
    }

    public void setVoiceScore(int voiceScore) {
        this.voiceScore = voiceScore;
    }

    public List<AuxiliaryToneListBean> getAuxiliaryToneList() {
        return auxiliaryToneList;
    }

    public void setAuxiliaryToneList(List<AuxiliaryToneListBean> auxiliaryToneList) {
        this.auxiliaryToneList = auxiliaryToneList;
    }

    public static class AuxiliaryToneListBean {
        /**
         * toneName : 低沉撩人音
         * tonePro : 8
         */

        private String toneName;
        private int tonePro;

        public String getToneName() {
            return toneName;
        }

        public void setToneName(String toneName) {
            this.toneName = toneName;
        }

        public int getTonePro() {
            return tonePro;
        }

        public void setTonePro(int tonePro) {
            this.tonePro = tonePro;
        }
    }
}
