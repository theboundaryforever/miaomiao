package com.tongdaxing.xchat_core.home;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> tab 标签数据 </p>
 *
 * @author Administrator
 * @date 2017/11/21
 */
public class TabInfo implements Parcelable {

    /**
     * id: 105,
     * name: "活动",
     * seqNo: 6
     */

    private int id;
    private String name;
    private int seqNo;

    public TabInfo(int id, String name) {
        this.id = id;
        this.name = name;
    }


    protected TabInfo(Parcel in) {
        id = in.readInt();
        name = in.readString();
        seqNo = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(seqNo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TabInfo> CREATOR = new Creator<TabInfo>() {
        @Override
        public TabInfo createFromParcel(Parcel in) {
            return new TabInfo(in);
        }

        @Override
        public TabInfo[] newArray(int size) {
            return new TabInfo[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public static List<TabInfo> getTabDefaultList() {
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(-1, "关注"));
//        tabInfoList.add(new TabInfo(-2, "热门"));
//        tabInfoList.add(new TabInfo(-3, "交友"));
        return tabInfoList;
    }

    @Override
    public String toString() {
        return "TabInfo{" +
                "name='" + name + '\'' +
                "id='" + id + '\'' +
                '}';
    }
}