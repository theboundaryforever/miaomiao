package com.tongdaxing.xchat_core.home.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public interface IMainView extends IMvpBaseView {
    /**
     * 退出房间
     */
    void exitRoom();

    void controlTab(UserInfo userInfo);

    /**
     * 被抱上麦 进行提示
     */
    void showInviteUpMicSuccess();
}
