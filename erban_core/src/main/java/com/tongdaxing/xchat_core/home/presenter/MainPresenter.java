package com.tongdaxing.xchat_core.home.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.netease.nim.uikit.session.SessionEventListener;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.model.MainModel;
import com.tongdaxing.xchat_core.home.view.IMainView;
import com.tongdaxing.xchat_core.makefriedns.BroadcastMsgEvent;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.music.IMusicCore;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.result.UserResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.broadcastMsg.FindNewMakeFriendsModel;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Map;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public class MainPresenter extends AbstractMvpPresenter<IMainView> {
    private final AvRoomModel mAvRoomMode;
    private final MainModel mainModel;
    private FindNewMakeFriendsModel findNewMakeFriendsModel;

    public static final String TAG = "MainPresenter";

    public MainPresenter() {
        mAvRoomMode = new AvRoomModel();
        mainModel = new MainModel();
        findNewMakeFriendsModel = new FindNewMakeFriendsModel();
    }

    @Override
    public void onCreatePresenter(@Nullable Bundle saveState) {
        super.onCreatePresenter(saveState);
        CoreUtils.register(this);
    }

    @Override
    public void onDestroyPresenter() {
        super.onDestroyPresenter();
        CoreUtils.unregister(this);
    }

    public void exitRoom() {
        mAvRoomMode.exitRoom(new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
                if (getMvpView() != null) {
                    getMvpView().exitRoom();
                }
            }

            @Override
            public void onFail(int code, String error) {

            }
        });
    }

    /**
     * 添加歌曲到我的曲库
     *
     * @param myMusicInfo
     */
    public void addSongToMyLibrary(final HotMusicInfo myMusicInfo) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("songId", String.valueOf(myMusicInfo.getId()));
        OkHttpManager.getInstance().doPostRequest(UriProvider.getAddHotSong(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                CoreManager.notifyClients(IMusicDownloaderCoreClient.class,
                        IMusicDownloaderCoreClient.METHOD_ON_MUSIC_ADD_SONG_FAILURE, myMusicInfo);
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    CoreManager.notifyClients(IMusicDownloaderCoreClient.class,
                            IMusicDownloaderCoreClient.METHOD_ON_MUSIC_ADD_SONG_SUCCEED, myMusicInfo);
                } else {
                    CoreManager.notifyClients(IMusicDownloaderCoreClient.class,
                            IMusicDownloaderCoreClient.METHOD_ON_MUSIC_ADD_SONG_FAILURE, myMusicInfo);
                }
            }
        });
    }

    public void uploadSong(final MusicLocalInfo musicLocalInfo, String remoteUri) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("url", remoteUri);
        params.put("size", String.valueOf(musicLocalInfo.getFileSize()));
        params.put("duration", String.valueOf(musicLocalInfo.getDuration()));
        params.put("title", musicLocalInfo.getSongName());
        params.put("album", musicLocalInfo.getAlbumName());
        params.put("artist", musicLocalInfo.getSingerName());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getUploadSong(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                CoreManager.notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.MUSIC_UPLOAD_FAILURE);
            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") == 200) {
                    CoreManager.getCore(IMusicCore.class).copyFileToCacheFolder(musicLocalInfo);//将歌曲复制到缓存
//                    CoreManager.getCore(IPlayerCore.class).addMusicToPlayerList(musicLocalInfo);//添加到播放列表
                    CoreManager.notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.MUSIC_UPLOAD_SUCCESS, musicLocalInfo);
                } else {
                    CoreManager.notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.MUSIC_UPLOAD_FAILURE, musicLocalInfo);
                }
            }
        });
    }

    public void requestUserInfo(final long userId) {
        if (userId <= 0) {
            return;
        }
        mainModel.getUserInfo(userId, new OkHttpManager.MyCallBack<UserResult>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(UserResult response) {
                if (null != response) {
                    if (response.isSuccess() && response.getData() != null) {
                        com.tongdaxing.xchat_framework.util.util.LogUtil.d("mainModel getUserInfo response.getData() is " + (response.getData() == null ? "null" : "not null"));
                        if (getMvpView() != null) {
                            getMvpView().controlTab(response.getData());
                        }
                    }
                }
            }
        });

    }

    /**
     * 判断己方是否有CP，
     * * 如无，则在私聊页中看所有人均为空心状态；cpState = 0 空心
     * * 如有CP，
     * * 则判断对方是否为我的CP，如是，则展示红心（满），cpState = 1 红心 + 亲密值
     * * 如不是，则不展示任何心（空心也不展示） cpState = 2 不显示心
     *
     * @param targetUid
     * @param cpStateCallback
     */
    public void cpCheck(final long targetUid, final SessionEventListener.GetCpStateCallback cpStateCallback) {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo == null) {
            if (cpStateCallback != null) {
                cpStateCallback.callback(0, -1);
            }
            return;
        }
        mainModel.getUserInfo(userInfo.getUid(), new OkHttpManager.MyCallBack<UserResult>() {
            @Override
            public void onError(Exception e) {
                if (cpStateCallback != null) {
                    cpStateCallback.callback(0, -1);
                }
            }

            @Override
            public void onResponse(UserResult response) {
                if (null != response) {
                    if (response.isSuccess() && response.getData() != null) {
                        UserInfo userInfo = response.getData();
                        L.debug(TAG, "cpCheck userInfo: %s", userInfo);
                        int cpState = 0;
                        int closeLevel = -1;
                        if (userInfo != null) {
                            if (userInfo.getCpUser() != null) {
                                if (userInfo.getCpUser().getUid() == targetUid) {
                                    cpState = 1;
                                    closeLevel = userInfo.getCpUser().getCloseLevel();
                                } else {
                                    cpState = 2;
                                }
                            }
                        }
                        if (cpStateCallback != null) {
                            cpStateCallback.callback(cpState, closeLevel);
                        }
                    }
                }
            }
        });
    }

    public void onInviteUpMic(final int micPosition) {
        L.info(TAG, "onInviteUpMic() micPosition = %d", micPosition);
        AvRoomDataManager.get().mIsNeedOpenMic = false;
        upMicroPhone(micPosition, String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()), true);
    }

    private void upMicroPhone(final int micPosition, final String uId, boolean isInviteUpMic) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        mAvRoomMode.upMicroPhone(micPosition, uId, String.valueOf(roomInfo.getRoomId()),
                isInviteUpMic, new CallBack<String>() {
                    @Override
                    public void onSuccess(String data) {
                        L.info(TAG, "用户%s上麦成功：%s", uId, data);
                        if (getMvpView() != null) {
                            getMvpView().showInviteUpMicSuccess();
                        }
                    }

                    @Override
                    public void onFail(int code, String error) {
                        L.info(TAG, "用户%s上麦失败：%s----", uId, error);
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOpenBroadcastSendDialog(BroadcastMsgEvent.OnBroadcastSendDialogOpen open) {
        findNewMakeFriendsModel.requestCD(new OkHttpManager.MyCallBack<ServiceResult<Integer>>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(ServiceResult<Integer> response) {
                if (response.isSuccess() && response.getData() != null) {
                    //获取剩余时间 发送给dialog
                    CoreUtils.send(new BroadcastMsgEvent.OnMsgSendCD(response.getData()));
                }
            }
        });
//        CoreUtils.send(new BroadcastMsgEvent.OnMsgSendCD(5));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBroadcastSend(final BroadcastMsgEvent.OnBroadcastSendDialogSend send) {
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo != null) {
            findNewMakeFriendsModel.pushMsg(mCurrentRoomInfo.getRoomId(), mCurrentRoomInfo.getUid(), send.getContent(), new OkHttpManager.MyCallBack<ServiceResult>() {
                @Override
                public void onError(Exception e) {
                    SingleToastUtil.showToast("发送失败");
                }

                @Override
                public void onResponse(ServiceResult response) {
                    if (response.isSuccess()) {
                        SingleToastUtil.showToast("发送成功");
                        AvRoomDataManager.get().setBroadCastMsgContent(send.getContent());
                        CoreUtils.send(new BroadcastMsgEvent.OnBroadcastSendDialogNeedDismiss());
                    }
                }
            });
        }
    }
}
