package com.tongdaxing.xchat_core.home;

import android.support.annotation.NonNull;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.log.LogHelper;
import com.tcloud.core.thread.ExecutorCenter;
import com.tcloud.core.thread.WorkRunnable;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.UpLoadFileEvent;
import com.tongdaxing.xchat_core.result.BannerResult;
import com.tongdaxing.xchat_core.result.HomeResult;
import com.tongdaxing.xchat_core.result.HomeTabResult;
import com.tongdaxing.xchat_core.result.HomeV2Result;
import com.tongdaxing.xchat_core.result.RankingResult;
import com.tongdaxing.xchat_core.result.TabResult;
import com.tongdaxing.xchat_core.utils.Logger;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * @author zhouxiangfeng
 * @date 2017/5/17
 */

public class HomeCoreImpl extends AbstractBaseCore implements IHomeCore {
    private static final String TAG = "HomeCoreImpl";

    private List<TabInfo> mTabInfoList;
    private LogHelper mLogHelper;
    private long mUserId;
    private String mFeedbackDesc;
    private String mContact;
    private boolean mIsSilentUpload = false;

    public HomeCoreImpl() {
        CoreUtils.register(this);
    }

    @Override
    public List<TabInfo> getMainTabInfos() {
        return mTabInfoList;
    }

    @Override
    public void setMainTabInfos(List<TabInfo> tabInfoList) {
        this.mTabInfoList = tabInfoList;
    }

    @Override
    public void getHomeData(final int page, int pageSize) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        requestParam.put("pageNum", String.valueOf(page));
        requestParam.put("pageSize", String.valueOf(pageSize));
        ResponseListener listener = new ResponseListener<HomeResult>() {
            @Override
            public void onResponse(HomeResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_DATA, response.getData(), page);
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_GET_HOME_LIST_ERROR, response.getMessage(), page);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_GET_HOME_LIST_ERROR, error.getErrorStr(), page);
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getMainHotData(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, listener, errorListener,
                        HomeResult.class, Request.Method.GET);

    }

    @Override
    public void getHomePartyData(final int tabType) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("type", String.valueOf(3));
        ResponseListener Listener = new ResponseListener<HomeV2Result>() {
            @Override
            public void onResponse(HomeV2Result response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_PARTY_DATA, response.getData(), tabType);
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_PARTY_DATA_FAIL, response.getMessage(), tabType);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                //Log.i(TAG, "onErrorResponse: ");
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_PARTY_DATA_FAIL, error.getErrorStr(), tabType);
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRoomListV2(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        HomeV2Result.class, Request.Method.GET);
    }

    @Override
    public void getLightChatData(final int tabType) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("type", String.valueOf(2));
        ResponseListener Listener = new ResponseListener<HomeV2Result>() {
            @Override
            public void onResponse(HomeV2Result response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_LIGHT_CHAT_DATA, response.getData(), tabType);
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_LIGHT_CHAT_DATA_FAIL, response.getMessage(), tabType);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_LIGHT_CHAT_DATA_FAIL, error.getErrorStr(), tabType);
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRoomListV2(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        HomeV2Result.class, Request.Method.GET);
    }

    @Override
    public void getHotData(final int tabType) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("type", String.valueOf(1));
        ResponseListener Listener = new ResponseListener<HomeV2Result>() {
            @Override
            public void onResponse(HomeV2Result response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOT_DATA, response.getData(), tabType);
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOT_DATA_FAIL, response.getMessage(), tabType);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOT_DATA_FAIL, error.getErrorStr(), tabType);
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRoomListV2(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        HomeV2Result.class, Request.Method.GET);
    }

    @Override
    public void commitFeedback(long uid, String feedbackDesc, String contact) {
        this.mUserId = uid;
        this.mFeedbackDesc = feedbackDesc;
        this.mContact = contact;
        // 上传日志
        mIsSilentUpload = false;
        uploadLogFile();
    }

    private void commitFeedback(String url) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(mUserId));
        requestParam.put("feedbackDesc", mFeedbackDesc);
        requestParam.put("img", url);
        requestParam.put("contact", mContact);
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener Listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_COMMIT_BACK);

                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_COMMIT_BACK, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_COMMIT_BACK_FAIL, error.getMessage());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.commitFeedback(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                requestParam, Listener, errorListener,
                ServiceResult.class, Request.Method.POST
        );
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUploadLogSuccess(UpLoadFileEvent.OnUploadFile uploadFile) {
        L.info(TAG, "onUploadLogSuccess: %b, filePath: %s", uploadFile.isSuccess(), uploadFile.getFilePath());
        if (uploadFile.isSuccess()) {
            commitFeedback(uploadFile.getFilePath());
        } else {
            notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_COMMIT_BACK_FAIL, uploadFile.getFilePath());
        }
    }

    @Subscribe
    public void onUploadLogFile(UpLoadFileEvent.OnSilentUploadLogFile uploadLogFile) {
        L.info(TAG, "onUploadLogFile");
        mIsSilentUpload = true;
        uploadLogFile();
    }

    private void uploadLogFile() {
        ExecutorCenter.getInstance().post(new WorkRunnable() {
            @NonNull
            @Override
            public String getTag() {
                return "uploadLogFile";
            }

            @Override
            public void run() {
                try {
                    if (mLogHelper == null) {
                        mLogHelper = new LogHelper();
                    }
                    File logFile = mLogHelper.getLog();
                    CoreManager.getCore(IFileCore.class).uploadPhoto(logFile);
                    L.info(TAG, "uploadLogFile fileName: %s", logFile.getPath());
                } catch (Exception e) {
                    CoreUtils.crashIfDebug(e, "uploadLogFile Exception");
                    L.info(TAG, "uploadLogFile Exception:", e);
                }
            }
        });
    }

    @Override
    public void getHomeBanner() {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();

        ResponseListener Listener = new ResponseListener<BannerResult>() {
            @Override
            public void onResponse(BannerResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_BANNER_LIST, response.getData());
                        // Log.i(TAG, "onResponse: ");
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_BANNER_LIST_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                //Log.i(TAG, "onErrorResponse: ");
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_BANNER_LIST_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getBannerList(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        BannerResult.class, Request.Method.GET);

    }

    @Override
    public void getRankingData() {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        ResponseListener Listener = new ResponseListener<RankingResult>() {
            @Override
            public void onResponse(RankingResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_RANKING_LIST, response.getData());
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_RANKING_LIST_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_RANKING_LIST_ERROR, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getHomeRanking(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        RankingResult.class, Request.Method.GET);
    }

    @Override
    public void getMainTabData() {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        ResponseListener listener = new ResponseListener<TabResult>() {
            @Override
            public void onResponse(TabResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        setMainTabInfos(response.getData());
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_TAB_LIST, response.getData());
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_TAB_LIST_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_TAB_LIST_ERROR, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getMainTabList(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, listener, errorListener,
                        TabResult.class, Request.Method.GET);
    }

    @Override
    public void getMainDataByTab(final int tagId, final int pageNum, int pageSize) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("tagId", String.valueOf(tagId));
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("pageNum", String.valueOf(pageNum));
        requestParam.put("pageSize", String.valueOf(pageSize));
        ResponseListener listener = new ResponseListener<HomeTabResult>() {
            @Override
            public void onResponse(HomeTabResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_DATA_BY_TAB, response.getData(), tagId, pageNum);
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_DATA_BY_TAB_ERROR, response.getMessage(), tagId, pageNum);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_DATA_BY_TAB_ERROR, error.getErrorStr(), tagId, pageNum);
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getMainDataByTab(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, listener, errorListener,
                        HomeTabResult.class, Request.Method.GET);
    }

    @Override
    public void getSortDataByTab(final int tagId, final int pageNum, int pageSize) {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("tagId", String.valueOf(tagId));
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("pageNum", String.valueOf(pageNum));
        requestParam.put("pageSize", String.valueOf(pageSize));
        ResponseListener listener = new ResponseListener<HomeTabResult>() {
            @Override
            public void onResponse(HomeTabResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_SORT_GET_DATA_BY_TAB, response.getData(), tagId, pageNum);
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_SORT_GET_DATA_BY_TAB_ERROR, response.getMessage(), tagId, pageNum);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_SORT_GET_DATA_BY_TAB_ERROR, error.getErrorStr(), tagId, pageNum);
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getMainDataByTab(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, listener, errorListener,
                        HomeTabResult.class, Request.Method.GET);
    }


    @Override
    public void getMainDataByMenu() {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        requestParam.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        ResponseListener listener = new ResponseListener<TabResult>() {
            @Override
            public void onResponse(TabResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.onGetHomeDataByMenuSuccess, response.getData());
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.onGetHomeDataByMenuFail, response.getErrorMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.onGetHomeDataByMenuFail);
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getMainDataByMenu(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, listener, errorListener,
                        HomeTabResult.class, Request.Method.GET);
    }


//    @Override
//    public void commitFeedback(long uid, String feedbackDesc, String img, String contact) {
//    }

}




