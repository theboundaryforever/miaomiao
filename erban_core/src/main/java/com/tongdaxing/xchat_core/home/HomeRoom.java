package com.tongdaxing.xchat_core.home;

import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.google.gson.annotations.SerializedName;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.List;

/**
 * @author zhouxiangfeng
 * @date 2017/5/17
 */

public class HomeRoom extends RoomInfo implements MultiItemEntity, Parcelable {
    //性别 1:男 2：女 0 ：未知
    private int gender;
    //头像
    private String avatar;

    private String nick;

    private String customTag;

    private long erbanNo;

    private int seqNo;

    public int showLine = 0;

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public String badge;//角标相关的

    /**
     * 跳转类型 默认是房间 1 房间, 2 栏目
     */
    private int targetType;
    @SerializedName(value = "bannerInfos", alternate = {"listBanner"})
    private List<BannerInfo> bannerInfos;
    private int itemType;//1是banner

    private int pos;//直播页控制banner位置

    public int getTargetType() {
        return targetType;
    }

    public void setTargetType(int targetType) {
        this.targetType = targetType;
    }

    protected HomeRoom(Parcel in) {
        super(in);
        this.gender = in.readInt();
        this.avatar = in.readString();
        this.nick = in.readString();
        this.customTag = in.readString();
        this.erbanNo = in.readLong();
        this.seqNo = in.readInt();
        this.showLine = in.readInt();
        this.bannerInfos = in.createTypedArrayList(BannerInfo.CREATOR);
        this.badge = in.readString();
        this.itemType = in.readInt();
        this.pos = in.readInt();
    }

    public String getCustomTag() {
        return customTag;
    }

    public void setCustomTag(String customTag) {
        this.customTag = customTag;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public HomeRoom(int type) {
        super(type);
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    public List<BannerInfo> getBannerInfos() {
        return bannerInfos;
    }

    public void setBannerInfos(List<BannerInfo> bannerInfos) {
        this.bannerInfos = bannerInfos;
    }

    @Override
    public String toString() {
        return "HomeRoom{" +
                "gender=" + gender +
                ", avatar='" + avatar + '\'' +
                ", nick='" + nick + '\'' +
                ", customTag='" + customTag + '\'' +
                ", erbanNo=" + erbanNo +
                ", seqNo=" + seqNo +
                ", showLine=" + showLine +
                ", bannerInfos=" + bannerInfos +
                ", targetType=" + targetType +
                ", badge='" + badge + '\'' +
                ", itemType=" + itemType +
                ", title='" + title + '\'' +
                ", roomPwd='" + roomPwd + '\'' +
                ", tagId=" + tagId +
                ", tagPict='" + tagPict + '\'' +
                ", onlineNum=" + onlineNum +
                ", pos=" + pos +
                '}';
    }

    public int getPos() {
        return pos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.gender);
        dest.writeString(this.avatar);
        dest.writeString(this.nick);
        dest.writeString(this.customTag);
        dest.writeLong(this.erbanNo);
        dest.writeInt(this.seqNo);
        dest.writeInt(this.showLine);
        dest.writeTypedList(this.bannerInfos);
        dest.writeString(this.badge);
        dest.writeInt(this.itemType);
        dest.writeInt(this.pos);
    }

    public static final Creator<HomeRoom> CREATOR = new Creator<HomeRoom>() {
        @Override
        public HomeRoom createFromParcel(Parcel source) {
            return new HomeRoom(source);
        }

        @Override
        public HomeRoom[] newArray(int size) {
            return new HomeRoom[size];
        }
    };
}
