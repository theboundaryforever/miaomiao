package com.tongdaxing.xchat_core.music;

import android.text.TextUtils;

import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerDbCore;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.FileUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.io.File;
import java.util.List;

public class MusicCoreImpl extends AbstractBaseCore implements IMusicCore {

    /**
     * 复制音乐到缓存文件夹中
     */
    @Override
    public void copyFileToCacheFolder(MusicLocalInfo musicLocalInfo) {
        File fileFolderPath = new File(getUserStoragePath());
        String filePath = FileUtil.copyFileNoRepeat(musicLocalInfo.getLocalUri(), fileFolderPath.getAbsolutePath());
        if (!TextUtils.isEmpty(filePath)) {
            MusicLocalInfo cacheMusicInfo = new MusicLocalInfo();
            cacheMusicInfo.copy(musicLocalInfo);
            cacheMusicInfo.setLocalUri(filePath);
            cacheMusicInfo.setOnCache(true);
            CoreManager.getCore(IPlayerDbCore.class).updateFromLocalMusics(cacheMusicInfo);
        }
    }

    /**
     * 删除歌曲的存储文件
     *
     * @param musicLocalInfo
     */
    @Override
    public void deleteFileFromCacheFolder(MusicLocalInfo musicLocalInfo) {
        MusicLocalInfo cacheInfo = getCacheMusicInfo(musicLocalInfo.getSongName(), musicLocalInfo.getSingerName(), musicLocalInfo.getFileSize());
        if (cacheInfo != null) {
            File file = new File(cacheInfo.getLocalUri());
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /**
     * 获取用户存储路径
     */
    @Override
    public String getUserStoragePath() {
        long userId = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        return BasicConfig.INSTANCE.getAppContext().getFilesDir().getAbsolutePath() + "/local_music_" + userId;
    }

    @Override
    public String getCurrentUserFolderName() {
        long userId = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        return "local_music_" + userId;
    }

    /**
     * 获取音乐缓存大小
     */
    @Override
    public long getMusicCacheSize() {
        long count = 0;
        File file = new File(getUserStoragePath());
        File[] files = file.listFiles();
        if (files == null || files.length == 0) {
            return count;
        }
        for (File file1 : files) {
            count += file1.length();
        }
        return count;
    }

    @Override
    public void delMusicCache() {
        CoreManager.getCore(IPlayerCore.class).stop();//停止音乐播放，解决播放音乐过程中，清除缓存闪退bug
        CoreManager.getCore(IPlayerCore.class).clearPlayerListMusicInfos();//清空播放列表所有信息
        File file = new File(getUserStoragePath());
        File[] files = file.listFiles();
        if (files == null || files.length == 0) {
            return;
        }
        for (File file1 : files) {
            if (file1.exists()) {
                file1.delete();
            }
            CoreManager.getCore(IPlayerDbCore.class).deleteFromLocalMusics(file1.getAbsolutePath());
        }
    }

    @Override
    public void setMusicStatus(List<MyMusicInfo> myMusicInfoList) {
        for (int i = 0; i < myMusicInfoList.size(); i++) {
            MyMusicInfo myMusicInfo = myMusicInfoList.get(i);
            MusicLocalInfo musicLocalInfo = CoreManager.getCore(IMusicCore.class).getCacheMusicInfo(myMusicInfo.getTitle(), myMusicInfo.getArtist(), myMusicInfo.getSize());
            if (musicLocalInfo != null) {//在缓存中
                if (musicLocalInfo.getLocalUri().contains(CoreManager.getCore(IMusicCore.class).getCurrentUserFolderName())) {//缓存中的音乐文件是否属于该用户
                    File file = new File(musicLocalInfo.getLocalUri());
                    if (file.exists()) {//如果本地文件存在
                        if (myMusicInfoList.get(i).isUserSong()) {
                            myMusicInfo.setLocalUri(musicLocalInfo.getLocalUri());
                            if (CoreManager.getCore(IPlayerCore.class).getState() == IPlayerCore.STATE_PLAY &&
                                    CoreManager.getCore(IPlayerCore.class).getCurrent().getLocalUri().equals(myMusicInfo.getLocalUri())) {
                                myMusicInfo.setFileStatus(2);
                            } else {
                                myMusicInfo.setFileStatus(1);
                            }
                            CoreManager.getCore(IPlayerDbCore.class).updateFromLocalMusic(musicLocalInfo, String.valueOf(myMusicInfo.getId()), 1);
                            CoreManager.getCore(IPlayerCore.class).addMusicToPlayerList(musicLocalInfo);//添加到播放列表
                        }else{
                            boolean isDownload = CoreManager.getCore(IMusicDownloaderCore.class).checkDownloadingQueue(myMusicInfoList.get(i).getUrl());
                            myMusicInfo.setFileStatus(isDownload ? 3 : 0);
                        }
                    } else {
                        boolean isDownload = CoreManager.getCore(IMusicDownloaderCore.class).checkDownloadingQueue(myMusicInfoList.get(i).getUrl());
                        myMusicInfo.setFileStatus(isDownload ? 3 : 0);
                    }
                } else {
                    boolean isDownload = CoreManager.getCore(IMusicDownloaderCore.class).checkDownloadingQueue(myMusicInfoList.get(i).getUrl());
                    myMusicInfo.setFileStatus(isDownload ? 3 : 0);
                }
            } else {
                boolean isDownload = CoreManager.getCore(IMusicDownloaderCore.class).checkDownloadingQueue(myMusicInfoList.get(i).getUrl());
                myMusicInfo.setFileStatus(isDownload ? 3 : 0);
            }
        }
    }

    @Override
    public MusicLocalInfo getCacheMusicInfo(String songName, String singerName, long fileSize) {
        List<MusicLocalInfo> list = CoreManager.getCore(IPlayerDbCore.class).requestLocalMusicInfoAll(songName, singerName, fileSize, true);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getLocalUri().contains(CoreManager.getCore(IMusicCore.class).getCurrentUserFolderName())) {
                return list.get(i);
            }
        }
        return null;
    }


}
















