package com.tongdaxing.xchat_core.music;

import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * Function:
 * Author: Edward on 2019/2/18
 */
public interface IMusicCoreClient extends ICoreClient {
    public static final String DEL_OPERATION = "delOperationRefresh";

    /**
     * 删除操作刷新
     *
     * @param isHidePlayer 是否要隐藏播放器
     */
    void delOperationRefresh(long id, boolean isHidePlayer);
}
