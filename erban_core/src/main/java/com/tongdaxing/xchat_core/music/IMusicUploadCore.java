package com.tongdaxing.xchat_core.music;

import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/2/20
 */
public interface IMusicUploadCore extends IBaseCore {
    void addUploadQueue(final MusicLocalInfo localMusicInfo);

    /**
     * 删除所有的下载中队列，以及对应的缓存信息<临时文件暂时不删除>
     */
    void deleteAllDownQueueAsWellMusicInfoAsTempFile();

    /**
     * 下载的歌曲被移除播放列表
     * @param uri
     */
    void deleteHotMusicDownloadingFromPlayList(String uri);

    /**
     * 检测HotMusicInfo是否处于下载队列中
     * @param uri
     */
    boolean checkHotMusicIsDownloading(String uri);

    /**
     * 查询下载中的LocalMusicIinfo队列的方法
     * @return
     */
    List<MusicLocalInfo> queryDownloadingMusicListInPlayList();

}
