package com.tongdaxing.xchat_core.music;

import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_core.player.bean.MyMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

import java.util.List;

public interface IMusicCore extends IBaseCore {
    public final int TEMP = 1;

    /**
     * 复制音乐到缓存文件夹中
     */
    void copyFileToCacheFolder(MusicLocalInfo musicLocalInfo);

    /**
     * @param musicLocalInfo
     */
    void deleteFileFromCacheFolder(MusicLocalInfo musicLocalInfo);

    /**
     * 获取对应缓存里的歌曲信息
     */
    MusicLocalInfo getCacheMusicInfo(String songName, String singerName, long fileSize);


    String getUserStoragePath();

    String getCurrentUserFolderName();

    /**
     * 获取音乐缓存大小
     */
    long getMusicCacheSize();

    /**
     * 删除音乐缓存
     */
    void delMusicCache();

    /**
     * 设置音乐状态
     *
     * @param myMusicInfoList
     */
    void setMusicStatus(List<MyMusicInfo> myMusicInfoList);


}
