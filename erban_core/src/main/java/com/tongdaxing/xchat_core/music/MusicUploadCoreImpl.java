package com.tongdaxing.xchat_core.music;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.utils.UrlSafeBase64;
import com.tongdaxing.xchat_core.common.QiNiuFileUploadProfile;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.ProgressInfo;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestProcessor;

import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Function:
 * Author: Edward on 2019/2/20
 */
public class MusicUploadCoreImpl extends AbstractBaseCore implements IMusicUploadCore {

    private final String TAG = MusicDownloaderCoreImpl.class.getSimpleName();

    //    private Map<String, HotMusicInfo> uploadHotMusicInfoQueue = new HashMap<>();
    private Map<String, MusicLocalInfo> uploadLocalMusicInfoQueue = new HashMap<>();
    //可以扩展实现单个下载请求的停止、暂停、恢复下载
//    private Map<String, DownloadRequest> downloadRequestMap = new HashMap<>();

    //可以扩展实现暂停所有下载、恢复所有下载、停止所有下载
    private RequestProcessor mCommonProcessor;
    private RequestProcessor.RequestFilter allRequestFilter = new RequestProcessor.RequestFilter() {
        @Override
        public boolean apply(Request<?> request) {
//            return null != request.getTag() && !TextUtils.isEmpty(request.getTag().toString());
            return true;
        }
    };

//    private long tempLocalId = 0;

    private ExecutorService downloadExService;

    public MusicUploadCoreImpl() {
//        mCommonProcessor = new DefaultRequestProcessor(100, "Music_upload_Queue");
//        mCommonProcessor.start();
//        tempLocalId = 0;

        //集中发生在Android7.0及以上的华为手机（EmotionUI_5.0及以上）的OOM
        //java.lang.OutOfMemoryError: pthread_create (1040KB stack) failed: Out of memory
        //这里的线程池 不能够一次性分配太多 测试中100个在HUA WEI FRD-AL00 Android 8的系统上可行
        //参考https://www.jianshu.com/p/e574f0ffdb42
//        downloadExService = Executors.newFixedThreadPool(100);
//        updateExService = Executors.newFixedThreadPool(100);
        //Executors各类型线程池的区别参考https://www.cnblogs.com/zhujiabin/p/5404771.html
        downloadExService = Executors.newFixedThreadPool(3);
    }

    /**
     * 添加上传队列
     */
    @Override
    public void addUploadQueue(MusicLocalInfo localMusicInfo) {
        notifyClients(IMusicUploadCoreClient.class, IMusicUploadCoreClient.METHOD_ON_MUSIC_START_UPLOAD, localMusicInfo);
        MusicLocalInfo uploadMusicInfo = new MusicLocalInfo();
        uploadMusicInfo.copy(localMusicInfo);
        //hot列表下载则默认添加进播放列表
//        localMusicInfo.setInPlayerList(true);
        //缓存下载队列以备更新下载进度
        uploadLocalMusicInfoQueue.put(uploadMusicInfo.getLocalUri(), uploadMusicInfo);
        downloadExService.execute(new UploadRunnable(uploadMusicInfo.getLocalUri()));
    }

    private class UploadRunnable implements Runnable {
        private String localUri;

        UploadRunnable(String localUri) {
            this.localUri = localUri;
        }

        @Override
        public void run() {
            try {
                // 1 构造上传策略
                JSONObject _json = new JSONObject();
                long _deadline = System.currentTimeMillis() / 1000 + 3600;
                _json.put("deadline", _deadline);// 有效时间为一个小时
                _json.put("scope", QiNiuFileUploadProfile.bucket);
                String _encodedPutPolicy = UrlSafeBase64.encodeToString(_json
                        .toString().getBytes());
                byte[] _sign = HmacSHA1Encrypt(_encodedPutPolicy, QiNiuFileUploadProfile.secretKey);
                String _encodedSign = UrlSafeBase64.encodeToString(_sign);
                String _uploadToken = QiNiuFileUploadProfile.accessKey + ':' + _encodedSign + ':'
                        + _encodedPutPolicy;
                UploadManager uploadManager = new UploadManager();
                File musicFile = new File(localUri);
                uploadManager.put(musicFile, null, _uploadToken,
                        new UpCompletionHandler() {
                            @Override
                            public void complete(String key, ResponseInfo info,
                                                 final JSONObject response) {
                                try {
                                    String imgName = response.getString("key");
                                    MusicLocalInfo musicLocalInfo = uploadLocalMusicInfoQueue.get(localUri);
                                    musicLocalInfo.setRemoteUri(QiNiuFileUploadProfile.accessUrl + imgName + QiNiuFileUploadProfile.picprocessing);
                                    onMusicDownloadResponse(localUri, musicLocalInfo);
                                } catch (final Exception e) {
                                    onMusicDownloadError(e.getMessage(), localUri, uploadLocalMusicInfoQueue.get(localUri));
                                }

                            }
                        }, null);
            } catch (Exception e) {
                e.printStackTrace();
                onMusicDownloadError(e.getMessage(), localUri, uploadLocalMusicInfoQueue.get(localUri));
            }
        }
    }

    /**
     * 使用 HMAC-SHA1 签名方法对encryptText进行签名
     *
     * @param encryptText 被签名的字符串
     * @param encryptKey  密钥
     * @return
     * @throws Exception
     */
    public static byte[] HmacSHA1Encrypt(String encryptText, String encryptKey)
            throws Exception {
        byte[] data = encryptKey.getBytes("UTF-8");
        // 根据给定的字节数组构造一个密钥,第二参数指定一个密钥算法的名称
        SecretKey secretKey = new SecretKeySpec(data, "HmacSHA1");
        // 生成一个指定 Mac 算法 的 Mac 对象
        Mac mac = Mac.getInstance("HmacSHA1");
        // 用给定密钥初始化 Mac 对象
        mac.init(secretKey);
        byte[] text = encryptText.getBytes("UTF-8");
        // 完成 Mac 操作
        return mac.doFinal(text);
    }

    /**
     * 上传出错
     */
    private void onMusicDownloadError(String error, String localUri, MusicLocalInfo localMusicInfo) {
        uploadLocalMusicInfoQueue.remove(localUri);
        if (!TextUtils.isEmpty(error)) {
            //通知播放列表移除item，通知热门曲库列表更新下载状态
            notifyClients(IMusicUploadCoreClient.class, IMusicUploadCoreClient.METHOD_ON_MUSIC_UPLOAD_ERROR, error, localMusicInfo);
        }
    }

    /**
     * 上传完成
     */
    private void onMusicDownloadResponse(String localUri, MusicLocalInfo localMusicInfo) {
        uploadLocalMusicInfoQueue.remove(localUri);
        Message msg = Message.obtain();
        msg.what = Msg_What_Downloaded_Completed_No_Scaned;
        msg.obj = localMusicInfo;
        musicDownloadHandler.sendMessage(msg);
    }


    private MusicDownloadHandler musicDownloadHandler = new MusicDownloadHandler(this);

    private static final int Msg_What_Local_Music_Info_Add = 0;
    private static final int Msg_What_Hot_Music_Downloaded_Scaned = 1;
    private static final int Msg_What_Hot_Music_Update_To_Local = 2;
    private static final int Msg_What_Downloaded_Completed_No_Scaned = 3;

    private class MusicDownloadHandler extends Handler {
        private WeakReference<MusicUploadCoreImpl> mWeakReference;

        MusicDownloadHandler(MusicUploadCoreImpl playerCore) {
            mWeakReference = new WeakReference<>(playerCore);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            MusicUploadCoreImpl playerCore = mWeakReference.get();
            if (playerCore == null) {
                return;
            }
            switch (msg.what) {
                case Msg_What_Hot_Music_Update_To_Local:
                    MusicLocalInfo localMInfo = (MusicLocalInfo) msg.getData().getSerializable("localMusicInfo");
//                    CoreManager.getCore(IPlayerCore.class).updateLocalMusicInfo(localMInfo);
                    hasUpdateCount += 1;
                    if (hasUpdateCount == lastHotMusicInfoNeedUpdateCount) {
                        notifyClients(IMusicUploadCoreClient.class, IMusicUploadCoreClient.METHOD_ON_HOT_UPDATE_LOCAL_COMPLEED);
                        lastHotMusicInfoNeedUpdateCount = 0;
                        hasUpdateCount = 0;
                    }
                    break;
//                case Msg_What_Hot_Music_Downloaded_Scaned:
//                    HotMusicInfo hotMusicInfo = (HotMusicInfo) msg.obj;
//                    notifyClients(IMusicUploadCoreClient.class,
//                            IMusicUploadCoreClient.METHOD_ON_MUSIC_DOWNLOAD_COMPLETED,
//                            hotMusicInfo);
//                    break;
                case Msg_What_Downloaded_Completed_No_Scaned:
                    MusicLocalInfo hotMusicInfo1 = (MusicLocalInfo) msg.obj;
                    notifyClients(IMusicUploadCoreClient.class,
                            IMusicUploadCoreClient.METHOD_ON_MUSIC_UPLOAD_COMPLETED,
                            hotMusicInfo1);
                    break;
                case Msg_What_Local_Music_Info_Add:
                    MusicLocalInfo musicInfo = (MusicLocalInfo) msg.obj;
                    MusicLocalInfo oldMusicInfo = (MusicLocalInfo) msg.getData().getSerializable("oldMusicInfo");
//                    CoreManager.getCore(IPlayerCore.class).replaceMusicInPlayerList(oldMusicInfo,musicInfo);
//                    CoreManager.getCore(IPlayerDbCore.class).insertOrUpdateLocalMusicInfo(musicInfo);
                    notifyClients(IMusicUploadCoreClient.class,
                            IMusicUploadCoreClient.METHOD_ON_MUSIC_DOWNLOAD_COMPLE_INFO_UPDATED,
                            musicInfo);
                    break;
                default:
            }
        }
    }

    /**
     * 下载进度更新
     *
     * @param info
     * @param localMusicInfo
     * @param hotMusicInfo
     */
    private void onMusicDownloadProgress(MusicLocalInfo localMusicInfo, HotMusicInfo hotMusicInfo, ProgressInfo info) {
        int progress = (int) (info.getProgress() * 100 / info.getTotal());
        //通知界面更新下载进度
        notifyClients(IMusicUploadCoreClient.class, IMusicUploadCoreClient.METHOD_ON_MUSIC_DOWNLOAD_PROGRESS_UPDATE,
                localMusicInfo, hotMusicInfo, progress);
    }


    /**
     * 删除所有的下载中队列，以及对应的缓存信息<临时文件暂时不删除>
     */
    @Override
    public void deleteAllDownQueueAsWellMusicInfoAsTempFile() {

    }

    /**
     * 下载的歌曲被移除播放列表
     *
     * @param url
     */
    @Override
    public void deleteHotMusicDownloadingFromPlayList(String url) {
        MusicLocalInfo musicInfo = uploadLocalMusicInfoQueue.get(url);
        if (null != musicInfo) {
            CoreManager.getCore(IPlayerCore.class).deleteMusicFromPlayerList(musicInfo);
            musicInfo.setInPlayerList(false);
        }
    }

    /**
     * 检测HotMusicInfo是否处于下载队列中
     *
     * @param uri
     */
    @Override
    public boolean checkHotMusicIsDownloading(String uri) {
        return uploadLocalMusicInfoQueue.get(uri) != null;
    }

    /**
     * 查询下载中的LocalMusicIinfo队列的方法
     *
     * @return
     */
    @Override
    public List<MusicLocalInfo> queryDownloadingMusicListInPlayList() {
        List<MusicLocalInfo> inPlayerListLocalMusicInfos = new ArrayList<>();
        for (MusicLocalInfo musicInfo : uploadLocalMusicInfoQueue.values()) {
            if (musicInfo.isInPlayerList()) {
                inPlayerListLocalMusicInfos.add(musicInfo);
            }
        }
        return inPlayerListLocalMusicInfos;
    }

    private int lastHotMusicInfoNeedUpdateCount = 0;
    private int hasUpdateCount = 0;

}
