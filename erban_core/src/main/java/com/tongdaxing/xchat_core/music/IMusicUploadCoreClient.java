package com.tongdaxing.xchat_core.music;

import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.bean.MusicLocalInfo;
import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * Function:
 * Author: Edward on 2019/2/20
 */
public interface IMusicUploadCoreClient extends ICoreClient {
    public static final String METHOD_ON_MUSIC_START_UPLOAD = "onHotMusicStartUpload";

    void onHotMusicStartUpload(MusicLocalInfo MusicLocalInfo);


    public static final String METHOD_ON_MUSIC_DOWNLOAD_PROGRESS_UPDATE = "onHotMusicDownloadProgressUpdate";

    /**
     * 上传进度通知
     *
     * @param hotMusicInfo
     * @param hotMusicInfo
     * @param progress     当前上传进度
     */
    void onHotMusicDownloadProgressUpdate(MusicLocalInfo MusicLocalInfo, HotMusicInfo hotMusicInfo, int progress);


    public static final String METHOD_ON_MUSIC_UPLOAD_COMPLETED = "onHotMusicUploadCompleted";

    /**
     * 上传完成通知--提示用户歌曲已经上传完成
     */
    void onHotMusicUploadCompleted(MusicLocalInfo MusicLocalInfo);

    public static final String METHOD_ON_MUSIC_UPLOAD_ERROR = "onMusicUploadError";

    /**
     * 上传出错通知
     *
     * @param msg
     * @param MusicLocalInfo
     */
    void onMusicUploadError(String msg, MusicLocalInfo MusicLocalInfo);

    public static final String METHOD_ON_MUSIC_DOWNLOAD_COMPLE_INFO_UPDATED = "onHotMusicDownloadCompleInfoUpdated";

    /**
     * 上传的歌曲本地信息更新通知
     *
     * @param MusicLocalInfo 更新后的MusicLocalInfo
     */
    void onHotMusicDownloadCompleInfoUpdated(MusicLocalInfo MusicLocalInfo, HotMusicInfo hotMusicInfo);

    public static final String METHOD_ON_HOT_UPDATE_LOCAL_COMPLEED = "onHotMusicInfoUpdateToLocalCompleted";

    /**
     * 热门曲库列表已经同步本地已经上传好的mp3信息了
     */
    void onHotMusicInfoUpdateToLocalCompleted();
}
