package com.tongdaxing.xchat_core.share;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.im.room.IIMRoomCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;
import com.tongdaxing.xchat_core.result.ShareRedPacketResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * Created by chenran on 2017/8/14.
 */

public class ShareCoreImpl extends AbstractBaseCore implements IShareCore {
    public ShareCoreImpl() {
        CoreManager.addClient(this);
    }

    @Override
    public void shareH5(final WebViewInfo webViewInfo, Platform platform) {
        L.debug("ShareCoreImpl", "shareH5");
        if (null != webViewInfo && platform != null) {
            Platform.ShareParams sp = new Platform.ShareParams();
            sp.setText(webViewInfo.getDesc());
            sp.setTitle(webViewInfo.getTitle());
            sp.setImageUrl(webViewInfo.getImgUrl());
            //QQ空间分享
            sp.setSite(webViewInfo.getDesc());
            sp.setSiteUrl(webViewInfo.getShowUrl() + "?shareUid=" + String.valueOf(CoreManager
                    .getCore(IAuthCore.class).getCurrentUid()));
            //QQ分享
            sp.setTitleUrl(webViewInfo.getShowUrl() + "?shareUid=" + String.valueOf(CoreManager
                    .getCore(IAuthCore.class).getCurrentUid()));
            //微信朋友圈分享
            sp.setUrl(webViewInfo.getShowUrl() + "?shareUid=" + String.valueOf(CoreManager
                    .getCore(IAuthCore.class).getCurrentUid()));
            sp.setShareType(Platform.SHARE_WEBPAGE);
            platform.setPlatformActionListener(new PlatformActionListener() {
                @Override
                public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                    String url = UriProvider.getLotteryActivityPage();
                    if (url.contains("/mm/luckdraw/index.html") &&
                            webViewInfo.getShowUrl() != null &&
                            webViewInfo.getShowUrl().contains("/mm/luckdraw/index.html")) {
                        reportShare(888, platform);
                    } else {
                        reportShare(1, platform);
                    }
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_SHARE_H5, webViewInfo.getShowUrl());
                }

                @Override
                public void onError(Platform platform, int i, Throwable throwable) {
                    L.error("ShareCoreImpl", "shareH5", throwable);
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_HSARE_H5_FAIL);
                }

                @Override
                public void onCancel(Platform platform, int i) {
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_HSARE_H5_CANCEL);
                }
            });
            platform.share(sp);
        }
    }

    @Override
    public void sharePage(WebViewInfo webViewInfo, Platform platform) {
        shareH5(webViewInfo, platform);
    }

    @Override
    public void shareRoom(Platform platform,
                          final long roomUid, String title) {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(roomUid);//房间主人信息
        //无UI API
        if (userInfo != null && platform != null) {
            Platform.ShareParams sp = new Platform.ShareParams();
            sp.setText(userInfo.getNick() + "带你走进Ta的房间——" + title);
            sp.setTitle("喵喵，你想要的我们都有！");
            sp.setImageUrl(userInfo.getAvatar());
            //QQ空间分享
            sp.setSite(userInfo.getNick() + "带你走进Ta的房间——" + title);
            String uid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            String roomuid = String.valueOf(roomUid);
            sp.setSiteUrl(UriProvider.IM_SERVER_URL + "/mm/share/share.html?shareUid=" + uid + "&uid=" + roomuid);
            //QQ分享
            sp.setTitleUrl(UriProvider.IM_SERVER_URL + "/mm/share/share.html?shareUid=" + uid + "&uid=" + roomuid);
            //微信朋友圈分享
            sp.setUrl(UriProvider.IM_SERVER_URL + "/mm/share/share.html?shareUid=" + uid + "&uid=" + roomuid);
            sp.setShareType(Platform.SHARE_WEBPAGE);


            platform.setPlatformActionListener(new PlatformActionListener() {
                @Override
                public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                    sendShareRoomTipMsg(roomUid);
                    reportShare(1, platform);
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_SHARE_ROOM);
                }

                @Override
                public void onError(Platform platform, int i, Throwable throwable) {
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_SHARE_ROOM_FAIL);
                }

                @Override
                public void onCancel(Platform platform, int i) {
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_SHARE_ROOM_CANCEL);
                }
            });
            platform.share(sp);
        }
    }

    @Override
    public void reportShare(int sharePageId, Platform platform) {
        LogUtil.d("reportShare", "1");
        int shareType = 0;
        if (platform.getName() == Wechat.NAME) {
            shareType = 1;
        } else if (platform.getName() == WechatMoments.NAME) {
            shareType = 2;
        } else if (platform.getName() == QQ.NAME) {
            shareType = 3;
        } else if (platform.getName() == QZone.NAME) {
            shareType = 4;
        }
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("shareType", String.valueOf(shareType));
        params.put("sharePageId", sharePageId + "");
        params.put("token", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<ShareRedPacketResult>() {
            @Override
            public void onResponse(ShareRedPacketResult response) {
                if (response != null && response.isSuccess())
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_SHARE_REPORT);
//                ShareRedBagInfo data = response.getData();
//                LogUtil.d("reportShare",data+"");
//                if (data != null && data.isNeedAlert()) {
//
//                    RedPacketInfoV2 redPacketInfoV2 = new RedPacketInfoV2();
//                    redPacketInfoV2.setNeedAlert(data.isNeedAlert());
//                    redPacketInfoV2.setPacketNum(data.getPacketNum());
//                    redPacketInfoV2.setPacketName("");
//                    notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient.METHOD_ON_RECEIVE_NEW_PACKET, redPacketInfoV2);
//                }

            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {

            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getShareRedPacket(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ShareRedPacketResult.class, Request.Method.POST);
    }

    private void sendShareRoomTipMsg(long targetUid) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(targetUid);
        if (roomInfo != null && userInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            UserInfo myUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(myUid);

            RoomTipAttachment roomTipAttachment = new RoomTipAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP, CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_SHARE_ROOM);
            roomTipAttachment.setUid(myUid);
            roomTipAttachment.setNick(myUserInfo.getNick());
            roomTipAttachment.setTargetUid(targetUid);
            roomTipAttachment.setTargetNick(userInfo.getNick());

            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    // 自定义消息
                    roomTipAttachment
            );

            CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
        }
    }


}
