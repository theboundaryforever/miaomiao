package com.tongdaxing.xchat_core.auth;

import android.text.TextUtils;

import com.mob.MobSDK;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.linked.ILinkedCore;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.result.LoginResult;
import com.tongdaxing.xchat_core.result.RegisterResult;
import com.tongdaxing.xchat_core.result.TicketResult;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.utils.Logger;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.http_image.util.DeviceUuidFactory;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.DESUtils;
import com.tongdaxing.xchat_framework.util.util.VersionUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * Created by chenran on 2017/2/16.
 */

public class AuthCoreImpl extends AbstractBaseCore implements IAuthCore {
    private static final String TAG = "AuthCoreImpl";
    private AccountInfo currentAccountInfo;
    private TicketInfo ticketInfo;
    private boolean isRequestTicket;
    private ThirdUserInfo thirdUserInfo;
    private Platform wechat;
    private Platform qq;
    private String mInfo;
//    private AuthCoreImplHander handler = new AuthCoreImplHander(this);

    public boolean isRequestTicket() {
        return isRequestTicket;
    }

    public void setRequestTicket(boolean requestTicket) {
        isRequestTicket = requestTicket;
    }

    public AuthCoreImpl() {
        MobSDK.init(getContext());
        currentAccountInfo = DemoCache.readCurrentAccountInfo();
        ticketInfo = DemoCache.readTicketInfo();
        if (currentAccountInfo == null) {
            currentAccountInfo = new AccountInfo();
        }
        if (ticketInfo == null) {
            ticketInfo = new TicketInfo();
        }

    }

    private String DESAndBase64(String psw) {
        String pwd = "";
        try {
            pwd = DESUtils.DESAndBase64Encrypt(psw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pwd;
    }

    private void reset() {
        currentAccountInfo = new AccountInfo();
        ticketInfo = new TicketInfo();
        DemoCache.saveCurrentAccountInfo(new AccountInfo());
        DemoCache.saveTicketInfo(new TicketInfo());
    }

    @Override
    public boolean isLogin() {
        if (currentAccountInfo == null || StringUtil.isEmpty(currentAccountInfo.getAccess_token()) || TextUtils.isEmpty(currentAccountInfo.getUid() + "")) {
            return false;
        }
        return currentAccountInfo.getAccess_token() != null ? true : false && !isRequestTicket;
    }

    @Override
    public long getCurrentUid() {
        return currentAccountInfo == null ? 0 : currentAccountInfo.getUid();
    }

    @Override
    public String getTicket() {
        if (ticketInfo != null && ticketInfo.getTickets() != null && ticketInfo.getTickets().size() > 0) {
            return ticketInfo.getTickets().get(0).getTicket();
        }
        return "";
    }

    @Override
    public AccountInfo getCurrentAccount() {
        return currentAccountInfo;
    }

    @Override
    public ThirdUserInfo getThirdUserInfo() {
        return thirdUserInfo;
    }


    @Override
    public void setThirdUserInfo(ThirdUserInfo thirdUserInfo) {
        this.thirdUserInfo = thirdUserInfo;
    }

    @Override
    public void autoLogin() {
        if (!isLogin()) {
            notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_LOGIN);
            return;
        }

        isRequestTicket = true;
        com.tongdaxing.xchat_framework.util.util.LogUtil.d("autoLogin requestTicket");
        requestTicket();
    }

    @Override
    public void doLogin(final String account, String password) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", account);
        params.put("version", VersionUtil.getLocalName(getContext()));
        params.put("client_id", "erban-client");
        params.put("username", account);
        params.put("password", DESAndBase64(password));
        params.put("grant_type", "password");
        params.put("client_secret", "uyzjdhds");
        params.put("IMEI", DeviceUuidFactory.getPhoneIMEI(BasicConfig.INSTANCE.getAppContext()));
        final ResponseListener listener = new ResponseListener<LoginResult>() {
            @Override
            public void onResponse(LoginResult response) {
                if (response.isSuccess()) {
                    currentAccountInfo = response.getData();
                    removeSomePersonInfo();
                    DemoCache.saveCurrentAccountInfo(currentAccountInfo);
                    requestTicket();
                } else {
                    reset();
//                    logout();
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getLoginResourceUrl(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        LoginResult.class, Request.Method.POST);
    }

    @Override
    public void requestTicket() {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("issue_type", "multi");
        params.put("access_token", currentAccountInfo.getAccess_token());
        ResponseListener listener = new ResponseListener<TicketResult>() {
            @Override
            public void onResponse(TicketResult response) {
                isRequestTicket = false;
                if (response.isSuccess()) {
                    ticketInfo = response.getData();
                    DemoCache.saveTicketInfo(ticketInfo);
                    com.tongdaxing.xchat_framework.util.util.LogUtil.d("notifyClients onLogin");
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN, currentAccountInfo);
//                    CoreManager.getCore(IBlackListCore.class).requestBLackList(null);
                    //获取到用户信息之后加载用户抽奖礼物
//                    ConnectManager.get().initIM(); FIXME IM还没开发完之前 注释掉
                    CoreManager.getCore(IGiftCore.class).requestGiftInfos();
                    CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
                } else {
                    reset();
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL);
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGOUT);
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REQUEST_TICKET_FAIL, response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                isRequestTicket = false;
                reset();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGOUT);
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL);
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REQUEST_TICKET_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getAuthTicket(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        TicketResult.class, Request.Method.GET);
    }

    @Override
    public void doRequestSMSCode(String phone, int type) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);
        params.put("type", String.valueOf(type));
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_SMS_SUCCESS);
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_SMS_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_SMS_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getSMSCode(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);

    }

    @Override
    public void requestResetPsw(String phone, String sms_code, String newPsw) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);
        params.put("smsCode", sms_code);
        params.put("newPwd", DESAndBase64(newPsw));
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_MODIFY_PSW);
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_MODIFY_PSW_FAIL, response.getMessage());
                        String e1 = response.getErrorMessage().toString();
                        System.out.println("e1" + e1);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_MODIFY_PSW_FAIL, error.getErrorStr());
                String e2 = error.getErrorStr().toString();
                System.out.println("e2" + e2);
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.modifyPsw(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.POST);
    }


    @Override
    public void doWxLogin() {
//        if(!(api.getWXAppSupportAPI() >= com.tencent.mm.sdk.constants.Build.PAY_SUPPORTED_SDK_INT)){
//           //未安装微信
//            return;
//        }
        wechat = ShareSDK.getPlatform(Wechat.NAME);
        if (!wechat.isClientValid()) {
            notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "未安装微信");
            return;
        }
        if (wechat.isAuthValid()) {
            wechat.removeAccount(true);
        }

        wechat.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(final Platform platform, int i, HashMap<String, Object> hashMap) {
                if (i == Platform.ACTION_USER_INFOR) {
                    String openid = platform.getDb().getUserId();
                    String unionid = platform.getDb().get("unionid");
                    L.info(TAG, "onComplete id: %s", unionid);
                    String access_token = platform.getDb().getToken();
                    thirdUserInfo = new ThirdUserInfo();
                    thirdUserInfo.setUserName(platform.getDb().getUserName());
                    thirdUserInfo.setUserGender(platform.getDb().getUserGender());
                    thirdUserInfo.setUserIcon(platform.getDb().getUserIcon());
                    com.tongdaxing.xchat_framework.util.util.LogUtil.d("wechat setPlatformActionListener onComplete");
                    doThirdLogin(openid, unionid, access_token, 1);
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                L.info(TAG, "onError exception: ", throwable);
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "第三方登录失败");
            }

            @Override
            public void onCancel(Platform platform, int i) {
                L.info(TAG, "onCancel platform: %s, i: %d", platform, i);
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "第三方登录取消");
            }
        });
        wechat.SSOSetting(false);
        wechat.showUser(null);
    }

    @Override
    public void doThirdLogin(String openid, String unionid, String access_token, int type) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("openid", openid);
        params.put("unionid", unionid);
        params.put("access_token", access_token);
        params.put("type", String.valueOf(type));
        params.put("IMEI", DeviceUuidFactory.getPhoneIMEI(BasicConfig.INSTANCE.getAppContext()));
        LinkedInfo linkedInfo = CoreManager.getCore(ILinkedCore.class).getLinkedInfo();
        if (linkedInfo != null && !StringUtil.isEmpty(linkedInfo.getChannel())) {
            params.put("linkedmeChannel", linkedInfo.getChannel());
        }
        ResponseListener listener = new ResponseListener<LoginResult>() {
            @Override
            public void onResponse(LoginResult response) {
                if (response.isSuccess()) {
                    currentAccountInfo = response.getData();
                    removeSomePersonInfo();
                    DemoCache.saveCurrentAccountInfo(currentAccountInfo);
                    com.tongdaxing.xchat_framework.util.util.LogUtil.d("doThirdLogin requestTicket");
                    requestTicket();
                } else {
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, response.getCode() + "错误," + response.getMessage());
                    logout();
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.requestWXLogin(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        LoginResult.class, Request.Method.POST);
    }

    /**
     * 去除新手用户登录时，记录的时间
     */
    private void removeSomePersonInfo() {
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo != null && currentAccountInfo.getUid() != cacheLoginUserInfo.getUid()) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
            String rt = sdf.format(calendar.getTime());
            SpUtils.remove(getContext(), rt);
        }
    }

    @Override
    public void doQqLogin() {
//        qq.authorize();\
        qq = ShareSDK.getPlatform(QQ.NAME);
        if (qq.isAuthValid()) {
            qq.removeAccount(true);
        }
        qq.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                if (i == Platform.ACTION_USER_INFOR) {
                    String openid = platform.getDb().getUserId();
                    String unionid = platform.getDb().get("unionid");
                    String access_token = platform.getDb().getToken();
                    thirdUserInfo = new ThirdUserInfo();
                    thirdUserInfo.setUserName(platform.getDb().getUserName());
                    thirdUserInfo.setUserGender(platform.getDb().getUserGender());
                    thirdUserInfo.setUserIcon(platform.getDb().getUserIcon());
                    doThirdLogin(openid, unionid, access_token, 2);
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "第三方登录失败");
            }

            @Override
            public void onCancel(Platform platform, int i) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "第三方登录取消");
            }
        });
        qq.SSOSetting(false);
        qq.showUser(null);
    }

    @Override
    public void isPhone(long uid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_IS_PHONE);
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_IS_PHONE_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_IS_PHONE_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.isPhones(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);

    }

    @Override
    public void ModifyBinderPhone(long uid, String phone, String code, String url) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("phone", phone);
        params.put("smsCode", code);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener responseListener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_MOIDFY_ON_BINDER);
                        IUserCore core = CoreManager.getCore(IUserCore.class);
                        if (core != null && core.getCacheLoginUserInfo() != null)
                            core.requestUserInfo(core.getCacheLoginUserInfo().getUid());
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_MOIDFY_ON_BINDER_FAIL, response.getMessage());
                    }
                }
            }
        };

        ResponseErrorListener responseErrorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_MOIDFY_ON_BINDER_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(url,
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                responseListener,
                responseErrorListener,
                ServiceResult.class,
                Request.Method.POST
        );
    }

    @Override
    public void BinderPhone(long uid, String phone, String code) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("phone", phone);
        params.put("code", code);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener responseListener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_BINDER);
                        IUserCore core = CoreManager.getCore(IUserCore.class);
                        if (core != null && core.getCacheLoginUserInfo() != null)
                            core.requestUserInfo(core.getCacheLoginUserInfo().getUid());
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_BINDER_FAIL, response.getMessage());
                    }
                }
            }
        };

        ResponseErrorListener responseErrorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_BINDER_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.binderPhone(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                responseListener,
                responseErrorListener,
                ServiceResult.class,
                Request.Method.POST
        );
    }


    @Override
    public void doGetSMSCode(String phone) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_GET_SMS_CODE_FAIL, response.getMessage() + "");
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_GET_SMS_CODE_FAIL, "绑定手机失败!");
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getSmS(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);
    }


    @Override
    public void getModifyPhoneSMSCode(String phone, String type) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);
        params.put("type", type);
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_GET_SMS_CODE_FAIL, response.getMessage() + "");
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_GET_SMS_CODE_FAIL, "换绑手机失败!");
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getModifyPhoneSMS(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ServiceResult.class, Request.Method.GET);
    }


    @Override
    public void logout() {
//        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
//        params.put("access_token", currentAccountInfo.getAccess_token());
//        ResponseListener listener = new ResponseListener<ServiceResult>() {
//            @Override
//            public void onResponse(ServiceResult response) {
//
//            }
//        };
//        ResponseErrorListener errorListener = new ResponseErrorListener() {
//            @Override
//            public void onErrorResponse(RequestError error) {
//
//            }
//        };
//        RequestManager.instance()
//                .submitJsonResultQueryRequest(UriProvider.logout(),
//                        CommonParamUtil.getDefaultHeaders(getContext()),
//                        params, listener, errorListener,
//                        ServiceResult.class, Request.Method.POST);
        reset();
        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGOUT);
    }

    @Override
    public void doRegister(String phone, String sms_code, String password) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);
        params.put("smsCode", sms_code);
        params.put("password", DESAndBase64(password));
        params.put("os", "android");
        LinkedInfo linkedInfo = CoreManager.getCore(ILinkedCore.class).getLinkedInfo();
        if (linkedInfo != null && !StringUtil.isEmpty(linkedInfo.getChannel())) {
            params.put("linkedmeChannel", linkedInfo.getChannel());
        }
        ResponseListener listener = new ResponseListener<RegisterResult>() {
            @Override
            public void onResponse(RegisterResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REGISTER);
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REGISTER_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REGISTER_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRegisterResourceUrl(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RegisterResult.class, Request.Method.POST);
    }

    @Override
    public String getWxInfo() {
        L.info(TAG, "getWxInfo mInfo: %s", mInfo);
        return mInfo;
    }

    @Override
    public void setWxInfo(String info) {
        L.info(TAG, "setWxInfo info: %s", info);
        this.mInfo = info;
    }

    //    static class AuthCoreImplHander extends Handler {
//        WeakReference<AuthCoreImpl> authCoreImpl;
//
//        public AuthCoreImplHander(AuthCoreImpl authCoreImpl) {
//            this.authCoreImpl = new WeakReference<>(authCoreImpl);
//        }
//
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            if (authCoreImpl == null || authCoreImpl.get() == null)
//                return;
//            authCoreImpl.get().requestTicket();
//        }
//    }

}
