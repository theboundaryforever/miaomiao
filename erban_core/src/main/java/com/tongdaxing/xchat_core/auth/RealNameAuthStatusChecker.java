package com.tongdaxing.xchat_core.auth;

import android.content.Context;
import android.text.TextUtils;

import com.tcloud.core.log.L;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

public class RealNameAuthStatusChecker {

    private final String TAG = "RealNameAuthStatusChecker";

    private RealNameAuthStatusChecker() {

    }

    private static RealNameAuthStatusChecker instance = null;

    public static RealNameAuthStatusChecker getInstance() {
        if (null == instance) {
            instance = new RealNameAuthStatusChecker();
        }
        return instance;
    }

    public boolean authStatus(Context context, String msg) {
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        boolean result = null != cacheLoginUserInfo && cacheLoginUserInfo.isRealNameAudit();
        L.info(TAG, "authStatus cacheLoginUserInfo: %s", cacheLoginUserInfo);
        if (!result) {
            //MainActivity回调弹出弹框
            CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_REAL_NAME_AUTH, context, msg);
        }
        return result;
    }

    public boolean phoneStatus(Context context, String msg) {
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        boolean result = null != cacheLoginUserInfo && !TextUtils.isEmpty(cacheLoginUserInfo.getPhone()) && !cacheLoginUserInfo.getPhone().equals(String.valueOf(cacheLoginUserInfo.getErbanNo()));
        L.info(TAG, "phoneStatus cacheLoginUserInfo: %s", cacheLoginUserInfo);
        if (!result) {
            //MainActivity回调弹出弹框
            CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_BIND_PHONE_AUTH, context, msg);
        }
        return result;
    }

    //该功能需要进行实名认证
    public static final int RESULT_FAILED_NEED_REAL_NAME_AUTH = 2202;
}
