package com.tongdaxing.xchat_core.auth;

import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * Created by chenran on 2017/2/13.
 */

public interface IAuthCore extends IBaseCore {
    AccountInfo getCurrentAccount();

    ThirdUserInfo getThirdUserInfo();

    void setThirdUserInfo(ThirdUserInfo thirdUserInfo);

    long getCurrentUid();

    String getTicket();

    boolean isLogin();

    void doRegister(String phone, String sms_code, String password);

    void doLogin(String account, String password);

    void autoLogin();

    void logout();

    void requestTicket();

    void doRequestSMSCode(String phone, int type);// Type取值：1注册短信；2登录短信；3找回或者修改密码短信

    void requestResetPsw(String phone, String sms_code, String newPsw);

    void doWxLogin();

    void doThirdLogin(String openid, String unionid, String access_token, int type);

    void doQqLogin();

    void isPhone(long uid);

    void BinderPhone(long uid, String phone, String code);

    void ModifyBinderPhone(long uid, String phone, String code, String url);


    void doGetSMSCode(String phone);

    void getModifyPhoneSMSCode(String phone, String type);

    String getWxInfo();

    void setWxInfo(String info);
}
