package com.tongdaxing.xchat_core.connect;

import android.os.Handler;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.tcloud.core.log.L;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.im.base.ICommonListener;
import com.tongdaxing.xchat_framework.im.base.IConnectListener;
import com.tongdaxing.xchat_framework.im.base.IMCallBack;
import com.tongdaxing.xchat_framework.im.base.IMError;
import com.tongdaxing.xchat_framework.im.base.IMErrorBean;
import com.tongdaxing.xchat_framework.im.base.IMKey;
import com.tongdaxing.xchat_framework.im.base.IMModelFactory;
import com.tongdaxing.xchat_framework.im.base.IMProCallBack;
import com.tongdaxing.xchat_framework.im.base.IMReportBean;
import com.tongdaxing.xchat_framework.im.base.IMSendCallBack;
import com.tongdaxing.xchat_framework.im.base.IMSendRoute;
import com.tongdaxing.xchat_framework.im.bean.ChatRoomMessage;
import com.tongdaxing.xchat_framework.im.manager.DefaultSocketProfile;
import com.tongdaxing.xchat_framework.im.manager.ISocketProfile;
import com.tongdaxing.xchat_framework.im.manager.SocketManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.java_websocket.handshake.ServerHandshake;

import java.net.URISyntaxException;
import java.util.List;


/**
 *
 */
public class ConnectManager {
    public static String TAG = "ConnectManager";
    private ICommonListener iCommonListener = null;
    private SocketManager mSocketManager = null;
    private static ConnectManager connectManager = null;
    private IConnectListener connectListener;
    private SocketManager.IMNoticeMsgListener imNoticeMsgListener;

    public final static String PRODUCT_WS_URL = "wss://wsim.meibao8888.com/";
    public final static String DEBUG_WS_URL = "ws://39.107.81.178:3006/";
    private ISocketProfile mSocketProfile;
    private Handler mHandler = new Handler();
    private final int mReConnectTIme = 3000;

    public ConnectManager() {
        mSocketProfile = new DefaultSocketProfile();
    }

    private void imLogin(final long uid, final String ticket) {
        L.info(TAG, "imLogin ---> uid = " + uid);
        IMProCallBack imProCallBack = new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                int errno = imReportBean.getReportData().errno;
                L.info(TAG, "imLogin ---> errno: %d, msg: %s",errno, imReportBean.getReportData().errmsg);
                if (errno != 0) {
                    if (!ConnectManager.get().isConnected()) {
                        ConnectManager.get().connect(uid, connectListener, mSocketProfile.connectTime()); //登录失败而且socket断开状态重连
                    } else {
                        if (errno != IMError.IM_ERROR_LOGIN_AUTH_FAIL && errno != IMError.IM_ERROR_GET_USER_INFO_FAIL) {
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imLogin(uid, ticket);
                                }
                            }, mReConnectTIme);
                        }
                        if (imNoticeMsgListener != null) {
                            imNoticeMsgListener.onLoginError(errno, imReportBean.getReportData().errmsg);
                        }
                    }
                } else {
                    if (imNoticeMsgListener != null) {
                        imNoticeMsgListener.onDisConntectIMLoginSuc();
                    }
                    onImLoginRenewEnterRoom();
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                L.info(TAG, "imLogin ---> errorCode = " + errorCode);
            }
        };
        ConnectManager.get().send(IMModelFactory.get().createLoginModel(ticket, String.valueOf(uid)), imProCallBack);
    }

    /**
     * im登录之后如果再房间，需要重新进入房间（重连）
     */
    private void onImLoginRenewEnterRoom() {
        L.info(TAG, "onImLogin ---> enterRoom");
        if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
            enterRoom(AvRoomDataManager.get().mCurrentRoomInfo, 1, new IMProCallBack() {
                @Override
                public void onSuccessPro(IMReportBean imReportBean) {
                    L.info(TAG, "onImLogin ---> onReconnection");
                    if (imNoticeMsgListener != null) {
                        imNoticeMsgListener.onDisConnectEnterRoomSuc();
                    }
                }

                @Override
                public void onError(int errorCode, String errorMsg) {
                    L.info(TAG, "onImLogin ---> enterRoom ---> onError");
                    if (imNoticeMsgListener != null) {
                        imNoticeMsgListener.onLoginError(errorCode, errorMsg);
                    }
                }
            });
        }
    }

    private void setServerMicInfo(String room_info, String member, List<Json> queueList, Json personInfo) {
//        Gson gson = new Gson();
//        RoomInfo extRoomInfo = gson.fromJson(room_info, RoomInfo.class);
//        IMChatRoomMember chatRoomMember = gson.fromJson(member, IMChatRoomMember.class);
//        if (chatRoomMember != null) {
//            AvRoomDataManager.get().setOwnerMember(chatRoomMember);
//            AvRoomDataManager.get().setLovesMember(chatRoomMember.getUserLoves());
//            AvRoomDataManager.get().setGuardMember(chatRoomMember.getUserGuardian());
//        }
//        //同步房间信息
//        AvRoomDataManager.get().setCurrentRoomInfo(extRoomInfo);
//        //同步麦序
//        for (int i = 0; i < queueList.size(); i++) {
//            Json json = queueList.get(i);
//            int key = json.num("key");
//            Json value = json.json_ok("value");
//            IMChatRoomMember imChatRoomMember = null;
//            if (value.has("member")) {
//                imChatRoomMember = gson.fromJson(value.str("member"), IMChatRoomMember.class);
//            }
//            RoomMicInfo micInfo = gson.fromJson(value.str("mic_info"), RoomMicInfo.class);
//            RoomQueueInfo roomQueueInfo = new RoomQueueInfo(micInfo, imChatRoomMember);
//            AvRoomDataManager.get().mMicQueueMemberMap.put(key, roomQueueInfo);
//        }
//        if (personInfo != null) {
//            AvRoomDataManager.get().setWaitQueuePersonInfo(JsonParser.parseJsonObject(personInfo.toString(), WaitQueuePersonInfo.class));
//        }
    }


    //-------------------------------------------对外开放的方法-------------------------------------------------------------------


    public static ConnectManager get() {
        if (connectManager == null) {
            synchronized (ConnectManager.class) {
                if (connectManager == null) {
                    connectManager = new ConnectManager();
                }
            }
        }
        return connectManager;
    }


    /**
     * 注册断开回调函数  告诉业务层断开( code 告诉我们时超时导致还是网络导致断开还是手动断开)
     * 注册服务器单向推消息处理回调
     *
     * @param iCommonListener
     */
    public void setiCommonListener(ICommonListener iCommonListener) {
        this.iCommonListener = iCommonListener;
        if (iCommonListener != null && mSocketManager != null) {
            mSocketManager.setiCommonListener(iCommonListener);
        }
    }

    /**
     * 手动断开链接
     */
    public void disconnect() {
        if (mSocketManager != null) {
            mSocketManager.disconnect();
        }
    }

    public void destroy() {
        if (mSocketManager != null) {
            mSocketManager.destroy();
        }
    }


    /**
     * @return 是否已连接
     */
    public boolean isConnected() {
        return mSocketManager != null && mSocketManager.isConnected();
    }

    /**
     * @return 是否在连接中
     */
    public boolean isConnecting() {
        return mSocketManager != null && mSocketManager.isConnecting();
    }


    /**
     * @param iConnectListener
     * @param delay            延迟connect 毫秒
     * @return
     */
    private void connect(long uid, IConnectListener iConnectListener, int delay) {
        if (mSocketManager != null) {
            mSocketManager.destroy();
        }
        mSocketManager = new SocketManager();
        String socketUrl = BasicConfig.isDebug ? DEBUG_WS_URL : PRODUCT_WS_URL + "?uid=" + uid;
        try {
            mSocketManager.setupSocketUri(socketUrl);
            if (iCommonListener != null) {
                mSocketManager.setiCommonListener(iCommonListener);
            }
            mSocketManager.connect(iConnectListener, delay);
        } catch (URISyntaxException e) {
            if (iConnectListener != null) {
                iConnectListener.onError(e);
            }
        }
    }

    public void setImNoticeMsgListener(SocketManager.IMNoticeMsgListener imNoticeMsgListener) {
        this.imNoticeMsgListener = imNoticeMsgListener;
    }

    /**
     * 对服务器发送消息
     *
     * @param content    发送内容
     * @param imCallBack 结果的回调
     * @return
     */
    public void send(Json content, @NonNull IMCallBack imCallBack) {
        L.info("request_info_im_send", content.toString());
        if (mSocketManager != null) {
            mSocketManager.send(content.toString(), imCallBack);
        } else {
            imCallBack.onError(-1, IMError.IM_MS_SEND_ERROR);
        }
    }


    /**
     * 登录回调之后初始化im，并登录
     */
    public void initIM() {
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        final String ticket = CoreManager.getCore(IAuthCore.class).getTicket();

        L.info(TAG, "initIM ---> connect --->  uid = " + uid);
        connectListener = new IConnectListener() {
            @Override
            public void onSuccess(ServerHandshake serverHandshake) {
                L.info(TAG, "initIM ---> onOpen ---> HttpStatus = " + serverHandshake.getHttpStatus() + " HttpStatusMessage = " + serverHandshake.getHttpStatusMessage());
                imLogin(uid, ticket);
            }

            @Override
            public void onError(Exception e) {
                L.info(TAG, "initIM ---> onError ---> Exception = " + e.getMessage());
                if ("Dubble connect!".equalsIgnoreCase(e.getMessage())) {
                    return;
                }
                ConnectManager.get().connect(uid, connectListener, mReConnectTIme); //断线重连延迟3秒执行
            }
        };
        ConnectManager.get().connect(uid, connectListener, 0);
        ConnectManager.get().setiCommonListener(new ICommonListener() {
            @Override
            public void onDisconnectCallBack(IMErrorBean err) {
                boolean isCloseSelf = err.getCloseReason() == SocketManager.CALL_BACK_CODE_SELFCLOSE;
                L.info(TAG, "initIM ---> onDisconnectCallBack ---> isCloseSelf = " + isCloseSelf + " err_code = " + err.getCode() + " reason = " + err.getReason());
                if (imNoticeMsgListener != null) {
                    imNoticeMsgListener.onDisConnection(isCloseSelf);
                }
                if (!isCloseSelf) { // 非手动关闭自动重连
                    ConnectManager.get().connect(uid, connectListener, mReConnectTIme); // 断线重连延迟执行
                }
            }

            @Override
            public void onNoticeMessage(String message) {
                if (imNoticeMsgListener != null) {
                    imNoticeMsgListener.onNotice(Json.parse(message));
                }
            }
        });

    }


    /**
     * 进入房间
     *
     * @param reconnect     是否是重连 1重连，0非重连
     * @param imProCallBack 传到这里的闭包的成功回调不需要再判断请求是否成功
     */
    public void enterRoom(RoomInfo roomInfo, int reconnect, final IMProCallBack imProCallBack) {
        //我们自己服务端信息
//        AvRoomDataManager.get().setCurrentRoomInfo(roomInfo);
//        AvRoomDataManager.get().setServiceRoomInfo(roomInfo);
//        //存储加群的状态 在room/get接口返回 此时已经更新了从接口获取到的房间信息
//        AvRoomDataManager.get().setPartRoomInfo(roomInfo);
//        L.info(TAG, "enterRoom ---> send ---> roomId = " + roomInfo.getRoomId());
//        if (!isConnected()) {
//            imProCallBack.onError(IMError.IM_LOGIN_FAIL, "网络初始化中，请稍后再试");
//            if (!isConnecting()) {//不是在连接中的话，初始化连接
//                initIM();
//            }
//        } else {
//            ConnectManager.get().send(IMModelFactory.get().createJoinAvRoomModel(roomInfo.getRoomId(), reconnect), new IMProCallBack() {
//                @Override
//                public void onSuccessPro(IMReportBean imReportBean) {
//                    IMNetEaseManager.get().setImRoomConnection(true);
//                    IMReportBean.ReportData reportData = imReportBean.getReportData();
//                    if (reportData.errno != 0) {
//                        L.info(TAG, "enterRoom ---> send ---> onError --- > errno = " + reportData.errno);
//                        imProCallBack.onError(reportData.errno, reportData.errmsg);
//                        return;
//                    }
//                    Json data = reportData.data;
//                    String room_info = data.str("room_info");
//                    String member = data.str("member");
//                    List<Json> queueList = data.jlist("queue_list");
//                    Json personInfo = data.json("person_info");
//                    //可能会解析错误
//                    try {
//                        setServerMicInfo(room_info, member, queueList, personInfo);
//                    } catch (Exception e) {
//                        imProCallBack.onError(IMError.IM_JSON_PARSE, "");
//                        return;
//
//                    }
//                    L.info(TAG, "enterRoom ---> send ---> onSuccessPro");
//                    imProCallBack.onSuccessPro(imReportBean);
//                }
//
//                @Override
//                public void onError(int errorCode, String errorMsg) {
//                    L.info(TAG, "enterRoom ---> send ---> onError ---> errorCode = " + errorCode);
//                    imProCallBack.onError(errorCode, errorMsg);
//                }
//            });
//        }
    }

    /**
     * 更新麦序操作：加入队列  如果已经其他麦序位置上，会自动更新位置
     */
//    public void updateQueue(String roomId, int micPosition, long uid, final IMCallBack imCallBack) {
//        L.info(TAG, "updateQueue ---> roomId = " + roomId + " --- micPosition = " + micPosition + " --- uid = " + uid);
//        updateQueue(roomId, micPosition, uid, AvRoomDataManager.UPPER_MICRO_NO_NOTIFY_MATCHMAKER, AvRoomDataManager.BE_INVITED, imCallBack);
//    }

    /**
     * 更新麦序操作：加入队列  如果已经其他麦序位置上，会自动更新位置
     */
    public void updateQueue(String roomId, int micPosition, long uid, int initiative, int beInvited, final IMCallBack imCallBack) {
        L.info(TAG, "updateQueue ---> roomId = " + roomId + " --- micPosition = " + micPosition + " --- uid = " + uid + " --- initiative = " + initiative + " --- beInvited = " + beInvited);
        ConnectManager.get().send(IMModelFactory.get().createUpdateQueue(roomId, micPosition, uid, initiative, beInvited), imCallBack);
    }

    /**
     * 退出房间
     */
    public void exitRoom(long roomId, IMProCallBack imProCallBack) {
        L.info(TAG, "exitRoom ---> roomId = " + roomId);
        send(IMModelFactory.get().createExitRoom(roomId), imProCallBack);
    }


    /**
     * 退出公聊房间
     *
     * @param roomId
     * @param imProCallBack
     */
    public void exitPublicRoom(long roomId, IMProCallBack imProCallBack) {
        L.info(TAG, "exitRoom ---> roomId = " + roomId);
        send(IMModelFactory.get().createExitPublicRoom(roomId), imProCallBack);
    }

    /**
     * 更新麦序操作：加入队列  如果已经其他麦序位置上，会自动更新位置
     *
     * @param roomId
     * @param micPosition
     * @param imSendCallBack
     */
    public void pollQueue(String roomId, int micPosition, final IMSendCallBack imSendCallBack) {
        L.info(TAG, "updateQueue ---> roomId = " + roomId + " --- micPosition = " + micPosition);
        send(IMModelFactory.get().createPollQueue(roomId, micPosition), imSendCallBack);
    }


    /**
     * 发送文本消息
     */
    public void sendTxtMessage(String roomId, ChatRoomMessage message, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set(IMKey.member, new Gson().toJson(message.getImChatRoomMember()));
        if (StringUtils.isNotEmpty(message.getContent())) {
            json.set(IMKey.content, message.getContent());
        }
        send(IMModelFactory.get().createRequestData(IMSendRoute.sendText, json), imSendCallBack);
    }

    /**
     * 发送自定义消息
     *
     * @param roomId
     * @param message
     * @param imSendCallBack
     */
    public void sendCustomMessage(String roomId, ChatRoomMessage message, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set(IMKey.custom, message.getAttachment().toJson(true));
        if (message.getImChatRoomMember() != null) {
            json.set(IMKey.member, message.getImChatRoomMember());
        }
        send(IMModelFactory.get().createRequestData(IMSendRoute.sendMessage, json), imSendCallBack);
    }

    /**
     * 发送自定义消息
     *
     * @param roomId
     * @param message
     * @param imSendCallBack
     */
    public void sendPulicMessage(String roomId, ChatRoomMessage message, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set(IMKey.custom, message.getAttachment().toJson());
        send(IMModelFactory.get().createRequestData(IMSendRoute.sendPublicMsg, json), imSendCallBack);
    }

    /**
     * 进入公聊大厅
     *
     * @param roomId
     * @param imSendCallBack
     */
    public void enterChatHallMessage(String roomId, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        send(IMModelFactory.get().createRequestData(IMSendRoute.enterPublicRoom, json), imSendCallBack);
    }
}