package com.tongdaxing.xchat_core.bean;

/**
 * Function:
 * Author: Edward on 2019/1/23
 */
public class BlessingBeastInfo {

    /**
     * icon : https://pic.miaomiaofm.com/Fn6khZkDXEP_jNARZIRI8Dq-sd8N?imageslim
     * name : 福猪
     * webUrl : https://www.miaomiaofm.com/mm/act_gift/index.html
     * remainTime : 599617
     * svgaUrl : https://pic.miaomiaofm.com/FpxUMj4LyEYDgH1v9yRerelWpYAw?imageslim
     */

    private String icon;
    private String name;
    private String webUrl;
    private long remainTime;
    private String svgaUrl;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public long getRemainTime() {
        return remainTime;
    }

    public void setRemainTime(long remainTime) {
        this.remainTime = remainTime;
    }

    public String getSvgaUrl() {
        return svgaUrl;
    }

    public void setSvgaUrl(String svgaUrl) {
        this.svgaUrl = svgaUrl;
    }
}
