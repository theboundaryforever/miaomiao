package com.tongdaxing.xchat_core.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class AudioMatchRandomInfo implements Parcelable {

    private int uid;
    private int erbanNo;
    private String nick;
    private int gender;
    private String avatar;
    private int fansNum;
    private int operatorStatus;//2为在线
    private String userDesc;
    private String userVoice;
    private int voiceDura;
    //生日
    private long birth;

    public long getBirth() {
        return birth;
    }

    public void setBirth(long birth) {
        this.birth = birth;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(int erbanNo) {
        this.erbanNo = erbanNo;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getFansNum() {
        return fansNum;
    }

    public void setFansNum(int fansNum) {
        this.fansNum = fansNum;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public String getUserDesc() {
        return userDesc;
    }

    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    public String getUserVoice() {
        return userVoice;
    }

    public void setUserVoice(String userVoice) {
        this.userVoice = userVoice;
    }

    public int getVoiceDura() {
        return voiceDura;
    }

    public void setVoiceDura(int voiceDura) {
        this.voiceDura = voiceDura;
    }

    public AudioMatchRandomInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.uid);
        dest.writeInt(this.erbanNo);
        dest.writeString(this.nick);
        dest.writeInt(this.gender);
        dest.writeString(this.avatar);
        dest.writeInt(this.fansNum);
        dest.writeInt(this.operatorStatus);
        dest.writeString(this.userDesc);
        dest.writeString(this.userVoice);
        dest.writeInt(this.voiceDura);
        dest.writeLong(this.birth);
    }

    protected AudioMatchRandomInfo(Parcel in) {
        this.uid = in.readInt();
        this.erbanNo = in.readInt();
        this.nick = in.readString();
        this.gender = in.readInt();
        this.avatar = in.readString();
        this.fansNum = in.readInt();
        this.operatorStatus = in.readInt();
        this.userDesc = in.readString();
        this.userVoice = in.readString();
        this.voiceDura = in.readInt();
        this.birth = in.readLong();
    }

    public static final Creator<AudioMatchRandomInfo> CREATOR = new Creator<AudioMatchRandomInfo>() {
        @Override
        public AudioMatchRandomInfo createFromParcel(Parcel source) {
            return new AudioMatchRandomInfo(source);
        }

        @Override
        public AudioMatchRandomInfo[] newArray(int size) {
            return new AudioMatchRandomInfo[size];
        }
    };

    @Override
    public String toString() {
        return "AudioMatchRandomInfo{" +
                "uid=" + uid +
                ", erbanNo=" + erbanNo +
                ", nick='" + nick + '\'' +
                ", gender=" + gender +
                ", avatar='" + avatar + '\'' +
                ", fansNum=" + fansNum +
                ", operatorStatus=" + operatorStatus +
                ", userDesc='" + userDesc + '\'' +
                ", userVoice='" + userVoice + '\'' +
                ", voiceDura=" + voiceDura +
                ", birth=" + birth +
                '}';
    }
}
