package com.tongdaxing.xchat_core.bean.attachmsg;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;

/**
 * <p>  队列自定义消息</p>
 *
 * @author jiahui
 * @date 2017/12/18
 */
public class RoomQueueMsgAttachment extends CustomAttachment {
    public RoomQueueInfo roomQueueInfo;
    public String uid;
    public int micPosition;

    public RoomQueueMsgAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        if (uid != null) {
            jsonObject.put("uid", uid);
        }
        jsonObject.put("micPosition", micPosition);
        return jsonObject;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        if (data != null) {
            if (data.containsKey("uid")) {
                uid = data.getString("uid");
            }
            if (data.containsKey("micPosition")) {
                micPosition = data.getIntValue("micPosition");
            }
        }
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
