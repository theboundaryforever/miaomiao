package com.tongdaxing.xchat_core.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RankingInfo implements Parcelable {
    private List<RankVo> rankVoList;
    private Me me;

    public List<RankVo> getRankVoList() {
        return rankVoList;
    }

    public void setRankVoList(List<RankVo> rankVoList) {
        this.rankVoList = rankVoList;
    }

    public Me getMe() {
        return me;
    }

    public void setMe(Me me) {
        this.me = me;
    }

    protected RankingInfo(Parcel in) {
        rankVoList = in.createTypedArrayList(RankVo.CREATOR);
        me = in.readParcelable(Me.class.getClassLoader());
    }

    public static final Creator<RankingInfo> CREATOR = new Creator<RankingInfo>() {
        @Override
        public RankingInfo createFromParcel(Parcel in) {
            return new RankingInfo(in);
        }

        @Override
        public RankingInfo[] newArray(int size) {
            return new RankingInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(rankVoList);
        dest.writeParcelable(me, flags);
    }

    public static class RankVo implements Parcelable {
        /**
         * uid : 1001818
         * erbanNo : 1111111
         * avatar : https://pic.miaomiaofm.com/FnMziuem9egbI5JOzqWLUtKaPojp?imageslim
         * nick : My.赌神白
         * gender : 1
         * totalNum : 56761
         * experLevel : 36
         * charmLevel : 31
         */

        private long uid;
        private String erbanNo;
        private String avatar;
        private String nick;
        private int gender;
        private long totalNum;
        private double distance;//距离上一级差值
        private int experLevel;
        private int charmLevel;
        /**
         * inviteUser : {"uid":109825,"erbanNo":7576330,"nick":"苹果哈哈哈哈嗝","avatar":"https://pic.miaomiaofm.com/FhX_olqdwMhhgmyBxEbykjfYbh05?imageslim","experLevel":null,"charmLevel":null,"gender":1}
         * recUser : {"uid":110034,"erbanNo":5807418,"nick":"橘子0625","avatar":"https://pic.miaomiaofm.com/default_head_pic.png","experLevel":null,"charmLevel":null,"gender":1}
         * giftId : 214
         * giftName : 鲜花戒指
         * cpGiftLevel : 1
         * closeLevel : 5464
         * create_date : 1
         * picUrl : https://pic.miaomiaofm.com/FqAzAkOWn9ZG8TJfAInenWAvcgUa?imageslim
         * vggUrl : https://pic.miaomiaofm.com/Fs-evmSw5rNDFwA8k7ufFa9xudJu?imageslim
         * cpType : 1
         */

        private InviteUserBean inviteUser;
        private RecUserBean recUser;
        private int giftId;
        private String giftName;
        private int cpGiftLevel;
        private int closeLevel;
        private int create_date;
        private String picUrl;
        private String vggUrl;
        private int cpType;


        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public long getUid() {
            return uid;
        }

        public void setUid(long uid) {
            this.uid = uid;
        }

        public String getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(String erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public long getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(long totalNum) {
            this.totalNum = totalNum;
        }

        public int getExperLevel() {
            return experLevel;
        }

        public void setExperLevel(int experLevel) {
            this.experLevel = experLevel;
        }

        public int getCharmLevel() {
            return charmLevel;
        }

        public void setCharmLevel(int charmLevel) {
            this.charmLevel = charmLevel;
        }

        @Override
        public String toString() {
            return "RankingInfo{" +
                    "uid=" + uid +
                    ", erbanNo='" + erbanNo + '\'' +
                    ", avatar='" + avatar + '\'' +
                    ", nick='" + nick + '\'' +
                    ", gender=" + gender +
                    ", totalNum=" + totalNum +
                    ", distance=" + distance +
                    ", experLevel=" + experLevel +
                    ", charmLevel=" + charmLevel +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.uid);
            dest.writeString(this.erbanNo);
            dest.writeString(this.avatar);
            dest.writeString(this.nick);
            dest.writeInt(this.gender);
            dest.writeLong(this.totalNum);
            dest.writeDouble(this.distance);
            dest.writeInt(this.experLevel);
            dest.writeInt(this.charmLevel);
        }

        public RankVo() {
        }

        protected RankVo(Parcel in) {
            this.uid = in.readLong();
            this.erbanNo = in.readString();
            this.avatar = in.readString();
            this.nick = in.readString();
            this.gender = in.readInt();
            this.totalNum = in.readLong();
            this.distance = in.readDouble();
            this.experLevel = in.readInt();
            this.charmLevel = in.readInt();
        }

        public static final Creator<RankVo> CREATOR = new Creator<RankVo>() {
            @Override
            public RankVo createFromParcel(Parcel source) {
                return new RankVo(source);
            }

            @Override
            public RankVo[] newArray(int size) {
                return new RankVo[size];
            }
        };

        public InviteUserBean getInviteUser() {
            return inviteUser;
        }

        public void setInviteUser(InviteUserBean inviteUser) {
            this.inviteUser = inviteUser;
        }

        public RecUserBean getRecUser() {
            return recUser;
        }

        public void setRecUser(RecUserBean recUser) {
            this.recUser = recUser;
        }

        public int getGiftId() {
            return giftId;
        }

        public void setGiftId(int giftId) {
            this.giftId = giftId;
        }

        public String getGiftName() {
            return giftName;
        }

        public void setGiftName(String giftName) {
            this.giftName = giftName;
        }

        public int getCpGiftLevel() {
            return cpGiftLevel;
        }

        public void setCpGiftLevel(int cpGiftLevel) {
            this.cpGiftLevel = cpGiftLevel;
        }

        public int getCloseLevel() {
            return closeLevel;
        }

        public void setCloseLevel(int closeLevel) {
            this.closeLevel = closeLevel;
        }

        public int getCreate_date() {
            return create_date;
        }

        public void setCreate_date(int create_date) {
            this.create_date = create_date;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public String getVggUrl() {
            return vggUrl;
        }

        public void setVggUrl(String vggUrl) {
            this.vggUrl = vggUrl;
        }

        public int getCpType() {
            return cpType;
        }

        public void setCpType(int cpType) {
            this.cpType = cpType;
        }

        public static class InviteUserBean {
            /**
             * uid : 109825
             * erbanNo : 7576330
             * nick : 苹果哈哈哈哈嗝
             * avatar : https://pic.miaomiaofm.com/FhX_olqdwMhhgmyBxEbykjfYbh05?imageslim
             * experLevel : null
             * charmLevel : null
             * gender : 1
             */

            @SerializedName("uid")
            private int uidX;
            @SerializedName("erbanNo")
            private int erbanNoX;
            @SerializedName("nick")
            private String nickX;
            @SerializedName("avatar")
            private String avatarX;
            @SerializedName("experLevel")
            private Object experLevelX;
            @SerializedName("charmLevel")
            private Object charmLevelX;
            @SerializedName("gender")
            private int genderX;

            public int getUidX() {
                return uidX;
            }

            public void setUidX(int uidX) {
                this.uidX = uidX;
            }

            public int getErbanNoX() {
                return erbanNoX;
            }

            public void setErbanNoX(int erbanNoX) {
                this.erbanNoX = erbanNoX;
            }

            public String getNickX() {
                return nickX;
            }

            public void setNickX(String nickX) {
                this.nickX = nickX;
            }

            public String getAvatarX() {
                return avatarX;
            }

            public void setAvatarX(String avatarX) {
                this.avatarX = avatarX;
            }

            public Object getExperLevelX() {
                return experLevelX;
            }

            public void setExperLevelX(Object experLevelX) {
                this.experLevelX = experLevelX;
            }

            public Object getCharmLevelX() {
                return charmLevelX;
            }

            public void setCharmLevelX(Object charmLevelX) {
                this.charmLevelX = charmLevelX;
            }

            public int getGenderX() {
                return genderX;
            }

            public void setGenderX(int genderX) {
                this.genderX = genderX;
            }
        }

        public static class RecUserBean {
            /**
             * uid : 110034
             * erbanNo : 5807418
             * nick : 橘子0625
             * avatar : https://pic.miaomiaofm.com/default_head_pic.png
             * experLevel : null
             * charmLevel : null
             * gender : 1
             */

            @SerializedName("uid")
            private int uidX;
            @SerializedName("erbanNo")
            private int erbanNoX;
            @SerializedName("nick")
            private String nickX;
            @SerializedName("avatar")
            private String avatarX;
            @SerializedName("experLevel")
            private Object experLevelX;
            @SerializedName("charmLevel")
            private Object charmLevelX;
            @SerializedName("gender")
            private int genderX;

            public int getUidX() {
                return uidX;
            }

            public void setUidX(int uidX) {
                this.uidX = uidX;
            }

            public int getErbanNoX() {
                return erbanNoX;
            }

            public void setErbanNoX(int erbanNoX) {
                this.erbanNoX = erbanNoX;
            }

            public String getNickX() {
                return nickX;
            }

            public void setNickX(String nickX) {
                this.nickX = nickX;
            }

            public String getAvatarX() {
                return avatarX;
            }

            public void setAvatarX(String avatarX) {
                this.avatarX = avatarX;
            }

            public Object getExperLevelX() {
                return experLevelX;
            }

            public void setExperLevelX(Object experLevelX) {
                this.experLevelX = experLevelX;
            }

            public Object getCharmLevelX() {
                return charmLevelX;
            }

            public void setCharmLevelX(Object charmLevelX) {
                this.charmLevelX = charmLevelX;
            }

            public int getGenderX() {
                return genderX;
            }

            public void setGenderX(int genderX) {
                this.genderX = genderX;
            }
        }
    }

    public static class Me implements Parcelable {

        /**
         * totalNum : 0
         * seqNo : 0
         * distance : 0
         */

        private long totalNum;
        private int seqNo;
        private double distance;

        public Me() {
        }

        protected Me(Parcel in) {
            totalNum = in.readLong();
            seqNo = in.readInt();
            distance = in.readDouble();
        }

        public static final Creator<Me> CREATOR = new Creator<Me>() {
            @Override
            public Me createFromParcel(Parcel in) {
                return new Me(in);
            }

            @Override
            public Me[] newArray(int size) {
                return new Me[size];
            }
        };

        public long getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(long totalNum) {
            this.totalNum = totalNum;
        }

        public int getSeqNo() {
            return seqNo;
        }

        public void setSeqNo(int seqNo) {
            this.seqNo = seqNo;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(totalNum);
            dest.writeInt(seqNo);
            dest.writeDouble(distance);
        }
    }
}
