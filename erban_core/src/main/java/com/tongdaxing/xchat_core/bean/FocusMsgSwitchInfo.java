package com.tongdaxing.xchat_core.bean;

/**
 * 消息免打扰
 */
public class FocusMsgSwitchInfo {
    private int chatPermission;

    public int getChatPermission() {
        return chatPermission;
    }

    public void setChatPermission(int chatPermission) {
        this.chatPermission = chatPermission;
    }
}
