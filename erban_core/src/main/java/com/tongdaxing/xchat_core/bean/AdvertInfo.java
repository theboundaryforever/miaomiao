package com.tongdaxing.xchat_core.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;

import java.util.List;

public class AdvertInfo implements Parcelable {
    private List<BannerInfo> listBanner;
    private List<HomeRoom> listAdvert;

    public List<BannerInfo> getListBanner() {
        return listBanner;
    }

    public void setListBanner(List<BannerInfo> listBanner) {
        this.listBanner = listBanner;
    }

    public List<HomeRoom> getListAdvert() {
        return listAdvert;
    }

    public void setListAdvert(List<HomeRoom> listAdvert) {
        this.listAdvert = listAdvert;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.listBanner);
        dest.writeTypedList(this.listAdvert);
    }

    public AdvertInfo() {
    }

    protected AdvertInfo(Parcel in) {
        this.listBanner = in.createTypedArrayList(BannerInfo.CREATOR);
        this.listAdvert = in.createTypedArrayList(HomeRoom.CREATOR);
    }

    public static final Parcelable.Creator<AdvertInfo> CREATOR = new Parcelable.Creator<AdvertInfo>() {
        @Override
        public AdvertInfo createFromParcel(Parcel source) {
            return new AdvertInfo(source);
        }

        @Override
        public AdvertInfo[] newArray(int size) {
            return new AdvertInfo[size];
        }
    };
}
