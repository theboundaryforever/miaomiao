package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

/**
 * 去找TA TA资料bean
 */
public class HerUserInfo implements Serializable{
    private long uid;
    private String nickname;

    public HerUserInfo(long uid, String nickname) {
        setUid(uid);
        setNickname(nickname);
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
