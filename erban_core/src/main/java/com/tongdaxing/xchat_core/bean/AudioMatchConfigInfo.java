package com.tongdaxing.xchat_core.bean;

public class AudioMatchConfigInfo {

    private int filterGender;
    private boolean voiceHide;

    public int getFilterGender() {
        return filterGender;
    }

    public void setFilterGender(int filterGender) {
        this.filterGender = filterGender;
    }

    public boolean isVoiceHide() {
        return voiceHide;
    }

    public void setVoiceHide(boolean voiceHide) {
        this.voiceHide = voiceHide;
    }
}
