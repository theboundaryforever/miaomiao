package com.tongdaxing.xchat_core.bean;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;

/**
 * <p>  房间麦序单个坑位信息实体，包含麦序状态，成员信息</p>
 *
 * @author jiahui
 * @date 2017/12/13
 */
public class RoomQueueInfo {
    /**
     * 坑位信息（是否所坑，开麦等）
     */
    public RoomMicInfo mRoomMicInfo;
    /**
     * 坑上人员信息
     */
    public ChatRoomMember mChatRoomMember;
    /**
     * 当前成员的性别，本属于ChatRoomMember的
     */
    public int gender;
    /**
     * CP对象的id
     */
    public long cpUserId;
    /**
     * 是否展示CP图标
     */
    public boolean isShowCpIcon;
    /**
     * CP展示范围，1黄色 2 蓝色 3红色
     */
    public int cpGiftLevel;
    public long time;

    public RoomQueueInfo() {
    }

    public RoomQueueInfo(RoomMicInfo roomMicInfo, ChatRoomMember chatRoomMember) {
        mRoomMicInfo = roomMicInfo;
        mChatRoomMember = chatRoomMember;
    }

    @Override
    public String toString() {
        return "RoomQueueInfo{" +
                "mRoomMicInfo=" + mRoomMicInfo +
                ", mChatRoomMember=" + mChatRoomMember +
                ", gender=" + gender +
                ", cpUserId=" + cpUserId +
                ", isShowCpIcon=" + isShowCpIcon +
                ", cpGiftLevel=" + cpGiftLevel +
                ", time=" + time +
                '}';
    }
}
