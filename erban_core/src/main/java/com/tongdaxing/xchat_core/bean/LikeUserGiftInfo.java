package com.tongdaxing.xchat_core.bean;

public class LikeUserGiftInfo {
    private int giftId;
    private String giftName;
    private long goldPrice;
    private String nobleName;
    private boolean isNobleGift;
    private String picUrl;
    private boolean hasVggPic;
    private String vggUrl;
    private boolean isLatest;
    private boolean isTimeLimit;
    private boolean hasEffect;
    private long createTime;
    private boolean isCanGive;
    private boolean isExpressGift;

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public long getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(long goldPrice) {
        this.goldPrice = goldPrice;
    }

    public String getNobleName() {
        return nobleName;
    }

    public void setNobleName(String nobleName) {
        this.nobleName = nobleName;
    }

    public boolean isNobleGift() {
        return isNobleGift;
    }

    public void setNobleGift(boolean nobleGift) {
        isNobleGift = nobleGift;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public boolean isHasVggPic() {
        return hasVggPic;
    }

    public void setHasVggPic(boolean hasVggPic) {
        this.hasVggPic = hasVggPic;
    }

    public String getVggUrl() {
        return vggUrl;
    }

    public void setVggUrl(String vggUrl) {
        this.vggUrl = vggUrl;
    }

    public boolean isLatest() {
        return isLatest;
    }

    public void setLatest(boolean latest) {
        isLatest = latest;
    }

    public boolean isTimeLimit() {
        return isTimeLimit;
    }

    public void setTimeLimit(boolean timeLimit) {
        isTimeLimit = timeLimit;
    }

    public boolean isHasEffect() {
        return hasEffect;
    }

    public void setHasEffect(boolean hasEffect) {
        this.hasEffect = hasEffect;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public boolean isCanGive() {
        return isCanGive;
    }

    public void setCanGive(boolean canGive) {
        isCanGive = canGive;
    }

    public boolean isExpressGift() {
        return isExpressGift;
    }

    public void setExpressGift(boolean expressGift) {
        isExpressGift = expressGift;
    }
}
