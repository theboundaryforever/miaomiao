package com.tongdaxing.xchat_core.bean;

import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.List;

public class MakeFriendsListInfo {
    private List<UserInfo> charmList;
    private List<HomeRoom> roomList;

    public List<UserInfo> getCharmList() {
        return charmList;
    }

    public void setCharmList(List<UserInfo> charmList) {
        this.charmList = charmList;
    }

    public List<HomeRoom> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<HomeRoom> roomList) {
        this.roomList = roomList;
    }
}
