package com.tongdaxing.xchat_core.user.bean;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * cp用户信息
 * <p>
 * Created by zhangjian on 2019/3/27.
 */
public class CpUserBean extends RealmObject implements Serializable {

    /**
     * avatar : string
     * charmLevel : 0
     * closeLevel : 0
     * erbanNo : 0
     * experLevel : 0
     * giftId : 0
     * nick : string
     * uid : 0
     */

    private String avatar;
    private int charmLevel;//魅力值
    private int closeLevel;//cp亲密值
    private int erbanNo;
    private int experLevel;//等级值
    private int giftId;
    private String nick;
    private int uid;
    private String sign;//尾灯
    private int cpGiftLevel;//1 2 3
    private String vggUrl;//svga web地址

    public int getCpGiftLevel() {
        return cpGiftLevel;
    }

    public void setCpGiftLevel(int cpGiftLevel) {
        this.cpGiftLevel = cpGiftLevel;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    public int getCloseLevel() {
        return closeLevel;
    }

    public void setCloseLevel(int closeLevel) {
        this.closeLevel = closeLevel;
    }

    public int getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(int erbanNo) {
        this.erbanNo = erbanNo;
    }

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getVggUrl() {
        return vggUrl;
    }

    public void setVggUrl(String vggUrl) {
        this.vggUrl = vggUrl;
    }

    @Override
    public String toString() {
        return "CpUserBean{" +
                "avatar='" + avatar + '\'' +
                ", charmLevel=" + charmLevel +
                ", closeLevel=" + closeLevel +
                ", erbanNo=" + erbanNo +
                ", experLevel=" + experLevel +
                ", giftId=" + giftId +
                ", nick='" + nick + '\'' +
                ", uid=" + uid +
                ", sign='" + sign + '\'' +
                ", cpGiftLevel=" + cpGiftLevel +
                ", vggUrl='" + vggUrl + '\'' +
                '}';
    }
}
