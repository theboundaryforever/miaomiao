package com.tongdaxing.xchat_core.user.bean;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Function:
 * Author: Edward on 2019/3/4
 */
public class AuxiliaryToneBean extends RealmObject implements Serializable {
    /**
     * toneName : 温婉电台音
     * tonePro : 8
     */

    private String toneName;
    private int tonePro;

    public String getToneName() {
        return toneName;
    }

    public void setToneName(String toneName) {
        this.toneName = toneName;
    }

    public int getTonePro() {
        return tonePro;
    }

    public void setTonePro(int tonePro) {
        this.tonePro = tonePro;
    }

    @Override
    public String toString() {
        return "AuxiliaryToneBean{" +
                "toneName='" + toneName + '\'' +
                ", tonePro=" + tonePro +
                '}';
    }
}
