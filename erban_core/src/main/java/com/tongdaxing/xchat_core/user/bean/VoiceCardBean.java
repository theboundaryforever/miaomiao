package com.tongdaxing.xchat_core.user.bean;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Function:
 * Author: Edward on 2019/3/4
 */
public class VoiceCardBean extends RealmObject implements Serializable {
    /**
     * auxiliaryTone : [{"toneName":"温婉电台音","tonePro":8},{"toneName":"傲娇学长音","tonePro":6},{"toneName":"古风公子音","tonePro":6}]
     * avatar : https://pic.miaomiaofm.com/Fjkx9uUQAmICDU7oStouD1Spz6hc?imageslim
     * bestMatch : 冷艳女王音
     * charmLevel : 3
     * createDate : 1551693713000
     * masterTone : 阳光治愈音
     * masterTonePro : 80
     * nick : 请你吃饭监控室，km囖囖了空空
     * similarPerson : 黄子韬
     * uid : 100738
     * url : https://pic.miaomiaofm.com/Fu3d2vUPMwSf9tTgKp9mgbDj5PcF?imageslim
     * voiceScore : 94
     */

    private String avatar;
    private String bestMatch;
    private int charmLevel;
    private long createDate;
    private String masterTone;
    private int masterTonePro;
    private String nick;
    private String similarPerson;
    private int uid;
    private String url;
    private int voiceScore;
    private RealmList<AuxiliaryToneBean> auxiliaryToneList;

    public RealmList<AuxiliaryToneBean> auxiliaryToneList() {
        return auxiliaryToneList;
    }

    public RealmList<AuxiliaryToneBean> getAuxiliaryToneList() {
        return auxiliaryToneList;
    }

    public void setAuxiliaryToneList(RealmList<AuxiliaryToneBean> auxiliaryToneList) {
        this.auxiliaryToneList = auxiliaryToneList;
    }

    public void setAuxiliaryTone(RealmList<AuxiliaryToneBean> auxiliaryToneList) {
        this.auxiliaryToneList = auxiliaryToneList;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBestMatch() {
        return bestMatch;
    }

    public void setBestMatch(String bestMatch) {
        this.bestMatch = bestMatch;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getMasterTone() {
        return masterTone;
    }

    public void setMasterTone(String masterTone) {
        this.masterTone = masterTone;
    }

    public int getMasterTonePro() {
        return masterTonePro;
    }

    public void setMasterTonePro(int masterTonePro) {
        this.masterTonePro = masterTonePro;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getSimilarPerson() {
        return similarPerson;
    }

    public void setSimilarPerson(String similarPerson) {
        this.similarPerson = similarPerson;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getVoiceScore() {
        return voiceScore;
    }

    public void setVoiceScore(int voiceScore) {
        this.voiceScore = voiceScore;
    }

    @Override
    public String toString() {
        return "VoiceCardBean{" +
                "avatar='" + avatar + '\'' +
                ", bestMatch='" + bestMatch + '\'' +
                ", charmLevel=" + charmLevel +
                ", createDate=" + createDate +
                ", masterTone='" + masterTone + '\'' +
                ", masterTonePro=" + masterTonePro +
                ", nick='" + nick + '\'' +
                ", similarPerson='" + similarPerson + '\'' +
                ", uid=" + uid +
                ", url='" + url + '\'' +
                ", voiceScore=" + voiceScore +
                ", auxiliaryToneList=" + auxiliaryToneList +
                '}';
    }
}
