package com.tongdaxing.xchat_core.user;

import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * Function:
 * Author: Edward on 2019/1/19
 */
public interface ICongratulationDialogCoreClient extends ICoreClient {
    String CLOSE_GIFT_DIALOG_FROM_CONGRA_DIALOG = "closeGiftDialogFromCongraDialog";
    String OPEN_GIFT_DIALOG_FROM_CONGRA_DIALOG = "openGiftDialogFromCongraDialog";

    void openGiftDialogFromCongraDialog();

    void closeGiftDialogFromCongraDialog();
}
