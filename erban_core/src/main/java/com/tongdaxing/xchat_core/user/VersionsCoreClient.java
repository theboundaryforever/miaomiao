package com.tongdaxing.xchat_core.user;

import com.tongdaxing.xchat_core.user.bean.CheckUpdataBean;
import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * Created by Administrator on 2017/7/12 0012.
 */

public interface VersionsCoreClient extends ICoreClient {
    public static final String METHOD_GET_VERSION = "onGetVersion";
    public static final String METHOD_GET_VERSION_ERROR = "onGetVersionError";
    String METHOD_VERSION_UPDATA_DIALOG = "onVersionUpdataDialog";
   public void onGetVersion(Boolean isBoolean);
    public void onGetVersion(String error);
    public void onVersionUpdataDialog(CheckUpdataBean checkUpdataBean);
}
