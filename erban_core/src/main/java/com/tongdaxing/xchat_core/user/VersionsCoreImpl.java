package com.tongdaxing.xchat_core.user;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.result.CheckUpdataResult;
import com.tongdaxing.xchat_core.result.VersionsResult;
import com.tongdaxing.xchat_core.user.bean.CheckUpdataBean;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Administrator on 2017/7/12 0012.
 */

public class VersionsCoreImpl extends AbstractBaseCore implements VersionsCore {
    public VersionsCoreImpl() {
        String configStr = (String) SpUtils.get(getContext(), SpEvent.config_key, "");
        configdata = Json.parse(configStr);
//        LogUtils.d("VersionsCoreImpl", configStr);
    }

    private int checkKick = 0;
    private Json configdata;
    private String sensitiveWordData;//敏感词数据

    @Override
    public void getVersions(int version) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("version", String.valueOf(1.10));
        ResponseListener listener = new ResponseListener<VersionsResult>() {
            @Override
            public void onResponse(VersionsResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(AttentionCoreClient.class, VersionsCoreClient.METHOD_GET_VERSION, response.getData());


                    } else {
                        notifyClients(AttentionCoreClient.class, VersionsCoreClient.METHOD_GET_VERSION_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(AttentionCoreClient.class, VersionsCoreClient.METHOD_GET_VERSION_ERROR, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getVersions(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param, listener, errorListener, VersionsResult.class, Request.Method.GET);
    }

    @Override
    public void checkVersion() {

        ResponseListener listener = new ResponseListener<CheckUpdataResult>() {
            @Override
            public void onResponse(CheckUpdataResult response) {
                if (null != response) {
                    if (response.isSuccess()) {

//                        LogUtil.d("checkVersion", response.getData() + "");
                        CheckUpdataBean data = response.getData();
                        notifyClients(VersionsCoreClient.class, VersionsCoreClient.METHOD_VERSION_UPDATA_DIALOG, data);
                        if (data != null)
                            checkKick = data.isKickWaiting();
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
//                notifyClients(AttentionCoreClient.class, VersionsCoreClient.METHOD_GET_VERSION_ERROR, error.getErrorStr());
            }
        };


        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.checkUpdata(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                CheckUpdataResult.class,
                Request.Method.GET);

    }

    @Override
    public int checkKick() {
        return checkKick;
    }

    @Override
    public void getConfig() {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        OkHttpManager.getInstance().doGetRequest(UriProvider.getConfigUrl(), requestParam, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") == 200) {
                    Json data = response.json_ok("data");
//                    if (data.num("timestamps") > configdata.num("timestamps")) {
                    try {
                        Constants.FULL_PUSH_MAX = Integer.valueOf(data.str("roomMsgLeftGold"));
                        Constants.PAY_CHANNEL = Integer.valueOf(data.str("payChannel"));
                        Constants.PAY_ALIPAY_CHANNEL = Integer.valueOf(data.str("alipayChannel"));
                    } catch (Exception e) {
                        Constants.FULL_PUSH_MAX = Constants.PUSH_DEFAULT;
                        Constants.PAY_CHANNEL = Constants.PAY_CHANNEL_DEFAULT;
                    }
                    configdata = data;
                    SpUtils.put(getContext(), SpEvent.config_key, data + "");
//                    }
                }
            }
        });
    }

    @Override
    public Json getConfigData() {
        return configdata;
    }

    @Override
    public void requestSensitiveWord() {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        OkHttpManager.getInstance().doGetRequest(UriProvider.getSensitiveWord(), requestParam, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") == 200) {


                    String data = response.str("data");

                    sensitiveWordData = data;

                }

            }
        });
    }

    @Override
    public String getSensitiveWordData() {
        return sensitiveWordData == null ? "" : sensitiveWordData;
    }


}
