package com.tongdaxing.xchat_core.user;

import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * 用户信息弹窗的client
 */
public interface IUserInfoDialogCoreClient extends ICoreClient {
    String METHOD_SHOW_MSG_CHAT_DIALOG = "showMsgChatDialog";
    String METHOD_SHOW_GIFT_DIALOG = "showGiftDialogFromUserInfo";

    void showMsgChatDialog(String userId);

    void showGiftDialogFromUserInfo(String userId);
}
