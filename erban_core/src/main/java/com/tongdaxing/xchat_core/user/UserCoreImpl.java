package com.tongdaxing.xchat_core.user;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.GiftWallListResult;
import com.tongdaxing.xchat_core.result.UserListResult;
import com.tongdaxing.xchat_core.result.UserResult;
import com.tongdaxing.xchat_core.room.bean.TaskBean;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * Created by chenran on 2017/3/15.
 */

public class UserCoreImpl extends AbstractBaseCore implements IUserCore {
    private IUserDbCore userDbCore;
    private UserInfo currentUserInfo;//并非当前登录用户！
    private static final String TAG = "UserCoreImpl";
    private Map<Long, UserInfo> mInfoCache = new ConcurrentHashMap(new HashMap<Long, UserInfo>());
    public static final int TYPE_HOME = 1;
    public static final int TYPE_ATTENTION = 2;

    public UserCoreImpl() {
        super();
        userDbCore = CoreManager.getCore(IUserDbCore.class);
        CoreManager.addClient(this);
    }

    private void saveCache(long userId, UserInfo uInfo) {
        if (userId > 0 && uInfo != null) {
            mInfoCache.put(userId, uInfo);
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(final AccountInfo accountInfo) {
        long uid = accountInfo.getUid();
        SpUtils.put(getContext(), SpEvent.cache_uid, uid + "");
        currentUserInfo = getCacheUserInfoByUid(accountInfo.getUid());
        if (currentUserInfo != null && (!StringUtil.isEmpty(currentUserInfo.getNick()) && !StringUtil.isEmpty(currentUserInfo.getAvatar()))) {
            updateCurrentUserInfo(currentUserInfo.getUid());
            return;
        }

        NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(accountInfo.getUid() + "");
        if (nimUserInfo == null) {
            NimUserInfoCache.getInstance().getUserInfoFromRemote(accountInfo.getUid() + "", new RequestCallbackWrapper<NimUserInfo>() {
                @Override
                public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                    if (i == ResponseCode.RES_SUCCESS) {
                        if (nimUserInfo == null || StringUtil.isEmpty(nimUserInfo.getName()) || StringUtil.isEmpty(nimUserInfo.getAvatar())) {
                            L.debug(TAG, "nimUserInfo == null METHOD_ON_NEED_COMPLETE_INFO");
                            notifyClients(IUserClient.class, IUserClient.METHOD_ON_NEED_COMPLETE_INFO);
                        } else {
                            updateCurrentUserInfo(accountInfo.getUid());
                        }
                    } else {
                        updateCurrentUserInfo(accountInfo.getUid());
                    }
                }
            });
        } else {
            if (StringUtil.isEmpty(nimUserInfo.getName()) || StringUtil.isEmpty(nimUserInfo.getAvatar())) {
                L.debug(TAG, "nimUserInfo.getName getAvatar == null METHOD_ON_NEED_COMPLETE_INFO");
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_NEED_COMPLETE_INFO);
            } else {
                updateCurrentUserInfo(accountInfo.getUid());
            }
        }

    }

    @Override
    public void updateCurrentUserInfo(final long userId) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", String.valueOf(userId));

        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<UserResult>() {
            @Override
            public void onResponse(UserResult response) {
                if (response.isSuccess()) {
                    UserInfo userInfo = response.getData();
                    if (StringUtil.isEmpty(userInfo.getNick()) || StringUtil.isEmpty(userInfo.getAvatar())) {
                        L.debug(TAG, "updateCurrentUserInfo METHOD_ON_NEED_COMPLETE_INFO");
                        notifyClients(IUserClient.class, IUserClient.METHOD_ON_NEED_COMPLETE_INFO);
                        return;
                    }
                    saveCache(userId, response.getData());
                    userDbCore.saveDetailUserInfo(response.getData());
                    currentUserInfo = response.getData();
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE, response.getData());
                } else {
                    if (currentUserInfo == null || StringUtil.isEmpty(currentUserInfo.getNick()) || StringUtil.isEmpty(currentUserInfo.getAvatar())) {
                        notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE_FAIL, response.getMessage());
                    }
                }
            }
        };

        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if (currentUserInfo == null || StringUtil.isEmpty(currentUserInfo.getNick()) || StringUtil.isEmpty(currentUserInfo.getAvatar())) {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE_FAIL, error.getErrorStr());
                }
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getUserInfo(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                UserResult.class,
                Request.Method.GET);
    }

    @Override
    public void searchUserInfo(String skey, int pageSize, int pageNo) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("key", skey);
        params.put("pageSize", String.valueOf(pageSize));
        params.put("pageNo", String.valueOf(pageNo));
        ResponseListener listener = new ResponseListener<UserResult>() {
            @Override
            public void onResponse(UserResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IUserClient.class, IUserClient.METHOD_ON_SEARCH_USERINFO, response.getData());
                    } else {
                        notifyClients(IUserClient.class, IUserClient.METHOD_ON_SEARCH_USERINFO_FAITH, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_SEARCH_USERINFO_FAITH, error.getErrorStr());

            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.searchUserInfo(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                UserResult.class,
                Request.Method.GET);
    }

    @Override
    public boolean isNewUser() {
        UserInfo userInfo = getCacheLoginUserInfo();
        return isNewUser(userInfo);
    }

    @Override
    public boolean isNewUser(UserInfo userInfo) {
        com.tongdaxing.xchat_framework.util.util.LogUtil.d("UserCoreImpl isNewUser currentUserInfo is " + (currentUserInfo == null ? "null" : "not null"));
        if (userInfo != null) {
            //少于7天都是新用户
            if (System.currentTimeMillis() - userInfo.getCreateTime() < (86400L * 1000 * 7) || userInfo.getCreateTime() == 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void requestUserInfo(final long userId) {
        L.info(TAG, "requestUserInfo userId: %d", userId);
        if (userId <= 0) {
            return;
        }

        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", String.valueOf(userId));
        //新增参数
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<UserResult>() {
            @Override
            public void onResponse(UserResult response) {
                L.info(TAG, "requestUserInfo onResponse response: %s", response);
                if (null != response) {
                    if (response.isSuccess()) {
                        saveCache(userId, response.getData());
                        userDbCore.saveDetailUserInfo(response.getData());
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO, response.getData());
                        CoreUtils.send(new UserEvent.OnUserInfoUpdate(response.getData()));
                    } else {
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR, response.getMessage());
                    }
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR, response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                L.error(TAG, "requestUserInfo onErrorResponse response: ", error);
                notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getUserInfo(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                UserResult.class,
                Request.Method.GET);

    }

    @Nullable
    @Override
    public UserInfo getCacheUserInfoByUid(long userId) {
        return getCacheUserInfoByUid(userId, false);
    }

    @Override
    public UserInfo getCacheUserInfoByUid(long userId, boolean refresh) {
        L.info(TAG, "getCacheUserInfoByUid userId: %d", userId);
        if (userDbCore == null) {
            userDbCore = CoreManager.getCore(IUserDbCore.class);
        }

        if (userId == 0) {
            return null;
        }

        UserInfo userInfo = mInfoCache.get(userId);
        L.info(TAG, "getCacheUserInfoByUid userDbCore: %s, userInfo: %s", userDbCore, userInfo);
        if (userInfo == null) {
            if (userDbCore != null)
                userInfo = userDbCore.queryDetailUserInfo(userId);

        }

        if (userInfo == null || refresh) {
            com.tongdaxing.xchat_framework.util.util.LogUtil.d("getCacheUserInfoByUid requestUserInfo userId == " + userId);
            requestUserInfo(userId);
        }
        return userInfo;
    }

    @Override
    public void requestUserInfoMapByUidList(@NonNull final List<Long> uidListToQuery, LinkedHashMap<Long, UserInfo> rstMap) {

        if (uidListToQuery == null || uidListToQuery.size() < 1) {
            notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP, rstMap);
            return;
        }
        final long loginUserUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        final boolean uidListContainsLoginUserUid = uidListToQuery.remove(loginUserUid);
        final LinkedHashMap<Long, UserInfo> userInfoLinkedHashMap
                = (rstMap == null ? new LinkedHashMap<Long, UserInfo>(uidListToQuery.size()) : rstMap);
        final int count = uidListToQuery.size();
        final int dealNum = count > 50 ? 50 : count;//每次最多处理50个
        List<Long> paramList = uidListToQuery.subList(0, dealNum);
//        boolean removeSuccess = uidListToQuery.removeAll(paramList);
//        if (!removeSuccess) {
//            notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP_ERROR, new Object() {
//            });
//            return;
//        }
        final int leftCountToDeal = count - dealNum;
        //请求
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();

        String listStr = StringUtils.join(paramList, ",");
//        String value = paramList.toString().replaceAll("[\\[\\]]", "");
//        String listStr = join(paramList, ",");
        params.put("uids", listStr);
        ResponseListener listener = new ResponseListener<UserListResult>() {


            @Override
            public void onResponse(UserListResult response) {
                L.debug(TAG, "response==null: %b", (response == null));
                if (response != null && response.isSuccess()) {
                    List<UserInfo> userInfoList = response.getData();
                    if (userInfoList == null || userInfoList.size() < 1) {
                        return;
                    }
                    for (UserInfo userInfo : userInfoList) {
                        L.debug(TAG, "userInfo == %s", userInfo);
                        saveCache(userInfo.getUid(), userInfo);
                        userDbCore.saveDetailUserInfo(userInfo);
                        userInfoLinkedHashMap.put(userInfo.getUid(), userInfo);
                    }
                    //把本地用户加进来
                    if (uidListContainsLoginUserUid) {
                        userInfoLinkedHashMap.put(loginUserUid, CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo());
                    }
                    if (leftCountToDeal <= 0) {//最后一次更新完才通知
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP, userInfoLinkedHashMap);
                    } else {
                        requestUserInfoMapByUidList(uidListToQuery.subList(dealNum, count), userInfoLinkedHashMap);//递归调用
                    }

                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP_ERROR, new Object() {
                    });
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP_ERROR, new Object() {
                });
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getUserInfoListUrl(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                UserListResult.class,
                Request.Method.GET);
    }

    @Override
    public void requestUserInfoMapByUidList(@NonNull final List<Long> uidListToQuery, LinkedHashMap<Long, UserInfo> rstMap, final int type) {

        if (uidListToQuery == null || uidListToQuery.size() < 1) {
            if (type == TYPE_ATTENTION) {
                notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP_ATTENTION, rstMap);
            } else {
                notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP_HOME, rstMap);
            }
            return;
        }
        final LinkedHashMap<Long, UserInfo> userInfoLinkedHashMap
                = (rstMap == null ? new LinkedHashMap<Long, UserInfo>(uidListToQuery.size()) : rstMap);
        final int count = uidListToQuery.size();
        final int dealNum = count > 50 ? 50 : count;//每次最多处理50个
        List<Long> paramList = uidListToQuery.subList(0, dealNum);
//        boolean removeSuccess = uidListToQuery.removeAll(paramList);
//        if (!removeSuccess) {
//            notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP_ERROR, new Object() {
//            });
//            return;
//        }
        final int leftCountToDeal = count - dealNum;
        //请求
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();

        String listStr = StringUtils.join(paramList, ",");
//        String value = paramList.toString().replaceAll("[\\[\\]]", "");
//        String listStr = join(paramList, ",");
        params.put("uids", listStr);
        ResponseListener listener = new ResponseListener<UserListResult>() {


            @Override
            public void onResponse(UserListResult response) {
                Log.d(TAG, "外面的response===" + (response == null));
                Log.d(TAG, "response===" + response);
                if (response != null && response.isSuccess()) {
                    Log.d(TAG, "里面的response===" + (response == null));
                    List<UserInfo> userInfoList = response.getData();
                    if (userInfoList == null || userInfoList.size() < 1) {
                        return;
                    }
                    for (UserInfo userInfo : userInfoList) {
                        saveCache(userInfo.getUid(), userInfo);
                        userDbCore.saveDetailUserInfo(userInfo);
                        userInfoLinkedHashMap.put(userInfo.getUid(), userInfo);
                    }
                    if (leftCountToDeal <= 0) {//最后一次更新完才通知
                        if (type == TYPE_ATTENTION) {
                            notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP_ATTENTION, userInfoLinkedHashMap);
                        } else {
                            notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_MAP_HOME, userInfoLinkedHashMap);
                        }

                    } else {
                        requestUserInfoMapByUidList(uidListToQuery.subList(dealNum, count), userInfoLinkedHashMap, type);//递归调用
                    }

                } else {
                    if (type == TYPE_ATTENTION) {
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR_ATTENTION, new Object() {
                        });
                    } else {
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR_HOME, new Object() {
                        });
                    }

                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {

                if (type == TYPE_ATTENTION) {
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR_ATTENTION, new Object() {
                    });
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR_HOME, new Object() {
                    });
                }
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getUserInfoListUrl(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                UserListResult.class,
                Request.Method.GET);
    }

    /**
     * 插入分隔符转成字符串
     *
     * @param paramList
     * @param str
     * @return
     */
    private synchronized String join(List<Long> paramList, String str) {
        String listStr = "";
        for (int i = 0; i < paramList.size(); i++) {
//            if (listStr.length() == 0) {
//                listStr = com.xteam.xchat_core.utils.StringUtils.splic(String.valueOf(paramList.get(i)));
//            } else {
////                listStr = com.xteam.xchat_core.utils.StringUtils.splic(listStr, str, String.valueOf(paramList.get(i)));
//            }
        }
        return listStr;
    }

    public LinkedHashMap<Long, UserInfo> getCacheThenServerUserInfoMapByUidList(List<Long> uidList) {
        if (uidList == null || uidList.size() < 1) return new LinkedHashMap<>();
        LinkedHashMap<Long, UserInfo> userInfoLinkedHashMap = new LinkedHashMap<>();
        Log.d(TAG, "uidListsize===" + uidList.size());
        ArrayList<Long> uidListToQuery = new ArrayList<>();
        //从缓存取
        for (int i = 0; i < uidList.size(); i++) {
            Long userId = uidList.get(i);
            UserInfo userInfo = mInfoCache.get(userId);
            if (userInfo == null) {
                userInfo = userDbCore.queryDetailUserInfo(userId);
            }
            userInfoLinkedHashMap.put(userId, userInfo);
            if (userInfo == null) {
                uidListToQuery.add(userId);
            }
        }
        if (uidListToQuery.size() > 0) {
            Log.d(TAG, "getCacheThenServerUserInfoMapByUidList: 111111111111");
        }
        requestUserInfoMapByUidList(uidListToQuery, userInfoLinkedHashMap);
        return userInfoLinkedHashMap;
    }

    @NonNull
    @Override
    public LinkedHashMap<Long, UserInfo> getCacheThenServerUserInfoMapByUidList(List<Long> uidList, int type) {
        if (uidList == null || uidList.size() < 1) return new LinkedHashMap<>();
        LinkedHashMap<Long, UserInfo> userInfoLinkedHashMap = new LinkedHashMap<>();
        Log.d(TAG, "uidListsize===" + uidList.size());
        ArrayList<Long> uidListToQuery = new ArrayList<>();
        //从缓存取
        for (int i = 0; i < uidList.size(); i++) {
            Long userId = uidList.get(i);
            UserInfo userInfo = mInfoCache.get(userId);
            if (userInfo == null) {
                userInfo = userDbCore.queryDetailUserInfo(userId);
            }
            userInfoLinkedHashMap.put(userId, userInfo);
            if (userInfo == null) {
                uidListToQuery.add(userId);
            }
        }
        if (uidListToQuery.size() > 0) {
            Log.d(TAG, "getCacheThenServerUserInfoMapByUidList: 111111111111");
        }
        requestUserInfoMapByUidList(uidListToQuery, userInfoLinkedHashMap, type);
        return userInfoLinkedHashMap;
    }

    @Nullable
    @Override
    public UserInfo getCacheLoginUserInfo() {
        UserInfo loginUser = getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        return loginUser;
    }

    @Override
    public void requestCompleteUserInfo(final UserInfo userInfo, String shareChannel, String shareUid, String roomUid) {


//        LogUtil.d("requestCompleteUserInfo","1    "+shareUid);
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("ticket", ticket);

        //解决uid可能为空，导致一直转圈的问题
        String cacheUid = "";

        try {
            cacheUid = (String) SpUtils.get(getContext(), SpEvent.cache_uid, "");
        } catch (Exception e) {

        }

        if (!StringUtils.isEmpty(String.valueOf(userInfo.getUid()))) {
            params.put("uid", String.valueOf(userInfo.getUid()));
        } else if (!StringUtils.isEmpty(cacheUid)) {
            params.put("uid", cacheUid);
        } else {
            notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USER_INFO_COMPLETE_FAITH, "数据异常，请杀掉进程再次打开");
            return;
        }
        if (!StringUtils.isEmpty(userInfo.getBirthStr())) {

            params.put("birth", String.valueOf(userInfo.getBirthStr()));
        }
        if (!StringUtils.isEmpty(userInfo.getNick())) {
            params.put("nick", userInfo.getNick());
        }
        if (!StringUtils.isEmpty(userInfo.getAvatar())) {
            params.put("avatar", userInfo.getAvatar());
        }
        if (userInfo.getGender() > 0) {
            params.put("gender", String.valueOf(userInfo.getGender()));
        }
        if (!StringUtils.isEmpty(shareChannel)) {
            params.put("shareChannel", shareChannel);
        }
        if (!StringUtils.isEmpty(shareUid)) {
            params.put("shareUid", shareUid);
            SpUtils.put(getContext(), SpEvent.linkedMeShareUid, "");
        } else {
            String spShareUid = (String) SpUtils.get(getContext(), SpEvent.linkedMeShareUid, "");
            if (!StringUtils.isEmpty(spShareUid)) {
                params.put("shareUid", spShareUid);
                SpUtils.put(getContext(), SpEvent.linkedMeShareUid, "");
            }

        }
        if (!StringUtils.isEmpty(roomUid)) {
            if (isNumeric(roomUid))
                params.put("roomUid", roomUid);
        }

        ResponseListener listener = new ResponseListener<UserResult>() {
            @Override
            public void onResponse(UserResult response) {
                if (response != null && response.isSuccess()) {
                    UserInfo data = response.getData();
                    saveCache(data.getUid(), data);
                    userDbCore.saveDetailUserInfo(data);
                    currentUserInfo = data;
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE, response.getData());
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USER_INFO_COMPLETE, response.getData());
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USER_INFO_COMPLETE_FAITH, response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USER_INFO_COMPLETE_FAITH, "网络异常");
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.updateUserInfo(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                UserResult.class,
                Request.Method.POST);
    }

    public boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }

    @Override
    public void requestUpdateUserInfo(UserInfo userInfo, final boolean isChangeAvatar) {
        requestUpdateUserInfo(userInfo, isChangeAvatar, (String[]) null);
    }

    @Override
    public void requestUpdateUserInfo(final UserInfo userInfo, final boolean isChangeAvatar, final String... updateParams) {
        LogUtil.d("requestCompleteUserInfo", "2");
//        http://www.daxiaomao.com/user/update?ticket=&uid=900001&birth=&nick=&signture=&userVoice=&avatar=&region=&userDesc=
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("ticket", ticket);
        if (!StringUtils.isEmpty(String.valueOf(userInfo.getUid())) || eqUpdateParam("uid", updateParams)) {
            params.put("uid", String.valueOf(userInfo.getUid()));
        } else {
            return;
        }
        if (!StringUtils.isEmpty(userInfo.getBirthStr()) || eqUpdateParam("uid", updateParams)) {
            params.put("birth", String.valueOf(userInfo.getBirthStr()));
        }
        if (!StringUtils.isEmpty(userInfo.getNick()) || eqUpdateParam("nick", updateParams)) {
            params.put("nick", userInfo.getNick());
        }
        if (!StringUtils.isEmpty(userInfo.getSignture()) || eqUpdateParam("signture", updateParams)) {
            params.put("signture", userInfo.getSignture());
        }
        if (!StringUtils.isEmpty(userInfo.getUserVoice()) || eqUpdateParam("userVoice", updateParams)) {
            params.put("userVoice", userInfo.getUserVoice());
        }
        if (userInfo.getVoiceDura() > 0 || eqUpdateParam("voiceDura", updateParams)) {
            params.put("voiceDura", String.valueOf(userInfo.getVoiceDura()));
        }
        if (!StringUtils.isEmpty(userInfo.getAvatar()) || eqUpdateParam("avatar", updateParams)) {
            params.put("avatar", userInfo.getAvatar());
        }
        if (!StringUtils.isEmpty(userInfo.getRegion()) || eqUpdateParam("region", updateParams)) {
            params.put("region", userInfo.getRegion());
        }
        if (!StringUtils.isEmpty(userInfo.getUserDesc()) || eqUpdateParam("userDesc", updateParams)) {
            params.put("userDesc", userInfo.getUserDesc());
        }
        if (userInfo.getGender() > 0 || eqUpdateParam("gender", updateParams)) {
            params.put("gender", String.valueOf(userInfo.getGender()));
        }

        ResponseListener listener = new ResponseListener<UserResult>() {
            @Override
            public void onResponse(UserResult response) {
                if (response != null && response.isSuccess()) {
                    UserInfo data = response.getData();
                    saveCache(data.getUid(), data);
                    userDbCore.saveDetailUserInfo(data);
                    currentUserInfo = data;
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE, response.getData());
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_UPDAET, response.getData());
                    if (isChangeAvatar) {//通知头像编辑成功了
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_ON_USER_AVATAR_CHANGED);
                    }
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_UPDAET_ERROR, response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_UPDAET_ERROR, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.updateUserInfo(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                UserResult.class,
                Request.Method.POST);
    }

    private boolean eqUpdateParam(String param, String[] params) {
        if (!TextUtils.isEmpty(param) && params != null && params.length != 0) {
            for (String p : params) {
                if (param.equals(p)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void requestAddPhoto(String url) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("ticket", ticket);
        params.put("photoStr", url);
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");

        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    updateCurrentUserInfo(uid);
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_ADD_PHOTO);
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_ADD_PHOTO_FAITH, response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_ADD_PHOTO_FAITH, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.addPhoto(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST);
    }

    @Override
    public void requestDeletePhoto(long pid) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("ticket", ticket);

        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("pid", pid + "");

        ResponseListener listener = new ResponseListener<ServiceResult>() {
            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    updateCurrentUserInfo(uid);
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_DELETE_PHOTO);
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_DELETE_PHOTO_FAITH, response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_DELETE_PHOTO_FAITH, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.deletePhoto(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                ServiceResult.class,
                Request.Method.POST);
    }

    @Override
    public void requestUserGiftWall(final long uid, int orderType) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid + "");
        params.put("orderType", orderType + "");


        ResponseListener listener = new ResponseListener<GiftWallListResult>() {
            @Override
            public void onResponse(GiftWallListResult response) {
                if (response != null && response.isSuccess()) {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_GIFT_WALL, uid, response.getData());
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_GIFT_WALL_FAIL, uid, response.getMessage());
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_GIFT_WALL_FAIL, uid, error.getErrorStr());
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.giftWall(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                params,
                listener,
                errorListener,
                GiftWallListResult.class,
                Request.Method.GET);
    }

    @Override
    public void requestUserMysteryGiftWall(final long uid, final long queryUid, int orderType) {
        Map<String, String> params = new HashMap<>();
        params.put("uid", uid + "");
        params.put("queryUid", queryUid + "");
        params.put("orderType", orderType + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doGetRequest(UriProvider.getMysteryGiftWall(), params, new OkHttpManager.MyCallBack<ServiceResult<List<GiftWallInfo>>>() {
            @Override
            public void onError(Exception e) {
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_GIFT_WALL_FAIL, queryUid, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<GiftWallInfo>> response) {
                if (response != null && response.isSuccess()) {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_GIFT_WALL, queryUid, response.getData());
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_GIFT_WALL_FAIL, queryUid, response != null ? response.getMessage() : "数据异常");
                }
            }
        });

    }

    private boolean isEmpty(String content) {
        if (!StringUtils.isEmpty(content)) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void getTaskList() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doGetRequest(UriProvider.getTaskList(), params, new OkHttpManager.MyCallBack<ServiceResult<TaskBean>>() {
            @Override
            public void onError(Exception e) {
                if (e != null)
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_TASK_LIST_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<TaskBean> response) {
                if (response != null && response.isSuccess() && response.getData() != null) {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_TASK_LIST, response.getData());
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_TASK_LIST_FAIL, response != null ? response.getErrorMessage() : "数据异常");
                }
            }
        });
    }

}
