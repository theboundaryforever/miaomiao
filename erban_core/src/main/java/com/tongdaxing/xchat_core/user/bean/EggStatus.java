package com.tongdaxing.xchat_core.user.bean;

/**
 * Function:
 * Author: Edward on 2018/12/20
 */
public class EggStatus {

    /**
     * chatPermission : 0
     * drawMsgHide : false
     * likedSend : 1
     * uid : 100563
     */

    private int chatPermission;
    private boolean drawMsgHide;
    private int likedSend;
    private int uid;

    public int getChatPermission() {
        return chatPermission;
    }

    public void setChatPermission(int chatPermission) {
        this.chatPermission = chatPermission;
    }

    public boolean isDrawMsgHide() {
        return drawMsgHide;
    }

    public void setDrawMsgHide(boolean drawMsgHide) {
        this.drawMsgHide = drawMsgHide;
    }

    public int getLikedSend() {
        return likedSend;
    }

    public void setLikedSend(int likedSend) {
        this.likedSend = likedSend;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
}
