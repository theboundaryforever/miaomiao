package com.tongdaxing.xchat_core.user;

import com.tongdaxing.xchat_core.user.bean.UserInfo;

/**
 * Created by Chen on 2019/6/13.
 */
public class UserEvent {
    public static class OnUserInfoUpdate {
        private UserInfo mUserInfo;
        public OnUserInfoUpdate(UserInfo userInfo) {
            this.mUserInfo = userInfo;
        }

        public UserInfo getUserInfo() {
            return mUserInfo;
        }
    }

    public static class OnAdminUserShow {
        private long mPlayerId;

        public OnAdminUserShow(long playerId) {
            this.mPlayerId = playerId;
        }

        public long getPlayerId() {
            return mPlayerId;
        }
    }
}
