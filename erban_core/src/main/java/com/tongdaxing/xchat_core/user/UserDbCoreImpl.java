package com.tongdaxing.xchat_core.user;


import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;

import io.realm.Realm;

/**
 * Created by chenran on 2017/3/15.
 */

public class UserDbCoreImpl extends AbstractBaseCore implements IUserDbCore {
    private Realm mRealm;

    public UserDbCoreImpl() {
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void saveDetailUserInfo(UserInfo userInfo) {
        mRealm.beginTransaction();
        mRealm.copyToRealmOrUpdate(userInfo);
        mRealm.commitTransaction();
    }

    @Override
    public UserInfo queryDetailUserInfo(long uid) {
        UserInfo userInfo = mRealm.where(UserInfo.class).equalTo("uid", uid).findFirst();
        return userInfo;
    }

    @Override
    public void updateCpGiftLevel(long uid, int level) {
        UserInfo userInfo = mRealm.where(UserInfo.class).equalTo("uid", uid).findFirst();
        if (userInfo != null && userInfo.getCpUser() != null) {
            mRealm.beginTransaction();
            userInfo.getCpUser().setCpGiftLevel(level);
            mRealm.commitTransaction();
        }
    }
}
