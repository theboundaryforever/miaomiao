package com.tongdaxing.xchat_core.user;

import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.result.AttentionListResult;
import com.tongdaxing.xchat_core.result.FansListResult;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;

/**
 * Created by Administrator on 2017/7/5 0005.
 */

public class AttentionCoreImpl extends AbstractBaseCore implements AttentionCore {
    private static final String TAG = "AttentionCoreImpl";

    @Override
    public void getAttentionList(long uid, final int page, int pageSize) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));
        param.put("pageSize", String.valueOf(pageSize));
        param.put("pageNo", String.valueOf(page));
        ResponseListener listener = new ResponseListener<AttentionListResult>() {
            @Override
            public void onResponse(AttentionListResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_ATTENTION_LIST, response.getData(), page);
                    } else {
                        notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_ATTENTION_LIST_FAIL, response.getMessage(), page);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_ATTENTION_LIST_FAIL, error.getErrorStr(), page);
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getAllFans(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param, listener, errorListener, AttentionListResult.class, Request.Method.GET);
    }

    @Override
    public void getFansList(long uid, final int pageCount, int pageSize, final int pageType) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));
        param.put("pageNo", String.valueOf(pageCount));
        param.put("pageSize", String.valueOf(pageSize));
        ResponseListener listener = new ResponseListener<FansListResult>() {
            @Override
            public void onResponse(FansListResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_FANSLIST, response.getData(), pageType, pageCount);
                    } else {
                        notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_FANSLIST_FAIL, response.getMessage(), pageType, pageCount);
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_FANSLIST_FAIL, error.getErrorStr(), pageType, pageCount);
            }
        };

        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getFansList(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param, listener, errorListener, FansListResult.class, Request.Method.GET);
    }
}
