package com.tongdaxing.xchat_core;

import android.content.Context;

/**
 * Created by Chen on 2019/6/24.
 */
public class AppUtils {
    private static final String sDefaultAppId = "com.miaomiao.mobile";

    public static boolean isMiaoMiaoApp(Context context) {
        if (context == null) {
            return false;
        }
        String packageName = context.getPackageName();
        if (sDefaultAppId.equals(packageName)) {
            return true;
        }
        return false;
    }

}
