package com.tongdaxing.xchat_core.manager.bean;

/**
 * Created by Chen on 2019/5/6.
 */
public class MicPlayerInfo {

    /**
     * avatar : https://pic.miaomiaofm.com/FgxJP9gS9nZsHpdO9wvt9Acnl1hW?imageslim
     * cpGiftLevel : 0
     * cpUserId : 0
     * gender : 2
     * nick : 四月底呵呵呵哦哦哦哦哦哦哦嗯嗯
     * uid : 100754
     */

    private String avatar;
    private int cpGiftLevel;
    private int cpUserId;
    private int gender;
    private String nick;
    private String uid;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getCpGiftLevel() {
        return cpGiftLevel;
    }

    public void setCpGiftLevel(int cpGiftLevel) {
        this.cpGiftLevel = cpGiftLevel;
    }

    public int getCpUserId() {
        return cpUserId;
    }

    public void setCpUserId(int cpUserId) {
        this.cpUserId = cpUserId;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
