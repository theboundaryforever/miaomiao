package com.tongdaxing.xchat_core.manager.agora;

import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.SparseArray;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.util.FileStorage;
import com.tcloud.core.util.LimitClickUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.OnLoginCompletionListener;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.zego.BaseAudioEngine;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;

import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_HIGH_QUALITY;
import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_STANDARD;
import static io.agora.rtc.Constants.AUDIO_RECORDING_QUALITY_LOW;

/**
 * <p> 声网管理类 </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public final class AgoraEngineManager extends BaseAudioEngine {
    private static volatile AgoraEngineManager sEngineManager;
    private static final Object SYNC_OBJECT = new Object();
    private static final String TAG = "AgoraEngineManager";

    private RtcEngine mRtcEngine;
    /**
     * 队列说话列表
     */
    private List<Integer> speakQueueMembersPosition;
    private String uid;
    /**
     * 麦上是否闭麦，true：闭麦，false：开麦
     */
    private boolean isPause;
    private boolean needRecord;
    private EngineEventHandler engineEventHandler;

    private AgoraEngineManager() {
        speakQueueMembersPosition = new ArrayList<>();
    }

    public static AgoraEngineManager get() {
        if (sEngineManager == null) {
            synchronized (SYNC_OBJECT) {
                if (sEngineManager == null) {
                    sEngineManager = new AgoraEngineManager();
                }
            }
        }
        return sEngineManager;
    }

    @Override
    public void setOnLoginCompletionListener(OnLoginCompletionListener listener) {

    }

    public void startRtcEngine(long uid, String token, RoomInfo curRoomInfo) {
        L.info(TAG, "curRoomInfo.getAudioLevel() = " + curRoomInfo.getAudioLevel());
        joinChannel(curRoomInfo.getRoomId(), curRoomInfo.getAudioLevel(), uid, token);
        if (curRoomInfo.getUid() == uid && curRoomInfo.getType() != RoomInfo.ROOMTYPE_HOME_PARTY) {
            //设置用户角色为主播,轰趴房不能默认设置房主为主播
            setRole(Constants.CLIENT_ROLE_BROADCASTER);
        } else {
            setRole(Constants.CLIENT_ROLE_AUDIENCE);
        }
    }

    private void joinChannel(long channelId, int audioLevel, long uid, String token) {
        int quality = AUDIO_PROFILE_MUSIC_STANDARD;
        if (audioLevel != 0) {//目前只有2种音质 0-AUDIO_PROFILE_MUSIC_STANDARD 非0-AUDIO_PROFILE_MUSIC_HIGH_QUALITY
            quality = AUDIO_PROFILE_MUSIC_HIGH_QUALITY;
        }
        initRtcEngine(channelId, uid, quality, token);
    }

    private void initRtcEngine(long channelId, long uid, int quality, String token) {
        L.info(TAG, "initRtcEngine channelId: %d, uid: %d, quality: %d", channelId, uid, quality);
        this.uid = uid + "";
        this.isMute = false;
        this.isRemoteMute = false;
        if (mRtcEngine == null) {
            try {
                if (engineEventHandler == null)
                    engineEventHandler = new EngineEventHandler(this);
                mRtcEngine = RtcEngine.create(BasicConfig.INSTANCE.getAppContext(), "9fec61b770264a498108d1518ddfee8e", engineEventHandler);
            } catch (Exception e) {
                L.info(TAG, "need to check rtc sdk init fatal error = " + e != null ? e.getMessage() : "");
            }
            //设置频道模式为直播
            mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
            mRtcEngine.enableAudioVolumeIndication(600, 10);
            mRtcEngine.setDefaultAudioRoutetoSpeakerphone(true);

            // 根目录下的miaomiao这个文件夹
            mRtcEngine.setLogFile(String.format("%s/%s/%s",
                    FileStorage.getInstance().getRootDir(FileStorage.Location.SDCard).getParentFile(),
                    L.sLogPath, "/agora-rtc.log"));
        }
        if (mRtcEngine != null) {
            //创建并加入频道
            mRtcEngine.setAudioProfile(quality, Constants.AUDIO_SCENARIO_GAME_STREAMING);
            mRtcEngine.joinChannel(token, String.valueOf(channelId), null, (int) uid);
        }
    }

    /**
     * 开启/关闭说话监听（耳返）
     */
    public void enableInEarMonitoring(boolean enable) {
        mRtcEngine.enableInEarMonitoring(enable);
    }

    /**
     * 设置变声
     */
    public void setVideoFilter(int pitch,
                               int e31,
                               int e62,
                               int e125,
                               int e250,
                               int e500,
                               int e1k,
                               int e2k,
                               int e4k,
                               int e8k,
                               int e16k,
                               int room,
                               int delay,
                               int strength,
                               int dry,
                               int wet) {
        mRtcEngine.setLocalVoicePitch((pitch * 1.f / 100.f) > 0.5f ? (pitch * 1.f / 100.f) : 0.5f);

        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_31, e31);
        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_62, e62);
        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_125, e125);
        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_250, e250);
        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_500, e500);
        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_1K, e1k);
        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_2K, e2k);
        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_4K, e4k);
        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_8K, e8k);
        mRtcEngine.setLocalVoiceEqualization(Constants.AUDIO_EQUALIZATION_BAND_16K, e16k);

        mRtcEngine.setLocalVoiceReverb(Constants.AUDIO_REVERB_ROOM_SIZE, room);
        mRtcEngine.setLocalVoiceReverb(Constants.AUDIO_REVERB_WET_DELAY, delay);
        mRtcEngine.setLocalVoiceReverb(Constants.AUDIO_REVERB_STRENGTH, strength);
        mRtcEngine.setLocalVoiceReverb(Constants.AUDIO_REVERB_DRY_LEVEL, dry);
        mRtcEngine.setLocalVoiceReverb(Constants.AUDIO_REVERB_WET_LEVEL, wet);
    }

    public void stopAudioMixing() {
        L.info(TAG, "stopAudioMixing");
        if (mRtcEngine != null) {
            mRtcEngine.stopAudioMixing();
        }
//        return -1;
    }

    public void leaveChannel() {
        L.info(TAG, "leaveChannel");
        if (mRtcEngine != null) {
            stopAudioMixing();
            mRtcEngine.leaveChannel();
        }
        if (handler != null)
            handler.removeCallbacksAndMessages(null);
        isAudienceRole = true;
        isMute = false;
        isRemoteMute = false;
        needRecord = false;
    }

    public boolean isQueueSpeaking(int position) {
        if (speakQueueMembersPosition != null) {
            if (speakQueueMembersPosition.contains(position)) {
                return true;
            }
        }
        return false;
    }

    public void setRemoteMute(boolean mute) {
        L.info(TAG, "setRemoteMute mute: %b", mute);
        if (mRtcEngine != null) {
            int result = mRtcEngine.muteAllRemoteAudioStreams(mute);
            if (result == 0) {
                isRemoteMute = mute;
            } else {
                L.info(TAG, "mute = " + mute + "  result = " + result);
            }
        }
    }

    /**
     * 接收/停止接收指定音频流
     *
     * @param uid   指定用户uid
     * @param muted true：停止接收指定用户的音频流 false：继续接收指定用户的音频流（默认）
     */
    public void muteRemoteAudioStream(int uid, boolean muted) {
        L.info(TAG, "setRemoteMute uid: %d, mute: %b", uid, muted);
        if (mRtcEngine != null) {
            mRtcEngine.muteRemoteAudioStream(uid, muted);
        }
    }

    /**
     * 设置角色，上麦，下麦（调用）
     *
     * @param role CLIENT_ROLE_AUDIENCE: 听众 ，CLIENT_ROLE_BROADCASTER: 主播
     */
    public void setRole(int role) {
        if (mRtcEngine != null) {
            int result;
            if (role == Constants.CLIENT_ROLE_BROADCASTER && IMNetEaseManager.get().isConnected()) {//已连接才能说话
                result = mRtcEngine.setClientRole(isMute ? Constants.CLIENT_ROLE_AUDIENCE : Constants.CLIENT_ROLE_BROADCASTER);
                L.info(TAG, "role = " + role + "  result = " + result);
                isAudienceRole = false;
            } else {
                result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
                L.info(TAG, "role = " + role + "  result = " + result);
                isAudienceRole = true;
            }

            //开麦add push url，闭麦remove
            if (result == 0) {
                boolean isMicOpen = role == Constants.CLIENT_ROLE_BROADCASTER && !isMute;
                L.info(TAG, "setRole-isMicOpen:" + isMicOpen);
            }
        }
    }

    /**
     * 设置是否能说话，静音,人自己的行为
     *
     * @param mute true：静音，false：不静音
     */
    public void setMute(boolean mute) {
        if (mRtcEngine != null) {
            int result;
            if (!mute && IMNetEaseManager.get().isConnected()) {//已连接才能说话
                result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
                if (result == 0) {
                    isMute = false;
                } else {
                    L.info(TAG, "mute = " + false + "  result = " + result);
                }
            } else {
                result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
                if (result == 0) {
                    isMute = true;
                } else {
                    L.info(TAG, "mute = " + mute + "  result = " + result);
                }
            }

            if (result == 0) {
                boolean isMicOpen = !isAudienceRole && !isMute;
                L.info(TAG, "setRole-isMicOpen:" + isMicOpen);
            }
        }
        L.info(TAG, "setMute mute: %b, isMute: %B", mute, isMute);
    }

    private Handler handler = new RtcEngineHandler(this);

    private static class RtcEngineHandler extends Handler {
        private WeakReference<AgoraEngineManager> mReference;

        RtcEngineHandler(AgoraEngineManager manager) {
            super(Looper.getMainLooper());
            mReference = new WeakReference<>(manager);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            AgoraEngineManager rtcEngineManager = mReference.get();
            if (rtcEngineManager == null) return;
            if (msg.what == 0) {
                IMNetEaseManager.get().joinAvRoom();
                if (rtcEngineManager.needRecord) {
                    rtcEngineManager.mRtcEngine.startAudioRecording(Environment.getExternalStorageDirectory()
                            + File.separator + BasicConfig.INSTANCE.getAppContext().getPackageName()
                            + "/audio/" + System.currentTimeMillis() + ".aac", AUDIO_RECORDING_QUALITY_LOW);
                }
            } else if (msg.what == 1) {
                IRtcEngineEventHandler.AudioVolumeInfo[] speakers = (IRtcEngineEventHandler.AudioVolumeInfo[]) msg.obj;
                RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (roomInfo == null) return;
                if (rtcEngineManager.speakQueueMembersPosition.size() > 0) {
                    rtcEngineManager.speakQueueMembersPosition.clear();
                }
                String exceptionUserId = "";//多次异常的uid，多个逗号隔开（麦上并没有说话的这个人，疑似炸房）
                if (speakers != null && speakers.length > 0) {
                    for (IRtcEngineEventHandler.AudioVolumeInfo speaker : speakers) {
                        // 0 代表的是自己,其他代表的是uid
                        int uid = speaker.uid == 0 ? Integer.valueOf(rtcEngineManager.uid) : speaker.uid;
                        int micPosition = AvRoomDataManager.get().getMicPosition(uid);
                        if (micPosition == Integer.MIN_VALUE) {//麦上并没有说话的这个人
                            if (roomInfo.isAlarmEnable() && speaker.volume > 0) {//开启了炸房检测 && 声音>0
                                if (System.currentTimeMillis() - AvRoomDataManager.get().getTimeOnMicDown(uid) > roomInfo.getTimeInterval()) {//这个人下麦时间已经超过设定的延时时间了（因为声音有延时，避免误杀）
                                    //停止接收此用户的推流
                                    AgoraEngineManager.get().muteRemoteAudioStream(uid, true);
                                    if (!TextUtils.isEmpty(exceptionUserId)) {
                                        exceptionUserId = exceptionUserId.concat(",").concat(String.valueOf(uid));
                                    } else {//第1个前面不加逗号
                                        exceptionUserId = exceptionUserId.concat(String.valueOf(uid));
                                    }
                                }
                            }
                        } else {
                            rtcEngineManager.speakQueueMembersPosition.add(micPosition);
                        }
                    }
                }
                if (!TextUtils.isEmpty(exceptionUserId) && AvRoomDataManager.get().isOnMic(rtcEngineManager.uid)) {//自己在麦上才做异常上报
                    L.info(TAG, "exceptionUserId = " + exceptionUserId);
                    CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.METHOD_ON_SPEAKER_EXCEPTION, exceptionUserId);
                }
                IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                        new RoomEvent().setEvent(RoomEvent.SPEAK_STATE_CHANGE)
                                .setMicPositionList(rtcEngineManager.speakQueueMembersPosition)
                );
            } else if (msg.what == 2) {
                Integer uid = (Integer) msg.obj;
            }
        }
    }

    private static class EngineEventHandler extends IRtcEngineEventHandler {
        private WeakReference<AgoraEngineManager> mReference;
        private SparseArray<VolumeWorkerRunnable> mVolumeWorkerRunnableArray = new SparseArray<>();
        private LimitClickUtils mLimitUtils = new LimitClickUtils();

        EngineEventHandler(AgoraEngineManager manager) {
            mReference = new WeakReference<>(manager);
        }

        @Override
        public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
            super.onJoinChannelSuccess(channel, uid, elapsed);
            L.info(TAG, "onJoinChannelSuccess  --  " + this);
            if (mReference.get() != null) {
                mReference.get().handler.sendEmptyMessage(0);
            }
        }

        @Override
        public void onLeaveChannel(RtcStats stats) {
            super.onLeaveChannel(stats);
            L.info(TAG, "onLeaveChannel");
            if (mVolumeWorkerRunnableArray != null) {
                AgoraEngineManager agoraEngineManager = mReference.get();
                if (agoraEngineManager != null) {
                    for (int i = 0; i < mVolumeWorkerRunnableArray.size(); i++) {
                        agoraEngineManager.handler.removeCallbacks(mVolumeWorkerRunnableArray.valueAt(i));
                    }
                    mVolumeWorkerRunnableArray.clear();
                }
            }
        }

        @Override
        public void onUserJoined(int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);
            L.info(TAG, "onUserJoined-uid:" + uid + " elapsed:" + elapsed);
//            if (mReference.get() != null) {
//                mReference.get().cdnLayout(uid);
//                mReference.get().setLiveTranscoding();
//            }
            int micPosition = AvRoomDataManager.get().getMicPosition(uid);
            mVolumeWorkerRunnableArray.put(micPosition, new VolumeWorkerRunnable(micPosition));
        }

        @Override
        public void onUserOffline(int uid, int reason) {
            super.onUserOffline(uid, reason);
            L.info(TAG, "onUserOffline-uid:" + uid + " reason:" + reason);
//            if (mReference.get() != null) {
//                mReference.get().removeCdnLayout(uid);
//                mReference.get().setLiveTranscoding();
//            }
            if (uid > 0) {
                int micPosition = AvRoomDataManager.get().getMicPosition(uid);
                mVolumeWorkerRunnableArray.get(micPosition).run();
                mVolumeWorkerRunnableArray.remove(micPosition);
            }
        }

        @Override
        public void onActiveSpeaker(int uid) {
            super.onActiveSpeaker(uid);
        }

        @Override
        public void onLastmileQuality(int quality) {
            super.onLastmileQuality(quality);
            if (quality >= 3) {
                IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                        new RoomEvent().setEvent(RoomEvent.RTC_ENGINE_NETWORK_BAD)
                );
            }
        }

        @Override
        public void onConnectionInterrupted() {
            super.onConnectionInterrupted();
            IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                    new RoomEvent().setEvent(RoomEvent.RTC_ENGINE_NETWORK_CLOSE)
            );
        }

        @Override
        public void onConnectionLost() {
            super.onConnectionLost();
            IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                    new RoomEvent().setEvent(RoomEvent.RTC_ENGINE_NETWORK_CLOSE)
            );
        }

        /**
         * 提示频道内谁正在说话以及说话者音量的回调。
         *
         * @param speakers    如果收到的音量提示回调中 speakers 数组为空，表示此时远端没有人说话。
         * @param totalVolume
         */
        @Override
        public void onAudioVolumeIndication(AudioVolumeInfo[] speakers, int totalVolume) {
            super.onAudioVolumeIndication(speakers, totalVolume);
            AgoraEngineManager manager = mReference.get();
            if (manager != null) {
                Message message = manager.handler.obtainMessage();
                message.what = 1;
                message.obj = speakers;
                manager.handler.sendMessage(message);
            }

            for (AudioVolumeInfo speaker : speakers) {
                if (!mLimitUtils.checkForTime("onAudioVolumeIndication", 3000)) {
                    L.info(TAG, "onAudioVolumeIndication speaker:%d -> %d ",
                            speaker.uid, speaker.volume);
                }
                onPlayerVolume(speaker.uid, totalVolume);
            }
        }

        private void onPlayerVolume(long playerId, int volume) {
            if (playerId == 0) {
                playerId = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            }
            int pos = AvRoomDataManager.get().getMicPosition(playerId);

            VolumeWorkerRunnable volumeWorkerRunnable = mVolumeWorkerRunnableArray.get(pos);
            if (volumeWorkerRunnable == null) {
                // 把说话的用户id存起来
                volumeWorkerRunnable = new VolumeWorkerRunnable(pos);
                mVolumeWorkerRunnableArray.put(pos, volumeWorkerRunnable);
            }
            AgoraEngineManager agoraEngineManager = mReference.get();
            if (agoraEngineManager != null) {
                agoraEngineManager.handler.removeCallbacks(volumeWorkerRunnable);
                agoraEngineManager.handler.postDelayed(volumeWorkerRunnable, 2000);
                CoreUtils.send(new LiveEvent.OnSpeakerVolume(pos, volume));
            }
        }

        /**
         * 远端用户静音回调
         * 提示有其他用户将他的音频流静音/取消静音。
         *
         * @param uid
         * @param muted
         */
        @Override
        public void onUserMuteAudio(int uid, boolean muted) {
            super.onUserMuteAudio(uid, muted);
            L.info(TAG, "onUserMuteAudio uid: %d, muted: %b", uid, muted);
            AgoraEngineManager manager = mReference.get();
            if (manager != null) {
                if (muted) {
                    Message message = manager.handler.obtainMessage();
                    message.what = 2;
                    message.obj = uid;
                    manager.handler.sendMessage(message);
                }
            }
        }

        @Override
        public void onAudioMixingFinished() {
            super.onAudioMixingFinished();
            L.info(TAG, "onAudioMixingFinished");
            IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                    new RoomEvent().setEvent(RoomEvent.METHOD_ON_AUDIO_MIXING_FINISHED)
            );
        }
    }

    //音乐播放相关---------------begin--------------------------

    public void adjustAudioMixingVolume(int volume) {
        L.info(TAG, "adjustAudioMixingVolume volume: %d", volume);
        if (mRtcEngine != null) {
            mRtcEngine.adjustAudioMixingVolume(volume);
        }
    }

    public void adjustRecordingSignalVolume(int volume) {
        L.info(TAG, "adjustRecordingSignalVolume volume: %d", volume);
        if (mRtcEngine != null) {
            mRtcEngine.adjustRecordingSignalVolume(volume);
        }
    }

    @Override
    public void resumeAudioMixing() {
        L.info(TAG, "resumeAudioMixing");
        if (mRtcEngine != null) {
            mRtcEngine.resumeAudioMixing();
        }
    }

    @Override
    public void pauseAudioMixing() {
        L.info(TAG, "pauseAudioMixing");
        if (mRtcEngine != null) {
            mRtcEngine.pauseAudioMixing();
        }
    }

    /**
     * 获取当前房间伴奏播放位置
     *
     * @return
     */
    @Override
    public long getAudioMixingCurrentPosition() {
        int currPosition = -1;
        if (mRtcEngine != null) {
            currPosition = mRtcEngine.getAudioMixingCurrentPosition();
        }
        L.info(TAG, "getAudioMixingCurrentPosition currPosition: %d", currPosition);
        return currPosition;
    }

    /**
     * 设置当前房间伴奏播放进度(百分比)
     *
     * @param position
     * @return
     */
    @Override
    public void setAudioMixingPosition(int position) {
        L.info(TAG, "setAudioMixingPosition position: %d", position);
        if (mRtcEngine != null) {
            int totalDur = mRtcEngine.getAudioMixingDuration();
            if (totalDur > 0) {
                int currDur = position * totalDur / 100;
                mRtcEngine.setAudioMixingPosition(currDur);
            }
        }
    }

    public long getAudioMixingDuration() {
        int dur = -1;
        if (mRtcEngine != null) {
            dur = mRtcEngine.getAudioMixingDuration();
        }
        L.info(TAG, "getAudioMixingDuration dur: %d", dur);
        return dur;
    }

    @Override
    public void stopPlayingStream(String uid) {
        L.info(TAG, "stopPlayingStream uid: %d", uid);
    }

    public int startAudioMixing(String filePath, boolean loopback, int cycle) {
        L.info(TAG, "startAudioMixing filePath: %s, loopback: %b, uid: %d", filePath, loopback, cycle);
        if (mRtcEngine != null) {
            mRtcEngine.stopAudioMixing();
            int result = 0;
            try {
                result = mRtcEngine.startAudioMixing(filePath, loopback, false, cycle);
            } catch (Exception e) {
                return -1;
            }
            return result;
        }
        return -1;
    }
}
