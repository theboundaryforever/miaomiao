package com.tongdaxing.xchat_core.manager;

import android.content.Context;

import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 * 房间底部更多功能按钮的红点管理
 */
public class RoomBottomMarkManager {

    public final static int BUTTON_TYPE_WHEEL_SURF = 0, BUTTON_TYPE_OPEN_PK = 1, BUTTON_TYPE_SET_AUDIO = 2, BUTTON_TYPE_ROOM_SETTING = 3, BUTTON_TYPE_QUESTION_REPORT = 4;

    @SuppressWarnings("StaticFieldLeak")
    private static volatile RoomBottomMarkManager instance;
    private Context context;

    //当前版本需要显示的红点
    private static int[] CURR_VERSION_MARKS = {BUTTON_TYPE_WHEEL_SURF};

    private RoomBottomMarkManager(Context context) {
        this.context = context.getApplicationContext();
    }

    public static RoomBottomMarkManager getInstance(Context context) {
        if (instance == null) {
            synchronized (RoomBottomMarkManager.class) {
                if (instance == null) {
                    instance = new RoomBottomMarkManager(context);
                }
            }
        }
        return instance;
    }

    /**
     * button的mark是否显示
     */
    public boolean isButtonMark(int buttonType) {
        for (int mark : CURR_VERSION_MARKS) {
            if (mark == buttonType) {//当前mark理应显示
                return isMarkNoCleared(buttonType);//如果没清除过就显应该示
            }
        }
        return false;
    }

    /**
     * 是否有button的mark需要显示
     */
    public boolean isHaveButtonMark() {
        Json configData = CoreManager.getCore(VersionsCore.class).getConfigData();
        if (configData != null) {
            if (!"true".equals(configData.str("turntableOption"))) {
                controlMoreButtonMarkWithWheelSurf();
            }
        } else {
            controlMoreButtonMarkWithWheelSurf();
        }
        for (int mark : CURR_VERSION_MARKS) {
            if (mark != -1 && isMarkNoCleared(mark)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 没有开启大转盘时，+号也不用显示红点
     */
    private void controlMoreButtonMarkWithWheelSurf() {
        if (CURR_VERSION_MARKS.length > 0) {
            for (int i = 0; i < CURR_VERSION_MARKS.length; i++) {
                if (CURR_VERSION_MARKS[i] == BUTTON_TYPE_WHEEL_SURF) {
                    CURR_VERSION_MARKS[i] = -1;
                }
            }
        }
    }

    /**
     * mark是否没被清除
     */
    private boolean isMarkNoCleared(int buttonType) {
        try {
            return !(boolean) SpUtils.get(context, SpEvent.roomBottomMarkClear + buttonType, false);//是否没清除过mark
        } catch (Exception e) {
            e.printStackTrace();
            return true;//宁愿红点不出来
        }
    }

    /**
     * 清除mark
     */
    public void clearMark(int buttonType) {
        SpUtils.put(context, SpEvent.roomBottomMarkClear + buttonType, true);
        CoreManager.notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_BOTTOM_MARK_CLEAR);
    }
}
