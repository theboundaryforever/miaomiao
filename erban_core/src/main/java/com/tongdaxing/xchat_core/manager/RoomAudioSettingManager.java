package com.tongdaxing.xchat_core.manager;

import android.content.Context;

import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * 房间调音设置管理类
 */
public class RoomAudioSettingManager {
    @SuppressWarnings("StaticFieldLeak")
    private static volatile RoomAudioSettingManager instance;
    private Context context;

    public static final int VOICE_FILTER_TYPE_NORMAL = 0, VOICE_FILTER_TYPE_UNCLE = 1, VOICE_FILTER_TYPE_BOY = 2,
            VOICE_FILTER_TYPE_FEMALE = 3, VOICE_FILTER_TYPE_LOLITA = 4, VOICE_FILTER_TYPE_MONSTER = 5;

    private static final int[] VOICE_FILTER_NORMAL = {100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    private static final int[] VOICE_FILTER_UNCLE = {80, -15, 0, 6, 1, -4, 1, -10, -5, 3, 3, 0, 90, 43, -6, -12};
    private static final int[] VOICE_FILTER_BOY = {123, 15, 11, -3, -5, -7, -7, -9, -15, -15, -15, 0, 91, 44, 4, 2};
    private static final int[] VOICE_FILTER_FEMALE = {121, -2, 4, 5, 6, 5, 4, 11, 11, -3, 15, 0, 32, 45, -12, -14};
    private static final int[] VOICE_FILTER_LOLITA = {145, 10, 6, 1, 1, -6, 13, 7, -14, 13, -13, 0, 31, 44, -11, -7};
    private static final int[] VOICE_FILTER_MONSTER = {50, -15, 3, -9, -8, -6, -4, -3, -2, -1, 1, 76, 124, 78, 10, -9};

    private RoomAudioSettingManager(Context context) {
        this.context = context.getApplicationContext();
    }

    public static RoomAudioSettingManager getInstance(Context context) {
        if (instance == null) {
            synchronized (RoomAudioSettingManager.class) {
                if (instance == null) {
                    instance = new RoomAudioSettingManager(context);
                }
            }
        }
        return instance;
    }

    /**
     * 说话监听是否开启（耳返是否开启）
     */
    public boolean isAudioMonitorEnable() {
        try {
            return (boolean) SpUtils.get(context, SpEvent.roomAudioSetting_audioMonitorEnable + CoreManager.getCore(IAuthCore.class).getCurrentUid(), false);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 设置说话监听（耳返）开启/关闭
     */
    public void enableAudioMonitor(boolean enable) {
//        RtcEngineManager.get().enableInEarMonitoring(enable);
//        SpUtils.put(context, SpEvent.roomAudioSetting_audioMonitorEnable + CoreManager.getCore(IAuthCore.class).getCurrentUid(), enable);
    }

    /**
     * 获取设置的变声
     */
    public int getAudioFilter() {
        try {
            return (int) SpUtils.get(context, SpEvent.roomAudioSetting_sound_filter + CoreManager.getCore(IAuthCore.class).getCurrentUid(), VOICE_FILTER_TYPE_NORMAL);
        } catch (Exception e) {
            e.printStackTrace();
            return VOICE_FILTER_TYPE_NORMAL;
        }
    }

    /**
     * 设置变声
     *
     * @param audioFilterType 例如 : RoomAudioSettingManager.VOICE_FILTER_TYPE_UNCLE
     */
    public void setAudioFilter(int audioFilterType) {
        int[] audioFilter = null;
        switch (audioFilterType) {
            case VOICE_FILTER_TYPE_NORMAL:
                audioFilter = VOICE_FILTER_NORMAL;
                break;
            case VOICE_FILTER_TYPE_UNCLE:
                audioFilter = VOICE_FILTER_UNCLE;
                break;
            case VOICE_FILTER_TYPE_BOY:
                audioFilter = VOICE_FILTER_BOY;
                break;
            case VOICE_FILTER_TYPE_FEMALE:
                audioFilter = VOICE_FILTER_FEMALE;
                break;
            case VOICE_FILTER_TYPE_LOLITA:
                audioFilter = VOICE_FILTER_LOLITA;
                break;
            case VOICE_FILTER_TYPE_MONSTER:
                audioFilter = VOICE_FILTER_MONSTER;
                break;
        }
        if (audioFilter != null && audioFilter.length >= 16) {
//            RtcEngineManager.get().setVideoFilter(audioFilter[0], audioFilter[1], audioFilter[2], audioFilter[3], audioFilter[4], audioFilter[5], audioFilter[6],
//                    audioFilter[7], audioFilter[8], audioFilter[9], audioFilter[10], audioFilter[11], audioFilter[12], audioFilter[13], audioFilter[14], audioFilter[15]);
//            SpUtils.put(context, SpEvent.roomAudioSetting_sound_filter + CoreManager.getCore(IAuthCore.class).getCurrentUid(), audioFilterType);
        }
    }
}
