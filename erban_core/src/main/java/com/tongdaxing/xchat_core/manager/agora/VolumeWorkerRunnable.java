package com.tongdaxing.xchat_core.manager.agora;

import com.tcloud.core.CoreUtils;

/**
 * Created by Chen on 2018/12/28.
 */
public class VolumeWorkerRunnable implements Runnable {
    private int mPlayerPosition;

    public VolumeWorkerRunnable(int playerPosition) {
        this.mPlayerPosition = playerPosition;
    }

    @Override
    public void run() {
        CoreUtils.send(new LiveEvent.OnSpeakerVolume(mPlayerPosition, 0));
    }

    public int getPlayerPosition() {
        return mPlayerPosition;
    }
}
