package com.tongdaxing.xchat_core.manager;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomNotificationAttachment;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomQueueChangeAttachment;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomStatusChangeData;
import com.netease.nimlib.sdk.chatroom.model.MemberOption;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.attachment.NotificationAttachment;
import com.netease.nimlib.sdk.msg.constant.ChatRoomQueueChangeType;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.NotificationType;
import com.netease.nimlib.sdk.msg.model.NIMAntiSpamOption;
import com.netease.nimlib.sdk.util.Entry;
import com.netease.nimlib.sdk.util.api.RequestResult;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.listener.IDisposableAddListener;
import com.tongdaxing.erban.libcommon.net.exception.ErrorThrowable;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;
import com.tongdaxing.xchat_core.cp.IRoomCpCoreClient;
import com.tongdaxing.xchat_core.find.SquareClient;
import com.tongdaxing.xchat_core.find.bean.SquareQueueInfo;
import com.tongdaxing.xchat_core.gift.CpGiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.AuctionAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.BlessingBeastAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CpGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MicInListAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NewPushAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PersonalAttentionAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmListAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.SquareCpMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.WeekCpAttachment;
import com.tongdaxing.xchat_core.im.room.IIMRoomCoreClient;
import com.tongdaxing.xchat_core.makefriedns.BroadcastMsgEvent;
import com.tongdaxing.xchat_core.makefriedns.FindBroadCastChatRoomController;
import com.tongdaxing.xchat_core.manager.bean.MicPlayerInfo;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.NewRoomEvent;
import com.tongdaxing.xchat_core.room.RoomAction;
import com.tongdaxing.xchat_core.room.auction.bean.AuctionInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.room.model.AuctionModel;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomManager;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.talk.message.AttentionMessage;
import com.tongdaxing.xchat_core.room.talk.message.CommonMessage;
import com.tongdaxing.xchat_core.room.talk.message.EggMessage;
import com.tongdaxing.xchat_core.room.talk.message.GiftMessage;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;
import com.tongdaxing.xchat_core.room.talk.message.NotificationMessage;
import com.tongdaxing.xchat_core.room.talk.message.TalkMessage;
import com.tongdaxing.xchat_core.room.talk.message.WeekCpMessage;
import com.tongdaxing.xchat_core.room.weekcp.week.WeekCpEvent;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.Constants;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_AUTH_STATUS_NOTIFY;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_BLESSING_BEAST;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_BLESSING_BEAST_SECONDS;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_AUCTION;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_FIRST;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_LOTTERY_BOX;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_MIC_IN_LIST;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_CHARM;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_CP_BOTTOM;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_CP_REQUEST;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_CP_STATE_REFRESH;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_CP_TOP;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_SECOND_CHARM_CLEAR;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_SECOND_CHARM_UPDATE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SQUARE_BROADCAST_MSG;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_TURN_TABLE_DETONATING;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_TURN_TABLE_NO_BUBBLE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_TURN_TABLE_PIECES_EXCHANGE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_TURN_TABLE_WITH_BUBBLE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_TYPE_BURST_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_TYPE_NEW_PUSH_FIRST;
import static com.tongdaxing.xchat_core.room.model.AvRoomModel.PUBLIC_CHAT_ROOM;

/**
 * <p>云信聊天室管理，一个全局的Model </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public final class IMNetEaseManager {
    private static final String TAG = "IMNetEaseManager";

    private static final Object SYNC_OBJECT = new Object();
    private static volatile IMNetEaseManager sManager;
    private final AvRoomModel model;

    /**
     * 是否已连接
     */
    private boolean isConnected;

    private RoomQueueInfo mCacheRoomQueueInfo;

    public JSONObject roomCharmList;

    public static IMNetEaseManager get() {
        if (sManager == null) {
            synchronized (SYNC_OBJECT) {
                if (sManager == null) {
                    sManager = new IMNetEaseManager();
                }
            }
        }
        return sManager;
    }

    private IMNetEaseManager() {
        registerChatMessageReceive();
        registerKickoutEvent();
        registerOnlineStatusChange();
        model = new AvRoomModel();
    }

    public void dealCharmUpdate(JSONObject json) {
        roomCharmList = json;

        noticeRoomCharm();
    }

    private void registerOnlineStatusChange() {
        Observer<ChatRoomStatusChangeData> onlineStatus = new Observer<ChatRoomStatusChangeData>() {
            @Override
            public void onEvent(ChatRoomStatusChangeData chatRoomStatusChangeData) {
                dealChatRoomOnlineStatus(chatRoomStatusChangeData);
            }
        };
        NIMChatRoomSDK.getChatRoomServiceObserve().observeOnlineStatus(onlineStatus, true);
    }

    private void registerKickoutEvent() {
        Observer<ChatRoomKickOutEvent> kickOutObserver = new Observer<ChatRoomKickOutEvent>() {
            @Override
            public void onEvent(ChatRoomKickOutEvent chatRoomKickOutEvent) {
                L.info(TAG, "收到踢人信息");
                // 提示被踢出的原因（聊天室已解散、被管理员踢出、被其他端踢出等
                Map<String, Object> extension = chatRoomKickOutEvent.getExtension();
                String account = null;
                if (extension != null) {
                    account = (String) extension.get("account");
                }
                noticeKickOutChatMember(chatRoomKickOutEvent, account);
                // 清空缓存数据
                AvRoomDataManager.get().release();
            }
        };
        NIMChatRoomSDK.getChatRoomServiceObserve().observeKickOutEvent(kickOutObserver, true);
    }

    private void registerChatMessageReceive() {
        Observer<List<ChatRoomMessage>> chatObserver = new Observer<List<ChatRoomMessage>>() {
            @Override
            public void onEvent(List<ChatRoomMessage> chatRoomMessages) {
                if (ListUtils.isListEmpty(chatRoomMessages)) {
                    return;
                }
                dealChatMessage(chatRoomMessages);
            }
        };
        NIMChatRoomSDK.getChatRoomServiceObserve().observeReceiveMessage(chatObserver, true);
    }


    private void dealChatRoomOnlineStatus(ChatRoomStatusChangeData chatRoomStatusChangeData) {
        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (chatRoomStatusChangeData.status == StatusCode.CONNECTING) {
            L.info(TAG, "连接中...");
        } else if (chatRoomStatusChangeData.status == StatusCode.UNLOGIN) {
            setConnected(false);
            RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
            int errorCode = NIMChatRoomSDK.getChatRoomService().getEnterErrorCode(chatRoomStatusChangeData.roomId);
            // 如果遇到错误码13001，13002，13003，403，404，414，表示无法进入聊天室，此时应该调用离开聊天室接口。
            if (errorCode == ResponseCode.RES_CHATROOM_STATUS_EXCEPTION) {
                // 聊天室连接状态异常
                L.error(TAG, "聊天室状态异常！");
            }
            L.error(TAG, "聊天室在线状态变为UNLOGIN！");
            if (AvRoomDataManager.get().isOnMic(currentUid)) {
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByAccount(String.valueOf(currentUid));
                if (roomQueueInfo == null) return;
                mCacheRoomQueueInfo = new RoomQueueInfo(roomQueueInfo.mRoomMicInfo, roomQueueInfo.mChatRoomMember);
            }
        } else if (chatRoomStatusChangeData.status == StatusCode.LOGINING) {
            L.info(TAG, "登录中...");
        } else if (chatRoomStatusChangeData.status == StatusCode.LOGINED) {
            L.info(TAG, "云信聊天室已登录成功");
            setConnected(true);
            CoreUtils.send(new NewRoomEvent.OnEnterRoomSuccess());
            noticeImNetReLogin(mCacheRoomQueueInfo);
        } else if (chatRoomStatusChangeData.status.wontAutoLogin()) {
            L.info(TAG, "需要重新登录（被踢或验证信息错误）...");
            //  需要重新登录（被踢或验证信息错误）
            setConnected(false);
            RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
        } else if (chatRoomStatusChangeData.status == StatusCode.NET_BROKEN) {
            L.info(TAG, "网络断开...");
            setConnected(false);
            RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
            if (AvRoomDataManager.get().isOnMic(currentUid)) {
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByAccount(String.valueOf(currentUid));
                if (roomQueueInfo == null) return;
                mCacheRoomQueueInfo = new RoomQueueInfo(roomQueueInfo.mRoomMicInfo, roomQueueInfo.mChatRoomMember);
            }
        }
        CoreUtils.send(new NewRoomEvent.OnNetworkChange());
    }

    public boolean isConnected() {
        return isConnected;
    }

    private void setConnected(boolean connected) {
        isConnected = connected;
//        isConnected = true;
    }

    private void dealChatMessage(List<ChatRoomMessage> chatRoomMessages) {
        ArrayList<ChatRoomMessage> faceMessages = new ArrayList<>();
        ArrayList<ChatRoomMessage> giftMessages = new ArrayList<>();
        boolean face = false;
        boolean gift = false;
        String currentRoomId = "";
        if (AvRoomDataManager.get().mCurrentRoomInfo != null)
            currentRoomId = AvRoomDataManager.get().mCurrentRoomInfo.getRoomId() + "";

        for (ChatRoomMessage msg : chatRoomMessages) {
            //——————拦截其他房间的信息——————————
            //NIMClient.getService(ChatRoomService.class).enterChatRoom 调用前携带的
            String room_id = msg.getSessionId();
            L.info(TAG, " msg, type:" + msg.getMsgType() + "   " + System.currentTimeMillis() + "  room_id: " + room_id + "  currentRoomId: " + currentRoomId);
            if (TextUtils.isEmpty(room_id)) {
                L.info(TAG, "intercept this msg if roomId is null");
                continue;
            }
            //拦截其他房间的信息
            if (AvRoomDataManager.get().mCurrentRoomInfo == null || !String.valueOf(AvRoomDataManager.get().mCurrentRoomInfo.getRoomId()).equals(room_id)) {
                long roomId = Long.parseLong(room_id);
                //线下的广场聊天室不拦截
                if (BasicConfig.isDebug && roomId != PublicChatRoomManager.devRoomId && roomId != PublicChatRoomManager.checkDevRoomId
                        && roomId != FindBroadCastChatRoomController.devRoomId) {
                    L.info(TAG, "intercept this msg for other room, sessionId: %s", room_id);
                    continue;
                }
                //线上的广场聊天室不拦截
                if (!BasicConfig.isDebug && roomId != PublicChatRoomManager.formalRoomId && roomId != PublicChatRoomManager.checkRoomId
                        && roomId != FindBroadCastChatRoomController.formalRoomId) {
                    L.info(TAG, "intercept this msg for other room, sessionId: %s", room_id);
                    continue;
                }
            }
            //——————拦截其他房间的信息——————————


            if (msg.getMsgType() == MsgTypeEnum.notification) {
                if (!currentRoomId.equals(room_id)) {
                    //公聊机器人进入通知消息room_id为空无法拦截问题
                    L.info(TAG, "intercept this msg if roomId is not equals sessionId");
                    continue;
                }
                NotificationAttachment attachment = (NotificationAttachment) msg.getAttachment();

                try {
                    String publicChatRoom = (String) msg.getChatRoomMessageExtension().getSenderExtension().get(PUBLIC_CHAT_ROOM);
                    if (PUBLIC_CHAT_ROOM.equals(publicChatRoom)) {
                        L.info(TAG, "intercept this msg if PUBLIC_CHAT_ROOM equals publicChatRoom");
                        continue;
                    }
                } catch (Exception e) {
                    L.error(TAG, "format publicChatRoom exception:", e);
                }

                if (attachment == null) {
                    L.error(TAG, "attachment is null");
                    continue;
                }
                L.info(TAG, "notification message type: " + attachment.getType() + "   " + msg.getFromAccount() + "    " + System.currentTimeMillis());
                if (attachment.getType() == NotificationType.ChatRoomQueueChange) {//房间坑位变化通知
                    chatRoomQueueChangeNotice(msg);
                } else if (attachment.getType() == NotificationType.ChatRoomInfoUpdated) {
                    chatRoomInfoUpdate(msg);
                } else if (attachment.getType() == NotificationType.ChatRoomMemberIn) {
                    //用户进入房间
                    //添加用户等级
                    //进场监听
                    L.info(TAG, "player into room");
                    try {
                        CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onUserCome, msg);
                    } catch (Exception e) {
                        L.error(TAG, "notifyClients exception:", e);
                        e.printStackTrace();
                    }

                    NotificationMessage message = new NotificationMessage();
                    message.setChatRoomMessage(msg);
                    sendMessage(message);

                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    chatRoomMemberIn(targets.get(0));
                } else if (attachment.getType() == NotificationType.ChatRoomMemberExit || attachment.getType() == NotificationType.ChatRoomMemberKicked) {
                    String fromAccount = msg.getFromAccount();

                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    String account = targets.get(0);
                    chatRoomMemberExit(account, fromAccount);
                    CoreUtils.send(new NewRoomEvent.OnExitRoom(account));
                } else if (attachment.getType() == NotificationType.ChatRoomManagerAdd) {
                    //管理员id集合列表
                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    addManagerMember(targets.get(0));
                } else if (attachment.getType() == NotificationType.ChatRoomManagerRemove) {
                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    removeManagerMember(targets.get(0));
                } else if (attachment.getType() == NotificationType.ChatRoomMemberBlackAdd) {
                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    noticeChatMemberBlackAdd(targets.get(0));
                } else if (attachment.getType() == NotificationType.ChatRoomQueueBatchChange) {

                    //-----排麦时杀掉进程，首个在麦上的人帮他移除------
                    String fromAccount = msg.getFromAccount();
                    SparseArray<Json> mMicInListMap = AvRoomDataManager.get().mMicInListMap;
                    if (fromAccount != null && mMicInListMap != null) {

                        final Json json = mMicInListMap.get(Integer.parseInt(fromAccount));
                        if (json != null) {
                            AvRoomDataManager.get().removeMicListInfo(fromAccount);
                            noticeMicInList();
                            L.info("ChatRoomQueueBatchChange", 2 + "");
                        } else {
                            L.info("ChatRoomQueueBatchChange", 1 + "");
                        }
                    }
                    //-----排麦时杀掉进程，首个在麦上的人帮他移除------

                }
            } else if (msg.getMsgType() == MsgTypeEnum.custom) {
                MsgAttachment attachment = msg.getAttachment();
                if (attachment == null) {
                    L.error(TAG, "attachment is null");
                    continue;
                }
                //自定义消息
                CustomAttachment customAttachment = (CustomAttachment) msg.getAttachment();

                if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_AUCTION
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT
                        ) {
                    sendGiftMessage(msg);
                } else if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP) {
                    sendCommonMessage(msg);
                }

                long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                L.info(TAG, " process, first: %d, second: %d, uuid: %s, data = %s", customAttachment.getFirst(), customAttachment.getSecond(), msg.getUuid(),
                        customAttachment.getData() == null ? "null" : customAttachment.toJson(false));
                switch (customAttachment.getFirst()) {
                    case CUSTOM_MSG_HEADER_TYPE_QUEUE:
                        RoomQueueMsgAttachment queueMsgAttachment = (RoomQueueMsgAttachment) attachment;
                        if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE) {
                            //邀請上麥
                            if (Objects.equals(queueMsgAttachment.uid, String.valueOf(uid))) {
                                noticeInviteUpMic(queueMsgAttachment.micPosition, queueMsgAttachment.uid);
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK) {
                            //踢他下麥
                            if (Objects.equals(queueMsgAttachment.uid, String.valueOf(uid))) {
                                int micPosition = AvRoomDataManager.get().getMicPosition(uid);
                                noticeKickDownMic(micPosition);
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
                            sendCommonMessage(msg);
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
                            sendCommonMessage(msg);
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN) {
                            sendCommonMessage(msg);
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE) {
                            sendCommonMessage(msg);
                        }
                        CoreUtils.send(new RoomTalkEvent.OnRoomMsg(msg));
                        break;
                    case CUSTOM_MSG_HEADER_TYPE_AUCTION:
                        AuctionAttachment auctionAttachment = (AuctionAttachment) attachment;
                        if (auctionAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_AUCTION_START) {
                            AuctionModel.get().setAuctionInfo(auctionAttachment.getAuctionInfo());
                            noticeAuctionStart(auctionAttachment.getAuctionInfo());
                        } else if (auctionAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_AUCTION_FINISH) {
                            AuctionModel.get().setAuctionInfo(null);
                            noticeAuctionFinish(auctionAttachment.getAuctionInfo());
                        } else if (CustomAttachment.CUSTOM_MSG_SUB_TYPE_AUCTION_UPDATE == auctionAttachment.getSecond()) {
                            AuctionModel.get().setAuctionInfo(auctionAttachment.getAuctionInfo());
                            noticeAuctionUpdate(auctionAttachment.getAuctionInfo());
                        }
                        break;
                    case CUSTOM_MSG_HEADER_TYPE_FACE:
                        faceMessages.add(msg);
                        face = true;
                        break;

                    case CUSTOM_MSG_HEADER_TYPE_GIFT://单个礼物消息
                    case CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT://多个和全麦消息
                        gift = true;
                        giftMessages.add(msg);
                        CoreUtils.send(new RoomTalkEvent.OnRoomMsg(msg));
                        break;
                    case CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT://全服消息类型
                        if (currentRoomId.equals(room_id)) {//公聊进入通知消息room_id为空无法拦截问题
                            gift = true;
                            giftMessages.add(msg);
                        }
                        break;
                    case CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM:
                        addMessages(msg);
                        break;
                    case CUSTOM_MSG_LOTTERY_BOX:
                        RoomInfo roomInfo1 = AvRoomDataManager.get().mCurrentRoomInfo;
                        if (roomInfo1 == null) {
                            return;
                        }
                        LotteryBoxAttachment lotteryBoxAttachment = (LotteryBoxAttachment) msg.getAttachment();
                        if (lotteryBoxAttachment != null) {
                            L.debug(TAG, "LotteryBoxAttachment = %s", lotteryBoxAttachment.getParams());
                        }
                        if (roomInfo1.getDrawMsgOption() == 0) {
                            if (lotteryBoxAttachment != null) {
                                String params = lotteryBoxAttachment.getParams();
                                Json json = Json.parse(params);
                                long myUid = json.num_l("uid");
                                if (myUid != uid) {
                                    //在不展示砸蛋消息的房间内砸蛋时，此时触发中奖消息，则仅自己可见
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                        if (currentRoomId.equals(room_id)) {
                            //公聊进入通知消息room_id为空无法拦截问题
                            EggMessage message = new EggMessage();
                            message.setChatRoomMessage(msg);
                            sendMessage(message);
                        }
                        break;
                    case CUSTOM_MSG_MIC_IN_LIST:
                        MicInListAttachment micInListAttachment = (MicInListAttachment) msg.getAttachment();
                        String params = micInListAttachment.getParams();
                        String key = Json.parse(params).str("key");
                        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
                        if (!TextUtils.isEmpty(key) && mMicQueueMemberMap != null) {
                            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(Integer.parseInt(key));
                            if (roomQueueInfo != null)
                                if (!roomQueueInfo.mRoomMicInfo.isMicLock())
                                    micInListToUpMic(key);
                        }
                        break;
                    case CUSTOM_MSG_HEADER_TYPE_PK_FIRST:
                        CoreUtils.send(new RoomTalkEvent.OnRoomPkMsg((PkCustomAttachment) msg.getAttachment()));
                        if (customAttachment.getSecond() != CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_SECOND_ADD) {
                            sendCommonMessage(msg);
                        }
                        break;
                    case CUSTOM_MSG_TYPE_BURST_GIFT://全服通知
                        if (currentRoomId.equals(room_id)) {//公聊进入通知消息room_id为空无法拦截问题
                            sendGiftMessage(msg);
                            CoreUtils.send(new RoomTalkEvent.OnRoomMsg(msg));
                        }
                        break;
                    case CUSTOM_MSG_TYPE_NEW_PUSH_FIRST:
                        RoomInfo roomInfo2 = AvRoomDataManager.get().mCurrentRoomInfo;
                        if (roomInfo2 == null) {
                            return;
                        }
                        NewPushAttachment newPushAttachment = (NewPushAttachment) msg.getAttachment();
                        if (newPushAttachment != null) {
                            L.debug(TAG, "NewPushAttachment = %s", newPushAttachment.getParams());
                        }
                        if (roomInfo2.getDrawMsgOption() == 0) {
                            if (newPushAttachment != null) {
                                String params1 = newPushAttachment.getParams();
                                Json json = Json.parse(params1);
                                long myUid = json.num_l("uid");
                                if (myUid != uid) {
                                    //在不展示砸蛋消息的房间内砸蛋时，此时触发中奖消息，则仅自己可见
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                        if (currentRoomId.equals(room_id)) {
                            //通知给其它房间，但自己的房间不显示
                            EggMessage message = new EggMessage();
                            message.setChatRoomMessage(msg);
                            sendMessage(message);
                        }
                        break;
                    case CUSTOM_MSG_AUTH_STATUS_NOTIFY:
                        //更新个人信息
                        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                        break;
                    case CUSTOM_MSG_ROOM_CHARM:
                        RoomCharmListAttachment roomCharmListAttachment = (RoomCharmListAttachment) msg.getAttachment();
                        if (roomCharmListAttachment.getSecond() == CUSTOM_MSG_ROOM_SECOND_CHARM_UPDATE) {
                            String attachmentParams = roomCharmListAttachment.getParams();
                            try {
                                dealCharmUpdate(new JSONObject(attachmentParams));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (roomCharmListAttachment.getSecond() == CUSTOM_MSG_ROOM_SECOND_CHARM_CLEAR) {
                            dealCharmUpdate(null);
                        }
                        break;
//                    case CUSTOM_MSG_ONE_AUDIO://接收服务器推送的一元爆音消息
//                        OneAudioAttachment oneAudioAttachment = (OneAudioAttachment) msg.getAttachment();
//                        if (oneAudioAttachment.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
//                            sendTextMsg(oneAudioAttachment.getContent());
//                        }
//                        break;
                    case CUSTOM_MSG_BLESSING_BEAST:
                        if (currentRoomId.equals(room_id)) {
                            CommonMessage message = new CommonMessage();
                            BlessingBeastAttachment blessingBeastAttachment = (BlessingBeastAttachment) msg.getAttachment();
                            if (blessingBeastAttachment.getSecond() == CUSTOM_MSG_BLESSING_BEAST) {//触发福兽
                                RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                                message.setChatRoomMessage(msg);
                                sendMessage(message);
                                if (roomInfo != null) {
                                    if (roomInfo.getUid() == blessingBeastAttachment.getRoomUid()) {//只有当前房间才会触发福猪
                                        CoreManager.notifyClients(IRoomCoreClient.class, IRoomCoreClient.METHOD_ON_UPDATE_ROOM_BLESSING_BEAST, blessingBeastAttachment);
                                    }
                                }
                            } else if (blessingBeastAttachment.getSecond() == CUSTOM_MSG_BLESSING_BEAST_SECONDS) {//奖励消息
                                message.setChatRoomMessage(msg);
                                sendMessage(message);
                                showBlessingBeastToast(blessingBeastAttachment);
                            }
                        }
                        break;
                    case CUSTOM_MSG_ROOM_CP_REQUEST://41 房间内cp邀请消息
                        if (currentRoomId.equals(room_id)) {//房间收到CP消息，那么也需要处理
                            CpGiftAttachment cpGiftAttachment = (CpGiftAttachment) msg.getAttachment();
                            CpGiftReceiveInfo giftReceiveInfo = cpGiftAttachment.getGiftReceiveInfo();
                            switch (giftReceiveInfo.getStatus()) {
                                case CpGiftAttachment.CUSTOM_MSG_CP_INVITE:
                                    //邀请
                                    CoreManager.notifyClients(IRoomCpCoreClient.class, IRoomCpCoreClient.METHOD_ON_CP_INVITE, giftReceiveInfo);
                                    break;
                                case CpGiftAttachment.CUSTOM_MSG_CP_INVITE_REFUSE:
                                    //已拒绝
                                    CoreManager.notifyClients(IRoomCpCoreClient.class, IRoomCpCoreClient.METHOD_ON_CP_INVITE_REFUSE, giftReceiveInfo);
                                    break;
                            }
                        }
                        break;
                    case CUSTOM_MSG_ROOM_CP_TOP://45 房间顶部公屏消息 展示全服CP结成
                        SquareCpMsgAttachment squareCpMsgAttachment1 = (SquareCpMsgAttachment) msg.getAttachment();
                        if (squareCpMsgAttachment1 != null && AvRoomDataManager.get().mCurrentRoomInfo != null) {
                            gift = true;
                            giftMessages.add(msg);
                        }
                        break;
                    case CUSTOM_MSG_ROOM_CP_BOTTOM://46 房间底部公屏消息 展示全服CP结成
                        if (currentRoomId.equals(room_id)) {
                            SquareCpMsgAttachment squareCpMsgAttachment = (SquareCpMsgAttachment) msg.getAttachment();
                            L.debug(TAG, "receive 46 message: squareCpMsgAttachment = %s, mCurrentRoomInfo: %d", squareCpMsgAttachment == null ? "null" :
                                    squareCpMsgAttachment.toJson(false), AvRoomDataManager.get().mCurrentRoomInfo == null ? -1 : AvRoomDataManager.get().mCurrentRoomInfo.getUid());
                            if (squareCpMsgAttachment != null && AvRoomDataManager.get().mCurrentRoomInfo != null) {
                                //所有房间都显示底部公屏消息
                                sendCommonMessage(msg);
                            }
                        }
                        break;
                    case CUSTOM_MSG_SQUARE_BROADCAST_MSG://44 结成CP广场消息
                        SquareCpMsgAttachment squareCpMsgAttachment2 = (SquareCpMsgAttachment) msg.getAttachment();
                        SquareQueueInfo squareQueueInfo = convertInfo(squareCpMsgAttachment2);
                        CoreManager.notifyClients(SquareClient.class, SquareClient.CP_BROADCAST_MSG_NOTIFY, squareQueueInfo);
                        break;
                    case CUSTOM_MSG_ROOM_CP_STATE_REFRESH://51 房间结成和解除CP消息 用于在房间内及时更新cp状态
                        CpGiftAttachment cpGiftAttachment = (CpGiftAttachment) msg.getAttachment();
                        if (cpGiftAttachment != null) {
                            CpGiftReceiveInfo giftReceiveInfo = cpGiftAttachment.getGiftReceiveInfo();
                            if (giftReceiveInfo != null) {
                                L.debug(TAG, "收到【51房间CP消息】getStatus() ：" + giftReceiveInfo.getStatus());

                                switch (giftReceiveInfo.getStatus()) {
                                    case CpGiftAttachment.CUSTOM_MSG_CP_INVITE_AGREE:
                                        //已同意
                                        CoreManager.notifyClients(IRoomCpCoreClient.class, IRoomCpCoreClient.METHOD_ON_CP_INVITE_AGREE, giftReceiveInfo);
                                        break;
                                    case CpGiftAttachment.CUSTOM_MSG_CP_REMOVE:
                                        //已解散CP
                                        CoreManager.notifyClients(IRoomCpCoreClient.class, IRoomCpCoreClient.METHOD_ON_CP_REMOVE, giftReceiveInfo);
                                        CoreUtils.send(new WeekCpEvent.OnCpUnband());
                                        break;
                                }
                            }
                        }
                        break;
                    case CUSTOM_MSG_TURN_TABLE_WITH_BUBBLE:
                        L.info("LogUtil", "IMNetEaseManager 魔力转圈圈 50全服气泡消息: = " + attachment.toJson(false));
                        if (customAttachment.getSecond() == CUSTOM_MSG_TURN_TABLE_WITH_BUBBLE) {
                            sendCommonMessage(msg);
                        }
                        break;
                    case CUSTOM_MSG_TURN_TABLE_NO_BUBBLE:
                        L.info("LogUtil", "IMNetEaseManager 魔力转圈圈 49全服无气泡消息: = " + attachment.toJson(false));
                        if (customAttachment.getSecond() == CUSTOM_MSG_TURN_TABLE_NO_BUBBLE) {
                            sendCommonMessage(msg);
                        }
                        break;
                    case CUSTOM_MSG_TURN_TABLE_DETONATING://魔力转圈圈开始暴走
                        if (customAttachment.getSecond() == CUSTOM_MSG_TURN_TABLE_DETONATING) {
                            sendCommonMessage(msg);
                        }
                        break;
                    case CUSTOM_MSG_TURN_TABLE_PIECES_EXCHANGE://夺宝大转盘碎片兑换消息
                        if (customAttachment.getSecond() == CUSTOM_MSG_TURN_TABLE_PIECES_EXCHANGE) {
                            sendCommonMessage(msg);
                        }
                        break;
                    case CustomAttachment.CUSTOM_MSG_ROOM_LOVER_WEEK_CP_MSG://一周CP
                        L.info(TAG, "week cp: %s", customAttachment.toString());
                        // second 1是普通消息，2是需要跳到专属页， 3是默契挑战请求消息 4是走心回答 5是亲密值领取完了更新UI
                        if (customAttachment instanceof WeekCpAttachment) {
                            WeekCpAttachment weekCpAttachment = (WeekCpAttachment) customAttachment;
                            WeekCpMessage message = new WeekCpMessage();
                            message.setWeekCpAttachment(weekCpAttachment);
                            if (weekCpAttachment.getSecond() == WeekCpAttachment.ROOM_WEEK_CP_ALL_MSG) {
                                sendMessage(message);
                            } else if (isSelfOrCp(weekCpAttachment)) {
                                CoreUtils.send(new WeekCpEvent.OnFromCpChange(weekCpAttachment));
                                sendMessage(message);
                            } else if (weekCpAttachment.getSecond() == WeekCpAttachment.ROOM_WEEK_CP_MERRY) {
                                CoreUtils.send(new WeekCpEvent.OnWeekCpMerry());
                            } else if (weekCpAttachment.getSecond() == WeekCpAttachment.ROOM_WEEK_CP_SINCERITY_ASK) {
                                sendMessage(message);
                            }
                        }
                        break;
                    case CustomAttachment.CUSTOM_MSG_ROOM_LOVER_WEEK_CP_OFFICE://一周CP
                        // second为4自动解绑，3是一周cp到期提示  1是结成一周cp
                        WeekCpAttachment weekCpAttachment = (WeekCpAttachment) customAttachment;
                        if (isSelfOrCp(weekCpAttachment)) {
                            CoreUtils.send(new WeekCpEvent.OnWeekCpOffice(weekCpAttachment.getSecond(), weekCpAttachment.getCpId()));
                        }
                        break;
                    case CustomAttachment.CUSTOM_MSG_ROOM_PERSONAL_ATTENTION_MSG:
                        PersonalAttentionAttachment attentionAttachment = (PersonalAttentionAttachment) customAttachment;
                        if (AvRoomDataManager.get().isSelf(attentionAttachment.getUid())) {
                            AttentionMessage message = new AttentionMessage();
                            message.setAttentionAttachment(attentionAttachment);
                            sendMessage(message);
                        }
                        break;
                    case CustomAttachment.CUSTOM_MSG_ROOM_PERSONAL_ATTENTION_SUCCESS_MSG:
                        sendCommonMessage(msg);
                        break;
                    case CustomAttachment.CUSTOM_MSG_ROOM_FIND_FRIENDS_BROADCAST_MSG://寻友广播
                        CoreUtils.send(new BroadcastMsgEvent.OnReceiveNewMsg(msg));
                        break;
                    default:
                }
            } else if (msg.getMsgType() == MsgTypeEnum.text) {
                L.info(TAG, "message is text");
//                addMessages(msg);
                TalkMessage talkMessage = new TalkMessage();
                talkMessage.setChatRoomMessage(msg);
                sendMessage(talkMessage);
            }
        }

        L.info(TAG, "dealChatMessage face: %b, gift: %b", face, gift);
        if (face) {
            CoreManager.getCore(IFaceCore.class).onReceiveChatRoomMessages(faceMessages);
        }
        if (gift) {
            CoreManager.getCore(IGiftCore.class).onReceiveChatRoomMessages(giftMessages);
        }
    }

    private void sendGiftMessage(ChatRoomMessage msg) {
        RoomInfo info = AvRoomDataManager.get().mCurrentRoomInfo;
        if (info != null) {
            int giftEffectSwitch = info.getGiftEffectSwitch();
            if (giftEffectSwitch == 0) {
                GiftMessage message = new GiftMessage();
                message.setChatRoomMessage(msg);
                sendMessage(message);
            }
        } else {
            GiftMessage message = new GiftMessage();
            message.setChatRoomMessage(msg);
            sendMessage(message);
        }
    }

    private void sendCommonMessage(ChatRoomMessage msg) {
        MsgAttachment attachment = msg.getAttachment();
        if (attachment instanceof CustomAttachment) {
            if (((CustomAttachment) attachment).getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_SECOND_ADD) {
                return;
            }
        }

        CommonMessage message = new CommonMessage();
        message.setChatRoomMessage(msg);
        sendMessage(message);
    }

    private void sendMessage(IRoomTalkMessage message) {
        CoreUtils.send(new RoomTalkEvent.OnTalkMessageAccept<>(message));
    }

    private boolean isSelfOrCp(WeekCpAttachment weekCpAttachment) {
        String msgUid = weekCpAttachment.getUid();
        String cpId = weekCpAttachment.getCpId();
        String userId = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        return userId.equals(msgUid) || userId.equals(cpId);
    }

    private SquareQueueInfo convertInfo(SquareCpMsgAttachment squareCpMsgAttachment) {
        SquareQueueInfo squareQueueInfo = new SquareQueueInfo();
        squareQueueInfo.setCloseLevel(squareCpMsgAttachment.getCloseLevel());
        squareQueueInfo.setCountTime(squareCpMsgAttachment.getCountTime());
        squareQueueInfo.setDesc(squareCpMsgAttachment.getDesc());
        squareQueueInfo.setCpGiftLevel(squareCpMsgAttachment.getCpGiftLevel());
        squareQueueInfo.setGiftId(squareCpMsgAttachment.getGiftId());
        squareQueueInfo.setInviteUser(squareCpMsgAttachment.getInviteUser());
        squareQueueInfo.setPicUrl(squareCpMsgAttachment.getPicUrl());
        squareQueueInfo.setRecUser(squareCpMsgAttachment.getRecUser());
        squareQueueInfo.setRoomUid(squareCpMsgAttachment.getRoomUid());
        squareQueueInfo.setTitle(squareCpMsgAttachment.getTitle());
        return squareQueueInfo;
    }

    private void showBlessingBeastToast(BlessingBeastAttachment blessingBeastAttachment) {
        if (blessingBeastAttachment == null) {
            return;
        }
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null && roomInfo.getUid() == blessingBeastAttachment.getRoomUid() &&
                blessingBeastAttachment.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
            if (!TextUtils.isEmpty(blessingBeastAttachment.getContent())) {
                SingleToastUtil.showToast(blessingBeastAttachment.getContent());
                CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();//刷新背包礼物
            }
        }
    }

    /**
     * 成员进入房间
     */
    private void chatRoomMemberIn(final String account) {
        List<String> list = new ArrayList<>(1);
        list.add(account);
        fetchRoomMembersByIds(list)
                .subscribe(new Consumer<List<ChatRoomMember>>() {
                    @Override
                    public void accept(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                        if (ListUtils.isListEmpty(chatRoomMemberList)) return;
                        ChatRoomMember chatRoomMember = chatRoomMemberList.get(0);
                        AvRoomDataManager.get().mRoomAllMemberList.add(chatRoomMember);
                        if (chatRoomMember.getMemberType() == MemberType.ADMIN) {
                            addManagerMember(chatRoomMember);
                        }
                        noticeRoomMemberChange(true, account);
                    }
                });
    }


    /**
     * 退出房间处理
     */
    private void chatRoomMemberExit(String account, final String fromAccount) {
        L.info(TAG, "chatRoomMemberExit account: %s", account);
        noticeRoomMemberChange(false, account);
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo == null) {
            return;
        }
        if (AvRoomDataManager.get().isOnMic(Long.valueOf(account))) {
            //在麦上的要退出麦
            IMNetEaseManager.get().downMicroPhoneBySdk(AvRoomDataManager.get().getMicPosition(Long.valueOf(account)), null);
            SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
            int size = mMicQueueMemberMap.size();
            L.info(TAG, "chatRoomMemberExit size: %s", size);
            for (int i = 0; i < size; i++) {
                RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo.mChatRoomMember != null
                        && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), account)) {
                    isShowCpIcon(roomQueueInfo, false);
                    roomQueueInfo.mChatRoomMember = null;

                    try {
                        //同时停止接收此用户的推流
                        RtcEngineManager.get().muteRemoteAudioStream(Integer.valueOf(account), true);
                        //记录一下下麦的时间
                        AvRoomDataManager.get().setTimeOnMicDown(Integer.valueOf(account), System.currentTimeMillis());
                    } catch (Exception e) {
                        e.printStackTrace();
                        L.error(TAG, "chatRoomMemberExit Exception:", e);
                    }


                    L.info("remove  mChatRoomMember", 3 + "");
                    noticeDownMic(String.valueOf(mMicQueueMemberMap.keyAt(i)), account);
                    break;
                } else {
                    L.error(TAG, "chatRoomMemberExit roomQueueInfo.mChatRoomMember: %s", roomQueueInfo.mChatRoomMember);
                }
            }
        }


        ChatRoomMember removeChatMember = null;
        boolean isAdmin = false;
        ListIterator<ChatRoomMember> iterator = AvRoomDataManager.get().mRoomAllMemberList.listIterator();
        for (; iterator.hasNext(); ) {
            removeChatMember = iterator.next();
            if (Objects.equals(removeChatMember.getAccount(), account)) {
                if (removeChatMember.getMemberType() == MemberType.ADMIN) {
                    isAdmin = true;
                }
                iterator.remove();
            }
        }
        if (isAdmin) {
            removeManagerMember(removeChatMember);
        }
    }

    /**
     * 麦序更新
     *
     * @param extension -
     * @param gson      -
     */
    private void roomQueueMicUpdate(Map<String, Object> extension, Gson gson) {
        String micInfo = (String) extension.get("micInfo");
        L.info(TAG, "roomQueueMicUpdate micInfo: %s", micInfo);
        if (!TextUtils.isEmpty(micInfo)) {
            RoomMicInfo roomMicInfo = gson.fromJson(micInfo, RoomMicInfo.class);
            if (roomMicInfo != null) {
                int position = roomMicInfo.getPosition();
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(position);
                if (roomQueueInfo != null) {
                    //解锁麦位
                    if (roomQueueInfo.mRoomMicInfo.isMicLock() && !roomMicInfo.isMicLock()) {
                        L.info("roomQueueMicUpdate", "unLockMic");
                        micInListToUpMic(position + "");
                    }

                    roomQueueInfo.mRoomMicInfo = roomMicInfo;
                    //处理声网声音相关的
                    if (roomQueueInfo.mChatRoomMember != null) {
                        String account = roomQueueInfo.mChatRoomMember.getAccount();
                        if (AvRoomDataManager.get().isSelf(account)) {
                            boolean micMute = roomQueueInfo.mRoomMicInfo.isMicMute();
                            RtcEngineManager.get().setRole(
                                    micMute ? Constants.CLIENT_ROLE_AUDIENCE : Constants.CLIENT_ROLE_BROADCASTER);
                            L.info(TAG, "roomQueueMicUpdate account: %s, isMicMute: %b", account, micMute);
                        }
                    }
                    noticeMicPosStateChange(position, roomQueueInfo);
                }
            }
        }
    }


    /**
     * 麦序状态，房间信息
     *
     * @param msg -
     */
    private void chatRoomInfoUpdate(ChatRoomMessage msg) {
        // : 2018/5/7  封麦解麦
        ChatRoomNotificationAttachment notificationAttachment = (ChatRoomNotificationAttachment) msg.getAttachment();
        Map<String, Object> extension = notificationAttachment.getExtension();
        L.info(TAG, "chatRoomInfoUpdate extension: %s", extension);
        if (extension != null) {
            Gson gson = new Gson();
            // 1----房间信息更新   2-----麦序信息更新
            int type = (int) extension.get("type");
            L.info(TAG, "chatRoomInfoUpdate type: %d", type);
            if (type == 2) {
                roomQueueMicUpdate(extension, gson);
            } else if (type == 1) {
                roomInfoUpdate(extension, gson);
            }
        }
    }


    /**
     * 房间信息更新
     *
     * @param extension --
     * @param gson      --
     */
    private void roomInfoUpdate(Map<String, Object> extension, Gson gson) {
        String roomInfoStr = (String) extension.get("roomInfo");
        if (!TextUtils.isEmpty(roomInfoStr)) {
            RoomInfo roomInfo = gson.fromJson(roomInfoStr, RoomInfo.class);
            if (roomInfo != null) {
                AvRoomDataManager.get().mCurrentRoomInfo = roomInfo;
//                L.info("RoomSettingActivity2", AvRoomDataManager.get().mCurrentRoomInfo + "");
                noticeRoomInfoUpdate(roomInfo);
            }
        }
    }

    private void chatRoomQueueChangeNotice(ChatRoomMessage msg) {
        ChatRoomQueueChangeAttachment roomQueueChangeAttachment = (ChatRoomQueueChangeAttachment) msg.getAttachment();
        //麦上成员信息（uid...）  key:坑位 ，content:{"nick":"行走的老者","uid":90972,"gender":1,"avatar":"https://img.erbanyy.com/Fmtbprx5cGc3KABKjDxs_udJZb3O?imageslim"}
        String content = roomQueueChangeAttachment.getContent();
        String key = roomQueueChangeAttachment.getKey();

        //排麦的key是用uid，所以length大于2
        boolean isMicInList = key != null && key.length() > 2;
        ChatRoomQueueChangeType changeType = roomQueueChangeAttachment.getChatRoomQueueChangeType();
        L.info(TAG, "chatRoomQueueChangeNotice key: %s, content: %s, changeType: %d, msg: %s", key, content, changeType == null ? -1 : changeType.ordinal(), msg.getContent());
        Gson gson = new Gson();
        MicPlayerInfo playerInfo = null;
        String account = "";
        try {
            playerInfo = gson.fromJson(content, MicPlayerInfo.class);
            account = playerInfo.getUid();
        } catch (Exception e) {
            L.error(TAG, "MicPlayerInfo from json exception: ", e);
        }

        switch (changeType) {
            case DROP:
                break;
            case POLL:
                // : 2018/4/26 2
                if (isMicInList) {
                    removeMicInList(key);
                } else {
                    downMicroQueue(key);
                }

                CoreUtils.send(new RoomAction.OnChairStateChange(false, account));
                break;
            case OFFER:

                if (isMicInList) {
                    addMicInList(key, content);
                } else {
                    upMicroQueue(content, key);
                }
                CoreUtils.send(new RoomAction.OnChairStateChange(true, account));
                break;
            case PARTCLEAR:
                break;
            case undefined:
                break;
            default:
        }
    }


    public void addMicInList(String key, String content) {
        if (TextUtils.isEmpty(key) || TextUtils.isEmpty(content))
            return;
        int keyInt = Integer.parseInt(key);
        JsonParser jsonParser = new JsonParser();
        JsonObject valueJsonObj = jsonParser.parse(content).getAsJsonObject();
        if (valueJsonObj == null)
            return;
        SparseArray<Json> mMicInListMap = AvRoomDataManager.get().mMicInListMap;
        Json json = mMicInListMap.get(keyInt);
        if (json == null) {
            json = new Json();
        }
        Set<String> strings = valueJsonObj.keySet();


        for (String jsonKey : strings) {
            json.set(jsonKey, valueJsonObj.get(jsonKey).getAsString());
        }


        AvRoomDataManager.get().addMicInListInfo(key, json);
        noticeMicInList();
    }

    private void removeMicInList(String key) {
        AvRoomDataManager.get().removeMicListInfo(key);
        noticeMicInList();
    }

    /**
     * 进入聊天室
     */
    public void joinAvRoom() {
        L.info(TAG, "joinAvRoom");
        RoomInfo curRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        noticeEnterMessages();
        handler.removeMessages(0);
        handler.sendEmptyMessageDelayed(0, 60000);

        if (curRoomInfo != null) {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            ResponseListener listener = new ResponseListener<ServiceResult<Object>>() {
                @Override
                public void onResponse(ServiceResult<Object> data) {
                }
            };
            ResponseErrorListener errorListener = new ResponseErrorListener() {
                @Override
                public void onErrorResponse(RequestError error) {
                    if (error != null)
                        L.error(TAG, "userroomin", error);
                }
            };
            model.userRoomIn(String.valueOf(uid), curRoomInfo.getUid(), listener, errorListener);
        }

    }

    private void addMessages(ChatRoomMessage msg) {
        if (msg == null) {
            return;
        }
        noticeReceiverMessage(msg);
    }

    private PublishProcessor<RoomEvent> roomProcessor;
    private PublishSubject<ChatRoomMessage> msgProcessor;

    @Deprecated
    /**
     * this method has been replaced by
     * {@code com.tongdaxing.xchat_core.manager.IMNetEaseManager.subscribeChatRoomMsgFlowable(Consumer<List<ChatRoomMessage>> chatMsg,IDisposableAddListener iDisposableAddListener)}
     */
    public Observable<List<ChatRoomMessage>> getChatRoomMsgFlowable() {
        return getChatRoomMsgPublisher().toFlowable(BackpressureStrategy.BUFFER)
                .toObservable().buffer(2000, TimeUnit.MILLISECONDS, 5)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void subscribeChatRoomMsgFlowable(Consumer<List<ChatRoomMessage>> chatMsg, IDisposableAddListener iDisposableAddListener) {
        Disposable disposable = getChatRoomMsgFlowable().subscribe(chatMsg);
        if (iDisposableAddListener != null)
            iDisposableAddListener.addDisposable(disposable);
    }

    private PublishSubject<ChatRoomMessage> getChatRoomMsgPublisher() {
        if (msgProcessor == null) {
            synchronized (IMNetEaseManager.class) {
                if (msgProcessor == null) {
                    msgProcessor = PublishSubject.create();
                    msgProcessor.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread());
                }
            }
        }
        return msgProcessor;
    }

    @Deprecated
    /**
     * this method has been replaced by
     * {@code com.tongdaxing.xchat_core.manager.IMNetEaseManager.subscribeChatRoomEventObservable(Consumer<RoomEvent> roomEvent,IDisposableAddListener iDisposableAddListener)}
     */
    public PublishProcessor<RoomEvent> getChatRoomEventObservable() {
        if (roomProcessor == null) {
            synchronized (IMNetEaseManager.class) {
                if (roomProcessor == null) {
                    roomProcessor = PublishProcessor.create();
                    roomProcessor.subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread());
                }
            }
        }
        return roomProcessor;
    }

    public void subscribeChatRoomEventObservable(Consumer<RoomEvent> roomEvent, IDisposableAddListener iDisposableAddListener) {
        Disposable disposable = getChatRoomEventObservable().subscribe(roomEvent);
        if (iDisposableAddListener != null)
            iDisposableAddListener.addDisposable(disposable);
    }

    public void clear() {
        roomCharmList = null;//清空房间魅力值
        Log.e(IMNetEaseManager.class.getSimpleName(), "清除房间消息....");
    }

    private MessageHandler handler = new MessageHandler();

    private static class MessageHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            IMNetEaseManager.get().sendStatistics();
            sendEmptyMessageDelayed(0, 60000);
        }
    }

    public void removeHandlerMsg() {
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

    /************************云信聊天室 普通操作(每个人都可以使用的) start******************************/

    /**
     * 发送公屏上的Tip信息
     * 子协议一: 关注房主提示- CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_SHARE_ROOM
     * 子协议二: 分享房间成功的提示- CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_ATTENTION_ROOM_OWNER
     *
     * @param targetUid -
     * @param subType   -发送公屏上Tip信息的子协议
     */
    public Single<ChatRoomMessage> sendTipMsg(long targetUid, int subType) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(targetUid);
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        UserInfo myUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(myUid);
        if (roomInfo != null && userInfo != null && myUserInfo != null) {
            RoomTipAttachment roomTipAttachment = new RoomTipAttachment(
                    CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP,
                    subType
            );
            roomTipAttachment.setUid(myUid);
            roomTipAttachment.setNick(myUserInfo.getNick());
            roomTipAttachment.setTargetUid(targetUid);
            roomTipAttachment.setTargetNick(userInfo.getNick());

            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    // 自定义消息
                    roomTipAttachment
            );

            return sendChatRoomMessage(message, false);
        }
        return Single.error(new Exception("roomInfo or userInfo or myUserInfo is null !"));
    }

    /**
     * 发送文本信息
     *
     * @param message -
     */
    public Single<ChatRoomMessage> sendTextMsg(long roomId, String message) {
        if (TextUtils.isEmpty(message) || TextUtils.isEmpty(message.trim()))
            return Single.error(new ErrorThrowable("message == null !!!"));


        ChatRoomMessage chatRoomMessage = ChatRoomMessageBuilder.createChatRoomTextMessage(
                String.valueOf(roomId), message);


        return sendChatRoomMessage(chatRoomMessage, false, false);
    }

    /**
     * 下麦
     *
     * @param key --
     */
    private void downMicroQueue(String key) {
        L.info("nim_sdk", "downMicroQueue_3");
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (!TextUtils.isEmpty(key) && mMicQueueMemberMap != null) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(Integer.parseInt(key));
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                if (AvRoomDataManager.get().isSelf(account)) {
                    // 更新声网闭麦 ,发送状态信息
                    RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                    AvRoomDataManager.get().mIsNeedOpenMic = true;
                }
                L.info("remove  mChatRoomMember", 2 + "");
                isShowCpIcon(roomQueueInfo, false);
                roomQueueInfo.mChatRoomMember = null;

                try {
                    //同时停止接收此用户的推流
                    RtcEngineManager.get().muteRemoteAudioStream(Integer.valueOf(account), true);
                    //记录一下下麦的时间
                    AvRoomDataManager.get().setTimeOnMicDown(Integer.valueOf(account), System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // 通知界面更新麦序信息
                noticeDownMic(key, account);

                //排麦
                if (!roomQueueInfo.mRoomMicInfo.isMicLock())
                    micInListToUpMic(key);
            }
        }
    }

    private void micInListToUpMic(final String key) {
        L.info("micInListToUpMic", "key:" + key);

        //房主不不能上
        if ("-1".equals(key))
            return;
        L.info("micInListToUpMic_!=-1", "key:" + key);
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null)
            return;
        //那之前更新一次队列
        // : 2018/4/27 更新
        final Json json = AvRoomDataManager.get().getMicInListTopInfo();
        if (json == null)
            return;

        final String micInListTopKey = json.str("uid");

        L.info("micInListToUpMic", micInListTopKey);

        checkMicInListUpMicSuccess(micInListTopKey, roomInfo.getRoomId(), key);

        if (!micInListTopKey.equals(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid() + "")) {
            return;
        }

        remveMicInlistOrUpMic(key, roomInfo, micInListTopKey);
    }

    private void remveMicInlistOrUpMic(final String key, RoomInfo roomInfo, final String micInListTopKey) {
        removeMicInList(micInListTopKey, roomInfo.getRoomId() + "", new RequestCallback() {
            @Override
            public void onSuccess(Object param) {
                //移除成功报上麦,判断是否自己,如果是自动上麦

                L.info("micInListToUpMic", 1 + "");
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onMicInListToUpMic, Integer.parseInt(key), micInListTopKey);
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.micInListDismiss, "");
            }

            @Override
            public void onFailed(int code) {
                L.info("micInListLogUpMic", 2 + "code:" + code);
            }

            @Override
            public void onException(Throwable exception) {
                L.info("micInListLogUpMic", 3 + "");
            }
        });
    }

    private void checkMicInListUpMicSuccess(final String micInListTopKey, final long roomId, final String key) {
        String account = getFirstMicUid();

        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        boolean isKicker = (currentUid + "").equals(account);
        //如果是首个在麦上的人
        if (isKicker) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Json json = AvRoomDataManager.get().getMicInListTopInfo();
                    if (json == null)
                        return;

                    String topKey = json.str("uid", "null");
                    //1.5秒后判断排麦，首位跟换没有
                    if (micInListTopKey.equals(topKey)) {

//                        remveMicInlistOrUpMic(key, roomInfo, micInListTopKey);
                        IMNetEaseManager.get().removeMicInList(micInListTopKey, roomId + "", new RequestCallback() {
                            @Override
                            public void onSuccess(Object param) {
                                //移除成功后通知别人上麦
                                sendMicInListNimMsg(key);
                                L.info("micInListToUpMicOnSuccess", key);


                            }

                            @Override
                            public void onFailed(int code) {
                                L.info("micInListToUpMiconFailed", key);
                            }

                            @Override
                            public void onException(Throwable exception) {
                                L.info("micInListToUpMiconException", key);
                            }
                        });
                        L.info("checkMicInListUpMicSuccess", "kick");

                    }
                }
            }, 1500);
        }


    }

    private String getFirstMicUid() {
        String account = "";
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
//
//            L.info("checkMicInListUpMicSuccess", mMicQueueMemberMap.keyAt(i) + "");
            if (roomQueueInfo == null)
                continue;
            ChatRoomMember mChatRoomMember = roomQueueInfo.mChatRoomMember;
            if (mChatRoomMember == null)
                continue;
            else {
                account = mChatRoomMember.getAccount();
                break;
            }
        }
        return account;
    }

    private void sendMicInListNimMsg(final String key) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        MicInListAttachment micInListAttachment = new MicInListAttachment(CUSTOM_MSG_MIC_IN_LIST, CUSTOM_MSG_MIC_IN_LIST);
        Json json = new Json();
        json.set("key", key);
        micInListAttachment.setParams(json + "");
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomInfo.getRoomId() + "",
                micInListAttachment
        );
        IMNetEaseManager.get().sendChatRoomMessage(message, false)
                .subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
                    @Override
                    public void accept(ChatRoomMessage chatRoomMessage,
                                       Throwable throwable) throws Exception {
                        if (chatRoomMessage != null) {
                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    micInListToUpMic(key);
                                }
                            }, 300);

                        }
                    }
                });


    }


    public void removeMicInList(String key, String roomId, RequestCallback requestCallback) {

        L.info("nim_sdk", "pollQueue_api_1");
        NIMClient.getService(ChatRoomService.class).pollQueue(roomId, key).setCallback(requestCallback);

    }


    /**
     * 上麦
     *
     * @param content --
     */
    private synchronized void upMicroQueue(String content, final String key) {
        L.info(TAG, "upMicroQueue() content = %s, key = %s", content, key);
        final SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (!TextUtils.isEmpty(content)) {
            JsonParser jsonParser = new JsonParser();
            JsonObject contentJsonObj = null;
            /**
             * 猜测是 RoomBaseModel updateQueueEx方法中通过空格符强转字符串的问题
             * https://blog.csdn.net/qq_24504453/article/details/72510241 参考这个解释
             * #1140 com.google.gson.stream.MalformedJsonException
             * Unterminated object at line 1 column 647 path $.erbanNo
             * com.google.gson.internal.f.a(Streams.java:60)
             */
            try {
                contentJsonObj = jsonParser.parse(content).getAsJsonObject();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (contentJsonObj != null) {
                int micPosition = Integer.parseInt(key);
                RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(micPosition);
                if (roomQueueInfo == null) {
                    return;
                }
                ChatRoomMember chatRoomMember = parseChatRoomMember(contentJsonObj, roomQueueInfo);

                int size = mMicQueueMemberMap.size();
                if (size > 0) {
                    for (int j = 0; j < size; j++) {//处理连续更换麦位，数据要置空
                        RoomQueueInfo temp = mMicQueueMemberMap.valueAt(j);
                        ChatRoomMember member = temp.mChatRoomMember;
                        if (member != null
                                && Objects.equals(member.getAccount(), chatRoomMember.getAccount())) {
                            //处理同一个人换坑问题
                            temp.mChatRoomMember = null;
                            temp.isShowCpIcon = false;
                        }
                        L.info(TAG, "upMicroQueue j: %d, member: %s, uid: %s", j, member, member != null ? member.getAccount() : "-1");
                    }
                }
                RoomQueueInfo tempRoomQueueInfo = mMicQueueMemberMap.get(micPosition);
                if (tempRoomQueueInfo != null && tempRoomQueueInfo.mChatRoomMember != null
                        && !Objects.equals(tempRoomQueueInfo.mChatRoomMember.getAccount(), chatRoomMember.getAccount())) {
                    //被挤下麦的情况
                    noticeDownCrowdedMic(micPosition, tempRoomQueueInfo.mChatRoomMember.getAccount());
                }

                roomQueueInfo.mChatRoomMember = chatRoomMember;
                isShowCpIcon(roomQueueInfo, true);
                //同时开始接收此用户的推流
                try {
                    Integer account = Integer.valueOf(chatRoomMember.getAccount());
                    L.info(TAG, "upMicroQueue() account = %d", account);
                    RtcEngineManager.get().muteRemoteAudioStream(account, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //重新更新队列，队列上是否还有自己
                if (!AvRoomDataManager.get().isSelfOnMic()) {
                    //处理可能自己被挤下还能说话的情况
                    RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                }

                if (AvRoomDataManager.get().isSelf(chatRoomMember.getAccount())) {
                    if (!roomQueueInfo.mRoomMicInfo.isMicMute()) {
                        //开麦
                        RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_BROADCASTER);
                        //是否需要开麦
                        if (!AvRoomDataManager.get().mIsNeedOpenMic) {
                            //闭麦
                            RtcEngineManager.get().setMute(true);
                            AvRoomDataManager.get().mIsNeedOpenMic = true;
                        }
                    } else {
                        RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                    }
                }
                micInListToDownMic(chatRoomMember.getAccount());
                noticeUpMic(Integer.parseInt(key), chatRoomMember.getAccount());
            }
        } else {
            RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
        }
    }

    private void micInListToDownMic(String key) {
        if (!AvRoomDataManager.get().checkInMicInlist())
            return;
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        removeMicInList(key, roomInfo.getRoomId() + "", null);
    }

    @NonNull
    private ChatRoomMember parseChatRoomMember(JsonObject contentJsonObj, RoomQueueInfo roomQueueInfo) {
        ChatRoomMember chatRoomMember = new ChatRoomMember();
        if (contentJsonObj.has("uid")) {
            int uid = contentJsonObj.get("uid").getAsInt();
            chatRoomMember.setAccount(String.valueOf(uid));
        }
        if (contentJsonObj.has("nick")) {
            chatRoomMember.setNick(contentJsonObj.get("nick").getAsString());
        }
        if (contentJsonObj.has("avatar")) {
            chatRoomMember.setAvatar(contentJsonObj.get("avatar").getAsString());
        }
        if (contentJsonObj.has("gender")) {
            roomQueueInfo.gender = contentJsonObj.get("gender").getAsInt();
        }
        L.debug(TAG, "parseChatRoomMember() contentJsonObj = %s", contentJsonObj);
        if (contentJsonObj.has(SpEvent.headwearUrl)) {
            Map<String, Object> stringStringMap = new HashMap<>();
            stringStringMap.put(SpEvent.headwearUrl, contentJsonObj.get(SpEvent.headwearUrl).getAsString());
            chatRoomMember.setExtension(stringStringMap);
        }
        if (contentJsonObj.has(SpEvent.cpUserId) && contentJsonObj.has(SpEvent.cpGiftLevel)) {
            roomQueueInfo.cpUserId = contentJsonObj.get(SpEvent.cpUserId).getAsLong();
            roomQueueInfo.cpGiftLevel = contentJsonObj.get(SpEvent.cpGiftLevel).getAsInt();
        } else {
            roomQueueInfo.cpUserId = 0;
            roomQueueInfo.cpGiftLevel = 0;
        }
        return chatRoomMember;
    }

    /**
     * 是否展示CP图标
     *
     * @param roomQueueInfo
     */
    private void isShowCpIcon(RoomQueueInfo roomQueueInfo, boolean isShow) {
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            ChatRoomMember c1 = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i)).mChatRoomMember;
            if (c1 != null) {
                //cp房 或者 非cp房 房主不在cp中可以显示cp标识
                if (AvRoomDataManager.get().mCurrentRoomInfo.getTagType() != RoomInfo.ROOMTYPE_LOVERS && AvRoomDataManager.get().isRoomOwner(c1.getAccount())) {
                    //房主不参与cp标识显示
                    continue;
                }
                if (c1.getAccount().equals(String.valueOf(roomQueueInfo.cpUserId))) {
                    mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i)).isShowCpIcon = isShow;
                    roomQueueInfo.isShowCpIcon = isShow;
                    return;
                }
            }
        }
    }


    public Single<List<ChatRoomMember>> fetchRoomMembersByIds(final List<String> accounts) {
        return Single.create(new SingleOnSubscribe<List<ChatRoomMember>>() {
            @Override
            public void subscribe(SingleEmitter<List<ChatRoomMember>> e) throws Exception {

                final RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (mCurrentRoomInfo == null || ListUtils.isListEmpty(accounts)) {
                    e.onError(new IllegalArgumentException("RoomInfo is null or accounts is null"));
                    CoreManager.notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.enterError);
                    return;
                }
                RequestResult<List<ChatRoomMember>> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .fetchRoomMembersByIds(String.valueOf(mCurrentRoomInfo.getRoomId()), accounts));
                if (result.exception != null)
                    e.onError(result.exception);
                else if (result.code != BaseMvpModel.RESULT_OK)
                    e.onError(new Exception("错误码: " + result.code));
                else
                    e.onSuccess(result.data);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /************************云信聊天室 普通操作 end******************************/

    /************************云信聊天室 房主/管理员操作 begin******************************/

    /**
     * 增加管理员
     *
     * @param chatRoomMember -
     */
    private void addManagerMember(final ChatRoomMember chatRoomMember) {
        AvRoomDataManager.get().addAdminMember(chatRoomMember);
    }

    /**
     * 增加管理员
     *
     * @param account -
     */
    private void addManagerMember(final String account) {
        List<String> accounts = new ArrayList<>(1);
        accounts.add(account);
        fetchRoomMembersByIds(accounts).subscribe(new BiConsumer<List<ChatRoomMember>, Throwable>() {
            @Override
            public void accept(List<ChatRoomMember> chatRoomMembers, Throwable throwable) throws Exception {
                if (!ListUtils.isListEmpty(chatRoomMembers)) {
                    ChatRoomMember chatRoomMember = chatRoomMembers.get(0);
                    addManagerMember(chatRoomMember);
                    if (AvRoomDataManager.get().isSelf(chatRoomMember.getAccount())) {
                        AvRoomDataManager.get().mOwnerMember = chatRoomMember;
                    }
                    // 放在这里的原因是,只有管理员身份改变了才能发送通知
                    noticeManagerChange(true, account);
                }
            }
        });
    }

    private void removeManagerMember(final String account) {
        if (TextUtils.isEmpty(account)) return;
        List<String> accounts = new ArrayList<>(1);
        accounts.add(account);
        fetchRoomMembersByIds(accounts).subscribe(new BiConsumer<List<ChatRoomMember>, Throwable>() {
            @Override
            public void accept(List<ChatRoomMember> chatRoomMemberList, Throwable throwable) throws Exception {
                if (!ListUtils.isListEmpty(chatRoomMemberList)) {
                    ChatRoomMember chatRoomMember = chatRoomMemberList.get(0);
                    if (AvRoomDataManager.get().isSelf(chatRoomMember.getAccount())) {
                        AvRoomDataManager.get().mOwnerMember = chatRoomMember;
                    }
                    removeManagerMember(chatRoomMember);
                    noticeManagerChange(false, account);
                }
            }
        });
    }

    private void removeManagerMember(ChatRoomMember chatRoomMember) {
        if (chatRoomMember == null) return;
        String account = chatRoomMember.getAccount();
        AvRoomDataManager.get().removeManagerMember(account);
    }

    /**
     * 加入黑名单
     *
     * @param roomId  -
     * @param account -
     * @param mark    true:设置，false：取消
     */
    public void markBlackListBySdk(String roomId, final String account, final boolean mark, final CallBack<ChatRoomMember> callBack) {
        NIMClient.getService(ChatRoomService.class)
                .markChatRoomBlackList(mark, new MemberOption(roomId, account))
                .setCallback(new RequestCallback<ChatRoomMember>() {
                    @Override
                    public void onSuccess(ChatRoomMember chatRoomMember) {
                        if (callBack != null) {
                            callBack.onSuccess(chatRoomMember);
                        }
                    }

                    @Override
                    public void onFailed(int i) {
                        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "操作失败，请重试");
                        if (callBack != null) {
                            callBack.onFail(i, "");
                        }
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "操作失败，请重试");
                        if (callBack != null) {
                            callBack.onFail(-1, throwable.getMessage());
                        }
                    }
                });
    }

    /**
     * 设置管理员
     *
     * @param roomId  -
     * @param account 要设置的管理员id
     * @param mark    true：设置，false：取消
     */
    public void markManagerListBySdk(String roomId, final String account, final boolean mark, final CallBack<ChatRoomMember> callBack) {
        NIMClient.getService(ChatRoomService.class)
                .markChatRoomManager(mark, new MemberOption(roomId, account))
                .setCallback(new RequestCallback<ChatRoomMember>() {
                    @Override
                    public void onSuccess(ChatRoomMember chatRoomMember) {
                        if (callBack != null)
                            callBack.onSuccess(chatRoomMember);
                        if (mark) addManagerMember(chatRoomMember);
                        else removeManagerMember(chatRoomMember);
                    }

                    @Override
                    public void onFailed(int i) {
                        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "操作失败，请重试");
                        if (callBack != null) {
                            callBack.onFail(i, "");
                        }
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "操作失败，请重试");
                        if (callBack != null) {
                            callBack.onFail(-1, throwable.getMessage());
                        }
                    }
                });
    }


    /**
     * 下麦
     *
     * @param micPosition -
     * @param callBack    -
     */
    public void downMicroPhoneBySdk(int micPosition, final CallBack<String> callBack) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        L.info(TAG, "downMicroPhoneBySdk micPosition: %d", micPosition);
        if (roomInfo == null) {
            L.info(TAG, "downMicroPhoneBySdk roomInfo is null.");
            return;
        }

        if (micPosition < -1) {
            return;
        }

        //防止房主掉麦
        if (micPosition == -1) {
            String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            if (!AvRoomDataManager.get().isRoomOwner(currentUid)) {
                return;
            }
        }
        L.info("nim_sdk", "downMicroPhoneBySdk: " + String.valueOf(micPosition));
        NIMChatRoomSDK.getChatRoomService()
                .pollQueue(String.valueOf(roomInfo.getRoomId()), String.valueOf(micPosition))
                .setCallback(new RequestCallback<Entry<String, String>>() {
                    @Override
                    public void onSuccess(Entry<String, String> stringStringEntry) {
                        if (callBack != null)
                            callBack.onSuccess("下麦成功");
                    }

                    @Override
                    public void onFailed(int i) {
                        if (callBack != null) {
                            callBack.onFail(-1, "下麦失败");
                        }
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        if (callBack != null) {
                            callBack.onFail(-1, "下麦异常:" + throwable.getMessage());
                        }
                    }
                });
    }

    /**
     * <p>下麦</p>
     * 云信聊天室队列服务：取出队列头部或者指定元素
     * <p>roomId - 聊天室id</p>
     * <p>key - 需要取出的元素的唯一键。若为 null，则表示取出队头元素</p>
     *
     * @param micPosition -
     */
    public Single<String> downMicroPhoneBySdk(final int micPosition) {


        L.info("nim_sdk", "pollQueue_api_3");
        if (micPosition < -1) {
            return null;
        }

        //防止房主掉麦
        if (micPosition == -1) {
            String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            if (!AvRoomDataManager.get().isRoomOwner(currentUid)) {
                return null;
            }
        }
        L.info("incomingChatObserver", "downMicroPhoneBySdk" + "    do");
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) return null;
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(SingleEmitter<String> e) throws Exception {
                L.info("incomingChatObserver", "pollQueue" + "     midIndex" + micPosition);
                RequestResult<Entry<String, String>> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .pollQueue(String.valueOf(roomInfo.getRoomId()), String.valueOf(micPosition)));
                if (result.exception != null)
                    e.onError(result.exception);
                else if (result.code != BaseMvpModel.RESULT_OK)
                    e.onError(new Exception("错误码: " + result.code));
                else {
                    e.onSuccess("下麦回调成功");
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 加入/移除黑名单
     * <p>添加/移出聊天室黑名单</p>
     * Parameters:
     * isAdd - true:添加, false:移出
     * memberOption - 请求参数，包含聊天室id，帐号id以及可选的扩展字段
     * Returns:
     * InvocationFuture 可以设置回调函数。回调中返回成员信息
     *
     * @param roomId  -
     * @param account -
     * @param mark    true:设置，false：取消
     */
    public Single<String> markBlackListBySdk(final String roomId, final String account, final boolean mark) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(SingleEmitter<String> e) throws Exception {
                RequestResult<ChatRoomMember> result = NIMClient.syncRequest(NIMClient.getService(ChatRoomService.class)
                        .markChatRoomBlackList(mark, new MemberOption(roomId, account)));
                if (result.exception != null)
                    e.onError(result.exception);
                else if (result.code != BaseMvpModel.RESULT_OK)
                    e.onError(new Exception("错误码: " + result.code));
                else {
                    e.onSuccess("黑名单处理回调成功");
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 踢出房间
     *
     * @param roomId          聊天室 id
     * @param account         踢出成员帐号。仅管理员可以踢；如目标是管理员仅创建者可以踢
     * @param notifyExtension 被踢通知扩展字段，这个字段会放到被踢通知的扩展字段中
     */
    public Single<String> kickMemberFromRoomBySdk(final long roomId, final long account, final Map<String, Object> notifyExtension) {


        statisticsReason(roomId, notifyExtension);
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(SingleEmitter<String> e) throws Exception {
                RequestResult<Void> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .kickMember(String.valueOf(roomId), String.valueOf(account), notifyExtension));
                if (result.exception != null)
                    e.onError(result.exception);
                else if (result.code != BaseMvpModel.RESULT_OK)
                    if (result.code == 404) {
                        e.onError(new Exception("该用户已不在房间内！"));
                    } else {
                        e.onError(new Exception("错误码: " + result.code));
                    }
                else {
                    e.onSuccess("踢人出房间回调成功");
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private void statisticsReason(long lroomId, Map<String, Object> extension) {

        if (extension == null)
            return;

        String roomId = lroomId + "";

        StringBuffer stringBuffer = new StringBuffer();
        Map<String, String> hashMap = new HashMap<>();
        System.currentTimeMillis();
        hashMap.put("uid", "b" + CoreManager.getCore(IAuthCore.class).getCurrentUid());
        hashMap.put("time", TimeUtils.stampToDate(System.currentTimeMillis()) + "  ");
        hashMap.put("roomId", roomId + "  ");
        for (Map.Entry<String, Object> entry : extension.entrySet()) {
            hashMap.put(entry.getKey() + "", entry.getValue() + "  ");
        }
        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
            stringBuffer.append(entry.getValue() + ",");
        }
//        L.info("statisticsReason", stringBuffer + "");

//        StatisticManager.get().onEvent(BasicConfig.INSTANCE.getAppContext(), StatisticModel.EventId.KICK_USER,
//                stringBuffer + "踢人", hashMap);

    }

    /**
     * <p>下麦</p>
     * 云信聊天室队列服务：加入或者更新队列元素，支持当用户掉线或退出聊天室后，是否删除这个元素
     * <p>roomId - 聊天室id</p>
     * <p>key -  新元素（或待更新元素）的唯一键</p>
     * <p>value -  新元素（待待更新元素）的内容</p>
     * <p>isTransient - (可选参数，不传默认false)。true表示当提交这个新元素的用户从聊天室掉线或退出的时候，需要删除这个元素；默认false表示不删除</p>
     *
     * @param micPosition -
     */
    public Single<Boolean> updateQueueEx(final long roomId, final int micPosition, final String value) {
        return Single.create(new SingleOnSubscribe<Boolean>() {
            @Override
            public void subscribe(SingleEmitter<Boolean> e) throws Exception {
                RequestResult<Void> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService().updateQueueEx(String.valueOf(roomId), String.valueOf(micPosition), value, true));
                if (result.exception != null)
                    e.onError(result.exception);
                else if (result.code != BaseMvpModel.RESULT_OK)
                    e.onError(new Exception("错误码: " + result.code));
                else
                    e.onSuccess(true);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 邀请上麦的自定义消息
     *
     * @param micUid   上麦用户uid
     * @param position 要上麦的位置
     */
    public Single<ChatRoomMessage> inviteMicroPhoneBySdk(final long micUid, final int position) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null)
            return Single.error(new IllegalArgumentException("inviteMicroPhoneBySdk roomInfo 不能为null"));
        return Single.create(
                new SingleOnSubscribe<ChatRoomMessage>() {
                    @Override
                    public void subscribe(SingleEmitter<ChatRoomMessage> e) throws Exception {
                        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(CUSTOM_MSG_HEADER_TYPE_QUEUE,
                                CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE);
                        queueMsgAttachment.uid = String.valueOf(micUid);
                        queueMsgAttachment.micPosition = position;
                        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                                String.valueOf(roomInfo.getRoomId()), queueMsgAttachment);
                        e.onSuccess(message);
                    }
                }).subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .filter(new Predicate<ChatRoomMessage>() {
                    @Override
                    public boolean test(ChatRoomMessage chatRoomMessage) throws Exception {
                        //上麦条件:游客不在麦上 或者是管理员，房主
                        return (!AvRoomDataManager.get().isOnMic(micUid)
                                || AvRoomDataManager.get().isRoomOwner()
                                || AvRoomDataManager.get().isRoomAdmin())
                                && position != Integer.MIN_VALUE;
                    }
                })
                .toSingle()
                .flatMap(new Function<ChatRoomMessage, SingleSource<? extends ChatRoomMessage>>() {
                    @Override
                    public SingleSource<? extends ChatRoomMessage> apply(ChatRoomMessage chatRoomMessage) throws Exception {
                        return sendChatRoomMessage(chatRoomMessage, false);
                    }
                });
    }

    /**
     * 踢人下麦
     *
     * @param micUid 被踢用户uid
     * @param roomId 房间ID
     */
    public Single<ChatRoomMessage> kickMicroPhoneBySdk(long micUid, long roomId) {
        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(CUSTOM_MSG_HEADER_TYPE_QUEUE,
                CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK);
        queueMsgAttachment.uid = String.valueOf(micUid);
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                String.valueOf(roomId), queueMsgAttachment);
        return sendChatRoomMessage(message, false);
    }

    /**
     * 发送Pk消息
     *
     * @param second
     * @param pkVoteInfo
     */
    public void sendPkNotificationBySdk(int first, int second, PkVoteInfo pkVoteInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        long roomId = roomInfo.getRoomId();
        PkCustomAttachment pkCustomAttachment = new PkCustomAttachment(first, second);
        pkCustomAttachment.setPkVoteInfo(pkVoteInfo);
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                String.valueOf(roomId), pkCustomAttachment);
        sendMessage(message, false);
    }

    /**
     * 发送开启和关闭屏蔽小礼物特效消息和屏蔽公屏消息
     *
     * @param uid
     * @param second
     * @param micIndex
     */
    public void systemNotificationBySdk(long uid, int second, int micIndex) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;

        if (roomInfo == null) {
            return;
        }
        long roomId = roomInfo.getRoomId();
        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(CUSTOM_MSG_HEADER_TYPE_QUEUE, second);
        queueMsgAttachment.uid = String.valueOf(uid);
        if (micIndex != -1)
            queueMsgAttachment.micPosition = micIndex;
        final ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                String.valueOf(roomId), queueMsgAttachment);
        sendChatRoomMessage(message, false).subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
            @Override
            public void accept(ChatRoomMessage chatRoomMessage, Throwable throwable) throws Exception {
                if (throwable == null) {
                    sendCommonMessage(message);
                }
            }
        });
    }

    private void sendMessage(final ChatRoomMessage msg, boolean resend) {
        sendChatRoomMessage(msg, resend).subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
            @Override
            public void accept(ChatRoomMessage chatRoomMessage, Throwable throwable) throws Exception {
                if (throwable == null) {
                    sendCommonMessage(msg);
                }
            }
        });
    }

    /************************云信聊天室 房主/管理员操作 end******************************/

    /**
     * <p>发送 云信聊天室信息</p>
     * <p>返回的是一个观察者，需要subscribe才会开始发送信息</p>
     * <p>不关心信息发送是否成功的，可以直接调用无参subscribe()方法，
     * 相反关心的则需要在subscribe(new Consumer(..){...})方法中通过Consumer来消耗事件</p>
     * <p>ps: 无参subscribe()方法调用如果发生错误，会在rxjava的error handler中得到回调,默认的实现在application的onCreate中</p>
     *
     * @param chatRoomMessage 自定义的聊天室信息
     * @param resend          是否自动重发
     * @param enable          是否反垃圾
     * @return 返回一个可被observer观察订阅的observable
     */
    public Single<ChatRoomMessage> sendChatRoomMessage(final ChatRoomMessage chatRoomMessage, final boolean resend, boolean enable) {
        if (chatRoomMessage == null) {
            throw new IllegalArgumentException("ChatRoomMessage can't be null!");
        }


        // 构造反垃圾对象
        NIMAntiSpamOption antiSpamOption = chatRoomMessage.getNIMAntiSpamOption();
        if (antiSpamOption == null) {
            antiSpamOption = new NIMAntiSpamOption();
        }
        antiSpamOption.enable = enable;
        chatRoomMessage.setNIMAntiSpamOption(antiSpamOption);

        return Single.create(new SingleOnSubscribe<ChatRoomMessage>() {
            @Override
            public void subscribe(SingleEmitter<ChatRoomMessage> e) throws Exception {
                RequestResult<Void> result =
                        NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                                .sendMessage(chatRoomMessage, resend));
                if (result.exception != null) {

                    e.onError(result.exception);
                } else if (result.code != BaseMvpModel.RESULT_OK) {
                    e.onError(new Exception("错误码: " + result.code));

                } else {
                    e.onSuccess(chatRoomMessage);

                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<ChatRoomMessage> sendChatRoomMessage(final ChatRoomMessage chatRoomMessage, final boolean resend) {
        return sendChatRoomMessage(chatRoomMessage, resend, false);
    }


    /**
     * -------------------------通知begin-------------------------------
     */
    private void noticeImNetReLogin(RoomQueueInfo roomQueueInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.ROOM_CHAT_RECONNECTION)
                .setRoomQueueInfo(roomQueueInfo));
    }

    private void noticeRoomMemberChange(boolean isMemberIn, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setAccount(account)
                .setEvent(isMemberIn ? RoomEvent.ROOM_MEMBER_IN : RoomEvent.ROOM_MEMBER_EXIT));
    }

    private void noticeManagerChange(boolean isAdd, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(isAdd ? RoomEvent.ROOM_MANAGER_ADD : RoomEvent.ROOM_MANAGER_REMOVE)
                .setAccount(account));
    }


    private void noticeReceiverMessageImmediately(ChatRoomMessage chatRoomMessage) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.RECEIVE_MSG)
                .setChatRoomMessage(chatRoomMessage)
        );
    }

    private void noticeReceiverMessage(ChatRoomMessage chatRoomMessage) {
        getChatRoomMsgPublisher().onNext(chatRoomMessage);
    }

    private void noticeEnterMessages() {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ENTER_ROOM));
    }

    public void noticeKickOutChatMember(ChatRoomKickOutEvent reason, String account) {
        L.info(TAG, account + ": noticeKickOutChatMember");
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.KICK_OUT_ROOM)
                .setReason(reason)
                .setAccount(account));
    }

    private void noticeKickDownMic(int position) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.KICK_DOWN_MIC)
                .setMicPosition(position));
    }

    private void noticeInviteUpMic(int position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.INVITE_UP_MIC)
                .setAccount(account)
                .setMicPosition(position));
    }

    public void noticeDownMic(String position, String account) {
        L.info(TAG, "noticeDownMic position: %s, account: %s", position, account);
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.DOWN_MIC)
                .setAccount(account)
                .setMicPosition(Integer.valueOf(position)));
    }


    private void noticeMicPosStateChange(int position, RoomQueueInfo roomQueueInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.MIC_QUEUE_STATE_CHANGE)
                .setMicPosition(position)
                .setRoomQueueInfo(roomQueueInfo));
    }


    private void noticeChatMemberBlackAdd(String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.ADD_BLACK_LIST)
                .setAccount(account));
    }


    private void noticeUpMic(int position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.UP_MIC)
                .setMicPosition(position)
                .setAccount(account)
        );
    }

    private void noticeMicInList() {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.MIC_IN_LIST_UPDATE)

        );
    }

    /**
     * 被挤下麦通知
     */
    private void noticeDownCrowdedMic(int position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.DOWN_CROWDED_MIC)
                .setMicPosition(position)
                .setAccount(account)
        );
    }

    private void noticeRoomInfoUpdate(RoomInfo roomInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ROOM_INFO_UPDATE).setRoomInfo(roomInfo));
    }

    private void noticeAuctionStart(AuctionInfo auctionInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.AUCTION_START).setAuctionInfo(auctionInfo));
    }

    private void noticeAuctionFinish(AuctionInfo auctionInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.AUCTION_FINISH).setAuctionInfo(auctionInfo));
    }

    private void noticeAuctionUpdate(AuctionInfo auctionInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.AUCTION_UPDATE).setAuctionInfo(auctionInfo));
    }

    public void noticeRoomCharm() {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ROOM_CHARM));
    }

    private void sendStatistics() {
        RoomInfo curRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (curRoomInfo != null) {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            long roomUid = curRoomInfo.getUid();
            String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
            Map<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("uid", String.valueOf(uid));
            params.put("roomUid", String.valueOf(roomUid));
            params.put("ticket", ticket);
            StatisticManager.get().sendStatisticToService(UriProvider.roomStatistics(), params);
        }
    }
}
