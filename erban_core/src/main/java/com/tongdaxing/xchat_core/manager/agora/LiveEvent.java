package com.tongdaxing.xchat_core.manager.agora;

/**
 * Created by Chen on 2018/10/23 0023.
 */
public class LiveEvent {
    public static class OnSpeakerVolume {
        private int mPos;
        private float mVolume = 0;

        public OnSpeakerVolume(int mPos, float mVolume) {
            this.mPos = mPos;
            this.mVolume = mVolume;
        }

        public int getPos() {
            return mPos;
        }

        public void setPos(int pos) {
            mPos = pos;
        }

        public float getVolume() {
            return mVolume;
        }

        public void setVolume(int volume) {
            mVolume = volume;
        }

        public boolean hasSound() {
            return mVolume > 1;
        }

    }

    public static class OnUserOffline {
        private int mUid;

        public OnUserOffline(int uid) {
            this.mUid = uid;
        }

        public int getUid() {
            return mUid;
        }
    }
}
