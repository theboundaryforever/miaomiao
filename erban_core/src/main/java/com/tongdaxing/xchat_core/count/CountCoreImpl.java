package com.tongdaxing.xchat_core.count;

import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.result.HomeResult;
import com.tongdaxing.xchat_core.utils.Logger;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;

/**
 * Created by seven on 2017/8/8.
 */

public class CountCoreImpl extends AbstractBaseCore implements ICountCore {
    public  static final String TAG ="CountCoreImpl";
    @Override
    public void AcgChattime() {
        HashMap<String, String> requestParam = CommonParamUtil.getDefaultParam();

        ResponseListener Listener = new ResponseListener<HomeResult>() {
            @Override
            public void onResponse(HomeResult response) {
                if (null != response ){
                    if (response.isSuccess()){
                        notifyClients(ICountCoreClient.class  , ICountCoreClient.METHOD_GET_ON_AVGCHATTIME,response.getData());
                    }else{
                        notifyClients(ICountCoreClient.class, ICountCoreClient.METHOD_GET_ON_AVGCHATTIME_FAIL,response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener(){
            @Override
            public void onErrorResponse(RequestError error) {
                Logger.error(TAG, error.getErrorStr());
                notifyClients(ICountCoreClient.class, ICountCoreClient.METHOD_GET_ON_AVGCHATTIME_FAIL, error.getErrorStr());
            }
        };

        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getAvgChattime(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        requestParam, Listener, errorListener,
                        HomeResult.class, Request.Method.GET);
    }
}
