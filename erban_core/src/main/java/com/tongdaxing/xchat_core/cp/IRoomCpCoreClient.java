package com.tongdaxing.xchat_core.cp;

import com.tongdaxing.xchat_core.gift.CpGiftReceiveInfo;
import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * <p>
 * Created by zhangjian on 2019/3/29.
 */
public interface IRoomCpCoreClient extends ICoreClient {
    String METHOD_ON_REFRESH_CP_ICON = "onRefreshCpIcon";
    //cp邀请
    public static final int CUSTOM_MSG_CP_INVITE = 0;
    //cp邀请-同意
    public static final int CUSTOM_MSG_CP_INVITE_AGREE = 1;
    //cp邀请-拒绝
    public static final int CUSTOM_MSG_CP_INVITE_REFUSE = 2;
    //cp邀请-超时失效
    public static final int CUSTOM_MSG_CP_INVITE_TIMEOUT = 3;
    //cp解除
    public static final int CUSTOM_MSG_CP_REMOVE = 4;

    public static final String METHOD_ON_OPEN_CP_GIFT_LIST = "onOpenCpGiftList";

    public static final String METHOD_ON_CP_INVITE = "onCpInvite";
    public static final String METHOD_ON_CP_INVITE_AGREE = "onCpInviteAgree";
    public static final String METHOD_ON_CP_INVITE_REFUSE = "onCpInviteRefuse";
    public static final String METHOD_ON_CP_REMOVE = "onCpRemove";

    void onRefreshCpIcon(long inviteUserId, long recUserId, boolean isShow);

    void onOpenCpGiftList(long uid);

    void onCpInvite(CpGiftReceiveInfo giftReceiveInfo);

    void onCpInviteAgree(CpGiftReceiveInfo giftReceiveInfo);

    void onCpInviteRefuse(CpGiftReceiveInfo giftReceiveInfo);

    void onCpRemove(CpGiftReceiveInfo giftReceiveInfo);
}
