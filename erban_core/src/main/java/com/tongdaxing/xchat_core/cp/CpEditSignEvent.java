package com.tongdaxing.xchat_core.cp;

/**
 * <p>
 * Created by zhangjian on 2019/4/30.
 */
public class CpEditSignEvent {
    public static class OnEditSuccess {
        String sign;

        public OnEditSuccess(String sign) {
            this.sign = sign;
        }

        public String getSign() {
            return sign;
        }
    }

    public static class OnEditClose {

    }

    public static class OnEditNeedCharge {

    }
}
