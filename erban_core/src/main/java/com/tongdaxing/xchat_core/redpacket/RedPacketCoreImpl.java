package com.tongdaxing.xchat_core.redpacket;

import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.result.ActionDialogResult;
import com.tongdaxing.xchat_core.result.RedDrawListInfoResult;
import com.tongdaxing.xchat_core.result.RedPacketResult;
import com.tongdaxing.xchat_core.result.WithdrawRedListResult;
import com.tongdaxing.xchat_core.result.WithdrawRedSucceedResult;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ${Seven} on 2017/9/20.
 */

public class RedPacketCoreImpl extends AbstractBaseCore implements IRedPacketCore {
    public RedPacketCoreImpl() {
        CoreManager.addClient(this);
    }

    //获取红包页面数据
    @Override
    public void getRedPacketInfo() {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        ResponseListener listener = new ResponseListener<RedPacketResult>() {
            @Override
            public void onResponse(RedPacketResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_RED_INFO, response.getData());
                    } else {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_RED_INFO_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                        .METHOD_ON_GET_RED_INFO_ERROR, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRedPacket(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RedPacketResult.class, Request.Method.GET);
    }

    @Override
    public void getActionDialog(int type) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("type", String.valueOf(type));
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        ResponseListener listener = new ResponseListener<ActionDialogResult>() {
            @Override
            public void onResponse(ActionDialogResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_ACTION_DIALOG, response.getData());
                    } else {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_ACTION_DIALOG_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                        .METHOD_ON_GET_ACTION_DIALOG_ERROR, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRedBagDialogType(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        ActionDialogResult.class, Request.Method.GET);
    }

    @Override
    public void getRedList() {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        ResponseListener listener = new ResponseListener<WithdrawRedListResult>() {
            @Override
            public void onResponse(WithdrawRedListResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_RED_LIST, response.getData());
                    } else {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_RED_LIST_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                        .METHOD_ON_GET_RED_LIST_ERROR, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRedBagList(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        WithdrawRedListResult.class, Request.Method.GET);

    }

    @Override
    public void getRedWithdraw(long uid, int packetId) {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("packetId", String.valueOf(packetId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<WithdrawRedSucceedResult>() {
            @Override
            public void onResponse(WithdrawRedSucceedResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_WITHDRAW, response.getData());
                    } else {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_WITHDRAW_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                        .METHOD_ON_GET_WITHDRAW_ERROR, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRedWithdraw(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        WithdrawRedSucceedResult.class, Request.Method.POST);
    }

    @Override
    public void getRedDrawList() {
        HashMap<String, String> params = CommonParamUtil.getDefaultParam();
        ResponseListener listener = new ResponseListener<RedDrawListInfoResult>() {
            @Override
            public void onResponse(RedDrawListInfoResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_RED_DRAW_LIST, response.getData());
                    } else {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_RED_DRAW_LIST_ERROR, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                        .METHOD_ON_GET_RED_DRAW_LIST_ERROR, error.getErrorStr());
            }
        };
        RequestManager.instance()
                .submitJsonResultQueryRequest(UriProvider.getRedDrawList(),
                        CommonParamUtil.getDefaultHeaders(getContext()),
                        params, listener, errorListener,
                        RedDrawListInfoResult.class, Request.Method.GET);
    }

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceivePersonalMessages(List<IMMessage> imMessages) {
        if (imMessages != null && imMessages.size() > 0) {
            for (IMMessage msg : imMessages) {
                if (msg.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment attachment = (CustomAttachment) msg.getAttachment();
                    if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PACKET) {
                        RedPacketAttachment redPacketAttachment = (RedPacketAttachment) msg.getAttachment();
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient.METHOD_ON_RECEIVE_NEW_PACKET, redPacketAttachment.getRedPacketInfo());
                    }
                }
            }
        }
    }
}
