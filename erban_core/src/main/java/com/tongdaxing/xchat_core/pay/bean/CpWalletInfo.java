package com.tongdaxing.xchat_core.pay.bean;

import java.io.Serializable;

/**
 * <p>
 * Created by zhangjian on 2019/3/28.
 */
public class CpWalletInfo implements Serializable {

    /**
     * avatar : https://pic.miaomiaofm.com/FvSKt0n0u-THlMQYuht0Uu_aucbR?imageslim
     * experLevel : 17
     * giftId : 169
     * giftName : 甜蜜套餐
     * giftNum : 1
     * giftPic : https://pic.miaomiaofm.com/Flx1EsE1jt0klqaFrLOT9REhF2aY?imageslim
     * goldPrice : 66
     * nick : 滴滴么么哒
     * targetAvatar : https://pic.miaomiaofm.com/default_head_pic.png
     * targetNick : 橘子
     * targetUid : 109820
     * uid : 109825
     * useGiftPurseGold : 66
     * userGiftPurseNum : 0
     */

    private String avatar;
    private int experLevel;
    private int giftId;
    private String giftName;
    private int giftNum;
    private String giftPic;
    private int goldPrice;
    private String nick;
    private String targetAvatar;
    private String targetNick;
    private int targetUid;
    private int uid;
    private int useGiftPurseGold;
    private int userGiftPurseNum;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(int giftNum) {
        this.giftNum = giftNum;
    }

    public String getGiftPic() {
        return giftPic;
    }

    public void setGiftPic(String giftPic) {
        this.giftPic = giftPic;
    }

    public int getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(int goldPrice) {
        this.goldPrice = goldPrice;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getTargetAvatar() {
        return targetAvatar;
    }

    public void setTargetAvatar(String targetAvatar) {
        this.targetAvatar = targetAvatar;
    }

    public String getTargetNick() {
        return targetNick;
    }

    public void setTargetNick(String targetNick) {
        this.targetNick = targetNick;
    }

    public int getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(int targetUid) {
        this.targetUid = targetUid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getUseGiftPurseGold() {
        return useGiftPurseGold;
    }

    public void setUseGiftPurseGold(int useGiftPurseGold) {
        this.useGiftPurseGold = useGiftPurseGold;
    }

    public int getUserGiftPurseNum() {
        return userGiftPurseNum;
    }

    public void setUserGiftPurseNum(int userGiftPurseNum) {
        this.userGiftPurseNum = userGiftPurseNum;
    }
}
