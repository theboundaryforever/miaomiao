package com.tongdaxing.xchat_core.pay.bean;

/**
 * <p>
 * Created by zhangjian on 2019/4/15.
 */
public class PaymentResponseBean {

    /**
     * appId : wxaf8731ed73d6bf3d
     * code : 0
     * hmac : F43143C9EBFB6776BFDB2F1975F7FC6D
     * nonceStr : tB7Rr0PLsaFJf3QdrG3S5qjpBcysEBVz
     * partnerId : 1505695561
     * paySign : IT5gLunlbPQHpDWdX/U8KvGG+KA4jopS7RcSbEzRfVFZQv29nWXWx71wG3ESiUfXs+piOwLcWZvAUI9nx6jUyPzdDgf3wOGujU1lhDmBTF64uxuJAniYUHxQtzWD5nPHFpVdoVOZP4tx1+o+1+rr9FoXOSw+B4ZMP9SMs2FNM9d826WN8zMv2udwKXazoKnu8Cgfbp/OZj3YfF4OzuFSFdpK8lgWPANL3zQ9EtsYr/n8c2llkvbJZJcxLqWSXGAk/LJCTueLAOSze2SkO+UIm02XQKCqF+y341P7D5pEUxUqH+SLtDxLIg+mdYPNHzDC6iW5LE2oBEMPyIzd3t9Nzg==
     * prepayId : wx15163751112167fa8d5941c30517521203
     * timeStamp : 1555317471
     */

    private String appId;
    private int code;
    private String hmac;
    private String nonceStr;
    private String partnerId;
    private String paySign;
    private String prepayId;
    private String timeStamp;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getHmac() {
        return hmac;
    }

    public void setHmac(String hmac) {
        this.hmac = hmac;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPaySign() {
        return paySign;
    }

    public void setPaySign(String paySign) {
        this.paySign = paySign;
    }

    public String getPrepayId() {
        return prepayId;
    }

    public void setPrepayId(String prepayId) {
        this.prepayId = prepayId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
