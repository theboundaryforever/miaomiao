package com.tongdaxing.xchat_core.pay;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.erban.libcommon.net.rxnet.RequestManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.notification.INotificationCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.result.ChargeListResult;
import com.tongdaxing.xchat_core.result.RequestChargeResult;
import com.tongdaxing.xchat_core.result.WalletInfoResult;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.NetworkUtils;

import java.util.HashMap;

/**
 * 实现网络的方法
 * Created by zhouxiangfeng on 2017/6/19.
 */

public class PayCoreImpl extends AbstractBaseCore implements IPayCore {
    public static final String TAG = "PayCoreImpl";
    private WalletInfo walletInfo;

    public PayCoreImpl() {
        CoreManager.addClient(this);
    }

    @Override
    public WalletInfo getCurrentWalletInfo() {
        return walletInfo;
    }

    @Override
    public void minusGold(int price) {
        if (walletInfo != null) {
            com.tongdaxing.xchat_framework.util.util.LogUtil.d("PayCoreImpl minusGold walletInfo原本信息：" + walletInfo.getGoldNum() + " price = " + price);
            double gold = walletInfo.getGoldNum();
            if (gold > 0 && gold >= price) {
                walletInfo.setGoldNum(gold - price);
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, walletInfo);
            }
        }
    }

    @Override
    public void setCurrentWalletInfo(WalletInfo walletInfo) {
        this.walletInfo = walletInfo;
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        if (accountInfo != null) {
            getWalletInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        }
    }

    @CoreEvent(coreClientClass = INotificationCoreClient.class)
    public void onReceivedWalletUpdateNotification(JSONObject attachment) {
        JSONObject jsonObject = (JSONObject) attachment.get("data");
        WalletInfo walletInfo = new WalletInfo();
        walletInfo.setUid(jsonObject.getLong("uid"));
        walletInfo.setDepositNum(jsonObject.getIntValue("depositNum"));
        walletInfo.setDiamondNum(jsonObject.getDouble("diamondNum"));
        walletInfo.setGoldNum(jsonObject.getIntValue("goldNum"));
        this.walletInfo = walletInfo;
        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, this.walletInfo);
    }

    @Override
    public void requestChargeOrOrderInfo() {
        String data = "";
        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_CHARGE_OR_ORDER_INFO, data);
    }

    //获取钱包信息
    @Override
    public void getWalletInfo(long uid) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<WalletInfoResult>() {
            @Override
            public void onResponse(WalletInfoResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        walletInfo = response.getData();
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_WALLENT_INOF, response.getData());
                    } else {
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_WALLENT_INOF_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_WALLENT_INOF_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getWalletInfos(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param,
                listener,
                errorListener,
                WalletInfoResult.class,
                Request.Method.GET
        );
    }

    @Override
    public void getChargeList(int channelType) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("channelType", String.valueOf(channelType));
        ResponseListener listener = new ResponseListener<ChargeListResult>() {
            @Override
            public void onResponse(ChargeListResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_CHARGE_LIST, response.getData());
                    } else {
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_CHARGE_LIST_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_CHARGE_LIST_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.getChargeList(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param,
                listener,
                errorListener,
                ChargeListResult.class,
                Request.Method.GET
        );
    }

    @Override
    public void requestCharge(int chargeProdId, String payChannel) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", String.valueOf(chargeProdId));
        param.put("payChannel", String.valueOf(payChannel));
        param.put("clientIp", NetworkUtils.getIPAddress(getContext()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<RequestChargeResult>() {
            @Override
            public void onResponse(RequestChargeResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_CHARGE_OR_ORDER_INFO, response.getData().toString());
                    } else {
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_CHARGE_OR_ORDER_INFO_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_CHARGE_OR_ORDER_INFO_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.requestCharge(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param,
                listener,
                errorListener,
                RequestChargeResult.class,
                Request.Method.POST
        );
    }

    @Override
    public void requestCDKeyCharge(String code) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("code", code);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<WalletInfoResult>() {
            @Override
            public void onResponse(WalletInfoResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        PayCoreImpl.this.walletInfo = response.getData();
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, response.getData());
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_CD_KEY_CHARGE, response.getData().getAmount());
                    } else {
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_CD_KEY_CHARGE_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_CD_KEY_CHARGE_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.requestCDKeyCharge(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param,
                listener,
                errorListener,
                WalletInfoResult.class,
                Request.Method.POST
        );
    }

    @Override
    public void exchangeGold(int diamondNum) {
        HashMap<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("diamondNum", String.valueOf(diamondNum));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        ResponseListener listener = new ResponseListener<WalletInfoResult>() {
            @Override
            public void onResponse(WalletInfoResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_EXCHANGE_GOLD, response.getData());

                        PayCoreImpl.this.walletInfo = response.getData();
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, response.getData());
                    } else {
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_EXCHANGE_GOLD_FAIL, response.getMessage());
                    }
                }
            }
        };
        ResponseErrorListener errorListener = new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_EXCHANGE_GOLD_FAIL, error.getErrorStr());
            }
        };
        RequestManager.instance().submitJsonResultQueryRequest(UriProvider.changeGold(),
                CommonParamUtil.getDefaultHeaders(getContext()),
                param,
                listener,
                errorListener,
                WalletInfoResult.class,
                Request.Method.POST
        );
    }
}
