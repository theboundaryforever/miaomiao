package com.tongdaxing.xchat_core.im.notification;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * Created by zhouxiangfeng on 2017/6/19.
 */

public interface INotificationCoreClient extends ICoreClient {
    String METHOD_ON_RECEIVED_WALLET_UPDATE_NOTIFICATION = "onReceivedWalletUpdateNotification";

    void onReceivedWalletUpdateNotification(JSONObject attachment);
}
