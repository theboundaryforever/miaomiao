package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Chen on 2019/5/22.
 */
public class FriendsBroadcastAttachment extends CustomAttachment {
    private String avatar;//房主头像
    private String roomTag;
    private String userNick;//房主昵称
    private String content;//发布内容
    private int onlineNum;
    private int roomUid;//房主ID
    private int roomId;//房间ID

    private boolean isBlueBg = true;//默认蓝色

    public FriendsBroadcastAttachment(int first, int second) {
        super(first, second);
    }

    public String getContent() {
        return content;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public String getUserNick() {
        return userNick;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public int getRoomUid() {
        return roomUid;
    }

    public int getRoomId() {
        return roomId;
    }

    public boolean isBlueBg() {
        return isBlueBg;
    }

    public void setBlueBg(boolean blueBg) {
        isBlueBg = blueBg;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public void setRoomUid(int roomUid) {
        this.roomUid = roomUid;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    @Override
    protected void parseData(JSONObject data) {
        avatar = data.getString("avatar");
        roomTag = data.getString("roomTag");
        userNick = data.getString("userNick");
        content = data.getString("content");
        onlineNum = data.getIntValue("onlineNum");
        roomUid = data.getIntValue("roomUid");
        roomId = data.getIntValue("roomId");
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("avatar", avatar);
        object.put("roomTag", roomTag);
        object.put("userNick", userNick);
        object.put("content", content);
        object.put("onlineNum", onlineNum);
        object.put("roomUid", roomUid);
        object.put("roomId", roomId);
        return object;
    }

    @Override
    public String toString() {
        return "FriendsBroadcastAttachment{" +
                "avatar='" + avatar + '\'' +
                ", roomTag='" + roomTag + '\'' +
                ", userNick='" + userNick + '\'' +
                ", content='" + content + '\'' +
                ", onlineNum=" + onlineNum +
                ", roomUid=" + roomUid +
                ", roomId=" + roomId +
                ", isBlueBg=" + isBlueBg +
                '}';
    }
}
