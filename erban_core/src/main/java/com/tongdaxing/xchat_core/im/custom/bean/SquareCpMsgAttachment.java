package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.find.bean.CpUserBean;

/**
 * Function:广场结成CP推送消息
 * Author: Edward on 2019/3/29
 */
public class SquareCpMsgAttachment extends CustomAttachment {
    /**
     * closeLevel : 30000
     * countTime : 2000
     * desc : 哇，恭喜js-学喵叫将【玫瑰花】赠予一壶酒成为众人羡慕的CP，大家来祝福他们吧。
     * giftId : 1
     * inviteUser : {"avatar":"https://pic.miaomiaofm.com/FnZ8wpBo26hjFfx9mriGV0Hr-sUK?imageslim","erbanNo":2048113,"nick":"js-学喵叫","uid":100000}
     * picUrl : https://pic.miaomiaofm.com/FklTLwCF9zcGKi45Qcp8FI66QpZs?imageslim
     * recUser : {"avatar":"https://pic.miaomiaofm.com/FmC72QC37C5g7Jqu0lT5WCt14A3N?imageslim","erbanNo":66666,"nick":"一壶酒","uid":100747}
     * roomUid : 25501704
     * title : cp广播
     */
    private int cpGiftLevel;//cp礼物等级
    private int closeLevel;
    private int countTime;
    private String desc;
    private int giftId;
    private CpUserBean inviteUser;
    private String picUrl;
    private CpUserBean recUser;
    private long roomUid;
    private String title;
    private String giftName;
    private String vggUrl;
    private int cpNum;//第几对cp
    private int second;//消息的second

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getCpGiftLevel() {
        return cpGiftLevel;
    }

    public void setCpGiftLevel(int cpGiftLevel) {
        this.cpGiftLevel = cpGiftLevel;
    }

    SquareCpMsgAttachment(int first, int second) {
        super(first, second);
    }

    public int getCloseLevel() {
        return closeLevel;
    }

    public void setCloseLevel(int closeLevel) {
        this.closeLevel = closeLevel;
    }

    public int getCountTime() {
        return countTime;
    }

    public void setCountTime(int countTime) {
        this.countTime = countTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public CpUserBean getInviteUser() {
        return inviteUser;
    }

    public void setInviteUser(CpUserBean inviteUser) {
        this.inviteUser = inviteUser;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public CpUserBean getRecUser() {
        return recUser;
    }

    public void setRecUser(CpUserBean recUser) {
        this.recUser = recUser;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVggUrl() {
        return vggUrl;
    }

    public void setVggUrl(String vggUrl) {
        this.vggUrl = vggUrl;
    }

    public int getCpNum() {
        return cpNum;
    }

    public void setCpNum(int cpNum) {
        this.cpNum = cpNum;
    }

    public int getDataSecond() {
        return second;
    }

    public void setDataSecond(int second) {
        this.second = second;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        if (data.containsKey("cpGiftLevel")) {
            cpGiftLevel = data.getInteger("cpGiftLevel");
        }
        if (data.containsKey("closeLevel")) {
            closeLevel = data.getInteger("closeLevel");
        }
        if (data.containsKey("countTime")) {
            countTime = data.getInteger("countTime");
        }
        if (data.containsKey("desc")) {
            desc = data.getString("desc");
        }
        if (data.containsKey("giftId")) {
            giftId = data.getInteger("giftId");
        }
        if (data.containsKey("giftName")) {
            giftName = data.getString("giftName");
        }
        if (data.containsKey("roomUid")) {
            roomUid = data.getLong("roomUid");
        }
        if (data.containsKey("title")) {
            title = data.getString("title");
        }
        if (data.containsKey("picUrl")) {
            picUrl = data.getString("picUrl");
        }
        if (data.containsKey("vggUrl")) {
            vggUrl = data.getString("vggUrl");
        }
        if (data.containsKey("cpNum")) {
            cpNum = data.getFloat("cpNum").intValue();
        }
        if (data.containsKey("second")) {
            second = data.getFloat("second").intValue();
        }
        if (data.containsKey("inviteUser")) {
            JSONObject object = data.getJSONObject("inviteUser");
            inviteUser = new CpUserBean();
            if (object.containsKey("avatar")) {
                inviteUser.setAvatar(object.getString("avatar"));
            }
            if (object.containsKey("erbanNo")) {
                inviteUser.setErbanNo(object.getLong("erbanNo"));
            }
            if (object.containsKey("nick")) {
                inviteUser.setNick(object.getString("nick"));
            }
            if (object.containsKey("uid")) {
                inviteUser.setUid(object.getLong("uid"));
            }
        }
        if (data.containsKey("recUser")) {
            JSONObject object = data.getJSONObject("recUser");
            recUser = new CpUserBean();
            if (object.containsKey("avatar")) {
                recUser.setAvatar(object.getString("avatar"));
            }
            if (object.containsKey("erbanNo")) {
                recUser.setErbanNo(object.getLong("erbanNo"));
            }
            if (object.containsKey("nick")) {
                recUser.setNick(object.getString("nick"));
            }
            if (object.containsKey("uid")) {
                recUser.setUid(object.getLong("uid"));
            }
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("cpGiftLevel", cpGiftLevel);
        object.put("closeLevel", closeLevel);
        object.put("countTime", countTime);
        object.put("desc", desc);
        object.put("picUrl", picUrl);
        object.put("giftId", giftId);
        object.put("giftName", giftName);
        object.put("roomUid", roomUid);
        object.put("title", title);
        object.put("vggUrl", vggUrl);
        object.put("cpNum", cpNum);
        object.put("second", second);
        object.put("inviteUser", inviteUser);
        object.put("recUser", recUser);
        return object;
    }
}
