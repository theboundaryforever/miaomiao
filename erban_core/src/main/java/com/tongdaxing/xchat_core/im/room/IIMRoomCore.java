package com.tongdaxing.xchat_core.im.room;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMemberUpdate;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

import java.util.Map;

/**
 * Created by chenran on 2017/2/16.
 */

public interface IIMRoomCore extends IBaseCore {


    /**
     * 进入聊天室
     *
     * @param roomId
     */
    void enterRoom(String roomId);

    /**
     * 离开聊天室
     */
    void quiteRoom();

    /**
     * 发送房间消息
     */
    void sendMessage(ChatRoomMessage message);

    /**
     * 踢出聊天室
     * 踢出成员。仅管理员可以踢；如目标是管理员仅创建者可以踢。
     * 可以添加被踢通知扩展字段，这个字段会放到被踢通知的扩展字段中。通知扩展字段最长1K；扩展字段需要传入 Map， SDK 会负责转成Json String。
     * 当有人被踢出聊天室时，会收到类型为 ChatRoomMemberKicked 的聊天室通知消息。
     */
    void kickMember(String roomId, String account, Map<String, Object> reason);


    int getChatRoomNumber();

//    void queryChatRoomMembers(String roomId);

    void queryQueue(String roomId);

    void markManagerList(String roomId, String account, boolean mark);

    void queryManagerList(String roomId);

    void markBlackList(String roomId, String account, boolean mark);

    void queryBlackList(String roomId);

    void updateQueue(String roomId, String key, String value);

    void updateMyRoomRole(String roomId, ChatRoomMemberUpdate chatRoomMemberUpdate, String account);
}
