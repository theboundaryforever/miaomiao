package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSONObject;


/**
 * Created by Administrator on 2018/3/20.
 */

public class PublicChatRoomAttachment extends CustomAttachment {


    private String msg;
    private String params;//avatar uid charmLevel nick
    private String roomId;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public PublicChatRoomAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        roomId = data.getString("roomId");
        msg = data.getString("msg");
        params = data.getString("params");
    }

    @Override
    protected JSONObject packData() {


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msg", msg);
        jsonObject.put("params", params);
        jsonObject.put("roomId", roomId);
        return jsonObject;
    }
}
