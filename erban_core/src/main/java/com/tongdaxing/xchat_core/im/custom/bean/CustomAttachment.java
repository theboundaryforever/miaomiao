package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;

/**
 * 先定义一个自定义消息附件的基类，负责解析你的自定义消息的公用字段，比如类型等等。
 *
 * @author zhouxiangfeng
 * @date 2017/6/8
 */
public class CustomAttachment implements MsgAttachment, IRoomTalkMessage {
    /**
     * 自定义消息附件的类型，根据该字段区分不同的自定义消息
     */
    private String mLocalId;

    public CustomAttachment() {
        setLocalId(System.currentTimeMillis() + "");
    }

    protected int first;
    protected int second;
    protected long time;
    protected JSONObject data;

    public static final int CUSTOM_MSG_HEADER_TYPE_AUCTION = 1;
    public static final int CUSTOM_MSG_SUB_TYPE_AUCTION_START = 11;
    public static final int CUSTOM_MSG_SUB_TYPE_AUCTION_FINISH = 12;
    public static final int CUSTOM_MSG_SUB_TYPE_AUCTION_UPDATE = 13;

    public static final int CUSTOM_MSG_HEADER_TYPE_ROOM_TIP = 2;
    public static final int CUSTOM_MSG_SUB_TYPE_ROOM_TIP_SHARE_ROOM = 21;
    public static final int CUSTOM_MSG_SUB_TYPE_ROOM_TIP_ATTENTION_ROOM_OWNER = 22;

    //单人礼物
    public static final int CUSTOM_MSG_HEADER_TYPE_GIFT = 3;
    public static final int CUSTOM_MSG_SUB_TYPE_SEND_GIFT = 31;

    public static final int CUSTOM_MSG_HEADER_TYPE_ACCOUNT = 5;

    public static final int CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI = 6;

    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE = 8;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE = 81;//抱上麦 {"data":{"micPosition":2,"uid":"109825"},"first":8,"second":81}
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK = 82;
    //屏蔽小礼物特效
    public static final int CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN = 155;
    public static final int CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE = 156;
    //公屏消息开关
    public static final int CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN = 153;
    public static final int CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE = 154;

    //麦上表情
    public static final int CUSTOM_MSG_HEADER_TYPE_FACE = 9;
    public static final int CUSTOM_MSG_SUB_TYPE_FACE_SEND = 91;

    public static final int CUSTOM_MSG_HEADER_TYPE_NOTICE = 10;


    public static final int CUSTOM_MSG_HEADER_TYPE_PACKET = 11;
    public static final int CUSTOM_MSG_SUB_TYPE_PACKET_FIRST = 111;

    //多人礼物
    public static final int CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT = 12;
    public static final int CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT = 14;
    public static final int CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT = 121;
    public static final int CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM = 15;//闲聊广场的聊天消息

    public static final int CUSTOM_MSG_MIC_IN_LIST = 17;

    //砸蛋全服消息 有气泡 云信不往自己房间发 本房间的是本地自己发的 本房间砸蛋消息一律无气泡
    public static final int CUSTOM_MSG_LOTTERY_BOX = 16;
    //砸蛋全服消息 没有气泡
    public static final int CUSTOM_MSG_TYPE_NEW_PUSH_FIRST = 21;//first和second一致
    public static final int CUSTOM_MSG_TYPE_NEW_PUSH_SECOND = 21;//first和second一致


    //转盘抽奖
    public static final int CUSTOM_MSG_HEADER_TYPE_LOTTERY = 13;
    public static final int CUSTOM_MSG_SUB_TYPE_NOTI_LOTTERY = 131;

    //（龙珠）速配，废弃
    public static final int CUSTOM_MSG_HEADER_TYPE_MATCH = 18;
    public static final int CUSTOM_MSG_HEADER_TYPE_MATCH_SPEED = 23;
    public static final int CUSTOM_MSG_HEADER_TYPE_MATCH_CHOICE = 24;

    //PK消息
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_FIRST = 19;
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_SECOND_START = 27;//开始
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_SECOND_END = 28;//结束
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_SECOND_CANCEL = 25;//取消
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_SECOND_ADD = 26;//投票

    //房间邀请好友消息
    public static final int CUSTOM_MSG_SHARE_FANS = 20;

    //房间内规则
    public static final int CUSTOM_MSG_TYPE_RULE_FIRST = 4;

    //爆出礼物的消息
    public static final int CUSTOM_MSG_TYPE_BURST_GIFT = 31;//first和second一致

    /**
     * 一元爆音房间公屏
     */
    public static final int CUSTOM_MSG_ONE_AUDIO = 23;
    /**
     * 新年福兽
     */
    public static final int CUSTOM_MSG_BLESSING_BEAST = 26;

    public static final int CUSTOM_MSG_BLESSING_BEAST_SECONDS = 27;

    //实名认证审核状态通知
    public static final int CUSTOM_MSG_AUTH_STATUS_NOTIFY = 35;

    //    public static final int
    //魅力值
    public static final int CUSTOM_MSG_ROOM_CHARM = 22;
    //魅力值更新
    public static final int CUSTOM_MSG_ROOM_SECOND_CHARM_UPDATE = 21;
    //魅力值清空
    public static final int CUSTOM_MSG_ROOM_SECOND_CHARM_CLEAR = 211;

    //禁言
    public static final int CUSTOM_MSG_BANNED = 37;
    //后台送礼，通知刷新礼物列表
    public static final int CUSTOM_MSG_REFRESH_GIFT_LIST = 39;
    //结成CP广场消息
    public static final int CUSTOM_MSG_SQUARE_BROADCAST_MSG = 44;

    //【IM消息】 CP消息
    public static final int CUSTOM_MSG_CP = 4011;
    //【IM消息】 CP配对成功IM消息：CP配对成功啦！快前往你的专属页
    public static final int CUSTOM_MSG_CP_TIP = 401;
    //【房间消息】 只传给房间内要结成cp的两人 只有请求和拒绝2种
    public static final int CUSTOM_MSG_ROOM_CP_REQUEST = 411;
    //【全服消息】位于房间顶部横幅 展示CP表白 second: 0表示表白（动效只在2人房间展示） 45-0全服表白横幅 -> 表白所在房间全屏动效 -> 46全服底部公屏展示结成cp消息
    public static final int CUSTOM_MSG_ROOM_CP_TOP = 451;
    public static final int CUSTOM_MSG_ROOM_CP_TOP_SECOND_INVITE = 0;
    //【全服消息】位于房间底部公屏 展示房间内CP结成
    public static final int CUSTOM_MSG_ROOM_CP_BOTTOM = 461;
    //【透传消息】 只传给cp两人 同意结成cp和解除cp 更新用户资料页、用户资料弹框、私聊页面CP标识
    public static final int CUSTOM_MSG_CP_NOTIFYCATION = 471;
    //【房间消息】 只传给cp所在房间 包括同意和解除 方便房间内进行更新cp状态 房间内第三方都能及时看到最新状态
    public static final int CUSTOM_MSG_ROOM_CP_STATE_REFRESH = 511;

    public static final int CUSTOM_MSG_USER_SETTING_UPLOAD_LOG_FILE = 52;//上传日志

    //魔力转圈圈 全服气泡消息
    public static final int CUSTOM_MSG_TURN_TABLE_WITH_BUBBLE = 50;
    //魔力转圈圈 全服无气泡消息
    public static final int CUSTOM_MSG_TURN_TABLE_NO_BUBBLE = 49;
    //魔力转圈圈 暴走消息，服务器直接给：魔力转圈圈开始暴走啦，快来争夺豪华大奖~
    public static final int CUSTOM_MSG_TURN_TABLE_DETONATING = 43;
    //夺宝大转盘碎片兑换消息
    public static final int CUSTOM_MSG_TURN_TABLE_PIECES_EXCHANGE = 48;

    public static final int CUSTOM_MSG_ROOM_LOVER_WEEK_CP_MSG = 541;
    public static final int CUSTOM_MSG_ROOM_LOVER_WEEK_CP_OFFICE = 551;//喵喵酱一周cp消息
    public static final int CUSTOM_MSG_ROOM_LOVER_WEEK_CP_TACIT = 561;

    public static final int CUSTOM_MSG_ROOM_PERSONAL_ATTENTION_MSG = 54;//进房间引导关注消息
    public static final int CUSTOM_MSG_ROOM_PERSONAL_ATTENTION_SUCCESS_MSG = 55; //房间关注成功消息

    public static final int CUSTOM_MSG_ROOM_FIND_FRIENDS_BROADCAST_MSG = 56; //房间寻友广播消息

    //财富等级
    protected int experLevel;
    //魅力等级
    protected int charmLevel;

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public CustomAttachment(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    // 解析附件内容。
    public void fromJson(JSONObject data) {
        if (data != null) {
            parseData(data);
        }
    }

    // 实现 MsgAttachment 的接口，封装公用字段，然后调用子类的封装函数。
    @Override
    public String toJson(boolean send) {
        return CustomAttachParser.packData(first, second, packData());
    }


    // 子类的解析和封装接口。
    protected void parseData(JSONObject data) {

    }

    protected JSONObject packData() {
        return null;
    }


    protected JSONArray packArray() {
        return null;
    }

    @Override
    public int getRoomTalkType() {
        return 0;
    }

    @Override
    public String getLocalId() {
        return mLocalId;
    }

    @Override
    public void setLocalId(String localId) {
        mLocalId = localId;
    }
}
