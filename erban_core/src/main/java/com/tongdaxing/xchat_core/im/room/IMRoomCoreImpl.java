package com.tongdaxing.xchat_core.im.room;

import android.annotation.SuppressLint;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.ChatRoomServiceObserver;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMemberUpdate;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.chatroom.model.MemberOption;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.util.Entry;
import com.tcloud.core.CoreUtils;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.talk.RoomTalkEvent;
import com.tongdaxing.xchat_core.room.talk.message.CommonMessage;
import com.tongdaxing.xchat_core.room.talk.message.EggMessage;
import com.tongdaxing.xchat_core.room.talk.message.GiftMessage;
import com.tongdaxing.xchat_core.room.talk.message.IRoomTalkMessage;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import io.reactivex.functions.BiConsumer;

/**
 * Created by zhouxiangfeng on 2017/5/20.
 */

public class IMRoomCoreImpl extends AbstractBaseCore implements IIMRoomCore {

    private static final String TAG = "IMRoomCoreImpl";

    public EnterChatRoomResultData imRoomInfo;

    public IMRoomCoreImpl() {
//        NIMClient.getService(ChatRoomServiceObserver.class)
//                .observeReceiveMessage(incomingChatRoomMsg, true);
//        NIMClient.getService(ChatRoomServiceObserver.class)
//                .observeKickOutEvent(kickOutObserver, true);
//        NIMClient.getService(ChatRoomServiceObserver.class)
//                .observeOnlineStatus(onlineStatus, true);
//        NIMClient.getService(MsgServiceObserve.class)
//                .observeMsgStatus(sendMsgObserver, true);
    }

    private void updateMember(String account) {
        if (imRoomInfo == null) {
            return;
        }
        List<String> accounts = new ArrayList<>();
        accounts.add(account);
        NIMClient.getService(ChatRoomService.class).fetchRoomMembersByIds(imRoomInfo.getRoomId(), accounts)
                .setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
                    @Override
                    public void onResult(int i, List<ChatRoomMember> chatRoomMembers, Throwable throwable) {
                        if (i == 200 && chatRoomMembers != null && chatRoomMembers.size() > 0) {
                            notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_MEMBER_UPDATE, chatRoomMembers.get(0));
                        }
                    }
                });
    }

    @Override
    public void queryQueue(String roomId) {
        if (StringUtils.isEmpty(roomId)) {
            return;
        }


        NIMClient.getService(ChatRoomService.class)
                .fetchQueue(roomId)
                .setCallback(new RequestCallback<List<Entry<String, String>>>() {
                    @Override
                    public void onSuccess(List<Entry<String, String>> entries) {
                        //麦序上人员信息
                        if (entries != null) {
                            for (Entry<String, String> entry : entries) {
//                                entry.value:
                            }
                        }
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_QUERY_CHATROOM_QUEUE_SUCCESS, entries);
                    }

                    @Override
                    public void onFailed(int i) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_QUERY_CHATROOM_QUEUE_FAIL);
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_QUERY_CHATROOM_QUEUE_FAIL);
                    }
                });
    }

    @Override
    public void markManagerList(String roomId, String account, boolean mark) {
        if (StringUtils.isEmpty(roomId) || StringUtils.isEmpty(account)) {
            return;
        }

        NIMClient.getService(ChatRoomService.class)
                .markChatRoomManager(mark, new MemberOption(roomId, account))
                .setCallback(new RequestCallback<ChatRoomMember>() {
                    @Override
                    public void onSuccess(ChatRoomMember chatRoomMember) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_MARK_MANAGER_LIST, chatRoomMember);
                    }

                    @Override
                    public void onFailed(int i) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_MARK_MANAGER_LIST_FAIL);
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_MARK_MANAGER_LIST_FAIL);
                    }
                });
    }

    @Override
    public void queryManagerList(String roomId) {
        if (StringUtils.isEmpty(roomId)) {
            return;
        }

        NIMClient.getService(ChatRoomService.class)
                .fetchRoomMembers(roomId, MemberQueryType.NORMAL, 0, 500)
                .setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
                    @Override
                    public void onResult(int i, List<ChatRoomMember> chatRoomMembers, Throwable throwable) {
                        if (i == 200) {
                            if (chatRoomMembers == null) {
                                chatRoomMembers = new ArrayList<>();
                            }
                            List<ChatRoomMember> managerList = new ArrayList<>();
                            for (int j = 0; j < chatRoomMembers.size(); j++) {
                                ChatRoomMember chatRoomMember = chatRoomMembers.get(j);
                                if (chatRoomMember.getMemberType() == MemberType.ADMIN) {
                                    managerList.add(chatRoomMember);
                                }
                            }
                            notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_QUERY_MANAGER_LIST, managerList);
                        } else {
                            notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_QUERY_MANAGER_LIST_FAIL);
                        }
                    }
                });
    }

    @Override
    public void markBlackList(String roomId, String account, boolean mark) {
        if (StringUtils.isEmpty(roomId) || StringUtils.isEmpty(account)) {
            return;
        }

        NIMClient.getService(ChatRoomService.class)
                .markChatRoomBlackList(mark, new MemberOption(roomId, account))
                .setCallback(new RequestCallback<ChatRoomMember>() {
                    @Override
                    public void onSuccess(ChatRoomMember chatRoomMember) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_MARK_BLACK_LIST, chatRoomMember);
                    }

                    @Override
                    public void onFailed(int i) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_MARK_BLACK_LIST_FAIL);
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_MARK_BLACK_LIST_FAIL);
                    }
                });
    }

    @Override
    public void queryBlackList(String roomId) {
        if (StringUtils.isEmpty(roomId)) {
            return;
        }

        NIMClient.getService(ChatRoomService.class)
                .fetchRoomMembers(roomId, MemberQueryType.NORMAL, 0, 500)
                .setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
                    @Override
                    public void onResult(int i, List<ChatRoomMember> chatRoomMembers, Throwable throwable) {
                        if (i == 200) {
                            if (chatRoomMembers == null) {
                                chatRoomMembers = new ArrayList<>();
                            }
                            List<ChatRoomMember> managerList = new ArrayList<>();
                            for (int j = 0; j < chatRoomMembers.size(); j++) {
                                ChatRoomMember chatRoomMember = chatRoomMembers.get(j);
                                if (chatRoomMember.isInBlackList()) {
                                    managerList.add(chatRoomMember);
                                }
                            }
                            notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_QUERY_BLACK_LIST, managerList);
                        } else {
                            notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_QUERY_BLACK_LIST_FAIL);
                        }
                    }
                });
    }

    @Override
    public void updateQueue(String roomId, String key, String value) {
        if (StringUtils.isEmpty(roomId)) {
            return;
        }
        NIMClient.getService(ChatRoomService.class)
                .updateQueue(roomId, key, value)
                .setCallback(new RequestCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_UPDATE_CHATROOM_QUEUE_SUCCESS);
                    }

                    @Override
                    public void onFailed(int i) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_UPDATE_CHATROOM_QUEUE_FAIL);
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_UPDATE_CHATROOM_QUEUE_FAIL);
                    }
                });
    }

    @Override
    public void updateMyRoomRole(final String roomId, final ChatRoomMemberUpdate chatRoomMemberUpdate, final String account) {
        if (StringUtils.isEmpty(roomId)) {
            return;
        }
        NIMClient.getService(ChatRoomService.class)
                .updateMyRoomRole(roomId, chatRoomMemberUpdate, true, null)
                .setCallback(new RequestCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        LogUtil.i(TAG, "onSuccess");
                        updateMember(account);
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_UPDATE_MY_ROOM_ROLE_SUCCESS, chatRoomMemberUpdate);
                    }

                    @Override
                    public void onFailed(int i) {
                        LogUtil.i(TAG, "onFailed");
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_UPDATE_MY_ROOM_ROLE_FAIL);
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        LogUtil.i(TAG, "onException");
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_UPDATE_MY_ROOM_ROLE_FAIL);
                    }
                });
    }

    private void queryChatRoomMembers(final String roomId) {
        NIMClient.getService(ChatRoomService.class)
                .fetchRoomMembers(roomId, MemberQueryType.GUEST, 0, 500)
                .setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
                    @Override
                    public void onResult(int i, final List<ChatRoomMember> guestChatRoomMember, Throwable throwable) {
                        if (i == 200) {
                            Collections.sort(guestChatRoomMember, new Comparator<ChatRoomMember>() {
                                public int compare(ChatRoomMember o1, ChatRoomMember o2) {
                                    if (o1.getEnterTime() < o2.getEnterTime()) {
                                        return 1;
                                    }
                                    if (o1.getEnterTime() == o2.getEnterTime()) {
                                        return 0;
                                    }
                                    return -1;
                                }
                            });
                            NIMClient.getService(ChatRoomService.class)
                                    .fetchRoomMembers(roomId, MemberQueryType.ONLINE_NORMAL, 0, 500)
                                    .setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
                                        @Override
                                        public void onResult(int i, List<ChatRoomMember> chatRoomMembers, Throwable throwable) {
                                            if (i == 200) {
                                                if (chatRoomMembers == null) {
                                                    chatRoomMembers = new ArrayList<>();
                                                }
                                                MLog.info(TAG, "guest count:" + chatRoomMembers.size());
                                                Collections.sort(chatRoomMembers, new Comparator<ChatRoomMember>() {
                                                    /*
                                                     * int compare(Student o1, Student o2) 返回一个基本类型的整型，
                                                     * 返回负数表示：o1 小于o2，
                                                     * 返回0 表示：o1和o2相等，
                                                     * 返回正数表示：o1大于o2。
                                                     */
                                                    public int compare(ChatRoomMember o1, ChatRoomMember o2) {
                                                        if (o1.getEnterTime() < o2.getEnterTime()) {
                                                            return 1;
                                                        }
                                                        if (o1.getEnterTime() == o2.getEnterTime()) {
                                                            return 0;
                                                        }
                                                        return -1;
                                                    }
                                                });
                                                notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_ROOM_ONLINE_MEMBER_UPDATE, guestChatRoomMember, chatRoomMembers);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    // : 2018/3/14
    @Override
    public void enterRoom(String roomId) {
        final long time = System.currentTimeMillis();
        LogUtil.i("enterRoom", "im enterRoom--->");
        EnterChatRoomData data = new EnterChatRoomData(roomId);

        NIMClient.getService(ChatRoomService.class).enterChatRoom(data)
                .setCallback(new RequestCallback<EnterChatRoomResultData>() {
                    @Override
                    public void onSuccess(EnterChatRoomResultData enterChatRoomResultData) {

                        LogUtil.i("enterRoom", "im enterRoom Success--->" + (System.currentTimeMillis() - time));
                        imRoomInfo = enterChatRoomResultData;
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_ENTER_ROOM);
                    }

                    @Override
                    public void onFailed(int i) {
//                        进入聊天室错误码主要有： 414: 参数错误 404: 聊天室不存在 403: 无权限 500: 服务器内部错误 13001: IM主连接状态异常 13002: 聊天室状态异常 13003: 黑名单用户禁止进入聊天室
                        String error;
                        switch (i) {
                            case 414:
                                error = "参数错误";
                                break;
                            case 404:
                                error = "聊天室不存在";
                                break;
                            case 403:
                                error = "无权限";
                                break;
                            case 500:
                                error = "服务器内部错误";
                                break;
                            case 13001:
                                error = "IM主连接状态异常";
                                break;
                            case 13002:
                                error = "聊天室状态异常";
                                break;
                            case 13003:
                                error = "黑名单用户禁止进入聊天室";
                                break;
                            default:
                                error = "网络异常";
                                break;
                        }
                        MLog.error(TAG, "code :" + i + " " + error);
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_ENTER_ROOM_FAITH, i, error);
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        MLog.error(TAG, "参数错误");
                        notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_ENTER_ROOM_FAITH, -1, "");
                    }
                });
    }

    @Override
    public int getChatRoomNumber() {
        if (imRoomInfo != null && imRoomInfo.getRoomInfo() != null) {
            return imRoomInfo.getRoomInfo().getOnlineUserCount();
        }
        return 0;
    }

    @Override
    public void quiteRoom() {
        if (null != imRoomInfo) {
            LogUtils.d("nim_sdk", "exitChatRoom_2");
            NIMClient.getService(ChatRoomService.class).exitChatRoom(imRoomInfo.getRoomId());
            imRoomInfo = null;
        }
    }

    @SuppressLint("CheckResult")
    @Override
    public void sendMessage(final ChatRoomMessage msg) {
        // 发送消息。如果需要关心发送结果，可设置回调函数。发送完成时，会收到回调。如果失败，会有具体的错误码。
        IMNetEaseManager.get().sendChatRoomMessage(msg, false).subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
            @Override
            public void accept(ChatRoomMessage chatRoomMessage, Throwable throwable) throws Exception {
                if (chatRoomMessage != null) {
                    // : 2017/12/20 两套逻辑
                    notifyClients(IIMRoomCoreClient.class, IIMRoomCoreClient.METHOD_ON_SEND_ROOM_MESSAGE_SUCCESS, msg);
                    if (msg.getMsgType() == MsgTypeEnum.custom) {
                        CustomAttachment attachment = (CustomAttachment) msg.getAttachment();
                        int first = attachment.getFirst();
                        if (first == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT ||
                                first == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                            RoomInfo info = AvRoomDataManager.get().mCurrentRoomInfo;
                            if (info != null) {
                                int giftEffectSwitch = info.getGiftEffectSwitch();
                                if (giftEffectSwitch == 0) {
                                    GiftMessage message = new GiftMessage();
                                    message.setChatRoomMessage(msg);
                                    sendMessage(message);
                                }
                            } else {
                                GiftMessage message = new GiftMessage();
                                message.setChatRoomMessage(msg);
                                sendMessage(message);
                            }
                        } else if (first == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP
                                || first == CustomAttachment.CUSTOM_MSG_TURN_TABLE_WITH_BUBBLE
                                || first == CustomAttachment.CUSTOM_MSG_TURN_TABLE_NO_BUBBLE
                                || first == CustomAttachment.CUSTOM_MSG_TURN_TABLE_DETONATING
                                || first == CustomAttachment.CUSTOM_MSG_TURN_TABLE_PIECES_EXCHANGE) {
                            sendCommonMessage(msg);
                        } else if (first == CustomAttachment.CUSTOM_MSG_LOTTERY_BOX ||
                                first == CustomAttachment.CUSTOM_MSG_TYPE_NEW_PUSH_FIRST) {
                            EggMessage message = new EggMessage();
                            message.setChatRoomMessage(msg);
                            sendMessage(message);
                        }
                    }
                } else {
                    LogUtil.e(TAG, throwable.getMessage(), throwable);
                }
            }
        });
    }

    private void sendCommonMessage(ChatRoomMessage msg) {
        CommonMessage message = new CommonMessage();
        message.setChatRoomMessage(msg);
        sendMessage(message);
    }

    private void sendMessage(IRoomTalkMessage message) {
        CoreUtils.send(new RoomTalkEvent.OnTalkMessageAccept<>(message));
    }

    @Override
    public void kickMember(String roomId, String account, Map<String, Object> reason) {
//        Map<String, Object> reason = new HashMap<>();
//        reason.put("reason", "kick");
//        String account = chatRoomMember.getAccount();
        NIMClient.getService(ChatRoomService.class)
                .kickMember(roomId, account, reason)
                .setCallback(new RequestCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }

                    @Override
                    public void onFailed(int i) {

                    }

                    @Override
                    public void onException(Throwable throwable) {

                    }
                });
    }

    public void registerKickOut(Observer<ChatRoomKickOutEvent> kickOutObserver, boolean register) {
        NIMClient.getService(ChatRoomServiceObserver.class)
                .observeKickOutEvent(kickOutObserver, register);
    }
}
