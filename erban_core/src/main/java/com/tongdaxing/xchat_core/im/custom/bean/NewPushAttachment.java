package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Function: 新全服消息
 * Author: Edward on 2018/12/20
 */
public class NewPushAttachment extends CustomAttachment {
    private String params;
    public String getParams() {
        return params;
    }
    public void setParams(String params) {
        this.params = params;
    }

    // {"first":21,"second":21,"data":{"params":"{\"nick\":\"出个流星吧。\",\"uid\":1506107,\"goldPrice\":1880,\"giftName\":\"木马\",\"count\":1,\"isFull\":true,\"roomId\":26256692}"}}
    private String nick;
    private long uid;
    private int goldPrice;
    private String giftName;
    private int count;
    private boolean isFull;

    public NewPushAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        params = data.getString("params");

//        setNick(data.getString("nick"));
//        setUid(data.getLong("uid"));
//        setGoldPrice(data.getInteger("goldPrice"));
//        setGiftName(data.getString("giftName"));
//        setCount(data.getInteger("count"));
//        setFull(data.getBoolean("isFull"));
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("params", params);

//        JSONObject object = new JSONObject();
//        object.put("nick", getNick());
//        object.put("uid", getUid());
//        object.put("goldPrice", getGoldPrice());
//        object.put("giftName", getGiftName());
//        object.put("count", getCount());
//        object.put("isFull", isFull());
        return object;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(int goldPrice) {
        this.goldPrice = goldPrice;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isFull() {
        return isFull;
    }

    public void setFull(boolean full) {
        isFull = full;
    }
}