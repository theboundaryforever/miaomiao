package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Chen on 2019/5/7.
 */
public class WeekCpAttachment extends CustomAttachment {

    public static final int ROOM_WEEK_CP_MSG = 1;     // 一周CP普通消息
    public static final int ROOM_WEEK_CP_MERRY = 2;     // 一周CP结成
    public static final int ROOM_WEEK_CP_TIP = 3;       // 默契考验
    public static final int ROOM_WEEK_CP_SINCERITY_ASK = 4;       // 走心回答
    public static final int ROOM_WEEK_CP_UPDATE = 5;       // 亲密值更新
    public static final int ROOM_WEEK_CP_BY_SOUL = 6;       // 灵魂话题增加亲密值
    public static final int ROOM_WEEK_CP_ALL_MSG = 7;       // 所有人看到的

    private String mJsonUidKey = "uid";
    private String mJsonCpIdKey = "cpUid";
    private String mJsonContentKey = "content";
    private String mJsonCloseLevelKey = "closeLevel";
    private String mJsonHeartValueKey = "heartValue";

    private String mUid;
    private String mCpId;
    private String mContent;
    private int mCloseLevel;
    private int mHeartValue;

    public WeekCpAttachment(int first, int second) {
        super(first, second);
    }

    public String getUid() {
        return mUid;
    }

    public void setUid(String uid) {
        mUid = uid;
    }

    public String getCpId() {
        return mCpId;
    }

    public void setCpId(String cpId) {
        mCpId = cpId;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public int getCloseLevel() {
        return mCloseLevel;
    }

    public void setCloseLevel(int closeLevel) {
        mCloseLevel = closeLevel;
    }

    public int getHeartValue() {
        return mHeartValue;
    }

    public void setHeartValue(int heartValue) {
        mHeartValue = heartValue;
    }

    @Override
    protected void parseData(JSONObject data) {
        mUid = data.getString(mJsonUidKey);
        mCpId = data.getString(mJsonCpIdKey);
        mContent = data.getString(mJsonContentKey);
        mCloseLevel = data.getIntValue(mJsonCloseLevelKey);
        mHeartValue = data.getIntValue(mJsonHeartValueKey);
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put(mJsonUidKey, mUid);
        object.put(mJsonCpIdKey, mCpId);
        object.put(mJsonContentKey, mContent);
        object.put(mJsonCloseLevelKey, mCloseLevel);
        object.put(mJsonHeartValueKey, mHeartValue);
        return object;
    }

    @Override
    public String toString() {
        return "WeekCpAttachment{" +
                "mUid='" + mUid + '\'' +
                ", mCpId='" + mCpId + '\'' +
                ", mContent='" + mContent + '\'' +
                ", mCloseLevel=" + mCloseLevel +
                ", mHeartValue=" + mHeartValue +
                '}';
    }
}
