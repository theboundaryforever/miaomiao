package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenran on 2017/10/25.
 */

public class MultiGiftAttachment extends CustomAttachment {
    private MultiGiftReceiveInfo multiGiftRecieveInfo;
    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public MultiGiftAttachment(int first, int second) {
        super(first, second);
    }

    public MultiGiftReceiveInfo getMultiGiftRecieveInfo() {
        return multiGiftRecieveInfo;
    }

    public void setMultiGiftAttachment(MultiGiftReceiveInfo multiGiftRecieveInfo) {
        this.multiGiftRecieveInfo = multiGiftRecieveInfo;
    }

    //财富等级
    private int experLevel;
    //魅力等级
    private int charmLevel;

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    @Override
    protected void parseData(JSONObject data) {
        experLevel = data.getInteger("experLevel");
        multiGiftRecieveInfo = new MultiGiftReceiveInfo();
        multiGiftRecieveInfo.setUid(data.getLong("uid"));
        multiGiftRecieveInfo.setGiftId(data.getInteger("giftId"));
        multiGiftRecieveInfo.setAvatar(data.getString("avatar"));
        multiGiftRecieveInfo.setNick(data.getString("nick"));
        multiGiftRecieveInfo.setGiftNum(data.getIntValue("giftNum"));
        JSONArray targetUidsJsonArray = data.getJSONArray("targetUids");
        if (targetUidsJsonArray != null) {
            List<Long> targetUids = new ArrayList<>();
            for (int i = 0; i < targetUidsJsonArray.size(); i++) {
                Long uid = targetUidsJsonArray.getLong(i);
                targetUids.add(uid);
            }
            multiGiftRecieveInfo.setTargetUids(targetUids);
        }

        JSONArray nickListJsonArray = data.getJSONArray("nickList");
        if (nickListJsonArray != null) {
            List<String> nickList = new ArrayList<>();
            for (int i = 0; i < nickListJsonArray.size(); i++) {
                String nick = nickListJsonArray.getString(i);
                nickList.add(nick);
            }
            multiGiftRecieveInfo.setNickList(nickList);
        }
        JSONArray avatarListJsonArray = data.getJSONArray("avatarList");
        if (avatarListJsonArray != null) {
            List<String> avatarList = new ArrayList<>();
            for (int i = 0; i < avatarListJsonArray.size(); i++) {
                String avatar = avatarListJsonArray.getString(i);
                avatarList.add(avatar);
            }
            multiGiftRecieveInfo.setAvatarList(avatarList);
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("uid", multiGiftRecieveInfo.getUid());
        object.put("giftId", multiGiftRecieveInfo.getGiftId());
        object.put("avatar", multiGiftRecieveInfo.getAvatar());
        object.put("nick", multiGiftRecieveInfo.getNick());
        object.put("giftNum", multiGiftRecieveInfo.getGiftNum());
        object.put("experLevel", experLevel);
        JSONArray jsonArray = new JSONArray();
        if (multiGiftRecieveInfo.getTargetUids() != null) {
            jsonArray.addAll(multiGiftRecieveInfo.getTargetUids());
        }
        object.put("targetUids", jsonArray);
        jsonArray = new JSONArray();
        if (multiGiftRecieveInfo.getNickList() != null) {
            jsonArray.addAll(multiGiftRecieveInfo.getNickList());
        }
        object.put("nickList", jsonArray);
        jsonArray = new JSONArray();
        if (multiGiftRecieveInfo.getAvatarList() != null) {
            jsonArray.addAll(multiGiftRecieveInfo.getAvatarList());
        }
        object.put("avatarList", jsonArray);
        return object;
    }
}
