package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Chen on 2019/5/7.
 */
public class WeekCpTacitAttachment extends CustomAttachment {

    public static final int ROOM_WEEK_CP_TACIT_TOPIC = 1;     // 题目推送
    public static final int ROOM_WEEK_CP_TACIT_ANSWER = 2;       // 解答推送
    public static final int ROOM_WEEK_CP_TACIT_EXIT = 3;     // 对方退出考验提示

    public WeekCpTacitAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);

    }

    @Override
    protected JSONObject packData() {
        return super.packData();

    }
}
