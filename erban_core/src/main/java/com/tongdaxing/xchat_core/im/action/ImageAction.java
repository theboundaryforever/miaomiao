package com.tongdaxing.xchat_core.im.action;

import android.text.TextUtils;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.session.actions.PickImageAction;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.io.File;

/**
 * Created by hzxuwen on 2015/6/12.
 */
public class ImageAction extends PickImageAction {

    public ImageAction() {
        super(R.drawable.icon_pic_action, R.string.input_panel_photo, true);
    }

    @Override
    protected void onPicked(File file) {
        IMMessage message = MessageBuilder.createImageMessage(getAccount(), getSessionType(), file, file.getName());
        sendMessage(message);
    }

    @Override
    public void onClick() {
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo == null) {
            return;
        }
        //先判断有没绑定手机
        boolean isBindPhone = !TextUtils.isEmpty(cacheLoginUserInfo.getPhone()) && !cacheLoginUserInfo.getPhone().equals(String.valueOf(cacheLoginUserInfo.getErbanNo()));
        if (!isBindPhone) {
            SingleToastUtil.showToast(getActivity(), "请先绑定手机");
        } else {
            if (cacheLoginUserInfo.getExperLevel() <= 0) {
                SingleToastUtil.showToast(getActivity(), "提升等级后可发送图片");
            } else {
                super.onClick();
            }
        }
    }
}

