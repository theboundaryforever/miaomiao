package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * <p>
 * Created by zhangjian on 2019/3/31.
 */
public class TipAttachment extends CustomAttachment {
    private String tip = "你们的CP关系已解除";
    private boolean isCpInviteSuccess;

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public boolean isCpInviteSuccess() {
        return isCpInviteSuccess;
    }

    public void setCpInviteSuccess(boolean cpInviteSuccess) {
        isCpInviteSuccess = cpInviteSuccess;
    }

    public TipAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        if (data.containsKey("isCpInviteSuccess")) {
            isCpInviteSuccess = data.getBoolean("isCpInviteSuccess");
        } else {
            if (getSecond() == CpGiftAttachment.CUSTOM_MSG_CP_REMOVE) {
                isCpInviteSuccess = false;
            }
        }
        if (!isCpInviteSuccess) {
            tip = "你们的CP关系已解除";
        } else {
            tip = data.getString("tip");
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();

        object.put("tip", tip);
        object.put("isCpInviteSuccess", isCpInviteSuccess);
        return object;
    }
}
