package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.bean.BlessingBeastInfo;

/**
 * Function:
 * Author: Edward on 2019/1/23
 */
public class BlessingBeastAttachment extends CustomAttachment {
    private long uid;
    private int type;
    private String content;
    private long roomUid;
    private long roomId;
    private BlessingBeastInfo blessingBeastInfo;

    public BlessingBeastInfo getBlessingBeastInfo() {
        return blessingBeastInfo;
    }

    public void setBlessingBeastInfo(BlessingBeastInfo blessingBeastInfo) {
        this.blessingBeastInfo = blessingBeastInfo;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getUid() {
        return uid;
    }

    @Override
    protected void parseData(JSONObject data) {
        uid = data.getLong("uid");
        type = data.getInteger("type");
        content = data.getString("context");
        roomUid = data.getLong("roomUid");
        roomId = data.getLong("roomId");
        if (data.containsKey("mascot")) {
            JSONObject jsonObject = data.getJSONObject("mascot");
            if (jsonObject != null) {
                BlessingBeastInfo blessingBeastInfo = new BlessingBeastInfo();
                blessingBeastInfo.setIcon(jsonObject.getString("icon"));
                blessingBeastInfo.setName(jsonObject.getString("name"));
                blessingBeastInfo.setWebUrl(jsonObject.getString("webUrl"));
                blessingBeastInfo.setRemainTime(jsonObject.getLong("remainTime"));
                blessingBeastInfo.setSvgaUrl(jsonObject.getString("svgaUrl"));
                this.blessingBeastInfo = blessingBeastInfo;
            }
        }
    }

    BlessingBeastAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("uid", uid);
        object.put("type", type);
        object.put("context", content);
        object.put("roomUid", roomUid);
        object.put("roomId", roomId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("icon", blessingBeastInfo.getIcon());
        jsonObject.put("name", blessingBeastInfo.getName());
        jsonObject.put("webUrl", blessingBeastInfo.getWebUrl());
        jsonObject.put("remainTime", blessingBeastInfo.getRemainTime());
        jsonObject.put("svgaUrl", blessingBeastInfo.getSvgaUrl());
        object.put("mascot", jsonObject.toJSONString());
        return object;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}