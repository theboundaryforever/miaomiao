package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachmentParser;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;
import com.tongdaxing.xchat_core.room.turnTable.bean.TurnTableDetonationAttachment;
import com.tongdaxing.xchat_core.room.turnTable.bean.TurnTablePiecesExchangeAttachment;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

/**
 * 云信自定义消息解析器
 *
 * @author polo
 * 自定义消息类型
 * 包装，解析
 */
public class CustomAttachParser implements MsgAttachmentParser {
    private static final String TAG = "CustomAttachParser";

    // 根据解析到的消息类型，确定附件对象类型
    @Override
    public MsgAttachment parse(String json) {
        CustomAttachment attachment = null;
        L.info(TAG, "CustomAttachParser.parse(): %s", json);
        try {
            JSONObject object = JSON.parseObject(json);
            int first = object.getInteger("first");
            int second = object.getInteger("second");

            switch (first) {
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_AUCTION:
                    attachment = new AuctionAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT:
                    attachment = new GiftAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT:
                    attachment = new MultiGiftAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI:
                    attachment = new OpenRoomNotiAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE:
                    attachment = new RoomQueueMsgAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE:
                    attachment = new FaceAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_NOTICE:
                    attachment = new NoticeAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP:
                    attachment = new RoomTipAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PACKET:
                    attachment = new RedPacketAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_LOTTERY:
                    attachment = new LotteryAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT:
                    attachment = new GiftAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM:
                    attachment = new PublicChatRoomAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_LOTTERY_BOX:
                    attachment = new LotteryBoxAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_MIC_IN_LIST:
                    attachment = new MicInListAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MATCH:
                    attachment = new RoomMatchAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_SHARE_FANS:
                    attachment = new ShareFansAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_FIRST:
                    attachment = new PkCustomAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_TYPE_BURST_GIFT:
                    attachment = new BurstGiftAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_TYPE_NEW_PUSH_FIRST:
                    attachment = new NewPushAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_CHARM:
                    attachment = new RoomCharmListAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ONE_AUDIO:
                    attachment = new OneAudioAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_BLESSING_BEAST:
                    attachment = new BlessingBeastAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_CP://40 IM CP消息
                    if (second == CpGiftAttachment.CUSTOM_MSG_CP_REMOVE) {
                        attachment = new TipAttachment(first, second);
                    } else {
                        attachment = new CpGiftAttachment(first, second);
                    }
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_CP_REQUEST://41 CP消息 跟CUSTOM_MSG_CP的消息格式一样
                    attachment = new CpGiftAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_SQUARE_BROADCAST_MSG:
                    attachment = new SquareCpMsgAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_CP_TOP:
                    attachment = new SquareCpMsgAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_CP_BOTTOM:
                    attachment = new SquareCpMsgAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_CP_TIP:
                    attachment = new TipAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_CP_STATE_REFRESH:
                    attachment = new CpGiftAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_TURN_TABLE_WITH_BUBBLE:
                    attachment = new LotteryBoxAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_TURN_TABLE_NO_BUBBLE:
                    attachment = new LotteryBoxAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_TURN_TABLE_DETONATING:
                    attachment = new TurnTableDetonationAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_TURN_TABLE_PIECES_EXCHANGE:
                    attachment = new TurnTablePiecesExchangeAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_USER_SETTING_UPLOAD_LOG_FILE:
                    attachment = new CustomAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_LOVER_WEEK_CP_MSG:
                    attachment = new WeekCpAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_LOVER_WEEK_CP_OFFICE:
                    attachment = new WeekCpMsgAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_LOVER_WEEK_CP_TACIT:
                    attachment = new WeekCpTacitAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_PERSONAL_ATTENTION_MSG:
                    attachment = new PersonalAttentionAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_PERSONAL_ATTENTION_SUCCESS_MSG:
                    attachment = new AttentionSuccessAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_FIND_FRIENDS_BROADCAST_MSG:
                    attachment = new FriendsBroadcastAttachment(first, second);
                    break;
                default:
                    break;
            }
            JSONObject data = object.getJSONObject("data");
            if (attachment != null) {
                attachment.fromJson(data);
            }
            if (first == CustomAttachment.CUSTOM_MSG_ROOM_CP_BOTTOM ||
                    first == CustomAttachment.CUSTOM_MSG_SQUARE_BROADCAST_MSG) {
                attachment.fromJson(object);
            }
        } catch (Exception e) {
            CoreUtils.crashIfDebug(e, TAG);
            e.printStackTrace();
        }

        return attachment;
    }

    public static String packData(int first, int second, JSONObject data) {
        JSONObject object = new JSONObject();
        object.put("first", first);
        object.put("second", second);
        if (data != null) {
            object.put("data", data);
        }
        return object.toJSONString();
    }
}