package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Chen on 2019/5/22.
 */
public class AttentionSuccessAttachment extends CustomAttachment {
    private String content;

    public AttentionSuccessAttachment(int first, int second) {
        super(first, second);
    }

    public String getContent() {
        return content;
    }

    @Override
    protected void parseData(JSONObject data) {
        content = data.getString("content");
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("content", content);
        return object;
    }
}
