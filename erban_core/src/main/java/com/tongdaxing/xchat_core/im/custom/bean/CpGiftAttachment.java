package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.gift.CpGiftReceiveInfo;

/**
 * Cp礼物自定义消息
 * Created by zhangjian on 2019/3/26.
 */

public class CpGiftAttachment extends CustomAttachment {
    //cp邀请
    public static final int CUSTOM_MSG_CP_INVITE = 0;
    //cp邀请-同意
    public static final int CUSTOM_MSG_CP_INVITE_AGREE = 1;
    //cp邀请-拒绝
    public static final int CUSTOM_MSG_CP_INVITE_REFUSE = 2;
    //cp邀请-超时失效
    public static final int CUSTOM_MSG_CP_INVITE_TIMEOUT = 3;
    //cp解除
    public static final int CUSTOM_MSG_CP_REMOVE = 4;

    private CpGiftReceiveInfo giftReceiveInfo;

    public CpGiftAttachment(int first, int second) {
        super(first, second);
    }

    public CpGiftReceiveInfo getGiftReceiveInfo() {
        return giftReceiveInfo;
    }

    public void setGiftReceiveInfo(CpGiftReceiveInfo giftReceiveInfo) {
        this.giftReceiveInfo = giftReceiveInfo;
    }

    @Override
    protected void parseData(JSONObject data) {
        giftReceiveInfo = new CpGiftReceiveInfo();
        giftReceiveInfo.setTitle(data.getString("title"));
        giftReceiveInfo.setDesc(data.getString("desc"));
        giftReceiveInfo.setPicUrl(data.getString("picUrl"));
        giftReceiveInfo.setInviteUid(data.getLong("inviteUid"));
        giftReceiveInfo.setInviteName(data.getString("inviteName"));
        giftReceiveInfo.setInviteAvatar(data.getString("inviteAvatar"));
        giftReceiveInfo.setGiftId(data.getInteger("giftId"));
        giftReceiveInfo.setGiftName(data.getString("giftName"));
        giftReceiveInfo.setCpGiftLevel(data.getInteger("cpGiftLevel"));
        giftReceiveInfo.setStatus(data.getInteger("status"));
        giftReceiveInfo.setRecordId(data.getInteger("recordId"));
        giftReceiveInfo.setRecUid(data.getInteger("recUid"));
        giftReceiveInfo.setVggUrl(data.getString("vggUrl"));
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("title", giftReceiveInfo.getTitle());
        object.put("desc", giftReceiveInfo.getDesc());
        object.put("picUrl", giftReceiveInfo.getPicUrl());
        object.put("inviteUid", giftReceiveInfo.getInviteUid());
        object.put("inviteName", giftReceiveInfo.getInviteName());
        object.put("inviteAvatar", giftReceiveInfo.getInviteAvatar());
        object.put("giftId", giftReceiveInfo.getGiftId());
        object.put("giftName", giftReceiveInfo.getGiftName());
        object.put("cpGiftLevel", giftReceiveInfo.getCpGiftLevel());
        object.put("status", giftReceiveInfo.getStatus());
        object.put("recordId", giftReceiveInfo.getRecordId());
        object.put("recUid", giftReceiveInfo.getRecUid());
        object.put("vggUrl", giftReceiveInfo.getVggUrl());
        return object;
    }

    @Override
    public String toString() {
        return "CpGiftAttachment{" +
                "giftReceiveInfo=" + giftReceiveInfo +
                ", first=" + first +
                ", second=" + second +
                '}';
    }
}
