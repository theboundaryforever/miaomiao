package com.tongdaxing.xchat_core.im.notification;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.netease.nim.uikit.custom.ICpNotifyCoreClient;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.model.CustomNotification;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.UpLoadFileEvent;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.custom.bean.CpGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.WeekCpTacitAttachment;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.weekcp.RoomLoverEvent;
import com.tongdaxing.xchat_core.room.weekcp.bean.TacitAnswerBean;
import com.tongdaxing.xchat_core.room.weekcp.bean.TacitTestBean;
import com.tongdaxing.xchat_core.room.weekcp.tacit.ITacitTestCtrl;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTestEvent;
import com.tongdaxing.xchat_core.room.weekcp.tacit.TacitTextInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.ChatUtil;

import java.util.List;
import java.util.Objects;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_USER_SETTING_UPLOAD_LOG_FILE;


/**
 * Created by zhouxiangfeng on 2017/6/19.
 */

public class NotificationCoreImpl extends AbstractBaseCore implements INotificationCore {

    private String TAG = "NotificationCoreImpl";

    @Override
    public void observeCustomNotification(boolean register) {
        // 如果有自定义通知是作用于全局的，不依赖某个特定的 Activity，那么这段代码应该在 Application 的 onCreate 中就调用
        NIMClient.getService(MsgServiceObserve.class).observeCustomNotification(new Observer<CustomNotification>() {
            @Override
            public void onEvent(CustomNotification notification) {
                String content = notification.getContent();
                JSONObject object = JSONObject.parseObject(content);
                int first = object.getIntValue("first");
                int second = object.getIntValue("second");
                L.info(TAG, "observeCustomNotification content: %s", content);
                switch (first) {
                    case CustomAttachment.CUSTOM_MSG_BANNED:
                        JSONObject jsonObject = (JSONObject) object.get("data");
                        ChatUtil.IM_PUSH_BANNED_ALL = jsonObject.getBoolean("all");
                        ChatUtil.IM_PUSH_BANNED_ROOM = jsonObject.getBoolean("room");
                        ChatUtil.IM_PUSH_BANNED_P2P = jsonObject.getBoolean("chat");
                        ChatUtil.IM_PUSH_BANNED_PUBLIC_ROOM = jsonObject.getBoolean("broadcast");
                        break;
                    case CustomAttachment.CUSTOM_MSG_REFRESH_GIFT_LIST:
                        //刷新礼物数据
                        CoreManager.getCore(IGiftCore.class).requestBackpackGiftInfos();
                        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
                        break;
                    case CustomAttachment.CUSTOM_MSG_CP_NOTIFYCATION:
                        //刷新cp状态
                        JSONObject cpObject = (JSONObject) object.get("data");
                        String recUid = cpObject.getString("recUid");
                        String inviteUid = cpObject.getString("inviteUid");
                        String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                        if (Objects.equals(recUid, currentUid)) {
                            recUid = inviteUid;
                        }

                        if (second == CpGiftAttachment.CUSTOM_MSG_CP_INVITE_AGREE) {
                            CoreUtils.send(new RoomLoverEvent.OnCpStateChange(recUid));
                            CoreManager.notifyClients(ICpNotifyCoreClient.class, ICpNotifyCoreClient.METHOD_ON_P2P_CP_INVITE_AGREE);
                        } else if (second == CpGiftAttachment.CUSTOM_MSG_CP_REMOVE) {
                            CoreUtils.send(new RoomLoverEvent.OnCpStateChange(""));
                            CoreManager.notifyClients(ICpNotifyCoreClient.class, ICpNotifyCoreClient.METHOD_ON_P2P_CP_REMOVE);
                        }
                        break;
                    case CUSTOM_MSG_USER_SETTING_UPLOAD_LOG_FILE://上传日志
                        if (second == CUSTOM_MSG_USER_SETTING_UPLOAD_LOG_FILE) {
                            CoreUtils.send(new UpLoadFileEvent.OnSilentUploadLogFile());
                        }
                        break;
                    case CustomAttachment.CUSTOM_MSG_ROOM_LOVER_WEEK_CP_TACIT:
                        // second为1是题目推送 2是双方解答情况推送 3是对方退出考验提示
                        TacitTextInfo tacitTextInfo = CoreManager.getCore(IRoomCore.class).getWeekManager().getTacitTextInfo();
                        if (second == WeekCpTacitAttachment.ROOM_WEEK_CP_TACIT_TOPIC) {
                            try {
                                Gson gson = new Gson();
                                TacitTestBean testBean = gson.fromJson(notification.getContent(), TacitTestBean.class);
                                if (testBean != null) {
                                    TacitTestBean.DataBean data = testBean.getData();
                                    if (data != null) {
                                        List<TacitTestBean.DataBean.QuestionBean> question = data.getQuestion();
                                        if (question != null) {
                                            tacitTextInfo.setDataBeans(question);
                                            long qid = data.getQid();
                                            L.debug(TAG, "question size: %d, qid: %s", question.size(), String.valueOf(qid));
                                            tacitTextInfo.setQuestionId(qid);
                                            tacitTextInfo.setCurrentState(ITacitTestCtrl.TACIT_TEST_UN_ASK);
                                            CoreUtils.send(new TacitTestEvent.OnTacitTestAsk(qid));
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                L.error(TAG, "first:56, second: 1, gson fromJson Exception:", e);
                            }
                        } else if (second == WeekCpTacitAttachment.ROOM_WEEK_CP_TACIT_ANSWER) {
                            try {
                                Gson gson = new Gson();
                                TacitAnswerBean tacitAnswerBean = gson.fromJson(notification.getContent(), TacitAnswerBean.class);
                                if (tacitAnswerBean != null) {
                                    tacitTextInfo.setTacitAnswerBean(tacitAnswerBean.getData());
                                    tacitTextInfo.setCurrentState(ITacitTestCtrl.TACIT_TEST_FINISHED);
                                    CoreUtils.send(new TacitTestEvent.OnTacitTestAskFinish(tacitAnswerBean.getData().getHeartValue()));
                                }
                            } catch (Exception e) {
                                L.error(TAG, "first:56, second: 2, gson fromJson Exception:", e);
                            }
                        } else {
                            CoreUtils.send(new TacitTestEvent.OnTacitTestAskExit(false));
                        }
                        break;
                    case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ACCOUNT:
                        notifyClients(INotificationCoreClient.class, INotificationCoreClient.METHOD_ON_RECEIVED_WALLET_UPDATE_NOTIFICATION, object);
                        break;
                }
            }
        }, register);
    }
}
