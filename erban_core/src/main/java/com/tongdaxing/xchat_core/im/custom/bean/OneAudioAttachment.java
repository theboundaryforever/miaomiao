package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Function:一元爆音自定义消息
 * Author: Edward on 2019/1/16
 */
public class OneAudioAttachment extends CustomAttachment {
    private long uid;
    private String nick;
    private String content;

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getUid() {
        return uid;
    }

    @Override
    protected void parseData(JSONObject data) {
        uid = data.getLong("uid");
        nick = data.getString("nick");
        content = data.getString("content");
    }

    OneAudioAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        return object;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}