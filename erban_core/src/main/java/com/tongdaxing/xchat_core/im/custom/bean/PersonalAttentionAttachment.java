package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Chen on 2019/5/7.
 */
public class PersonalAttentionAttachment extends CustomAttachment {
    /**
     * content:关注消息文本
     * title:关注消息按钮文本
     * uid:需要弹窗玩家的uid
     */
    private String content;
    private String title;
    private String uid;
    private boolean isAttention;

    public PersonalAttentionAttachment(int first, int second) {
        super(first, second);
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public String getUid() {
        return uid;
    }

    public boolean isAttention() {
        return isAttention;
    }

    public void setAttention(boolean attention) {
        isAttention = attention;
    }

    @Override
    protected void parseData(JSONObject data) {
        content = data.getString("content");
        title = data.getString("title");
        uid = data.getString("uid");
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("content", content);
        object.put("title", title);
        object.put("uid", uid);
        return object;
    }
}
