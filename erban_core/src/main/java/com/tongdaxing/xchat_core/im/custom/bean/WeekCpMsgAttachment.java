package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Chen on 2019/5/7.
 */
public class WeekCpMsgAttachment extends CustomAttachment {
    public static final int ROOM_WEEK_CP_OFFICE_MSG_MERRY = 1;       // 结成CP
    public static final int ROOM_WEEK_CP_OFFICE_MSG_TIP = 3;       // 到期提示
    public static final int ROOM_WEEK_CP_OFFICE_MSG_UNBAN = 4;       // CP解除
    private String content;

    public WeekCpMsgAttachment(int first, int second) {
        super(first, second);
    }

    public String getContent() {
        return content;
    }

    @Override
    protected void parseData(JSONObject data) {
        content = data.getString("content");
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("content", content);
        return object;
    }
}
