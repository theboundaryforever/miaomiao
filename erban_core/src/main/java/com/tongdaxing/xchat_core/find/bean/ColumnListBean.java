package com.tongdaxing.xchat_core.find.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class ColumnListBean implements Parcelable{
    @Override
    public String toString() {
        return "ColumnListBean{" +
                "endDate='" + endDate + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", seqNo=" + seqNo +
                ", startDate='" + startDate + '\'' +
                ", status=" + status +
                ", roomList=" + roomList +
                '}';
    }

    /**
     * endDate : 23:07
     * id : 9
     * name : 123
     * roomList : [{"abChannelType":1,"alarmEnable":true,"backPic":"0","calcSumDataIndex":0,"count":0,"factor":10,"isPermitRoom":1,"isRecom":0,"meetingName":"a13917829b5c4722a940ffba209462e3","officeUser":1,"onlineNum":20,"openTime":1548486833000,"operatorStatus":1,"roomDesc":"11","roomId":56717603,"roomNotice":"了","roomPwd":"","roomTag":"新秀","tagId":9,"tagPict":"https://pic.miaomiaofm.com/room_tag_new_newbie.png","targetType":1,"timeInterval":5000,"title":"【全服】","type":3,"uid":100739,"valid":true}]
     * seqNo : 123
     * startDate : 00:03
     * status : 1
     */

    private String endDate;
    private int id;
    private String name;
    private int seqNo;
    private String startDate;
    private int status;
    private List<ColumnListBean.RoomListBean> roomList;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ColumnListBean.RoomListBean> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<ColumnListBean.RoomListBean> roomList) {
        this.roomList = roomList;
    }

    public static class RoomListBean implements Parcelable{
        @Override
        public String toString() {
            return "RoomListBean{" +
                    "nick='" + nick + '\'' +
                    ", avatar='" + avatar + '\'' +
                    ", badge='" + badge + '\'' +
                    ", abChannelType=" + abChannelType +
                    ", alarmEnable=" + alarmEnable +
                    ", backPic='" + backPic + '\'' +
                    ", calcSumDataIndex=" + calcSumDataIndex +
                    ", count=" + count +
                    ", factor=" + factor +
                    ", isPermitRoom=" + isPermitRoom +
                    ", isRecom=" + isRecom +
                    ", meetingName='" + meetingName + '\'' +
                    ", officeUser=" + officeUser +
                    ", onlineNum=" + onlineNum +
                    ", openTime=" + openTime +
                    ", operatorStatus=" + operatorStatus +
                    ", roomDesc='" + roomDesc + '\'' +
                    ", roomId=" + roomId +
                    ", roomNotice='" + roomNotice + '\'' +
                    ", roomPwd='" + roomPwd + '\'' +
                    ", roomTag='" + roomTag + '\'' +
                    ", tagId=" + tagId +
                    ", tagPict='" + tagPict + '\'' +
                    ", targetType=" + targetType +
                    ", timeInterval=" + timeInterval +
                    ", title='" + title + '\'' +
                    ", type=" + type +
                    ", uid=" + uid +
                    ", valid=" + valid +
                    ", customTag='" + customTag + '\'' +
                    '}';
        }

        /**
         * abChannelType : 1
         * alarmEnable : true
         * backPic : 0
         * calcSumDataIndex : 0
         * count : 0
         * factor : 10
         * isPermitRoom : 1
         * isRecom : 0
         * meetingName : a13917829b5c4722a940ffba209462e3
         * officeUser : 1
         * onlineNum : 20
         * openTime : 1548486833000
         * operatorStatus : 1
         * roomDesc : 11
         * roomId : 56717603
         * roomNotice : 了
         * roomPwd :
         * roomTag : 新秀
         * tagId : 9
         * tagPict : https://pic.miaomiaofm.com/room_tag_new_newbie.png
         * targetType : 1
         * timeInterval : 5000
         * title : 【全服】
         * type : 3
         * uid : 100739
         * valid : true
         */
        private String nick;
        private String avatar;
        private String badge;
        private int abChannelType;
        private boolean alarmEnable;
        private String backPic;
        private int calcSumDataIndex;
        private int count;
        private int factor;
        private int isPermitRoom;
        private int isRecom;
        private String meetingName;
        private int officeUser;
        private int onlineNum;
        private long openTime;
        private int operatorStatus;
        private String roomDesc;
        private int roomId;
        private String roomNotice;
        private String roomPwd;
        private String roomTag;
        private int tagId;
        private String tagPict;
        private int targetType;
        private int timeInterval;
        private String title;
        private int type;
        private int uid;
        private boolean valid;
        private String customTag;

        public String getCustomTag() {
            return customTag;
        }

        public void setCustomTag(String customTag) {
            this.customTag = customTag;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getBadge() {
            return badge;
        }

        public void setBadge(String badge) {
            this.badge = badge;
        }

        public int getAbChannelType() {
            return abChannelType;
        }

        public void setAbChannelType(int abChannelType) {
            this.abChannelType = abChannelType;
        }

        public boolean isAlarmEnable() {
            return alarmEnable;
        }

        public void setAlarmEnable(boolean alarmEnable) {
            this.alarmEnable = alarmEnable;
        }

        public String getBackPic() {
            return backPic;
        }

        public void setBackPic(String backPic) {
            this.backPic = backPic;
        }

        public int getCalcSumDataIndex() {
            return calcSumDataIndex;
        }

        public void setCalcSumDataIndex(int calcSumDataIndex) {
            this.calcSumDataIndex = calcSumDataIndex;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getFactor() {
            return factor;
        }

        public void setFactor(int factor) {
            this.factor = factor;
        }

        public int getIsPermitRoom() {
            return isPermitRoom;
        }

        public void setIsPermitRoom(int isPermitRoom) {
            this.isPermitRoom = isPermitRoom;
        }

        public int getIsRecom() {
            return isRecom;
        }

        public void setIsRecom(int isRecom) {
            this.isRecom = isRecom;
        }

        public String getMeetingName() {
            return meetingName;
        }

        public void setMeetingName(String meetingName) {
            this.meetingName = meetingName;
        }

        public int getOfficeUser() {
            return officeUser;
        }

        public void setOfficeUser(int officeUser) {
            this.officeUser = officeUser;
        }

        public int getOnlineNum() {
            return onlineNum;
        }

        public void setOnlineNum(int onlineNum) {
            this.onlineNum = onlineNum;
        }

        public long getOpenTime() {
            return openTime;
        }

        public void setOpenTime(long openTime) {
            this.openTime = openTime;
        }

        public int getOperatorStatus() {
            return operatorStatus;
        }

        public void setOperatorStatus(int operatorStatus) {
            this.operatorStatus = operatorStatus;
        }

        public String getRoomDesc() {
            return roomDesc;
        }

        public void setRoomDesc(String roomDesc) {
            this.roomDesc = roomDesc;
        }

        public int getRoomId() {
            return roomId;
        }

        public void setRoomId(int roomId) {
            this.roomId = roomId;
        }

        public String getRoomNotice() {
            return roomNotice;
        }

        public void setRoomNotice(String roomNotice) {
            this.roomNotice = roomNotice;
        }

        public String getRoomPwd() {
            return roomPwd;
        }

        public void setRoomPwd(String roomPwd) {
            this.roomPwd = roomPwd;
        }

        public String getRoomTag() {
            return roomTag;
        }

        public void setRoomTag(String roomTag) {
            this.roomTag = roomTag;
        }

        public int getTagId() {
            return tagId;
        }

        public void setTagId(int tagId) {
            this.tagId = tagId;
        }

        public String getTagPict() {
            return tagPict;
        }

        public void setTagPict(String tagPict) {
            this.tagPict = tagPict;
        }

        public int getTargetType() {
            return targetType;
        }

        public void setTargetType(int targetType) {
            this.targetType = targetType;
        }

        public int getTimeInterval() {
            return timeInterval;
        }

        public void setTimeInterval(int timeInterval) {
            this.timeInterval = timeInterval;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.nick);
            dest.writeString(this.avatar);
            dest.writeString(this.badge);
            dest.writeInt(this.abChannelType);
            dest.writeByte(this.alarmEnable ? (byte) 1 : (byte) 0);
            dest.writeString(this.backPic);
            dest.writeInt(this.calcSumDataIndex);
            dest.writeInt(this.count);
            dest.writeInt(this.factor);
            dest.writeInt(this.isPermitRoom);
            dest.writeInt(this.isRecom);
            dest.writeString(this.meetingName);
            dest.writeInt(this.officeUser);
            dest.writeInt(this.onlineNum);
            dest.writeLong(this.openTime);
            dest.writeInt(this.operatorStatus);
            dest.writeString(this.roomDesc);
            dest.writeInt(this.roomId);
            dest.writeString(this.roomNotice);
            dest.writeString(this.roomPwd);
            dest.writeString(this.roomTag);
            dest.writeInt(this.tagId);
            dest.writeString(this.tagPict);
            dest.writeInt(this.targetType);
            dest.writeInt(this.timeInterval);
            dest.writeString(this.title);
            dest.writeInt(this.type);
            dest.writeInt(this.uid);
            dest.writeByte(this.valid ? (byte) 1 : (byte) 0);
            dest.writeString(this.customTag);
        }

        public RoomListBean() {
        }

        protected RoomListBean(Parcel in) {
            this.nick = in.readString();
            this.avatar = in.readString();
            this.badge = in.readString();
            this.abChannelType = in.readInt();
            this.alarmEnable = in.readByte() != 0;
            this.backPic = in.readString();
            this.calcSumDataIndex = in.readInt();
            this.count = in.readInt();
            this.factor = in.readInt();
            this.isPermitRoom = in.readInt();
            this.isRecom = in.readInt();
            this.meetingName = in.readString();
            this.officeUser = in.readInt();
            this.onlineNum = in.readInt();
            this.openTime = in.readLong();
            this.operatorStatus = in.readInt();
            this.roomDesc = in.readString();
            this.roomId = in.readInt();
            this.roomNotice = in.readString();
            this.roomPwd = in.readString();
            this.roomTag = in.readString();
            this.tagId = in.readInt();
            this.tagPict = in.readString();
            this.targetType = in.readInt();
            this.timeInterval = in.readInt();
            this.title = in.readString();
            this.type = in.readInt();
            this.uid = in.readInt();
            this.valid = in.readByte() != 0;
            this.customTag = in.readString();
        }

        public static final Creator<RoomListBean> CREATOR = new Creator<RoomListBean>() {
            @Override
            public RoomListBean createFromParcel(Parcel source) {
                return new RoomListBean(source);
            }

            @Override
            public RoomListBean[] newArray(int size) {
                return new RoomListBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.endDate);
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.seqNo);
        dest.writeString(this.startDate);
        dest.writeInt(this.status);
        dest.writeList(this.roomList);
    }

    public ColumnListBean() {
    }

    protected ColumnListBean(Parcel in) {
        this.endDate = in.readString();
        this.id = in.readInt();
        this.name = in.readString();
        this.seqNo = in.readInt();
        this.startDate = in.readString();
        this.status = in.readInt();
        this.roomList = new ArrayList<RoomListBean>();
        in.readList(this.roomList, RoomListBean.class.getClassLoader());
    }

    public static final Creator<ColumnListBean> CREATOR = new Creator<ColumnListBean>() {
        @Override
        public ColumnListBean createFromParcel(Parcel source) {
            return new ColumnListBean(source);
        }

        @Override
        public ColumnListBean[] newArray(int size) {
            return new ColumnListBean[size];
        }
    };
}
