package com.tongdaxing.xchat_core.find;

import com.tongdaxing.xchat_core.find.bean.SquareQueueInfo;
import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * Function:
 * Author: Edward on 2019/3/29
 */
public interface SquareClient extends ICoreClient {
    String CP_BROADCAST_MSG_NOTIFY = "onCpBroadcastMsgNotify";
    String METHOD_ON_ROOM_CP_TOP_MSG_CLICK = "onRoomCpTopClick";//房间顶部公屏消息点击

    void onCpBroadcastMsgNotify(SquareQueueInfo squareQueueInfo);

    void onRoomCpTopClick(long recUserId);
}
