package com.tongdaxing.xchat_core.find.bean;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class LoverMatchInfo {

    /**
     * abChannelType : 1
     * alarmEnable : true
     * backPic :
     * calcSumDataIndex : 0
     * count : 0
     * factor : 10
     * isPermitRoom : 1
     * isRecom : 0
     * meetingName : 41f5b6b0ce714fcbb61935dc7140e4dd
     * officeUser : 1
     * onlineNum : 10
     * openTime : 1547547058000
     * operatorStatus : 1
     * roomDesc :
     * roomId : 25526393
     * roomPwd :
     * roomTag : 听歌
     * tagId : 4
     * tagPict : https://pic.miaomiaofm.com/room_tag_new_music.png
     * targetType : 1
     * timeInterval : 5000
     * title : 美美美的房间
     * type : 3
     * uid : 100086
     * valid : true
     */

    private int abChannelType;
    private boolean alarmEnable;
    private String backPic;
    private int calcSumDataIndex;
    private int count;
    private int factor;
    private int isPermitRoom;
    private int isRecom;
    private String meetingName;
    private int officeUser;
    private int onlineNum;
    private long openTime;
    private int operatorStatus;
    private String roomDesc;
    private int roomId;
    private String roomPwd;
    private String roomTag;
    private int tagId;
    private String tagPict;
    private int targetType;
    private int timeInterval;
    private String title;
    private int type;
    private int uid;
    private boolean valid;

    public int getAbChannelType() {
        return abChannelType;
    }

    public void setAbChannelType(int abChannelType) {
        this.abChannelType = abChannelType;
    }

    public boolean isAlarmEnable() {
        return alarmEnable;
    }

    public void setAlarmEnable(boolean alarmEnable) {
        this.alarmEnable = alarmEnable;
    }

    public String getBackPic() {
        return backPic;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }

    public int getCalcSumDataIndex() {
        return calcSumDataIndex;
    }

    public void setCalcSumDataIndex(int calcSumDataIndex) {
        this.calcSumDataIndex = calcSumDataIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public int getIsPermitRoom() {
        return isPermitRoom;
    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }

    public int getIsRecom() {
        return isRecom;
    }

    public void setIsRecom(int isRecom) {
        this.isRecom = isRecom;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public int getOfficeUser() {
        return officeUser;
    }

    public void setOfficeUser(int officeUser) {
        this.officeUser = officeUser;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public long getOpenTime() {
        return openTime;
    }

    public void setOpenTime(long openTime) {
        this.openTime = openTime;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getRoomPwd() {
        return roomPwd;
    }

    public void setRoomPwd(String roomPwd) {
        this.roomPwd = roomPwd;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public int getTargetType() {
        return targetType;
    }

    public void setTargetType(int targetType) {
        this.targetType = targetType;
    }

    public int getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
