package com.tongdaxing.xchat_core.find.bean;

public class VoiceGroupImInfo {

    /**
     * context : 点赞了你的评论
     * createDate : 1553077450000
     * fromAvatar : https://pic.miaomiaofm.com/Fkrj1-MjJDxcmL5arKMhrPbf9O0A?imageslim
     * fromNick : 配对
     * fromUid : 100582
     * id : 325
     * momentId : 49
     * momentUid : 100642
     * playerId : 0
     * readStatus : 1
     * type : 3
     * uid : 100642
     * updateDate : 1551681742000
     */

    private String context;
    private long createDate;
    private String fromAvatar;
    private String fromNick;
    private int fromUid;
    private int id;
    private int momentId;
    private int momentUid;
    private int playerId;
    private int readStatus;
    private int type;
    private int uid;
    private long updateDate;

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getFromAvatar() {
        return fromAvatar;
    }

    public void setFromAvatar(String fromAvatar) {
        this.fromAvatar = fromAvatar;
    }

    public String getFromNick() {
        return fromNick;
    }

    public void setFromNick(String fromNick) {
        this.fromNick = fromNick;
    }

    public int getFromUid() {
        return fromUid;
    }

    public void setFromUid(int fromUid) {
        this.fromUid = fromUid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMomentId() {
        return momentId;
    }

    public void setMomentId(int momentId) {
        this.momentId = momentId;
    }

    public int getMomentUid() {
        return momentUid;
    }

    public void setMomentUid(int momentUid) {
        this.momentUid = momentUid;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }
}
