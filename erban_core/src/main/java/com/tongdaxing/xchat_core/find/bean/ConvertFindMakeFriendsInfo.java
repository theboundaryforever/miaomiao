package com.tongdaxing.xchat_core.find.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class ConvertFindMakeFriendsInfo implements MultiItemEntity {
    private GameAndLoversMatchInfo gameAndLoversMatchInfo;
    private RoomListBeanX roomListBeanX;
    private ColumnListBean columnListBean;
    private WarmAccompanyGroupInfo warmAccompanyGroupInfo;
    @Override
    public String toString() {
        return "ConvertFindMakeFriendsInfo{" +
                "gameAndLoversMatchInfo=" + gameAndLoversMatchInfo +
                ", roomListBeanX=" + roomListBeanX +
                ", columnListBean=" + columnListBean +
                ", warmAccompanyGroupInfo=" + warmAccompanyGroupInfo +
                ", itemType=" + itemType +
                '}';
    }

    public GameAndLoversMatchInfo getGameAndLoversMatchInfo() {
        return gameAndLoversMatchInfo;
    }

    public void setGameAndLoversMatchInfo(GameAndLoversMatchInfo gameAndLoversMatchInfo) {
        this.gameAndLoversMatchInfo = gameAndLoversMatchInfo;
    }

    public WarmAccompanyGroupInfo getWarmAccompanyGroupInfo() {
        return warmAccompanyGroupInfo;
    }

    public void setWarmAccompanyGroupInfo(WarmAccompanyGroupInfo warmAccompanyGroupInfo) {
        this.warmAccompanyGroupInfo = warmAccompanyGroupInfo;
    }

    public ColumnListBean getColumnListBean() {
        return columnListBean;
    }

    public void setColumnListBean(ColumnListBean columnListBean) {
        this.columnListBean = columnListBean;
    }

    public RoomListBeanX getRoomListBeanX() {
        return roomListBeanX;
    }

    public void setRoomListBeanX(RoomListBeanX roomListBeanX) {
        this.roomListBeanX = roomListBeanX;
    }

    private int itemType;

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    @Override
    public int getItemType() {
        return itemType;
    }
}
