package com.tongdaxing.xchat_core.find.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Function:
 * Author: Edward on 2019/3/14
 */
public class PictureListInfo implements Parcelable {
    private long id;
    private int momentId;
    private String url;
    private String layoutType;//本地字段
    private int picCount;////本地字段
    private int picWidth;
    private int picHeight;

    public int getPicWidth() {
        return picWidth;
    }

    public void setPicWidth(int picWidth) {
        this.picWidth = picWidth;
    }

    public int getPicHeight() {
        return picHeight;
    }

    public void setPicHeight(int picHeight) {
        this.picHeight = picHeight;
    }

    @Override
    public String toString() {
        return "PictureListInfo{" +
                "id=" + id +
                ", momentId=" + momentId +
                ", url='" + url + '\'' +
                ", layoutType='" + layoutType + '\'' +
                ", picCount=" + picCount +
                ", picWidth=" + picWidth +
                ", picHeight=" + picHeight +
                '}';
    }

    public String getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }

    public int getPicCount() {
        return picCount;
    }

    public void setPicCount(int picCount) {
        this.picCount = picCount;
    }

    public int getMomentId() {
        return momentId;
    }

    public void setMomentId(int momentId) {
        this.momentId = momentId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public PictureListInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeInt(this.momentId);
        dest.writeString(this.url);
        dest.writeString(this.layoutType);
        dest.writeInt(this.picCount);
        dest.writeInt(this.picWidth);
        dest.writeInt(this.picHeight);
    }

    protected PictureListInfo(Parcel in) {
        this.id = in.readLong();
        this.momentId = in.readInt();
        this.url = in.readString();
        this.layoutType = in.readString();
        this.picCount = in.readInt();
        this.picWidth = in.readInt();
        this.picHeight = in.readInt();
    }

    public static final Creator<PictureListInfo> CREATOR = new Creator<PictureListInfo>() {
        @Override
        public PictureListInfo createFromParcel(Parcel source) {
            return new PictureListInfo(source);
        }

        @Override
        public PictureListInfo[] newArray(int size) {
            return new PictureListInfo[size];
        }
    };
}