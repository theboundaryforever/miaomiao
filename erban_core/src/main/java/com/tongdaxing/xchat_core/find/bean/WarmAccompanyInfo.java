package com.tongdaxing.xchat_core.find.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class WarmAccompanyInfo implements Parcelable {
    private int gender;

    @Override
    public String toString() {
        return "WarmAccompanyInfo{" +
                "gender=" + gender +
                ", avatar='" + avatar + '\'' +
                ", abChannelType=" + abChannelType +
                ", alarmEnable=" + alarmEnable +
                ", backPic='" + backPic + '\'' +
                ", calcSumDataIndex=" + calcSumDataIndex +
                ", count=" + count +
                ", factor=" + factor +
                ", isPermitRoom=" + isPermitRoom +
                ", isRecom=" + isRecom +
                ", meetingName='" + meetingName + '\'' +
                ", officeUser=" + officeUser +
                ", onlineNum=" + onlineNum +
                ", openTime=" + openTime +
                ", operatorStatus=" + operatorStatus +
                ", roomDesc='" + roomDesc + '\'' +
                ", roomId=" + roomId +
                ", roomTag='" + roomTag + '\'' +
                ", tagId=" + tagId +
                ", tagPict='" + tagPict + '\'' +
                ", targetType=" + targetType +
                ", timeInterval=" + timeInterval +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", uid=" + uid +
                ", valid=" + valid +
                ", roomPwd='" + roomPwd + '\'' +
                ", roomNotice='" + roomNotice + '\'' +
                ", badge='" + badge + '\'' +
                '}';
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    /**
     * abChannelType : 1
     * alarmEnable : true
     * backPic :
     * calcSumDataIndex : 0
     * count : 0
     * factor : 0
     * isPermitRoom : 3
     * isRecom : 0
     * meetingName : 9f85a266af324974a83d7a7578c37861
     * officeUser : 1
     * onlineNum : 10
     * openTime : 1545718717000
     * operatorStatus : 1
     * roomDesc :
     * roomId : 60214462
     * roomTag : 女神
     * tagId : 3
     * tagPict : https://pic.miaomiaofm.com/room_tag_new_goddess.png
     * targetType : 1
     * timeInterval : 5000
     * title : 看你妹的
     * type : 3
     * uid : 104446
     * valid : true
     * roomPwd :
     * roomNotice : chhfhgdfgfffghhgfdghffgg
     * badge : https://pic.miaomiaofm.com/FqBObOXxQyP15qFEPTC59VwUTIWa?imageslim
     */
    private String nick;
    private String avatar;
    private int abChannelType;
    private boolean alarmEnable;
    private String backPic;
    private int calcSumDataIndex;
    private int count;
    private int factor;
    private int isPermitRoom;
    private int isRecom;
    private String meetingName;
    private int officeUser;
    private int onlineNum;
    private long openTime;
    private int operatorStatus;
    private String roomDesc;
    private int roomId;
    private String roomTag;
    private int tagId;
    private String tagPict;
    private int targetType;
    private int timeInterval;
    private String title;
    private int type;
    private int uid;
    private boolean valid;
    private String roomPwd;
    private String roomNotice;
    private String badge;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getAbChannelType() {
        return abChannelType;
    }

    public void setAbChannelType(int abChannelType) {
        this.abChannelType = abChannelType;
    }

    public boolean isAlarmEnable() {
        return alarmEnable;
    }

    public void setAlarmEnable(boolean alarmEnable) {
        this.alarmEnable = alarmEnable;
    }

    public String getBackPic() {
        return backPic;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }

    public int getCalcSumDataIndex() {
        return calcSumDataIndex;
    }

    public void setCalcSumDataIndex(int calcSumDataIndex) {
        this.calcSumDataIndex = calcSumDataIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public int getIsPermitRoom() {
        return isPermitRoom;
    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }

    public int getIsRecom() {
        return isRecom;
    }

    public void setIsRecom(int isRecom) {
        this.isRecom = isRecom;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public int getOfficeUser() {
        return officeUser;
    }

    public void setOfficeUser(int officeUser) {
        this.officeUser = officeUser;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public long getOpenTime() {
        return openTime;
    }

    public void setOpenTime(long openTime) {
        this.openTime = openTime;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public int getTargetType() {
        return targetType;
    }

    public void setTargetType(int targetType) {
        this.targetType = targetType;
    }

    public int getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getRoomPwd() {
        return roomPwd;
    }

    public void setRoomPwd(String roomPwd) {
        this.roomPwd = roomPwd;
    }

    public String getRoomNotice() {
        return roomNotice;
    }

    public void setRoomNotice(String roomNotice) {
        this.roomNotice = roomNotice;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public WarmAccompanyInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.gender);
        dest.writeString(this.avatar);
        dest.writeInt(this.abChannelType);
        dest.writeByte(this.alarmEnable ? (byte) 1 : (byte) 0);
        dest.writeString(this.backPic);
        dest.writeInt(this.calcSumDataIndex);
        dest.writeInt(this.count);
        dest.writeInt(this.factor);
        dest.writeInt(this.isPermitRoom);
        dest.writeInt(this.isRecom);
        dest.writeString(this.meetingName);
        dest.writeInt(this.officeUser);
        dest.writeInt(this.onlineNum);
        dest.writeLong(this.openTime);
        dest.writeInt(this.operatorStatus);
        dest.writeString(this.roomDesc);
        dest.writeInt(this.roomId);
        dest.writeString(this.roomTag);
        dest.writeInt(this.tagId);
        dest.writeString(this.tagPict);
        dest.writeInt(this.targetType);
        dest.writeInt(this.timeInterval);
        dest.writeString(this.title);
        dest.writeInt(this.type);
        dest.writeInt(this.uid);
        dest.writeByte(this.valid ? (byte) 1 : (byte) 0);
        dest.writeString(this.roomPwd);
        dest.writeString(this.roomNotice);
        dest.writeString(this.badge);
    }

    protected WarmAccompanyInfo(Parcel in) {
        this.gender = in.readInt();
        this.avatar = in.readString();
        this.abChannelType = in.readInt();
        this.alarmEnable = in.readByte() != 0;
        this.backPic = in.readString();
        this.calcSumDataIndex = in.readInt();
        this.count = in.readInt();
        this.factor = in.readInt();
        this.isPermitRoom = in.readInt();
        this.isRecom = in.readInt();
        this.meetingName = in.readString();
        this.officeUser = in.readInt();
        this.onlineNum = in.readInt();
        this.openTime = in.readLong();
        this.operatorStatus = in.readInt();
        this.roomDesc = in.readString();
        this.roomId = in.readInt();
        this.roomTag = in.readString();
        this.tagId = in.readInt();
        this.tagPict = in.readString();
        this.targetType = in.readInt();
        this.timeInterval = in.readInt();
        this.title = in.readString();
        this.type = in.readInt();
        this.uid = in.readInt();
        this.valid = in.readByte() != 0;
        this.roomPwd = in.readString();
        this.roomNotice = in.readString();
        this.badge = in.readString();
    }

    public static final Creator<WarmAccompanyInfo> CREATOR = new Creator<WarmAccompanyInfo>() {
        @Override
        public WarmAccompanyInfo createFromParcel(Parcel source) {
            return new WarmAccompanyInfo(source);
        }

        @Override
        public WarmAccompanyInfo[] newArray(int size) {
            return new WarmAccompanyInfo[size];
        }
    };
}
