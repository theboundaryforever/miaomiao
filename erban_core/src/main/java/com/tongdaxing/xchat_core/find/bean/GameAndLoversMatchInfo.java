package com.tongdaxing.xchat_core.find.bean;

import java.util.List;

/**
 * Function:组队开黑+情侣速配实体
 * Author: Edward on 2019/3/15
 */
public class GameAndLoversMatchInfo {
    private List<LoverMatchUserHeadInfo> matchBeanList;

    public List<LoverMatchUserHeadInfo> getMatchBeanList() {
        return matchBeanList;
    }

    public void setMatchBeanList(List<LoverMatchUserHeadInfo> matchBeanList) {
        this.matchBeanList = matchBeanList;
    }
}
