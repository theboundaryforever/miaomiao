package com.tongdaxing.xchat_core.find;

import com.tongdaxing.xchat_core.find.bean.CreateRoomLabelInfo;
import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/19
 */
public interface ICreateRoomClient extends ICoreClient {
    String SELECTED_LABEL_ID = "selectedLabelId";
    String LOAD_LABEL_DATA = "addLabelSucceed";
    String CREATE_ROOM_SUCCEED = "createRoomSucceed";
    String START_LOVERS_MATCH = "startLoversMatch";
    String REFRESH_SQUARE_NOTIFY = "refreshSquareNotify";
    String FINISH_SQUARE_NOTIFY = "finishSquareNotify";

    void finishSquareNotify();

    void refreshSquareNotify(int countDownTime);

    void startLoversMatch();

    void createRoomSucceed(int tagId, int type);

    void addLabelSucceed(int roomType, List<CreateRoomLabelInfo.TagListBean> listBeans);

    void selectedLabelId(int labelId);
}
