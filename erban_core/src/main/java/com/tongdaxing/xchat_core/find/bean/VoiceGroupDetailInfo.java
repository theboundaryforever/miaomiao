package com.tongdaxing.xchat_core.find.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public class VoiceGroupDetailInfo implements Parcelable {

    public String getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }

    /**
     * avatar : https://pic.miaomiaofm.com/FpuOvp7egcHAO15aFn4g9Z7vGQ09?imageslim
     * context : ggcf
     * createDate : 1552372409000
     * id : 51
     * isLike : false
     * likeNum : 0
     * nick : 偏偏结果冲凉先把他的房间的房间
     * picList : []
     * playerList : [{"avatar":"https://pic.miaomiaofm.com/FpuOvp7egcHAO15aFn4g9Z7vGQ09?imageslim","context":"vbnb","createDate":1552397596000,"gender":2,"id":24,"isLike":false,"likeNum":0,"momentId":51,"momentUid":100642,"nick":"偏偏结果冲凉先把他的房间的房间","status":1,"type":1,"uid":100642},{"avatar":"https://pic.miaomiaofm.com/FpuOvp7egcHAO15aFn4g9Z7vGQ09?imageslim","context":"vggcc","createDate":1552397709000,"gender":2,"id":25,"isLike":false,"likeNum":0,"momentId":51,"momentUid":100642,"nick":"偏偏结果冲凉先把他的房间的房间","status":1,"type":1,"uid":100642}]
     * playerNum : 2
     * status : 1
     * uid : 100642
     */

    private String layoutType;
    private String avatar;
    private String context;
    private long createDate;
    private int id;
    private int gender;
    private boolean isLike;
    private boolean isFans;
    private int likeNum;
    private String nick;
    private int playerNum;
    private int status;
    private int uid;
    private List<PictureListInfo> picList;
    private List<PlayerListBean> playerList;

    @Override
    public String toString() {
        return "VoiceGroupDetailInfo{" +
                "layoutType='" + layoutType + '\'' +
                ", avatar='" + avatar + '\'' +
                ", context='" + context + '\'' +
                ", createDate=" + createDate +
                ", id=" + id +
                ", gender=" + gender +
                ", isLike=" + isLike +
                ", isFans=" + isFans +
                ", likeNum=" + likeNum +
                ", nick='" + nick + '\'' +
                ", playerNum=" + playerNum +
                ", status=" + status +
                ", uid=" + uid +
                ", picList=" + picList +
                ", playerList=" + playerList +
                '}';
    }

    public boolean isFans() {
        return isFans;
    }

    public void setFans(boolean fans) {
        isFans = fans;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isIsLike() {
        return isLike;
    }

    public void setIsLike(boolean isLike) {
        this.isLike = isLike;
    }

    public int getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(int likeNum) {
        this.likeNum = likeNum;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getPlayerNum() {
        return playerNum;
    }

    public void setPlayerNum(int playerNum) {
        this.playerNum = playerNum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public List<PictureListInfo> getPicList() {
        return picList;
    }

    public void setPicList(List<PictureListInfo> picList) {
        this.picList = picList;
    }

    public List<PlayerListBean> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<PlayerListBean> playerList) {
        this.playerList = playerList;
    }

    public static class PlayerListBean implements Parcelable {
        /**
         * avatar : https://pic.miaomiaofm.com/FpuOvp7egcHAO15aFn4g9Z7vGQ09?imageslim
         * context : vbnb
         * createDate : 1552397596000
         * gender : 2
         * id : 24
         * isLike : false
         * likeNum : 0
         * momentId : 51
         * momentUid : 100642
         * nick : 偏偏结果冲凉先把他的房间的房间
         * status : 1
         * type : 1
         * uid : 100642
         */
        private long playerUid;
        private String playerNick;
        private String avatar;
        private String context;
        private long createDate;
        private int gender;
        private int id;
        private boolean isLike;
        private int likeNum;
        private int momentId;
        private int momentUid;
        private String nick;
        private int status;
        private int type;
        private int uid;

        public long getPlayerUid() {
            return playerUid;
        }

        public void setPlayerUid(long playerUid) {
            this.playerUid = playerUid;
        }

        public String getPlayerNick() {
            return playerNick;
        }

        public void setPlayerNick(String playerNick) {
            this.playerNick = playerNick;
        }

        @Override
        public String toString() {
            return "PlayerListBean{" +
                    "playerUid=" + playerUid +
                    ", playerNick='" + playerNick + '\'' +
                    ", avatar='" + avatar + '\'' +
                    ", context='" + context + '\'' +
                    ", createDate=" + createDate +
                    ", gender=" + gender +
                    ", id=" + id +
                    ", isLike=" + isLike +
                    ", likeNum=" + likeNum +
                    ", momentId=" + momentId +
                    ", momentUid=" + momentUid +
                    ", nick='" + nick + '\'' +
                    ", status=" + status +
                    ", type=" + type +
                    ", uid=" + uid +
                    '}';
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getContext() {
            return context;
        }

        public void setContext(String context) {
            this.context = context;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isIsLike() {
            return isLike;
        }

        public void setIsLike(boolean isLike) {
            this.isLike = isLike;
        }

        public int getLikeNum() {
            return likeNum;
        }

        public void setLikeNum(int likeNum) {
            this.likeNum = likeNum;
        }

        public int getMomentId() {
            return momentId;
        }

        public void setMomentId(int momentId) {
            this.momentId = momentId;
        }

        public int getMomentUid() {
            return momentUid;
        }

        public void setMomentUid(int momentUid) {
            this.momentUid = momentUid;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.playerUid);
            dest.writeString(this.playerNick);
            dest.writeString(this.avatar);
            dest.writeString(this.context);
            dest.writeLong(this.createDate);
            dest.writeInt(this.gender);
            dest.writeInt(this.id);
            dest.writeByte(this.isLike ? (byte) 1 : (byte) 0);
            dest.writeInt(this.likeNum);
            dest.writeInt(this.momentId);
            dest.writeInt(this.momentUid);
            dest.writeString(this.nick);
            dest.writeInt(this.status);
            dest.writeInt(this.type);
            dest.writeInt(this.uid);
        }

        public PlayerListBean() {
        }

        protected PlayerListBean(Parcel in) {
            this.playerUid = in.readLong();
            this.playerNick = in.readString();
            this.avatar = in.readString();
            this.context = in.readString();
            this.createDate = in.readLong();
            this.gender = in.readInt();
            this.id = in.readInt();
            this.isLike = in.readByte() != 0;
            this.likeNum = in.readInt();
            this.momentId = in.readInt();
            this.momentUid = in.readInt();
            this.nick = in.readString();
            this.status = in.readInt();
            this.type = in.readInt();
            this.uid = in.readInt();
        }

        public static final Creator<PlayerListBean> CREATOR = new Creator<PlayerListBean>() {
            @Override
            public PlayerListBean createFromParcel(Parcel source) {
                return new PlayerListBean(source);
            }

            @Override
            public PlayerListBean[] newArray(int size) {
                return new PlayerListBean[size];
            }
        };
    }

    public VoiceGroupDetailInfo() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.layoutType);
        dest.writeString(this.avatar);
        dest.writeString(this.context);
        dest.writeLong(this.createDate);
        dest.writeInt(this.id);
        dest.writeInt(this.gender);
        dest.writeByte(this.isLike ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isFans ? (byte) 1 : (byte) 0);
        dest.writeInt(this.likeNum);
        dest.writeString(this.nick);
        dest.writeInt(this.playerNum);
        dest.writeInt(this.status);
        dest.writeInt(this.uid);
        dest.writeList(this.picList);
        dest.writeTypedList(this.playerList);
    }

    protected VoiceGroupDetailInfo(Parcel in) {
        this.layoutType = in.readString();
        this.avatar = in.readString();
        this.context = in.readString();
        this.createDate = in.readLong();
        this.id = in.readInt();
        this.gender = in.readInt();
        this.isLike = in.readByte() != 0;
        this.isFans = in.readByte() != 0;
        this.likeNum = in.readInt();
        this.nick = in.readString();
        this.playerNum = in.readInt();
        this.status = in.readInt();
        this.uid = in.readInt();
        this.picList = new ArrayList<PictureListInfo>();
        in.readList(this.picList, PictureListInfo.class.getClassLoader());
        this.playerList = in.createTypedArrayList(PlayerListBean.CREATOR);
    }

    public static final Creator<VoiceGroupDetailInfo> CREATOR = new Creator<VoiceGroupDetailInfo>() {
        @Override
        public VoiceGroupDetailInfo createFromParcel(Parcel source) {
            return new VoiceGroupDetailInfo(source);
        }

        @Override
        public VoiceGroupDetailInfo[] newArray(int size) {
            return new VoiceGroupDetailInfo[size];
        }
    };
}
