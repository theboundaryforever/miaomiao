package com.tongdaxing.xchat_core.find.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.tongdaxing.xchat_core.room.bean.RoomStatus;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

/**
 * Function:
 * Author: Edward on 2019/3/30
 */
public class DatumPageInfo implements MultiItemEntity {
    private RoomStatus roomStatus;
    private UserInfo userInfo;
    private int itemType;

    public RoomStatus getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(RoomStatus roomStatus) {
        this.roomStatus = roomStatus;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}
