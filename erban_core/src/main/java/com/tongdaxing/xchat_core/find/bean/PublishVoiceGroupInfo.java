package com.tongdaxing.xchat_core.find.bean;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/11
 */
public class PublishVoiceGroupInfo {

    /**
     * avatar : https://pic.miaomiaofm.com/FpuOvp7egcHAO15aFn4g9Z7vGQ09?imageslim
     * context : ghb
     * createDate : 1552299633409
     * gender : 2
     * id : 32
     * likeNum : 0
     * nick : 偏偏结果冲凉先把他的房间的房间
     * picList : []
     * playerNum : 0
     * status : 1
     * uid : 100642
     */

    private String avatar;
    private String context;
    private long createDate;
    private int gender;
    private int id;
    private int likeNum;
    private String nick;
    private int playerNum;
    private int status;
    private int uid;
    private List<PictureListInfo> picList;
    private List<String> illegalPicList;//违规图片列表

    public List<String> getIllegalPicList() {
        return illegalPicList;
    }

    public void setIllegalPicList(List<String> illegalPicList) {
        this.illegalPicList = illegalPicList;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(int likeNum) {
        this.likeNum = likeNum;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getPlayerNum() {
        return playerNum;
    }

    public void setPlayerNum(int playerNum) {
        this.playerNum = playerNum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public List<PictureListInfo> getPicList() {
        return picList;
    }

    public void setPicList(List<PictureListInfo> picList) {
        this.picList = picList;
    }

    @Override
    public String toString() {
        return "PublishVoiceGroupInfo{" +
                "avatar='" + avatar + '\'' +
                ", context='" + context + '\'' +
                ", createDate=" + createDate +
                ", gender=" + gender +
                ", id=" + id +
                ", likeNum=" + likeNum +
                ", nick='" + nick + '\'' +
                ", playerNum=" + playerNum +
                ", status=" + status +
                ", uid=" + uid +
                ", picList=" + picList +
                ", illegalPicList=" + illegalPicList +
                '}';
    }
}
