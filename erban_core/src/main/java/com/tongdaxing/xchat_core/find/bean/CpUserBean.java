package com.tongdaxing.xchat_core.find.bean;

/**
 * Function:
 * Author: Edward on 2019/3/29
 */
public class CpUserBean {
    /**
     * avatar : https://pic.miaomiaofm.com/FnZ8wpBo26hjFfx9mriGV0Hr-sUK?imageslim
     * erbanNo : 2048113
     * nick : js-学喵叫
     * uid : 100000
     */

    private String avatar;
    private long erbanNo;
    private String nick;
    private long uid;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "{" +
                "avatar='" + avatar + '\'' +
                ", erbanNo=" + erbanNo +
                ", nick='" + nick + '\'' +
                ", uid=" + uid +
                '}';
    }
}
