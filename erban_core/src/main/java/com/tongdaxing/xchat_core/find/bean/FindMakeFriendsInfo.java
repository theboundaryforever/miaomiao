package com.tongdaxing.xchat_core.find.bean;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/14
 */
public class FindMakeFriendsInfo implements Parcelable {
    private List<CharmListBean> charmList;//暂时无用数据
    private List<ColumnListBean> columnList;//自定义列表
    private List<WarmAccompanyListBean> femaleList;//暖心陪伴，女
    private List<WarmAccompanyListBean> manList;//暖心陪伴，男
    private List<RoomListBeanX> roomList;//聊天交友
    private List<LoverMatchUserHeadInfo> mateList;//情侣速配，切换

    public List<LoverMatchUserHeadInfo> getMateList() {
        return mateList;
    }

    public void setMateList(List<LoverMatchUserHeadInfo> mateList) {
        this.mateList = mateList;
    }

    @Override
    public String toString() {
        return "FindMakeFriendsInfo{" +
                "charmList=" + charmList +
                ", columnList=" + columnList +
                ", femaleList=" + femaleList +
                ", manList=" + manList +
                ", roomList=" + roomList +
                ", mateList=" + mateList +
                '}';
    }

    public List<CharmListBean> getCharmList() {
        return charmList;
    }

    public void setCharmList(List<CharmListBean> charmList) {
        this.charmList = charmList;
    }

    public List<ColumnListBean> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<ColumnListBean> columnList) {
        this.columnList = columnList;
    }

    public List<WarmAccompanyListBean> getFemaleList() {
        return femaleList;
    }

    public void setFemaleList(List<WarmAccompanyListBean> femaleList) {
        this.femaleList = femaleList;
    }

    public List<WarmAccompanyListBean> getManList() {
        return manList;
    }

    public void setManList(List<WarmAccompanyListBean> manList) {
        this.manList = manList;
    }

    public List<RoomListBeanX> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<RoomListBeanX> roomList) {
        this.roomList = roomList;
    }


    public static class CharmListBean {
        /**
         * avatar : https://pic.miaomiaofm.com/Fjkx9uUQAmICDU7oStouD1Spz6hc?imageslim
         * birth : 746467200000
         * carName :
         * carUrl :
         * chargeNum : 1
         * charmLevel : 35
         * createTime : 1537858721000
         * defUser : 1
         * erbanNo : 88888
         * experLevel : 48
         * fansNum : 7
         * findNewUsers : 0
         * followNum : 8
         * gender : 1
         * hasPrettyErbanNo : false
         * hasRegPacket : false
         * headwearName :
         * headwearUrl :
         * nick : 请你吃饭监控室，km囖囖了空空
         * phone :
         * privatePhoto : [{"createTime":1550653324000,"photoUrl":"https://pic.miaomiaofm.com/FnhMc1ydS1N4yH6vb1xoFTC9uiS7?imageslim","pid":546,"seqNo":1},{"createTime":1550653260000,"photoUrl":"https://pic.miaomiaofm.com/FhueRb8OsvLP0FKqkTV8v1GEFZkk?imageslim","pid":542,"seqNo":1}]
         * realNameAudit : true
         * uid : 100738
         * userDesc : 今个星期了？？？？？？？？？？？？？？？？？？？？？？&/$^O^.'%,(ฅ>ω<*ฅ)~✘SSSG hhhhhhhh
         * userVoice : https://pic.miaomiaofm.com/100738_1548061535432_10.aac
         * voiceCard : {"auxiliaryTone":"[{\"toneName\":\"温婉电台音\",\"tonePro\":8},{\"toneName\":\"傲娇学长音\",\"tonePro\":6},{\"toneName\":\"古风公子音\",\"tonePro\":6}]","auxiliaryToneList":[{"toneName":"温婉电台音","tonePro":8},{"toneName":"傲娇学长音","tonePro":6},{"toneName":"古风公子音","tonePro":6}],"avatar":"https://pic.miaomiaofm.com/Fjkx9uUQAmICDU7oStouD1Spz6hc?imageslim","bestMatch":"冷艳女王音","charmLevel":3,"createDate":1551693713000,"masterTone":"阳光治愈音","masterTonePro":80,"nick":"请你吃饭监控室，km囖囖了空空","similarPerson":"黄子韬","uid":100738,"url":"https://pic.miaomiaofm.com/Fu3d2vUPMwSf9tTgKp9mgbDj5PcF?imageslim","voiceScore":94}
         * voiceDura : 6
         * voiceHide : false
         */

        private String avatar;
        private long birth;
        private String carName;
        private String carUrl;
        private int chargeNum;
        private int charmLevel;
        private long createTime;
        private int defUser;
        private int erbanNo;
        private int experLevel;
        private int fansNum;
        private int findNewUsers;
        private int followNum;
        private int gender;
        private boolean hasPrettyErbanNo;
        private boolean hasRegPacket;
        private String headwearName;
        private String headwearUrl;
        private String nick;
        private String phone;
        private boolean realNameAudit;
        private int uid;
        private String userDesc;
        private String userVoice;
        private VoiceCardBean voiceCard;
        private int voiceDura;
        private boolean voiceHide;
        private List<PrivatePhotoBean> privatePhoto;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public long getBirth() {
            return birth;
        }

        public void setBirth(long birth) {
            this.birth = birth;
        }

        public String getCarName() {
            return carName;
        }

        public void setCarName(String carName) {
            this.carName = carName;
        }

        public String getCarUrl() {
            return carUrl;
        }

        public void setCarUrl(String carUrl) {
            this.carUrl = carUrl;
        }

        public int getChargeNum() {
            return chargeNum;
        }

        public void setChargeNum(int chargeNum) {
            this.chargeNum = chargeNum;
        }

        public int getCharmLevel() {
            return charmLevel;
        }

        public void setCharmLevel(int charmLevel) {
            this.charmLevel = charmLevel;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public int getDefUser() {
            return defUser;
        }

        public void setDefUser(int defUser) {
            this.defUser = defUser;
        }

        public int getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(int erbanNo) {
            this.erbanNo = erbanNo;
        }

        public int getExperLevel() {
            return experLevel;
        }

        public void setExperLevel(int experLevel) {
            this.experLevel = experLevel;
        }

        public int getFansNum() {
            return fansNum;
        }

        public void setFansNum(int fansNum) {
            this.fansNum = fansNum;
        }

        public int getFindNewUsers() {
            return findNewUsers;
        }

        public void setFindNewUsers(int findNewUsers) {
            this.findNewUsers = findNewUsers;
        }

        public int getFollowNum() {
            return followNum;
        }

        public void setFollowNum(int followNum) {
            this.followNum = followNum;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public boolean isHasPrettyErbanNo() {
            return hasPrettyErbanNo;
        }

        public void setHasPrettyErbanNo(boolean hasPrettyErbanNo) {
            this.hasPrettyErbanNo = hasPrettyErbanNo;
        }

        public boolean isHasRegPacket() {
            return hasRegPacket;
        }

        public void setHasRegPacket(boolean hasRegPacket) {
            this.hasRegPacket = hasRegPacket;
        }

        public String getHeadwearName() {
            return headwearName;
        }

        public void setHeadwearName(String headwearName) {
            this.headwearName = headwearName;
        }

        public String getHeadwearUrl() {
            return headwearUrl;
        }

        public void setHeadwearUrl(String headwearUrl) {
            this.headwearUrl = headwearUrl;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public boolean isRealNameAudit() {
            return realNameAudit;
        }

        public void setRealNameAudit(boolean realNameAudit) {
            this.realNameAudit = realNameAudit;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public String getUserDesc() {
            return userDesc;
        }

        public void setUserDesc(String userDesc) {
            this.userDesc = userDesc;
        }

        public String getUserVoice() {
            return userVoice;
        }

        public void setUserVoice(String userVoice) {
            this.userVoice = userVoice;
        }

        public VoiceCardBean getVoiceCard() {
            return voiceCard;
        }

        public void setVoiceCard(VoiceCardBean voiceCard) {
            this.voiceCard = voiceCard;
        }

        public int getVoiceDura() {
            return voiceDura;
        }

        public void setVoiceDura(int voiceDura) {
            this.voiceDura = voiceDura;
        }

        public boolean isVoiceHide() {
            return voiceHide;
        }

        public void setVoiceHide(boolean voiceHide) {
            this.voiceHide = voiceHide;
        }

        public List<PrivatePhotoBean> getPrivatePhoto() {
            return privatePhoto;
        }

        public void setPrivatePhoto(List<PrivatePhotoBean> privatePhoto) {
            this.privatePhoto = privatePhoto;
        }

        public static class VoiceCardBean {
            /**
             * auxiliaryTone : [{"toneName":"温婉电台音","tonePro":8},{"toneName":"傲娇学长音","tonePro":6},{"toneName":"古风公子音","tonePro":6}]
             * auxiliaryToneList : [{"toneName":"温婉电台音","tonePro":8},{"toneName":"傲娇学长音","tonePro":6},{"toneName":"古风公子音","tonePro":6}]
             * avatar : https://pic.miaomiaofm.com/Fjkx9uUQAmICDU7oStouD1Spz6hc?imageslim
             * bestMatch : 冷艳女王音
             * charmLevel : 3
             * createDate : 1551693713000
             * masterTone : 阳光治愈音
             * masterTonePro : 80
             * nick : 请你吃饭监控室，km囖囖了空空
             * similarPerson : 黄子韬
             * uid : 100738
             * url : https://pic.miaomiaofm.com/Fu3d2vUPMwSf9tTgKp9mgbDj5PcF?imageslim
             * voiceScore : 94
             */

            private String auxiliaryTone;
            private String avatar;
            private String bestMatch;
            private int charmLevel;
            private long createDate;
            private String masterTone;
            private int masterTonePro;
            private String nick;
            private String similarPerson;
            private int uid;
            private String url;
            private int voiceScore;
            private List<AuxiliaryToneListBean> auxiliaryToneList;

            public String getAuxiliaryTone() {
                return auxiliaryTone;
            }

            public void setAuxiliaryTone(String auxiliaryTone) {
                this.auxiliaryTone = auxiliaryTone;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getBestMatch() {
                return bestMatch;
            }

            public void setBestMatch(String bestMatch) {
                this.bestMatch = bestMatch;
            }

            public int getCharmLevel() {
                return charmLevel;
            }

            public void setCharmLevel(int charmLevel) {
                this.charmLevel = charmLevel;
            }

            public long getCreateDate() {
                return createDate;
            }

            public void setCreateDate(long createDate) {
                this.createDate = createDate;
            }

            public String getMasterTone() {
                return masterTone;
            }

            public void setMasterTone(String masterTone) {
                this.masterTone = masterTone;
            }

            public int getMasterTonePro() {
                return masterTonePro;
            }

            public void setMasterTonePro(int masterTonePro) {
                this.masterTonePro = masterTonePro;
            }

            public String getNick() {
                return nick;
            }

            public void setNick(String nick) {
                this.nick = nick;
            }

            public String getSimilarPerson() {
                return similarPerson;
            }

            public void setSimilarPerson(String similarPerson) {
                this.similarPerson = similarPerson;
            }

            public int getUid() {
                return uid;
            }

            public void setUid(int uid) {
                this.uid = uid;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public int getVoiceScore() {
                return voiceScore;
            }

            public void setVoiceScore(int voiceScore) {
                this.voiceScore = voiceScore;
            }

            public List<AuxiliaryToneListBean> getAuxiliaryToneList() {
                return auxiliaryToneList;
            }

            public void setAuxiliaryToneList(List<AuxiliaryToneListBean> auxiliaryToneList) {
                this.auxiliaryToneList = auxiliaryToneList;
            }

            public static class AuxiliaryToneListBean {
                /**
                 * toneName : 温婉电台音
                 * tonePro : 8
                 */

                private String toneName;
                private int tonePro;

                public String getToneName() {
                    return toneName;
                }

                public void setToneName(String toneName) {
                    this.toneName = toneName;
                }

                public int getTonePro() {
                    return tonePro;
                }

                public void setTonePro(int tonePro) {
                    this.tonePro = tonePro;
                }
            }
        }

        public static class PrivatePhotoBean {
            /**
             * createTime : 1550653324000
             * photoUrl : https://pic.miaomiaofm.com/FnhMc1ydS1N4yH6vb1xoFTC9uiS7?imageslim
             * pid : 546
             * seqNo : 1
             */

            private long createTime;
            private String photoUrl;
            private int pid;
            private int seqNo;

            public long getCreateTime() {
                return createTime;
            }

            public void setCreateTime(long createTime) {
                this.createTime = createTime;
            }

            public String getPhotoUrl() {
                return photoUrl;
            }

            public void setPhotoUrl(String photoUrl) {
                this.photoUrl = photoUrl;
            }

            public int getPid() {
                return pid;
            }

            public void setPid(int pid) {
                this.pid = pid;
            }

            public int getSeqNo() {
                return seqNo;
            }

            public void setSeqNo(int seqNo) {
                this.seqNo = seqNo;
            }
        }
    }

    public FindMakeFriendsInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.charmList);
        dest.writeList(this.columnList);
        dest.writeList(this.femaleList);
        dest.writeList(this.manList);
        dest.writeList(this.roomList);
    }

    protected FindMakeFriendsInfo(Parcel in) {
        this.charmList = new ArrayList<CharmListBean>();
        in.readList(this.charmList, CharmListBean.class.getClassLoader());
        this.columnList = new ArrayList<ColumnListBean>();
        in.readList(this.columnList, ColumnListBean.class.getClassLoader());
        this.femaleList = new ArrayList<WarmAccompanyListBean>();
        in.readList(this.femaleList, WarmAccompanyListBean.class.getClassLoader());
        this.manList = new ArrayList<WarmAccompanyListBean>();
        in.readList(this.manList, WarmAccompanyListBean.class.getClassLoader());
        this.roomList = new ArrayList<RoomListBeanX>();
        in.readList(this.roomList, RoomListBeanX.class.getClassLoader());
    }

    public static final Creator<FindMakeFriendsInfo> CREATOR = new Creator<FindMakeFriendsInfo>() {
        @Override
        public FindMakeFriendsInfo createFromParcel(Parcel source) {
            return new FindMakeFriendsInfo(source);
        }

        @Override
        public FindMakeFriendsInfo[] newArray(int size) {
            return new FindMakeFriendsInfo[size];
        }
    };
}
