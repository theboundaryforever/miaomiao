package com.tongdaxing.xchat_core.find.bean;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class RoomListBeanX {
    /**
     * alarmEnable : true
     * avatar : https://pic.miaomiaofm.com/FnVlwElYRrAfoga8cC3mt371JF1G?imageslim
     * backPic : 0
     * calcSumDataIndex : 0
     * count : 0
     * erbanNo : 2048113
     * gender : 1
     * isPermitRoom : 2
     * isRecom : 0
     * nick : 好好学习
     * onlineNum : 20
     * operatorStatus : 1
     * recomSeq : 100
     * roomDesc :
     * roomId : 60175351
     * roomTag : 交友
     * score : 6
     * tagId : 8
     * tagPict : https://pic.miaomiaofm.com/room_tag_new_friend.png
     * targetType : 1
     * timeInterval : 5000
     * title : xjdjdk的房间
     * type : 3
     * uid : 100016
     */
    private String showTitle;//本地字段
    private boolean alarmEnable;
    private String avatar;
    private String backPic;
    private int calcSumDataIndex;
    private String roomPwd;
    private String badge;
    private int count;
    private long erbanNo;
    private int gender;
    private int isPermitRoom;
    private int isRecom;
    private String nick;
    private int onlineNum;
    private int operatorStatus;
    private int recomSeq;
    private String roomDesc;
    private long roomId;
    private String roomTag;
    private double score;
    private int tagId;
    private String tagPict;
    private int targetType;
    private int timeInterval;
    private String title;
    private int type;
    private long uid;

    @Override
    public String toString() {
        return "RoomListBeanX{" +
                "showTitle=" + showTitle +
                ", alarmEnable=" + alarmEnable +
                ", avatar='" + avatar + '\'' +
                ", backPic='" + backPic + '\'' +
                ", calcSumDataIndex=" + calcSumDataIndex +
                ", roomPwd='" + roomPwd + '\'' +
                ", badge='" + badge + '\'' +
                ", count=" + count +
                ", erbanNo=" + erbanNo +
                ", gender=" + gender +
                ", isPermitRoom=" + isPermitRoom +
                ", isRecom=" + isRecom +
                ", nick='" + nick + '\'' +
                ", onlineNum=" + onlineNum +
                ", operatorStatus=" + operatorStatus +
                ", recomSeq=" + recomSeq +
                ", roomDesc='" + roomDesc + '\'' +
                ", roomId=" + roomId +
                ", roomTag='" + roomTag + '\'' +
                ", score=" + score +
                ", tagId=" + tagId +
                ", tagPict='" + tagPict + '\'' +
                ", targetType=" + targetType +
                ", timeInterval=" + timeInterval +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", uid=" + uid +
                '}';
    }

    public String getShowTitle() {
        return showTitle;
    }

    public void setShowTitle(String showTitle) {
        this.showTitle = showTitle;
    }

    public String getRoomPwd() {
        return roomPwd;
    }

    public void setRoomPwd(String roomPwd) {
        this.roomPwd = roomPwd;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public boolean isAlarmEnable() {
        return alarmEnable;
    }

    public void setAlarmEnable(boolean alarmEnable) {
        this.alarmEnable = alarmEnable;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBackPic() {
        return backPic;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }

    public int getCalcSumDataIndex() {
        return calcSumDataIndex;
    }

    public void setCalcSumDataIndex(int calcSumDataIndex) {
        this.calcSumDataIndex = calcSumDataIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getIsPermitRoom() {
        return isPermitRoom;
    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }

    public int getIsRecom() {
        return isRecom;
    }

    public void setIsRecom(int isRecom) {
        this.isRecom = isRecom;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public int getRecomSeq() {
        return recomSeq;
    }

    public void setRecomSeq(int recomSeq) {
        this.recomSeq = recomSeq;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public int getTargetType() {
        return targetType;
    }

    public void setTargetType(int targetType) {
        this.targetType = targetType;
    }

    public int getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(int timeInterval) {
        this.timeInterval = timeInterval;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}