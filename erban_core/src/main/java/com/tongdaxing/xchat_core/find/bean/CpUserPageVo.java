package com.tongdaxing.xchat_core.find.bean;

/**
 * Function:
 * Author: Edward on 2019/3/28
 */
public class CpUserPageVo {
    public static final int CP_TYPE_FORMAL = 1;
    public static final int CP_TYPE_ONE_WEEK = 2;
    private int giftId;
    private int closeLevel;
    private int create_date;
    private String picUrl;
    private int cpGiftLevel;
    private CpUserInfoVo inviteUser;
    private CpUserInfoVo recUser;
    private String vggUrl;//svga web地址
    private int cpType;//cp类型 1正式cp 2一周cp
    private String inviteSign;//邀请人sign
    private String recSign;//接收人sign

    public int getCpGiftLevel() {
        return cpGiftLevel;
    }

    public void setCpGiftLevel(int cpGiftLevel) {
        this.cpGiftLevel = cpGiftLevel;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public int getCloseLevel() {
        return closeLevel;
    }

    public void setCloseLevel(int closeLevel) {
        this.closeLevel = closeLevel;
    }

    public String getRecSign() {
        return recSign;
    }

    public void setRecSign(String recSign) {
        this.recSign = recSign;
    }

    public int getCreate_date() {

        return create_date;
    }

    public void setCreate_date(int create_date) {
        this.create_date = create_date;
    }

    public CpUserInfoVo getInviteUser() {
        return inviteUser;
    }

    public void setInviteUser(CpUserInfoVo inviteUser) {
        this.inviteUser = inviteUser;
    }

    public CpUserInfoVo getRecUser() {
        return recUser;
    }

    public void setRecUser(CpUserInfoVo recUser) {
        this.recUser = recUser;
    }

    public String getVggUrl() {
        return vggUrl;
    }

    public void setVggUrl(String vggUrl) {
        this.vggUrl = vggUrl;
    }

    public int getCpType() {
        return cpType;
    }

    public void setCpType(int cpType) {
        this.cpType = cpType;
    }

    public String getInviteSign() {
        return inviteSign;
    }

    public void setInviteSign(String inviteSign) {
        this.inviteSign = inviteSign;
    }

    public static class CpUserInfoVo {
        private long uid;
        private long erbanNo;
        private String nick;
        private String avatar;
        private int experLevel;
        private int charmLevel;

        public long getUid() {
            return uid;
        }

        public void setUid(long uid) {
            this.uid = uid;
        }

        public long getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(long erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getExperLevel() {
            return experLevel;
        }

        public void setExperLevel(int experLevel) {
            this.experLevel = experLevel;
        }

        public int getCharmLevel() {
            return charmLevel;
        }

        public void setCharmLevel(int charmLevel) {
            this.charmLevel = charmLevel;
        }
    }
}
