package com.tongdaxing.xchat_core.find.bean;

/**
 * Function:
 * Author: Edward on 2019/3/13
 */
public class RecommendModuleInfo {

    /**
     * app : xchat
     * appVersion : 1.0.8
     * avatar : https://pic.miaomiaofm.com/FpuOvp7egcHAO15aFn4g9Z7vGQ09?imageslim
     * birth : 27273600000
     * channel : mm
     * channelType : 1
     * createTime : 1535336168000
     * defUser : 1
     * deviceId : c42456f1-3a3d-3a5d-9d3a-4a98e45d9b01
     * erbanNo : 11
     * fansNum : 14
     * followNum : 8
     * gender : 2
     * hasPrettyErbanNo : false
     * ispType : 4
     * model : vivo Z1
     * netType : 2
     * nick : 偏偏结果冲凉先把他的房间的房间
     * nobleId : 0
     * os : android
     * osversion : 8.1.0
     * phone : 11
     * uid : 100642
     * updateTime : 1552033813000
     * userDesc : 呵呵呵呵李宁now哦哦，哦哦哦哦，哦哦哦哦哦，哦哦哦噢噢噢哦哦哦哦，，哦哦，哦哦，哦哦⊙∀⊙！⊙∀⊙
     * userVoice : https://pic.miaomiaofm.com/100642_1548055125980_48.aac
     * voiceDura : 6
     * voiceHide : false
     * withdrawStatus : 0
     */

    private String app;
    private String appVersion;
    private String avatar;
    private long birth;
    private String channel;
    private int channelType;
    private long createTime;
    private int defUser;
    private String deviceId;
    private int erbanNo;
    private int fansNum;
    private int followNum;
    private int gender;
    private boolean hasPrettyErbanNo;
    private String ispType;
    private String model;
    private String netType;
    private String nick;
    private int nobleId;
    private String os;
    private String osversion;
    private String phone;
    private int uid;
    private long updateTime;
    private String userDesc;
    private String userVoice;
    private int voiceDura;
    private boolean voiceHide;
    private int withdrawStatus;

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getBirth() {
        return birth;
    }

    public void setBirth(long birth) {
        this.birth = birth;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public int getChannelType() {
        return channelType;
    }

    public void setChannelType(int channelType) {
        this.channelType = channelType;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getDefUser() {
        return defUser;
    }

    public void setDefUser(int defUser) {
        this.defUser = defUser;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(int erbanNo) {
        this.erbanNo = erbanNo;
    }

    public int getFansNum() {
        return fansNum;
    }

    public void setFansNum(int fansNum) {
        this.fansNum = fansNum;
    }

    public int getFollowNum() {
        return followNum;
    }

    public void setFollowNum(int followNum) {
        this.followNum = followNum;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public boolean isHasPrettyErbanNo() {
        return hasPrettyErbanNo;
    }

    public void setHasPrettyErbanNo(boolean hasPrettyErbanNo) {
        this.hasPrettyErbanNo = hasPrettyErbanNo;
    }

    public String getIspType() {
        return ispType;
    }

    public void setIspType(String ispType) {
        this.ispType = ispType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNetType() {
        return netType;
    }

    public void setNetType(String netType) {
        this.netType = netType;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getNobleId() {
        return nobleId;
    }

    public void setNobleId(int nobleId) {
        this.nobleId = nobleId;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public String getUserDesc() {
        return userDesc;
    }

    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    public String getUserVoice() {
        return userVoice;
    }

    public void setUserVoice(String userVoice) {
        this.userVoice = userVoice;
    }

    public int getVoiceDura() {
        return voiceDura;
    }

    public void setVoiceDura(int voiceDura) {
        this.voiceDura = voiceDura;
    }

    public boolean isVoiceHide() {
        return voiceHide;
    }

    public void setVoiceHide(boolean voiceHide) {
        this.voiceHide = voiceHide;
    }

    public int getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(int withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }
}
