package com.tongdaxing.xchat_core.find.bean;

import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class ConvertPlayGameInfo {
    private List<WarmAccompanyInfo> roomList;
    private List<PlayGameTabInfo> tagList;

    public List<WarmAccompanyInfo> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<WarmAccompanyInfo> roomList) {
        this.roomList = roomList;
    }

    public List<PlayGameTabInfo> getTagList() {
        return tagList;
    }

    public void setTagList(List<PlayGameTabInfo> tagList) {
        this.tagList = tagList;
    }

}
