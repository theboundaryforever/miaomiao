package com.tongdaxing.xchat_core.find.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/12
 */
public class VoiceGroupInfo implements Parcelable, MultiItemEntity {


    /**
     * avatar : https://pic.miaomiaofm.com/FpuOvp7egcHAO15aFn4g9Z7vGQ09?imageslim
     * context : ggcf
     * createDate : 1552372409000
     * gender : 2
     * id : 51
     * isLike : false
     * likeNum : 0
     * nick : 偏偏结果冲凉先把他的房间的房间
     * picList : []
     * playerNum : 0
     * status : 1
     * uid : 100642
     */
    private String layoutType;
    private String avatar;
    private String context;
    private long createDate;
    private int gender;
    private int id;
    private boolean isFans;
    private boolean isLike;
    private int likeNum;
    private String nick;
    private int playerNum;
    private int status;
    private int itemType;//本地字段，item的类型
    private int uid;
    private List<UserInfo> userInfoList;//用于多布局类型，本地字段
    private List<RecommendModuleInfo> recommendModuleInfoList;//用于多布局类型，本地字段
    private List<PictureListInfo> picList;

    @Override
    public String toString() {
        return "VoiceGroupInfo{" +
                "layoutType='" + layoutType + '\'' +
                ", avatar='" + avatar + '\'' +
                ", context='" + context + '\'' +
                ", createDate=" + createDate +
                ", gender=" + gender +
                ", id=" + id +
                ", isFans=" + isFans +
                ", isLike=" + isLike +
                ", likeNum=" + likeNum +
                ", nick='" + nick + '\'' +
                ", playerNum=" + playerNum +
                ", status=" + status +
                ", itemType=" + itemType +
                ", uid=" + uid +
                ", userInfoList=" + userInfoList +
                ", recommendModuleInfoList=" + recommendModuleInfoList +
                ", picList=" + picList +
                '}';
    }

    public String getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }

    public List<RecommendModuleInfo> getRecommendModuleInfoList() {
        return recommendModuleInfoList;
    }

    public void setRecommendModuleInfoList(List<RecommendModuleInfo> recommendModuleInfoList) {
        this.recommendModuleInfoList = recommendModuleInfoList;
    }

    public List<UserInfo> getUserInfoList() {
        return userInfoList;
    }

    public void setUserInfoList(List<UserInfo> userInfoList) {
        this.userInfoList = userInfoList;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public boolean isFans() {
        return isFans;
    }

    public void setFans(boolean fans) {
        isFans = fans;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isIsLike() {
        return isLike;
    }

    public void setIsLike(boolean isLike) {
        this.isLike = isLike;
    }

    public int getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(int likeNum) {
        this.likeNum = likeNum;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getPlayerNum() {
        return playerNum;
    }

    public void setPlayerNum(int playerNum) {
        this.playerNum = playerNum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public List<PictureListInfo> getPicList() {
        return picList;
    }

    public void setPicList(List<PictureListInfo> picList) {
        this.picList = picList;
    }

    public VoiceGroupInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.layoutType);
        dest.writeString(this.avatar);
        dest.writeString(this.context);
        dest.writeLong(this.createDate);
        dest.writeInt(this.gender);
        dest.writeInt(this.id);
        dest.writeByte(this.isFans ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isLike ? (byte) 1 : (byte) 0);
        dest.writeInt(this.likeNum);
        dest.writeString(this.nick);
        dest.writeInt(this.playerNum);
        dest.writeInt(this.status);
        dest.writeInt(this.itemType);
        dest.writeInt(this.uid);
        dest.writeList(this.userInfoList);
        dest.writeList(this.recommendModuleInfoList);
        dest.writeTypedList(this.picList);
    }

    protected VoiceGroupInfo(Parcel in) {
        this.layoutType = in.readString();
        this.avatar = in.readString();
        this.context = in.readString();
        this.createDate = in.readLong();
        this.gender = in.readInt();
        this.id = in.readInt();
        this.isFans = in.readByte() != 0;
        this.isLike = in.readByte() != 0;
        this.likeNum = in.readInt();
        this.nick = in.readString();
        this.playerNum = in.readInt();
        this.status = in.readInt();
        this.itemType = in.readInt();
        this.uid = in.readInt();
        this.userInfoList = new ArrayList<UserInfo>();
        in.readList(this.userInfoList, UserInfo.class.getClassLoader());
        this.recommendModuleInfoList = new ArrayList<RecommendModuleInfo>();
        in.readList(this.recommendModuleInfoList, RecommendModuleInfo.class.getClassLoader());
        this.picList = in.createTypedArrayList(PictureListInfo.CREATOR);
    }

    public static final Creator<VoiceGroupInfo> CREATOR = new Creator<VoiceGroupInfo>() {
        @Override
        public VoiceGroupInfo createFromParcel(Parcel source) {
            return new VoiceGroupInfo(source);
        }

        @Override
        public VoiceGroupInfo[] newArray(int size) {
            return new VoiceGroupInfo[size];
        }
    };
}
