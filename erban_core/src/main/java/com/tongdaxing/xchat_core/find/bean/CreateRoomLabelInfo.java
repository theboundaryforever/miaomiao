package com.tongdaxing.xchat_core.find.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class CreateRoomLabelInfo implements Parcelable{
    @Override
    public String toString() {
        return "CreateRoomLabelInfo{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", typeEnum='" + typeEnum + '\'' +
                ", tagList=" + tagList +
                '}';
    }

    /**
     * name : 娱乐房间
     * tagList : [{"children":"8","createTime":1511155717000,"description":"","id":8,"istop":true,"leftLevel":0,"name":"交友","pict":"https://pic.miaomiaofm.com/room_tag_new_friend.png","seq":1,"status":true,"tmpstr":"","type":1},{"children":"2","createTime":1511155709000,"defPic":"https://pic.miaomiaofm.com/home_nanshen_moren@3x.png","description":"","id":2,"istop":false,"leftLevel":0,"name":"男神","optPic":"https://pic.miaomiaofm.com/home_nanshen_dianji@3x.png","pict":"https://pic.miaomiaofm.com/room_tag_new_god.png","seq":6,"status":true,"tmpint":1,"tmpstr":"","type":1},{"children":"3","createTime":1511155717000,"defPic":"https://pic.miaomiaofm.com/home_nvshen_moren@3x.png","description":"","id":3,"istop":false,"leftLevel":0,"name":"女神","optPic":"https://pic.miaomiaofm.com/home_nvshen_dianji@3x.png","pict":"https://pic.miaomiaofm.com/room_tag_new_goddess.png","seq":5,"status":true,"tmpint":1,"tmpstr":"","type":1},{"children":"4,6","createTime":1511155717000,"defPic":"https://pic.miaomiaofm.com/home_song_moren@3x.png","description":"","id":4,"istop":false,"leftLevel":0,"name":"听歌","optPic":"https://pic.miaomiaofm.com/home_song_dianji@3x.png","pict":"https://pic.miaomiaofm.com/room_tag_new_music.png","seq":9,"status":true,"tmpint":1,"tmpstr":"","type":1},{"children":"6","createTime":1511155717000,"defPic":"https://pic.miaomiaofm.com/home_diantai_moren@3x.png","description":"","id":6,"istop":false,"leftLevel":0,"name":"电台","optPic":"https://pic.miaomiaofm.com/home_diantai_dianji@3x.png","pict":"https://pic.miaomiaofm.com/room_tag_new_radio.png","seq":3,"status":true,"tmpstr":"","type":1},{"children":"5","createTime":1511155717000,"description":"","id":5,"istop":false,"leftLevel":0,"name":"陪玩","pict":"https://pic.miaomiaofm.com/room_tag_new_play.png","seq":4,"status":true,"tmpstr":"","type":1}]
     * type : 3
     * typeEnum : recreation
     */

    private String name;
    private int type;
    private String typeEnum;
    private boolean isCustomTag;
    private List<TagListBean> tagList;
    private List<String> titleList;

    private boolean isSelected;//本地字段 是否选中

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCustomTag() {
        return isCustomTag;
    }

    public void setCustomTag(boolean customTag) {
        isCustomTag = customTag;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeEnum() {
        return typeEnum;
    }

    public void setTypeEnum(String typeEnum) {
        this.typeEnum = typeEnum;
    }

    public List<TagListBean> getTagList() {
        return tagList;
    }

    public void setTagList(List<TagListBean> tagList) {
        this.tagList = tagList;
    }

    public List<String> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<String> titleList) {
        this.titleList = titleList;
    }

    public static class TagListBean implements Parcelable{
        private boolean isSelected;//本地字段，是选中
        private int isCustomLabel;//本地字段，用于自定义标签，-1表示这个标签不可删除，0表示可删除

        @Override
        public String toString() {
            return "TagListBean{" +
                    "isSelected=" + isSelected +
                    ", isCustomLabel=" + isCustomLabel +
                    ", children='" + children + '\'' +
                    ", createTime=" + createTime +
                    ", description='" + description + '\'' +
                    ", id=" + id +
                    ", istop=" + istop +
                    ", leftLevel=" + leftLevel +
                    ", name='" + name + '\'' +
                    ", pict='" + pict + '\'' +
                    ", seq=" + seq +
                    ", status=" + status +
                    ", tmpstr='" + tmpstr + '\'' +
                    ", type=" + type +
                    ", defPic='" + defPic + '\'' +
                    ", optPic='" + optPic + '\'' +
                    ", tmpint=" + tmpint +
                    '}';
        }

        public int getIsCustomLabel() {
            return isCustomLabel;
        }

        public void setIsCustomLabel(int isCustomLabel) {
            this.isCustomLabel = isCustomLabel;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        /**
         * children : 8
         * createTime : 1511155717000
         * description :
         * id : 8
         * istop : true
         * leftLevel : 0
         * name : 交友
         * pict : https://pic.miaomiaofm.com/room_tag_new_friend.png
         * seq : 1
         * status : true
         * tmpstr :
         * type : 1
         * defPic : https://pic.miaomiaofm.com/home_nanshen_moren@3x.png
         * optPic : https://pic.miaomiaofm.com/home_nanshen_dianji@3x.png
         * tmpint : 1
         */

        private String children;
        private long createTime;
        private String description;
        private int id;
        private boolean istop;
        private int leftLevel;
        private String name;
        private String pict;
        private int seq;
        private boolean status;
        private String tmpstr;
        private int type;//0 自定义标签
        private String defPic;
        private String optPic;
        private int tmpint;

        public String getChildren() {
            return children;
        }

        public void setChildren(String children) {
            this.children = children;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isIstop() {
            return istop;
        }

        public void setIstop(boolean istop) {
            this.istop = istop;
        }

        public int getLeftLevel() {
            return leftLevel;
        }

        public void setLeftLevel(int leftLevel) {
            this.leftLevel = leftLevel;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPict() {
            return pict;
        }

        public void setPict(String pict) {
            this.pict = pict;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public String getTmpstr() {
            return tmpstr;
        }

        public void setTmpstr(String tmpstr) {
            this.tmpstr = tmpstr;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getDefPic() {
            return defPic;
        }

        public void setDefPic(String defPic) {
            this.defPic = defPic;
        }

        public String getOptPic() {
            return optPic;
        }

        public void setOptPic(String optPic) {
            this.optPic = optPic;
        }

        public int getTmpint() {
            return tmpint;
        }

        public void setTmpint(int tmpint) {
            this.tmpint = tmpint;
        }

        public TagListBean() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
            dest.writeInt(this.isCustomLabel);
            dest.writeString(this.children);
            dest.writeLong(this.createTime);
            dest.writeString(this.description);
            dest.writeInt(this.id);
            dest.writeByte(this.istop ? (byte) 1 : (byte) 0);
            dest.writeInt(this.leftLevel);
            dest.writeString(this.name);
            dest.writeString(this.pict);
            dest.writeInt(this.seq);
            dest.writeByte(this.status ? (byte) 1 : (byte) 0);
            dest.writeString(this.tmpstr);
            dest.writeInt(this.type);
            dest.writeString(this.defPic);
            dest.writeString(this.optPic);
            dest.writeInt(this.tmpint);
        }

        protected TagListBean(Parcel in) {
            this.isSelected = in.readByte() != 0;
            this.isCustomLabel = in.readInt();
            this.children = in.readString();
            this.createTime = in.readLong();
            this.description = in.readString();
            this.id = in.readInt();
            this.istop = in.readByte() != 0;
            this.leftLevel = in.readInt();
            this.name = in.readString();
            this.pict = in.readString();
            this.seq = in.readInt();
            this.status = in.readByte() != 0;
            this.tmpstr = in.readString();
            this.type = in.readInt();
            this.defPic = in.readString();
            this.optPic = in.readString();
            this.tmpint = in.readInt();
        }

        public static final Creator<TagListBean> CREATOR = new Creator<TagListBean>() {
            @Override
            public TagListBean createFromParcel(Parcel source) {
                return new TagListBean(source);
            }

            @Override
            public TagListBean[] newArray(int size) {
                return new TagListBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.type);
        dest.writeString(this.typeEnum);
        dest.writeList(this.tagList);
    }

    public CreateRoomLabelInfo() {
    }

    protected CreateRoomLabelInfo(Parcel in) {
        this.name = in.readString();
        this.type = in.readInt();
        this.typeEnum = in.readString();
        this.tagList = new ArrayList<TagListBean>();
        in.readList(this.tagList, TagListBean.class.getClassLoader());
    }

    public static final Creator<CreateRoomLabelInfo> CREATOR = new Creator<CreateRoomLabelInfo>() {
        @Override
        public CreateRoomLabelInfo createFromParcel(Parcel source) {
            return new CreateRoomLabelInfo(source);
        }

        @Override
        public CreateRoomLabelInfo[] newArray(int size) {
            return new CreateRoomLabelInfo[size];
        }
    };
}
