package com.tongdaxing.xchat_core.find.bean;

/**
 * Function:
 * Author: Edward on 2019/3/28
 */
public class SquareQueueInfo {


    /**
     * closeLevel : 10000
     * countTime : 33825
     * desc : 哇，恭喜js-学喵叫将【玫瑰花】赠予一壶酒成为众人羡慕的CP，大家来祝福他们吧。
     * endTime : 1553782371000
     * first : 44
     * giftId : 1
     * inviteUser : {"avatar":"https://pic.miaomiaofm.com/FnZ8wpBo26hjFfx9mriGV0Hr-sUK?imageslim","erbanNo":2048113,"nick":"js-学喵叫","uid":100000}
     * picUrl : https://pic.miaomiaofm.com/FklTLwCF9zcGKi45Qcp8FI66QpZs?imageslim
     * recUser : {"avatar":"https://pic.miaomiaofm.com/FmC72QC37C5g7Jqu0lT5WCt14A3N?imageslim","erbanNo":66666,"nick":"一壶酒","uid":100747}
     * roomUid : 25501704
     * second : 44
     * title : cp广播
     */
    private int cpGiftLevel;
    private int closeLevel;
    private int countTime;
    private String desc;
    private int first;
    private int giftId;
    private CpUserBean inviteUser;
    private String picUrl;
    private CpUserBean recUser;
    private long roomUid;
    private int second;
    private String title;

    public int getCpGiftLevel() {
        return cpGiftLevel;
    }

    public void setCpGiftLevel(int cpGiftLevel) {
        this.cpGiftLevel = cpGiftLevel;
    }

    @Override
    public String toString() {
        return "{" +
                "cpGiftLevel=" + cpGiftLevel +
                ", closeLevel=" + closeLevel +
                ", countTime=" + countTime +
                ", desc='" + desc + '\'' +
                ", first=" + first +
                ", giftId=" + giftId +
                ", inviteUser=" + inviteUser +
                ", picUrl='" + picUrl + '\'' +
                ", recUser=" + recUser +
                ", roomUid=" + roomUid +
                ", second=" + second +
                ", title='" + title + '\'' +
                '}';
    }

    public int getCloseLevel() {
        return closeLevel;
    }

    public void setCloseLevel(int closeLevel) {
        this.closeLevel = closeLevel;
    }

    public int getCountTime() {
        return countTime;
    }

    public void setCountTime(int countTime) {
        this.countTime = countTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public CpUserBean getInviteUser() {
        return inviteUser;
    }

    public void setInviteUser(CpUserBean inviteUser) {
        this.inviteUser = inviteUser;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public CpUserBean getRecUser() {
        return recUser;
    }

    public void setRecUser(CpUserBean recUser) {
        this.recUser = recUser;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
