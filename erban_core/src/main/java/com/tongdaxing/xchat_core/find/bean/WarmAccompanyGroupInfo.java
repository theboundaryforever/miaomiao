package com.tongdaxing.xchat_core.find.bean;

import java.util.List;

/**
 * Function: 暖心陪伴
 * Author: Edward on 2019/3/15
 */
public class WarmAccompanyGroupInfo {
    private List<WarmAccompanyListBean> femaleList;//暖心陪伴，女
    private List<WarmAccompanyListBean> manList;//暖心陪伴，男

    @Override
    public String toString() {
        return "WarmAccompanyGroupInfo{" +
                "femaleList=" + femaleList +
                ", manList=" + manList +
                '}';
    }

    public List<WarmAccompanyListBean> getFemaleList() {
        return femaleList;
    }

    public void setFemaleList(List<WarmAccompanyListBean> femaleList) {
        this.femaleList = femaleList;
    }

    public List<WarmAccompanyListBean> getManList() {
        return manList;
    }

    public void setManList(List<WarmAccompanyListBean> manList) {
        this.manList = manList;
    }
}
