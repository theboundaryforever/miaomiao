package com.tongdaxing.xchat_core.find.bean;

/**
 * Function:
 * Author: Edward on 2019/3/15
 */
public class PlayGameTabInfo {

    /**
     * children : 10
     * createTime : 1511155717000
     * description :
     * id : 10
     * istop : false
     * leftLevel : 0
     * name : 二次元
     * pict : https://pic.miaomiaofm.com/room_tag_new_dm.png
     * seq : 8
     * status : true
     * tmpstr :
     * type : 1
     */

    private String children;
    private long createTime;
    private String description;
    private int id;
    private boolean istop;
    private int leftLevel;
    private String name;
    private String pict;
    private int seq;
    private boolean status;
    private String tmpstr;
    private int type;

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isIstop() {
        return istop;
    }

    public void setIstop(boolean istop) {
        this.istop = istop;
    }

    public int getLeftLevel() {
        return leftLevel;
    }

    public void setLeftLevel(int leftLevel) {
        this.leftLevel = leftLevel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPict() {
        return pict;
    }

    public void setPict(String pict) {
        this.pict = pict;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getTmpstr() {
        return tmpstr;
    }

    public void setTmpstr(String tmpstr) {
        this.tmpstr = tmpstr;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
