package com.tongdaxing.xchat_core.find.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Function:
 * Author: Edward on 2019/3/21
 */
public class LoverMatchUserHeadInfo implements Parcelable {

    /**
     * female : {"avatar":"https://pic.miaomiaofm.com/Fp3nOBVBw8Bku8U3AaIiffOf5ctP?imageslim","gender":2,"nick":"那朵花","uid":109815}
     * man : {"avatar":"https://pic.miaomiaofm.com/FmC72QC37C5g7Jqu0lT5WCt14A3N?imageslim","gender":1,"nick":"一壶酒","uid":100747}
     */

    private FemaleBean female;
    private ManBean man;

    public FemaleBean getFemale() {
        return female;
    }

    public void setFemale(FemaleBean female) {
        this.female = female;
    }

    public ManBean getMan() {
        return man;
    }

    public void setMan(ManBean man) {
        this.man = man;
    }

    @Override
    public String toString() {
        return "LoverMatchUserHeadInfo{" +
                "female=" + female +
                ", man=" + man +
                '}';
    }

    public static class FemaleBean implements Parcelable{

        /**
         * avatar : https://pic.miaomiaofm.com/Fp3nOBVBw8Bku8U3AaIiffOf5ctP?imageslim
         * gender : 2
         * nick : 那朵花
         * uid : 109815
         */

        private String avatar;
        private int gender;
        private String nick;
        private int uid;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.avatar);
            dest.writeInt(this.gender);
            dest.writeString(this.nick);
            dest.writeInt(this.uid);
        }

        public FemaleBean() {
        }

        protected FemaleBean(Parcel in) {
            this.avatar = in.readString();
            this.gender = in.readInt();
            this.nick = in.readString();
            this.uid = in.readInt();
        }

        public static final Creator<FemaleBean> CREATOR = new Creator<FemaleBean>() {
            @Override
            public FemaleBean createFromParcel(Parcel source) {
                return new FemaleBean(source);
            }

            @Override
            public FemaleBean[] newArray(int size) {
                return new FemaleBean[size];
            }
        };

        @Override
        public String toString() {
            return "FemaleBean{" +
                    "avatar='" + avatar + '\'' +
                    ", gender=" + gender +
                    ", nick='" + nick + '\'' +
                    ", uid=" + uid +
                    '}';
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.female, flags);
        dest.writeParcelable(this.man, flags);
    }

    public LoverMatchUserHeadInfo() {
    }

    protected LoverMatchUserHeadInfo(Parcel in) {
        this.female = in.readParcelable(FemaleBean.class.getClassLoader());
        this.man = in.readParcelable(ManBean.class.getClassLoader());
    }

    public static final Creator<LoverMatchUserHeadInfo> CREATOR = new Creator<LoverMatchUserHeadInfo>() {
        @Override
        public LoverMatchUserHeadInfo createFromParcel(Parcel source) {
            return new LoverMatchUserHeadInfo(source);
        }

        @Override
        public LoverMatchUserHeadInfo[] newArray(int size) {
            return new LoverMatchUserHeadInfo[size];
        }
    };

    public static class ManBean implements Parcelable{

        /**
         * avatar : https://pic.miaomiaofm.com/FmC72QC37C5g7Jqu0lT5WCt14A3N?imageslim
         * gender : 1
         * nick : 一壶酒
         * uid : 100747
         */

        private String avatar;
        private int gender;
        private String nick;
        private int uid;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.avatar);
            dest.writeInt(this.gender);
            dest.writeString(this.nick);
            dest.writeInt(this.uid);
        }

        public ManBean() {
        }

        protected ManBean(Parcel in) {
            this.avatar = in.readString();
            this.gender = in.readInt();
            this.nick = in.readString();
            this.uid = in.readInt();
        }

        public static final Creator<ManBean> CREATOR = new Creator<ManBean>() {
            @Override
            public ManBean createFromParcel(Parcel source) {
                return new ManBean(source);
            }

            @Override
            public ManBean[] newArray(int size) {
                return new ManBean[size];
            }
        };

        @Override
        public String toString() {
            return "ManBean{" +
                    "avatar='" + avatar + '\'' +
                    ", gender=" + gender +
                    ", nick='" + nick + '\'' +
                    ", uid=" + uid +
                    '}';
        }
    }
}
