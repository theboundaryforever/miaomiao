package com.tongdaxing.xchat_core.find.bean;

/**
 * 通知消息bean
 * <p>
 * Created by zhangjian on 2019/7/16.
 */
public class NoticeIMBean {
    public static final int TYPE_FOLLOW = 1;
    public static final int TYPE_AUDIO_MATCH_LIKE = 2;
    public static final int TYPE_NOTICE_FANS = 3;
    /**
     * avatar : string
     * createTime : 2019-07-16T08:51:06.139Z
     * lid : 0
     * nick : string
     * type : 0
     * uid : 0
     */

    private String avatar;
    private long createTime;
    private int lid;
    private String nick;
    private int type;//1是关注 2是声音匹配喜欢
    private int uid;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getLid() {
        return lid;
    }

    public void setLid(int lid) {
        this.lid = lid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
}
