package com.tongdaxing.xchat_core.find;

import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * Function:
 * Author: Edward on 2019/3/14
 */
public interface IVoiceGroupClient extends ICoreClient {
    String PUBLISH_VOICE_GROUP_REFRESH_LIST = "publishVoiceGroupRefreshList";

    void publishVoiceGroupRefreshList();
}
