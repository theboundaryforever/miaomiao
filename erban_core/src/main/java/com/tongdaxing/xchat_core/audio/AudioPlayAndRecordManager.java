package com.tongdaxing.xchat_core.audio;

import android.content.Context;

import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_framework.media.AudioPlayer;
import com.tongdaxing.xchat_framework.media.OnPlayListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhouxiangfeng on 2017/5/20.
 */

public class AudioPlayAndRecordManager {

    private volatile static AudioPlayAndRecordManager audioPlayManager;
    private AudioRecorder recorder;

    private List<AudioPlayer> players;

    private AudioPlayAndRecordManager() {
        players = new ArrayList<>();
    }

    public static AudioPlayAndRecordManager getInstance() {
        if (audioPlayManager == null) {
            synchronized (AudioPlayAndRecordManager.class) {
                if (audioPlayManager == null) {
                    audioPlayManager = new AudioPlayAndRecordManager();
                }
            }
        }
        return audioPlayManager;
    }

    public AudioPlayer createAudioPlayer(Context context, OnPlayListener listener) {
        AudioPlayer player = new AudioPlayer(context, null, listener);
        players.add(player);
        return player;
    }

    public void releaseAudioPlayer(AudioPlayer player) {
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
            player.setOnPlayListener(null);
            if (players.contains(player)) {
                players.remove(player);
            }
        }
    }

    public void stopAllPlayer() {
        if (!ListUtils.isListEmpty(players)) {
            for (AudioPlayer player : players) {
                if (player != null && player.isPlaying()) {
                    player.stop();
                }
            }
        }
    }

    @Deprecated
    public AudioRecorder getAudioRecorder(Context context, IAudioRecordCallback callback) {        // 初始化recorder
        recorder = new AudioRecorder(
                context,
                RecordType.AAC, // 录制音频类型（aac/amr)
                0, // 最长录音时长，到该长度后，会自动停止录音, 默认120s
                callback);
        return recorder;
    }

    public AudioRecorder getAudioRecorder(Context context, int maxDuration, IAudioRecordCallback callback) {
        // 初始化recorder
        return new AudioRecorder(
                context,
                RecordType.AAC, // 录制音频类型（aac/amr)
                maxDuration, // 最长录音时长，到该长度后，会自动停止录音, 默认120s
                callback);
    }

    @Deprecated
    public void startRecord() {
        if (null != recorder) {
            if (recorder.isRecording()) {
                recorder.completeRecord(true);
                recorder.destroyAudioRecorder();
            }
            recorder.startRecord();
        }
    }

    @Deprecated
    public void stopRecord(boolean cancel) {
        if (null != recorder && recorder.isRecording()) {
            recorder.completeRecord(cancel);
        }
    }

    /**
     * 释放资源（释放所有的播放器资源）
     * 这里会释放所有的播放器，不保证的场景不能随便用
     */
    public void releaseAllAudioPlayer() {
        if (!ListUtils.isListEmpty(players)) {
            for (AudioPlayer player : players) {
                //注意这里使用releaseAudioPlayer(AudioPlayer player)可能会有remove列表position变化出现问题，未验证
                if (player != null) {
                    if (player.isPlaying()) {
                        player.stop();
                    }
                    player.setOnPlayListener(null);
                }
            }
            players.clear();
        }
    }
}
