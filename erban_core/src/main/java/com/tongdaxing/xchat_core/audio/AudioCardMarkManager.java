package com.tongdaxing.xchat_core.audio;

import android.content.Context;

import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * 声卡的红点管理
 */
public class AudioCardMarkManager {

    @SuppressWarnings("StaticFieldLeak")
    private static volatile AudioCardMarkManager instance;
    private Context context;

    private AudioCardMarkManager(Context context) {
        this.context = context.getApplicationContext();
    }

    public static AudioCardMarkManager getInstance(Context context) {
        if (instance == null) {
            synchronized (AudioCardMarkManager.class) {
                if (instance == null) {
                    instance = new AudioCardMarkManager(context);
                }
            }
        }
        return instance;
    }

    /**
     * mark是否显示
     */
    public boolean isMark() {
        return isMarkNoCleared();//如果没清除过就显应该示
    }

    /**
     * mark是否没被清除
     */
    private boolean isMarkNoCleared() {
        try {
            return !(boolean) SpUtils.get(context, SpEvent.audioCardMarkClear, false);//是否没清除过mark
        } catch (Exception e) {
            e.printStackTrace();
            return true;//宁愿红点不出来
        }
    }

    /**
     * 清除mark
     */
    public void clearMark() {
        SpUtils.put(context, SpEvent.audioCardMarkClear, true);
        CoreManager.notifyClients(IAudioCardCoreClient.class, IAudioCardCoreClient.METHOD_ON_MARK_CLEAR);
    }
}
