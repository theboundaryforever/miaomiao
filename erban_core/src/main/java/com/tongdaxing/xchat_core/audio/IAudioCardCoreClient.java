package com.tongdaxing.xchat_core.audio;

import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

public interface IAudioCardCoreClient extends ICoreClient {

    String METHOD_ON_MARK_CLEAR = "onAudioCardMarkClear";

}
