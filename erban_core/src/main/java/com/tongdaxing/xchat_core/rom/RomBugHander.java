package com.tongdaxing.xchat_core.rom;

import android.app.Application;
import android.os.AsyncTask;

import com.llew.huawei.verifier.LoadedApkHuaWei;
import com.tongdaxing.xchat_framework.util.util.VersionUtil;
import com.tongdaxing.xchat_framework.util.util.field.FieldUtils;

import java.lang.reflect.Field;

import static com.tongdaxing.xchat_framework.util.util.VersionUtil.HUAWEI;
import static com.tongdaxing.xchat_framework.util.util.VersionUtil.VERSION_5_1;
import static com.tongdaxing.xchat_framework.util.util.VersionUtil.VERSION_5_1_1;
import static com.tongdaxing.xchat_framework.util.util.VersionUtil.VIVO;

/**
 * rom的bug的处理
 */
public class RomBugHander {

    /**
     * 这里处理VIVO手机rom自定义AbsListView的crash问题
     */
    public static void initVivoAbsListViewCrashHander() {
        try {
            if (VersionUtil.getManufacturer().toLowerCase().contains(VIVO.toLowerCase())) {//VIVO手机
                FieldUtils.setFinalStatic(AsyncTask.class.getDeclaredField("SERIAL_EXECUTOR"), new SafeSerialExecutor());
                Field defaultField = AsyncTask.class.getDeclaredField("sDefaultExecutor");
                defaultField.setAccessible(true);
                defaultField.set(null, AsyncTask.SERIAL_EXECUTOR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 修复华为华为5.1与5.1.1两个版本提示注册广播过多的bug
     */
    public static void initHuaweiVerifier(Application application) {
        if (VersionUtil.getManufacturer().equals(HUAWEI)) {
            String systemVersion = VersionUtil.getSystemVersion();
            if (VERSION_5_1.equals(systemVersion) || VERSION_5_1_1.equals(systemVersion)) {
                LoadedApkHuaWei.hookHuaWeiVerifier(application);
            }
        }
    }
}
