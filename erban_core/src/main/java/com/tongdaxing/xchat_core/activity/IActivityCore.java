package com.tongdaxing.xchat_core.activity;

import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * Created by chenran on 2017/12/26.
 */

public interface IActivityCore extends IBaseCore {
    void requestLotteryActivity();
}
