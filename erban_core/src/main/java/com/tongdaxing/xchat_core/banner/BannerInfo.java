package com.tongdaxing.xchat_core.banner;

/**
 * banner实体类
 * <p>
 * Created by zhangjian on 2019/7/2.
 */
public class BannerInfo {
    /**
     * bannerId : 135
     * bannerName : 确定
     * bannerPic : https://pic.miaomiaofm.com/FnudvqKem_j_x_DQVBUsZ5YvU8bz?imageslim
     * skipType : 3
     * skipUri : 阿达
     * seqNo : 3
     * isNewUser : 0
     */

    private int bannerId;
    private String bannerName;
    private String bannerPic;
    private int skipType;
    private String skipUri;
    private int seqNo;
    private int isNewUser;

    public int getBannerId() {
        return bannerId;
    }

    public void setBannerId(int bannerId) {
        this.bannerId = bannerId;
    }

    public String getBannerName() {
        return bannerName;
    }

    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    public String getBannerPic() {
        return bannerPic;
    }

    public void setBannerPic(String bannerPic) {
        this.bannerPic = bannerPic;
    }

    public int getSkipType() {
        return skipType;
    }

    public void setSkipType(int skipType) {
        this.skipType = skipType;
    }

    public String getSkipUri() {
        return skipUri;
    }

    public void setSkipUri(String skipUri) {
        this.skipUri = skipUri;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public int getIsNewUser() {
        return isNewUser;
    }

    public void setIsNewUser(int isNewUser) {
        this.isNewUser = isNewUser;
    }
}
