package com.tongdaxing.xchat_core.banner;

import android.text.TextUtils;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * <p>
 * Created by zhangjian on 2019/7/3.
 */
public class BannerClickModel {
    public static final String HOME_TOP_BANNER = "home_top";
    public static final String HOME_ADVERT_BANNER = "home_advert";
    public static final String TAB_LIVE_BANNER = "anchor_top";
    public static final String SPLASH = "splash";
    public static final String ROOM_ACTIVITY_BANNER = "room_activity";
    private static final String TAG = "BannerClickModel";

    /**
     * banner点击统计
     *
     * @param bannerId       bannerId
     * @param bannerLocation home_top, home_advert, anchor_top, splash, room_activity
     */
    public void bannerStat(int bannerId, String bannerLocation) {
        if (TextUtils.isEmpty(bannerLocation)) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("bannerId", String.valueOf(bannerId));
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("viewIndex", bannerLocation);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getBannerStat(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                L.error(TAG, "bannerStat", e);
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response.isSuccess()) {
                    L.debug(TAG, "bannerStat isSuccess");
                } else {
                    L.debug(TAG, "bannerStat is not Success");
                }
            }
        });
    }
}
