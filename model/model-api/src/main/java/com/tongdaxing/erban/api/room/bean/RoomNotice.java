package com.tongdaxing.erban.api.room.bean;

/**
 * Created by Chen on 2019/4/18.
 */
public class RoomNotice {
    private String mRoomNotice;
    private long mRoomId;

    public String getRoomNotice() {
        return mRoomNotice;
    }

    public void setRoomNotice(String roomNotice) {
        mRoomNotice = roomNotice;
    }

    public long getRoomId() {
        return mRoomId;
    }

    public void setRoomId(long roomId) {
        mRoomId = roomId;
    }
}
