package com.tongdaxing.erban.api.room.info;


import android.text.TextUtils;

/**
 * Created by Chen on 2019/4/18.
 */
public class RoomBaseInfo {

    private long mRoomId;
    private long mRoomId2; //靓号
    private String mRoomName;
    private String roomGreeting;
    private int mViewerNum;  //观众数
    private String mPassword;
    private int mRoomPattern;//房间模式
    private int mRoomCategory; //房间标签
    private boolean mIsFocusRoom;//房间关注
    private int mRoomChairPattern;

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getRoomGreeting() {
        return roomGreeting;
    }

    public void setRoomGreeting(String roomGreeting) {
        this.roomGreeting = roomGreeting;
    }

    public long getRoomId2() {
        return mRoomId2;
    }

    public void setRoomId2(long id2) {
        this.mRoomId2 = id2;
    }

    public int getRoomPattern() {
        return mRoomPattern;
    }

    public void setRoomPattern(int mRoomPattern) {
        this.mRoomPattern = mRoomPattern;
    }

    public int getRoomCategory() {
        return mRoomCategory;
    }

    public void setRoomCategory(int mRoomCategory) {
        this.mRoomCategory = mRoomCategory;
    }

    public long getRoomId() {
        return mRoomId;
    }

    public void setRoomId(long roomId) {
        mRoomId = roomId;
    }

    public String getRoomName() {
        return mRoomName;
    }

    public void setRoomName(String roomName) {
        mRoomName = roomName;
    }

    public int getViewerNum() {
        return mViewerNum;
    }

    public void setViewerNum(int viewerNum) {
        mViewerNum = viewerNum;
    }

    public boolean hasPassword() {
        return !TextUtils.isEmpty(mPassword);
    }

    public boolean isRoomOwner() {
        return false;
    }

    public boolean isFocusRoom() {
        return mIsFocusRoom;
    }

    public void setFocusRoom(boolean focusRoom) {
        mIsFocusRoom = focusRoom;
    }

    public int getRoomChairPattern() {

        return mRoomChairPattern;
    }

    public void setRoomChairPattern(int roomChairPattern) {
        mRoomChairPattern = roomChairPattern;
    }
}
