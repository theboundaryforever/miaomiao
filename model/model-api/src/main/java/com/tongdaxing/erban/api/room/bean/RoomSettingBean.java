package com.tongdaxing.erban.api.room.bean;

/**
 * Created by Chen on 2019/4/18.
 */
public class RoomSettingBean {
    private String mRoomName;
    private String mRoomPsw;
    private String mRoomGreeting;
    private int mRoomCategory;
    private int mRoomPattern;
    private int mRoomChairPattern;

    public String getRoomName() {
        return mRoomName;
    }

    public RoomSettingBean setRoomName(String roomName) {
        this.mRoomName = roomName;
        return this;
    }

    public String getRoomPsw() {
        return mRoomPsw;
    }

    public RoomSettingBean setRoomPsw(String roomPsw) {
        this.mRoomPsw = roomPsw;
        return this;
    }

    public String getRoomGreeting() {
        return mRoomGreeting;
    }

    public RoomSettingBean setRoomGreeting(String roomGreeting) {
        this.mRoomGreeting = roomGreeting;
        return this;
    }

    public int getRoomCategory() {
        return mRoomCategory;
    }

    public RoomSettingBean setRoomCategory(int roomCategory) {
        this.mRoomCategory = roomCategory;
        return this;
    }

    public int getRoomPattern() {
        return mRoomPattern;
    }

    public RoomSettingBean setRoomPattern(int roomPattern) {
        this.mRoomPattern = roomPattern;
        return this;
    }

    public int getRoomChairPattern() {
        return mRoomChairPattern;
    }

    public RoomSettingBean setRoomChairPattern(int roomChairPattern) {
        mRoomChairPattern = roomChairPattern;
        return this;
    }
}
