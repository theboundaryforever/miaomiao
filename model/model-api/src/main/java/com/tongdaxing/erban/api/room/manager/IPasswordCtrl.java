package com.tongdaxing.erban.api.room.manager;

/**
 * Created by Chen on 2019/4/18.
 */
public interface IPasswordCtrl {

    String getRoomPassword(long roomId);

}
