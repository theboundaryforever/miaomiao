package com.tongdaxing.erban.api.room.session;

import com.tongdaxing.erban.api.room.info.ChairsInfo;
import com.tongdaxing.erban.api.room.info.MasterInfo;
import com.tongdaxing.erban.api.room.info.RoomBaseInfo;
import com.tongdaxing.erban.api.room.info.RoomEnterInfo;
import com.tongdaxing.erban.api.room.info.RoomOwnerInfo;
import com.tongdaxing.erban.api.room.info.SettingInfo;
import com.tongdaxing.erban.api.room.info.TalkInfo;
import com.tongdaxing.erban.api.room.info.UserListInfo;

/**
 * Created by Chen on 2019/4/18.
 */
public interface IRoomSession {
    RoomTicket getRoomTicket();

    RoomBaseInfo getRoomBaseInfo();

    ChairsInfo getChairsInfo();

    MasterInfo getMasterInfo();

    UserListInfo getUserListInfo();

    RoomOwnerInfo getRoomOwnerInfo();

    SettingInfo getSettingInfo();

    RoomEnterInfo getRoomEnterInfo();

    TalkInfo getTalkInfo();

    boolean isSelfRoom();

    boolean isLostConnect();

    boolean isRejoin();
}
