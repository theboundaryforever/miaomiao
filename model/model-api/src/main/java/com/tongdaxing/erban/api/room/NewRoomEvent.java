package com.tongdaxing.erban.api.room;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;

/**
 * Created by Chen on 2019/4/19.
 */
public class NewRoomEvent {

    public static class OnRoomShowTalkMessage {
        ChatRoomMessage mChatRoomMessage;
        public OnRoomShowTalkMessage(ChatRoomMessage chatRoomMessage) {
            this.mChatRoomMessage = chatRoomMessage;
        }

        public ChatRoomMessage getChatRoomMessage() {
            return mChatRoomMessage;
        }
    }
}
