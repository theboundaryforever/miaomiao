package com.tongdaxing.erban.api.room.session;

import android.os.SystemClock;

/**
 * Created by Chen on 2019/4/18.
 */
public class RoomTicket {

    private long mContext;
    private long mRoomId;
    private String mPassword = "";
    private boolean mIsRejoin = false;
    private long mFollowId;

    public void setRoomId(long roomId) {
        mRoomId = roomId;
    }

    public RoomTicket() {
        mContext = SystemClock.elapsedRealtime();
    }


    public long getContext() {
        return mContext;
    }

    public long getRoomId() {
        return mRoomId;
    }


    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    //是否是重连
    public boolean isRejoin() {
        return mIsRejoin;
    }

    public void setIsRejoin(boolean isRejoin) {
        mIsRejoin = isRejoin;
    }

    public long getFollowId() {
        return mFollowId;
    }

    public void setFollowId(long followId) {
        mFollowId = followId;
    }

    @Override
    public String toString() {
        return "RoomTicket{" +
                "roomId:" + mRoomId +
                ",pwd:" + mPassword +
                ",context:" + mContext +
                ",isRejoin:" + mIsRejoin +
                "}";

    }
}
