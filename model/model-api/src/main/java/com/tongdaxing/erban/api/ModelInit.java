package com.tongdaxing.erban.api;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.api.room.IRoomService;
import com.tongdaxing.xchat_framework.coremanager.CoreFactory;

/**
 * Created by Chen on 2019/4/30.
 */
public class ModelInit {

    private final static String TAG = "ModelInit";

    public static void init() {
        registerModel();
    }

    private static void registerModel() {
        boolean hasRegistered = CoreFactory.hasRegisteredCoreClass(IRoomService.class);
        L.info(TAG, "registerModel hasRegistered: %b", hasRegistered);
        if (!hasRegistered) {
            CoreFactory.registerCoreClass(IRoomService.class, "com.tongdaxing.erban.impl.room.RoomService");
        }
    }
}
