package com.tongdaxing.erban.api.user;

/**
 * Created by Chen on 2019/4/25.
 */
public interface IUserService {
    IUserMgr getUserMgr();

    IUserProfile getUserProfile();
}
