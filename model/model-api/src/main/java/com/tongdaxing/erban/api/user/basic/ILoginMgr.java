package com.tongdaxing.erban.api.user.basic;

/**
 * Created by Chen on 2019/4/25.
 */
public interface ILoginMgr {
    void login();

    void logout();

    void kickout();
}
