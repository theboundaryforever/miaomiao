package com.tongdaxing.erban.api.room;

import com.tongdaxing.erban.api.room.manager.IRoomBasicMgr;
import com.tongdaxing.erban.api.room.session.IRoomSession;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * Created by Chen on 2019/4/12.
 */
public interface IRoomService extends IBaseCore {
    IRoomBasicMgr getRoomBasicMgr();

    IRoomSession getRoomSession();
}
