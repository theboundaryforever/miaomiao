package com.tongdaxing.erban.api.room.manager;

/**
 * Created by Chen on 2019/4/18.
 */
public interface IChairCtrl {

    void sitChair(long targetPlayerId, int chairId);

    void leaveChair(long targetPlayerId, int chairId);

    void moveChair(int fromChairId, int toChairId);

    void agreeForcedOnMic();

    void refuseForcedOnMic(int chairId, long playerId);

    void optChairQueue(boolean isJoin);

    void jumpChairQueue(long playerId);

    void muteChair(boolean isMute, final long playerId);

    void setChairSpeakOnOff(final boolean speakOnOff);

    void setChairStatus(final int chairId, final boolean open);

    void queryChairQueue();

    void setChairSpeakStatus(final int chairId, final boolean isSpeak);

    void searchHoldPlayer(long playerId);

    void requestPlayerIsNotOnChair();
}
