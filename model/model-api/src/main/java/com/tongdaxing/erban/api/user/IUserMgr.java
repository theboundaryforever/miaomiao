package com.tongdaxing.erban.api.user;

import com.tongdaxing.erban.api.user.basic.ILoginMgr;
import com.tongdaxing.erban.api.user.basic.ISettingMgr;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * Created by Chen on 2019/4/25.
 */
public interface IUserMgr extends IBaseCore {
    ILoginMgr getLoginMgr();

    ISettingMgr getSettingMgr();
}
