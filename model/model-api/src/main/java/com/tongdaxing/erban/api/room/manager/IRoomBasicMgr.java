package com.tongdaxing.erban.api.room.manager;

import com.tongdaxing.erban.api.room.session.RoomTicket;

/**
 * Created by Chen on 2019/4/18.
 */
public interface IRoomBasicMgr {

    void enterRoom(RoomTicket roomTicket);

    void leaveRoom();

    void kickoutRoom(long playerId);

    ITalkCtrl getTalkCtrl();

}
