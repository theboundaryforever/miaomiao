package com.tongdaxing.erban.api.room.manager;


import com.tongdaxing.erban.api.room.bean.RoomNotice;
import com.tongdaxing.erban.api.room.bean.RoomSettingBean;

/**
 * Created by Chen on 2019/4/18.
 */
public interface ISettingCtrl {
    void setRoom(RoomSettingBean settingBean);

    void setRoomAdmin(long playerId, int adminType);

    void getAdminList();

    void getPswd();

    void updateRoomNotice(RoomNotice roomNotice);

    void requestRoomNotice(long roomId);

    void requestRoomFocusStatus(long roomId);

    void updateRoomFocusStatus(long roomId, boolean isFocus);

    void searchPlayerData(int page, int pageSize, String keyWord);

    void requestRoomReception(String content);
}
