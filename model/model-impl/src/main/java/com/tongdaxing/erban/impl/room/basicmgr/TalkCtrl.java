package com.tongdaxing.erban.impl.room.basicmgr;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.api.room.manager.ITalkCtrl;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.msg.Message;
import com.tongdaxing.xchat_core.room.NewRoomEvent;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Chen on 2019/4/19.
 */
public class TalkCtrl extends BaseCtrl implements ITalkCtrl {

    private String TAG = "TalkCtrl";
    private CompositeDisposable mCompositeDisposable;

    public TalkCtrl() {
        init();
    }

    private void init() {
        L.info(TAG, "TalkCtrl init");
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }

        mCompositeDisposable.add(IMNetEaseManager.get().getChatRoomMsgFlowable()
                .subscribe(messages -> {
                    if (messages.size() == 0) {
                        return;
                    }
                    ChatRoomMessage chatRoomMessage = messages.get(0);

                    if (checkNoNeedMsg(chatRoomMessage)) {
                        return;
                    }

                    onCurrentRoomReceiveNewMsg(chatRoomMessage);
                }));

        mCompositeDisposable.add(IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(roomEvent -> {
                    if (roomEvent == null || roomEvent.getEvent() != RoomEvent.RECEIVE_MSG)  {
                        return;
                    }
                    ChatRoomMessage chatRoomMessage = roomEvent.getChatRoomMessage();

                    if (checkNoNeedMsg(chatRoomMessage)) {
                        return;
                    }
                    onCurrentRoomReceiveNewMsg(chatRoomMessage);
                }));
    }

    @Override
    public void onEnterRoom(Object response) {
        super.onEnterRoom(response);
    }

    @Override
    public void onLeaveRoom() {
        super.onLeaveRoom();
        if (mCompositeDisposable != null) {
            mCompositeDisposable.dispose();
            mCompositeDisposable = null;
        }
    }

    private void onCurrentRoomReceiveNewMsg(ChatRoomMessage chatRoomMessage) {
    }

    /**
     * 检查是否是公屏需要的消息，送礼物和谁来了的消息不加入公屏
     *
     * @param chatRoomMessage
     * @return
     */
    private boolean checkNoNeedMsg(ChatRoomMessage chatRoomMessage) {
        if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {

            CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
            //增加second判断避免将房间分享消息屏蔽掉（问题：分享成功后没显示分享消息，最小化后重新进入又出现）
            if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP &&
                    attachment.getSecond() != CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_SHARE_ROOM) {
                return true;
            }

            if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT &&
                    AvRoomDataManager.get().mCurrentRoomInfo.getGiftEffectSwitch() == 1) {//屏蔽
                return true;
            }

            if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void forbidSpeak(long playerId, int forbidTime) {
        L.info(TAG, "forbidSpeak playerId: %d, forbidTime: %d", playerId, forbidTime);
    }

    @Override
    public void sendChat(Message message) {

    }

    @Override
    public void playDice() {

    }

    @Override
    public List<Message> getTalkMessage() {
        return null;
    }

    @Override
    public void addRoomTalkLocalMessage(Message message) {

    }
}
