package com.tongdaxing.erban.impl.room.basicmgr.chair;

import com.tcloud.core.log.L;
import com.tongdaxing.erban.api.room.manager.IChairCtrl;
import com.tongdaxing.erban.impl.room.basicmgr.BaseCtrl;

/**
 * Created by Chen on 2019/4/19.
 */
public class ChairCtrl extends BaseCtrl implements IChairCtrl {
    private String TAG = "ChairCtrl";

    @Override
    public void sitChair(long targetPlayerId, int chairId) {
        L.info(TAG, "sitChair targetPlayerId: %d, chairId: %d", targetPlayerId, chairId);
    }

    @Override
    public void leaveChair(long targetPlayerId, int chairId) {

    }

    @Override
    public void moveChair(int fromChairId, int toChairId) {

    }

    @Override
    public void agreeForcedOnMic() {

    }

    @Override
    public void refuseForcedOnMic(int chairId, long playerId) {

    }

    @Override
    public void optChairQueue(boolean isJoin) {

    }

    @Override
    public void jumpChairQueue(long playerId) {

    }

    @Override
    public void muteChair(boolean isMute, long playerId) {

    }

    @Override
    public void setChairSpeakOnOff(boolean speakOnOff) {

    }

    @Override
    public void setChairStatus(int chairId, boolean open) {

    }

    @Override
    public void queryChairQueue() {

    }

    @Override
    public void setChairSpeakStatus(int chairId, boolean isSpeak) {

    }

    @Override
    public void searchHoldPlayer(long playerId) {

    }

    @Override
    public void requestPlayerIsNotOnChair() {

    }
}
