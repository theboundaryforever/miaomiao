package com.tongdaxing.erban.impl.room.basicmgr;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.util.WorkerHandler;
import com.tongdaxing.erban.api.room.info.MasterInfo;
import com.tongdaxing.erban.impl.room.RoomSession;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Chen on 2019/4/19.
 */
public abstract class BaseCtrl {

    private WorkerHandler mHandler;
    protected RoomSession mRoomSession;
    private Map<Class<? extends BaseCtrl>, BaseCtrl> mSubCtrls;

    public BaseCtrl() {
        CoreUtils.register(this);
    }

    public synchronized void addSubCtrl(BaseCtrl ctrl) {
        if (mSubCtrls == null) {
            mSubCtrls = new HashMap<>();
        }
        mSubCtrls.put(ctrl.getClass(), ctrl);
    }

    public <T extends BaseCtrl> T getSubCtrl(Class<T> clazz) {
        return (T) mSubCtrls.get(clazz);
    }

    public void setHandler(WorkerHandler handler) {
        mHandler = handler;

        if (mSubCtrls == null) {
            return;
        }
        Iterator iterator = mSubCtrls.entrySet().iterator();
        while (iterator.hasNext()) {
            BaseCtrl ctrl = (BaseCtrl) ((Map.Entry) iterator.next()).getValue();
            ctrl.setHandler(handler);
        }
    }

    public void setRoomSession(RoomSession roomSession) {
        mRoomSession = roomSession;

        if (mSubCtrls == null) {
            return;
        }
        Iterator iterator = mSubCtrls.entrySet().iterator();
        while (iterator.hasNext()) {
            BaseCtrl ctrl = (BaseCtrl) ((Map.Entry) iterator.next()).getValue();
            ctrl.setRoomSession(roomSession);
        }
    }

    public WorkerHandler getHandler() {
        return mHandler;
    }

    public RoomSession getRoomSession() {
        return mRoomSession;
    }

    public MasterInfo getMasterInfo() {
        return mRoomSession.getMasterInfo();
    }

    public void onEnterRoom(Object response) {
        if (mSubCtrls == null) {
            return;
        }
        Iterator iterator = mSubCtrls.entrySet().iterator();
        while (iterator.hasNext()) {
            BaseCtrl ctrl = (BaseCtrl) ((Map.Entry) iterator.next()).getValue();
            ctrl.onEnterRoom(response);
        }
    }

    public void onLeaveRoom() {
        if (mSubCtrls == null) {
            return;
        }
        Iterator iterator = mSubCtrls.entrySet().iterator();
        while (iterator.hasNext()) {
            BaseCtrl ctrl = (BaseCtrl) ((Map.Entry) iterator.next()).getValue();
            ctrl.onLeaveRoom();
        }
    }

}
