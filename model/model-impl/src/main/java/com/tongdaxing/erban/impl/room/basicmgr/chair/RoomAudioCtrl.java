package com.tongdaxing.erban.impl.room.basicmgr.chair;

import com.tongdaxing.erban.impl.room.basicmgr.BaseCtrl;
import com.tongdaxing.erban.impl.room.basicmgr.chair.pattern.IChairObserver;

/**
 * Created by Chen on 2019/4/19.
 */
public class RoomAudioCtrl extends BaseCtrl implements IChairObserver {
    @Override
    public void onSitChair(long targetPlayerId, int chairId, String permissionKey) {

    }

    @Override
    public void onLeaveChair(long targetPlayerId, int chairId, String permissionKey) {

    }

    @Override
    public void onMoveChair(int fromChairId, int toChairId) {

    }

    @Override
    public void onSpeakOnOff(boolean speakOnOff) {

    }

    @Override
    public void onChairPlayerChange(boolean isMasterChanged) {

    }

    @Override
    public void onChairMute(boolean isMute, long playerId) {

    }

    @Override
    public void onChairOpened(int chairId, boolean open) {

    }
}
