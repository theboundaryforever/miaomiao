package com.tongdaxing.erban.impl.room;

import com.tongdaxing.erban.api.room.info.ChairsInfo;
import com.tongdaxing.erban.api.room.info.MasterInfo;
import com.tongdaxing.erban.api.room.info.RoomBaseInfo;
import com.tongdaxing.erban.api.room.info.RoomEnterInfo;
import com.tongdaxing.erban.api.room.info.RoomOwnerInfo;
import com.tongdaxing.erban.api.room.info.SettingInfo;
import com.tongdaxing.erban.api.room.info.TalkInfo;
import com.tongdaxing.erban.api.room.info.UserListInfo;
import com.tongdaxing.erban.api.room.session.IRoomSession;
import com.tongdaxing.erban.api.room.session.RoomTicket;

/**
 * Created by Chen on 2019/4/18.
 */
public class RoomSession implements IRoomSession {

    private Data mData;

    private class Data {
        RoomTicket mRoomTicket = new RoomTicket();
        RoomBaseInfo mRoomBaseInfo = new RoomBaseInfo();
        MasterInfo mMasterInfo = new MasterInfo();
        ChairsInfo mChairsInfo = new ChairsInfo();
        UserListInfo mUserListInfo = new UserListInfo();
        RoomOwnerInfo mRoomOwnerInfo = new RoomOwnerInfo();
        SettingInfo mSettingInfo = new SettingInfo();
        RoomEnterInfo mRoomEnterInfo = new RoomEnterInfo();
        TalkInfo mTalkInfo = new TalkInfo();
        boolean mLostConnect = false;
    }

    public RoomSession() {
        reset();
    }

    public void reset() {
        mData = new Data();
    }

    @Override
    public RoomTicket getRoomTicket() {
        return mData.mRoomTicket;
    }

    @Override
    public RoomBaseInfo getRoomBaseInfo() {
        return mData.mRoomBaseInfo;
    }

    @Override
    public ChairsInfo getChairsInfo() {
        return mData.mChairsInfo;
    }

    @Override
    public MasterInfo getMasterInfo() {
        return mData.mMasterInfo;
    }

    @Override
    public UserListInfo getUserListInfo() {
        return mData.mUserListInfo;
    }

    @Override
    public RoomOwnerInfo getRoomOwnerInfo() {
        return mData.mRoomOwnerInfo;
    }

    @Override
    public SettingInfo getSettingInfo() {
        return mData.mSettingInfo;
    }

    @Override
    public RoomEnterInfo getRoomEnterInfo() {
        return mData.mRoomEnterInfo;
    }

    @Override
    public TalkInfo getTalkInfo() {
        return mData.mTalkInfo;
    }

    @Override
    public boolean isSelfRoom() {
        return false;
    }

    @Override
    public boolean isLostConnect() {
        return mData.mLostConnect;
    }

    public void setIsLostConnect(boolean isLost) {
        mData.mLostConnect = isLost;
    }

    @Override
    public boolean isRejoin() {
        return mData.mRoomTicket.isRejoin();
    }
}
