package com.tongdaxing.erban.impl.room;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.util.WorkerHandler;
import com.tongdaxing.erban.api.room.manager.IRoomBasicMgr;
import com.tongdaxing.erban.api.room.manager.ITalkCtrl;
import com.tongdaxing.erban.api.room.session.RoomTicket;
import com.tongdaxing.erban.impl.room.basicmgr.BaseCtrl;
import com.tongdaxing.erban.impl.room.basicmgr.TalkCtrl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2019/4/18.
 */
public class RoomBasicMgr implements IRoomBasicMgr {

    private WorkerHandler mHandler;
    private RoomSession mRoomSession;
    private List<BaseCtrl> mCtrlList = new ArrayList<>();
    private TalkCtrl mTalkCtrl;

    public RoomBasicMgr(WorkerHandler handler) {
        this.mHandler = handler;

        mTalkCtrl = new TalkCtrl();

        mCtrlList.add(mTalkCtrl);

        setHandler(handler);
        CoreUtils.register(this);
    }

    private void setHandler(WorkerHandler handler) {
        mHandler = handler;

        for (BaseCtrl ctrl : mCtrlList) {
            ctrl.setHandler(handler);
        }
    }

    public void setRoomSession(RoomSession roomSession) {
        mRoomSession = roomSession;
        for (BaseCtrl ctrl : mCtrlList) {
            ctrl.setRoomSession(roomSession);
        }
    }


    @Override
    public void enterRoom(RoomTicket roomTicket) {
        mHandler.optPost(new Runnable() {
            @Override
            public void run() {
                doEnterRoom();
            }
        });
    }

    private void doEnterRoom() {
        L.info(RoomService.TAG, "doEnterRoom.");
    }

    private void onEnterRoomSuccess() {
        if (mRoomSession == null) {
            L.error(RoomService.TAG, "mRoomSession is null.");
            return;
        }
    }

    @Override
    public void leaveRoom() {
        mHandler.optPost(new Runnable() {
            @Override
            public void run() {
                doLeaveRoom();
            }
        });
    }

    private void doLeaveRoom() {

    }

    @Override
    public void kickoutRoom(long playerId) {
        mHandler.optPost(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    @Override
    public ITalkCtrl getTalkCtrl() {
        return mTalkCtrl;
    }
}
