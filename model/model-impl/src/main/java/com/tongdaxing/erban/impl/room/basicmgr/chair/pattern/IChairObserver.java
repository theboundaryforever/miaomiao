package com.tongdaxing.erban.impl.room.basicmgr.chair.pattern;

/**
 * Created by Chen on 2019/4/19.
 */
public interface IChairObserver {

    void onSitChair(long targetPlayerId, int chairId, String permissionKey);

    void onLeaveChair(long targetPlayerId, int chairId, String permissionKey);

    void onMoveChair(int fromChairId, int toChairId);

    //本人开麦
    void onSpeakOnOff(boolean speakOnOff);

    void onChairPlayerChange(boolean isMasterChanged);

    //禁麦
    void onChairMute(boolean isMute, long playerId);

    //打开座位锁
    void onChairOpened(int chairId, boolean open);

}
