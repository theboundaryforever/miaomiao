package com.tongdaxing.erban.impl.room;

import android.os.HandlerThread;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.util.WorkerHandler;
import com.tongdaxing.erban.api.room.IRoomService;
import com.tongdaxing.erban.api.room.manager.IRoomBasicMgr;
import com.tongdaxing.erban.api.room.session.IRoomSession;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;

/**
 * Created by Chen on 2019/4/12.
 */
public class RoomService extends AbstractBaseCore implements IRoomService {
    public final static String TAG = "RoomService";
    private HandlerThread mHandlerThread;
    private WorkerHandler mHandler;
    private RoomSession mRoomSession;
    private RoomBasicMgr mRoomBasicMgr;

    public RoomService() {
        init();
    }

    private void init() {
        L.info(TAG, "RoomService init");
        initWorker(TAG);

        mRoomSession = new RoomSession();
        mRoomBasicMgr = new RoomBasicMgr(mHandler);
        mRoomBasicMgr.setRoomSession(mRoomSession);
    }

    private void initWorker(final String name) {
        mHandlerThread = new HandlerThread(name);
        mHandlerThread.start();
        mHandler = new WorkerHandler(mHandlerThread.getLooper());
        mHandlerThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                initWorker(name);
                CoreUtils.crashIfDebug(e, "[%s]worker throw exception!", name);
            }
        });
    }

    @Override
    public IRoomBasicMgr getRoomBasicMgr() {
        return mRoomBasicMgr;
    }

    @Override
    public IRoomSession getRoomSession() {
        return mRoomSession;
    }
}
