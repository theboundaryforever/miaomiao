package com.tongdaxing.erban.impl.room.basicmgr;

import com.tongdaxing.erban.api.room.bean.RoomNotice;
import com.tongdaxing.erban.api.room.bean.RoomSettingBean;
import com.tongdaxing.erban.api.room.manager.ISettingCtrl;

/**
 * Created by Chen on 2019/4/19.
 */
public class SettingCtrl extends BaseCtrl implements ISettingCtrl {
    @Override
    public void setRoom(RoomSettingBean settingBean) {

    }

    @Override
    public void setRoomAdmin(long playerId, int adminType) {

    }

    @Override
    public void getAdminList() {

    }

    @Override
    public void getPswd() {

    }

    @Override
    public void updateRoomNotice(RoomNotice roomNotice) {

    }

    @Override
    public void requestRoomNotice(long roomId) {

    }

    @Override
    public void requestRoomFocusStatus(long roomId) {

    }

    @Override
    public void updateRoomFocusStatus(long roomId, boolean isFocus) {

    }

    @Override
    public void searchPlayerData(int page, int pageSize, String keyWord) {

    }

    @Override
    public void requestRoomReception(String content) {

    }
}
