package com.tongdaxing.erban.libcommon.net.rxnet.callback;

import com.google.gson.internal.$Gson$Types;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class HttpRequestCallBack<T> {

    public Type mType;

    protected HttpRequestCallBack() {
        mType = getSuperclassTypeParameter(getClass());
    }

    private static Type getSuperclassTypeParameter(Class<?> subclass) {
        Type superclass = subclass.getGenericSuperclass();
        if (superclass instanceof Class) {
            throw new RuntimeException("Missing type parameter.");
        }
        ParameterizedType parameterized = (ParameterizedType) superclass;
        return $Gson$Types.canonicalize(parameterized.getActualTypeArguments()[0]);
    }

//    private OkHttpManager.MyCallBack<ServiceResult<T>> baseCallBack;
//
//    protected HttpRequestCallBack() {
//        baseCallBack = new OkHttpManager.MyCallBack<ServiceResult<T>>() {
//            @Override
//            public void onError(Exception e) {
//                onFinish();
//                onFailure(-1, e == null ? "请求失败" : e.getMessage());
//            }
//
//            @Override
//            public void onResponse(ServiceResult<T> response) {
//                onFinish();
//                if (response.isSuccess()) {
//                    onSuccess(response.getData());
//                } else {
//                    onFailure(response.getCode(), response.getMessage());
//                }
//            }
//        };
//    }
//
//    public OkHttpManager.MyCallBack<ServiceResult<T>> getBaseCallBack() {
//        return baseCallBack;
//    }

    public abstract void onFinish();

    public abstract void onSuccess(String message, T response);

    public abstract void onFailure(int code, String msg);
}
