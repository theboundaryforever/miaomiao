package com.tongdaxing.erban.libcommon.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.text.style.ImageSpan;

import com.juxiao.library_utils.DisplayUtils;
import com.tcloud.core.log.L;

/**
 * <p>
 * Created by zhangjian on 2019/4/1.
 */
public class CpSignImageSpan extends ImageSpan {
    public static final String TAG = "CpSignImageSpan";
    private String sign;
    private Context context;
    private int mTextSize = 9;
    private int mXOffset = 20;
    private int mYOffset = 0;
    private int imgWidth = -1;
    private int imgHeight = -1;
    private Rect textBounds;

    public CpSignImageSpan(Context context, int resourceId) {
        super(context, resourceId);
    }

    public CpSignImageSpan(Context context, int resourceId, String sign, int textSize, int xOffset, int yOffset) {
        this(context, resourceId, sign);
        mTextSize = textSize;
        mXOffset = xOffset;
        mYOffset = yOffset;
    }

    public CpSignImageSpan(Context context, int resourceId, String sign) {
        super(context, resourceId, ALIGN_BASELINE);
        this.context = context.getApplicationContext();
        if (TextUtils.isEmpty(sign)) {
            sign = "";
        }
        if (sign.length() > 3) {
            this.sign = sign.substring(0, 3);
        } else {
            this.sign = sign;
        }
        textBounds = new Rect();
    }

    public int getImgWidth() {
        return imgWidth;
    }

    public void setImgWidth(int imgWidth) {
        this.imgWidth = imgWidth;
    }

    public int getImgHeight() {
        return imgHeight;
    }

    public void setImgHeight(int imgHeight) {
        this.imgHeight = imgHeight;
    }

    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        L.debug(TAG, "x = %f, top = %d, y = %d, bottom = %d, imgWidth = %d, imgHeight = %d", x, top, y, bottom, imgWidth, imgHeight);
//        if (imgWidth == -1 && imgHeight == -1) {
//            super.draw(canvas, text, start, end, x, top, y, bottom, paint);
//        } else {
//            Drawable b = getDrawable();
//            b.setBounds(0, 0, imgWidth == -1 ? b.getIntrinsicWidth() : getImgWidth(), imgHeight == -1 ? b.getIntrinsicHeight() : getImgHeight());
//            canvas.save();
//            int transY;
//            x = DisplayUtils.dip2px(context, 2);
//            transY = ((bottom - top) - b.getBounds().bottom) / 2 + top;
//            canvas.translate(x, transY);
//            b.draw(canvas);
//            canvas.restore();
//        }

        Drawable b = getDrawable();
        canvas.save();
        int transY;
        transY = ((bottom - top) - b.getBounds().bottom) / 2 + top;
        canvas.translate(x, transY);
        b.draw(canvas);
        canvas.restore();

        paint.setTextSize(DisplayUtils.sp2px(context, mTextSize));
        paint.setFakeBoldText(true);
        paint.setColor(Color.WHITE);
        float rowX = x + DisplayUtils.dip2px(context, mXOffset);
        float rowY;
        paint.getTextBounds(sign, 0, sign.length(), textBounds);
        rowY = transY + textBounds.height() + (b.getBounds().height() - textBounds.height()) / 4 + mYOffset;//文字居中显示
        canvas.drawText(sign, rowX, rowY, paint);
    }

    @Override
    public Drawable getDrawable() {
        return super.getDrawable();
    }
}
