package com.tongdaxing.erban.libcommon.net.rxnet;

import android.content.Context;

import com.tongdaxing.xchat_framework.http_image.http.Cache;
import com.tongdaxing.xchat_framework.http_image.http.DefaultRequestProcessor;
import com.tongdaxing.xchat_framework.http_image.http.DiskCache;
import com.tongdaxing.xchat_framework.http_image.http.DownloadRequest;
import com.tongdaxing.xchat_framework.http_image.http.ProgressListener;
import com.tongdaxing.xchat_framework.http_image.http.Request;
import com.tongdaxing.xchat_framework.http_image.http.RequestProcessor;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.http.SameThreadRequestProcessor;

import java.util.Map;

/**
 * Http管理器
 *
 * @author zhongyongsheng on 14-6-11.
 */
public class RequestManager {

    private static RequestManager mFactory;
    private RequestProcessor mCommonProcessor;
    private RequestProcessor mSameThreadProcessor;
    private Cache mCache;

    private RequestManager() {
    }

    public static synchronized RequestManager instance() {
        if (mFactory == null) {
            mFactory = new RequestManager();
        }
        return mFactory;
    }

    public synchronized void init(Context context, String cacheDir) {
        mCache = new DiskCache(DiskCache.getCacheDir(context, cacheDir), 5 * 1024 * 1024, 0.2f);
        mCache.initialize();

        mCommonProcessor = new DefaultRequestProcessor(2, "Http_");
        mCommonProcessor.start();

        mSameThreadProcessor = new SameThreadRequestProcessor();
    }

    public Cache getCache() {
        return mCache;
    }

    /**
     * 下载文件请求,调用者保证下载到本地文件的目录已经创建好
     *
     * @param url              下载服务器路径
     * @param downloadFilePath 下载本地路径
     * @param successListener  成功回调
     * @param errorListener    失败回调
     * @param progressListener 进度回调
     * @return
     */
    public DownloadRequest submitDownloadRequest(String url,
                                                 Map<String, String> headers,
                                                 String downloadFilePath,
                                                 ResponseListener<String> successListener,
                                                 ResponseErrorListener errorListener,
                                                 ProgressListener progressListener) {
        return submitDownloadRequest(url, headers, downloadFilePath, successListener,
                errorListener, progressListener, false);
    }

    /**
     * 下载文件请求,调用者保证下载到本地文件的目录已经创建好
     *
     * @param url                 下载服务器路径
     * @param downloadFilePath    下载本地路径
     * @param successListener     成功回调
     * @param errorListener       失败回调
     * @param progressListener    进度回调
     * @param useContinueDownload 是否使用断点续传
     * @return
     */
    public DownloadRequest submitDownloadRequest(String url,
                                                 Map<String, String> headers,
                                                 String downloadFilePath,
                                                 ResponseListener<String> successListener,
                                                 ResponseErrorListener errorListener,
                                                 ProgressListener progressListener,
                                                 boolean useContinueDownload) {
        if (url == null
                || downloadFilePath == null
                || successListener == null
                || errorListener == null
                || progressListener == null) {
            return null;
        }
        DownloadRequest req = new DownloadRequest(url, downloadFilePath, successListener,
                errorListener, progressListener, useContinueDownload);
        req.getHeaders().putAll(headers);
        mCommonProcessor.add(req);
        return req;
    }

    /**
     * 创建一个返回String的http请求,并执行
     * add 2014.11.21 by chanry
     *
     * @param url             url
     * @param param           url参数
     * @param successListener 成功回调
     * @param errorListener   失败回调
     * @param cls             返回的对象类型
     * @return
     */
    @Deprecated
    public void submitJsonResultQueryRequest(String url,
                                             Map<String, String> headers,
                                             Map<String, String> param,
                                             ResponseListener<Object> successListener,
                                             ResponseErrorListener errorListener, Class cls) {
        submitJsonResultQueryRequest(url, headers, param, successListener, errorListener, cls, Request.Method.GET);
    }

    @Deprecated
    public void submitJsonResultQueryRequest(String url,
                                             Map<String, String> headers,
                                             Map<String, String> param,
                                             ResponseListener<Object> successListener,
                                             ResponseErrorListener errorListener, Class cls, int method) {
        if (url != null && successListener != null && errorListener != null) {
            OkHttpManager.getInstance().http_retrofit(url, headers, param, successListener, errorListener, method);
        }
    }
}
