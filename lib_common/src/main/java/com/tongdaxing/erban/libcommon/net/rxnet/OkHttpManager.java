package com.tongdaxing.erban.libcommon.net.rxnet;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.internal.$Gson$Types;
import com.miaomiao.ndklib.JniUtils;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_framework.http_image.http.RequestError;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.okhttp.CookiesManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.SignUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;


/**
 * 创建者     polo
 * 创建时间   2017/8/19 17:13
 * 描述	      ${}
 * <p>
 * 更新者     $Author$
 * 更新时间   $Date$
 * 更新描述   ${}
 */

public class OkHttpManager {

    public static final int HTTP_RESPONSE_CODE_SUCCESS = 200;
    private String TAG = "OkHttpManager";
    private static final byte[] LOCKER = new byte[0];
    @SuppressWarnings("StaticFieldLeak")//context为applicationContext
    private static OkHttpManager mInstance;
    private static String kp = "";
    public static Handler mHandler = new Handler(Looper.getMainLooper());
    private Gson mGson = new Gson();
    private Context context;

    public final static int DEFAULT_CODE_ERROR = -1;
    public final static String DEFAULT_MSG_ERROR = "数据异常，请稍后重试！";

    private OkHttpManager(Context context) {
        this.context = context.getApplicationContext();
        OkHttpClient.Builder ClientBuilder = new OkHttpClient.Builder();
        ClientBuilder.readTimeout(20, TimeUnit.SECONDS);//读取超时
        ClientBuilder.connectTimeout(10, TimeUnit.SECONDS);//连接超时
        ClientBuilder.writeTimeout(60, TimeUnit.SECONDS);//写入超时
        ClientBuilder.cookieJar(new CookiesManager(this.context));
        kp = JniUtils.getDk(this.context);
    }

    public static void init(Context context) {
        if (mInstance == null) {
            synchronized (LOCKER) {
                if (mInstance == null) {
                    mInstance = new OkHttpManager(context.getApplicationContext());
                }
            }
        }
    }

    public static OkHttpManager getInstance() {
        return mInstance;
    }

    public static abstract class MyCallBack<T> {
        Type mType;

        public MyCallBack() {
            mType = getSuperclassTypeParameter(getClass());
        }

        static Type getSuperclassTypeParameter(Class<?> subclass) {
            Type superclass = subclass.getGenericSuperclass();
            if (superclass instanceof Class) {
                throw new RuntimeException("Missing type parameter.");
            }
            ParameterizedType parameterized = (ParameterizedType) superclass;
            return $Gson$Types.canonicalize(parameterized.getActualTypeArguments()[0]);
        }

        public abstract void onError(Exception e);

        public abstract void onResponse(T response);
    }

    @Deprecated
    public void http_retrofit(String url,
                              Map<String, String> headers,
                              Map<String, String> param,
                              final ResponseListener<Object> successListener,
                              final ResponseErrorListener errorListener, final int method) {
        String time = System.currentTimeMillis() + "";
        //加上签名
        headers.put("t", time);
        String sign = SignUtils.signParams(url, param, kp, time);
        headers.put("sn", sign);
        retrofit2.Callback<ResponseBody> requestCallback = new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<ResponseBody> call, @NonNull retrofit2.Response<ResponseBody> response) {
                L.info(TAG, "http_retrofit onResponse body: %s, code: %d", response.body(), response.code());
                if (call.request() != null && call.request().url() != null) {
                    LogUtil.d("request_info", "code-->>" + String.valueOf(response.code()) + "\nurl-->>" + call.request().url().toString());
                }
                if (response.body() == null && response.errorBody() == null) {
                    setError(errorListener, DEFAULT_MSG_ERROR);
                } else {
                    dealResponseResult(successListener, errorListener, response.body() == null ? response.errorBody() : response.body());
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<ResponseBody> call, @NonNull Throwable t) {
                L.error(TAG, "onFailure: ", t);
                try {
                    LogUtil.d("url-->>" + call.request().url().toString());
                    setError(errorListener, t.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    setError(errorListener, DEFAULT_MSG_ERROR);
                }
            }
        };
        try {
            encryptParams(param);
        } catch (Exception e) {
            e.printStackTrace();
            errorListener.onErrorResponse(new RequestError("请求失败"));
            return;
        }

        //打印日志
        LogUtil.d("request_info", "url-->>" + url);
        StringBuilder headersBuilder = new StringBuilder();
        for (Map.Entry<String, String> p : headers.entrySet()) {
            headersBuilder.append(p.getKey()).append("=").append(p.getValue()).append("  ");
        }
        LogUtil.d("request_info", "header-->>" + headersBuilder.toString());
        StringBuilder paramsBuilder = new StringBuilder();
        for (Map.Entry<String, String> p : param.entrySet()) {
            paramsBuilder.append(p.getKey()).append("=").append(p.getValue()).append("&");
        }
        LogUtil.d("request_info", "body-->>" + paramsBuilder.substring(0, paramsBuilder.length() - 1));

        //POST
        if (method == com.tongdaxing.xchat_framework.http_image.http.Request.Method.POST) {
            RxNet.create(RxNetService.class).postCallAddHeader(url, headers, param).enqueue(requestCallback);
        } else {//GET
            RxNet.create(RxNetService.class).getCallAddHeader(url, headers, param).enqueue(requestCallback);
        }
    }


    /**
     * 处理返回结果
     */
    @Deprecated
    private void dealResponseResult(ResponseListener<Object> successListener, ResponseErrorListener
            errorListener, ResponseBody data) {
        if (data == null) {
            setError(errorListener, DEFAULT_MSG_ERROR);
        } else {
            String string = null;
            try {
                string = decryptParams(data);
                LogUtil.d("request_info", "response_body-->>" + string);
            } catch (Exception e) {
                e.printStackTrace();
                setError(errorListener, e.getMessage());
            }
            Object o = null;
            if (successListener.mType.toString().equals(Json.class.toString())) {
                o = Json.parse(string);
            } else {
                try {
                    o = mGson.fromJson(string, successListener.mType);
                } catch (Exception e) {
                    setError(errorListener, DEFAULT_MSG_ERROR);
                }
            }
            if (o != null) {
                try {
                    successListener.onResponse(o);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                setError(errorListener, DEFAULT_MSG_ERROR);
            }
        }
    }

    /**
     * 获取签名头
     */
    private Map<String, String> signRequestHeader(String url, Map<String, String> headers, Map<String, String> params) {
        if (headers == null)
            headers = new HashMap<>();
        String time = System.currentTimeMillis() + "";
        //加上签名
        headers.put("t", time);
        String sign = SignUtils.signParams(url, params, kp, time);
        headers.put("sn", sign);
        return headers;
    }

    /**
     * 接口请求错误回调
     */
    private void setError(ResponseErrorListener errorListener, String msg) {
        RequestError requestError = new RequestError(msg);
        try {
            errorListener.onErrorResponse(requestError);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * GET请求
     * Use {@link #doGetRequest(String, Map, HttpRequestCallBack)}
     */
    @Deprecated
    public void doGetRequest(String url, Map<String, String> params, final MyCallBack myCallBack) {
        doGetRequest(url, null, params, myCallBack);
    }

    /**
     * GET请求
     */
    public <T> void doGetRequest(String url, Map<String, String> params, final HttpRequestCallBack<T> callBack) {
        doGetRequest(url, null, params, null, callBack);
    }

    /**
     * GET请求 - 带有请求头的
     * Use {@link #doGetRequest(String, Map, HttpRequestCallBack)}
     */
    @Deprecated
    public void doGetRequest(String url, Map<String, String> header, Map<String, String> params, final MyCallBack myCallBack) {
        doGetRequest(url, header, params, myCallBack, null);
    }

    /**
     * GET请求 - 带有请求头的
     */
    private void doGetRequest(String url, Map<String, String> header, Map<String, String> params, final MyCallBack myCallBack, final HttpRequestCallBack callBack) {
        Map<String, String> headers = signRequestHeader(url, header, params);
        try {
            encryptParams(params);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return;
        }

        //打印日志
        LogUtil.d("request_info", "url-->>" + url);
        StringBuilder headersBuilder = new StringBuilder();
        for (Map.Entry<String, String> p : headers.entrySet()) {
            headersBuilder.append(p.getKey()).append("=").append(p.getValue()).append("  ");
        }
        LogUtil.d("request_info", "header-->>" + headersBuilder.toString());
        StringBuilder paramsBuilder = new StringBuilder();
        for (Map.Entry<String, String> p : params.entrySet()) {
            paramsBuilder.append(p.getKey()).append("=").append(p.getValue()).append("&");
        }
        LogUtil.d("request_info", "body-->>" + paramsBuilder.substring(0, paramsBuilder.length() - 1));

        RxNet.create(RxNetService.class).getCallAddHeader(url, headers, params).enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<ResponseBody> call, @NonNull retrofit2.Response<ResponseBody> response) {
                dealResponseResult(call, response, callBack, myCallBack);
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<ResponseBody> call, @NonNull Throwable t) {
                L.error(TAG, "onFailure:", t);
                if (call.request() != null && call.request().url() != null)
                    LogUtil.d("request_info", "url-->>" + call.request().url().toString());
                try {
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, t.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
                }
            }
        });
    }

    private void onError(MyCallBack myCallBack, HttpRequestCallBack callBack, int code, String message) {
        if (myCallBack != null) {
            try {
                myCallBack.onError(new Exception(message));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (callBack != null) {
            try {
                callBack.onFinish();
                callBack.onFailure(code, message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onResponse(MyCallBack myCallBack, HttpRequestCallBack callBack, int code, String message, Object response) {
        if (myCallBack != null) {
            try {
                myCallBack.onResponse(response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (callBack != null) {
            try {
                callBack.onFinish();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (code == ServiceResult.SC_SUCCESS) {
                try {
                    callBack.onSuccess(message, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    callBack.onFailure(code, message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * post请求
     * use doPostRequest(String url, Map<String, String> headers, Map<String, String> params, boolean isURLDecoderParams, final MyCallBack myCallBack)
     */
    @Deprecated
    public void doPostRequest(String url, Map<String, String> params, MyCallBack myCallBack) {
        doPostRequest(url, null, params, myCallBack);
    }


    /**
     * post请求 - 带有请求头的
     */
    public void doPostRequest(String url, Map<String, String> header, Map<String, String> params, final MyCallBack myCallBack) {
        Map<String, String> headers = signRequestHeader(url, header, params);
        try {
            encryptParams(params);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                myCallBack.onError(new Exception("请求失败"));
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return;
        }

        //打印日志
        if (headers != null) {
            LogUtil.d("request_info", "url-->>" + url);
            StringBuilder headersBuilder = new StringBuilder();
            for (Map.Entry<String, String> p : headers.entrySet()) {
                headersBuilder.append(p.getKey()).append("=").append(p.getValue()).append("  ");
            }
            LogUtil.d("request_info", "header-->>" + headersBuilder.toString());
        }
        if (params != null) {
            StringBuilder paramsBuilder = new StringBuilder();
            for (Map.Entry<String, String> p : params.entrySet()) {
                paramsBuilder.append(p.getKey()).append("=").append(p.getValue()).append("&");
            }
            LogUtil.d("request_info", "body-->>" + paramsBuilder.substring(0, paramsBuilder.length() - 1));
        }

        RxNet.create(RxNetService.class).postCallAddHeader(url, headers, params).enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<ResponseBody> call, @NonNull retrofit2.Response<ResponseBody> response) {
                dealResponseResult(call, response, null, myCallBack);
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<ResponseBody> call, @NonNull Throwable t) {
                L.error(TAG, "onFailure:", t);
                if (call.request() != null && call.request().url() != null)
                    LogUtil.d("request_info", "url-->>" + call.request().url().toString());
                try {
                    try {
                        myCallBack.onError(new Exception(t.getMessage()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        myCallBack.onError(new Exception(DEFAULT_MSG_ERROR));
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 处理返回结果
     */
    private void dealResponseResult(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response, HttpRequestCallBack callBack, MyCallBack myCallBack) {
        if (myCallBack == null && callBack == null)
            return;
        if (response == null) {
            L.error(TAG, "dealResponseResult response is null.");
            onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
            return;
        }
        String string = "";
        if (response.body() != null) {
            try {
                string = decryptParams(response.body());
                LogUtil.d("request_info", "response_body-->>\n" + string);
            } catch (Exception e) {
                L.error(TAG, "dealResponseResult exception:", e);
                onError(myCallBack, callBack, DEFAULT_CODE_ERROR, e.getMessage());
            }
            int code = DEFAULT_CODE_ERROR;
            String message = "";
            Object data = null;
            if (myCallBack != null) {
                try {
                    if (myCallBack.mType.toString().equals(Json.class.toString())) {
                        data = Json.parse(string);
                    } else if (myCallBack.mType.toString().equals(String.class.toString())) {
                        data = string;
                    } else {
                        data = mGson.fromJson(string, myCallBack.mType);
                    }
                    onResponse(myCallBack, callBack, code, message, data);
                } catch (Exception e) {
                    L.error(TAG, "dealResponseResult exception:", e);
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, e.getMessage());
                }
            }
            if (callBack != null) {
                try {
                    JSONObject mResultJson = new JSONObject(string);
                    if (mResultJson.has("data") && !mResultJson.isNull("data")) {
                        String resultStr = mResultJson.getString("data");
                        if (callBack.mType.toString().equals(Json.class.toString())) {
                            data = Json.parse(resultStr);
                        } else if (callBack.mType.toString().equals(String.class.toString())) {
                            data = resultStr;
                        } else {
                            data = mGson.fromJson(resultStr, callBack.mType);
                        }
                    }
                    if (mResultJson.has("code") && !mResultJson.isNull("code")) {
                        code = mResultJson.getInt("code");
                    } else {
                        code = DEFAULT_CODE_ERROR;
                    }
                    if (mResultJson.has("message") && !mResultJson.isNull("message")) {
                        message = mResultJson.getString("message");
                    } else {
                        message = DEFAULT_MSG_ERROR;
                    }
                    onResponse(myCallBack, callBack, code, message, data);
                } catch (Exception e) {
                    L.error(TAG, "dealResponseResult exception:", e);
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, e.getMessage());
                }
            }
        } else {
            if (response.errorBody() != null) {
                try {
                    string = response.errorBody().string();
                    L.error(TAG, "dealResponseResult string: %s", string);
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, string);
                } catch (IOException e) {
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
                }
            } else {
                onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
            }
        }
    }

    private void encryptParams(Map<String, String> params) throws Exception {
        if (BasicConfig.isDebug) {//debug版本不加密参数 返回体也不会自动加密
            return;
        }
        if (params != null && !params.isEmpty()) {
            StringBuilder paramsBuilder = new StringBuilder();
            for (Map.Entry<String, String> param : params.entrySet()) {
                if (param.getValue() != null) {
                    paramsBuilder.append(param.getKey()).append("=").append(URLEncoder.encode(param.getValue(), "utf-8")).append("&");
                }
            }
            String paramsStr = paramsBuilder.substring(0, paramsBuilder.length() - 1);//去掉最后一个&（最后一个必然会是&）
            LogUtil.d("request_info", "pre_encrypt_body-->>" + paramsStr);
            params.clear();
            params.put("ed", JniUtils.encryptAes(context, paramsStr));//只传加密后参数ed
        }
    }

    private String decryptParams(ResponseBody data) throws Exception {
        String bodyStr;
        Json bodyJson;
        try {
            bodyStr = data.string();
            bodyJson = Json.parse(bodyStr);
        } catch (Exception e) {
            L.error(TAG, "decryptParams:", e);
            e.printStackTrace();
            throw new Exception(DEFAULT_MSG_ERROR);
        }
        String string;
        if (bodyJson == null || !bodyJson.has("ed")) {
            string = bodyStr;
        } else {
            try {
                string = JniUtils.decryptAes(context, bodyJson.getString("ed"));
            } catch (Exception e) {
                L.error(TAG, "decryptParams:", e);
                e.printStackTrace();
                throw new Exception(DEFAULT_MSG_ERROR);
            }
        }
        return string;
    }

}