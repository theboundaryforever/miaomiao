package com.tongdaxing.erban.libcommon.utils;

/**
 * Created by Administrator on 2018/3/7.
 */

public interface SpEvent {
    String linkedMeShareUid = "linkedMeShareUid";
    String linkedMeChannel = "linkedMeChannel";
    String onKickRoomInfo = "onKickRoomId";

    String roomUid = "roomUid";
    String time = "time";
    String cache_uid = "cache_uid";
    String not_hot_menu = "not_hot_menu";
    String config_key = "config_key";
    String headwearUrl = "headwearUrl";
    String cpUserId = "cpUserId";
    String cpGiftLevel = "cpGiftLevel";
    String search_history = "search_history";
    String first_open = "first_open";

    String cleckLoginTime = "cleck_login_time";

    String roomBottomMarkClear = "room_bottom_mark_clear_";
    String roomAudioSetting_audioMonitorEnable = "room_audio_setting_audio_monitor_enable_";
    String roomAudioSetting_sound_filter = "room_audio_setting_sound_filter_";

    String messageIgnoreCache = "message_ignore_cache";
    String audioCardMarkClear = "audio_card_mark_clear_";

    String audioMatchTutorialShowed = "audio_match_tutorial_showed";//声音匹配是否已显示教程
    String audioMatchTipShowed = "audio_match_tip_showed";//声音匹配首页提示是否已显示
    String audioMatchLikeSelectedGiftId = "audio_match_like_selected_gift_id";//声音匹配喜欢选择的礼物id

    String SP_ACTIVATING_SAVE = "activatingSave";//用户第一次使用应用需要激活
    String SP_KS_ACTIVE = "ksActive";//用户第一次使用应用需要激活 快手imei激活

    String createRoomNoticeFans = "createRoomNoticeFans";
}
