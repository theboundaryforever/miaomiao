package com.erban.ui.mvp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import com.erban.ui.baseview.BaseFrameLayout;


/**
 */

public abstract class MVPBaseFrameLayout<UIInterface, Presenter extends BasePresenter<UIInterface>>
        extends BaseFrameLayout {
    private boolean isInit = false;
    protected Presenter mPresenter;

    public MVPBaseFrameLayout(@NonNull Context context) {
        super(context);
        init();
    }

    public MVPBaseFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MVPBaseFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private final void init() {
        mPresenter = createPresenter();
        if (mPresenter != null) {
            mPresenter.attachView((UIInterface) this);
        }
        if (getContentViewId() != 0) {
            LayoutInflater.from(getContext()).inflate(getContentViewId(), this);
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        initView();
        if (mPresenter != null) {
            mPresenter.onCreate();
        }
    }

    @Override
    public void onCreateView() {
        super.onCreateView();
        initView();
        if (mPresenter != null) {
            mPresenter.onCreateView();
        }
    }

    private void initView() {
        if (!isInit) {
            findView();
            setView();
            setListener();
            isInit = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPresenter != null) {
            mPresenter.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPresenter != null) {
            mPresenter.onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.onDestroyView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.release();
            mPresenter.onDestroy();
            mPresenter.detachView();
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
    }

    @Override
    public void onSupportInvisible() {

    }

    @Override
    public void onSupportVisible() {

    }

    /**
     * Presenter创建方法
     *
     * @return
     */
    public abstract int getContentViewId();

    @NonNull
    protected abstract Presenter createPresenter();

    protected abstract void findView();

    protected abstract void setView();

    protected abstract void setListener();

}
