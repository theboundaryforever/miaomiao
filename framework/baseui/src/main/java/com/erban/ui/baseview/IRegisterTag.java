package com.erban.ui.baseview;

/**
 * Created by logwee on 2019/1/11.
 */
public interface IRegisterTag {

    void onRegistered(IViewLifecycleRegister register);

}
