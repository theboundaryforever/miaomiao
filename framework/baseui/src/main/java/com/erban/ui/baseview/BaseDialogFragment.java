package com.erban.ui.baseview;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tcloud.core.log.L;

import me.yokeyword.fragmentation.ExtraTransaction;
import me.yokeyword.fragmentation.ISupportFragment;
import me.yokeyword.fragmentation.SupportFragmentDelegate;
import me.yokeyword.fragmentation.anim.FragmentAnimator;


/**
 * Created by chen on 2018/11/21.
 */
public abstract class BaseDialogFragment extends DialogFragment implements ISupportFragment {

    final SupportFragmentDelegate mDelegate = new SupportFragmentDelegate(this);
    private static final String TAG = "BaseDialogFragment";
    private static final String KEY_BUNDLE = "DIALOG_FRAGMENT_KEY.BUNDLE";


    protected Activity mActivity;
    private Bundle mBundle;
    protected View mContentView;
    private LifecycleViewMgr mLifecycleViewMgr = new LifecycleViewMgr();
    private boolean mIsRestoreFromMemory = false;
    protected Handler mHandler = new Handler();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mIsRestoreFromMemory = true;
            if (mBundle == null) {
                mBundle = savedInstanceState.getBundle(KEY_BUNDLE);
            }
        }
    }


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        L.debug(TAG, "onCreateView: " + this);
        initBefore();

        ActivityHelper.setFactory(inflater, mLifecycleViewMgr);
        mContentView = inflater.inflate(getContentViewId(), container, false);

        findView();
        setView();
        setListener();
        mLifecycleViewMgr.onCreateView();
        return mContentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mLifecycleViewMgr.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mLifecycleViewMgr.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mLifecycleViewMgr.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mLifecycleViewMgr.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLifecycleViewMgr.onDestroyView();

        mContentView = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLifecycleViewMgr.onDestroy();
        mContentView = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mBundle != null) {
            outState.putBundle(KEY_BUNDLE, mBundle);
        }
    }

    public boolean isRestoreFromMemory() {
        return mIsRestoreFromMemory;
    }

    // ondestroyview() 和 ondestroy()之间有间隙。此时用这个判断
    public boolean isViewDestroyed() {
        return mContentView == null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }


    @Nullable
    @Override
    public View getView() {
        return mContentView;
    }

    public String getTagText() {
        return getClass().getSimpleName();
    }

    public void setBundle(Bundle args) {
        mBundle = args;
    }

    public Bundle getBundle() {
        return mBundle;
    }

    /**
     * 从这个fragment的根view(the one returned by {@link #onCreateView})中通过id查找指定的view<br>
     * 注意这个方法必须在{@link #onCreateView}调用后才能使用, 并且{@link #onCreateView}不返加null<br>
     *
     * @param id
     * @return
     */
    public View findViewById(int id) {
        if (mContentView != null) {
            return mContentView.findViewById(id);
        }
        return null;
    }

    public boolean isAttached() {
        return mActivity != null;
    }

    /**
     * 该Fragment内容layoutId
     *
     * @return
     */
    public abstract int getContentViewId();

    public abstract void initBefore();

    public abstract void findView();

    public abstract void setView();

    public abstract void setListener();

    /**
     * 是否要后台加载view，true为是
     *
     * @return
     */
    @Deprecated
    public boolean isCanBackDrawView() {
        return false;
    }

    @Override
    public SupportFragmentDelegate getSupportDelegate() {
        return mDelegate;
    }

    @Override
    public ExtraTransaction extraTransaction() {
        return mDelegate.extraTransaction();
    }

    @Override
    public void enqueueAction(Runnable runnable) {
        mDelegate.enqueueAction(runnable);
    }

    @Override
    public void post(Runnable runnable) {
        mDelegate.post(runnable);
    }

    @Override
    public void onEnterAnimationEnd(@Nullable Bundle savedInstanceState) {
        mDelegate.onEnterAnimationEnd(savedInstanceState);
    }

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        mDelegate.onLazyInitView(savedInstanceState);
    }

    @Override
    public void onSupportVisible() {
        mDelegate.onSupportVisible();
    }

    @Override
    public void onSupportInvisible() {
        mDelegate.onSupportInvisible();
    }

    @Override
    public boolean isSupportVisible() {
        return mDelegate.isSupportVisible();
    }

    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        return mDelegate.onCreateFragmentAnimator();
    }

    @Override
    public FragmentAnimator getFragmentAnimator() {
        return mDelegate.getFragmentAnimator();
    }

    @Override
    public void setFragmentAnimator(FragmentAnimator fragmentAnimator) {
        mDelegate.setFragmentAnimator(fragmentAnimator);
    }

    @Override
    public void setFragmentResult(int resultCode, Bundle bundle) {
        mDelegate.setFragmentResult(resultCode, bundle);
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        mDelegate.onFragmentResult(requestCode, resultCode, data);
    }

    @Override
    public void onNewBundle(Bundle args) {
        mDelegate.onNewBundle(args);
    }

    @Override
    public void putNewBundle(Bundle newBundle) {
        mDelegate.putNewBundle(newBundle);
    }

    @Override
    public boolean onBackPressedSupport() {
        return mDelegate.onBackPressedSupport();
    }
}
