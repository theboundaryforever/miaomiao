package com.erban.ui.baseview;

/**
 * Created by logwee on 2018/11/23.
 */
public interface IViewLifecycleRegister {

    void registerLifecycleView(IViewLifecycle view);

    void unregisterLifecycleView(IViewLifecycle view);

}
