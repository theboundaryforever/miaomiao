/**
 *
 */
package com.erban.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentationMagician;

import com.erban.ui.baseview.BaseFragment;
import com.tcloud.core.log.L;

import java.util.List;

import me.yokeyword.fragmentation.ISupportFragment;


/**
 * Fragment缓存池，复用Fragment
 */
public class FragmentUtil {

    public static BaseFragment getFragment(Class<?> clazz, FragmentManager fragmentManager) {
        if (clazz != null) {
            BaseFragment fragment = (BaseFragment) fragmentManager.findFragmentByTag(clazz.getSimpleName());
            if (fragment == null) {
                try {
                    fragment = (BaseFragment) clazz.newInstance();
                } catch (InstantiationException e) {
                    L.error("FragmentUtil", e.toString());
                } catch (IllegalAccessException e) {
                    L.error("FragmentUtil", e.toString());
                }
            }
            return fragment;
        }
        return null;
    }

    public static ISupportFragment getTopVisibleFragment(FragmentManager fragmentManager) {
        List<Fragment> fragmentList = FragmentationMagician.getActiveFragments(fragmentManager);
        if (fragmentList == null) return null;

        for (int i = fragmentList.size() - 1; i >= 0; i--) {
            Fragment fragment = fragmentList.get(i);
            if (fragment instanceof ISupportFragment) {
                ISupportFragment iFragment = (ISupportFragment) fragment;
                if (iFragment.isSupportVisible()) {
                    return iFragment;
                }
            }
        }
        return null;
    }
}
