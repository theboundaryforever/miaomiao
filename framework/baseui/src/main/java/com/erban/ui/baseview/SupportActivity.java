package com.erban.ui.baseview;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.CallSuper;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentationMagician;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;

import java.util.List;

import me.yokeyword.fragmentation.ExtraTransaction;
import me.yokeyword.fragmentation.ISupportActivity;
import me.yokeyword.fragmentation.ISupportFragment;
import me.yokeyword.fragmentation.SupportActivityDelegate;
import me.yokeyword.fragmentation.SupportHelper;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

/**
 * Base class for activities that use the support-based
 * {@link ISupportActivity} and
 * {@link AppCompatActivity} APIs.
 * <p>
 * Created by YoKey on 17/6/20.
 *
 * @link(https://github.com/YoKeyword/Fragmentation/wiki/2.-API)
 */
public abstract class SupportActivity extends AppCompatActivity implements ISupportActivity, IBaseActivity {

    final SupportActivityDelegate mDelegate = new SupportActivityDelegate(this);
    protected LifeCycleManager mLifeCycleManager = new LifeCycleManager(this);
    private LifecycleViewMgr mLifecycleViewMgr = new LifecycleViewMgr();
    private boolean mHasStateSaved = false;
    protected Handler mHandler = new Handler();
    private boolean mWillDestroyCalled = false;

    @Override
    public SupportActivityDelegate getSupportDelegate() {
        return mDelegate;
    }

    /**
     * Perform some extra transactions.
     * 额外的事务：自定义Tag，添加SharedElement动画，操作非回退栈Fragment
     */
    @Override
    public ExtraTransaction extraTransaction() {
        return mDelegate.extraTransaction();
    }

    @Override
    @CallSuper
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDelegate.onCreate(savedInstanceState);
        ARouter.getInstance().inject(this);
    }

    @Override
    @CallSuper
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mLifecycleViewMgr.onNewIntent(intent);
    }

    @Override
    @CallSuper
    protected void onStart() {
        super.onStart();
        mLifecycleViewMgr.onStart();
    }

    @Override
    @CallSuper
    protected void onStop() {
        super.onStop();
        mLifecycleViewMgr.onStop();
    }

    @Override
    @CallSuper
    protected void onResume() {
        super.onResume();
        mHasStateSaved = false;
        mLifecycleViewMgr.onResume();
    }

    @Override
    @CallSuper
    protected void onPause() {
        super.onPause();
        mLifecycleViewMgr.onPause();
        if (!mWillDestroyCalled && isFinishing()) {
            onWillDestroy();
        }
    }

    @Override
    @CallSuper
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mLifecycleViewMgr.onWindowFocusChanged(hasFocus);
    }

    @Override
    @CallSuper
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mHasStateSaved = false;
        mLifecycleViewMgr.onActivityResult(requestCode, resultCode, data);
        handleFragmentActivityResult(getSupportFragmentManager(), requestCode, resultCode, data);
    }

    private void handleFragmentActivityResult(android.support.v4.app.FragmentManager fragmentManager, int requestCode, int resultCode, Intent data) {
        if (fragmentManager != null) {
            List<Fragment> fragmentList = FragmentationMagician.getActiveFragments(fragmentManager);
            if (fragmentList != null && fragmentList.size() > 0) {
                for (int i = 0; i < fragmentList.size(); i++) {
                    Fragment fragment = fragmentList.get(i);
                    if (fragment != null) {
                        fragment.onActivityResult(requestCode, resultCode, data);
                        handleFragmentActivityResult(fragment.getChildFragmentManager(), requestCode, resultCode, data);
                    }
                }
            }
        }
    }

    @Override
    @CallSuper
    protected void onSaveInstanceState(Bundle outState) {
        mHasStateSaved = true;
        super.onSaveInstanceState(outState);
    }

    @Override
    @CallSuper
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mHasStateSaved = false;
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    @CallSuper
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDelegate.onPostCreate(savedInstanceState);
    }

    //将要销毁
    //解决activity onDestroy回调比较慢造成状态不一致的问题
    @CallSuper
    protected void onWillDestroy() {
        if (mWillDestroyCalled) {
            return;
        }
        L.info(this, "onWillDestroy");
        mWillDestroyCalled = true;
        mDelegate.onDestroy();
        mLifeCycleManager.onDestroy();
        mLifecycleViewMgr.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    @CallSuper
    protected void onDestroy() {
        if (!mWillDestroyCalled) {
            onWillDestroy();
        }
        super.onDestroy();
    }

    /**
     * Note： return mDelegate.dispatchTouchEvent(ev) || super.dispatchTouchEvent(ev);
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            return mDelegate.dispatchTouchEvent(ev) || super.dispatchTouchEvent(ev);
        } catch (ArrayIndexOutOfBoundsException e) {
            //fix TextView getOffsetForHorizontal Crash
            CoreUtils.crashIfDebug(e, "DispatchTouchEvent Exception");
            return false;
        }
    }

    /**
     * 不建议复写该方法,请使用 {@link #onBackPressedSupport} 代替
     */
    @Override
    final public void onBackPressed() {
        mDelegate.onBackPressed();
    }

    /**
     * 该方法回调时机为,Activity回退栈内Fragment的数量 小于等于1 时,默认finish Activity
     * 请尽量复写该方法,避免复写onBackPress(),以保证SupportFragment内的onBackPressedSupport()回退事件正常执行
     */
    @Override
    public void onBackPressedSupport() {
        mDelegate.onBackPressedSupport();
    }

    /**
     * 获取设置的全局动画 copy
     *
     * @return FragmentAnimator
     */
    @Override
    public FragmentAnimator getFragmentAnimator() {
        return mDelegate.getFragmentAnimator();
    }

    /**
     * Set all fragments animation.
     * 设置Fragment内的全局动画
     */
    @Override
    public void setFragmentAnimator(FragmentAnimator fragmentAnimator) {
        mDelegate.setFragmentAnimator(fragmentAnimator);
    }

    /**
     * Set all fragments animation.
     * 构建Fragment转场动画
     * <p/>
     * 如果是在Activity内实现,则构建的是Activity内所有Fragment的转场动画,
     * 如果是在Fragment内实现,则构建的是该Fragment的转场动画,此时优先级 > Activity的onCreateFragmentAnimator()
     *
     * @return FragmentAnimator对象
     */
    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        return mDelegate.onCreateFragmentAnimator();
    }

    /****************************************以下为可选方法(Optional methods)******************************************************/

    /**
     * 加载根Fragment, 即Activity内的第一个Fragment 或 Fragment内的第一个子Fragment
     *
     * @param containerId 容器id
     * @param toFragment  目标Fragment
     */
    public void loadRootFragment(int containerId, @NonNull ISupportFragment toFragment) {
        if (toFragment == null) {
            CoreUtils.crashIfDebug("loadRootFragment toFragment == null");
            return;
        }
        mDelegate.loadRootFragment(containerId, toFragment);
    }

    public void loadRootFragment(int containerId, ISupportFragment toFragment, boolean addToBackStack, boolean allowAnimation) {
        if (toFragment == null) {
            CoreUtils.crashIfDebug("loadRootFragment toFragment == null");
            return;
        }
        mDelegate.loadRootFragment(containerId, toFragment, addToBackStack, allowAnimation);
    }

    /**
     * 加载多个同级根Fragment,类似Wechat, QQ主页的场景
     */
    public void loadMultipleRootFragment(int containerId, int showPosition, ISupportFragment... toFragments) {
        mDelegate.loadMultipleRootFragment(containerId, showPosition, toFragments);
    }

    /**
     * show一个Fragment,hide其他同栈所有Fragment
     * 使用该方法时，要确保同级栈内无多余的Fragment,(只有通过loadMultipleRootFragment()载入的Fragment)
     * <p>
     * 建议使用更明确的{@link #showHideFragment(ISupportFragment, ISupportFragment)}
     *
     * @param showFragment 需要show的Fragment
     */
    public void showHideFragment(ISupportFragment showFragment) {
        if (showFragment == null) {
            CoreUtils.crashIfDebug("showHideFragment showFragment == null");
            return;
        }
        mDelegate.showHideFragment(showFragment);
    }

    /**
     * show一个Fragment,hide一个Fragment ; 主要用于类似微信主页那种 切换tab的情况
     */
    public void showHideFragment(ISupportFragment showFragment, ISupportFragment hideFragment) {
        mDelegate.showHideFragment(showFragment, hideFragment);
    }

    /**
     * It is recommended to use {@link BaseFragment#start(ISupportFragment)}.
     */
    public void start(ISupportFragment toFragment) {
        if (toFragment == null) {
            CoreUtils.crashIfDebug("start toFragment == null");
            return;
        }
        mDelegate.start(toFragment);
    }

    /**
     * It is recommended to use {@link BaseFragment#start(ISupportFragment, int)}.
     *
     * @param launchMode Similar to Activity's LaunchMode.
     */
    public void start(ISupportFragment toFragment, @ISupportFragment.LaunchMode int launchMode) {
        if (toFragment == null) {
            CoreUtils.crashIfDebug("start toFragment == null");
            return;
        }
        mDelegate.start(toFragment, launchMode);
    }

    /**
     * It is recommended to use {@link BaseFragment#startForResult(ISupportFragment, int)}.
     * Launch an fragment for which you would like a result when it poped.
     */
    public void startForResult(ISupportFragment toFragment, int requestCode) {
        mDelegate.startForResult(toFragment, requestCode);
    }

    /**
     * It is recommended to use {@link BaseFragment#startWithPop(ISupportFragment)}.
     * Launch a fragment while poping self.
     */
    public void startWithPop(ISupportFragment toFragment) {
        mDelegate.startWithPop(toFragment);
    }

    /**
     * It is recommended to use {@link BaseFragment#replaceFragment(ISupportFragment, boolean)}.
     */
    public void replaceFragment(ISupportFragment toFragment, boolean addToBackStack) {
        mDelegate.replaceFragment(toFragment, addToBackStack);
    }

    /**
     * Pop the fragment.
     */
    public void pop() {
        mDelegate.pop();
    }

    /**
     * Pop the last fragment transition from the manager's fragment
     * back stack.
     * <p>
     * 出栈到目标fragment
     *
     * @param targetFragmentClass   目标fragment
     * @param includeTargetFragment 是否包含该fragment
     */
    public void popTo(Class<?> targetFragmentClass, boolean includeTargetFragment) {
        mDelegate.popTo(targetFragmentClass, includeTargetFragment);
    }

    /**
     * If you want to begin another FragmentTransaction immediately after popTo(), use this method.
     * 如果你想在出栈后, 立刻进行FragmentTransaction操作，请使用该方法
     */
    public void popTo(Class<?> targetFragmentClass, boolean includeTargetFragment, Runnable afterPopTransactionRunnable) {
        mDelegate.popTo(targetFragmentClass, includeTargetFragment, afterPopTransactionRunnable);
    }

    public void popTo(Class<?> targetFragmentClass, boolean includeTargetFragment, Runnable afterPopTransactionRunnable, int popAnim) {
        mDelegate.popTo(targetFragmentClass, includeTargetFragment, afterPopTransactionRunnable, popAnim);
    }

    /**
     * 当Fragment根布局 没有 设定background属性时,
     * Fragmentation默认使用Theme的android:windowbackground作为Fragment的背景,
     * 可以通过该方法改变其内所有Fragment的默认背景。
     */
    public void setDefaultFragmentBackground(@DrawableRes int backgroundRes) {
        mDelegate.setDefaultFragmentBackground(backgroundRes);
    }

    /**
     * 得到位于栈顶Fragment
     */
    public ISupportFragment getTopFragment() {
        return SupportHelper.getTopFragment(getSupportFragmentManager());
    }

    /**
     * 获取栈内的fragment对象
     */
    public <T extends ISupportFragment> T findFragment(Class<T> fragmentClass) {
        return SupportHelper.findFragment(getSupportFragmentManager(), fragmentClass);
    }

    @Override
    public void registerLifecycleView(IViewLifecycle view) {
        mLifecycleViewMgr.registerLifecycleView(view);
    }

    @Override
    public void unregisterLifecycleView(IViewLifecycle view) {
        mLifecycleViewMgr.unregisterLifecycleView(view);
    }

    //Use getSupportFragmentManager() instead
    @Deprecated
    @Override
    public FragmentManager getFragmentManager() {
        return super.getFragmentManager();
    }

    /**
     * @return default return {@link LifeCycleManager#MESSAGE_LIFECYCLE_RESUME_PAUSE}
     */
    protected int getMessageLifeCycle() {
        return LifeCycleManager.MESSAGE_LIFECYCLE_RESUME_PAUSE;
    }

    @Override
    public boolean isActivityCreated() {
        return mLifeCycleManager.isCreated();
    }

    @Override
    public boolean isActivityResumed() {
        return mLifeCycleManager.isResumed();
    }

    @Override
    public boolean isActivityStarted() {
        return mLifeCycleManager.isStarted();
    }

    @Override
    public boolean isActivityPaused() {
        return mLifeCycleManager.isPaused();
    }

    @Override
    public boolean isActivityStopped() {
        return mLifeCycleManager.isStopped();
    }

    @Override
    public boolean isActivityDestroyed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return isDestroyed();
        }
        return mLifeCycleManager.isDestroyed();
    }

    @Override
    public boolean checkActivityValid() {
        if (isFinishing()) {
            return false;
        }

        if (Build.VERSION.SDK_INT >= 17 && isDestroyed()) {
            return false;
        }
        return true;
    }

    @Override
    public boolean hasStateSaved() {
        return mHasStateSaved;
    }

    @Override
    public void post(Runnable runnable) {

    }
}
