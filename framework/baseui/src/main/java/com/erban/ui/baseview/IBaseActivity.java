package com.erban.ui.baseview;

/**
 * Created by zxs on 17/5/31.
 */

public interface IBaseActivity extends IViewLifecycleRegister {

    boolean hasStateSaved();

    boolean checkActivityValid();

    boolean isActivityCreated();

    boolean isActivityResumed();

    boolean isActivityStarted();

    boolean isActivityPaused();

    boolean isActivityStopped();

    boolean isActivityDestroyed();


}
