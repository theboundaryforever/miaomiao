package com.erban.ui.baseview;

import android.content.Intent;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by zxs on 17/5/31.
 */

public class LifecycleViewMgr implements IViewLifecycle, IViewLifecycleRegister {

    private List<IViewLifecycle> mLiftcycleList = new CopyOnWriteArrayList<>();

    public LifecycleViewMgr() {

    }

    @Override
    public IViewLifecycle getLifecycleDelegate() {
        return this;
    }

    @Override
    public void registerLifecycleView(IViewLifecycle view) {
        if (!mLiftcycleList.contains(view)) {
            if (view instanceof IRegisterTag) {
                ((IRegisterTag) view).onRegistered(this);
            }
            mLiftcycleList.add(view);
        }
    }

    @Override
    public void unregisterLifecycleView(IViewLifecycle view) {
        mLiftcycleList.remove(view);
    }

    @Override
    public void onNewIntent(Intent intent) {
        for (IViewLifecycle view : mLiftcycleList) {
            view.onNewIntent(intent);
        }
    }

    //交由viewGroup的onAttachToWindow
    @Override
    public void onCreate() {

    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onStart() {
        for (IViewLifecycle view : mLiftcycleList) {
            view.onStart();
        }
    }

    @Override
    public void onResume() {
        for (IViewLifecycle view : mLiftcycleList) {
            view.onResume();
        }
    }

    @Override
    public void onPause() {
        for (IViewLifecycle view : mLiftcycleList) {
            view.onPause();
        }
    }

    @Override
    public void onStop() {
        for (IViewLifecycle view : mLiftcycleList) {
            view.onStop();
        }
    }

    @Override
    public void onDestroyView() {

    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        for (IViewLifecycle view : mLiftcycleList) {
            view.onWindowFocusChanged(hasWindowFocus);
        }
    }

    @Override
    public void onDestroy() {
        for (IViewLifecycle view : mLiftcycleList) {
            view.onDestroy();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (IViewLifecycle view : mLiftcycleList) {
            view.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onSupportVisible() {

    }

    @Override
    public void onSupportInvisible() {

    }

    @Override
    public void onAttachedToWindow() {

    }

    @Override
    public void onDetachedFromWindow() {

    }
}
