package com.erban.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.tcloud.core.CoreUtils;
import com.tongdaxing.erban.baseui.R;

import java.lang.ref.WeakReference;

/**
 * Created by logwee on 2017/11/10.
 */

public class BaseViewStub extends View {

    private int mInflatedId;
    private int mLayoutResource;

    private WeakReference<View> mInflatedViewRef;

    private LayoutInflater mInflater;
    private BaseViewStub.OnInflateListener mInflateListener;

    public BaseViewStub(Context context) {
        super(context);
    }

    public BaseViewStub(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public BaseViewStub(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.BaseViewStub);
        mInflatedId = a.getResourceId(R.styleable.BaseViewStub_stub_inflatedId, NO_ID);
        mLayoutResource = a.getResourceId(R.styleable.BaseViewStub_stub_layout, 0);
        a.recycle();

        setVisibility(GONE);
        setWillNotDraw(true);
    }

    @IdRes
    public int getInflatedId() {
        return mInflatedId;
    }

    public void setInflatedId(@IdRes int inflatedId) {
        mInflatedId = inflatedId;
    }

    @LayoutRes
    public int getLayoutResource() {
        return mLayoutResource;
    }

    public void setLayoutResource(@LayoutRes int layoutResource) {
        mLayoutResource = layoutResource;
    }

    public void setLayoutInflater(LayoutInflater inflater) {
        mInflater = inflater;
    }

    public LayoutInflater getLayoutInflater() {
        return mInflater;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(0, 0);
    }

    @Override
    public void draw(Canvas canvas) {
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
    }

    @Override
    public void setVisibility(int visibility) {
        if (mInflatedViewRef != null) {
            View view = mInflatedViewRef.get();
            if (view != null) {
                view.setVisibility(visibility);
            } else {
                throw new IllegalStateException("setVisibility called on un-referenced view");
            }
        } else {
            super.setVisibility(visibility);
            if (visibility == VISIBLE || visibility == INVISIBLE) {
                inflate();
            }
        }
    }

    public void setStubView(View view) {
        final ViewParent viewParent = getParent();

        if (view != null && viewParent != null && viewParent instanceof ViewGroup) {
            if (view.getParent() != null) {
                CoreUtils.crashIfDebug("parent != null");
                return;
            }
            if (mInflatedId != NO_ID) {
                view.setId(mInflatedId);
            }

            final ViewGroup parent = (ViewGroup) viewParent;
            final int index = parent.indexOfChild(this);
            parent.removeViewInLayout(this);

            final ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (layoutParams != null) {
                parent.addView(view, index, layoutParams);
            } else {
                parent.addView(view, index);
            }

            if (mInflateListener != null) {
                mInflateListener.onInflate(this, view);
            }
        }
    }

    public View inflate() {
        final ViewParent viewParent = getParent();

        if (viewParent != null && viewParent instanceof ViewGroup) {
            if (mLayoutResource != 0) {
                final ViewGroup parent = (ViewGroup) viewParent;
                final LayoutInflater factory;
                if (mInflater != null) {
                    factory = mInflater;
                } else {
                    factory = LayoutInflater.from(getContext());
                }
                final View view = factory.inflate(mLayoutResource, parent,
                        false);

                if (mInflatedId != NO_ID) {
                    view.setId(mInflatedId);
                }

                final int index = parent.indexOfChild(this);
                parent.removeViewInLayout(this);

                final ViewGroup.LayoutParams layoutParams = getLayoutParams();
                if (layoutParams != null) {
                    parent.addView(view, index, layoutParams);
                } else {
                    parent.addView(view, index);
                }

                mInflatedViewRef = new WeakReference<View>(view);

                if (mInflateListener != null) {
                    mInflateListener.onInflate(this, view);
                }

                return view;
            } else {
                throw new IllegalArgumentException("ViewStub must have a valid layoutResource");
            }
        } else {
            throw new IllegalStateException("ViewStub must have a non-null ViewGroup viewParent");
        }
    }

    public void setOnInflateListener(BaseViewStub.OnInflateListener inflateListener) {
        mInflateListener = inflateListener;
    }

    public static interface OnInflateListener {
        /**
         * Invoked after a ViewStub successfully inflated its layout resource.
         * This method is invoked after the inflated view was added to the
         * hierarchy but before the layout pass.
         *
         * @param stub     The ViewStub that initiated the inflation.
         * @param inflated The inflated View.
         */
        void onInflate(BaseViewStub stub, View inflated);
    }
}
