package com.erban.ui.mvp;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.erban.ui.baseview.SupportActivity;


/**
 * MVP模式基础Activity，所有使用Fragment的Activity必须继承该类
 */

public abstract class MVPBaseActivity<UIInterface, Presenter extends BasePresenter<UIInterface>>
        extends SupportActivity {

    protected Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int contentViewId = getContentViewId();
        if (contentViewId != 0) {
            setContentView(getContentViewId());
        }
        findView();
        setView();
        setListener();
        mPresenter = createPresenter();
        if (mPresenter != null) {
            mPresenter.attachView((UIInterface) this);
            mPresenter.onCreate();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPresenter != null) {
            mPresenter.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPresenter != null) {
            mPresenter.onPause();
        }
    }

    @Override
    protected void onWillDestroy() {
        super.onWillDestroy();
        if (mPresenter != null) {
            mPresenter.detachView();
            mPresenter.onDestroy();
        }
    }

    protected abstract int getContentViewId();

    @NonNull
    protected abstract Presenter createPresenter();

    public abstract void findView();

    public abstract void setView();

    public abstract void setListener();

    @Deprecated
    public boolean isCanBackDrawView() {
        return false;
    }

}
