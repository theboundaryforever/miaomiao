package com.erban.ui.baseview;

import android.content.Context;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;

public class ActivityHelper {

    public static SupportActivity getActivityFromView(View view) {
        Context context = view.getContext();
        if (context instanceof SupportActivity) {
            return (SupportActivity) context;
        } else if (context instanceof ContextThemeWrapper) {
            //v4的包fragment里的context不是真正的activity
            Context realContext = ((ContextThemeWrapper) context).getBaseContext();
            if (realContext != null && realContext instanceof SupportActivity) {
                return (SupportActivity) realContext;
            }
        }
        return null;
    }

    public static void setFactory(final LayoutInflater inflater, final IViewLifecycleRegister register) {
        LayoutInflaterCompat.setFactory(inflater, new LayoutInflaterFactory() {
            @Override
            public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
                Context realContext = context;
                if (context instanceof ContextThemeWrapper) {
                    realContext = ((ContextThemeWrapper) context).getBaseContext();
                }
                if (realContext instanceof AppCompatActivity) {
                    View view = ((AppCompatActivity) realContext).getDelegate().createView(parent, name, context, attrs);
                    if (view == null && -1 != name.indexOf('.')) {
                        try {
                            view = inflater.createView(name, null, attrs);
                        } catch (Exception e) {
                        }
                        if (view != null && view instanceof IViewLifecycle) {
                            register.registerLifecycleView(((IViewLifecycle) view).getLifecycleDelegate());
                        }
                    }
                    return view;
                }
                return null;
            }
        });
    }


}
