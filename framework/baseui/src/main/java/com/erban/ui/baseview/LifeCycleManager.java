package com.erban.ui.baseview;


/**
 * 生命周期管理
 * 1. 记录生命周期
 * 2. 根据生命周期以及{@link #setMessageLifeCycle(int)}设置的值进行消息注册/反注册
 *
 * @author carlosliu on 2016/12/2.
 */
public class LifeCycleManager {
    private static final String TAG = "LifeCycleManager";

    private static final int LIFE_CYCLE_CREATED = 4;
    private static final int LIFE_CYCLE_VIEW_CREATED = 3;
    private static final int LIFE_CYCLE_STARTED = 2;
    private static final int LIFE_CYCLE_RESUMED = 1;
    private static final int LIFE_CYCLE_INVALID = 1000;
    private static final int LIFE_CYCLE_VIEW_DESTROYED = -LIFE_CYCLE_VIEW_CREATED;
    private static final int LIFE_CYCLE_PAUSED = -LIFE_CYCLE_RESUMED;
    private static final int LIFE_CYCLE_STOPPED = -LIFE_CYCLE_STARTED;
    private static final int LIFE_CYCLE_DESTROYED = -LIFE_CYCLE_CREATED;

    public static final int MESSAGE_LIFECYCLE_INVALID = LIFE_CYCLE_INVALID;
    public static final int MESSAGE_LIFECYCLE_CREATE_DESTROY = LIFE_CYCLE_CREATED;
    public static final int MESSAGE_LIFECYCLE_START_STOP = LIFE_CYCLE_STARTED;
    public static final int MESSAGE_LIFECYCLE_RESUME_PAUSE = LIFE_CYCLE_RESUMED;
    public static final int MESSAGE_LIFECYCLE_VIEW_CREATE_DESTROY = LIFE_CYCLE_VIEW_CREATED;

    private Object mReceiver;
    private int mMessageLifeCycle = MESSAGE_LIFECYCLE_INVALID;
    private int mLifeCycleStatus = LIFE_CYCLE_INVALID;

    public LifeCycleManager(Object receiver) {
        mReceiver = receiver;
    }

    private void updateLifeCycleStatus(int status) {
        mLifeCycleStatus = status;
    }

    public void setMessageLifeCycle(int messageLifeCycle) {
        mMessageLifeCycle = messageLifeCycle;
    }

    public void onCreate() {
        updateLifeCycleStatus(LIFE_CYCLE_CREATED);
    }

    public void onStart() {
        updateLifeCycleStatus(LIFE_CYCLE_STARTED);
    }

    public void onResume() {
        updateLifeCycleStatus(LIFE_CYCLE_RESUMED);

    }

    public void onPause() {
        updateLifeCycleStatus(LIFE_CYCLE_PAUSED);

    }

    public void onStop() {
        updateLifeCycleStatus(LIFE_CYCLE_STOPPED);
    }

    public void onDestroy() {
        updateLifeCycleStatus(LIFE_CYCLE_DESTROYED);

    }

    public void onViewCreated() {
        updateLifeCycleStatus(LIFE_CYCLE_VIEW_CREATED);
    }

    public void onDestroyView() {
        updateLifeCycleStatus(LIFE_CYCLE_VIEW_DESTROYED);
    }

    public boolean isViewCreated() {
        return reached(LIFE_CYCLE_VIEW_CREATED) && before(LIFE_CYCLE_VIEW_DESTROYED);
    }

    public boolean isCreated() {
        return reached(LIFE_CYCLE_CREATED) && before(LIFE_CYCLE_DESTROYED);
    }

    public boolean isResumed() {
        return reached(LIFE_CYCLE_RESUMED) && before(LIFE_CYCLE_PAUSED);
    }

    public boolean isStarted() {
        return reached(LIFE_CYCLE_STARTED) && before(LIFE_CYCLE_STOPPED);
    }

    public boolean isPaused() {
        return reached(LIFE_CYCLE_PAUSED);
    }

    public boolean isStopped() {
        return reached(LIFE_CYCLE_STOPPED);
    }

    public boolean isDestroyed() {
        return reached(LIFE_CYCLE_DESTROYED);
    }

    public boolean isViewDestroyed() {
        return reached(LIFE_CYCLE_VIEW_DESTROYED);
    }

    private boolean before(int status) {
        int index = mLifeCycleStatus;
        return index > status;
    }

    private boolean reached(int status) {
        int index = mLifeCycleStatus;
        return index <= status;
    }

}
