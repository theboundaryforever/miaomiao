package com.erban.ui.baseview;

import android.content.Intent;
import android.os.Handler;
import android.view.View;

/**
 * @author luowei on 2016/2/1.
 */
public class BaseViewDelegate implements IViewLifecycle, IRegisterTag {

    protected static final String TAG = BaseViewDelegate.class.getSimpleName();

    private View mView;
    private IViewLifecycle mLifeCycleView;
    protected Handler mHandler = new Handler();
    private boolean mOnFirstFrameVisible = false;
    private boolean mDestroyCalled = false;
    private boolean mAttached = false;
    private boolean mStarted = false;
    private boolean mResumed = false;
    private IViewLifecycleRegister mRegister;

    public BaseViewDelegate(View view) {
        mView = view;
        mLifeCycleView = (IViewLifecycle) view;
    }

    public void init() {
        registerLifecycle();
    }

    @Override
    public void onRegistered(IViewLifecycleRegister register) {
        if (mRegister != null && mRegister != register) {
            mRegister.unregisterLifecycleView(this);
        }
        mRegister = register;
    }

    @Override
    public IViewLifecycle getLifecycleDelegate() {
        return this;
    }

    private void registerLifecycle() {
        if (getActivity() instanceof IViewLifecycleRegister) {
            IViewLifecycleRegister viewLifecycleRegister = getActivity();
            viewLifecycleRegister.registerLifecycleView(this);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (hasWindowFocus && !mOnFirstFrameVisible) {
            mOnFirstFrameVisible = true;
        }
    }

    @Override
    public void onAttachedToWindow() {
        mAttached = true;
        mLifeCycleView.onCreate();
        mLifeCycleView.onCreateView();
        onStart();
        onResume();
    }

    @Override
    public void onDetachedFromWindow() {
        onDestroy();
        mAttached = false;
    }

    public void onNewIntent(Intent intent) {
        mLifeCycleView.onNewIntent(intent);
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onCreateView() {
    }


    @Override
    public void onStart() {
        if (!mAttached || mStarted) {
            return;
        }
        mStarted = true;
        mLifeCycleView.onStart();
    }

    @Override
    public void onResume() {
        if (!mAttached || mResumed) {
            return;
        }
        mResumed = true;
        mLifeCycleView.onResume();
    }


    /* ----------------------------------------------------------------------------------------- */

    @Override
    public void onPause() {
        if (mDestroyCalled) {
            return;
        }
        mResumed = false;
        mLifeCycleView.onPause();
    }

    @Override
    public void onStop() {
        if (!mStarted || mDestroyCalled) {
            return;
        }
        mStarted = false;
        mLifeCycleView.onStop();
    }

    @Override
    public void onDestroyView() {
        //will be call In [onDestroy]
    }

    //从onDetachedFromWindow 或者 activity.onWillDestroy回调过来
    @Override
    public void onDestroy() {
        if (!mAttached || mDestroyCalled) {
            return;
        }
        mDestroyCalled = true;
        if (mStarted) {
            mStarted = false;
            mLifeCycleView.onStop();
        }
        if (mResumed) {
            mLifeCycleView.onPause();
        }
        mLifeCycleView.onDestroyView();
        mLifeCycleView.onDestroy();
        mRegister = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLifeCycleView.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSupportVisible() {
    }

    @Override
    public void onSupportInvisible() {

    }

    protected SupportActivity getActivity() {
        return ActivityHelper.getActivityFromView(mView);
    }

}




