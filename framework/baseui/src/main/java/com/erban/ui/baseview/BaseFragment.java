package com.erban.ui.baseview;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.erban.ui.FragmentUtil;
import com.tcloud.core.log.L;

import me.yokeyword.fragmentation.anim.FragmentAnimator;


/**
 */

public abstract class BaseFragment extends BaseFragmentation implements IViewLifecycleRegister {
    private static final String TAG = "BaseFragment";

    private static final String KEY_BUNDLE = "FRAGMENT_KEY.BUNDLE";

    protected Activity mActivity;
    private Bundle mBundle;
    protected View mContentView;
    private LifecycleViewMgr mLifecycleViewMgr = new LifecycleViewMgr();
    private boolean mIsRestoreFromMemory = false;
    protected Handler mHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mIsRestoreFromMemory = true;
            if (mBundle == null) {
                mBundle = savedInstanceState.getBundle(KEY_BUNDLE);
            }
        }
    }

    @Override
    public void registerLifecycleView(IViewLifecycle view) {
        mLifecycleViewMgr.registerLifecycleView(view);
    }

    @Override
    public void unregisterLifecycleView(IViewLifecycle view) {
        mLifecycleViewMgr.unregisterLifecycleView(view);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        L.debug(TAG, "onCreateView: " + this);
        initBefore();

        LayoutInflater inflaterClone = inflater.cloneInContext(getContext());
        ActivityHelper.setFactory(inflaterClone, mLifecycleViewMgr);
        mContentView = inflaterClone.inflate(getContentViewId(), container, false);

        findView();
        setView();
        setListener();
        mLifecycleViewMgr.onCreateView();
        return mContentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mLifecycleViewMgr.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mLifecycleViewMgr.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mLifecycleViewMgr.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mLifecycleViewMgr.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLifecycleViewMgr.onDestroyView();

        mContentView = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLifecycleViewMgr.onDestroy();
        mContentView = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mBundle != null) {
            outState.putBundle(KEY_BUNDLE, mBundle);
        }
    }

    public boolean isRestoreFromMemory() {
        return mIsRestoreFromMemory;
    }

    // ondestroyview() 和 ondestroy()之间有间隙。此时用这个判断
    public boolean isViewDestroyed() {
        return mContentView == null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }


    @Override
    public void onSupportVisible() {
        super.onSupportVisible();
        mLifecycleViewMgr.onSupportVisible();
    }

    @Override
    public void onSupportInvisible() {
        super.onSupportInvisible();
        mLifecycleViewMgr.onSupportInvisible();
    }

    @Nullable
    @Override
    public View getView() {
        return mContentView;
    }

    public String getTagText() {
        return getClass().getSimpleName();
    }

    /**
     * fragment跳转的动画，如果需要自定义，请重写该方法
     *
     * @return
     */
    public int[] getEnterExitAnimation() {

        return null;
    }

    /**
     * Fragment跳转, 根据传入的参数hideCurrent确定是否隐藏或移除传入的当前Fragment
     *
     * @param clazz       需要跳转的Fragment
     * @param curClazz    传入的当前Fragment
     * @param hideCurrent 是否隐藏当前Fragment
     */
    public void switchFragment(Class<? extends BaseFragment> clazz, Class<? extends BaseFragment> curClazz, boolean hideCurrent) {
        switchFragment(clazz, curClazz, null, false, hideCurrent);
    }

    /**
     * Fragment跳转, 隐藏或移除当前Fragment
     *
     * @param clazz          需要跳转的Fragment
     * @param addToBackStack 是否添加到后退stack
     * @param hideCurrent    是否隐藏当前Fragment
     */
    public void switchFragment(Class<? extends BaseFragment> clazz, Bundle args, boolean addToBackStack, boolean hideCurrent) {
        switchFragment(clazz, null, args, addToBackStack, hideCurrent);
    }

    /**
     * Fragment跳转, 根据传入的参数hideCurrent确定是否隐藏或移除当前Frag传入的当前Fragment
     *
     * @param desClazz    需要跳转的Fragment
     * @param curClazz    当前Fragment
     * @param args        跳转附带的参数
     * @param hideCurrent 是否隐藏当前Fragment
     */
    public void switchFragment(Class<? extends BaseFragment> desClazz, Class<? extends BaseFragment> curClazz, Bundle args, boolean addToBackStack, boolean hideCurrent) {
        Log.d(TAG, "switchFragment: " + desClazz + " " + curClazz + " args=" + args + " hideCurrent=" + hideCurrent);
        if (hideCurrent) {
            if (desClazz == curClazz) {
                return;
            }
        }
        BaseFragment currentFragment = curClazz == null ? this : FragmentUtil.getFragment(curClazz, getChildFragmentManager());
        BaseFragment destFragment = FragmentUtil.getFragment(desClazz, getChildFragmentManager());
        if (destFragment == null || currentFragment.getTagText().equals(destFragment.getTagText())) {
            return;
        }
        int containerViewId = destFragment.getContainerViewId();

        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        int[] a = destFragment.getEnterExitAnimation();
        if (a != null && a.length == 4) {
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            destFragment.setFragmentAnimator(new FragmentAnimator(a[0], a[1], a[2], a[3]));
        }
        destFragment.setBundle(args);
        try {

            boolean isAdded = destFragment.isAdded();
            if (!isAdded) { // 先判断是否被add过
                if (hideCurrent && currentFragment != null) {
                    if (currentFragment.needCacheInMemory()) {
                        transaction.hide(currentFragment);
                    } else {
                        transaction.remove(currentFragment);

                    }
                }
                transaction.add(containerViewId, destFragment, destFragment.getTagText());
                if (addToBackStack) {
                    transaction.addToBackStack(destFragment.getTagText());
                }
                transaction.commitAllowingStateLoss();
                fm.executePendingTransactions();
            } else {
                if (hideCurrent && currentFragment != null) {
                    if (currentFragment.needCacheInMemory()) {
                        transaction.hide(currentFragment);
                    } else {
                        transaction.remove(currentFragment);
                    }
                }
                transaction.show(destFragment);
                if (addToBackStack) {
                    transaction.addToBackStack(destFragment.getTagText());
                }
                transaction.commitAllowingStateLoss();
                fm.executePendingTransactions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setBundle(Bundle args) {
        mBundle = args;
    }

    public Bundle getBundle() {
        return mBundle;
    }

    /**
     * 从这个fragment的根view(the one returned by {@link #onCreateView})中通过id查找指定的view<br>
     * 注意这个方法必须在{@link #onCreateView}调用后才能使用, 并且{@link #onCreateView}不返加null<br>
     *
     * @param id
     * @return
     */
    public View findViewById(int id) {
        if (mContentView != null) {
            return mContentView.findViewById(id);
        }
        return null;
    }

    public boolean isAttached() {
        return mActivity != null;
    }

    /**
     * Fragment中按返回方法回调
     *
     * @return
     */
    public abstract boolean onBackPressed();

    /**
     * 是否要做ui现场恢复,true为需要
     *
     * @return
     */
    public abstract boolean needCacheInMemory();

    /**
     * 该Fragment所在的容器id
     *
     * @return
     */
    public abstract int getContainerViewId();

    /**
     * 该Fragment内容layoutId
     *
     * @return
     */
    public abstract int getContentViewId();

    public abstract void initBefore();

    public abstract void findView();

    public abstract void setView();

    public abstract void setListener();

    /**
     * 是否要后台加载view，true为是
     *
     * @return
     */
    @Deprecated
    public boolean isCanBackDrawView() {
        return false;
    }


}
