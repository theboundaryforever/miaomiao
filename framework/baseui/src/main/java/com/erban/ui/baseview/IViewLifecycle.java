package com.erban.ui.baseview;

import android.content.Intent;

/**
 */

public interface IViewLifecycle {

    IViewLifecycle getLifecycleDelegate();

    void onNewIntent(Intent intent);

    void onCreate();

    void onCreateView();

    void onWindowFocusChanged(boolean hasWindowFocus);

    void onStart();

    void onResume();

    void onPause();

    void onStop();

    void onDestroyView();

    void onDestroy();

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onSupportVisible();

    void onSupportInvisible();

    void onAttachedToWindow();

    void onDetachedFromWindow();

}
