package com.erban.ui.baseview;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.CallSuper;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.tcloud.core.log.L;

/**
 * @author luowei on 2016/2/1.
 */
public abstract class BaseRelativeLayout extends RelativeLayout implements IViewLifecycle {

    protected static final String TAG = BaseRelativeLayout.class.getSimpleName();

    private BaseViewDelegate mViewDelegate = new BaseViewDelegate(this);
    protected Handler mHandler = new Handler();

    public BaseRelativeLayout(Context context) {
        super(context);
        init();
    }

    public BaseRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mViewDelegate.init();
    }

    @Override
    public IViewLifecycle getLifecycleDelegate() {
        return mViewDelegate;
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        mViewDelegate.onWindowFocusChanged(hasWindowFocus);
    }

    @Override
    @CallSuper
    final public void onAttachedToWindow() {
        L.verbose(this, "onAttachedToWindow");
        super.onAttachedToWindow();
        mViewDelegate.onAttachedToWindow();
    }

    @Override
    @CallSuper
    final public void onDetachedFromWindow() {
        L.verbose(this, "onDetachedFromWindow");
        super.onDetachedFromWindow();
        mViewDelegate.onDetachedFromWindow();
    }

    protected SupportActivity getActivity() {
        return ActivityHelper.getActivityFromView(this);
    }

    @Override
    public void onCreate() {
        L.verbose(this, "onCreate");
    }

    @Override
    public void onNewIntent(Intent intent) {
    }

    @Override
    public void onCreateView() {
        L.verbose(this, "onCreateView");
    }

    @Override
    public void onStart() {
        L.verbose(this, "onStart");
    }

    @Override
    public void onResume() {
        L.verbose(this, "onResume");
    }

    @Override
    public void onPause() {
        L.verbose(this, "onPause");
    }

    @Override
    public void onStop() {
        L.verbose(this, "onStop");
    }

    @Override
    public void onDestroy() {
        L.verbose(this, "onDestroy");
    }

    @Override
    public void onDestroyView() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onSupportVisible() {
    }

    @Override
    public void onSupportInvisible() {
    }
}




