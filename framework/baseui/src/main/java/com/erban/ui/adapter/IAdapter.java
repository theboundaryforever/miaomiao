package com.erban.ui.adapter;

/**
 * 类 描 述: 扩展的Adapter接口规范
 */
public interface IAdapter<T> {

    void onUpdate(BaseAdapterHelper helper, T item, int position);

    int getLayoutResId(T item, int position);
}

