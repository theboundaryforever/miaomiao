package com.erban.ui.adapter;

import java.util.List;

/**
 * 类 描 述: 数据操作接口规范
 */
@SuppressWarnings("unused")
public interface IData<T> {

    List<T> getData();

    void add(T elem);

    void addAll(List<T> elem);

    void set(T oldElem, T newElem);

    void set(int index, T elem);

    void remove(T elem);

    void remove(int index);

    void replaceAll(List<T> elem);

    boolean contains(T elem);

    void clear();

}
