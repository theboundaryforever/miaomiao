package com.tcloud.core.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.tcloud.core.log.L;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Config {
    private static Map<String, Config> mConfigMaps = new HashMap<>();
    private static String NAME = null;

    private SharedPreferences mPreferences;

    public static synchronized Config getInstance(Context context) {
        if (null == NAME) {
            NAME = Utils.getProcessName(context) + ".configuration";
        }
        return getConfig(context, NAME);
    }

    private static Config getConfig(Context context, String name) {
        Config config = null;
        if (mConfigMaps.containsKey(name)) {
            config = mConfigMaps.get(name);
        }
        if (config == null) {
            config = new Config(context, name);
            mConfigMaps.put(name, config);
        }
        return config;
    }

    public static synchronized Config getInstance(Context context, String name) {
        if (name == null) name = "";
        return getConfig(context, name);
    }

    private Config(Context context, String name) {
        String sharePreferenceName = name + ".configuration";
        mPreferences = context.getSharedPreferences(sharePreferenceName, Context.MODE_PRIVATE);
    }

    public void setOnChangeListenner(SharedPreferences.OnSharedPreferenceChangeListener listenner) {
        if (listenner != null) {
            mPreferences.registerOnSharedPreferenceChangeListener(listenner);
        }
    }


    public synchronized boolean setString(String key, String value) {
        Editor editor = mPreferences.edit().putString(key, value);
        return doApply(editor);
    }

    public synchronized boolean setStringSync(String key, String value) {
        Editor editor = mPreferences.edit().putString(key, value);
        return doCommit(editor);
    }

    public synchronized boolean setInt(String key, int value) {
        Editor editor = mPreferences.edit().putInt(key, value);
        return doApply(editor);
    }

    public synchronized boolean setIntSync(String key, int value) {
        Editor editor = mPreferences.edit().putInt(key, value);
        return doCommit(editor);
    }

    public synchronized boolean setLong(String key, long value) {
        Editor editor = mPreferences.edit().putLong(key, value);
        return doApply(editor);
    }

    public synchronized boolean setLongSync(String key, long value) {
        Editor editor = mPreferences.edit().putLong(key, value);
        return doCommit(editor);
    }

    public synchronized boolean setBoolean(String key, boolean value) {
        Editor editor = mPreferences.edit().putBoolean(key, value);
        return doApply(editor);
    }

    public synchronized boolean setBooleanSync(String key, boolean value) {
        Editor editor = mPreferences.edit().putBoolean(key, value);
        return doCommit(editor);
    }

    public synchronized boolean setFloat(final String key, float value) {
        Editor editor = mPreferences.edit().putFloat(key, value);
        return doApply(editor);
    }

    public synchronized boolean setFloatSync(final String key, float value) {
        Editor editor = mPreferences.edit().putFloat(key, value);
        return doCommit(editor);
    }

    public synchronized boolean setStringSet(String key, Set<String> value) {
        Editor editor = mPreferences.edit().putStringSet(key, value);
        return doApply(editor);
    }

    public synchronized boolean setStringSetSync(String key, Set<String> value) {
        Editor editor = mPreferences.edit().putStringSet(key, value);
        return doCommit(editor);
    }

    public synchronized String getString(String key, String defaultValue) {
        return mPreferences.getString(key, defaultValue);
    }

    public synchronized int getInt(String key, int defaultValue) {
        return mPreferences.getInt(key, defaultValue);
    }

    public synchronized long getLong(String key, long defaultValue) {
        return mPreferences.getLong(key, defaultValue);
    }

    public synchronized boolean getBoolean(String key, boolean defaultValue) {
        return mPreferences.getBoolean(key, defaultValue);
    }

    public synchronized float getFloat(final String key, final float defaultValue) {
        return mPreferences.getFloat(key, defaultValue);
    }

    public synchronized Set<String> getStringSet(String key, Set<String> deaultValue) {
        return mPreferences.getStringSet(key, deaultValue);
    }

    public synchronized void clearAllSync() {
        Editor editor = mPreferences.edit().clear();
        doCommit(editor);
    }

    public synchronized void clearAllAsync() {
        mPreferences.edit().clear().apply();
    }

    private boolean doCommit(Editor editor) {
        boolean result;
        try {
            result = editor.commit();
        } catch (Throwable t) {
            L.error("Config", t);
            result = false;
        }
        return result;
    }

    private boolean doApply(Editor editor) {
        boolean result = true;
        try {
            editor.apply();
        } catch (Throwable t) {
            L.error("Config", t);
            result = false;
        }
        return result;
    }
}
