package com.tcloud.core.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.tcloud.core.log.L;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

/**
 * Use to listen the status change of network.
 */
public class NetworkUtil {
    public static final String NET_TYPE_UNKNOWN = "unknown";
    public static final String NET_TYPE_NONE = "none";
    public static final String NET_TYPE_WIFI = "wifi";
    public static final String NET_TYPE_2G = "2G";
    public static final String NET_TYPE_3G = "3G";
    public static final String NET_TYPE_4G = "4G";

    public static String getWifiSSID(Context context) {
        try {
            WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = wifiMgr.getConnectionInfo();
            String wifiId = info != null ? info.getSSID() : "";
            return wifiId;
        } catch (Throwable t) {
            L.error("NetworkUtil", t);
        }
        return "";
    }

    /**
     * Tell whether the current network is Wifi.
     * NOTE this can only be called when YYMobile.gContext is set.
     *
     * @return True if wifi is active, false otherwise.
     */

    public static boolean isWifiActive(Context context) {
        ConnectivityManager mgr = null;
        try {
            mgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
        } catch (Exception e) {
            L.error("NetworkUtil", e);
        }

        if (mgr == null) {
            return false;
        }
        NetworkInfo networkInfo = mgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }

    public static boolean is2GOr3GActive(Context context) {
        ConnectivityManager mgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = mgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
    }

    public static String getNetWorkType(Context context) {
        if (!isNetworkAvailable(context)) {
            return NET_TYPE_NONE;
        }
        if (isWifiActive(context)) {
            return NET_TYPE_WIFI;
        }
        return getNetWorkSubType(context);
    }

    public static String getNetWorkSubType(Context context) {
        ConnectivityManager mgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = mgr.getActiveNetworkInfo();
        String result = NET_TYPE_UNKNOWN;
        if (null == networkInfo) {
            return result;
        }
        int subType = networkInfo.getSubtype();
        switch (subType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                result = NET_TYPE_2G;
                break;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                result = NET_TYPE_3G;
                break;
            case TelephonyManager.NETWORK_TYPE_LTE:
                result = NET_TYPE_4G;
                break;
        }
        return result;
    }

    public static boolean isNetworkStrictlyAvailable(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
            if (ni != null && ni.isAvailable() && ni.isConnected()) {
                return true;
            } else {
                String info = null;
                if (ni != null) {
                    info = "network type = " + ni.getType() + ", "
                            + (ni.isAvailable() ? "available" : "inavailable")
                            + ", " + (ni.isConnected() ? "" : "not") + " connected";
                } else {
                    info = "no active network";
                }
                L.info("network", info);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isNetworkAvailable(Context context) {
//        ConnectivityManager connectivityManager = (ConnectivityManager) context
//                .getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
//        if (ni == null) {
//            return false;
//        }
//        return ni.isConnected()
//                || (ni.isAvailable() && ni.isConnectedOrConnecting());
        return isNetworkStrictlyAvailable(context);
    }

    public static void openNetworkConfig(Context c) {
        Intent i = null;
        if (android.os.Build.VERSION.SDK_INT > 10) {
            i = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
        } else {
            i = new Intent();
            i.setClassName("com.android.settings", "com.android.settings.WirelessSettings");
            i.setAction(Intent.ACTION_MAIN);
        }
        try {
            c.startActivity(i);
        } catch (Exception e) {
        }
    }

    private static final int MIN_PORT = 0;
    private static final int MAX_PORT = 65535;
    private static final int DEFAULT_PROXY_PORT = 80;

    public static InetSocketAddress getTunnelProxy(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.WRITE_APN_SETTINGS") ==
                PackageManager.PERMISSION_DENIED) {
            return null;
        }
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null) {
            if (netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                return null;
            }
        }
        String proxy = "";
        String portStr = "";
        Uri uri = Uri.parse("content://telephony/carriers/preferapn");
        Cursor cr = context.getContentResolver().query(uri, null,
                null, null, null);
        if (cr != null && cr.moveToNext()) {
            proxy = cr.getString(cr.getColumnIndex("proxy"));
            portStr = cr.getString(cr.getColumnIndex("port"));
            L.info("getTunnelProxy", Utils.getOperator(context) + ", proxy = " + proxy + ", port = " + portStr);
            if (proxy != null && proxy.length() > 0) {
                cr.close();
                cr = null;
                int port;
                try {
                    port = Integer.parseInt(portStr);
                    if (port < MIN_PORT || port > MAX_PORT) {
                        port = DEFAULT_PROXY_PORT;
                    }
                } catch (Exception e) {
                    L.info("getTunnelProxy", "port is invalid, e = " + e);
                    port = DEFAULT_PROXY_PORT;
                }
                InetSocketAddress addr = null;
                try {
                    addr = new InetSocketAddress(proxy, port);
                } catch (Exception e) {
                    L.info("getTunnelProxy", "create address failed, e = " + e);
                }
                return addr;
            }
        }
        if (cr != null) {
            cr.close();
            cr = null;
        }
        return null;
    }

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && (inetAddress instanceof Inet4Address)) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            L.error("NetworkUtils getLocalIpAddress:", ex.toString());
        }
        return null;
    }


    //设备是否开启了代理
    public static boolean isUseHttpProxy() {
        String host = System.getProperty("http.proxyHost");
        String port = System.getProperty("http.proxyPort");
        return !TextUtils.isEmpty(host) && !TextUtils.isEmpty(port);
    }

}
