package com.tcloud.core.thread.pool;


import com.tcloud.core.log.L;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by gaotianyu on 11/23/16.
 */
public class Pools {
    public static ScheduledExecutor newScheduledExecutor(String type, int priority, int threadCount) {
        return new ScheduledPoolExecutor(type, priority, threadCount, new HandlerWorkFactory());
    }

    public static ScheduledExecutor newScheduledThreadPoolExecutor(String threadNamePrefix, int coreCount) {
        return new ScheduledExecutorAdapter(
                new ScheduledThreadPoolExecutor(coreCount, new DefaultThreadFactory(threadNamePrefix)) {
                    @Override
                    protected void afterExecute(Runnable r, Throwable t) {
                        super.afterExecute(r, t);
                        L.error("Executor", t);
                    }
                }
        );
    }

    private static class DefaultThreadFactory implements ThreadFactory {
        private static final AtomicInteger poolNumber = new AtomicInteger(1);
        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final String namePrefix;

        DefaultThreadFactory(String threadNamePrefix) {
            namePrefix = threadNamePrefix + poolNumber.getAndIncrement() + "-thread-";
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r, namePrefix + threadNumber.getAndIncrement());
            return t;
        }
    }
}
