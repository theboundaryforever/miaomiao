package com.tcloud.core.util;

import android.os.PowerManager;

/**
 * Created by chaoqun_liu on 2015/3/24.
 */
public class WakeLockHelper {
    private PowerManager.WakeLock mWakeLock;
    private int mSendMsgCount;
    private static WakeLockHelper INSTANCE = new WakeLockHelper();

    public static WakeLockHelper getInstance() {
        return INSTANCE;
    }

    private static final String TAG = WakeLockHelper.class.getSimpleName();

    public synchronized void acquireWakeLock() {
//        if (mWakeLock == null) {
//            PowerManager pm = (PowerManager) BaseApp.gContext
//                    .getSystemService(Context.POWER_SERVICE);
//            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
//        }
//
//        if (mSendMsgCount == 0) {
//            mWakeLock.acquire();
//        }
//        mSendMsgCount++;
//        L.info(TAG, "[acquireWakeLock] mSendMsgCount = " + mSendMsgCount);
    }

    public synchronized void releaseWakeLock() {
//        if (mWakeLock == null) {
////            L.info(TAG, "[releaseWakeLock] mWakeLock is null, mSendMsgCount = " + mSendMsgCount);
//            return;
//        }
//
//        if (mSendMsgCount > 0) {
//            mSendMsgCount--;
//        }
////        L.info(TAG, "[releaseWakeLock] mSendMsgCount = " + mSendMsgCount);
//        if (mSendMsgCount == 0) {
//            mWakeLock.release();
//        }
    }
}
