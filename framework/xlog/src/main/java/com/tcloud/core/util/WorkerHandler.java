package com.tcloud.core.util;

import android.os.Handler;
import android.os.Looper;

public class WorkerHandler extends Handler {

    public WorkerHandler(Looper looper) {
        super(looper);
    }

    public void optPost(Runnable runnable) {
        if (this.getLooper() == Looper.myLooper()) {
            runnable.run();
        } else {
            super.post(runnable);
        }
    }

    public void optPostDelay(Runnable runnable, long delayMillis) {
        if (this.getLooper() == Looper.myLooper() && delayMillis == 0) {
            runnable.run();
        } else {
            super.postDelayed(runnable, delayMillis);
        }
    }


}
