package com.tcloud.core.thread;

/**
 * Created by gaotianyu on 11/23/16.
 */
public abstract class PriorityRunnable implements Runnable {
    private String mKey;

    protected String getKey() {
        return mKey;
    }

    public PriorityRunnable(String key) {
        mKey = key;
    }
}
