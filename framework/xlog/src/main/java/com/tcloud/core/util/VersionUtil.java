package com.tcloud.core.util;


import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;

public class VersionUtil {
    static int sLocalVer[] = null;
    static String sLocalName = null;

    private static final String DOT = ".";

    public static Ver getVerFromStr(String version) {
        if (version.matches("\\d{1,}\\.\\d{1,}\\.\\d{1,}")) {
            Ver ver = new Ver();
            int dotPos = version.indexOf(DOT);
            int prevPos = 0;
            ver.mMajor = Integer.valueOf(version.substring(prevPos, dotPos));
            prevPos = dotPos + 1;
            dotPos = version.indexOf(DOT, prevPos);
            ver.mMinor = Integer.valueOf(version.substring(prevPos, dotPos));
            prevPos = dotPos + 1;
            ver.mBuild = Integer.valueOf(version.substring(prevPos));
            return ver;
        }
        return null;
    }

    public static Ver getLocalVer(Context c) {
        Ver v = new Ver();
        int ver[] = VersionUtil.getLocal(c);
        v.mMajor = ver[0];
        v.mMinor = ver[1];
        v.mBuild = ver[2];
        return v;
    }

    public static String getLocalName(Context c) {
        if (sLocalName != null) {
            return sLocalName;
        }

        loadLoaclVer(c);

        return sLocalName;
    }

    public static int[] getLocal(Context c) {
        if (sLocalVer != null) {
            return sLocalVer;
        }

        loadLoaclVer(c);

        return sLocalVer;
    }

    static void loadLoaclVer(Context c) {
        try {
            sLocalName = c.getPackageManager().getPackageInfo(c.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            throw new RuntimeException("Local Ver Package Error");
        }

        if (sLocalName == null) {
            throw new RuntimeException("Local Ver VersionName Not Exist");
        }

        // handle maven standard version like this "1.0.0-SNAPSHOT";
        int pos = sLocalName.indexOf('-');
        if (pos != -1) {
            sLocalName = sLocalName.substring(0, pos);
        }
        String verStr[] = sLocalName.split("\\.");

        if (verStr.length != 3) {
            throw new RuntimeException("Local Ver VersionName Error");
        }

        sLocalVer = new int[3];

        try {
            for (int i = 0; i < 3; i++) {
                sLocalVer[i] = Integer.parseInt(verStr[i]);
            }
        } catch (NumberFormatException e) {
            throw new RuntimeException("Local Ver VersionName Error");
        }
    }
}
