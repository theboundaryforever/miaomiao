package com.tcloud.core.util;

import android.os.Build;

/**
 * Created by legend on 15/5/18.
 */
public class MiUiUtils {

    public static boolean isXiaoMiMobile() {
        return Build.MANUFACTURER.equalsIgnoreCase("xiaomi");
    }

    public static boolean isMiui() {
        String ret = ShellCmd.getSystemProperty("ro.miui.ui.version.name");
        return ret != null && ret.length() > 0;
    }

    public static int miuiVer() {
        String ret = ShellCmd.getSystemProperty("ro.miui.ui.version.name");
        try {
            String ver = ret.substring(1);
            if (ver != null) {
                return Integer.parseInt(ver);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    public static boolean isEmui() {
        String ret = ShellCmd.getSystemProperty("ro.build.version.emui");
        if (ret == null || ret.length() == 0) {
            return false;
        } else {
            String[] splitRet = ret.split("_");
            if (splitRet.length > 1) {
                float ver = Float.parseFloat(splitRet[1]);
                return ver >= 2.3f;
            } else {
                return false;
            }
        }
    }
}
