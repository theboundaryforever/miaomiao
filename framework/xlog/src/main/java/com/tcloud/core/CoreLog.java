package com.tcloud.core;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.tcloud.core.log.L;
import com.tcloud.core.log.LogProxy;
import com.tcloud.core.util.FP;
import com.tcloud.core.util.FileStorage;
import com.tcloud.core.util.ResourceUtils;
import com.tcloud.core.util.Utils;

import java.io.File;

/**
 * Created by Chen on 2019/4/4.
 */
public class CoreLog {
    public static Context gContext;
    public static String gTag = "CoreLog";
    public static String sProcessName;
    public static boolean gIsTest = false;

    public static void init(Context context) {
        gContext = context.getApplicationContext();
        initTag();
        initLog();
    }

    private static void initTag() {
        gTag = ResourceUtils.getMetaValue(gContext, "TAG");
        if (FP.empty(gTag)) {
            String[] packageNames = gContext.getPackageName().split("\\.");
            int length = packageNames.length;
            if (length >= 2) {
                gTag = packageNames[1];
            } else {
                gTag = packageNames[packageNames.length - 1];
            }
        }
    }

    private static void initLog() {
        sProcessName = Utils.getProcessName();
        if (isTestEnv()) {
            L.setLogLevel(Log.VERBOSE);
        }
        if (!isMainProcess()) {
            gTag = String.format("%s/%s", gTag, sProcessName);
        }
        Log.e(gTag, "gTag = " + gTag);
        L.TAG = gTag;
        L.isStoreExist = FileStorage.isStoreExist(FileStorage.Location.SDCard);
        L.sLogPath = String.format("/%s/logs", gTag);
//        L.sLogDir = FileStorage.getInstance().getRootDir(FileStorage.Location.SDCard)
//                .getParentFile().getAbsolutePath();
//        L.sLogDir = gContext.getFilesDir().getAbsolutePath();
        L.sLogDir = Environment.getExternalStorageDirectory().getPath();

        String logCacheDir = "log" + File.separator;
        if (!isMainProcess()) {
            logCacheDir += Utils.getProcessName();
        }
        L.sLogCacheDir = gContext.getFilesDir().getAbsolutePath() + File.separator + logCacheDir;

        LogProxy.resetRoot(L.sLogDir);
        LogProxy.resetLogPath(L.sLogPath);
        //设置正确的路径后才能打开log，否则会造成xlog写错目录
        L.setLogEnable(true);
        L.debug(gTag, "log:%s,cache:%s", L.sLogDir, L.sLogCacheDir);
    }

    public static boolean isMainProcess() {
        return !FP.empty(sProcessName) && gContext.getPackageName().equals(sProcessName);
    }

    public static boolean isTestEnv() {
        return gIsTest;
    }

    public static void setgIsTest(boolean gIsTest) {
        CoreLog.gIsTest = gIsTest;
    }
}
