package com.tcloud.core.util;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by gaotianyu on 15/3/27.
 */
public class StartActivityUtil {
    public static boolean miuiSecurityCenterDetailVer5(Activity activity) {
        /*Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        activity.startActivity(intent);*/
        try {
            Intent intent = new Intent("miui.intent.action.APP_PERM_EDITOR");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setClassName("com.android.settings", "com.miui.securitycenter.permission.AppPermissionsEditor");
            intent.putExtra("extra_package_uid",
                    activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0)
                            .applicationInfo.uid);
            activity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean miuiSecurityCenterAutoStartVer6(Activity activity) {
        try {
            Intent intent = new Intent("miui.intent.action.APP_PERM_EDITOR");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setClassName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity");
            activity.startActivity(intent);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
