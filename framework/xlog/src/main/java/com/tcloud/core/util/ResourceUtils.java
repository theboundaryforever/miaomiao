package com.tcloud.core.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.tcloud.core.log.L;

public class ResourceUtils {

    public static String getMetaValue(Context context, String key) {
        return getMetaValue(context, key, "");
    }

    public static String getStringById(Context context, int id) {
        return context.getResources().getString(id);
    }

    public static int getMetaIntValue(Context context, String key) {
        return getMetaIntValue(context, key, 0);
    }

    public static synchronized String getMetaValue(Context context, String key, String defaultValue) {
        ApplicationInfo appInfo;
        try {
            appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName()
                    , PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return defaultValue;
        } catch (Exception e) {
            return defaultValue;
        }
        Bundle bundle = appInfo.metaData;
        if (bundle == null) {
            return defaultValue;
        }
        String value;
        int valueInt = 0;

        try {
            value = bundle.getString(key, "");
            if (!FP.empty(value)) {
                return value;
            }
        } catch (Exception e) {
            L.warn("ResourceUtils", "ClassCastException Key name = " + key);
        }
        valueInt = bundle.getInt(key, 0);

        return valueInt == 0 ? defaultValue : String.valueOf(valueInt);
    }

    public static synchronized int getMetaIntValue(Context context, String key, int defaultValue) {
        ApplicationInfo appInfo;
        try {
            appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName()
                    , PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return defaultValue;
        } catch (Exception e) {
            return defaultValue;
        }
        Bundle bundle = appInfo.metaData;
        if (bundle == null) {
            return defaultValue;
        }
        int valueInt = bundle.getInt(key, 0);

        return valueInt == 0 ? defaultValue : valueInt;
    }


    public static String getStringByName(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "string",
                context.getPackageName());
        if (id > 0) {
            return context.getString(id);
        }
        L.error(context, "can not find string by name : %s", name);
        return "";
    }

    public static int getDrawableIdByName(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "drawable",
                context.getPackageName());
        if (id == 0) {
            L.error(context, "can not find drawableId by name : %s", name);
        }
        return id;
    }

    public static int getXmlIdByName(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "xml",
                context.getPackageName());
        if (id == 0) {
            L.error(context, "can not find xmlId by name : %s", name);
        }
        return id;
    }

    public static int getIdIdByName(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "id",
                context.getPackageName());
        if (id == 0) {
            L.error(context, "can not find idId by name : %s", name);
        }
        return id;
    }
}
