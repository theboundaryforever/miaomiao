package com.tcloud.core.util;

import android.os.Handler;
import android.os.Message;

/**
 * @author carlosliu on 2015/5/21.
 */
public class EasyTimer {
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (mRunnable != null) {
                mRunnable.run();
            }
            if (mRuning) {
                sendEmptyMessageDelayed(0, mDuration);
            }
        }
    };
    private int mDuration;
    private Runnable mRunnable;
    private boolean mRuning = false; //如果不加，无法在run函数中停止

    public void setDuration(int duration) {
        mDuration = duration;
    }

    public void setRunnable(Runnable run) {
        mRunnable = run;
    }

    public boolean isRuning() {
        return mRuning;
    }

    public void stop() {
        mRuning = false;
        mHandler.removeMessages(0);
    }

    public void start() {
        mRuning = true;
        mHandler.sendEmptyMessageDelayed(0, 0);
    }

    public void resetAndStart(int duration, Runnable run) {
        setRunnable(run);
        stop();
        setDuration(duration);
        start();
    }

    public void delayedStart(int delayed) {
        if (delayed < 0) delayed = 0;

        mRuning = true;
        mHandler.sendEmptyMessageDelayed(0, delayed);
    }
}
