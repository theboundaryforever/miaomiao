package com.tcloud.core.util;

/**
 * @author carlosliu on 2016/10/17.
 */
public class Tags {
    public final Object[] mTags;

    public Tags(Object... tags) {
        this.mTags = tags;
    }

    @Override
    public String toString() {
        if (mTags == null || mTags.length == 0) {
            return "EmptyTags";
        }

        StringBuilder builder = new StringBuilder();
        for (Object tag : mTags) {
            builder.append('[').append(tag).append(']');
        }

        return builder.toString();
    }
}
