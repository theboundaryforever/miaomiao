package com.tcloud.core.log;

import android.os.Environment;
import android.os.Process;
import android.util.Log;

import com.tcloud.core.util.Tags;

import java.io.PrintWriter;
import java.io.StringWriter;

public class L {
    private static int LOG_LEVEL = Log.INFO;
    // can overwrite this tag by ark.config
    public static String TAG = "ark";
    public static String sLogDir = "/";
    public static String sLogPath = "/";
    public static String sLogCacheDir = "/log";

    public static boolean isStoreExist = Environment.getExternalStorageState().equalsIgnoreCase(
            Environment.MEDIA_MOUNTED);

    public static boolean mLogEnable = false; //必须resetPath后才能打开日志，否则日志路径异常

    public static void setIsStorageExist(boolean exist) {
        isStoreExist = exist;
    }

    public static void setTag(String tag) {
        TAG = tag;
    }

    public static boolean isLogEnable() {
        return mLogEnable;
    }

    public static void setLogEnable(boolean enable) {
        mLogEnable = enable;
    }

    public static void setLogLevel(int logLevel) {
        LOG_LEVEL = logLevel;
    }

    public static int getLogLevel() {
        return LOG_LEVEL;
    }

    public static String getAbsLogPath() {
        return sLogDir + sLogPath;
    }

    public static String getCurrentLogAbsPath() {
        return LogProxy.getCurrentLogAbsPath();
    }

    public static boolean isLogLevelEnabled(int logLevel) {
        return LOG_LEVEL <= logLevel && isLogEnable();
    }

    public static void verbose(String msg) {
        if (!isLogLevelEnabled(Log.VERBOSE)) {
            return;
        }
        doLog(Log.VERBOSE, null, msg, null, true);
    }

    public static void verbose(Object tag, String format) {
        if (!isLogLevelEnabled(Log.VERBOSE)) {
            return;
        }
        doLog(Log.VERBOSE, tag, format, null, true);
    }


    public static void verbose(Object tag, String format, Object... args) {
        if (!isLogLevelEnabled(Log.VERBOSE)) {
            return;
        }
        doLog(Log.VERBOSE, tag, String.format(format, args), null, true);
    }

    public static void verbose(Object tag, String message, Throwable t) {
        if (!isLogLevelEnabled(Log.VERBOSE)) {
            return;
        }
        doLog(Log.VERBOSE, tag, message, t, true);
    }

    public static void verbose(Object tag, Throwable t) {
        if (!isLogLevelEnabled(Log.VERBOSE)) {
            return;
        }
        doLog(Log.VERBOSE, tag, "Exception occurs at", t, true);
    }

    public static void debug(String msg) {
        if (!isLogLevelEnabled(Log.DEBUG)) {
            return;
        }
        doLog(Log.DEBUG, null, msg, null, true);
    }

    public static void debug(Object tag, String message) {
        if (!isLogLevelEnabled(Log.DEBUG)) {
            return;
        }
        doLog(Log.DEBUG, tag, message, null, true);
    }

    public static void debug(Object tag, String format, Object... args) {
        if (!isLogLevelEnabled(Log.DEBUG)) {
            return;
        }
        doLog(Log.DEBUG, tag, String.format(format, args), null, true);
    }

    public static void debug(Object tag, String message, Throwable t) {
        if (!isLogLevelEnabled(Log.DEBUG)) {
            return;
        }
        doLog(Log.DEBUG, tag, message, t, true);
    }

    public static void debug(Object tag, Throwable t) {
        if (!isLogLevelEnabled(Log.DEBUG)) {
            return;
        }
        doLog(Log.DEBUG, tag, "Exception occurs at", t, true);
    }

    public static void info(String msg) {
        if (!isLogLevelEnabled(Log.INFO)) {
            return;
        }
        doLog(Log.INFO, null, msg, null, true);
    }

    public static void info(Object tag, String format) {
        if (!isLogLevelEnabled(Log.INFO)) {
            return;
        }
        doLog(Log.INFO, tag, format, null, true);
    }

    public static void info(Object tag, String format, Object... args) {
        if (!isLogLevelEnabled(Log.INFO)) {
            return;
        }
        doLog(Log.INFO, tag, String.format(format, args), null, true);
    }

    public static void info(Object tag, String message, Throwable t) {
        if (!isLogLevelEnabled(Log.INFO)) {
            return;
        }
        doLog(Log.INFO, tag, message, t, true);
    }

    public static void info(Object tag, Throwable t) {
        if (!isLogLevelEnabled(Log.INFO)) {
            return;
        }
        doLog(Log.INFO, tag, "Exception occurs at", t, true);
    }

    public static void warn(String msg) {
        if (!isLogLevelEnabled(Log.WARN)) {
            return;
        }
        doLog(Log.WARN, null, msg, null, true);
    }


    public static void warn(Object tag, String message) {
        if (!isLogLevelEnabled(Log.WARN)) {
            return;
        }
        doLog(Log.WARN, tag, message, null, true);
    }

    public static void warn(Object tag, String format, Object... args) {
        if (!isLogLevelEnabled(Log.WARN)) {
            return;
        }
        doLog(Log.WARN, tag, String.format(format, args), null, true);
    }

    public static void warn(Object tag, String message, Throwable t) {
        if (!isLogLevelEnabled(Log.WARN)) {
            return;
        }
        doLog(Log.WARN, tag, message, t, true);
    }

    public static void warn(Object tag, Throwable t) {
        if (!isLogLevelEnabled(Log.WARN)) {
            return;
        }
        doLog(Log.WARN, tag, "Exception occurs at", t, true);
    }

    public static void error(String msg) {
        if (!isLogLevelEnabled(Log.ERROR)) {
            return;
        }
        doLog(Log.ERROR, null, msg, null, true);
    }

    public static void error(Object tag, String message) {
        if (!isLogLevelEnabled(Log.ERROR)) {
            return;
        }
        doLog(Log.ERROR, tag, message, null, true);
    }

    public static void error(Object tag, String format, Object... args) {
        if (!isLogLevelEnabled(Log.ERROR)) {
            return;
        }
        doLog(Log.ERROR, tag, String.format(format, args), null, true);
    }

    public static void error(Object tag, String message, Throwable t) {
        if (!isLogLevelEnabled(Log.ERROR)) {
            return;
        }
        doLog(Log.ERROR, tag, message, t, true);
    }

    public static void error(Object tag, Throwable t) {
        if (!isLogLevelEnabled(Log.ERROR)) {
            return;
        }
        doLog(Log.ERROR, tag, "Exception occurs at", t, true);
    }

    public static void log(int logLevel, Object tag, String message, Throwable t, boolean needStackTrace) {
        if (!isLogLevelEnabled(logLevel)) {
            return;
        }
        doLog(logLevel, tag, message, t, needStackTrace);
    }

    private static void doLog(int logLevel, Object tag, String message, Throwable t, boolean needStackTrace) {
        String fileName = "";
        int lineNumber = 0;
        if (needStackTrace) {
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            StackTraceElement element = null;
            if (stackTraceElements != null && stackTraceElements.length > 4) {
                element = stackTraceElements[4];
            }
            if (element != null) {
                fileName = element.getFileName();
                lineNumber = element.getLineNumber();
            }
        }
        String msg = msgForTextLog(tag, fileName, lineNumber, message, t, needStackTrace);
        logByLevel(logLevel, msg);
    }

    private static void logByLevel(int logLevel, String msg) {
        LogProxy.logByLevel(logLevel, msg, TAG, LogProxy.getLogName());
    }

    private static String msgForTextLog(Object tag, String filename, int line,
                                        String msg, Throwable t, boolean needStackTrace) {
        StringBuilder sb = new StringBuilder();
        sb.append(msg);
        if (needStackTrace) {
            sb.append("(P:").append(Process.myPid()).append(")");
            sb.append("(T:").append(Thread.currentThread().getId()).append(")");
            sb.append("(C:").append(objClassName(tag)).append(")");
            sb.append("at (").append(filename).append(":").append(line).append(")");
        }
        if (t != null) {
            sb.append('\n').append(Log.getStackTraceString(t));
        }
        return sb.toString();
    }

    private static String objClassName(Object obj) {
        if (obj == null) {
            return "Global";
        }
        if (obj instanceof String) {
            return (String) obj;
        } else if (obj instanceof Tags) {
            return obj.toString();
        } else if (obj instanceof Class) {
            return ((Class) obj).getSimpleName();
        } else {
            return obj.getClass().getSimpleName();
        }
    }

    public static void uncaughtException(Throwable t) {
        if (!isStoreExist || t == null) {
            return;
        }
        StringWriter sw = new StringWriter();
        sw.write("\n");
        t.printStackTrace(new PrintWriter(sw));
        try {
            LogProxy.logByLevel(Log.ERROR, sw.toString(), TAG, LogProxy.getUELogName());
            LogProxy.flushToDisk();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
