package com.tcloud.core.util;

import android.os.Environment;

import com.tcloud.core.CoreLog;

import java.io.File;

/**
 * app私有存储
 * /sdcard/{gTag}/...
 * /sdcard/{gTag}/.nomedia
 */
public class FileStorage {

    public enum Location {
        Cache,
        SDCard,
        Media  //在{SDCard}/.nomedia目录下，防止文件管理器扫描出来
    }

    private static FileStorage ourInstance = new FileStorage();

    public static FileStorage getInstance() {
        return ourInstance;
    }

    public static boolean isStoreExist(Location location) {
        return getInstance().getRootDir(location) != null;
    }

    private File mCacheDirectory;
    private File mSDCardDirectory;
    private File mMediaDirectory;

    private FileStorage() {
        init();
    }

    public File getRootDir(Location location) {
        switch (location) {
            case Cache:
                return mCacheDirectory;
            case SDCard:
                return mSDCardDirectory;
            case Media:
                return mMediaDirectory;
            default:
                return null;
        }
    }

    public long getSize(Location location) {
        return IOUtils.getFileSize(getRootDir(location));
    }

    public boolean clear(Location location) {
        return IOUtils.removeFile(getRootDir(location), false);
    }

    public boolean exist(Location location, String relativePath) {
        PathInfo pathInfo = parsePath(relativePath);
        return exist(location, pathInfo.fileName, pathInfo.dirs);
    }

    public byte[] readBytes(Location location, String relativePath) {
        PathInfo pathInfo = parsePath(relativePath);
        return readBytes(location, pathInfo.fileName, pathInfo.dirs);
    }

    public boolean writeBytes(Location location, String relativePath, byte[] data) {
        PathInfo pathInfo = parsePath(relativePath);
        return writeBytes(location, pathInfo.fileName, data, pathInfo.dirs);
    }

    public boolean exist(Location location, String fileName, String... dirs) {
        return getFile(location, fileName, dirs) != null;
    }

    public File getFile(Location location, String fileName, String... dirs) {
        return IOUtils.getFile(getRootDir(location), fileName, dirs);
    }

    public byte[] readBytes(Location location, String fileName, String... dirs) {
        return IOUtils.readBytes(getRootDir(location), fileName, dirs);
    }

    public boolean writeBytes(Location location, String fileName, byte[] data
            , String... dirs) {
        return IOUtils.writeBytes(getRootDir(location), fileName, data, dirs);
    }

    private void init() {
        mCacheDirectory = tryToGetCacheDir();
        mSDCardDirectory = tryToGetSDCardDir();

        if (mCacheDirectory == null && mSDCardDirectory != null) {
            mCacheDirectory = IOUtils.createDirIfNoExist(mSDCardDirectory, "cache");
        } else if (mCacheDirectory != null && mSDCardDirectory == null) {
            mSDCardDirectory = createOwnSDCardDirIfNoExist(mCacheDirectory);
        }
        mMediaDirectory = IOUtils.createDirIfNoExist(mSDCardDirectory, ".nomedia");
        if (mMediaDirectory == null) {
            mMediaDirectory = mSDCardDirectory;
        }
    }

    private File tryToGetCacheDir() {
        File cacheDir = CoreLog.gContext.getCacheDir();
        if (testDirectoryAvailable(cacheDir)) {
            return cacheDir;
        }
        return null;
    }

    private File tryToGetSDCardDir() {
        File sdCardDir;
        try {
            sdCardDir = Environment.getExternalStorageDirectory();
        } catch (Exception e) {
            return null;
        }
        File ownSDCardDir = createOwnSDCardDirIfNoExist(sdCardDir);
        if (testDirectoryAvailable(ownSDCardDir)) {
            return ownSDCardDir;
        }
        sdCardDir = new File(sdCardDir.getAbsolutePath().replace("0", "1"));
        ownSDCardDir = createOwnSDCardDirIfNoExist(sdCardDir);
        if (testDirectoryAvailable(ownSDCardDir)) {
            return ownSDCardDir;
        }
        return null;
    }

    private File createOwnSDCardDirIfNoExist(File root) {
        return IOUtils.createDirIfNoExist(root, CoreLog.gTag);
    }

    private boolean testDirectoryAvailable(File dir) {
        return dir != null && dir.isDirectory() && dir.canRead() && dir.canWrite();
    }

    private static PathInfo parsePath(String relativePath) {
        if (relativePath.startsWith(File.separator)) {
            relativePath = relativePath.substring(1);
        }
        if (relativePath.length() == 0) {
            throw new IllegalArgumentException("relativePath is empty");
        }
        String[] paths = relativePath.split(File.separator);
        int length = paths.length;
        PathInfo pathInfo = new PathInfo();
        if (length == 1) {
            pathInfo.fileName = relativePath;
            pathInfo.dirs = new String[0];
        } else {
            String[] dirs = new String[length - 1];
            ArrayUtils.remove(paths, dirs, paths.length - 1);
            pathInfo.fileName = paths[length - 1];
            pathInfo.dirs = dirs;
        }
        return pathInfo;
    }

    private static class PathInfo {
        public String fileName;
        public String[] dirs;
    }
}
