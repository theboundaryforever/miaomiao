package com.tcloud.core.thread.pool;

/**
 * Created by gaotianyu on 11/23/16.
 */
public interface WorkThread {
    void post(Runnable runnable, long delay);
}
