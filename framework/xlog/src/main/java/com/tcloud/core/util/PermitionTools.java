package com.tcloud.core.util;

import android.content.Context;
import android.hardware.Camera;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;

/**
 * Created by chen on 17/4/18.
 */

public class PermitionTools {

    private static final String TAG = "PermitionTools";

    public static final int STATE_RECORDING = -1;
    public static final int STATE_NO_PERMISSION = -2;
    public static final int STATE_SUCCESS = 1;

    private static boolean sCheckCamera = false;
    private static boolean sCheckAudio = false;
    private static boolean sCanUseCamera = false;
    private static boolean sCanUseAudio = false;

    // TODO: 2017/4/26 这个权限检测在使用声网的预览在部分机型上会崩溃
    public static boolean canUseCamera(Context context, boolean forceCheck) {
        if (sCheckCamera && !forceCheck) {
            return sCanUseCamera;
        }
        sCheckCamera = true;
        CameraProxy cameraProxy = new CameraProxy(context);
        sCanUseCamera = cameraProxy.openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
        cameraProxy.releaseCamera();
        return sCanUseCamera;
    }

    public static boolean canUseAudioRecord(boolean forceCheck) {
        return canUseAudioRecord(forceCheck, true);
    }

    public static boolean canUseAudioRecord(boolean forceCheck, boolean canBeUse) {
        if (sCheckAudio && !forceCheck) {
            return sCanUseAudio;
        }
        sCheckAudio = true;
        int state = STATE_SUCCESS;
        try {
            state = getRecordState();
        } catch (Exception e) {
            CoreUtils.crashIfDebug(e, "getRecordState error");
        }
        if (canBeUse) {
            //TODO: 2017/4/29  进入频道后sdk自己使用麦克风
            //TODO: 在部分手机上就没法判断是被sdk用了，还是权限被禁用，比如胡椒的手机
            sCanUseAudio = state == STATE_RECORDING || state == STATE_SUCCESS;
        } else {
            sCanUseAudio = state == STATE_SUCCESS;
        }
        return sCanUseAudio;
    }

    private static int getRecordState() {
        int minBuffer = AudioRecord.getMinBufferSize(44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        AudioRecord audioRecord = null;
        short[] point = new short[minBuffer];
        int readSize = 0;
        try {
            audioRecord = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, 44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, (minBuffer * 100));
            audioRecord.startRecording();//检测是否可以进入初始化状态
        } catch (Exception e) {
            if (audioRecord != null) {
                audioRecord.release();
                L.error(TAG, "无法进入录音初始状态");
            }
            return STATE_NO_PERMISSION;
        }
        if (audioRecord == null) {
            return STATE_NO_PERMISSION;
        }
        if (audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
            //6.0以下机型都会返回此状态，故使用时需要判断bulid版本
            //检测是否在录音中
            if (audioRecord != null) {
                audioRecord.stop();
                audioRecord.release();
                audioRecord = null;
                L.error(TAG, "录音机被占用");
            }
            return STATE_RECORDING;
        } else {
            //检测是否可以获取录音结果
            readSize = audioRecord.read(point, 0, point.length);
            if (readSize <= 0) {
                if (audioRecord != null) {
                    audioRecord.stop();
                    audioRecord.release();
                    audioRecord = null;
                }
                L.error(TAG, "录音的结果为空");
                return STATE_NO_PERMISSION;

            } else {
                if (audioRecord != null) {
                    audioRecord.stop();
                    audioRecord.release();
                    audioRecord = null;
                }
                return STATE_SUCCESS;
            }
        }
    }

}
