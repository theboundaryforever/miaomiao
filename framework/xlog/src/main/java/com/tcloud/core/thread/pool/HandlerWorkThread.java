package com.tcloud.core.thread.pool;

import android.os.Handler;
import android.os.HandlerThread;

/**
 * Created by gaotianyu on 11/23/16.
 */
class HandlerWorkThread extends HandlerThread implements WorkThread {
    private Handler mHandler = null;

    public HandlerWorkThread(String name) {
        super(name);
    }

    public HandlerWorkThread(String name, int priority) {
        super(name, priority);
    }

    @Override
    public void post(Runnable runnable, long delay) {
        if (0L == delay) {
            mHandler.post(runnable);
        } else {
            mHandler.postDelayed(runnable, delay);
        }
    }

    @Override
    public synchronized void start() {
        super.start();
        mHandler = new Handler(getLooper());
    }
}
