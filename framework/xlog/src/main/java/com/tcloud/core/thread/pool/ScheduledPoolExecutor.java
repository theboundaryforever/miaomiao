package com.tcloud.core.thread.pool;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Future;

/**
 * Created by gaotianyu on 11/23/16.
 */
class ScheduledPoolExecutor implements ScheduledExecutor {
    private ThreadFactory mFactory;
    private String mType;
    private int mPriority;
    private int mCount;
    private boolean mBusy[];
    private WorkThread mWorks[];
    private int mIndex = 0;


    ScheduledPoolExecutor(String type, int priority, int threadCount, ThreadFactory factory) {
        this.mType = type;
        this.mPriority = priority;
        this.mCount = threadCount;
        this.mFactory = factory;
        init();
    }

    private void init() {
        mBusy = new boolean[this.mCount];
        mWorks = new WorkThread[this.mCount];
        for (int i = 0; i < this.mCount; ++i) {
            mBusy[i] = false;
            mWorks[i] = mFactory.newThread(mType + "children " + i, mPriority);
        }
    }

    @Override
    public void execute(Runnable command, long delay) {
        realExecute(command, delay);
    }

    @Override
    public Future submit(Runnable command, long delay) {
        return null;
    }

    @Override
    public void execute(@NotNull Runnable command) {
        realExecute(command, 0);
    }

    public void realExecute(Runnable command, long delay) {
        synchronized (this) {
            int workIndex = getWorkIndex();
            mWorks[workIndex].post(new RunnableDelegate(command, workIndex), delay);
        }
    }

    private int getWorkIndex() {
        for (int i = 0; i < mCount; ++i) {
            if (mBusy[i]) {
                return i;
            }
        }
        return (++mIndex) % mCount;
    }

    class RunnableDelegate implements Runnable {
        private Runnable mTask;
        private int mWorkIndex;

        public RunnableDelegate(Runnable runnable, int index) {
            mTask = runnable;
            mWorkIndex = index;
        }

        @Override
        public void run() {
            synchronized (ScheduledPoolExecutor.this) {
                mBusy[mWorkIndex] = true;
            }
            mTask.run();
            synchronized (ScheduledPoolExecutor.this) {
                mBusy[mWorkIndex] = false;
            }
        }
    }

}
