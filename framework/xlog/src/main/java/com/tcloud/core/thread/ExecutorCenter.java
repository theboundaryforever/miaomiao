package com.tcloud.core.thread;

import com.tcloud.core.CoreUtils;
import com.tcloud.core.log.L;
import com.tcloud.core.thread.pool.LogUtil;
import com.tcloud.core.thread.pool.Pools;
import com.tcloud.core.thread.pool.ScheduledExecutor;

import java.util.concurrent.Future;

/**
 * Created by gaotianyu on 11/23/16.
 */
public class ExecutorCenter {
    private static final String TAG = "ExecutorCenter";
    private static final int RUN_TIME_MAX = 10000;
    private static final int RUN_TIME_NORMAL = 2000;
    private static ExecutorCenter msInstance = null;
    private static ScheduledExecutor mExecutor;

    public static ExecutorCenter getInstance() {
        if (null == msInstance) {
            synchronized (ExecutorCenter.class) {
                if (null == msInstance) {
                    msInstance = new ExecutorCenter();
                }
            }
        }
        return msInstance;
    }

    private ExecutorCenter() {
        mExecutor = Pools.newScheduledThreadPoolExecutor("self-executor", ParamBuild.CPU_COUNT + 1);
    }

    public static void setLogger(LogUtil.Logger logger) {
        LogUtil.setLog(logger);
    }

    public ScheduledExecutor getExecutor(String type) {
        return mExecutor;
    }

    public void post(final WorkRunnable runnable) {
        if (checkNull(runnable)) {
            return;
        }
        mExecutor.execute(getRealRun(runnable));
    }

    public void postDelay(final WorkRunnable runnable, long delay) {
        if (checkNull(runnable)) {
            return;
        }
        mExecutor.execute(getRealRun(runnable), delay);
    }

    public Future submitDelay(final WorkRunnable runnable, long delay) {
        if (checkNull(runnable)) {
            return null;
        }
        return mExecutor.submit(getRealRun(runnable), delay);
    }

    private Runnable getRealRun(final WorkRunnable runnable) {
        Runnable realRun = new Runnable() {
            @Override
            public void run() {
                try {
                    long startTime = System.currentTimeMillis();
                    runnable.run();
                    long useTime = System.currentTimeMillis() - startTime;
                    if (!runnable.isKeep()) {
                        if (useTime > RUN_TIME_MAX) {
                            CoreUtils.crashIfDebug("[ExecutorCenter] 耗时：run = %s, useTime = %d", runnable.getTag(), useTime);
                        } else if (useTime > RUN_TIME_NORMAL) {
                            L.error(TAG, "耗时：run = %s, useTime = %d", runnable.getTag(), useTime);
                        }
                    }
                } catch (Exception e) {
                    CoreUtils.crashIfDebug(e, "Executor(%s) Exception", runnable.getTag());
                }
            }
        };
        return realRun;
    }

    private boolean checkNull(Runnable runnable) {
        if (null == runnable) {
            L.info(TAG, "runnable null!!!!");
            return true;
        }
        return false;
    }
}
