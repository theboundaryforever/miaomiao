package com.tcloud.core.util;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import java.util.Vector;

/**
 * Created by chaoqun_liu on 2015/6/4.
 */
public class WakeHandler {
    public interface HandlerCallback {
        void handleMessage(Message msg);
    }

    private HandlerThread mThread;
    private Looper mLooper;
    private boolean mKeepWake;

    public WakeHandler() {
        init(null, true);
    }

    public WakeHandler(String name) {
        this(name, true);
    }

    public WakeHandler(String name, boolean keepWake) {
        mThread = new HandlerThread(name);
        mThread.start();
        init(mThread.getLooper(), keepWake);
    }

    public WakeHandler(Looper looper) {
        init(looper, true);
    }

    private void init(Looper looper, boolean keepWake) {
        mLooper = looper;
        mKeepWake = keepWake;
    }

    public HandlerAdapter getAdapter() {
        return getAdapter(null);
    }

    public HandlerAdapter getAdapter(boolean awake) {
        return getAdapter(null, awake);
    }

    public HandlerAdapter getAdapter(HandlerCallback callback) {
        return getAdapter(callback, mKeepWake);
    }

    /**
     * @param callback
     * @param awake
     * @return
     */
    public HandlerAdapter getAdapter(HandlerCallback callback, boolean awake) {
        return new HandlerAdapter(getLooper(), callback, awake);
    }

    /**
     * 获取当前HandlerThread的Looper，如果Looper未设置，则返回调用此方法所在线程的Looper
     *
     * @return
     */
    public Looper getLooper() {
        if (mLooper == null) {
            return Looper.myLooper();
        }

        return mLooper;
    }

    public String getThreadName() {
        return getLooper().getThread().getName();
    }

    /**
     * 对Handler进行封装，在sendMessage时获取WakeLock, 在deliverMessage之后释放WakeLock，以确保
     * 这一过程中系统CPU不会休眠，保证操作能够完成
     */
    public static class HandlerAdapter {
        private MsgHandler mHandler;

        public HandlerAdapter(Looper looper, HandlerCallback callback, boolean keepWake) {
            mHandler = new MsgHandler(looper, callback, keepWake);
        }

        public void removeMessages(int what) {
            Message msg = mHandler.obtainMessage(what);
            mHandler.removeMessageForWakeLock(msg);
            mHandler.removeMessages(what);
        }

        public void removeMessages(int what, Object object) {
            Message msg = mHandler.obtainMessage(what, object);
            mHandler.removeMessageForWakeLock(msg);
            mHandler.removeMessages(what, object);
        }

        public void removeCallbacksAndMessages(Object token) {

            mHandler.removeMessagesForWakeLock(token);
            mHandler.removeCallbacksAndMessages(token);
        }

        public void post(Runnable run) {
            mHandler.post(run);
        }

        public boolean postAtTime(Runnable r, Object token, long uptimeMillis) {
            return mHandler.postAtTime(r, token, uptimeMillis);
        }

        public boolean hasMessages(int what) {
            return mHandler.hasMessages(what);
        }

        public Message obtainMessage(int what) {
            return mHandler.obtainMessage(what);
        }

        public Message obtainMessage(int what, Object obj) {
            return mHandler.obtainMessage(what, obj);
        }

        public void sendMessage(Message msg) {
            mHandler.sendMessage(msg);
        }

        public void sendEmptyMessage(int what) {
            mHandler.sendEmptyMessage(what);
        }
    }

    private static class MsgHandler extends Handler {
        private Vector<Message> msgQueue = new Vector<Message>();
        private HandlerCallback mCallback;
        private boolean mKeepWakeLock;

        public MsgHandler(Looper looper, HandlerCallback callback, boolean keepWakeLock) {
            super(looper);
            mCallback = callback;
            mKeepWakeLock = keepWakeLock;
        }

        @Override
        public boolean sendMessageAtTime(Message msg, long uptimeMillis) {
            addMessageForWakeLock(msg);
            return super.sendMessageAtTime(msg, uptimeMillis);
        }

        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            removeMessageForWakeLock(msg);
        }

        @Override
        public void handleMessage(Message msg) {
            if (mCallback != null) {
                mCallback.handleMessage(msg);
            }
        }

        public synchronized void addMessageForWakeLock(Message msg) {
            if (!mKeepWakeLock) {
                return;
            }
            msgQueue.add(msg);
//            L.info(TAG, "WakeLockHelper [" + HandlerUtil.CURRENT.getThreadName() + "] addMessageForWakeLock, msg = " + msg);
            WakeLockHelper.getInstance().acquireWakeLock();
        }

        public synchronized void removeMessageForWakeLock(Message msg) {
            if (!mKeepWakeLock) {
                return;
            }
            boolean remove = msgQueue.remove(msg);
//            L.info(TAG, "WakeLockHelper  [" + HandlerUtil.CURRENT.getThreadName() + "] removeMessageForWakeLock, msg = " + msg + ", remove = " + remove);
            if (remove) {
                WakeLockHelper.getInstance().releaseWakeLock();
            }
        }

        public synchronized void removeMessagesForWakeLock(Object token) {
            if (!mKeepWakeLock) {
                return;
            }
            for (int i = 0; i < msgQueue.size(); i++) {
                Message msg = msgQueue.get(i);
                if (token == null || (token.equals(msg.obj))) {
                    removeMessageForWakeLock(msg);
                    i--;
                }
            }
        }

    }
}
