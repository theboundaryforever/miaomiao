package com.tcloud.core.util;

import android.content.Context;
import android.text.TextUtils;

import com.tcloud.core.log.L;

/**
 * Created by chen on 2018/10/11.
 */
public class ChannelUtils {

    private final static String TAG = "ChannelUtil";
    private final static String KEY_LAST_CHANNEL = "KEY_LAST_CHANNEL";

    private final static String DEFAULT_CHANNEL = "official";
    private final static String METADATA_KEY_CHANNEL = "InstallChannel";

    private static String sChannelName = "";


    public static String getChannel(Context context) {
        if (!TextUtils.isEmpty(sChannelName)) {
            return sChannelName;
        }
        String prfChannelId = Config.getInstance(context).getString(KEY_LAST_CHANNEL, "");
        String fileChannelId = ResourceUtils.getMetaValue(context, METADATA_KEY_CHANNEL, DEFAULT_CHANNEL);
        if (TextUtils.isEmpty(prfChannelId)) {
            L.info(TAG, "prfChannelId = is empty | fileChannelId = " + fileChannelId);
            Config.getInstance(context).setString(KEY_LAST_CHANNEL, fileChannelId);
            sChannelName = fileChannelId;
        } else {
            if (prfChannelId.equals(fileChannelId)) {
                sChannelName = fileChannelId;
                L.info(TAG, "prfChannelId == fileChannelId = " + fileChannelId);
            } else {
                L.info(TAG, "prfChannelId != fileChannelId = " + fileChannelId + " | prfChannelId = " + prfChannelId);
                sChannelName = prfChannelId;
            }
        }

        return sChannelName;

    }


}
