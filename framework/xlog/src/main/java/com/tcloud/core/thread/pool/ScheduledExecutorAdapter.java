package com.tcloud.core.thread.pool;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by gaotianyu on 12/2/16.
 */
public class ScheduledExecutorAdapter implements ScheduledExecutor {
    private ScheduledExecutorService mExecutor;
//    private static WorkThread mWork = new HandlerWorkFactory().newThread("ScheduledExecutorAdapter", Process.THREAD_PRIORITY_DEFAULT);

    public ScheduledExecutorAdapter(ScheduledExecutorService executor) {
        if (null == executor) {
            throw new NullPointerException("ScheduledThreadPoolExecutor may not be null");
        }
        mExecutor = executor;
    }

    @Override
    public void execute(final Runnable command, long delay) {
//        mWork.post(new Runnable() {
//            @Override
//            public void run() {
//                execute(command);
//            }
//        }, delay);
        mExecutor.schedule(command, delay, TimeUnit.MILLISECONDS);
    }

    @Override
    public void execute(@NotNull Runnable command) {
        mExecutor.execute(command);
    }


    @Override
    public Future submit(Runnable command, long delay) {
        return mExecutor.schedule(command, delay, TimeUnit.MILLISECONDS);
    }
}
