package com.tcloud.core.log;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import com.tencent.mars.xlog.Xlog;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by gaotianyu on 1/5/17.
 */
class XLogProxy {
    public static int MAX_FILE_SIZE = 2; //MB
    public static File sRootDir;
    public static final String XLOG = ".xlog";
    //rewrite in App
    public static String sLogPath = "/mewe/logs";
    public static final String LOG_NAME = "logs";
    public static final String UE_LOG_NAME = "uncaught_exception";
    public static Handler mHandler;
    private static long msMaxWait = 3000;

    private volatile static String mCurrentLog = null;

    static {
        com.tencent.mars.xlog.Log.setLogImp(new Xlog());
        try {
            System.loadLibrary("stlport_shared");
            System.loadLibrary("marsxlog");
        } catch (Exception e) {
            Log.e("XLogProxy", "loadLibrary error,", e);
        }
        Xlog.setConsoleLogOpen(false);
    }

    public static void logByLevel(final int type, final String msg, final String TAG, final String fileName) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (!fileName.equals(mCurrentLog)) {
                    switchOutputFile(fileName);
                } else {
                    // TODO: 17/4/20 大于3mb也不挪动了
                    //renameFileIfNeed();
                }
                logByLevelReal(type, msg, TAG);
            }
        });
    }

    private static void logByLevelReal(int type, String msg, String TAG) {
        switch (type) {
            case Log.VERBOSE: {
                Log.v(TAG, msg);
                // do not save verbose log to file
//                logToFile("VERBOSE: " + msg);
                break;
            }
            case Log.DEBUG: {
                Log.d(TAG, msg);
                com.tencent.mars.xlog.Log.d(TAG, msg);
                break;
            }
            case Log.INFO: {
                Log.i(TAG, msg);
                com.tencent.mars.xlog.Log.i(TAG, msg);
                break;
            }
            case Log.WARN: {
                Log.w(TAG, msg);
                com.tencent.mars.xlog.Log.w(TAG, msg);
                break;
            }
            case Log.ERROR: {
                Log.e(TAG, msg);
                com.tencent.mars.xlog.Log.e(TAG, msg);
                break;
            }
        }
    }

    public static String getCurrentLogAbsPath() {
        return String.format("%s/%s", sRootDir.getAbsolutePath(), getCurrentLogName());
    }

    public static String getCurrentLogName() {
        return String.format("%s_%s%s", LOG_NAME, new SimpleDateFormat("yyyyMMdd").format(new Date()), XLOG);
    }

    public static String getLastDayLogName() {
        return String.format("%s_%s%s", LOG_NAME, new SimpleDateFormat("yyyyMMdd").format(getLastDay()), XLOG);
    }

    public static String getCurrentUEHLogName() {
        return String.format("%s_%s%s", UE_LOG_NAME, new SimpleDateFormat("yyyyMMdd").format(new Date()), XLOG);
    }

    public static String getLastDayUEHLogName() {
        return String.format("%s_%s%s", UE_LOG_NAME, new SimpleDateFormat("yyyyMMdd").format(getLastDay()), XLOG);
    }

    /**
     * make sure logs.xlog is less than 3MB
     * TODO:logs.xlog不存在，没必要做这个挪动了
     */
    @Deprecated
    private static void renameFileIfNeed() {
        String dir = getRootDir().getAbsolutePath() + sLogPath + File.separator + mCurrentLog + XLOG;
        File file = new File(dir);
        if (file.exists()) {
            long fileSize = (file.length() >>> 20);
            if (fileSize >= MAX_FILE_SIZE) {
                renameReal(file);
            }
        }
    }


    @Deprecated
    private static void renameReal(File file) {
        if (file.exists()) {
            String dir = file.getParent(), fileName = file.getName().replace(XLOG, "");
            //close File
            com.tencent.mars.xlog.Log.appenderClose();

            //rename
            SimpleDateFormat simpleDateFormate = new SimpleDateFormat(
                    "-MM-dd-kk-mm-ss");
            String fileExt = simpleDateFormate.format(new Date());

            StringBuilder sb = new StringBuilder(dir);
            sb.append(File.separator).append(fileName).append(fileExt)
                    .append(XLOG);

            File fileNameTo = new File(sb.toString());
            file.renameTo(fileNameTo);
            //reopen
            Xlog.appenderOpen(Xlog.LEVEL_DEBUG, Xlog.AppednerModeAsync, "",
                    dir, fileName);
        }
    }


    private static void switchOutputFile(String file) {
        closeIfNeed();
        String dir = getRootDir().getAbsolutePath() + sLogPath;
        File fileDir = new File(dir);
        if (!fileDir.exists()) {
            boolean success = fileDir.mkdirs();
            if (!success) {
                Log.e("XLogProxy", "make dir failed !!! dir " + dir);
            }
        }
        Xlog.appenderOpen(Xlog.LEVEL_DEBUG, Xlog.AppednerModeAsync, L.sLogCacheDir, dir, file);
        mCurrentLog = file;
    }

    private static void closeIfNeed() {
        if (null != mCurrentLog) {
            com.tencent.mars.xlog.Log.appenderClose();
            mCurrentLog = null;
        }
    }

    public static void flushToDisk() {
        final CountDownLatch wait = new CountDownLatch(1);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (null != mCurrentLog) {
                    com.tencent.mars.xlog.Log.appenderFlush(true);
                }
                wait.countDown();
            }
        });
        try {
            wait.await(msMaxWait, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Log.e("XLogProxy", "flushToDisk error ", e);
        }
        Log.e("XLogProxy", "flushToDisk finish");
    }

    public static void close() {
        final CountDownLatch wait = new CountDownLatch(1);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                closeIfNeed();
                wait.countDown();
            }
        });
        try {
            wait.await(msMaxWait, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Log.e("XLogProxy", "close error ", e);
        }
        Log.e("XLogProxy", "close finish");
    }

    private static File getRootDir() {
        return sRootDir == null ? Environment.getExternalStorageDirectory() : sRootDir;
    }

    private static Date getLastDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

}
