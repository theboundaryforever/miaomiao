/**
 *
 */
package com.tcloud.core.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.os.Looper;
import android.view.Display;
import android.view.WindowManager;

import com.tcloud.core.log.L;

import java.util.List;

/**
 * @author daixiang
 */
public final class SystemUtils {

    public static boolean isMainThread() {

        return Looper.getMainLooper() == Looper.myLooper();
    }

    public static boolean isBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        for (RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(context.getPackageName())) {
                L.info(context.getPackageName(), "[back]此appimportace ="
                        + appProcess.importance
                        + ",context.getClass().getName()="
                        + context.getClass().getName());
                if (appProcess.importance != RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    L.info(context.getPackageName(), "[back]处于后台"
                            + appProcess.processName);
                    return true;
                } else {
                    L.info(context.getPackageName(), "[back]处于前台"
                            + appProcess.processName);
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean isMainProcess(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        int myPid = android.os.Process.myPid();
        String mainProcessName = context.getPackageName();
        for (RunningAppProcessInfo info : am.getRunningAppProcesses()) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }

    public static final Display getDisplay(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        return windowManager.getDefaultDisplay();
    }

    public static final Point getScreenSize(Context context) {
        Point ret = new Point();
        Display defaultDisplay = getDisplay(context);
        if (Build.VERSION.SDK_INT >= 13) {
            defaultDisplay.getSize(ret);
        } else {
            ret.x = defaultDisplay.getWidth();
            ret.y = defaultDisplay.getHeight();
        }
        return ret;
    }

    public static String getAppName(Context context) {
        String appName = "";
        try {
            PackageInfo pkg = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appName = pkg.applicationInfo.loadLabel(context.getPackageManager()).toString();
        } catch (PackageManager.NameNotFoundException e) {
            L.error("SystemUtils", e);
        }
        return appName;
    }
}
