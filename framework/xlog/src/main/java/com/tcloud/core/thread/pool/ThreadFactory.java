package com.tcloud.core.thread.pool;

/**
 * Created by gaotianyu on 11/23/16.
 */
public interface ThreadFactory {
    WorkThread newThread(String name, int priority);
}
