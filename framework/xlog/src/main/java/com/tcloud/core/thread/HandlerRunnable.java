package com.tcloud.core.thread;

import android.annotation.TargetApi;
import android.os.Looper;
import android.os.Process;

/**
 * Created by zxs on 2017/6/12.
 */

public class HandlerRunnable implements Runnable {
    int mTid = -1;
    Looper mLooper;

    @Override
    public void run() {
        mTid = Process.myTid();
        Looper.prepare();
        synchronized (this) {
            mLooper = Looper.myLooper();
            notifyAll();
        }
        onLooperPrepared();
        Looper.loop();
        mTid = -1;
    }

    protected void onLooperPrepared() {
    }

    public Looper getLooper() {
        // If the thread has been started, wait until the looper has been created.
        synchronized (this) {
            while (mLooper == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }
        return mLooper;
    }

    public boolean quit() {
        Looper looper = getLooper();
        if (looper != null) {
            looper.quit();
            return true;
        }
        return false;
    }

    @TargetApi(18)
    public boolean quitSafely() {
        Looper looper = getLooper();
        if (looper != null) {
            looper.quitSafely();
            return true;
        }
        return false;
    }

    public int getThreadId() {
        return mTid;
    }
}
