package com.tcloud.core.util;

import android.os.Handler;
import android.os.HandlerThread;

import com.tcloud.core.CoreUtils;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executor;

/**
 * Created by Legend on 2015/2/1.
 */
public class HandlerExecutor implements Executor {

    private Handler mHandler;

    public HandlerExecutor(String name) {
        initWorker(name);
    }

    private void initWorker(final String name) {
        HandlerThread thread = new HandlerThread(name);
        thread.start();
        mHandler = new Handler(thread.getLooper());
        thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                initWorker(name);
                CoreUtils.crashIfDebug(e, "[%s]worker throw exception!", name);
            }
        });
    }

    public HandlerExecutor(Handler handler) {
        mHandler = handler;
    }

    public Handler getHandler() {
        return mHandler;
    }

    @Override
    public void execute(@NotNull Runnable runnable) {
        mHandler.post(runnable);
    }
}
