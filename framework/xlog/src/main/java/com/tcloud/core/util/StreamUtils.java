package com.tcloud.core.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by legend on 15/6/17.
 */
public class StreamUtils {

    final static int BUFFER_SIZE = 4096;

    public static String inputStreamToString(InputStream in) {
        byte[] bytes = inputStreamToByteArray(in);
        return bytes == null ? null : new String(bytes);
    }

    public static byte[] inputStreamToByteArray(InputStream in) {
        try {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            byte[] data = new byte[BUFFER_SIZE];
            int count;
            while ((count = in.read(data, 0, BUFFER_SIZE)) != -1) {
                outStream.write(data, 0, count);
            }
            return outStream.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }
}
