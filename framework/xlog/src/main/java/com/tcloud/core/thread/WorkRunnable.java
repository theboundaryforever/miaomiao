package com.tcloud.core.thread;


import android.support.annotation.NonNull;

/**
 * Created by zxs on 2017/7/3.
 */

public abstract class WorkRunnable implements Runnable {

    //获取Run标志
    @NonNull
    public abstract String getTag();

    public boolean isKeep() {
        return false;
    }
}
