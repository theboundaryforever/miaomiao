package com.tcloud.core.util;

public class MathUtils {

    // [min, max)
    public static int random(int min, int max) {
        return (int) (Math.random() * (max - min) + min);
    }

    public static float min(float... fs) {
        int length = fs.length;
        if (length == 0) {
            return 0;
        }

        float min = fs[0];
        for (int i = 1; i < length; ++i) {
            min = Math.min(fs[i], min);
        }

        return min;
    }

    public static float max(float... fs) {
        int length = fs.length;
        if (length == 0) {
            return 0;
        }

        float max = fs[0];
        for (int i = 1; i < length; ++i) {
            max = Math.max(fs[i], max);
        }

        return max;
    }

    // ref: http://stackoverflow.com/questions/9578639/best-way-to-convert-a-signed-integer-to-an-unsigned-long
    public static long getUnsignedInt(int x) {
        return x & 0x00000000ffffffffL;
    }

    public static long intBackToLong(int intLong) {
        return Long.valueOf(Integer.toBinaryString(intLong), 2);
    }
}
