package com.tcloud.core.thread.pool;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;

/**
 * Created by gaotianyu on 11/23/16.
 */
public interface ScheduledExecutor extends Executor {
    void execute(Runnable command, long delay);

    Future submit(Runnable command, long delay);
}
