package com.tcloud.core.util;

import android.app.KeyguardManager;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.tcloud.core.log.L;

import java.util.UUID;

/*
 * device info
 */
public class DeviceUtils {

    public static boolean isScreenLocked(Context context) {
        KeyguardManager mKeyguardManager =
                (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        return mKeyguardManager.inKeyguardRestrictedInputMode();
    }

    public static String getImei(Context context) {
        String deviceId = getDeviceId(context);
        if (!FP.empty(deviceId)) {
            return deviceId;
        }
        String macAddress = getMacAddress(context);
        if (!FP.empty(macAddress)) {
            return macAddress;
        }
        Config config = Config.getInstance(context);
        String rndId = config.getString("RANDOM_UUID", null);
        if (StringUtils.isNullOrEmpty(rndId)) {
            rndId = UUID.randomUUID().toString();
            config.setString("RANDOM_UUID", rndId);
        }
        return rndId;
    }

    public static String getDeviceId(Context context) {
        try {
            String deviceId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
            if (StringUtils.isNullOrEmpty(deviceId) == false) {
                return deviceId;
            }
        } catch (Throwable throwable) {
            L.error(DeviceUtils.class, "getDeviceId fail: %s", throwable);
        }
        return "";
    }

    public static String getMacAddress(Context context) {
        WifiInfo wifiInfo = null;
        try {
            wifiInfo = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE))
                    .getConnectionInfo();
        } catch (Throwable throwable) {
            L.error(DeviceUtils.class, "getMacAddress failed " + throwable.toString());
            wifiInfo = null;
        }

        return wifiInfo == null ? "" : wifiInfo.getMacAddress();
    }

    public static String getDeviceDesc() {
        return String.format("%s-%s-%s", Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE);
    }

    public static String getProvidersName(Context context) {
        String providersName = "无Sim卡";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            String IMSI = telephonyManager.getSubscriberId();
            // IMSI号前面3位460是国家，紧接着后面2位00 02是中国移动，01是中国联通，03是中国电信。
            if (IMSI != null) {
                if (IMSI.startsWith("46000") || IMSI.startsWith("46002")) {
                    providersName = "中国移动";
                } else if (IMSI.startsWith("46001")) {
                    providersName = "中国联通";
                } else if (IMSI.startsWith("46003")) {
                    providersName = "中国电信";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            L.error("DeviceUtils", "getProvidersName error:%s", e.getMessage());
        }
        return providersName;
    }

}
