package com.tcloud.core.util;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.tcloud.core.log.L;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

public class CpuInfoUtils {
    public static final int BETTER_CPU_DEVICE_FREQ = 900;
    public static final int BETTER_CPU_DEVICE_FREQ_BOGO = 600;

    public static String getMaxCpuFreq() {
        String result = "";
        ProcessBuilder cmd;
        try {
            String[] args = {"/system/bin/cat",
                    "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"};
            cmd = new ProcessBuilder(args);
            Process process = cmd.start();
            InputStream in = process.getInputStream();
            byte[] re = new byte[24];
            while (in.read(re) != -1) {
                result = result + new String(re);
            }
            in.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            result = "N/A";
        }
        return result.trim();
    }

    public static String getMinCpuFreq() {
        String result = "";
        ProcessBuilder cmd;
        try {
            String[] args = {"/system/bin/cat",
                    "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"};
            cmd = new ProcessBuilder(args);
            Process process = cmd.start();
            InputStream in = process.getInputStream();
            byte[] re = new byte[24];
            while (in.read(re) != -1) {
                result = result + new String(re);
            }
            in.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            result = "N/A";
        }
        return result.trim();
    }

    public static String getCurCpuFreq() {
        String result = "N/A";
        BufferedReader br = null;
        try {
            FileReader fr = new FileReader(
                    "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
            br = new BufferedReader(fr);
            String text = br.readLine();
            result = text.trim();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public static String getCpuName() {
        BufferedReader br = null;
        try {
            FileReader fr = new FileReader("/proc/cpuinfo");
            br = new BufferedReader(fr);
            String text = br.readLine();
            String[] array = text.split(":\\s+", 2);
            for (int i = 0; i < array.length; i++) {
            }
            return array[1];
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }

    private static final String BogoMIPS = "BOGOMIPS";

    public static float getBogoMips(Context context) {
        float ret = -1;
        String cpuInfo = readCpuInfo(context);
        if (!StringUtils.isNullOrEmpty(cpuInfo)) {
            String bogoMips = null;
            BufferedReader br = new BufferedReader(new StringReader(cpuInfo));
            String line = null;
            try {
                while ((line = br.readLine()) != null) {
                    if (line.toUpperCase().startsWith(BogoMIPS)) {
                        int index = line.indexOf(':');
                        bogoMips = line.substring(index + 1).trim();
                        break;
                    }
                }

                ret = Float.valueOf(bogoMips);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return ret;
    }

    public static String readCpuInfo(Context context) {
        return FileUtils.getTxtFileContent(context, "/proc/cpuinfo");
    }

    // seq_printf(m, "Hardware\t: %s\n", machine_name);
    public static String getHardware(Context context) {
        String result = "wtf";
        String doc = readCpuInfo(context);
        if (TextUtils.isEmpty(doc)) return result;
        int start = doc.indexOf("Hardware");
        int end = doc.indexOf("\n", start);
        if (start == -1 || end == -1) {
            return "wtf";
        }
        String line = doc.substring(start, end);
        int pos = line.indexOf(":");
        if (pos == -1) {
            return "wtf";
        }
        return result;

    }

    private static boolean canSubString(int start, int end) {
        return start != -1 && start < end && end != -1;
    }

    private static final String PROCESSOR = "processor";

    public static boolean isMulCore(Context context) {
        int processorCount = 0;
        String cpuInfo = readCpuInfo(context);
        L.info(CpuInfoUtils.class, "cpuInfo: %s", cpuInfo);
        if (!StringUtils.isNullOrEmpty(cpuInfo)) {
            BufferedReader br = new BufferedReader(new StringReader(cpuInfo));
            String line = null;
            try {
                while ((line = br.readLine()) != null) {
                    if (line.startsWith(PROCESSOR)) {
                        processorCount++;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        L.info(CpuInfoUtils.class, "processor count: %d", processorCount);
        return processorCount > 1;
    }

    public static boolean powerfulCpuDevice(Context context) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            L.info(CpuInfoUtils.class,
                    "sdk version > 3.0. Build.VERSION.SDK_INT: %d",
                    Build.VERSION.SDK_INT);
            return true;
        }

        if (isMulCore(context)) {
            L.info(CpuInfoUtils.class, "multi core");
            return true;
        }

        String maxCpuFreq = CpuInfoUtils.getMaxCpuFreq();
        L.info(CpuInfoUtils.class, "maxCpuFreq: " + maxCpuFreq);
        if (!StringUtils.isNullOrEmpty(maxCpuFreq)) {
            int id = -1;
            try {
                id = Integer.valueOf(maxCpuFreq);
            } catch (NumberFormatException e) {
                id = -1;
            }
            L.info(CpuInfoUtils.class, "CpuDevice id: " + id);
            if (id != -1) {
                id = id / 1000;
                L.info(CpuInfoUtils.class, "powerful=%B",
                        id > CpuInfoUtils.BETTER_CPU_DEVICE_FREQ);
                return id > CpuInfoUtils.BETTER_CPU_DEVICE_FREQ;
            }
        }

        float bogoMips = CpuInfoUtils.getBogoMips(context);
        L.info(CpuInfoUtils.class, "CPU bogoMips: " + bogoMips);
        L.info(CpuInfoUtils.class, "powerful=%B",
                bogoMips > CpuInfoUtils.BETTER_CPU_DEVICE_FREQ);
        return bogoMips > CpuInfoUtils.BETTER_CPU_DEVICE_FREQ_BOGO;
    }

    public static String getProcessCpuRateInfo(Context context) {
        StringBuilder tv = new StringBuilder();
        BufferedReader br = null;
        try {
            String packName = context.getPackageName();
            String result;
            Process p;
            p = Runtime.getRuntime().exec("top -n 1");
            int line = 0;
            br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((result = br.readLine()) != null) {
                if (result.trim().length() < 1) {
                    continue;
                } else {
                    if (line < 3) {
                        //前三行表示标题
                        tv.append("\n").append(result);
                    } else {
                        if (result.contains(packName)) {
                            tv.append("\n").append(result);
                        }
                    }
                    line++;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return tv.toString();
    }
}
