package com.tcloud.core.util;

import android.os.Looper;

public enum WakeHandlerPool {
    WORKER("ThreadWorker"),
    MAIN(Looper.getMainLooper()),
    CURRENT();

    WakeHandlerPool() {
        mHandler = new WakeHandler();
    }

    WakeHandlerPool(String name) {
        mHandler = new WakeHandler(name);
    }

    WakeHandlerPool(String name, boolean keepWake) {
        mHandler = new WakeHandler(name, keepWake);
    }

    WakeHandlerPool(Looper looper) {
        mHandler = new WakeHandler(looper);
    }

    private WakeHandler mHandler;

    public WakeHandler getHandler() {
        return mHandler;
    }

}
