package com.tcloud.core.util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * similar to QCache in qt
 * <p>ref: http://qt-project.org/doc/qt-4.7/qcache.html
 *
 * @author qingfeng
 */
public class QCache<K, V> {
    private ArrayList<K> mKeyList = new ArrayList<K>();
    private static final Integer KDefaultCapacity = 50;
    private HashMap<K, V> mData = new HashMap<K, V>();
    private Integer mCapacity = 0;

    public QCache() {
        init(KDefaultCapacity);
    }

    public QCache(int capacity) {
        init(capacity);
    }

    public void put(K k, V v) {
        if (!mData.containsKey(k)) {
            trim();
            mKeyList.add(mKeyList.size(), k);
        }
        mData.put(k, v);
        bringToTop(k);
    }

    public V get(K k) {
        V v = mData.get(k);
        if (v != null) {
            bringToTop(k);
        }
        return v;
    }

    private void init(int capacity) {
        mCapacity = capacity;
    }

    private void bringToTop(K k) {
        int index = mKeyList.indexOf(k);
        if (index == -1 || index == 0)
            return;
        mKeyList.remove(index);
        mKeyList.add(0, k);
    }

    private void trim() {
        if (mKeyList.size() + 1 > mCapacity) {
            mKeyList.remove(mKeyList.size() - 1);
        }
    }
}
