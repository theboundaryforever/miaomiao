package com.tcloud.core.util;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by gaotianyu on 15/9/7.
 */
public class HandlerPoolExecutor implements Executor {

    private ExecutorService mExector;

    public HandlerPoolExecutor() {
        mExector = Executors.newCachedThreadPool();
    }

    /**
     * Executes the given command at some time in the future.  The command
     * may execute in a new thread, in a pooled thread, or in the calling
     * thread, at the discretion of the {@code Executor} implementation.
     *
     * @param command the runnable task
     * @throws RejectedExecutionException if this task cannot be
     *                                    accepted for execution
     * @throws NullPointerException       if command is null
     */
    @Override
    public void execute(@NotNull Runnable command) {
        mExector.execute(command);
    }
}
