package com.tcloud.core;

import android.os.Handler;
import android.os.Looper;

import com.tcloud.core.log.L;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.EventBusException;


/**
 * Created by legend on 15/3/20.
 */
public class CoreUtils {

    private static final String TAG = "CoreUtils";
    private static final Handler gMainHandler = new Handler(Looper.myLooper());
    private static boolean sIsTestEnv;

    public static void setIsTestEnv(boolean isTestEnv) {
        sIsTestEnv = isTestEnv;
    }

    public static boolean isTestEnv() {
        return sIsTestEnv;
    }

    public static <T> void send(T moduleCallback) {
        send(moduleCallback, false, isTestEnv());
    }

    public static <T> void sendSticky(T moduleCallback) {
        send(moduleCallback, true, isTestEnv());
    }

    public static <T> void send(T moduleCallback, boolean sticky, boolean needLog) {
        if (needLog) {
            L.debug(CoreUtils.class, "send callback: %s, sticky: %b", moduleCallback, sticky);
        }
        if (moduleCallback == null) {
            CoreUtils.crashIfDebug("moduleCallback == null");
            return;
        }

        try {
            if (sticky) {
                EventBus.getDefault().postSticky(moduleCallback);
            } else {
                EventBus.getDefault().post(moduleCallback);
            }
        } catch (Exception e) {
            CoreUtils.crashIfDebug(e, "EventBus exception");
        }
    }

    public static void removeStickyEvent(Object event) {
        EventBus.getDefault().removeStickyEvent(event);
    }

    public static <T> void register(T receiver) {
        try {
            EventBus.getDefault().register(receiver);
        } catch (EventBusException var2) {
//            L.debug(CoreUtils.class, var2);
        }
    }

    public static <T> void unregister(T receiver) {
        try {
            EventBus.getDefault().unregister(receiver);
        } catch (EventBusException var2) {
        }

    }

    public static void crashIfDebug(String format, Object... args) {
        crashIfDebug(null, format, args);
    }

    public static void crashIfDebug(final Throwable cause, final String format, Object... args) {
        if (cause != null) {
            cause.printStackTrace();
        }
        final String message = String.format(format, args);
        String causeMsg = cause == null ? "" : cause.getMessage();
        L.error(TAG, "crashIfDebug: %s.exception:%s", message, causeMsg);
        L.uncaughtException(cause);
        if (isTestEnv()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    throw cause == null ? new RuntimeException(message)
                            : new RuntimeException(message, cause);
                }
            });
        }
    }

    public static void crashIfInMainThreadDebug(String format, Object... args) {
        if (Thread.currentThread().getId() == Looper.getMainLooper().getThread().getId()) {
            crashIfDebug(format, args);
        }
    }

    public static void crashIfNotInMainThreadDebug(final String format, Object... args) {
        if (Thread.currentThread().getId() != Looper.getMainLooper().getThread().getId()) {
            crashIfDebug(format, args);
        }
    }

    public static void runInMainThread(Runnable runnable) {
        gMainHandler.post(runnable);
    }

    public static void runInMainThreadDelay(Runnable runnable, long delayMillis) {
        gMainHandler.postDelayed(runnable, delayMillis);
    }

}
