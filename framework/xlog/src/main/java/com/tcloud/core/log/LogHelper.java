package com.tcloud.core.log;



import com.tcloud.core.util.FP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by chen on 17/4/20.
 */

public class LogHelper {

    private static final String LOG_ZIP_FILE_NAME = "logsZip.zip";
    private static final String AGORA_RTC = "agora-rtc.log";


    private static String[] getLogFileName() {
        return new String[]{
                LogProxy.getLastDayFullLogName(),
                LogProxy.getLastDayUELogName(),
                LogProxy.getFullUELogName(),
                LogProxy.getFullLogName(),
                AGORA_RTC,
        };
    }

    public File getLog() {
        return getLog(false);
    }

    public File getLog(boolean requireAllDayLog) {
        try {
            LogProxy.flushToDisk();

            String dir = getDir();
            List<File> files = new ArrayList<File>();
            for (String logFilename : getLogFileName()) {
                String path = dir + File.separator + logFilename;
                if (!FP.empty(path)) {
                    File log = new File(path);
                    if (log.exists()) {
                        files.add(log);
                    }
                }
            }

            Pattern logPattern = null;
            if (requireAllDayLog) {
                SimpleDateFormat dateformat = new SimpleDateFormat("MMdd");
                String pattern = String.format("logs_%s.*(\\.xlog)$", dateformat.format(new Date()));
                logPattern = Pattern.compile(pattern);
            }

            File logDir = new File(dir);
            File[] logFiles = logDir.listFiles();
            if (null != logFiles) {
                for (File dump : logFiles) {
                    if (logPattern != null && logPattern.matcher(dump.getName()).matches()) {
                        files.add(dump);
                    }
                    if (dump.isFile() && dump.getName().contains(".dmp")) {
                        files.add(dump);
                    }
                }
            }
            return compressFile(files);
        } catch (Exception e) {
            L.error("feedback", "compress logs file error = " + e);
            return null;
        }
    }

    private File compressFile(List<File> files) {
        if (files.size() <= 0)
            return null;

        byte[] buffer = new byte[1024];
        try {
            String zipPath = getDir() + File.separator + LOG_ZIP_FILE_NAME;
            L.verbose("feedback", "zipPath = " + zipPath);

            File zipFile = new File(zipPath);
            if (zipFile.exists()) {
                zipFile.delete();
            }

            zipFile.createNewFile();

            FileOutputStream fos = new FileOutputStream(zipPath);
            ZipOutputStream zos = new ZipOutputStream(fos);

            for (File file : files) {
                if (file == null || !file.exists())
                    continue;

                ZipEntry ze = new ZipEntry(file.getName());
                zos.putNextEntry(ze);

                FileInputStream in =
                        new FileInputStream(file);

                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
            }

            zos.closeEntry();
            zos.close();

            return zipFile;
        } catch (Exception ex) {
            L.error("feedback", "compress logs file error = " + ex.getMessage());
            return null;
        }
    }

    private String getDir() {
        return L.getAbsLogPath();
    }

}
