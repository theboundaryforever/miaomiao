package com.tcloud.core.thread.pool;

import android.util.Log;

/**
 * Created by gaotianyu on 11/23/16.
 */
public class LogUtil {

    private static Logger mLog = new Logger() {
        @Override
        public void info(String TAG, String msg) {
            Log.i(TAG, msg);
        }

        @Override
        public void info(String TAG, Throwable t) {
            Log.i(TAG, t.toString());
        }

    };

    public static void info(String TAG, String msg) {
        mLog.info(TAG, msg);
    }

    public static void info(String TAG, Throwable t) {
        mLog.info(TAG, t);
    }

    public static void setLog(Logger log) {
        if (null == log) {
            throw new NullPointerException("Logger can not be null");
        }
        mLog = log;
    }

    public interface Logger {
        void info(String TAG, String msg);

        void info(String TAG, Throwable t);
    }
}
