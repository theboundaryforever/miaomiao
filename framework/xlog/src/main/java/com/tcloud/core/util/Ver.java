package com.tcloud.core.util;

import android.annotation.SuppressLint;

public class Ver {
    public int mMajor;
    public int mMinor;
    public int mBuild;

    public boolean bigThan(Ver v) {
        return (mMajor > v.mMajor) || ((mMajor == v.mMajor) && (mMinor > v.mMinor))
                || ((mMajor == v.mMajor) && (mMinor == v.mMinor) && (mBuild > v.mBuild));
    }

    public boolean smallThan(Ver v) {
        return (mMajor < v.mMajor) || ((mMajor == v.mMajor) && (mMinor < v.mMinor))
                || ((mMajor == v.mMajor) && (mMinor == v.mMinor) && (mBuild < v.mBuild));
    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (!(o instanceof Ver)) {
            return false;
        }
        Ver v = (Ver) o;
        return (mMajor == v.mMajor) && (mMinor == v.mMinor)
                && (mBuild == v.mBuild);
    }

    @Override
    public int hashCode() {
        return mMajor * 100 + mMajor * 10 + mBuild;
    }

    @SuppressLint("DefaultLocale")
    public String toString() {
        return String.format("%d.%d.%d", mMajor, mMinor, mBuild);
    }
}
