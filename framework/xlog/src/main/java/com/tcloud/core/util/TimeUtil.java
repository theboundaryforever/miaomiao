package com.tcloud.core.util;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TimeUtil {
    public static final SimpleDateFormat DATE_FORMAT_DEFAULT = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm");
    public static final SimpleDateFormat DATE_FORMAT_YMD_HMS = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat DATE_FORMAT_THIS_YEAR = new SimpleDateFormat(
            "MM-dd HH:mm");
    public static final SimpleDateFormat DATE_FORMAT_YMD = new SimpleDateFormat(
            "yyyy-MM-dd");
    public static final SimpleDateFormat DATE_FORMAT_MS = new SimpleDateFormat(
            "mm:ss");
    public static final SimpleDateFormat DATE_FORMAT_HMS = new SimpleDateFormat(
            "HH:mm:ss");
    public static final SimpleDateFormat DATE_FORMAT_TIME_DETAIL = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss:SS");

    public static final SimpleDateFormat DATE_FORMAT_TIME_DETAIL_SLASH = new SimpleDateFormat(
            "yyyy/MM/dd HH:mm");

    public static final SimpleDateFormat DATE_FORMAT_TIME_DETAIL_YMD = new SimpleDateFormat(
            "yyyy/MM/dd");

    private static SimpleDateFormat HOUR_OF_DAY = new SimpleDateFormat("HH:mm", Locale.CHINA);

    public static final long MINUTES_OF_HOUR = 60;
    public static final long SECONDS_OF_MINUTE = 60;
    public static final long MILLIS_OF_SECOND = 1000;

    private static final long ONE_MINUTE = 60000L;
    private static final long ONE_HOUR = 3600000L;
    private static final long ONE_DAY = 86400000L;

    private static final String ONE_MINUTE_AGO = "分钟前";
    private static final String ONE_HOUR_AGO = "小时前";
    private static final String ONE_DAY_AGO = "天前";
    private static final String SEVEN_DAY_AGO = "很久以前";

    private static Date sFormatDate = new Date();

    public static String parseTimeCurrentMillis(long time) {
        sFormatDate.setTime(time);
        return DATE_FORMAT_HMS.format(sFormatDate);
    }

    public static String parseTimeWithGMT(long time, SimpleDateFormat format) {
        time = time - TimeZone.getDefault().getOffset(time);
        sFormatDate.setTime(time);
        return format.format(sFormatDate);
    }

    public static String parseTimeHMSWithGMT(long time) {
        return parseTimeWithGMT(time, DATE_FORMAT_HMS);
    }

    public static String parseTimeMSWithGMT(long time) {
        return parseTimeWithGMT(time, DATE_FORMAT_MS);
    }

    public static String parseTimeYMD(long time) {
        return parseTimeWithGMT(time, DATE_FORMAT_YMD);
    }

    public static String parseTimeThisYear(long time) {
        return parseTimeWithGMT(time, DATE_FORMAT_THIS_YEAR);
    }

    public static String parseTimeAsShortAsPossibleWithGMT(long time) {
//        long sec = (time / 1000) % 60;
//        long minute = (time / 60 / 1000) % 60;
        long hour = time / 60 / 60 / 1000;
        if (hour > 0) {
            return parseTimeHMSWithGMT(time);
        } else {
            return parseTimeMSWithGMT(time);
        }
    }

    private static final Calendar CALENDAR = Calendar.getInstance();
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat();

    public static long getSeconds(long milliseconds) {
        return (milliseconds - toMinutes(milliseconds) * (1000 * 60)) / 1000;
    }

    public static long toMinutes(long milliseconds) {
        return milliseconds / (1000 * 60);
    }

    public static long toHours(long milliseconds) {
        return milliseconds / (1000 * 60 * 60);
    }

    public static long toDays(long date) {
        return toHours(date) / 24L;
    }

    public static long getMinutes(long milliseconds) {
        return (milliseconds - toHours(milliseconds) * 1000 * 60 * 60) / (1000 * 60);
    }

    public static long getHours(long milliseconds) {
        return (milliseconds / (1000 * 60 * 60) + 8) % 24;
    }

    public static String getMSFormatTime(long milliseconds) {
        long minutes = getMinutes(milliseconds);
        long seconds = getSeconds(milliseconds);
        return String.format("%02d:%02d", minutes, seconds);
    }

    public static String getHMFormatTime(long milliseconds) {
        long hours = getHours(milliseconds);
        long minutes = getMinutes(milliseconds);
        return String.format("%02d:%02d", hours, minutes);
    }

    public static String getHMAMPMFormatTime(long milliseconds) {
        long hours = getHours(milliseconds);
        if (hours >= 12) {
            hours = hours - 12;
        }
        long minutes = getMinutes(milliseconds);
        return String.format("%02d:%02d", hours, minutes);
    }


    public static String getPropHMSFormatTime(long milliseconds) {
        long hour = getHours(milliseconds);
        if (hour == 0) return getMSFormatTime(milliseconds);
        long minutes = getMinutes(milliseconds);
        long seconds = getSeconds(milliseconds);
        return String.format("%02d:%02d:%02d", hour, minutes, seconds);

    }

    public static String getFullHMSFormatTime(long milliseconds) {
        long hour = getHours(milliseconds);
        long minutes = getMinutes(milliseconds);
        long seconds = getSeconds(milliseconds);
        return String.format("%02d:%02d:%02d", hour, minutes, seconds);
    }

    public static String getFormattedTime(String formatStr, long milliseconds) {
        CALENDAR.setTimeInMillis(milliseconds);
        DATE_FORMAT.applyLocalizedPattern(formatStr);
        return DATE_FORMAT.format(CALENDAR.getTime());
    }

    public static boolean isToyear(long milliseconds) {
        CALENDAR.setTimeInMillis(milliseconds);
        int year = CALENDAR.get(Calendar.YEAR);
        CALENDAR.setTimeInMillis(System.currentTimeMillis());
        return year == CALENDAR.get(Calendar.YEAR);
    }


    public static String parseTimeWithMilliseconds(long milliseconds) {
        CALENDAR.setTimeInMillis(milliseconds);
        int year = CALENDAR.get(Calendar.YEAR);
        int month = CALENDAR.get(Calendar.MONTH) + 1;
        int day = CALENDAR.get(Calendar.DATE);
        StringBuffer timeSB = new StringBuffer();
        timeSB.append(year).append("/").append(month).append("/").append(day);
        return timeSB.toString();
    }

    public static String getLastLoginAtTimeStr(long timeStamp) {
        return getTimeStr(timeStamp*1000);
    }

    public static String getTimeStr(long timeStamp) {
        long delta = new Date().getTime() - timeStamp;
        if (delta < 1L * ONE_MINUTE) {
            return 1 + ONE_MINUTE_AGO;
        }
        if (delta < 60L * ONE_MINUTE) {
            long minutes = toMinutes(delta);
            return (minutes <= 0 ? 1 : minutes) + ONE_MINUTE_AGO;
        }
        if (delta < 24L * ONE_HOUR) {
            long minutes = toMinutes(delta);
            int hours = (int) Math.rint(minutes/60f);
            return hours + ONE_HOUR_AGO;
        }
        if (delta < 7L * ONE_DAY) {
            long days = toDays(delta);
            return (days <= 0 ? 1 : days) + ONE_DAY_AGO;
        }
        return SEVEN_DAY_AGO;

    }

}
