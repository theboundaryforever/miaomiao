package com.tcloud.core.util;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

/**
 * Created by qingfeng on 15/7/24.
 */
public class ConfigWithTimeout {

    private static ConfigWithTimeout mInstance;
    private static final String EXPIRED_TIME_PREFIX = "expired_time_";
    private static final String CONFIG_TIMEOUT_PREFIX = "config_timeout_";
    private Context mContext;

    public synchronized static ConfigWithTimeout instance(@NotNull Context context) {
        if (mInstance == null) {
            mInstance = new ConfigWithTimeout(context);
        }
        return mInstance;
    }

    private ConfigWithTimeout(@NotNull Context context) {
        mContext = context;
    }

    private String getExpiredTimeKey(final String key) {
        return EXPIRED_TIME_PREFIX + key;
    }

    private String getTimeoutKey(final String key) {
        return CONFIG_TIMEOUT_PREFIX + key;
    }

    private boolean setExpiredTime(final String key, final long timeoutMillis) {
        String expiredTimeKey = getExpiredTimeKey(key);
        boolean result = Config.getInstance(mContext).setLong(expiredTimeKey,
                System.currentTimeMillis() + timeoutMillis);
        Utils.dwAssert(result);
        return result;
    }

    /**
     * set string to config with specific expired milli seconds
     *
     * @param key           config key
     * @param value         config value
     * @param timeoutMillis expired milli seconds
     * @return success set or not
     */
    public synchronized boolean setString(final String key, String value, final long timeoutMillis) {
        boolean result = Config.getInstance(mContext).setString(
                getTimeoutKey(key), value);
        Utils.dwAssert(result);
        result |= setExpiredTime(key, timeoutMillis);
        return result;
    }

    public synchronized boolean setFloat(final String key, float value, final long timeoutMillis) {
        boolean result = Config.getInstance(mContext).setFloat(
                getTimeoutKey(key), value);
        Utils.dwAssert(result);
        result |= setExpiredTime(key, timeoutMillis);
        return result;
    }

    public synchronized boolean setInteger(final String key, int value, final long timeoutMillis) {
        boolean result = Config.getInstance(mContext).setInt(
                getTimeoutKey(key), value);
        Utils.dwAssert(result);
        result |= setExpiredTime(key, timeoutMillis);
        return result;
    }

    private boolean isExpired(final String key) {
        String expiredTimeKey = getExpiredTimeKey(key);
        long expiredTime = Config.getInstance(mContext).getLong(expiredTimeKey, 0);
        return expiredTime < System.currentTimeMillis();
    }

    public synchronized String getString(String key, String defaultValue) {
        if (isExpired(key)) {
            return defaultValue;
        } else {
            return Config.getInstance(mContext).getString(getTimeoutKey(key), defaultValue);
        }
    }

    public synchronized float getFloat(String key, float defaultValue) {
        if (isExpired(key)) {
            return defaultValue;
        } else {
            return Config.getInstance(mContext).getFloat(getTimeoutKey(key), defaultValue);
        }
    }

    public synchronized int getInteger(String key, int defaultValue) {
        if (isExpired(key)) {
            return defaultValue;
        } else {
            return Config.getInstance(mContext).getInt(getTimeoutKey(key), defaultValue);
        }
    }
}
