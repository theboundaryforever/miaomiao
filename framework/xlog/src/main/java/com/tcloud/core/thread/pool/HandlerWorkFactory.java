package com.tcloud.core.thread.pool;

/**
 * Created by gaotianyu on 11/23/16.
 */
public class HandlerWorkFactory implements ThreadFactory {
    @Override
    public WorkThread newThread(String name, int priority) {
        HandlerWorkThread handlerWorkThread = new HandlerWorkThread(name, priority);
        handlerWorkThread.start();
        return handlerWorkThread;
    }
}
