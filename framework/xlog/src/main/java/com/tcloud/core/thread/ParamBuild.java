package com.tcloud.core.thread;


import static android.os.Process.THREAD_PRIORITY_BACKGROUND;

/**
 * Created by gaotianyu on 12/2/16.
 */
public class ParamBuild {
    public static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
    private int mCoreCount = CPU_COUNT + 1;
    private String mType = null;
    private int mPriority = THREAD_PRIORITY_BACKGROUND;
    private boolean mHasLooper = true;

    public ParamBuild setCoreCount(int count) {
        mCoreCount = count;
        return this;
    }

    public ParamBuild setType(String type) {
        mType = type;
        return this;
    }

    public ParamBuild setPriority(int priority) {
        mPriority = priority;
        return this;
    }

    public ParamBuild setHasLooper(boolean has) {
        mHasLooper = has;
        return this;
    }

    public Params build() {
        return new Params(mCoreCount, mType, mPriority, mHasLooper);
    }

    public static class Params {
        private int mCoreCount;
        private String mType;
        private int mPriority;
        private boolean mHasLooper;

        public Params(int count, String type, int priority, boolean hasLooper) {
            mCoreCount = count;
            mType = type;
            mPriority = priority;
            mHasLooper = hasLooper;
        }

        public int getCount() {
            return mCoreCount;
        }

        public String getType() {
            return mType;
        }

        public int getPriority() {
            return mPriority;
        }

        public boolean hasLooper() {
            return mHasLooper;
        }

    }
}
