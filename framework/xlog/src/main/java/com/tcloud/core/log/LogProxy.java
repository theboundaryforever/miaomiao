package com.tcloud.core.log;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import java.io.File;


/**
 * Created by gaotianyu on 2/10/17.
 */

public class LogProxy {
    private static volatile boolean msUseXlog = true;

    private static Logger mLog = new XLogger();
    private static Handler mHandler;

    static {
        HandlerThread thread = new HandlerThread("LogThread");
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.start();
        mHandler = new Handler(thread.getLooper());
        XLogProxy.mHandler = mHandler;
    }

    public static void setUseXlog(boolean isXlog) {
        msUseXlog = isXlog;
        mLog = msUseXlog ? new XLogger() : new OldLogger();
    }

    public static boolean isXlog() {
        return msUseXlog;
    }

    public static void resetRoot(String rootPath) {
        File root = new File(rootPath);
        XLogProxy.sRootDir = root;
        LogToES.sRootDir = root;
    }

    public static void resetLogPath(String path) {
        XLogProxy.sLogPath = path;
        LogToES.sLogPath = path;
    }

    public static File getRoot() {
        return msUseXlog ? XLogProxy.sRootDir : LogToES.sRootDir;
    }

    public static void flushToDisk() {
        if (msUseXlog) {
            XLogProxy.flushToDisk();
        } else {
            LogToES.flushToDisk();
        }
    }

    public static String getLogPath() {
        return msUseXlog ? XLogProxy.sLogPath : LogToES.sLogPath;
    }

    public static String getFullUELogName() {
        return msUseXlog ? XLogProxy.getCurrentUEHLogName() : LogToES.UE_LOG_NAME;
    }

    public static String getLastDayUELogName() {
        return msUseXlog ? XLogProxy.getLastDayUEHLogName() : LogToES.UE_LOG_NAME;
    }

    public static String getUELogName() {
        return msUseXlog ? XLogProxy.UE_LOG_NAME : LogToES.UE_LOG_NAME;
    }

    public static String getFullLogName() {
        return msUseXlog ? XLogProxy.getCurrentLogName() : LogToES.LOG_NAME;
    }

    public static String getLastDayFullLogName() {
        return msUseXlog ? XLogProxy.getLastDayLogName() : LogToES.LOG_NAME;
    }

    // TODO: 17/4/20  LogToES
    public static String getCurrentLogAbsPath() {
        return msUseXlog ? XLogProxy.getCurrentLogAbsPath() : "";
    }

    public static String getLogName() {
        return msUseXlog ? XLogProxy.LOG_NAME : LogToES.LOG_NAME;
    }


    public static void close() {
        if (msUseXlog) {
            XLogProxy.close();
        } else {
            LogToES.flushToDisk();
        }
    }


    static void logByLevel(int type, String msg, String TAG, String fileName) {
        if (null != mLog) {
            mLog.logByLevel(type, msg, TAG, fileName);
        }
    }

    interface Logger {
        void logByLevel(int type, String msg, String TAG, String fileName);
    }

    static class XLogger implements Logger {

        @Override
        public void logByLevel(int type, String msg, String TAG, String fileName) {
            XLogProxy.logByLevel(type, msg, TAG, fileName);
        }
    }

    static class OldLogger implements Logger {

        @Override
        public void logByLevel(int type, String msg, String TAG, String fileName) {
            String logMsg = "";
            switch (type) {
                case Log.VERBOSE: {
                    Log.v(TAG, msg);
                    break;
                }
                case Log.DEBUG: {
                    Log.d(TAG, msg);
                    logMsg = "DEBUG: " + msg;
                    break;
                }
                case Log.INFO: {
                    Log.i(TAG, msg);
                    logMsg = "INFO: " + msg;
                    break;
                }
                case Log.WARN: {
                    Log.w(TAG, msg);
                    logMsg = "WARN: " + msg;
                    break;
                }
                case Log.ERROR: {
                    Log.e(TAG, msg);
                    logMsg = "ERROR: " + msg;
                    break;
                }
            }
            LogToES.writeLogToFile(LogToES.sLogPath, LogToES.LOG_NAME, logMsg);
        }
    }
}
