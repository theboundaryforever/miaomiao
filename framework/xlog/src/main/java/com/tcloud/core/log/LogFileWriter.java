package com.tcloud.core.log;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gaotianyu on 4/13/16.
 */
class LogFileWriter {
    private static class Buffer {
        Buffer() {
            buffer = new StringBuilder();
            current = 0;
        }

        public final StringBuilder buffer;
        public int current = 0;
    }

    private static Map<String, Buffer> mBuffer = new HashMap<>();
    protected static int mBufferCount = 15;
    private String mFileName;

    public LogFileWriter(String file) {
        if (null == file) {
            throw new NullPointerException("file may not be null");
        }
        mFileName = file;
        if (!mBuffer.containsKey(file)) {
            mBuffer.put(file, new Buffer());
        }
    }

    public void write(StringBuffer msg) throws IOException {
        if (null == msg || 0 == msg.length()) {
            return;
        }
        Buffer buf = mBuffer.get(mFileName);
        if (null == buf) {
            throw new NullPointerException("Buffer may not be null !");
        }
        buf.buffer.append(msg);
        ++buf.current;
        if (buf.current >= mBufferCount) {
            flush();
        }
    }

    public void write(String msg) throws IOException {
        if (null == msg || msg.isEmpty()) {
            return;
        }
        Buffer buf = mBuffer.get(mFileName);
        if (null == buf) {
            throw new NullPointerException("Buffer may not be null !");
        }
        buf.buffer.append(msg);
        ++buf.current;
        if (buf.current >= mBufferCount) {
            flush();
        }
    }

    public void flush() throws IOException {
        flush(mFileName);
    }

    private static void flush(String name) throws IOException {
        Buffer buf = mBuffer.get(name);
        if (null == buf) {
            throw new NullPointerException("Buffer may not be null !");
        }
        if (buf.current == 0 || buf.buffer.length() == 0) {
            return;
        }
        synchronized (buf) {
            FileWriter fileWriter = new FileWriter(new File(name), true);
            fileWriter.write(buf.buffer.toString());
            fileWriter.flush();
            fileWriter.close();
            buf.current = 0;
            buf.buffer.delete(0, buf.buffer.length());
        }
    }

    public static void flushAll() throws IOException {
        for (String name : mBuffer.keySet()) {
            flush(name);
        }
    }
}
