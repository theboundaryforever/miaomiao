package com.tcloud.core.util;

/**
 * reuse object in object pool, in order to low gc frequency
 *
 * @param <T>
 * @author qingfeng
 */
public class ObjectPool<T> {
    public interface ObjectCreator {
        Object create(Object[] args);
    }

    private Object[] pooledObjs;
    private int freeIdx;
    private ObjectCreator creator;

    public ObjectPool(int size, ObjectCreator objectCreator) {
        pooledObjs = new Object[size];
        creator = objectCreator;
        freeIdx = -1;
    }

    @SuppressWarnings("unchecked")
    synchronized public T borrow(Object[] args) {
        if (freeIdx < 0) {
            return (T) creator.create(args);
        } else {
            return (T) pooledObjs[freeIdx--];
        }
    }

    synchronized public void release(T v) {
        Utils.dwAssert(v != null);
        // check pool full or not, if full just drop it
        if (freeIdx >= pooledObjs.length - 1) {
            return;
        }
        pooledObjs[++freeIdx] = v;
    }

    synchronized public void reset() {
        for (int i = 0; i <= freeIdx; i++) {
            pooledObjs[i] = null;
        }
        freeIdx = -1;
    }
}
