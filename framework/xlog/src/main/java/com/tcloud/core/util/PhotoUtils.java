package com.tcloud.core.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.util.LruCache;
import android.widget.ImageView;

import com.tcloud.core.log.L;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by gaotianyu on 14-11-14.
 */
public class PhotoUtils {
    private final static int IMAGE_HIGH = 1200;
    private final static int IMAGE_WIDTH = 1600;

    //Create image file to store photo，and file name is unique
    public static File createImageFile(String path) throws IOException {
        File storageDir = null;
        if (StringUtils.isNullOrEmpty(path) || !(new File(path).isDirectory())) {
            storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
        } else {
            storageDir = new File(path);
        }
        FileUtils.ensureDirExists(storageDir.getAbsolutePath());
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    public static void addPhotoToGallery(Context context, String path) {
        if (StringUtils.isNullOrEmpty(path)) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(path);
        Uri contentUri = Uri.fromFile(file);
        intent.setData(contentUri);
        context.sendBroadcast(intent);
    }

    public static String compressUsingCacheDir(Context context, String filePath, boolean needOrientation) throws IOException {
        if (null == filePath) {
            return null;
        }
        String path = null;
        File dir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        if (dir == null) {
            path = Environment.getExternalStorageDirectory().getAbsolutePath();//getPath().replace("0", "1");
        } else {
            path = dir.getAbsolutePath();
        }
        if (needOrientation) {
            return compressAndAdjustOrientation(filePath, createImageFile(path).getAbsolutePath());
        } else {
            return compress(filePath, createImageFile(path).getAbsolutePath());
        }
    }

    public static String compress(String filePath, String saveAs) throws IOException {
        Bitmap bmpPic = decodeFile(filePath);
        FileOutputStream bmpFile = new FileOutputStream(saveAs);
        bmpPic.compress(Bitmap.CompressFormat.JPEG, 60, bmpFile);
        bmpFile.flush();
        bmpFile.close();
        return saveAs;
    }

    public static String compressAndAdjustOrientation(String filePath, String saveAs) throws IOException {
        int degree = getCameraPhotoOrientation(filePath);
        Bitmap bmpPic = decodeFile(filePath);
        bmpPic = 0 == degree ? bmpPic : rotaingImageView(degree, bmpPic);
        FileOutputStream bmpFile = new FileOutputStream(saveAs);
        bmpPic.compress(Bitmap.CompressFormat.JPEG, 60, bmpFile);
        bmpFile.flush();
        bmpFile.close();
        return saveAs;
    }

    //provide by android developer
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static Bitmap decodeFile(String path) throws IOException {
        File f = new File(path);
        Bitmap b = null;

        //Decode image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        FileInputStream fis = new FileInputStream(f);
        BitmapFactory.decodeStream(fis, null, options);
        fis.close();

        int scale = 1;
        if (options.outWidth > options.outHeight && options.outWidth > IMAGE_WIDTH) {
            scale = options.outWidth / IMAGE_WIDTH;
        } else if (options.outWidth < options.outHeight && options.outHeight > IMAGE_HIGH) {
            scale = options.outHeight / IMAGE_HIGH;
        } else if (options.outWidth == options.outHeight && options.outHeight > IMAGE_WIDTH) {
            scale = options.outHeight / IMAGE_WIDTH;
        }
        if (scale <= 0) {
            scale = 1;
        }
//        int scale=calculateInSampleSize(options,IMAGE_WIDTH,IMAGE_HIGH);
        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();

        o2.inSampleSize = scale;
        fis = new FileInputStream(f);
        b = BitmapFactory.decodeStream(fis, null, o2);
        fis.close();

        return b;
    }

    private static LruCache<String, Bitmap> mCache;

    public static void photoBind(ImageView imageView, String path, int width, int height) {
        imageView.setImageBitmap(getThumbnailUtils(path, width, height));
    }

    public static void photoBind(ImageView imageView, String path) {
        int width = imageView.getLayoutParams().width;
        int height = imageView.getLayoutParams().height;
        imageView.setImageBitmap(getThumbnailUtils(path, width, height));
    }

    public static Bitmap getThumbnailUtils(String path, int width, int height) {
        if (null == mCache) {
            int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            int cacheSize = maxMemory / 8;
            mCache = new LruCache<String, Bitmap>(cacheSize);
        }
        String Key = path + "size" + String.valueOf(width) + ":" + String.valueOf(height);
        if (null == mCache.get(Key)) {
            int degree = PhotoUtils.getCameraPhotoOrientation(path);
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            Bitmap temp = ThumbnailUtils.extractThumbnail(bitmap, width, height);
            if (null == temp) {
                return null;
            }
            mCache.put(Key, degree == 0 ? temp : rotaingImageView(degree, temp));
            return temp;
        } else {
            return mCache.get(Key);
        }
    }

    //decode the image from file to bitmap.
    //This function can not be replaced by displayImage().There is a bug in displayImage
    private static void readFromFile(ImageView imageView, String path) {
        // Get the dimensions of the View
        int targetW = imageView.getLayoutParams().width;
        int targetH = imageView.getLayoutParams().height;
        readFromFile(imageView, path, targetW, targetH);
    }

    private static void readFromFile(ImageView imageView, String path, int width, int height) {
        // Get the dimensions of the View
        int targetW = width;
        int targetH = height;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
        imageView.setImageBitmap(bitmap);
    }

    /**
     * rotate Photo
     *
     * @param angle
     * @param bitmap
     * @return Bitmap
     */
    public static Bitmap rotaingImageView(int angle, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return resizedBitmap;
    }

    /**
     * read photo Orientation
     *
     * @param imagePath absolute path
     * @return degree
     */
    public static int getCameraPhotoOrientation(String imagePath) {
        int rotate = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            L.error("PhotoUtils error : ", e);
        }
        return rotate;
    }

}
