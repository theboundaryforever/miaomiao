package com.tcloud.core.util;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.os.Handler;

import java.util.ArrayList;

/**
 * Created by chen on 2017/11/17.
 */

public class RunnableAnimator extends Animator {

    private Runnable mRunnable;
    private boolean mEndListenersCalled = false;
    private boolean mRunning = false;
    private long mStartDelay = 0;
    private long mDuration = 0;  //in ms
    protected Handler mHandler = new Handler();

    public RunnableAnimator() {
    }

    public RunnableAnimator(Runnable runnable) {
        mRunnable = runnable;
    }

    public RunnableAnimator(Runnable runnable, int startDelayOfSec) {
        this(runnable);
        setStartDelay(startDelayOfSec * 1000);
    }

    public RunnableAnimator(Runnable runnable, long startDelayOfMillis) {
        this(runnable);
        setStartDelay(startDelayOfMillis);
    }

    public Runnable getRunnable() {
        return mRunnable;
    }

    public void setRunnable(Runnable runnable) {
        this.mRunnable = runnable;
    }

    @Override
    public void start() {
        mRunning = true;

        mHandler.postDelayed(mStartAnimatorRunner, mStartDelay);

        mHandler.postDelayed(mEndAnimatorRunner, mStartDelay + getDuration());

    }

    private Runnable mStartAnimatorRunner = new Runnable() {
        @Override
        public void run() {
            doAnimator();
        }
    };

    private Runnable mEndAnimatorRunner = new Runnable() {
        @Override
        public void run() {
            endAnimator();
        }
    };

    @Override
    public void cancel() {
        mHandler.removeCallbacks(mStartAnimatorRunner);
        mHandler.removeCallbacks(mEndAnimatorRunner);

        if (getListeners() != null) {
            ArrayList<AnimatorListener> tmpListeners =
                    (ArrayList<AnimatorListener>) getListeners().clone();
            int numListeners = tmpListeners.size();
            for (int i = 0; i < numListeners; ++i) {
                tmpListeners.get(i).onAnimationCancel(this);
            }
        }
        endAnimator();
    }

    protected void endAnimator() {
        mRunning = false;
        notifyListeners(false);
    }

    protected void doAnimator() {
        notifyListeners(true);
        if (mRunnable != null) {
            mRunnable.run();
        }
    }

    @Override
    public long getStartDelay() {
        return mStartDelay;
    }

    @Override
    public void setStartDelay(long startDelay) {
        mStartDelay = startDelay;
    }

    public Animator setDurationOfSec(long duration) {
        return setDuration(duration * 1000);
    }

    @Override
    public Animator setDuration(long duration) {
        mDuration = duration;
        return this;
    }

    @Override
    public long getDuration() {
        return mDuration;
    }

    @Override
    public void setInterpolator(TimeInterpolator value) {
    }

    @Override
    public boolean isRunning() {
        return mRunning;
    }

    private void notifyListeners(boolean bStarted) {
        if (getListeners() != null && !mEndListenersCalled) {
            ArrayList<AnimatorListener> tmpListeners =
                    (ArrayList<AnimatorListener>) getListeners().clone();
            int numListeners = tmpListeners.size();
            for (int i = 0; i < numListeners; ++i) {
                if (bStarted) {
                    tmpListeners.get(i).onAnimationStart(this);
                } else {
                    tmpListeners.get(i).onAnimationEnd(this);
                }
            }
        }
        if (!bStarted) {
            mEndListenersCalled = true;
        }
    }

}
