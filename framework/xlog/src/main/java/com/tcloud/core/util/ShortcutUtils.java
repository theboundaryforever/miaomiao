package com.tcloud.core.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;

import com.tcloud.core.log.L;

import java.util.List;

/**
 * Created by qingfeng on 15/6/1.
 */
public class ShortcutUtils {
    private static final String TAG = "ShortcutUtils";

    public static void createShortcut(Activity activity,
                                      final String title, int appIcon) {
        Intent target = new Intent();
        target.setClassName(activity, activity.getClass().getName());

        createShortcut(activity.getApplicationContext(), target, title, appIcon);
    }

    public static void createShortcut(Context context, Intent target, String title, Bitmap bitmap) {
        L.info(TAG, "create shortcut %s", title);
        Intent shortcutIntent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, title);
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bitmap);
        shortcutIntent.putExtra("duplicate", false);
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, target);
        context.sendBroadcast(shortcutIntent);
    }

    public static void createShortcut(Context context, Intent target, String title, int appIcon) {
        L.info(TAG, "create shortcut %s", title);
        Intent shortcutIntent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, title);
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(context, appIcon));
        shortcutIntent.putExtra("duplicate", false);
        shortcutIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, target);
        context.sendBroadcast(shortcutIntent);
    }

    public static boolean hasShortcut(Context context, final String title) {
        String AUTHORITY = getAuthorityFromPermission(context.getApplicationContext(),
                "com.android.launcher.permission.READ_SETTINGS");
        if (StringUtils.isNullOrEmpty(AUTHORITY)) {
            AUTHORITY = getAuthorityFromPermission(context.getApplicationContext(),
                    "com.android.launcher.permission.WRITE_SETTINGS");
        }
        L.debug(TAG, "authority: %s", AUTHORITY);
        final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/favorites?notify=true");
        try {
            Cursor c = context.getContentResolver().query(
                    CONTENT_URI,
                    new String[]{"title"},
                    "title=?",
                    new String[]{title},
                    null);
            if (c != null) {
                L.debug(TAG, "cursor cnt: %d", c.getCount());
                if (c.getCount() > 0 && c.moveToNext()) {
                    c.close();
                    return true;
                }
                c.close();
            } else {
                L.debug(TAG, "cursor is null");
            }
        } catch (Exception e) {
            L.error(TAG, e);
        }
        return false;
    }

    // ref: http://stackoverflow.com/questions/8501306/android-shortcut-access-launcher-db
    private static String getAuthorityFromPermission(Context context, String permission) {
        if (TextUtils.isEmpty(permission)) {
            return null;
        }
        List<PackageInfo> packs = context.getPackageManager().getInstalledPackages(
                PackageManager.GET_PROVIDERS);
        if (packs == null) {
            return null;
        }
        for (PackageInfo pack : packs) {
            ProviderInfo[] providers = pack.providers;
            if (providers != null) {
                for (ProviderInfo provider : providers) {
                    if (null == provider) { // skip null provider
                        continue;
                    }
                    if (permission.equals(provider.readPermission) ||
                            permission.equals(provider.writePermission)) {
                        return provider.authority;
                    }
                }
            }
        }
        return null;
    }

    public static boolean isSupportShortcut() {
        if (Build.MANUFACTURER.equalsIgnoreCase("xiaomi") ||
                Build.MANUFACTURER.equalsIgnoreCase("meizu") ||
                Build.MANUFACTURER.equalsIgnoreCase("huawei")) {
            return false;
        } else if (Build.MANUFACTURER.equalsIgnoreCase("lge")) {
            return true;
        } else {
            return true;
        }
    }

}
