package com.tcloud.core.util;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.tcloud.core.log.L;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chaoqun_liu on 2015/6/11.
 */
public class LocationUtil {
    private static Location sLastLocation;
    private static final String TAG = "LocationUtil";

    private static class SimpleLocationListener implements LocationListener {

        private LocationManager mLocationManager;
        private String mProviderName;

        public SimpleLocationListener(LocationManager context, String providerName) {
            mLocationManager = context;
            mProviderName = providerName;
        }

        public String getProviderName() {
            return mProviderName;
        }

        @Override
        public void onLocationChanged(Location location) {
            mLocationManager.removeUpdates(this);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }

    public static void requestLocation(final Context context, final LocationListener listener) {
        LocationManager locationManager
                = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        try {
            requestLocation(locationManager, listener);
        } catch (SecurityException e) {
            L.info(TAG, "[requestLocation] exception occurred, should be user denied location permission");
            L.error(TAG, e);
        } catch (Exception e) {
            L.error(TAG, e);
        }
    }

    public static Location getLocation() {
        return sLastLocation;
    }

//    private static final String KEY_REQUEST_LOCATION = "request_location";
//    private static final int REQ_CACHE_TIMEOUT_HOURS = 24;

    private static void requestLocation(LocationManager manager, LocationListener locationListener) {

//        boolean hasTimeOut = PeriodSkill.isFullChargeByHours(KEY_REQUEST_LOCATION, REQ_CACHE_TIMEOUT_HOURS);
//        if (!hasTimeOut) {
//            if (locationListener != null && sLastLocation != null) {
//                locationListener.onLocationChanged(sLastLocation);
//            }
//            return;
//        }
//
//        PeriodSkill.triggerByHours(KEY_REQUEST_LOCATION, REQ_CACHE_TIMEOUT_HOURS);

        String bestProvider = getBestProvider(manager);
        List<String> providers = manager.getProviders(true);

        if (FP.empty(providers)) {
            return;
        }
        Location lastKnownLocation = getLastKnownLocation(manager, bestProvider, providers);

        if (lastKnownLocation != null) {
            sLastLocation = lastKnownLocation;
        }
        requestUpdates(manager, providers, locationListener);
    }

    private static void requestUpdates(final LocationManager manager, List<String> providers, final LocationListener locationListener) {
        final List<LocationListener> listeners = new ArrayList<>(providers.size());

        for (final String provider : providers) {
            if (provider == null) {
                continue;
            }

            SimpleLocationListener listener = new SimpleLocationListener(manager, provider) {
                @Override
                public void onLocationChanged(Location location) {
                    super.onLocationChanged(location);

                    listeners.remove(this);

                    String providerName = getProviderName();
                    boolean isBestProvider = providerName.equals(getBestProvider(manager));
                    if (location != null) {
                        sLastLocation = location;
                        if (isBestProvider) {
                            for (LocationListener request : listeners) {
                                manager.removeUpdates(request);
                            }
                            listeners.clear();
                        }
                    }

                    if (FP.empty(listeners)) {
                        if (sLastLocation != null) {
                            if (locationListener != null) {
                                locationListener.onLocationChanged(sLastLocation);
                            }
                        }
                    }
                }
            };
            manager.requestLocationUpdates(provider, 1000, 1, listener);
            listeners.add(listener);

            //防止定位获取不到，一直定位。如果5s没回来就停止定位
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (listeners != null) {
                        for (LocationListener request : listeners) {
                            manager.removeUpdates(request);
                        }
                        listeners.clear();
                    }
                    handler.removeCallbacks(this);
                }
            }, 5000);
        }
    }

    @Nullable
    private static Location getLastKnownLocation(LocationManager manager, String bestProvider, List<String> providers) {
        Location returnValue = null;
        for (String provider : providers) {
            Location lastKnownLocation = manager.getLastKnownLocation(provider);
            if (lastKnownLocation != null) {
                returnValue = lastKnownLocation;
                if (provider.equals(bestProvider)) {
                    break;
                }
            }
        }
        return returnValue;
    }

    private static String getBestProvider(LocationManager locationManager) {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE); // 高精度
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW); // 低功耗

        try {
            return locationManager.getBestProvider(criteria, true);
        } catch (Exception e) {
            return "";
        }
    }
//
//    /**
//     * 强制帮用户打开GPS
//     *
//     * @param context
//     */
//    public static final void openGps(Context context) {
//        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//        intent.putExtra("enabled", true);
//        context.sendBroadcast(intent);
//
//        String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
//        if (!provider.contains("gps")) { //if gps is disabled
//            final Intent poke = new Intent();
//            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
//            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
//            poke.setData(Uri.parse("3"));
//            context.sendBroadcast(poke);
//        }
//    }
}
