package com.tcloud.core.log;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

class LogToES {
    public static final int MAX_FILE_SIZE = 2; // M bytes
    public static final int MAX_BUFFER_COUNT = 15;
    // can overwrite this path by ark.config(ark.common.path)
    public static File sRootDir;
    public static String sLogPath = "/kiwi/logs";
    public static final String LOG_NAME = "logs.txt";
    public static final String UE_LOG_NAME = "uncaught_exception.txt";
    public static final String NATIVE_LOG_NAME = "native_crash.txt";
    public static final String NATIVE_BAK_LOG_NAME = "native_crash.bak";

    public static Handler mLogHandler;

    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat LOG_FORMAT = new SimpleDateFormat(
            "yyyy:MM:dd HH:mm:ss.SSS");


    public static class LogMsg {
        public String path;
        public String fileName;
        public String msg;
        public Date date;
    }

    public static ConcurrentLinkedQueue<LogMsg> logMsgQueue = new ConcurrentLinkedQueue<>();
    public static AtomicInteger sLogMsgQueueSize = new AtomicInteger(0);
    public static final int MAX_LOG_MSG_QUEUE_SIZE = 1000;

    public static Runnable logRunnable = null;

    public static void writeLogToFile(String path,
                                      String fileName, String msg) {
        LogMsg logMsg = new LogMsg();
        logMsg.path = path;
        logMsg.fileName = fileName;
        logMsg.msg = msg;
        logMsg.date = new Date();

        // skip current log if log cache size is too much
        if (sLogMsgQueueSize.get() > MAX_LOG_MSG_QUEUE_SIZE) {
            return;
        }

        logMsgQueue.add(logMsg);
        sLogMsgQueueSize.incrementAndGet();

        if (logRunnable == null) {
            logRunnable = new Runnable() {

                @Override
                public void run() {
                    LogMsg logMsg = logMsgQueue.poll();
                    if (logMsg != null) {
                        sLogMsgQueueSize.decrementAndGet();
                    }
                    while (logMsg != null) {
                        try {
                            writeLogToFileReal(logMsg.path,
                                    logMsg.fileName,
                                    logMsg.msg,
                                    logMsg.date,
                                    false
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        logMsg = logMsgQueue.poll();
                        if (logMsg != null) {
                            sLogMsgQueueSize.decrementAndGet();
                        }
                    }
                    logRunnable = null;
                }
            };
            mLogHandler.post(logRunnable);
        }

    }

    @SuppressLint("SimpleDateFormat")
    public synchronized static void writeLogToFileReal(String path,
                                                       String fileName, String msg) throws IOException {

        Date date = new Date();
        writeLogToFileReal(path, fileName, msg, date, true);
    }

    @SuppressLint("SimpleDateFormat")
    private synchronized static void writeLogToFileReal(String path,
                                                        String fileName,
                                                        String msg,
                                                        Date date,
                                                        boolean immediately)
            throws IOException {
        File esdf = getRootDir();
        String dir = esdf.getPath() + path;
        File dirFile = new File(dir);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        File logFile = new File(dir + File.separator + fileName);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        } else {
            long fileSize = (logFile.length() >>> 20); // convert to M bytes
            if (fileSize > MAX_FILE_SIZE) {
                deleteOldLogs();

                SimpleDateFormat simpleDateFormate = new SimpleDateFormat(
                        "-MM-dd-kk-mm-ss");
                String fileExt = simpleDateFormate.format(date);

                StringBuilder sb = new StringBuilder(dir);
                sb.append(File.separator).append(fileName).append(fileExt)
                        .append(".bak");

                File fileNameTo = new File(sb.toString());
                logFile.renameTo(fileNameTo);
            }
        }

        StringBuffer sb = new StringBuffer();
        LOG_FORMAT.format(date, sb, new FieldPosition(0));
        sb.append(' ');
        sb.append(msg);
        sb.append('\n');

        // we can make FileWriter static, but when to close it
        LogFileWriter logFileWriter = new LogFileWriter(logFile.getAbsolutePath());
        logFileWriter.write(sb);
        if (immediately) {
            logFileWriter.flush();
        }
    }

    private static final long DAY_DELAY = 5 * 24 * 60 * 60 * 1000;

    private static void deleteOldLogs() {
        File extDir = getRootDir();
        if (!extDir.exists()) {
            return;
        }
        String dir = extDir.getAbsolutePath() + sLogPath;
        File dirFile = new File(dir);
        if (!dirFile.exists()) {
            return;
        }

        long now = System.currentTimeMillis();
        File files[] = dirFile.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().endsWith(".bak")) {
                    long lastModifiedTime = file.lastModified();
                    if (now - lastModifiedTime > DAY_DELAY) {
                        file.delete();
                    }
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat FILE_NAME_FORMAT = new SimpleDateFormat(
            "MM-dd_HH-mm-ss");
    private static final String LOGCAT_CMD[] = {"logcat", "-d", "-v", "time"};

    // to use this method, we should add
    // permission(android.permission.READ_LOGS) in Manifest
    public static void writeAllLogsToFile() {
        new Thread(new Runnable() {

            public void run() {
                try {
                    Date date = new Date();
                    Process process = Runtime.getRuntime().exec(LOGCAT_CMD);
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(process.getInputStream()),
                            1024);
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        sb.append(line);
                        sb.append(System.getProperty("line.separator"));
                    }
                    bufferedReader.close();

                    Log.i("yy", "all logs: " + sb.toString());

                    File esdf = getRootDir();
                    String dir = esdf.getAbsolutePath() + sLogPath;
                    File dirFile = new File(dir);
                    if (!dirFile.exists()) {
                        dirFile.mkdirs();
                    }
                    String fileName = dir + File.separator
                            + FILE_NAME_FORMAT.format(date) + ".log";
                    File file = new File(fileName);
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(sb.toString().getBytes());
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("yy", "writeAllLogsToFile " + e.toString());
                }
            }
        }).start();
    }

    private static File getRootDir() {
        return sRootDir == null ? Environment.getExternalStorageDirectory() : sRootDir;
    }

    public static synchronized void flushToDisk() {
        try {
            LogFileWriter.flushAll();
        } catch (Exception e) {
            L.error(LogToES.class.getName(), e);
        }
    }
}
