package com.tcloud.core.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class ImeUtil {
    public ImeUtil() {
    }

    public static void hideIME(Activity activity) {
        View view = activity.getCurrentFocus();
        if (null != view) {
            hideIME(activity, view);
        }

    }

    public static void hideIME(Activity activity, View v) {
        if (activity != null && v != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public static void showIME(Activity activity, View view) {
        if (null == view) {
            view = activity.getCurrentFocus();
            if (null == view) {
                return;
            }
        }

        ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(view, 2);
    }

    public static void showIME(Activity activity, View view, int flag) {
        if (null == view) {
            view = activity.getCurrentFocus();
            if (null == view) {
                return;
            }
        }

        ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(view, flag);
    }
}
