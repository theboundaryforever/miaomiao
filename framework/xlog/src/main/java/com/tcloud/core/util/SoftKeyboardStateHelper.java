package com.tcloud.core.util;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import com.tcloud.core.log.L;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SoftKeyboardStateHelper implements OnGlobalLayoutListener {
    private final List<SoftKeyboardStateListener> listeners;
    private final View activityRootView;
    private int lastSoftKeyboardHeightInPx;
    private boolean isSoftKeyboardOpened;
    private int mMaxRootHeight;

    public SoftKeyboardStateHelper(View activityRootView) {
        this(activityRootView, false);
    }

    public SoftKeyboardStateHelper(View activityRootView, boolean isSoftKeyboardOpened) {
        this.listeners = new LinkedList();
        this.activityRootView = activityRootView;
        this.isSoftKeyboardOpened = isSoftKeyboardOpened;
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    public void onGlobalLayout() {
        Rect r = new Rect();
        this.activityRootView.getWindowVisibleDisplayFrame(r);
        int rootHeight = this.activityRootView.getRootView().getHeight();
        int heightDiff = this.activityRootView.getRootView().getHeight() - (r.bottom - r.top);
        mMaxRootHeight = Math.max(rootHeight, mMaxRootHeight);
        L.verbose("SoftKeyboardStateHelper", " rooHeight = " + this.activityRootView.getRootView().getHeight() + " bottom = " + r.bottom + " r.top = " + r.top);
        L.verbose("SoftKeyboardStateHelper", " isSoftKeyboardOpened = " + isSoftKeyboardOpened + " heightDiff = " + heightDiff);
        if (!this.isSoftKeyboardOpened && heightDiff > 100) {
            this.isSoftKeyboardOpened = true;
            this.notifyOnSoftKeyboardOpened(heightDiff);
        } else if (this.isSoftKeyboardOpened && heightDiff < 100 && mMaxRootHeight == rootHeight) {
            this.isSoftKeyboardOpened = false;
            this.notifyOnSoftKeyboardClosed();
        }

    }

    public void setIsSoftKeyboardOpened(boolean isSoftKeyboardOpened) {
        this.isSoftKeyboardOpened = isSoftKeyboardOpened;
    }

    public boolean isSoftKeyboardOpened() {
        return this.isSoftKeyboardOpened;
    }

    public int getLastSoftKeyboardHeightInPx() {
        return this.lastSoftKeyboardHeightInPx;
    }

    public void addSoftKeyboardStateListener(SoftKeyboardStateListener listener) {
        this.listeners.add(listener);
    }

    public void removeSoftKeyboardStateListener(SoftKeyboardStateListener listener) {
        this.listeners.remove(listener);
    }

    private void notifyOnSoftKeyboardOpened(int keyboardHeightInPx) {
        this.lastSoftKeyboardHeightInPx = keyboardHeightInPx;
        Iterator var2 = this.listeners.iterator();

        while (var2.hasNext()) {
            SoftKeyboardStateListener listener = (SoftKeyboardStateListener) var2.next();
            if (listener != null) {
                listener.onSoftKeyboardOpened(keyboardHeightInPx);
            }
        }

    }

    private void notifyOnSoftKeyboardClosed() {
        Iterator var1 = this.listeners.iterator();

        while (var1.hasNext()) {
            SoftKeyboardStateListener listener = (SoftKeyboardStateListener) var1.next();
            if (listener != null) {
                listener.onSoftKeyboardClosed();
            }
        }

    }

    public interface SoftKeyboardStateListener {
        void onSoftKeyboardOpened(int keyboardHeight);

        void onSoftKeyboardClosed();
    }
}
