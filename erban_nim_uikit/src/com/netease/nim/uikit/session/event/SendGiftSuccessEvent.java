package com.netease.nim.uikit.session.event;

/**
 * 用于更新私聊页的亲密数值
 * <p>
 * Created by zhangjian on 2019/5/7.
 */
public class SendGiftSuccessEvent {
    private int useGiftPurseGold;

    public SendGiftSuccessEvent(int useGiftPurseGold) {
        this.useGiftPurseGold = useGiftPurseGold;
    }

    public int getUseGiftPurseGold() {
        return useGiftPurseGold;
    }
}
