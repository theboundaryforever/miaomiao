package com.netease.nim.uikit.session.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.OnlineStateChangeListener;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.custom.ICpNotifyCoreClient;
import com.netease.nim.uikit.custom.UserEvent;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nim.uikit.model.ToolBarOptions;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.SessionEventListener;
import com.netease.nim.uikit.session.constant.Extras;
import com.netease.nim.uikit.session.event.SendGiftSuccessEvent;
import com.netease.nim.uikit.session.fragment.MessageFragment;
import com.netease.nim.uikit.uinfo.UserInfoHelper;
import com.netease.nim.uikit.uinfo.UserInfoObservable;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomNotification;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tcloud.core.log.L;
import com.tongdaxing.erban.libcommon.utils.QMUI.QMUIStatusBarHelper;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.BaseConstant;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Set;


/**
 * 点对点聊天界面
 * <p/>
 * Created by huangjun on 2015/2/1.
 */
public class P2PMessageActivity extends BaseMessageActivity {
    private String TAG = "P2PMessageActivity";
    private boolean isResume = false;
    private SessionEventListener sessionEventListener;
    private ImageView ivCp;
    private FrameLayout flCpLevel;
    private TextView tvCpCloseLevel;
    private int mCpCloseLevel;
    private int cpState = 0;//CP状态 0自己没有cp-空心 1跟对方是cp-红心 2自己有cp但跟对方不是cp-不显示心

    public static void start(Context context, String contactId, boolean cpInvite, SessionCustomization customization, IMMessage anchor) {
        Intent intent = new Intent();
        intent.putExtra(Extras.EXTRA_ACCOUNT, contactId);
        intent.putExtra(Extras.EXTRA_CUSTOMIZATION, customization);
        LogUtil.d("start P2PMessageActivity cpInvite = " + cpInvite);
        intent.putExtra(Extras.EXTRA_CP_INVITE, cpInvite);
        if (anchor != null) {
            intent.putExtra(Extras.EXTRA_ANCHOR, anchor);
        }
        intent.setClass(context, P2PMessageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 单聊特例话数据，包括个人信息，
        requestBuddyInfo();
        displayOnlineState();
        registerObservers(true);
        registerOnlineStateChangeListener(true);

        EventBus.getDefault().register(this);
        CoreManager.addClient(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        QMUIStatusBarHelper.translucent(this, 0xffffffff);
        QMUIStatusBarHelper.setStatusBarLightMode(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResume = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isResume = false;
    }

    private RelativeLayout rlIsTheRoom;

    public static void start(Context context, String contactId, SessionCustomization customization, IMMessage anchor) {
        start(context, contactId, false, customization, anchor);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        CoreManager.removeClient(this);
        try {
            registerObservers(false);
            registerOnlineStateChangeListener(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ImageView p2pMessageAvatar;

    private void requestBuddyInfo() {
        // 显示自己的textview并且居中
        TextView tvToolbarTitle = getToolBar().findViewById(R.id.tv_toolbar_title);
        ImageView ivPersonalInfo = getToolBar().findViewById(R.id.iv_personal_info);
        p2pMessageAvatar = (ImageView) findViewById(R.id.p2p_message_avatar);
        rlIsTheRoom = (RelativeLayout) findViewById(R.id.rl_is_the_room);
        ivCp = (ImageView) findViewById(R.id.iv_cp);
        flCpLevel = (FrameLayout) findViewById(R.id.fl_cp_level);
        tvCpCloseLevel = (TextView) findViewById(R.id.iv_cp_close_level);
        final String userTitleName = UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P);
        sessionEventListener = NimUIKit.getSessionListener();
        ivPersonalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMMessage imMessage = MessageBuilder.createTipMessage(sessionId, SessionTypeEnum.P2P);//随便构造一个实体
                imMessage.setFromAccount(sessionId);
                if (sessionEventListener != null) {
                    sessionEventListener.onAvatarClicked(getBaseContext(), imMessage);
                }
            }
        });

        tvToolbarTitle.setText(userTitleName);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        Log.e("requestBuddyInfo", "requestBuddyInfo: " + userTitleName);
        setTitle("");
        if (sessionEventListener != null) {
            sessionEventListener.loadRoomStatus(Long.valueOf(sessionId), new SessionEventListener.LoadRoomStatusCallback() {
                @Override
                public void callback(final long uid, String avatar) {//uid是房间roomUid
                    rlIsTheRoom.setVisibility(View.VISIBLE);
                    GlideApp.with(P2PMessageActivity.this).load(avatar)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .dontAnimate()
                            .circleCrop()
                            .placeholder(R.drawable.icon_empty)
                            .into(p2pMessageAvatar);
                    rlIsTheRoom.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (sessionEventListener != null) {
                                sessionEventListener.onEnterRoom(P2PMessageActivity.this, uid, Long.valueOf(sessionId), userTitleName);
                            }
                        }
                    });
                }
            });
        }

        ivCp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cpState == 1) {
                    //有CP 打开CP专属页 交给MainActivity处理
                    if (sessionEventListener != null) {
                        sessionEventListener.openCpExclusive(P2PMessageActivity.this, Long.valueOf(sessionId));
                    }
                } else {
                    //打开戒指栏 交给MainActivity处理
                    if (sessionEventListener != null) {
                        sessionEventListener.openCpGiftDialog(P2PMessageActivity.this, Long.valueOf(sessionId));
                    }
                }
            }
        });
        checkCPState();
        //从他人资料页CP按钮点击过来，自动打开戒指栏
        autoOpenCpDialog(getIntent());
    }

    @CoreEvent(coreClientClass = ICpNotifyCoreClient.class)
    public void onP2PCpInviteAgree() {
        LogUtil.d("P2PMessageActivity onP2PCpInviteAgree CP配对成功啦");
        checkCPState();
    }

    @CoreEvent(coreClientClass = ICpNotifyCoreClient.class)
    public void onP2PCpRemove() {
        LogUtil.d("P2PMessageActivity onP2PCpRemove 你们的CP关系已解除");
        checkCPState();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtil.d("onNewIntent autoOpenCpDialog");
        autoOpenCpDialog(intent);
    }

    private void autoOpenCpDialog(Intent intent) {
        if (isDestroyed() || isFinishing()) {
            return;
        }
        LogUtil.d("autoOpenCpDialog CP_INVITE = " + intent.getBooleanExtra(Extras.EXTRA_CP_INVITE, false));
        //从他人资料页CP按钮点击过来，自动打开戒指栏
        if (intent.getBooleanExtra(Extras.EXTRA_CP_INVITE, false)) {
            if (sessionEventListener != null) {
                sessionEventListener.openCpGiftDialog(P2PMessageActivity.this, Long.valueOf(sessionId));
            }
        }
    }

    /**
     * 从个人信息跳转过来，打开普通礼物或者cp礼物弹窗
     *
     * @param userEvent
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showGiftDialogFromUserInfoDialog(UserEvent userEvent) {
        //打开戒指栏 交给MainActivity处理
        if (isDestroyed() || isFinishing()) {
            return;
        }
        if (userEvent != null && userEvent.getUid() == Long.valueOf(sessionId) && sessionEventListener != null) {
            if (userEvent.isShowCpGift()) {
                sessionEventListener.openCpGiftDialog(P2PMessageActivity.this, Long.valueOf(sessionId));
            } else {
                sessionEventListener.openGiftDialog(P2PMessageActivity.this, Long.valueOf(sessionId));
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshCpCloseLevel(SendGiftSuccessEvent event) {
        if (flCpLevel.getVisibility() == View.VISIBLE) {
            mCpCloseLevel += event.getUseGiftPurseGold();
            showCpLevel();
        }
    }

    /**
     * 检查Cp状态
     * CP状态 0自己没有cp-空心 1跟对方是cp-红心 2自己有cp但跟对方不是cp-不显示心
     * <p>
     * 判断己方是否有CP，
     * 如无，则在私聊页中看所有人均为空心状态；cpState = 0 空心
     * 如有CP，
     * 则判断对方是否为我的CP，如是，则展示红心（满），cpState = 1 红心 + 亲密值
     * 如不是，则不展示任何心（空心也不展示） cpState = 2 不显示心
     */
    private void checkCPState() {
        L.debug(TAG, "checkCPState() -> sessionId = %s, BaseConstant.getmiaomiaoOfficialAccount() = %s", sessionId, BaseConstant.getmiaomiaoOfficialAccount());
        if (BaseConstant.getmiaomiaoOfficialAccount().equals(sessionId)) {//喵喵酱不显示cp按钮
            ivCp.setVisibility(View.GONE);
            flCpLevel.setVisibility(View.GONE);
            return;
        }
        SessionEventListener sessionEventListener = NimUIKit.getSessionListener();
        if (sessionEventListener != null) {
            sessionEventListener.getCpState(Long.valueOf(sessionId), new SessionEventListener.GetCpStateCallback() {
                @Override
                public void callback(int cpState, int closeLevel) {
                    P2PMessageActivity.this.cpState = cpState;
                    switch (cpState) {
                        case 0:
                            ivCp.setVisibility(View.VISIBLE);
                            ivCp.setSelected(false);
                            flCpLevel.setVisibility(View.GONE);
                            break;
                        case 1:
                            ivCp.setVisibility(View.VISIBLE);
                            ivCp.setSelected(true);
                            flCpLevel.setVisibility(View.VISIBLE);
                            mCpCloseLevel = closeLevel;
                            showCpLevel();
                            break;
                        case 2:
                            ivCp.setVisibility(View.GONE);
                            flCpLevel.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    /**
     * 展示cp亲密数值
     */
    private void showCpLevel() {
        StringBuilder text = new StringBuilder();
        if (mCpCloseLevel / 10000 >= 1) {
            text.append(new DecimalFormat("#.##").format(mCpCloseLevel / 10000f));
            text.append("万");
        } else {
            text.append(mCpCloseLevel);
        }
        tvCpCloseLevel.setText(text);
    }

    private void registerObservers(boolean register) {
        if (register) {
            registerUserInfoObserver();
        } else {
            unregisterUserInfoObserver();
        }
        NIMClient.getService(MsgServiceObserve.class).observeCustomNotification(commandObserver, register);
//        FriendDataCache.getInstance().registerFriendDataChangedObserver(friendDataChangedObserver, register);
    }

    private UserInfoObservable.UserInfoObserver uinfoObserver;

    OnlineStateChangeListener onlineStateChangeListener = new OnlineStateChangeListener() {
        @Override
        public void onlineStateChange(Set<String> accounts) {
            // 更新 toolbar
            if (accounts.contains(sessionId)) {
                // 按照交互来展示
                displayOnlineState();
            }
        }
    };

    private void registerOnlineStateChangeListener(boolean register) {
        if (!NimUIKit.enableOnlineState()) {
            return;
        }
        if (register) {
            NimUIKit.addOnlineStateChangeListeners(onlineStateChangeListener);
        } else {
            NimUIKit.removeOnlineStateChangeListeners(onlineStateChangeListener);
        }
    }

    private void displayOnlineState() {
        if (!NimUIKit.enableOnlineState()) {
            return;
        }
        String detailContent = NimUIKit.getOnlineStateContentProvider().getDetailDisplay(sessionId);
        setSubTitle(detailContent);
    }

    private void registerUserInfoObserver() {
        if (uinfoObserver == null) {
            uinfoObserver = new UserInfoObservable.UserInfoObserver() {
                @Override
                public void onUserInfoChanged(List<String> accounts) {
                    if (accounts.contains(sessionId)) {
                        requestBuddyInfo();
                    }
                }
            };
        }

        UserInfoHelper.registerObserver(uinfoObserver);
    }

    private void unregisterUserInfoObserver() {
        if (uinfoObserver != null) {
            UserInfoHelper.unregisterObserver(uinfoObserver);
        }
    }

    /**
     * 命令消息接收观察者
     */
    Observer<CustomNotification> commandObserver = new Observer<CustomNotification>() {
        @Override
        public void onEvent(CustomNotification message) {


            // : 2018/4/12
            if (!sessionId.equals(message.getSessionId()) || message.getSessionType() != SessionTypeEnum.P2P) {
                return;
            }
            showCommandMessage(message);
        }
    };

    protected void showCommandMessage(CustomNotification message) {
        if (!isResume) {
            return;
        }

        String content = message.getContent();
        try {
            JSONObject json = JSON.parseObject(content);
            int id = json.getIntValue("id");
            if (id == 1) {
                // 正在输入
                Toast.makeText(P2PMessageActivity.this, "对方正在输入...", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {

        }
    }

    @Override
    protected MessageFragment fragment() {
        MessageFragment fragment = new MessageFragment();
        Bundle arguments = getIntent().getExtras();
        if (arguments != null)
            arguments.putSerializable(Extras.EXTRA_TYPE, SessionTypeEnum.P2P);
        fragment.setArguments(arguments);
        fragment.setContainerId(R.id.message_fragment_container);
        return fragment;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.nim_message_activity;
    }

    @Override
    protected void initToolBar() {
        ToolBarOptions options = new ToolBarOptions();
        setToolBar(R.id.toolbar, options);
    }

    //设置单聊页面状态栏为黑色
    @Override
    public boolean blackStatusBar() {
        return false;
    }
}
