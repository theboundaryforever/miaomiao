package com.netease.nim.uikit.session;

import android.content.Context;

import com.netease.nimlib.sdk.msg.model.IMMessage;

/**
 * 会话窗口消息列表一些点击事件的响应处理函数
 */
public interface SessionEventListener {

    // 头像点击事件处理，一般用于打开用户资料页面
    void onAvatarClicked(Context context, IMMessage message);

    // 头像长按事件处理，一般用于群组@功能，或者弹出菜单，做拉黑，加好友等功能
    void onAvatarLongClicked(Context context, IMMessage message);

    //应急途径，暂时这么做吧
    void onEnterRoom(Context context, long uid, long userId, String userTitleName);

    void loadRoomStatus(long queryUid, LoadRoomStatusCallback loadRoomStatusCallback);

    interface LoadRoomStatusCallback {
        void callback(long uid, String avatar);
    }

    // 获取CP状态 targetUid为对方的uid
    void getCpState(long targetUid, GetCpStateCallback callback);

    // 打开礼物栏
    void openGiftDialog(Context context, long uid);

    // 打开Cp戒指栏
    void openCpGiftDialog(Context context, long uid);

    // 打开Cp专属页
    void openCpExclusive(Context context, long uid);

    interface GetCpStateCallback {
        void callback(int cpState, int closeLevel);
    }
}
