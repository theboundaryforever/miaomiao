package com.netease.nim.uikit.cache;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SpEvent;
import com.tongdaxing.erban.libcommon.utils.SpUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

/**
 * 忽略未读消息缓存
 */
public class MsgIgnoreCache {
    private static final String TAG = "MsgIgnoreCache";
    private volatile static MsgIgnoreCache instance;

    public static MsgIgnoreCache getInstance() {
        if (instance == null) {
            synchronized (MsgIgnoreCache.class) {
                if (instance == null) {
                    instance = new MsgIgnoreCache();
                }
            }
        }
        return instance;
    }

    private MsgIgnoreCache() {
    }

    /**
     * 保存忽略消息未读
     *
     * @param contactId 联系人ID
     * @param number    忽略条数
     */
    public void saveMsgIgnoreNumber(Context context, String contactId, int number) {
        try {
            JSONObject jsonObject = getMsgIgnoreNumberCache(context);
            jsonObject.put(contactId, number);
            SpUtils.put(context, SpEvent.messageIgnoreCache, jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存忽略消息未读
     *
     * @param recentContacts 联系人列表
     */
    public void saveMsgIgnoreNumber(Context context, List<RecentContact> recentContacts) {
        if (!ListUtils.isListEmpty(recentContacts)) {
            try {
                JSONObject jsonObject = getMsgIgnoreNumberCache(context);
                for (RecentContact recentContact : recentContacts) {
                    jsonObject.put(recentContact.getContactId(), recentContact.getUnreadCount());
                    MsgIgnoreCache.getInstance().saveMsgIgnoreNumber(context, recentContact.getContactId(), recentContact.getUnreadCount());
                }
                SpUtils.put(context, SpEvent.messageIgnoreCache, jsonObject.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取忽略消息未读条数
     *
     * @param contactId 联系人ID
     * @return 忽略条数
     */
    public int getMsgIgnoreNumber(Context context, String contactId) {
        JSONObject jsonObject = getMsgIgnoreNumberCache(context);
        if (jsonObject.has(contactId)) {
            try {
                return (Integer) jsonObject.get(contactId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    /**
     * 获取忽略消息未读条数（总数）
     *
     * @return 忽略条数
     */
    public int getMsgIgnoreNumber(Context context) {
        JSONObject jsonObject = getMsgIgnoreNumberCache(context);
        int ignoreNumber = 0;
        try {
            for (Iterator<String> it = jsonObject.keys(); it.hasNext(); ) {
                String key = it.next();
                ignoreNumber += (Integer) jsonObject.get(key);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ignoreNumber;
    }

    /**
     * 清除本地缓存的忽略未读消息数
     */
    public void clearAllMsgIgnoreNumber(Context context) {
        SpUtils.put(context, SpEvent.messageIgnoreCache, "");
    }

    /**
     * 清除本地缓存的忽略未读消息数
     */
    public void clearMsgIgnoreNumber(Context context, String contactId) {
        try {
            JSONObject jsonObject = getMsgIgnoreNumberCache(context);
            if (jsonObject.has(contactId)) {
                jsonObject.put(contactId, 0);
                SpUtils.put(context, SpEvent.messageIgnoreCache, jsonObject.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private @NonNull
    JSONObject getMsgIgnoreNumberCache(Context context) {
        String cacheStr = SpUtils.get(context, SpEvent.messageIgnoreCache, "").toString();
        if (!TextUtils.isEmpty(cacheStr)) {
            try {
                return new JSONObject(cacheStr);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return new JSONObject();
    }
}
