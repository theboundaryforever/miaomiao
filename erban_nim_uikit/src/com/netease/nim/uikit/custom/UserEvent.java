package com.netease.nim.uikit.custom;

/**
 * <p>
 * Created by zhangjian on 2019/4/28.
 */
public class UserEvent {
    private long uid;
    private boolean showCpGift;

    public UserEvent(long uid) {
        this.uid = uid;
    }

    public UserEvent(long uid, boolean showCpGift) {
        this.uid = uid;
        this.showCpGift = showCpGift;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public boolean isShowCpGift() {
        return showCpGift;
    }

    public void setShowCpGift(boolean showCpGift) {
        this.showCpGift = showCpGift;
    }
}
