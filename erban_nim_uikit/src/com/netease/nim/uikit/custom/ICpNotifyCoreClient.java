package com.netease.nim.uikit.custom;

import com.tongdaxing.xchat_framework.coremanager.ICoreClient;

/**
 * cp透传消息处理 同意cp和cp解除
 * <p>
 * Created by zhangjian on 2019/3/29.
 */
public interface ICpNotifyCoreClient extends ICoreClient {
    public static final String METHOD_ON_P2P_CP_INVITE_AGREE = "onP2PCpInviteAgree";
    public static final String METHOD_ON_P2P_CP_REMOVE = "onP2PCpRemove";

    void onP2PCpInviteAgree();

    void onP2PCpRemove();
}
